<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Workout extends Model
{
    //
    protected $table='workout';
    protected $fillable=['member_id','outlet_id','settype_id','bodypart_id','excercise_id','sets','rep','rest','video_url','week'];
}
