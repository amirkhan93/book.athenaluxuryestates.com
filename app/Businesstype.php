<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Businesstype extends Model
{
    //
    protected $table='businesstype';
    protected  $fillable=[ 'name','code','choices'];
}
