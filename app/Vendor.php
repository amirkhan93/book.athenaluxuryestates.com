<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
class Vendor extends Authenticatable
{
    //
    use Notifiable;
    protected $guard = 'vendor';
    protected $table='vendors';
    protected $fillable=['name','code','email','password','businessname','businesspackage_id','businesstype_id','profile_image'];
}
