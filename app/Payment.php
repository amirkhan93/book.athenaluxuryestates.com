<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    //
    protected $table='paymentmode';
    protected $fillable=['name','code','vendor_id'];
}
