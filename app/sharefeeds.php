<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class sharefeeds extends Model
{
    protected $table='sharefeeds';
    protected $fillable=['feed_id','shared_by_id','shared_user_id'];
}
