<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bankname extends Model
{
    //
    protected $table='bankname';
    protected $fillable=['name','code','vendor_id'];
}
