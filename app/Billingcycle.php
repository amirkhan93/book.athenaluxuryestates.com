<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Billingcycle extends Model
{
    //
    protected $table='billingcycle';
    protected $fillable=['name','code','vendor_id'];
}
