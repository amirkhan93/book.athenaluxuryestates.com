<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Businesspackage extends Model
{
    //
    protected $table='businesspackage';
    protected  $fillable=[ 'name','code','price','taxtype','vendor_id','start_date','enddate'];
}
