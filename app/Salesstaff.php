<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salesstaff extends Model
{
    //
    protected $table='salesstaff';
    protected $fillable=['outlet_id','name','code'];
}
