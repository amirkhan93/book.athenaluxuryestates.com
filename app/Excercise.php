<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Excercise extends Model
{
    //
    protected $table='excercisetype';
    protected $fillable=['name','code'];
}
