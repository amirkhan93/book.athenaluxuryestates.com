<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Followedmember extends Model
{
    protected $table='followed_member';
    protected $fillable=['following_member_id','follower_member_id','follow_type'];
}
