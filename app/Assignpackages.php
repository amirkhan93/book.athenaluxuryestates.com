<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assignpackages extends Model
{
    //
    protected $table='assignpackages';
    protected $fillable=['outlet_id'];
}
