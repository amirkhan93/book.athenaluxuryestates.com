<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WorkoutMapping extends Model
{
    //
    protected $table='member_workout_mapping';
    protected $fillable=['member_id','workout_id'];
}
