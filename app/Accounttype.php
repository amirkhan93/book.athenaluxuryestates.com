<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Accounttype extends Model
{
    //
    protected $table='accounttype';
    protected $fillable=['name','code','vendor_id'];
}
