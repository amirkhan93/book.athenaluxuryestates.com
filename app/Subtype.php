<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subtype extends Model
{
    //
    protected $table='subsettype';
    protected $fillable=['name','code'];
}
