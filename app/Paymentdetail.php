<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paymentdetail extends Model
{
    //
    protected $table='paymentdetail';
    protected $fillable=['paymentmode_id,note','outlet_id'];
}
