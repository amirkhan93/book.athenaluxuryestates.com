<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendorusers extends Model
{
    //
    protected $table='vendor_users';
    protected $fillable=['username','code','email','roles_id','outlet_id','vendor_id'];
}
