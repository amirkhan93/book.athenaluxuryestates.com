<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class tax extends Model
{
    protected $table='tax';
    protected $fillable=['name','percentage'];
}
