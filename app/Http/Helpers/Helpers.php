<?php

namespace App\Http\Helpers;

use App\Addnewoutlet;
use App\User;
use App\Vendor;
use View;
use Session;
use App\Permission;
use App\Role_user;
use App\Permission_role;
use DB;
class Helpers
{

    public static function has_permission($user_id, $permissions = '')
    {
       // echo $user_id;die;
        $permissions = explode('|', $permissions);
        $user_permissions = DB::table('permissions')->whereIn('name', $permissions)->get();
        $permission_id = [];
        $i = 0;
        foreach ($user_permissions as $value) {
            $permission_id[$i++] = $value->id;
        }
        $role = DB::table('role_user')->where('user_id', $user_id)->first();

        if(count($permission_id) && isset($role->role_id)){
            $has_permit = DB::table('permission_role')->where('role_id', $role->role_id)->whereIn('permission_id', $permission_id);
            return $has_permit->count();
        }
        else return 0;
    }

    public static function outlets(){
           $outlet=Session::get('outlet_id');
    }

    public static function getAdminDetail($id){
        $results  =User::where('id',$id)->first();
        return $results;
    }

    public static function getVendorDetail($id){
        $results  =Vendor::where('id',$id)->first();
        return $results;
    }

    public static function getOutletDetail($id){
        $results  =Addnewoutlet::where('id',$id)->first();
        return $results;
    }

    public static function languages()
    {
        $languages = Language::where('status', '1')->get();
        $default_language = Language::where('default_language', 1)->first()->value;
        return array('languages' => $languages, 'default_languages' => $default_language);
    }

}
