<?php

namespace App\Http\Controllers\outlet;

use App\Addnewlead;
use App\Feedback;
use App\Source;
use App\Status;
use Session;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Addnewoutlet;


class AddnewleadController extends Controller
{
    //
    public function index()
    {
        if(Auth::guard('outlet')->id()) {
            $bn = Addnewlead::where('outlet_id',Auth::guard('outlet')->id())->get();
            return view('outlet.addnewlead.index',compact('bn'));
        }
    }
    public function create()
    {
        if (Auth::guard('outlet')->id()) {
            $an=Addnewoutlet::where('id',Auth::guard('outlet')->id())->first();
            $ac = Source::where('vendor_id',$an->vendor_id)->get();
            $acs = Status::where('vendor_id',$an->vendor_id)->get();
            return view('outlet.addnewlead.create', compact('ac', 'acs'));
        }
    }
    public function store(Request $request){

        $validator = $request->validate([
            'name' => 'required',
            'email' => 'required|unique:addnewlead',
            'sources' => 'required',
            'status' => 'required',
            'contact' => 'required',
            'nextfollow' => 'required',


        ], [
            'name.required' => '*Name is required',
            'email.required' => '*Email is required',
            'sources.required' => '*Sources is required',
            'status.required' => '*status is required',
            'contact.required' => '*Contact is required',
            'nextfollow.required' => '*nextfollow is required',

        ]);
        if ($validator == false) {
            return back();
        } else {

            //echo"helloo";die;
            $fileName = '';
            $file = $_FILES['profile_pic1'];
           // echo '<pre>';
           // print_r($file);exit;
            $file2 = $_POST['profile_pic'];
            if ($_FILES['profile_pic1']['name'] != "") {
                $targets = base_path() . "/resources/assets/images/";
                $extensions = strrchr($_FILES['profile_pic1']['name'], "."); //extracting the extension
                if ($extensions = ".txt" || $extensions = ".doc" || $extensions = ".docx" || $extensions = ".png" || $extensions = ".gif" || $extensions = ".jpeg" || $extensions = ".pdf") {
                    $fileName = rand() . $_FILES['profile_pic1']['name'];
                    if (move_uploaded_file($_FILES['profile_pic1']['tmp_name'], $targets . $fileName)) {

                    }
                }

            } else if($file2){
                $image_parts = explode(";base64,", $file2);
                $image_type_aux = explode("image/", $image_parts[0]);
                $image_type = $image_type_aux[1];

                $image_base64 = base64_decode($image_parts[1]);
                $fileName = uniqid() . '.png';

                $targets = base_path() . "/resources/assets/images/";
                $file = $targets . $fileName;
                file_put_contents($file, $image_base64);
            }

            $vendor_id = Auth::guard('outlet')->id();
            $input = $request->all();
            $products = New Addnewlead();
            $products->outlet_id = $vendor_id;
            $products->name = $request->name;
            $products->email = $request->email;
            $products->contact = $request->contact;
            $products->status_id = $request->status;
            $products->source_id = $request->sources;
            $products->gender = $request->gender;
            $products->dob = $request->dob;
            $products->followup = $request->nextfollow;
            $products->traildate = $request->traildate;
            $products->trailtime = $request->trailtime;
            $products->address = $request->address;
            $products->note = $request->note;
            $products->description = $request->description;
            $products->workname = $request->workname;
            $products->profile_pic = $fileName;

            //  echo $request->images;die;
            //echo $file = $_FILES['image']['tmp_name'];die;

            //  $products->image= $profileimages;


            $code_get_id = Addnewlead::select('code')->where('outlet_id',$vendor_id)->orderBy('id','desc')->first();
            // echo " $catlast";die;
            $code_id = '';
            if($code_get_id['code'] == ''):
                $code_id = 'ANl001';
            else:
                $code_id = "".$code_get_id['code']."";
                $code_id++;
            endif;
            $products->code = $code_id;
            $products->save();
            return redirect('outlet/addnewlead')->with('Success', 'Sucessfully add');
        }
    }
    public function view($id)
    {
        $bn = Addnewlead::where('id', $id)->get();
        return view('outlet.addnewlead.view',compact('bn'));
    }
    public function feedlist($id)
    {
        $bn = Addnewlead::where('id', $id)->get();
        return view('outlet.addnewlead.feedlist',compact('bn'));
    }
    public function edit($id)
    {
        if (Auth::guard('outlet')->id() != "") {
            $an=Addnewoutlet::where('id',Auth::guard('outlet')->id())->first();
            $act = Source::where('vendor_id',$an->vendor_id)->get();
            $acs = Status::where('vendor_id',$an->vendor_id)->get();
            $ac = Addnewlead::FindorFail($id);
            return view('outlet.addnewlead.edit', compact('ac', 'acs', 'act'));
        }
    }

    public function delete($id)
    {
        if (Auth::guard('outlet')->id() != "") {

            $ac = Addnewlead::where('id',$id)->delete();
            
            return redirect('outlet/addnewlead');
        }
    }

    public function update(Request $request,$id)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {

            $user = Addnewlead::findOrFail($id);
            $user->update($validator);
            return redirect('outlet/addnewlead');
        }
    }
    public function destroy($id)
    {
        $bn = Addnewlead::findOrFail($id);
        $bn->delete();
        return redirect('outlet/addnewlead');
    }
    public function active($id)
    {
        $subCat = Addnewlead::where('id', $id)->first();
        $subCat->status = 1;
        $subCat->save();
        return redirect()->back();
    }

    public function inactive($id)
    {
        //echo 'd';exit;
        $subCat =  Addnewlead::where('id', $id)->first();
        $subCat->status = 0;
        $subCat->save();
        return redirect()->back();
    }

    public function feedstore(Request $request){
        $vendor_id = Auth::guard('outlet')->id();
        $input = $request->all();

        // echo implode($input) ;die;
           // echo $request->message;die;
            //  echo $request->images;die;
            //echo $file = $_FILES['image']['tmp_name'];die;
       // $addnewlead_id = DB::table('addnewlead')->get('id');
               $products  =New Feedback();
        $products->outlet_id = $vendor_id;
      //  $products->addnewlead_id= $addnewlead_id;
              $products->message= $request->message;
              $products->save();
             // echo 'll';exit;
  return redirect('outlet/addnewlead')->with('Success', 'add');
        }


        public function followupleads(){
            if(Auth::guard('outlet')->id()!="") {
                $bn = Addnewlead::where('outlet_id',Auth::guard('outlet')->id())->get();
                return view('outlet.addnewlead.followleads',compact('bn'));
            }
        }

    public function activeleads(){
        if(Auth::guard('outlet')->id()!="") {
            $bn = Addnewlead::where('outlet_id',Auth::guard('outlet')->id())->where('status','=',1)->get();
            return view('outlet.addnewlead.activeleads',compact('bn'));
        }
    }
    public function archivedleads(){
        if(Auth::guard('outlet')->id()!="") {
            $bn = Addnewlead::where('outlet_id',Auth::guard('outlet')->id())->where('status','=',0)->get();
            return view('outlet.addnewlead.archivedleads',compact('bn'));
        }
    }

}
