<?php

namespace App\Http\Controllers\outlet;

use App\Salesstaff;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Auth;

class SalesstaffController extends Controller
{
    //
    public function index()
    {
        if (Auth::guard('outlet')->id()) {
           // echo Auth::guard('outlet')->id();die;
            $bst = Salesstaff::where('outlet_id',Auth::guard('outlet')->id())->get();
            return view('outlet.salesstaff.index', compact('bst'));
        }
    }
    public function create()
    {
        return view('outlet.salesstaff.create');
    }
    public function store(Request $request)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {
            $outlet_id = Auth::guard('outlet')->id();
          //  echo $outlet_id;die;
            $input = $request->all();
            $products = New Salesstaff();
            $products->name = $request->name;
            $products->outlet_id = $outlet_id;

            // echo " $catlast";die;
            $code_get_id = Salesstaff::select('code')->where('outlet_id',$outlet_id)->orderBy('id','desc')->first();
            // echo " $catlast";die;
            $code_id = '';
            if($code_get_id['code'] == ''):
                $code_id = 'STAFF001';
            else:
                $code_id = "".$code_get_id['code']."";
                $code_id++;
            endif;
            $products->code = $code_id;
            $products->save();
            return redirect('outlet/salesstaff')->with('Success', 'Sucessfully add');
        }
    }
    public function edit($id){
        $ac = Salesstaff::findOrFail($id);
        return view('outlet.salesstaff.edit',compact('ac'));
    }
    public function update(Request $request,$id)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {

            $user = Salesstaff::findOrFail($id);
            $user->update($validator);
            return redirect('outlet/salesstaff');
        }
    }
    public function destroy($id)
    {
        $acs = Salesstaff::findOrFail($id);
        $acs->delete();

        return redirect('outlet/salesstaff');
    }
    public function active($id)
    {
        $subCat =  Salesstaff::where('id', $id)->first();
        $subCat->status = 1;
        $subCat->save();
        return redirect()->back();
    }

    public function inactive($id)
    {
        //echo 'd';exit;
        $subCat =  Salesstaff::where('id', $id)->first();
        $subCat->status = 0;
        $subCat->save();
        return redirect()->back();
    }




}
