<?php

namespace App\Http\Controllers\outlet;

use App\Addnewmember;
use App\Excercise;
use App\Settype;
use App\Subtype;
use App\Workout;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Auth;
class WorkoutController extends Controller
{
    //
    public function index()
    {
        if (Auth::guard('outlet')->id()) {
            $bst = Workout::where('outlet_id', Auth::guard('outlet')->id())->get();
            return view('outlet.workout.index', compact('bst'));

        }
    }

    public function create()
    {
        if (Auth::guard('outlet')->id()) {
            $ac = Addnewmember::where('outlet_id', Auth::guard('outlet')->id())->get();
            $settype=Settype::all();
            $body = Subtype::all();
            $excercise = Excercise::all();
            return view('outlet.workout.create', compact('ac', 'body', 'excercise','settype'));
        }
    }

    public function store(Request $request)
    {
        $validator = $request->validate([
            'settype_id' => 'required',

        ], [
            'settype_id.required' => '*Settype is required',

        ]);
        if ($validator == false) {
            return back();
        } else {

            $alldata = $request->workout;
            $settype = $request->settype_id;
            $daysid = implode(',', $request->days);
            $week = $request->week;

            foreach ($alldata as $value) {
                if (!empty($value['bodypart_id'])) {
                    $vendor_id = Auth::guard('outlet')->id();
                    $products = New Workout();
                    $products->outlet_id = $vendor_id;
                    $products->settype_id = $settype;
                    $products->bodypart_id = $value['bodypart_id'];
                    $products->excercise_id = $value['excercise_id'];
                    $products->days_id = $daysid;
                    $products->name  = $request->name;
                    $products->sets = $value['set'];
                    $products->rep = $value['rep'];
                    $products->rest = $value['rest'];
                    $products->week = $week;
                    $products->video_url = $value['video_url'];
                    $file = $value['userimage'];
                    if ($file) {
                        $targets = base_path() . "/resources/assets/images/";
                        $profileimages = $file->getClientOriginalName();
                        $file->move($targets, $profileimages);
                    } else {
                        $profileimages = '';
                    }

                    $file = $value['video'];
                    if ($file) {
                        $targets = base_path() . "/resources/assets/images/";
                        $videos = $file->getClientOriginalName();
                        $file->move($targets, $videos);
                    } else {
                        $videos = '';
                    }
                    $products->video = $videos;
                    $products->image = $profileimages;
                    $products->save();
                }

            }
            return redirect('outlet/workout')->with('Success', 'Sucessfully add');

        }
    }

    public function workoutdetail($id){
        $ac = Addnewmember::where('outlet_id', Auth::guard('outlet')->id())->get();
        $settype=Settype::all();
        $body = Subtype::all();
        $excercise = Excercise::all();
        $bst = Workout::where('id',$id)->first();
        return view('outlet.workout.detail', compact('bst', 'ac', 'body', 'excercise','settype'));
    }

    public function delete($id){
        $del = Workout::where('id',$id)->delete();
        if($del){
            return redirect('outlet/workout')->with('Success', 'Sucessfully deleted');
        }
        return redirect('outlet/workout')->with('Success', 'Error in delete');
    }
}
