<?php

namespace App\Http\Controllers\outlet;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WebcamController extends Controller
{
    //
    public function index(){
        return view('outlet.webcam.index');
    }
    public function create(){
        return view('outlet.webcam.create');
    }
}
