<?php

namespace App\Http\Controllers\outlet;

use App\Invoice;
use App\Packagelist;
use App\Payment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Addnewmember;
use App\Tags;
use App\Salesstaff;
use App\Businesspackage;
use Session;
use Auth;
use App\Addnewoutlet;
class TransactionController extends Controller
{
    //
    public function invoice()
    {
        if (Auth::guard('outlet')->id()) {
            $an = Addnewoutlet::where('id', Auth::guard('outlet')->id())->first();
            $ac = Addnewmember::where('outlet_id', Auth::guard('outlet')->id())->get();
            $tags = Tags::all();
            $sales = Salesstaff::where('outlet_id', Auth::guard('outlet')->id())->get();
            $package = Businesspackage::where('vendor_id',$an->vendor_id)->get();
            return view('outlet.transaction.invoice', compact('ac', 'tags', 'sales', 'package'));
        }
    }

    public function invoicestore(Request $request)
    {

        $validator = $request->validate([
            'membername' => 'required',


        ], [
            'membername.required' => '*Member Name is required',


        ]);
        if ($validator == false) {
            return back();
        } else {

            //echo"helloo";die;
            $vendor_id = Auth::guard('outlet')->id();

            // echo $vendor_id;die;
            $input = $request->all();
            $products = New Invoice();
            $products->outlet_id = $vendor_id;
            $products->invoice = $request->invoice;
            $products->addmember_id = $request->membername;
            $products->sales_id = $request->sales;
            $products->package_id = $request->package_id;


            //  echo $request->images;die;
            //echo $file = $_FILES['image']['tmp_name'];die;

            //  $products->image= $profileimages;


            $code_get_id = Invoice::select('code')->where('outlet_id',$vendor_id)->orderBy('id','desc')->first();
            // echo " $catlast";die;
            $code_id = '';
            if($code_get_id['code'] == ''):
                $code_id = 'INV001';
            else:
                $code_id = "".$code_get_id['code']."";
                $code_id++;
            endif;

            $products->code = $code_id;
            $products->save();
            return redirect('outlet/invoicelist')->with('Success', 'Sucessfully add');
        }
    }

    public function invoicelist()
    {

        if (Auth::guard('outlet')->id() != "") {
            $bn = Invoice::where('outlet_id', Auth::guard('outlet')->id())->get();
            return view('outlet.transaction.invoicelist', compact('bn'));
        }
    }

    public function paymentcollection(){
        $ac=Payment::all();
        $acs=Addnewmember::all();
        $act=Packagelist::all();
        return view('outlet.transaction.paymentcollection',compact('ac','acs','act'));
    }

    public function staffcash()
    {
        if (Auth::guard('outlet')->id() != "") {
            $bst = Salesstaff::where('outlet_id', Auth::guard('outlet')->id())->get();
            return view('outlet.transaction.staffcash', compact('bst'));
        }
    }
}
