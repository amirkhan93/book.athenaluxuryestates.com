<?php

namespace App\Http\Controllers\outlet;

use App\Addnewmember;
use App\Billingcycle;
use App\Businesspackage;
use App\Invoice;
use App\Nutrition;
use App\NutritionMemberMapping;
use App\Packagelist;
use App\Payment;
use App\Paymentdetail;
use App\Salesstaff;
use App\Source;
use App\Tags;
use App\Taxsetting;
use App\Workout;
use App\WorkoutMapping;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Auth;
use Barryvdh\DomPDF\Facade as PDF; 
use App\Addnewoutlet;
use Illuminate\Support\Facades\DB;

class AddnewmemberController extends Controller
{
    //
    public function index()
    {
        if (Auth::guard('outlet')->id()) {

            // $bn = Addnewmember::select('*')
            //                     ->join('businesspackage','addnewmember.package_id','=','businesspackage.id')
            //                     ->where('addnewmember.outlet_id', Auth::guard('outlet')->id())
            //                     ->get();

            $bn = Addnewmember::select(DB::raw('addnewmember.code,addnewmember.id,addnewmember.name,addnewmember.created_at,addnewmember.email,addnewmember.contact,addnewmember.total_earned_points, addnewmember.note,addnewmember.refferal_code,
DATE_ADD(addnewmember.created_at, INTERVAL DATEDIFF(businesspackage.end_date,businesspackage.start_date) day) as pack_ending_date,
DATEDIFF(DATE_ADD(addnewmember.created_at, INTERVAL DATEDIFF(businesspackage.end_date,businesspackage.start_date) day),CURRENT_DATE()) as remaining_days'))
                                ->join('businesspackage','addnewmember.package_id','=','businesspackage.id')
                                ->join('addnewoutlet','addnewoutlet.id','=','addnewmember.outlet_id')
                                ->where('addnewoutlet.id','=',Auth::guard('outlet')->id())
                                ->orderBy('addnewmember.id','DESC')
                                ->get();

            // $bn = DB::select("select am.id from addnewmember am, businesspackage bp where am.package_id = bp.id AND am.outlet_id = '?' GROUP BY am.id",[Auth::guard('outlet')->id()]);
              
              // print_r('<pre>');
              // print_r($bn);
              // die;                  
            return view('outlet.addnewmember.index', compact('bn'));
        }
    }

    public function create()
    {
        if (Auth::guard('outlet')->id()) {
            $an = Addnewoutlet::where('id', Auth::guard('outlet')->id())->first();
            $ac = Source::where('vendor_id',$an->vendor_id)->get();
            $tags = Tags::where('vendor_id',$an->vendor_id)->get();
            return view('outlet.addnewmember.create', compact('ac', 'tags'));
        }
    }

    public function store(Request $request)
    {

        $validator = $request->validate([
            'name' => 'required',
            'email' => 'required|unique:addnewmember',
            'dob' => 'required',
            'contact' => 'required',
            //'memberid' => 'required',


        ], [
            'name.required' => '*Name is required',
            'email.required' => '*Email is required',
            'dob.required' => '*Date of Birth is required',
            'contact.required' => '*Contact is required',
            //'memberid.required' => '*Memberid is required',


        ]);
        if ($validator == false) {
            return back();
        } else {

            $fileName = '';
            $file = $_FILES['profile_pic1'];
           // echo '<pre>';
           // print_r($file);exit;
            $file2 = $_POST['profile_pic'];
            if ($_FILES['profile_pic1']['name'] != "") {
                $targets = base_path() . "/resources/assets/images/";
                $extensions = strrchr($_FILES['profile_pic1']['name'], "."); //extracting the extension
                if ($extensions = ".txt" || $extensions = ".doc" || $extensions = ".docx" || $extensions = ".png" || $extensions = ".gif" || $extensions = ".jpeg" || $extensions = ".pdf") {
                    $fileName = rand() . $_FILES['profile_pic1']['name'];
                    if (move_uploaded_file($_FILES['profile_pic1']['tmp_name'], $targets . $fileName)) {

                    }
                }

            } else if($file2){
                $image_parts = explode(";base64,", $file2);
                $image_type_aux = explode("image/", $image_parts[0]);
                $image_type = $image_type_aux[1];

                $image_base64 = base64_decode($image_parts[1]);
                $fileName = uniqid() . '.png';

                $targets = base_path() . "/resources/assets/images/";
                $file = $targets . $fileName;
                file_put_contents($file, $image_base64);
            }

            //echo"helloo";die;
            $vendor_id = Auth::guard('outlet')->id();
            // echo $vendor_id;die;
            $input = $request->all();
            $products = New Addnewmember();
            $products->outlet_id = $vendor_id;
           // $products->member_id = $request->memberid;
            $products->name = $request->name;
            $products->email = $request->email;
            $products->contact = $request->contact;
            $products->em_contact = $request->em_contact;

            $products->gender = $request->gender;
            $products->dob = $request->dob;
            $products->source_id = $request->sources;
            $products->note = $request->note;
            $products->description = $request->description;
            $products->address = $request->address;
            $products->workout = $request->workoutname;
            $products->tags_id = $request->searchtags;

            $products->profile_pic =$fileName;

            $code_get_id = Addnewmember::select('code')->where('outlet_id',$vendor_id)->orderBy('id','desc')->first();
            // echo " $catlast";die;
            $code_id = '';
            if($code_get_id['code'] == ''):
                $code_id = 'IFM001';
            else:
                $code_id = "".$code_get_id['code']."";
                $code_id++;
            endif;

            $products->code = $code_id;

            $refferal_code = 'REF'.$code_id;
            $products->refferal_code = $refferal_code;

             if(!empty($input['reffered_by'])){

                $products->reffered_by = $input['reffered_by'];

                // get the details of refferer & increase his points by 10-------
                $datavendor = Addnewmember::select('total_earned_points')
                                    ->where('refferal_code',$input['reffered_by'])
                                    ->first();

                $newpoints = $datavendor['total_earned_points'] + 10;

                Addnewmember::where('refferal_code',$input['reffered_by'])
                                     ->update(['total_earned_points'=>$newpoints]);
                //----------------------------------------------------------------

                // give 50 points to new member----------------------------                  
                $products->total_earned_points = 50;
                //---------------------------------------------------------  
            }

            $products->is_gst = $request->is_gst; //applying GST is optional

            $products->save();
            if ($products) {

                return redirect('outlet/invoice/' . $products->id)->with('Success', 'Sucessfully add');
            }
        }
    }

    public function view($id)
    {
        // echo "saddsasd"; die;
        $bn = Addnewmember::where('id', $id)->get(); 

        return view('outlet.addnewmember.view', compact('bn'));
    }

    public function edit($id)
    {

        if (Auth::guard('outlet')->id() != "") {
            $acs=Addnewmember::FindorFail($id);
            $an = Addnewoutlet::where('id', Auth::guard('outlet')->id())->first();
            $ac = Addnewmember::where('outlet_id',Auth::guard('outlet')->id())->get();
            $act = Source::where('vendor_id',$an->vendor_id)->get();
            return view('outlet.addnewmember.edit', compact('ac', 'act','acs'));
        }
        else{
            //echo "gacujgwsjhgwchjwd";
        }
    }

     public function delete($id)
    {
        
        $bn = Addnewmember::where('id', $id)->delete(); 
        return redirect('outlet/addnewmember');
    }

    public function update(Request $request, $id)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {

            $user = Addnewmember::findOrFail($id);
            $user->update($validator);
            return redirect('outlet/addnewmember');
        }
    }

    public function destroy($id)
    {
        $bn = Addnewmember::findOrFail($id);
        $bn->delete();
        return redirect('outlet/addnewmember');
    }

    public function active($id)
    {
        $subCat = Addnewmember::where('id', $id)->first();
        $subCat->status = 1;
        $subCat->save();
        return redirect()->back();
    }

    public function inactive($id)
    {
        $subCat = Addnewmember::where('id', $id)->first();
        $subCat->status = 0;
        $subCat->save();
        return redirect()->back();
    }

    public function invoice($id)
    {
        if (Auth::guard('outlet')->id() != "") {
            $acs = Addnewmember::FindorFail($id);
            $an = Addnewoutlet::where('id', Auth::guard('outlet')->id())->first();
            $tags = Tags::all();
            $sales = Salesstaff::all();
            $package = Businesspackage::where('vendor_id',$an->vendor_id)->get();
            return view('outlet.addnewmember.invoice', compact('id','tags', 'sales', 'package', 'acs'));
        }
    }

    public function invoicestore(Request $request, $id)
    {

        $validator = $request->validate([
            'membername' => 'required',
            'sales'      =>'required',
            'invoice'   =>'required',


        ], [
            'membername.required' => '*Member Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {

            //echo"helloo";die;
            $vendor_id = Auth::guard('outlet')->id();
            $addmember_id = Addnewmember::where('id', $id)->first();
            // echo $vendor_id;die;
            $input = $request->all();
            $products = New Invoice();
            $products->outlet_id = $vendor_id;
            $products->addmember_id = $addmember_id->id;
            $products->invoice = $request->invoice;
            $products->sales_id = $request->sales;
            $products->package_id = $request->package_id;
            $products->tags_id = $request->tags;
            $catlast = Invoice::orderBy('id', 'desc')->first();
            $code = '';
            // echo " $catlast";die;
            if ($catlast) {

                if ($catlast->code) {
                    $exp = explode('INV', $catlast->code);
                    $inc = sprintf('%03d', $exp[1] + 1);
                    $code = 'INV' . $inc;
                } else {
                    $code = 'INV001';
                }
                $post['code'] = $code;
            }

            $products->code = $code;
            $products->save();
            return redirect('outlet/addnewmember')->with('Success', 'Sucessfully add');
        }
    }

    public function packagelist(Request $request)
    {
        $validator = $request->validate([
            'choices' => 'required',


        ], [
            'choices.required' => '*Check one required',
        ]);

        if ($validator == false) {
            return back();
        } else {

            $vendor_id = Auth::guard('outlet')->id();
            $input = $request->all();
            $products = New Packagelist();
            $products->outlet_id = $vendor_id;
            $products->package_id = $request->choices;
            $products->save();

            $assign_package = Addnewmember::where('id',$request->user_id)->
                                            update(['package_id'=>$request->choices]);

            if ($products) {
                return redirect('outlet/packageindexlist/' . $products->id)
                        ->with('Success', 'Sucessfully add');
            }
        }
    }

    public function packageindexlist($id)
    {
        $acs = Packagelist::FindorFail($id);
        $user_data = Addnewmember::select('id','is_gst')->orderBy('id', 'DESC')->first();
        $users = Businesspackage::where('id', $acs->package_id)->first();
        return view('outlet.addnewmember.packagelist', compact('acs', 'users','user_data'))->with('uniq_id',$id);
    }

    public function total($id)
    {
       $taxdetails=Taxsetting::where('id',$id)->first();
        $ac = Payment::all();
        return view('outlet.addnewmember.total', compact('ac','taxdetails'));
    }

    public function paymentdetail(Request $request)
    {
        $validator = $request->validate([
            'payment' => 'required',


        ], [
            'payments.required' => '*Paymentmode is required',


        ]);
        if ($validator == false) {
            return back();
        } else {

            $vendor_id = Auth::guard('outlet')->id();
            $input = $request->all();
            $products = New Paymentdetail();
            $products->outlet_id = $vendor_id;
            $products->paymentmode_id = $request->payment;
            $products->note = $request->note;
            $products->save();
            if ($products) {
                return redirect('outlet/addnewmember/')->with('Success', 'Sucessfully add');
            }
        }
    }

    public function packagestore(Request $request,$id)
    {
        $outlet_id = Auth::guard('outlet')->id();
        $input = $request->all();
        $products = New Taxsetting();
        $products->package_id = $id;
        $products->outlet_id = $outlet_id;
        $products->baseprice = $request->baseprice;
        $products->tax = $request->tax;
        $products->totalamount = $request->totalamount;
        $products->save();
        if ($products) {
            return redirect('outlet/total/'.$products->id)->with('Success', 'Sucessfully add');
        }
    }

    public function assignWorkOut(){
        $memId = $_POST['memId'];
        $workId = $_POST['workId'];
        $workMapp = new WorkoutMapping();
        $workMapp->member_id = $memId;
        $workMapp->workout_id = $workId;
        $result = $workMapp->save();
        if($result){
            echo 1;
        }else{
            echo 0;
        }
    }

    public function getassignWorkOut(){
        $memId = $_POST['memId'];
        $bst = Workout::where('outlet_id', Auth::guard('outlet')->id())->get();
        return view('outlet.addnewmember.assign', compact('bst', 'memId'));
    }

    public function assignNutrition(){
        $memId = $_POST['memId'];
        $workId = $_POST['workId'];
        $workMapp = new NutritionMemberMapping();
        $workMapp->member_id = $memId;
        $workMapp->nutrition_id = $workId;
        $result = $workMapp->save();
        if($result){
            echo 1;
        }else{
            echo 0;
        }
    }

    public function getassignNutrition(){
        $memId = $_POST['memId'];
        $bst = Nutrition::where('outlet_id', Auth::guard('outlet')->id())->get();
        return view('outlet.addnewmember.assign-nutrition', compact('bst', 'memId'));
    }

    public function generateInvoice($id){
        $acs = Packagelist::FindorFail($id);
        $user_data = Addnewmember::orderBy('id', 'DESC')->first();
        $users = Businesspackage::where('id', $acs->package_id)->first();
        $pdf = PDF::loadView('outlet.invoice.generateInvoice', compact('acs', 'user_data','users'));
        return $pdf->download('invoice.pdf');

        //return view('outlet.invoice.generateInvoice',compact('acs', 'user_data','users'));
    }

    //  public function invoice($id){
    //     $orders=Order::where('id',$id)->first();
    //     $customer=Customeraddress::where('id',$orders->address_id)->first();
    //     $pdf = PDF::loadView('admin.order.invoice', compact('order','customer'));
    //     return $pdf->download('invoice.pdf');
    // }


}

