<?php

namespace App\Http\Controllers\outlet;

use Illuminate\Http\Request;
use App\Nutrition;
use App\NutritionFoodMapping;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Session;
use Auth;
class NutritionController extends Controller
{
    public function index(){
        $results = Nutrition::orderBy('id', 'DESC')->get();
        return view('outlet.nutrition.index',compact('results'));
    }

    public function create(){
        return view('outlet.nutrition.create');
    }

    public function store(Request $request){

          $validator = $request->validate([
            'nutrition' => 'required',
            // 'nutrition[0][meal_time]' => 'required',
            // 'nutrition[0][nutrifood][]' => 'required',
            'week' => 'required',
            'days' => 'required',
        ], [
            'nutrition.required' => '*Meal Type is required',
            // 'nutrition[0][meal_time].required' => '*Meal Time is required',
            // 'nutrition[0][nutrifood][].required' => '*Food is required',
            'week.required' => '*Week is required',
            'days.required' => '*Days required',
           
        ]);

        if ($validator == false) {
            return back();
        } else {

        $postDtaas = $request->nutrition;
        $week = $request->week;
        $daysid = implode(',', $request->days);
        if($postDtaas){

            foreach($postDtaas as $postDtaa){
                $nutri = new Nutrition();
                $nutri->outlet_id = Auth::guard('outlet')->id();
                $nutri->meal_type = $postDtaa['meal_type'];
                $nutri->meal_time = $postDtaa['meal_time'];
                $nutri->week = $week;
                $nutri->days_id = $daysid;
                $save = $nutri->save();
                if($save){
                    foreach($postDtaa['nutrifood'] as $food){
                        $nutrifood = new NutritionFoodMapping();
                        $nutrifood->nutrition_id = $nutri->id;
                        $nutrifood->food_name = $food;
                        $result = $nutrifood->save();
                    }

                }
            }

        }

        if($result){
            return redirect('outlet/nutrition')->with('Success', 'Sucessfully add');
        }else{
            return redirect('outlet/nutrition')->with('danger', 'Error in add');
        }

    }
    }

    public function getnutritionfood(){
        $id = $_POST['id'];
        $results = NutritionFoodMapping::where('nutrition_id',$id)->get();
        return view('outlet.nutrition.detail',compact('results'));
    }

    public function deleteNutrifood(){
        $id = $_POST['id'];
        $results = NutritionFoodMapping::where('id',$id)->delete();
        if($results){
            echo 1;
        }else{
            echo 0;
        }
    }

    public function delete($id){
        $del = Nutrition::where('id',$id)->delete();
        if($del){
            return redirect('outlet/nutrition')->with('Success', 'Sucessfully deleted');
        }
        return redirect('outlet/nutrition')->with('Success', 'Error in delete');
    }
}
