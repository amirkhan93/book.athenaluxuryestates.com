<?php

namespace App\Http\Controllers\outlet;

use App\Addnewlead;
use App\Addnewoutlet;
use App\Businesspackage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use Session;
use Auth;

class SetupController extends Controller
{
    //
    public function package()
    {
        if (Auth::guard('outlet')->id()) {
            $an=Addnewoutlet::where('id',Auth::guard('outlet')->id())->first();
            $bn = Businesspackage::where('vendor_id',$an->vendor_id)->get();
            return view('outlet.setup.package', compact('bn'));
        }
    }
    public function taxdetails(){
        return view('outlet.setup.tax');
    }
    public function leadstatus(){

        if (Auth::guard('outlet')->id() != "") {
        $bn=Addnewlead::where('outlet_id',Auth::guard('outlet')->id())->get();
       // print_r($bn);die;
        return view('outlet.setup.leadstatus',compact('bn'));
    }
    }
    public function roles()
    {
        if (Auth::guard('outlet')->id() != "") {
            $an = Addnewoutlet::where('id', Auth::guard('outlet')->id())->first();
            $bn = Role::where('vendor_id', $an->vendor_id)->get();
            return view('outlet.setup.roles', compact('bn'));
        }
    }
}
