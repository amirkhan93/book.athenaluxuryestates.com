<?php

namespace App\Http\Controllers\ifitmash;

use App\Addnewoutlet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Auth;
class ApprovaloutletController extends Controller
{
    //
    public function index()
    {
        if (Auth::guard('vendor')->id()) {
            $bn = Addnewoutlet::where('vendor_id',Session::get('vendor_id'))->get();
            return view('ifitmash.outletapproval.index', compact('bn'));
        }
    }
    public function active($id)
    {
        $subCat =  Addnewoutlet::where('id', $id)->first();
        $subCat->status = 1;
        $subCat->save();
        return redirect()->back();
    }

    public function inactive($id)
    {
        //echo 'd';exit;
        $subCat =  Addnewoutlet::where('id', $id)->first();
        $subCat->status = 0;
        $subCat->save();
        return redirect()->back();
    }
}
