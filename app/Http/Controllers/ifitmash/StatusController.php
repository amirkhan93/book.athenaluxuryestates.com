<?php

namespace App\Http\Controllers\ifitmash;

use App\Businesspackage;
use App\Status;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Auth;

class StatusController extends Controller
{
    //
    public function index()
    {
        if (Auth::guard('vendor')->id()) {
            $bst = Status::where('vendor_id', Auth::guard('vendor')->id())->get();
            return view('ifitmash.status.index', compact('bst'));
        }
    }

    public function create()
    {
        return view('ifitmash.status.create');
    }

    public function store(Request $request)
    {
        {
            $validator = $request->validate([
                'name' => 'required',
            ], [
                'name.required' => 'Name is required',
            ]);
            if ($validator == false) {
                return back();
            } else {
                $vendor_id = Auth::guard('vendor')->id();
                $input = $request->all();
                $products = New Status();
                $products->vendor_id = $vendor_id;
                $products->name = $request->name;
                $code_get_id = Status::select('code')->where('vendor_id',$vendor_id)->orderBy('id','desc')->first();
                // echo " $catlast";die;
                $code_id = '';
                if($code_get_id['code'] == ''):
                    $code_id = 'SAT001';
                else:
                    $code_id = "".$code_get_id['code']."";
                    $code_id++;
                endif;

                $products->code = $code_id;
                $products->save();
                return redirect('member/status')->with('Success', 'Sucessfully add');
            }
        }
    }
    public function edit($id){
        $ac = Status::findOrFail($id);
        return view('ifitmash.status.edit',compact('ac'));
    }
    public function update(Request $request,$id)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {

            $user = Status::findOrFail($id);
            $user->update($validator);
            return redirect('member/status');
        }
    }
    public function destroy($id)
    {
        $acs = Status::findOrFail($id);
        $acs->delete();

        return redirect('member/status');
    }
    public function active($id)
    {
        $subCat =  Status::where('id', $id)->first();
        $subCat->status = 1;
        $subCat->save();
        return redirect()->back();
    }

    public function inactive($id)
    {
        //echo 'd';exit;
        $subCat =  Status::where('id', $id)->first();
        $subCat->status = 0;
        $subCat->save();
        return redirect()->back();
    }

}