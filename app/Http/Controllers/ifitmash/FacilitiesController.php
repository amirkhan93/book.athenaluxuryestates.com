<?php

namespace App\Http\Controllers\ifitmash;

use App\Facilities;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Auth;

class FacilitiesController extends Controller
{
    //
    public function index()
    {
        if (Auth::guard('vendor')->id()) {
            $bst = Facilities::where('vendor_id', Auth::guard('vendor')->id())->get();
            return view('ifitmash.facilities.index', compact('bst'));
        }
    }

    public function create()
    {
        return view('ifitmash.facilities.create');
    }

    public function store(Request $request)
    {
        {
            $validator = $request->validate([
                'name' => 'required',
            ], [
                'name.required' => 'Name is required',
            ]);
            if ($validator == false) {
                return back();
            } else {
                $vendor_id = Auth::guard('vendor')->id();
                $input = $request->all();
                $products = New     Facilities();
                $products->vendor_id = $vendor_id;
                $products->name = $request->name;
                $catlast = Facilities::orderBy('id', 'desc')->first();
                $code_get_id = Facilities::select('code')->where('vendor_id',$vendor_id)->orderBy('id','desc')->first();
                // echo " $catlast";die;
                $code_id = '';
                if($code_get_id['code'] == ''):
                    $code_id = 'FAC001';
                else:
                    $code_id = "".$code_get_id['code']."";
                    $code_id++;
                endif;

                $products->code = $code_id;
                $products->save();
                return redirect('member/facilities')->with('Success', 'Sucessfully add');
            }
        }
    }
    public function edit($id){
        $ac = Facilities::findOrFail($id);
        return view('ifitmash.facilities.edit',compact('ac'));
    }
    public function update(Request $request,$id)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {

            $user = Facilities::findOrFail($id);
            $user->update($validator);
            return redirect('member/facilities');
        }
    }
    public function destroy($id)
    {
        $acs = Facilities::findOrFail($id);
        $acs->delete();

        return redirect('member/facilities');
    }
    public function active($id)
    {
        $subCat =  Facilities::where('id', $id)->first();
        $subCat->status = 1;
        $subCat->save();
        return redirect()->back();
    }

    public function inactive($id)
    {
        //echo 'd';exit;
        $subCat =  Facilities::where('id', $id)->first();
        $subCat->status = 0;
        $subCat->save();
        return redirect()->back();
    }
}

