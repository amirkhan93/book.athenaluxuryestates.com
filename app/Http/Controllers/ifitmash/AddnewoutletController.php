<?php

namespace App\Http\Controllers\ifitmash;

use App\Accounttype;
use App\Addnewmember; 
use App\Addnewoutlet;
use App\Bankname;
use App\Billingcycle;
use App\Facilities;
use App\Payment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use  App\Cities;
use  App\Counteries;
use App\States;
use Session;
use Auth; 
use Illuminate\Support\Facades\Mail;

class AddnewoutletController extends Controller
{
    //

    public function mail()
    {
        $data = array('name'=>"Our Code World");
        // Path or name to the blade template to be rendered
        $template_path = 'email_template';

        Mail::send(['text'=> $template_path ], $data, function($message) {
            // Set the receiver and subject of the mail.
            $message->to('amitgarg@stackmindz.com', 'Receiver Name')->subject('Laravel First Mail');
            // Set the sender
            $message->from('amitgarg@stackmindz.com','Our Code World');
        });

        return "Basic email sent, check your inbox.";
    }

    public function index()
    {
        if(Auth::guard('vendor')->id()) {
            $bn = Addnewoutlet::where('vendor_id',Auth::guard('vendor')->id())->get();
            return view('ifitmash.addnewoutlet.index', compact('bn'));
        }

    }
 
    public function create()
    {
        if (Auth::guard('vendor')->id() != "") {
            $ac = Facilities::where('vendor_id',Auth::guard('vendor')->id())->get();
            $countries = Counteries::all();
            $paymentmode = Payment::where('vendor_id',Auth::guard('vendor')->id())->get();
            $accounttype = Accounttype::where('vendor_id',Auth::guard('vendor')->id())->get();;
            $bankname = Bankname::where('vendor_id',Auth::guard('vendor')->id())->get();
            $billingcycle = Billingcycle::where('vendor_id',Auth::guard('vendor')->id())->get();
            $states = States::where('country_id', '101')->get();

            return view('ifitmash.addnewoutlet.create', compact('countries', 'ac', 'paymentmode', 'accounttype', 'bankname', 'billingcycle','states'));

        } 
    }

    public function store(Request $request)
    {
        $validator = $request->validate([
            'name' => 'required',
            'email' => 'required|unique:addnewoutlet',
            'address1' => 'required',
            //'address2' => 'required',
            //'smsspoc' => 'required',
            'contact' => 'required',
            //'address' => 'required',
            //'billingcycle_id' => 'required',
            'payeename' => 'required',
            'bankname_id' =>'required',
            //'paymentmode_id' =>'required',
            'accounttype_id'  =>'required',
            'grace_period'  =>'required',


        ], [
            'name.required' => '*Name is required',
            'email.required' => '*Email is required',
            'address1.required' => '*Street Address is required',
            //'address2.required' => '*Street Address is required',
            //'smsspoc.required' => '*Sms Spoc is required',
            'contact.required' => '*Contact is required',
            //'address.required' => '*Billing Address is required',
            //'billingcycle_id.required' => '*Billing Cycle is required',
            'payeename.required' => '*Payment Name is required',
            'bankname_id.required' =>'*BankName is required',
            //'paymentmode_id.required' =>'*PaymentMode is required',
            'accounttype_id.required'  =>'*Accounttype is required',
            'grace_period.required'  =>'*Grace Period is required',


        ]);
        if ($validator == false) {
            return back();
        } else {
            
            $vendor_id = Auth::guard('vendor')->id();
            $facility = $request->input('facility_id');
            if(count($facility) > 0){
                $facility = implode(',', $facility);
            }
            else{
                $facility = '';
            }
            
            if(!empty($request->file('outletpick')))
            {
            $images=array();
                        if($files=$request->file('outletpick')){
                            foreach($files as $file){
                                //$targets = base_path() . "/resources/assets/images/";
                                $name=$file->getClientOriginalName();
                                $file->move(base_path('\resources\assets\images'),$name);
                                $images[]=$name;
                            }
                        } 
             }
 
             $outletpick = implode(",", $images);
                
                if(!empty($request->file('managerpick')))
                {
             $file = $request->file('managerpick');
                $manegerpick = $file->getClientOriginalName();
                $movefile = $file->move(base_path('\resources\assets\images\userimages'), $file->getClientOriginalName());
            }
          
            $products = New Addnewoutlet();
            $products->vendor_id = $vendor_id;
            $products->outlet_name = $request->name;
            $products->outlet_manager_photo = $manegerpick;
            $products->outlet_manager_name  = $request->managername;
            $products->outlet_photos = $outletpick;
            $products->street_address1 = $request->address1;
           // $products->street_address2 = $request->address2;
            $products->email = $request->email;
            $products->contact = $request->contact;
            $products->secondary_contact = $request->contact2;
            $products->state_id = $request->state_id;
            $products->country_id = $request->country_id; 
            $products->city_id = $request->cities_id;
            //$products->name_spoc = $request->namespoc;
            //$products->smsspoc = $request->smsspoc;
            $products->facility_id=$facility;
            //$products->billing_name = $request->billingname;
            //$products->billing_address = $request->address;
            $products->po_number = $request->ponumber;
            //$products->pricecommited = $request->pricecommited;
            //$products->classcommited = $request->classcommited;
            $products->pincode = $request->pincode;
            //$products->choices = $request->choices;
            //$products->url = $request->url;
            //$products->landmark = $request->landmark;
            $products->googlemaps = $request->map;
            $products->password = bcrypt($request->password);
            //$products->summary = $request->note;
            $products->payeename = $request->payeename;
            $products->accountno = $request->accountno;
            $products->ifsccode = $request->ifsccode;
            $products->bankname_id = $request->bankname_id;
            $products->accounttype_id = $request->accounttype_id;
            $products->paymentmode_id = $request->paymentmode_id;
            //$products->billingcycle_id = $request->billingcycle_id;
            $products->grace_period = $request->grace_period;

            $code_get_id = Addnewoutlet::select('code')->where('vendor_id',$vendor_id)->orderBy('id','desc')->first();
            // echo " $catlast";die;
            $code_id = '';
            if($code_get_id['code'] == ''):
                $code_id = 'ADN001';
            else:
                $code_id = "".$code_get_id['code']."";
                $code_id++;
            endif;

            $products->code = $code_id;

            $products->save();
            return redirect('member/addnewoutlet')->with('Success', 'Sucessfully add');
        }

    } 
    public function edit($id)
    { 
        $ac = Facilities::where('vendor_id',Auth::guard('vendor')->id())->get();
        $pt = PackageType::where('vendor_id',Auth::guard('vendor')->id())->get();
        $countries = Counteries::all();
        $paymentmode=Payment::where('vendor_id',Auth::guard('vendor')->id())->get();
        $accounttype=Accounttype::where('vendor_id',Auth::guard('vendor')->id())->get();
        $bankname=Bankname::where('vendor_id',Auth::guard('vendor')->id())->get();
        $billingcycle=Billingcycle::all();
        $states = States::where('country_id', '101')->get();

        $act=Addnewoutlet::FindorFail($id);
        return view('ifitmash.addnewoutlet.edit',compact('ac','countries','act','paymentmode','accounttype','bankname','billingcycle','states','pt'));
    }
    public function destroy($id)
    {
        $bn = Addnewoutlet::findOrFail($id);
        $bn->delete();
        return redirect('member/addnewoutlet');
    }
    public function active($id)
    {
        $subCat = Addnewoutlet::where('id', $id)->first();
        $subCat->status = 1;
        $subCat->save();
        return redirect()->back();
    }

    public function inactive($id)
    {
        $subCat =  Addnewoutlet::where('id', $id)->first();
        $subCat->status = 0;
        $subCat->save();
        return redirect()->back();
    }

    // public function states()
    // {
    //     $id = ltrim($_POST['id'], ',');
    //     $states = States::whereIn('country_id', '101')->get();
    //     if ($states) {
    //         $html = '<option value="">Please Select</option>';
    //         foreach ($states as $state) {
    //             $html .= '<option value="' . $state->id . '">' . ucfirst($state->name) . '</option>';
    //         }
    //     } else {
    //         $html = '<option value="">No State</option>';
    //     }
    //     echo $html;
    // }


    public function cities()
    {
        $id = ltrim($_POST['id'], ',');
        //print_r($id);die();
        $cities = Cities::whereIn('state_id', explode(',', $id))->get();
        //print_r($cities);die();
        if ($cities) {
            $html = '<option value="">Please Select</option>';
            foreach ($cities as $city) {
                $html .= '<option value="' . $city->id . '">' . ucfirst($city->name) . '</option>';
            }
        } else {
            $html = '<option value="">No State</option>';
        }
        echo $html; 
    } 

    public function view($id)
    {
        $bn=Addnewoutlet::where('id', $id)->get();
       return view('ifitmash.addnewoutlet.view',compact('bn'));
    }
    public function update(Request $request,$id)
    {
        
        $products = Addnewoutlet::findOrFail($id);
        //print_r($request->all()); die();
        $facility = $request->input('facility_id');
            if(count($facility) > 0){
                $facility = implode(',', $facility);
            }
            else{
                $facility = '';
            }
     if(!empty($request->file('outletpick')))
            {
            $images=array();
                        if($files=$request->file('outletpick')){
                            foreach($files as $file){
                                //$targets = base_path() . "/resources/assets/images/";
                                $name=$file->getClientOriginalName();
                                $file->move(base_path('\resources\assets\images'),$name);
                                $images[]=$name;
                            }
                        } 
                $outletpick = implode(",", $images);
             }
             else{

                $outletpick = $request->old_outlet_photo;
             }
 
             
                
                if(!empty($request->file('managerpick')))
                {
             $file = $request->file('managerpick');
                $manegerpick = $file->getClientOriginalName();
                $movefile = $file->move(base_path('\resources\assets\images\userimages'), $file->getClientOriginalName());
            }
            else{ 
                $manegerpick = $request->old_manager_photo;
            }


            $products->outlet_name = $request->name;
            $products->outlet_manager_photo = $manegerpick;
            $products->outlet_manager_name  = $request->managername;
            $products->outlet_photos = $outletpick;
            $products->street_address1 = $request->address1;
           
            $products->email = $request->email;
            $products->contact = $request->contact;
            $products->secondary_contact = $request->contact2;
            $products->state_id = $request->state_id;
            $products->country_id = $request->country_id;
            $products->city_id = $request->cities_id;
            
           // $products->facility_id=$facility;
            
            $products->po_number = $request->ponumber;
            
            $products->pincode = $request->pincode;
           
            $products->googlemaps = $request->map;
           
            $products->payeename = $request->payeename;
            $products->accountno = $request->accountno;
            $products->ifsccode = $request->ifsccode;
            $products->bankname_id = $request->bankname_id;
            $products->accounttype_id = $request->accounttype_id;
            $products->paymentmode_id = $request->paymentmode_id;
           
            $products->grace_period = $request->grace_period;

            
            $products->save();
            return redirect('member/addnewoutlet');
        
    }

}


