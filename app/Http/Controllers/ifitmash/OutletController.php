<?php

namespace App\Http\Controllers\ifitmash;

use App\Invoice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Illuminate\Support\Facades\DB;
use Auth;
use App\Addnewlead;
use App\Addnewmember;
use App\Packagelist;
use App\Businesspackage;
use App\Addnewoutlet;

class OutletController extends Controller
{
    //
    public function index()
    {
        if (Auth::guard('outlet')->id()) {
            return redirect('outlet/dashboard');

        }
        return view('ifitmash.outlet.login');
    }

    public function login(Request $request)
    {
        $email = $request->email;
        $pass = $request->password;
        if(Auth::guard('outlet')->attempt(['email' =>$email, 'password' => $pass]))
        {
            return redirect()->intended('outlet/dashboard'); // Redirect to dashboard page
        }

        else
        {
            $status = "Sorry! your credentials are not matching";
            return redirect('outlet')->with('status', $status);
        }

    }
    public function dashboard()
    {
        if (Auth::guard('outlet')->id()) {

            $sessId=Auth::guard('outlet')->id(); 
            //$member = Packagelist::select('')->where('outlet_id', $sessId)->get();
            $today = date('Y-m-d');
           // $lead = Addnewlead::where('outlet_id',$sessId)->get();
            $lead = Addnewlead::where('outlet_id',$sessId)->where('created_at', 'like',$today .'%')->get();
            $leadfollow = Addnewlead::where('outlet_id',$sessId)->where('followup', 'like',$today .'%')
            ->get();
            $lostlead = Addnewlead::where('status','=',0)->get();
            $member = Addnewmember::where('outlet_id', $sessId)->get();
            $memberBdy = Addnewmember::where('dob',date('Y-m-d'))->get();
            $inactivemember=Addnewmember::where('status','=',0)->where('outlet_id', $sessId)->get();
            $activemember=Addnewmember::where('status','=',1)->where('outlet_id',$sessId)->get();
            $invoice=Invoice::where('outlet_id',$sessId)->get();

            
            $renewal_due = Addnewmember::select(DB::raw('COUNT(addnewmember.id) as renew'))
                                ->join('businesspackage','addnewmember.package_id','=','businesspackage.id')
                                ->join('addnewoutlet','addnewoutlet.id','=','addnewmember.outlet_id')
                                ->where('addnewoutlet.id','=',Auth::guard('outlet')->id())
                                ->whereRaw('DATEDIFF(DATE_ADD(addnewmember.created_at, INTERVAL DATEDIFF(businesspackage.end_date,businesspackage.start_date) day),CURRENT_DATE()) < ?',[5])
                                ->get();
            // DB::enableQueryLog();
            $expired_membership = Addnewmember::
                                select(DB::raw('COUNT(addnewmember.id) as expired_membership'))
                                ->join('businesspackage','addnewmember.package_id','=','businesspackage.id')
                                ->join('addnewoutlet','addnewoutlet.id','=','addnewmember.outlet_id')
                                ->where('addnewoutlet.id','=',Auth::guard('outlet')->id())
                                ->whereRaw('DATEDIFF(DATE_ADD(addnewmember.created_at, INTERVAL DATEDIFF(businesspackage.end_date,businesspackage.start_date) day),CURRENT_DATE()) < ?',[0])
                                ->get();

            
            // $laQuery = DB::getQueryLog(); 
            // print_r($laQuery);die;

            $renewal_due = $renewal_due[0]->renew;
            $expired_membership = $expired_membership[0]->expired_membership; 

            return view('ifitmash.outlet.dashboard',compact('lead','member','inactivemember','invoice','lostlead','activemember','memberBdy','renewal_due','leadfollow','expired_membership'));
        }else{
           return redirect('outlet');
       }
    }

    public function renewal_due(){

      //  DB::enableQueryLog();

        $renewal_due = Addnewmember::select(DB::raw('addnewmember.name,addnewmember.created_at,addnewmember.email,addnewmember.contact,
DATE_ADD(addnewmember.created_at, INTERVAL DATEDIFF(businesspackage.end_date,businesspackage.start_date) day) as pack_ending_date,
DATE_ADD(DATE_ADD(addnewmember.created_at, INTERVAL DATEDIFF(businesspackage.end_date,businesspackage.start_date) day) , INTERVAL addnewoutlet.grace_period  day ) as with_grace, addnewoutlet.grace_period'))
                                ->join('businesspackage','addnewmember.package_id','=','businesspackage.id')
                                ->join('addnewoutlet','addnewoutlet.id','=','addnewmember.outlet_id')
                                ->where('addnewoutlet.id','=',Auth::guard('outlet')->id())
                                ->whereRaw('DATEDIFF(DATE_ADD(addnewmember.created_at, INTERVAL DATEDIFF(businesspackage.end_date,businesspackage.start_date) day),CURRENT_DATE()) < ?',[5])
                                ->get();

        // $laQuery = DB::getQueryLog();
        //$grace_period = $grace_period[0]->grace_period;
        // print_r('<pre>');
        // print_r($renewal_due);die;

        return view('ifitmash.outlet.renewal_due',compact('renewal_due','grace_period'));
    }

    public function members_followup(){
 
        $member = Addnewmember::where('outlet_id', Auth::guard('outlet')->id())->get();
        return view('ifitmash.outlet.members_followup',compact('member'));
    }

    public function members_bdy(){
 
       $memberBdy = Addnewmember::where('dob',date('Y-m-d'))->get();
        return view('ifitmash.outlet.members_bdy',compact('memberBdy'));
    }

     public function inactivemember(){
 
        $inactivem=Addnewmember::where('status','=',0)->where('outlet_id', Auth::guard('outlet')->id())
                                        ->get();
        return view('ifitmash.outlet.inactivemember',compact('inactivem'));
    }

     public function invoice(){
 
       $invoice=Invoice::select('*')
                        ->join('addnewmember','addnewmember.id','=','invoice.addmember_id')
                        ->where('invoice.outlet_id',Auth::guard('outlet')->id())->get();


       // print_r('<pre>');
       // print_r($invoice);
       // die;
        return view('ifitmash.outlet.invoice',compact('invoice'));
    }

     public function today_lead(){
        $today = date('Y-m-d');
       $today_lead = Addnewlead::where('outlet_id',Auth::guard('outlet')->id())
                               ->where('created_at', 'like',$today .'%')
                               ->get();
       // print_r('<pre>');
       // print_r($invoice);
       // die;
        return view('ifitmash.outlet.today_lead',compact('today_lead'));
    }

     public function followup_lead(){
        
        $today = date('Y-m-d');

        $leadfollow = Addnewlead::where('outlet_id',Auth::guard('outlet')->id())
                                ->where('followup', 'like',$today .'%')
                                ->get();


       // print_r('<pre>');
       // print_r($invoice);
       // die;
        return view('ifitmash.outlet.followup_lead',compact('leadfollow'));
    }

     public function expired_membership(){
        
       // $today = date('Y-m-d');

        $expired_membership = Addnewmember::
                                select(DB::raw('*,DATE_ADD(addnewmember.created_at, INTERVAL DATEDIFF(businesspackage.end_date,businesspackage.start_date) day) as expiry_date'))
                                ->join('businesspackage','addnewmember.package_id','=','businesspackage.id')
                                ->join('addnewoutlet','addnewoutlet.id','=','addnewmember.outlet_id')
                                ->where('addnewoutlet.id','=',Auth::guard('outlet')->id())
                                ->whereRaw('DATEDIFF(DATE_ADD(addnewmember.created_at, INTERVAL DATEDIFF(businesspackage.end_date,businesspackage.start_date) day),CURRENT_DATE()) < ?',[0])
                                ->get();

       // print_r('<pre>');
       // print_r($expired_membership);
       // die;
        return view('ifitmash.outlet.expired_membership',compact('expired_membership'));
    }


















    public function logout(Request $request){
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect('outlet');
    }
	
	public function change_id(Request $request){
        $request->session()->put('outlet_id',$_GET['value']);
	}


}
