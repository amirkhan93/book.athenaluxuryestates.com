<?php

namespace App\Http\Controllers\ifitmash;

use App\Addnewoutlet;
use App\Role;
use App\Vendorusers;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Auth;

class UserController extends Controller
{
    //
    public function index()
    {
        if (Auth::guard('vendor')->id()) {
            $bst=Vendorusers::where('vendor_id',Auth::guard('vendor')->id())->get();
            return view('ifitmash.users.view',compact('bst'));
        }
    }
    public function create()
    {
        if (Auth::guard('vendor')->id()) {
            $ac = Role::where('vendor_id',Auth::guard('vendor')->id())->get();
            $outlet = Addnewoutlet::where('vendor_id',Auth::guard('vendor')->id())->get();
            //print_r($outlet);die;
            return view('ifitmash.users.add', compact('ac', 'outlet'));
        }
    }

    public function store(Request $request){
        $validator = $request->validate([
            'name' => 'required',
            'email'=>'required |unique:vendor_users'
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {
           $vendor_id = Auth::guard('vendor')->id();
            //  echo $vendor_id;die;exit;
            $input = $request->all();
            $products = New Vendorusers();
            $products->vendor_id = $vendor_id;
            $products->username = $request->name;
            $products->email = $request->email;
            $products->password = $request->password;
            $products->roles_id = $request->roles;
            $outlet = $request->input('outlet');
            $outlets = implode(',', $outlet);
            $products->outlet_id=$outlets;

            $code_get_id = Vendorusers::select('code')->where('vendor_id',$vendor_id)->orderBy('id','desc')->first();
            // echo " $catlast";die;
            $code_id = '';
            if($code_get_id['code'] == ''):
                $code_id = 'USR001';
            else:
                $code_id = "".$code_get_id['code']."";
                $code_id++;
            endif;
            $products->code = $code_id;
            $products->save();
            return redirect('member/users/list')->with('Success', 'Sucessfully add');
        }
    }
    public function destroy($id)
    {
        $acs = Vendorusers::findOrFail($id);
        $acs->delete();

        return redirect('member/users/list');
    }
    public function active($id)
    {
        $subCat =  Vendorusers::where('id', $id)->first();
        $subCat->status = 1;
        $subCat->save();
        return redirect()->back();
    }

    public function inactive($id)
    {
        //echo 'd';exit;
        $subCat =  Vendorusers::where('id', $id)->first();
        $subCat->status = 0;
        $subCat->save();
        return redirect()->back();
    }
    public function edit($id){
        $acs = Vendorusers::findOrFail($id);
        $ac=Role::all();
        $outlet=Addnewoutlet::all();
        return view('ifitmash.users.edit',compact('acs','ac','outlet'));
    }
    public function update(Request $request,$id)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {

            $user = Vendorusers::findOrFail($id);
            $user->update($validator);
            return redirect('member/users/list');
        }
    }
}
