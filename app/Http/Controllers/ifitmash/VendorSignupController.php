<?php

namespace App\Http\Controllers\ifitmash;

use App\Addnewoutlet;
use App\Businesspackage;
use App\Businesstype;
use App\Vendor;
use Auth;
use Session;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VendorSignupController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('vendor');
    }

    public function index()
    {
       // $plan = Businesspackage::all();
        $ac = Businesstype::all();
        return view('admin.signup.signup',compact('ac'));
    }

    public function registervendor(Request $request)
    {
        $validator = $request->validate([
            'name' => 'required',
            'email' => 'required|unique:vendors',
            'businessname' => 'required',
            'password' => 'required',
            'businesstype' => 'required',


        ], [
            'name.required' => '*Name is required',
            'email.required' => '*Email is required',
            'businessname.required' => '*BusinessName is required',
            'businesstype.required' => '*BusinessType is required',
            'password.required' => '*Password is required',

        ]);
        if ($validator == false) {
            return back();
        } else {

            $input = $request->all();
            $products = new Vendor();
            $products->name = $input['name'];
            $products->business_name = $input['businessname'];
            $products->businesstype_id = $input['businesstype'];
            $products->email = $input['email'];
            $products->password = bcrypt($input['password']);
            $catlast = Vendor::orderBy('id', 'desc')->first();
            //echo " $catlast";die;
            if ($catlast) {

                if ($catlast->code) {
                    $exp = explode('FIT', $catlast->code);
                    $inc = sprintf('%03d', $exp[1] + 1);
                    $code = 'FIT' . $inc;
                } else {
                    $code = 'FIT001';
                }

            } else {
                $code = 'FIT001';
            }
            //$post['code'] = $code;

            $products->code = $code;
            $save = $products->save();
            if ($save) {

                $status="Succesfully Registered";
                //return redirect($this->redirectTo)->with('success', 'Vendor Signup successfully Registered!');
                return redirect('member')->withMessage('status',$status);
            } else {
                return redirect()->back()->withErrors('Error in registeration');
            }
        }
    }
    public function dashboard()
    {
//        if (Auth::guard('vendor')->id()) {
////            $outlets = Addnewoutlet::where('vendor_id',Auth::guard('vendor')->id())->get();
////            return view('admin.signup.dashboard', compact('outlets'));
////        }else{
////            return redirect('member');
////        }
        $outlets = Addnewoutlet::where('vendor_id',Auth::guard('vendor')->id())->get();
        return view('admin.signup.dashboard', compact('outlets'));
    }

    public function loginform()
    {
//        if (Auth::guard('vendor')->id()) {
//            return redirect('member/dashboard');
//        }
        return view('admin.signup.login');
    }

    public function login(Request $request)
    {
        $email = $request->email;
        $pass = $request->password;
        if(Auth::guard('vendor')->attempt(['email' =>$email, 'password' => $pass]))
        {
            return redirect()->intended('member/dashboard'); // Redirect to dashboard page
        }

        else
        {
            $status = "Sorry! your credentials are not matching";
            return redirect('member')->with('status', $status);
        }

    }
    public function logout(Request $request)
    {
        Auth::guard('vendor')->logout();
        $request->session()->flush();
        $request->session()->regenerate();
        return redirect('member');
    }

}
