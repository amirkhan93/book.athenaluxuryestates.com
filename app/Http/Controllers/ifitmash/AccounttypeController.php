<?php

namespace App\Http\Controllers\ifitmash;

use App\Accounttype;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Auth;

class AccounttypeController extends Controller
{
    //
    public function index()
    {
        if (Auth::guard('vendor')->id()) {
            $bst = Accounttype::where('vendor_id', Auth::guard('vendor')->id())->get();
            return view('ifitmash.accounttype.index', compact('bst'));
        }
    }
    public function create()
    {
        return view('ifitmash.accounttype.create');
    }
    public function store(Request $request)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {
            $vendor_id = Auth::guard('vendor')->id();
          //  echo $vendor_id;die;exit;
            $input = $request->all();
            $products = New Accounttype();
            $products->vendor_id = $vendor_id;
            $products->name = $request->name;
            $code_get_id = Accounttype::select('code')->where('vendor_id',$vendor_id)->orderBy('id','desc')->first();
            // echo " $catlast";die;
            $code_id = '';
            if($code_get_id['code'] == ''):
                $code_id = 'ACC001';
            else:
                $code_id = "".$code_get_id['code']."";
                $code_id++;
            endif;
            $products->code = $code_id;
            $products->save();
            return redirect('member/accounttype')->with('Success', 'Sucessfully add');
        }
    }
    public function edit($id){
        $ac = Accounttype::findOrFail($id);
        return view('ifitmash.accounttype.edit',compact('ac'));
    }
    public function update(Request $request,$id)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {

            $user = Accounttype::findOrFail($id);
            $user->update($validator);
            return redirect('member/accounttype');
        }
    }
    public function destroy($id)
    {
        $acs = Accounttype::findOrFail($id);
        $acs->delete();

        return redirect('member/accounttype');
    }
    public function active($id)
    {
        $subCat =  Accounttype::where('id', $id)->first();
        $subCat->status = 1;
        $subCat->save();
        return redirect()->back();
    }

    public function inactive($id)
    {
        //echo 'd';exit;
        $subCat =  Accounttype::where('id', $id)->first();
        $subCat->status = 0;
        $subCat->save();
        return redirect()->back();
    }

}
