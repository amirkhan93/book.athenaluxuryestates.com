<?php

namespace App\Http\Controllers\ifitmash;

use App\BusinessFeatures;
use App\Businesspackage;
use DemeterChain\B;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Auth;


class BusinesspackageController extends Controller
{
    //
    public function index()
    {
        if (Auth::guard('vendor')->id()) {
            $bst = Businesspackage::where('vendor_id',Auth::guard('vendor')->id())->get();
            return view('ifitmash.businesspackage.index', compact('bst'));
        }
    }
    public function create()
    {
        return view('ifitmash.businesspackage.create');
    }
    public function store(Request $request)
    {
        $vendor_id = Auth::guard('vendor')->id();
        $input = $request->all();
        if ($request->has('_token')) {
            $products = new Businesspackage();
            $products->vendor_id = $vendor_id;
            $products->name = $input['name'];
            $products->price = $input['price'];
            $products->taxtype = $input['taxtype'];
            $products->start_date = $input['startdate'];
            $products->end_date = $input['enddate'];

            $catlast = Businesspackage::orderBy('id', 'desc')->first();
            // echo " $catlast";die;
            $code_get_id = Businesspackage::select('code')->where('vendor_id',$vendor_id)->orderBy('id','desc')->first();
            // echo " $catlast";die;
            $code_id = '';
            if($code_get_id['code'] == ''):
                $code_id = 'ANP001';
            else:
                $code_id = "".$code_get_id['code']."";
                $code_id++;
            endif;

            $products->code = $code_id;
            $products->save();
            return redirect('member/businesspackage');
        }
    }
    public function edit($id){
        $ac = Businesspackage::findOrFail($id);

        return view('ifitmash.businesspackage.edit',compact('ac'));
    }
    public function update(Request $request,$id)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {

            $user = Businesspackage::findOrFail($id);
            $user->update($validator);
            return redirect('member/businesspackage');
        }
    }

    public function destroy($id)
    {
        $acs =  Businesspackage::findOrFail($id);
        $acs->delete();

        return redirect('member/businesspackage');
    }
    public function active($id)
    {
        $subCat = Businesspackage::where('id', $id)->first();
        $subCat->status = 1;
        $subCat->save();
        return redirect()->back();
    }

    public function inactive($id)
    {
        //echo 'd';exit;
        $subCat = Businesspackage::where('id', $id)->first();
        $subCat->status = 0;
        $subCat->save();
        return redirect()->back();
    }

}
