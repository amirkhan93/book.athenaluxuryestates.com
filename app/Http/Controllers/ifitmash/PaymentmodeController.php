<?php

namespace App\Http\Controllers\ifitmash;

use App\Payment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Auth;

class PaymentmodeController extends Controller
{
    //
    public function index()
    {
        if (Auth::guard('vendor')->id()) {
            $bst = Payment::where('vendor_id', Auth::guard('vendor')->id())->get();
            return view('ifitmash.paymentmode.index', compact('bst'));
        }
    }
    public function create()
    {
        return view('ifitmash.paymentmode.create');
    }
    public function store(Request $request)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {
            $vendor_id = Auth::guard('vendor')->id();
            $input = $request->all();
            $products = New Payment();
            $products->vendor_id = $vendor_id;
            $products->name = $request->name;
            $code_get_id = Payment::select('code')->where('vendor_id',$vendor_id)->orderBy('id','desc')->first();
            // echo " $catlast";die;
            $code_id = '';
            if($code_get_id['code'] == ''):
                $code_id = 'PAY001';
            else:
                $code_id = "".$code_get_id['code']."";
                $code_id++;
            endif;

            $products->code = $code_id;
            $products->save();
            return redirect('member/paymentmode')->with('Success', 'Sucessfully add');
        }
    }
    public function edit($id){
        $ac = Payment::findOrFail($id);
        return view('ifitmash.paymentmode.edit',compact('ac'));
    }
    public function update(Request $request,$id)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {

            $user = Payment::findOrFail($id);
            $user->update($validator);
            return redirect('member/paymentmode');
        }
    }
    public function destroy($id)
    {
        $acs = Payment::findOrFail($id);
        $acs->delete();

        return redirect('member/paymentmode');
    }
    public function active($id)
    {
        $subCat =  Payment::where('id', $id)->first();
        $subCat->status = 1;
        $subCat->save();
        return redirect()->back();
    }

    public function inactive($id)
    {
        //echo 'd';exit;
        $subCat =  Payment::where('id', $id)->first();
        $subCat->status = 0;
        $subCat->save();
        return redirect()->back();
    }



 }
