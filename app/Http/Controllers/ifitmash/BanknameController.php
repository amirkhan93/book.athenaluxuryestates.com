<?php

namespace App\Http\Controllers\ifitmash;

use App\Bankname;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Auth;

class BanknameController extends Controller
{
    //
    public function index()
    {
        if (Auth::guard('vendor')->id()) {
            $bst = Bankname::where('vendor_id', Auth::guard('vendor')->id())->get();
            return view('ifitmash.bankname.index', compact('bst'));
        }
    }
    public function create()
    {
        return view('ifitmash.bankname.create');
    }
    public function store(Request $request)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {
            $vendor_id = Auth::guard('vendor')->id();
            $input = $request->all();
            $products = New Bankname();
            $products->vendor_id = $vendor_id;
            $products->name = $request->name;
            $code_get_id = Bankname::select('code')->where('vendor_id',$vendor_id)->orderBy('id','desc')->first();
            // echo " $catlast";die;
            $code_id = '';
            if($code_get_id['code'] == ''):
                $code_id = 'BNK001';
            else:
                $code_id = "".$code_get_id['code']."";
                $code_id++;
            endif;
            $products->code = $code_id;
            $products->save();
            return redirect('member/bankname')->with('Success', 'Sucessfully add');
        }
    }
    public function edit($id){
        $ac = Bankname::findOrFail($id);
        return view('ifitmash.bankname.edit',compact('ac'));
    }
    public function update(Request $request,$id)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {

            $user = Bankname::findOrFail($id);
            $user->update($validator);
            return redirect('member/bankname');
        }
    }
    public function destroy($id)
    {
        $acs = Bankname::findOrFail($id);
        $acs->delete();

        return redirect('member/bankname');
    }
    public function active($id)
    {
        $subCat =  Bankname::where('id', $id)->first();
        $subCat->status = 1;
        $subCat->save();
        return redirect()->back();
    }

    public function inactive($id)
    {
        //echo 'd';exit;
        $subCat =  Bankname::where('id', $id)->first();
        $subCat->status = 0;
        $subCat->save();
        return redirect()->back();
    }


}
