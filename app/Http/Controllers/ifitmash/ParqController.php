<?php

namespace App\Http\Controllers\ifitmash;

use App\Parq;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Auth;

class ParqController extends Controller
{
    //
    public function index()
    {
        if (Auth::guard('vendor')->id()) {
            $bst = Parq::where('vendor_id', Auth::guard('vendor')->id())->get();
            return view('ifitmash.parq.index', compact('bst'));
        }
    }
    public function create()
    {
        return view('ifitmash.parq.create');
    }
    public function store(Request $request)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {
            $vendor_id = Auth::guard('vendor')->id();
            //  echo $vendor_id;die;exit;
            $input = $request->all();
            $products = New Parq();
            $products->vendor_id = $vendor_id;
            $products->name = $request->name;
            $code_get_id = Parq::select('code')->where('vendor_id',$vendor_id)->orderBy('id','desc')->first();
            // echo " $catlast";die;
            $code_id = '';
            if($code_get_id['code'] == ''):
                $code_id = 'PRQ001';
            else:
                $code_id = "".$code_get_id['code']."";
                $code_id++;
            endif;

            $products->code = $code_id;
            $products->save();
            return redirect('member/parq')->with('Success', 'Sucessfully add');
        }
    }
    public function edit($id){
        $ac = Parq::findOrFail($id);
        return view('ifitmash.parq.edit',compact('ac'));
    }
    public function update(Request $request,$id)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {

            $user = Parq::findOrFail($id);
            $user->update($validator);
            return redirect('member/parq');
        }
    }
    public function destroy($id)
    {
        $acs = Parq::findOrFail($id);
        $acs->delete();

        return redirect('member/parq');
    }
    public function active($id)
    {
        $subCat =  Parq::where('id', $id)->first();
        $subCat->status = 1;
        $subCat->save();
        return redirect()->back();
    }

    public function inactive($id)
    {
        //echo 'd';exit;
        $subCat =  Parq::where('id', $id)->first();
        $subCat->status = 0;
        $subCat->save();
        return redirect()->back();
    }
}
