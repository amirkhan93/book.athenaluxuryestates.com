<?php

namespace App\Http\Controllers\ifitmash;

use App\Addnewlead;
use App\Feedback;
use App\Source;
use App\Status;
use Session;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class AddnewleadController extends Controller
{
    //
    public function index()
    {
        if(Auth::guard('vendor')->id()) {
            $bn = Addnewlead::where('vendor_id',Auth::guard('vendor')->id())->get();
            return view('ifitmash.addnewlead.index',compact('bn'));
        }
    }
    public function create()
    {
        $ac=Source::all();
        $acs=Status::all();
        return view('ifitmash.addnewlead.create',compact('ac','acs'));
    }
    public function store(Request $request){

        $validator = $request->validate([
            'name' => 'required',
            'email' => 'required|unique:addnewlead',
            'dob' => 'required',
            'sources' => 'required',
            'status' => 'required',
            'contact' => 'required',
            'nextfollow' => 'required',


        ], [
            'name.required' => '*Name is required',
            'email.required' => '*Email is required',
            'dob.required' => '*Date of Birth is required',
            'sources.required' => '*Sources is required',
            'status.required' => '*status is required',
            'contact.required' => '*Contact is required',
            'nextfollow.required' => '*nextfollow is required',

        ]);
        if ($validator == false) {
            return back();
        } else {

            //echo"helloo";die;
            $vendor_id = Auth::guard('vendor')->id();
            $input = $request->all();
            $products = New Addnewlead();
            $products->vendor_id = $vendor_id;
            $products->name = $request->name;
            $products->email = $request->email;
            $products->contact = $request->contact;
            $products->status_id = $request->status;
            $products->source_id = $request->sources;
            $products->gender = $request->gender;
            $products->dob = $request->dob;
            $products->followup = $request->nextfollow;
            $products->traildate = $request->traildate;
            $products->trailtime = $request->trailtime;
            $products->address = $request->address;
            $products->note = $request->note;
            $products->description = $request->description;
            $products->workname = $request->workname;

            //  echo $request->images;die;
            //echo $file = $_FILES['image']['tmp_name'];die;

            //  $products->image= $profileimages;


            $catlast = Addnewlead::orderBy('id', 'desc')->first();
            $code = '';
            // echo " $catlast";die;
            if ($catlast) {

                if ($catlast->code) {
                    $exp = explode('ADL', $catlast->code);
                    $inc = sprintf('%03d', $exp[1] + 1);
                    $code = 'ADL' . $inc;
                } else {
                    $code = 'ADL001';
                }
                $post['code'] = $code;
            }

            $products->code = $code;
            $products->save();
            return redirect('member/addnewlead')->with('Success', 'Sucessfully add');
        }
    }
    public function view($id)
    {
        $bn = Addnewlead::where('id', $id)->get();
        return view('ifitmash.addnewlead.view',compact('bn'));
    }
    public function feedlist($id)
    {
        $bn = Addnewlead::where('id', $id)->get();
        return view('ifitmash.addnewlead.feedlist',compact('bn'));
    }
    public function edit($id)
    {
        $act=Source::all();
        $acs=Status::all();
        $ac=Addnewlead::FindorFail($id);
        return view('ifitmash.addnewlead.edit',compact('ac','acs','act'));
    }
    public function update(Request $request,$id)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {

            $user = Addnewlead::findOrFail($id);
            $user->update($validator);
            return redirect('member/addnewlead');
        }
    }
    public function destroy($id)
    {
        $bn = Addnewlead::findOrFail($id);
        $bn->delete();
        return redirect('member/addnewlead');
    }
    public function active($id)
    {
        $subCat = Addnewlead::where('id', $id)->first();
        $subCat->status = 1;
        $subCat->save();
        return redirect()->back();
    }

    public function inactive($id)
    {
        //echo 'd';exit;
        $subCat =  Addnewlead::where('id', $id)->first();
        $subCat->status = 0;
        $subCat->save();
        return redirect()->back();
    }

    public function feedstore(Request $request){

        $input = $request->all();
        // echo implode($input) ;die;
           // echo $request->message;die;
            //  echo $request->images;die;
            //echo $file = $_FILES['image']['tmp_name'];die;
       // $addnewlead_id = DB::table('addnewlead')->get('id');
               $products  =New Feedback();
      //  $products->addnewlead_id= $addnewlead_id;
              $products->message= $request->message;



            $products->save();
  return redirect('member/addnewlead')->with('Success', 'add');
        }

}
