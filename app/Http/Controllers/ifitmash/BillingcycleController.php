<?php

namespace App\Http\Controllers\ifitmash;

use App\Billingcycle;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Auth;

class BillingcycleController extends Controller
{
    //

    public function index()
    {
        if (Auth::guard('vendor')->id()) {
            $bst = Billingcycle::where('vendor_id', Auth::guard('vendor')->id())->get();
            return view('ifitmash.billingcycle.index', compact('bst'));
        }
    }
    public function create()
    {
        return view('ifitmash.billingcycle.create');
    }
    public function store(Request $request)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {
            $vendor_id = Auth::guard('vendor')->id();
            $input = $request->all();
            $products = New Billingcycle();
            $products->vendor_id = $vendor_id;
            $products->name = $request->name;
            $code_get_id = Billingcycle::select('code')->where('vendor_id',$vendor_id)->orderBy('id','desc')->first();
            // echo " $catlast";die;
            $code_id = '';
            if($code_get_id['code'] == ''):
                $code_id = 'BLC001';
            else:
                $code_id = "".$code_get_id['code']."";
                $code_id++;
            endif;
            $products->code = $code_id;
            $products->save();
            return redirect('member/billingcycle')->with('Success', 'Sucessfully add');
        }
    }
    public function edit($id){
        $ac = Billingcycle::findOrFail($id);
        return view('ifitmash.billingcycle.edit',compact('ac'));
    }
    public function update(Request $request,$id)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {

            $user = Billingcycle::findOrFail($id);
            $user->update($validator);
            return redirect('member/billingcycle');
        }
    }
    public function destroy($id)
    {
        $acs = Billingcycle::findOrFail($id);
        $acs->delete();

        return redirect('member/billingcycle');
    }
    public function active($id)
    {
        $subCat =  Billingcycle::where('id', $id)->first();
        $subCat->status = 1;
        $subCat->save();
        return redirect()->back();
    }

    public function inactive($id)
    {
        //echo 'd';exit;
        $subCat =  Billingcycle::where('id', $id)->first();
        $subCat->status = 0;
        $subCat->save();
        return redirect()->back();
    }


}
