<?php

namespace App\Http\Controllers\ifitmash;

use App\Addnewmember;
use App\Billingcycle;
use App\Businesspackage;
use App\Invoice;
use App\Packagelist;
use App\Payment;
use App\Paymentdetail;
use App\Salesstaff;
use App\Source;
use App\Tags;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Illuminate\Support\Facades\Auth;

class AddnewmemberController extends Controller
{
    //
    public function index()
    {
        if(Auth::guard('vendor')->id()) {

            $bn = Addnewmember::where('vendor_id',Auth::guard('vendor')->id())->get();
            return view('ifitmash.addnewmember.index',compact('bn'));
        }
    }
    public function create()
    {
        $ac=Source::all();
        $tags=Tags::all();
        return view('ifitmash.addnewmember.create',compact('ac','tags'));
    }
    public function store(Request $request){

        $validator = $request->validate([
            'name' => 'required',
            'email' => 'required|unique:addnewlead',
            'dob' => 'required',
            'contact'=>'required',
            'memberid'=>'required',



        ], [
            'name.required' => '*Name is required',
            'email.required' => '*Email is required',
            'dob.required' => '*Date of Birth is required',
            'contact.required' => '*Contact is required',
            'memberid.required' => '*Memberid is required',


        ]);
        if ($validator == false) {
            return back();
        } else {

            //echo"helloo";die;
            $vendor_id = Auth::guard('vendor')->id();
           // echo $vendor_id;die;
            $input = $request->all();
            $products = New Addnewmember();
            $products->vendor_id = $vendor_id;
            $products->member_id = $request->memberid;
            $products->name = $request->name;
            $products->email = $request->email;
            $products->contact = $request->contact;


            $products->gender = $request->gender;
            $products->dob = $request->dob;
            $products->source_id = $request->sources;
            $products->note = $request->note;
            $products->description = $request->description;
            $products->address = $request->address;
            $products->workout = $request->workoutname;
            $products->tags_id = $request->searchtags;


            //  echo $request->images;die;
            //echo $file = $_FILES['image']['tmp_name'];die;

            //  $products->image= $profileimages;


            $catlast = Addnewmember::orderBy('id', 'desc')->first();
            $code = '';
            // echo " $catlast";die;
            if ($catlast) {

                if ($catlast->code) {
                    $exp = explode('ADM', $catlast->code);
                    $inc = sprintf('%03d', $exp[1] + 1);
                    $code = 'ADM' . $inc;
                } else {
                    $code = 'ADM001';
                }
                $post['code'] = $code;
            }

            $products->code = $code;
            $products->save();
            if($products) {

                return redirect('member/invoice/'.$products->id)->with('Success', 'Sucessfully add');
            }
        }
    }
    public function view($id)
    {
        $bn = Addnewmember::where('id', $id)->get();
        return view('ifitmash.addnewmember.view',compact('bn'));
    }
    public function edit($id)
    {

        $ac=Addnewmember::FindorFail($id);
        $act=Source::all();
        return view('ifitmash.addnewmember.edit',compact('ac','act'));
    }
    public function update(Request $request,$id)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {

            $user = Addnewmember::findOrFail($id);
            $user->update($validator);
            return redirect('member/addnewmember');
        }
    }
    public function destroy($id)
    {
        $bn = Addnewmember::findOrFail($id);
        $bn->delete();
        return redirect('member/addnewmember');
    }
    public function active($id)
    {
        $subCat = Addnewmember::where('id', $id)->first();
        $subCat->status = 1;
        $subCat->save();
        return redirect()->back();
    }

    public function inactive($id)
    {
        //echo 'd';exit;
        $subCat =  Addnewmember::where('id', $id)->first();
        $subCat->status = 0;
        $subCat->save();
        return redirect()->back();
    }
    public function invoice($id){
        $acs=Addnewmember::FindorFail($id);
        $ac=Addnewmember::all();
        $tags=Tags::all();
        $sales=Salesstaff::all();
        $package=Businesspackage::all();
        return view('ifitmash.addnewmember.invoice',compact('ac','tags','sales','package','acs'));
    }
    public function invoicestore(Request $request,$id){

        $validator = $request->validate([
            'membername' => 'required',



        ], [
            'membername.required' => '*Member Name is required',



        ]);
        if ($validator == false) {
            return back();
        } else {

            //echo"helloo";die;
            $vendor_id = Session::get('vendor_id');
            $addmember_id = Addnewmember::where('id',$id)->first();
            // echo $vendor_id;die;
            $input = $request->all();
            $products = New Invoice();
            $products->vendor_id = $vendor_id;
            $products->addmember_id = $addmember_id->id;
            $products->invoice= $request->invoice;
            $products->member_id = $request->membername;
            $products->sales_id = $request->sales;
            $products->package_id = $request->package;
            $products->tags_id = $request->tags;




            //  echo $request->images;die;
            //echo $file = $_FILES['image']['tmp_name'];die;

            //  $products->image= $profileimages;


            $catlast = Invoice::orderBy('id', 'desc')->first();
            $code = '';
            // echo " $catlast";die;
            if ($catlast) {

                if ($catlast->code) {
                    $exp = explode('INV', $catlast->code);
                    $inc = sprintf('%03d', $exp[1] + 1);
                    $code = 'INV' . $inc;
                } else {
                    $code = 'INV001';
                }
                $post['code'] = $code;
            }

            $products->code = $code;
            $products->save();
            return redirect('member/addnewmember')->with('Success', 'Sucessfully add');
        }
    }
    public function packagelist(Request $request)
    {
        $validator = $request->validate([
            'choices' => 'required',


        ], [
            'choices.required' => '*Check one required',


        ]);
        if ($validator == false) {
            return back();
        } else {

            //echo"helloo";die;
            $vendor_id = Session::get('vendor_id');
            // $addmember_id = Addnewmember::where('id',$id)->first();
            // echo $vendor_id;die;
            $input = $request->all();
            $products = New Packagelist();
            $products->vendor_id = $vendor_id;
            $products->package_id = $request->choices;


            //  echo $request->images;die;
            //echo $file = $_FILES['image']['tmp_name'];die;

            //  $products->image= $profileimages;


            $products->save();
               if($products) {
                   return redirect('member/packageindexlist/'.$products->id)->with('Success', 'Sucessfully add');
               }
        }
    }
        public function packageindexlist($id){

        $acs=Packagelist::FindorFail($id);
        $ac=Businesspackage::where('id',$acs->package_id)->get();
        return view('ifitmash.addnewmember.packagelist',compact('acs','ac'));
        }
        public function total(){
        $ac=Payment::all();
        return view('ifitmash.addnewmember.total',compact('ac'));
        }
       public function paymentdetail(Request $request){
           $validator = $request->validate([
               'payment' => 'required',


           ], [
               'payments.required' => '*Paymentmode is required',


           ]);
           if ($validator == false) {
               return back();
           } else {

               //echo"helloo";die;
               $vendor_id = Session::get('vendor_id');
               // $addmember_id = Addnewmember::where('id',$id)->first();
               // echo $vendor_id;die;
               $input = $request->all();
               $products = New Paymentdetail();
               $products->vendor_id = $vendor_id;
               $products->paymentmode_id = $request->payment;
               $products->note = $request->note;


               //  echo $request->images;die;
               //echo $file = $_FILES['image']['tmp_name'];die;

               //  $products->image= $profileimages;


               $products->save();
               if($products) {
                   return redirect('member/addnewmember/')->with('Success', 'Sucessfully add');
               }
           }
       }
}
