<?php

namespace App\Http\Controllers\ifitmash;

use App\Tags;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Auth;

class TagsController extends Controller
{
    //
    public function index()
    {
        if (Auth::guard('vendor')->id()) {
            $bst = Tags::where('vendor_id', Auth::guard('vendor')->id())->get();
            return view('ifitmash.tags.index', compact('bst'));
        }
    }
    public function create()
    {
        return view('ifitmash.tags.create');
    }
    public function store(Request $request)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {
            $vendor_id = Auth::guard('vendor')->id();
            $input = $request->all();
            $products = New Tags();
            $products->vendor_id = $vendor_id;
            $products->name = $request->name;
            $code_get_id = Tags::select('code')->where('vendor_id',$vendor_id)->orderBy('id','desc')->first();
            // echo " $catlast";die;
            $code_id = '';
            if($code_get_id['code'] == ''):
                $code_id = 'TAG001';
            else:
                $code_id = "".$code_get_id['code']."";
                $code_id++;
            endif;

            $products->code = $code_id;
            $products->save();
            return redirect('member/tags')->with('Success', 'Sucessfully add');
        }
    }
    public function edit($id){
        $ac = Tags::findOrFail($id);
        return view('ifitmash.tags.edit',compact('ac'));
    }
    public function update(Request $request,$id)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {

            $user = Tags::findOrFail($id);
            $user->update($validator);
            return redirect('member/tags');
        }
    }
    public function destroy($id)
    {
        $acs = Tags::findOrFail($id);
        $acs->delete();

        return redirect('member/tags');
    }
    public function active($id)
    {
        $subCat =  Tags::where('id', $id)->first();
        $subCat->status = 1;
        $subCat->save();
        return redirect()->back();
    }

    public function inactive($id)
    {
        //echo 'd';exit;
        $subCat =  Tags::where('id', $id)->first();
        $subCat->status = 0;
        $subCat->save();
        return redirect()->back();
    }


}
