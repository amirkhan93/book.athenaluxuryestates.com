<?php

namespace App\Http\Controllers\ifitmash;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Session;
use Auth;

class RoleController extends Controller
{
    //
    public function index()
    {
        if (Auth::guard('vendor')->id()) {
            $data['menu'] = 'setting';
            $data['sub_menu'] = 'company';
            $data['list_menu'] = 'role';
            $data['roleData'] = DB::table('roles')->where('vendor_id',Auth::guard('vendor')->id())->get();;

            return view('ifitmash.role.role', $data);
        }
    }

    public function create()
    {
        $data['menu'] = 'setting';
        $data['sub_menu'] = 'company';
        $data['list_menu'] = 'role';
        $data['permissions'] = DB::table('permissions')->get();
        return view('ifitmash.role.role_add', $data);
    }


    public function store(Request $request)
    {
        $validator = $request->validate([
            'name'         => 'required|unique:roles',
            'display_name' => 'required',
            'description'  => 'required'
        ]);


       // $validator = Validator::make($request->all(),$rules);

        if ($validator == false) {
            return back();
        } else {
            $vendor_id = Auth::guard('vendor')->id();
            $role['name'] = $request->name;
            $role['vendor_id']  = $vendor_id;
            $role['display_name'] = $request->display_name;
            $role['description'] = $request->description;
            $roleId = DB::table('roles')->insertGetId($role);

            if($request->permission)
                foreach ($request->permission as $key => $value) {
                    DB::table('permission_role')->insert(['permission_id' => $value, 'role_id' => $roleId]);
                }
            Session::flash('message','Information added successfully');
            return redirect('member/role/list');
        }
    }

    public function edit($id)
    {
        $data['menu'] = 'setting';
        $data['sub_menu'] = 'company';
        $data['list_menu'] = 'role';
        $data['role'] = DB::table('roles')->where('id',$id)->first();

        $data['permissions'] = DB::table('permissions')->get();
        $data['stored_permissions'] = DB::table('permission_role')->where('role_id',$id)->pluck('permission_id');
        return view('ifitmash.role.role_edit', $data);
    }


    public function update(Request $request)
    {

        $validator = $request->validate([
            'name'         => 'required|unique:roles',
            'display_name' => 'required',
            'description'  => 'required'
        ]);


        // $validator = Validator::make($request->all(),$rules);

        if ($validator == false) {
            return back();
        } else {
            $role['name'] = $request->name;
            $role['display_name'] = $request->display_name;
            $role['description'] = $request->description;
            DB::table('roles')->where('id',$request->id)->update($role);

            $stored_permissions = DB::table('permission_role')->where('role_id',$request->id)->pluck('permission_id');
            $permission = isset($request->permission) ? $request->permission : [];
            if(!empty($stored_permissions)){
                foreach ($stored_permissions as $key => $value) {
                    if(!in_array($value, $permission))
                        DB::table('permission_role')->where(['permission_id' => $value, 'role_id' => $request->id])->delete();

                }
            }
            if(!empty($permission)){
                foreach ($permission as $key => $value) {
                    if(!in_array($value, $stored_permissions)){
                        DB::table('permission_role')->insert(['permission_id' => $value, 'role_id' => $request->id]);
                    }
                }
            }

            return redirect('member/role/list');
        }

        \Session::flash('success',trans('message.success.update_success'));
        return redirect('member/role/list');
    }

    public function destroy(Request $request)
    {
        $id = $request->id;
        DB::table('roles')->where(['id'=>$id])->delete();
        DB::table('permission_role')->where(['role_id'=>$id])->delete();
        Session::flash('success',trans('message.success.delete_success'));
        return redirect()->intended('member/role/list');

    }
}
