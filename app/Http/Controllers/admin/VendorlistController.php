<?php

namespace App\Http\Controllers\admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Vendor;
use App\Addnewoutlet;

class VendorlistController extends Controller
{
    //
    public function index()
    {
        $vendors = Vendor::where('status', '!=', 1)->orderBy('id', 'DESC')->get();
        return view('admin.vendorlist.index', compact('vendors'));
    }

    public function approvedmemberlist()
    {
        $vendors = Vendor::where('status', '=', 1)->orderBy('id', 'DESC')->get();
        return view('admin.approvedmemberlist.index', compact('vendors'));
    }

    public function destroy($id)
    {
        $vendors = Vendor::findOrFail($id);
        $vendors->delete();

        return redirect('admin/approvedmember');
    }

    public function active($id)
    {
        $subCat = Vendor::where('id', $id)->first();
        $subCat->status = 1;
        $subCat->save();
        return redirect()->back();
    }

    public function inactive($id)
    {
        //echo 'd';exit;
        $subCat = Vendor::where('id', $id)->first();
        $subCat->status = 0;
        $subCat->save();
        return redirect()->back();
    }
    
      public function reject($id)
    {
        //echo 'd';exit;
        $subCat = Vendor::where('id', $id)->first();
        $subCat->status = 2;
        $subCat->save();
        return redirect()->back();
    }
    
    public function view($id)
    {
        // $vendors=Vendor::findorfail($id);
        $vendors = Vendor::where('id', $id)->get();
        $outlet = Addnewoutlet::where('vendor_id', $id)->get();
        return view('admin.vendorlist.view', compact('vendors', 'outlet'));
    }


}
