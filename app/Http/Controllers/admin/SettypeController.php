<?php

namespace App\Http\Controllers\admin;

use App\Settype;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettypeController extends Controller
{
    //
    public function index()
    {
        $bst = Settype::all();
        return view('admin.settype.index',compact('bst'));
    }
    public function create()
    {
        return view('admin.settype.create');
    }
    public function store(Request $request)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {
            $input = $request->all();
            $products = New Settype();
            $products->name = $request->name;

            $catlast = Settype::orderBy('id', 'desc')->first();
            // echo " $catlast";die;
            $code='';
            if ($catlast) {

                if ($catlast->code) {
                    $exp = explode('SET', $catlast->code);
                    $inc = sprintf('%03d', $exp[1] + 1);
                    $code = 'SET' . $inc;
                } else {
                    $code = 'SET001';
                }
                $post['code'] = $code;
            }

            $products->code = $code;
            $products->save();
            return redirect('admin/settype')->with('Success', 'Sucessfully add');
        }
    }
    public function edit($id){
        $ac = Settype::findOrFail($id);
        return view('admin.settype.edit',compact('ac'));
    }
    public function update(Request $request,$id)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {

            $user = Settype::findOrFail($id);

            $user->update($validator);
            return redirect('admin/settype');
        }
    }
    public function destroy($id)
    {
        //echo"hello";die;
        $acs = Settype::findOrFail($id);
        //print_r($acs);die;
        $acs->delete();

        return redirect('admin/settype');
    }
    public function active($id)
    {
        $subCat =  Settype::where('id', $id)->first();
        $subCat->status = 1;
        $subCat->save();
        return redirect()->back();
    }

    public function inactive($id)
    {
        //echo 'd';exit;
        $subCat =  Settype::where('id', $id)->first();
        $subCat->status = 0;
        $subCat->save();
        return redirect()->back();
    }
    public function edits()
    {
        $ac = DB::table('settype')->where('id', '=', $_POST['id'])->get();
        return view('admin.settype.edit',compact('ac'));
    }

}
