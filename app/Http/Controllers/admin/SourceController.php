<?php

namespace App\Http\Controllers\admin;

use App\Source;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Auth;
class SourceController extends Controller
{
    //
    public function index()
    {
        if (Auth::guard()->id()) {
            $bst = Source::all();
            return view('ifitmash.source.index', compact('bst'));
        }
    }

    public function create()
    {
        return view('ifitmash.source.create');
    }

    public function store(Request $request)
    {
        {
            $validator = $request->validate([
                'name' => 'required',
            ], [
                'name.required' => 'Name is required',
            ]);
            if ($validator == false) {
                return back();
            } else {
                $vendor_id = Auth::guard('vendor')->id();
                $input = $request->all();
                $products = New Source();
                $products->vendor_id = $vendor_id;
                $products->name = $request->name;
                $code_get_id = Source::select('code')->where('vendor_id',$vendor_id)->orderBy('id','desc')->first();
                // echo " $catlast";die;
                $code_id = '';
                if($code_get_id['code'] == ''):
                    $code_id = 'SRC001';
                else:
                    $code_id = "".$code_get_id['code']."";
                    $code_id++;
                endif;
                $products->code = $code_id;
                $products->save();
                return redirect('admin/source')->with('Success', 'Sucessfully add');
            }
        }
    }
    public function edit($id){
        $ac = Source::findOrFail($id);
        return view('ifitmash.source.edit',compact('ac'));
    }
    public function update(Request $request,$id)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {

            $user = Source::findOrFail($id);
            $user->update($validator);
            return redirect('admin/source');
        }
    }
    public function destroy($id)
    {
        $acs = Source::findOrFail($id);
        $acs->delete();

        return redirect('admin/source');
    }
    public function active($id)
    {
        $subCat =  Source::where('id', $id)->first();
        $subCat->status = 1;
        $subCat->save();
        return redirect()->back();
    }

    public function inactive($id)
    {
        //echo 'd';exit;
        $subCat =  Source::where('id', $id)->first();
        $subCat->status = 0;
        $subCat->save();
        return redirect()->back();
    }
}
