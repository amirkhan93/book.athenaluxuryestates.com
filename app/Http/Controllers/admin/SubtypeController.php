<?php

namespace App\Http\Controllers\admin;

use App\Subtype;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SubtypeController extends Controller
{
    //
    public function index()
    {
        $bst = Subtype::all();
        return view('admin.subsettype.index',compact('bst'));
    }
    public function create()
    {
        return view('admin.subsettype.create');
    }
    public function store(Request $request)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {
            $input = $request->all();
            $products = New Subtype();
            $products->name = $request->name;

            $catlast = Subtype::orderBy('id', 'desc')->first();
            // echo " $catlast";die;
            $code='';
            if ($catlast) {

                if ($catlast->code) {
                    $exp = explode('SET', $catlast->code);
                    $inc = sprintf('%03d', $exp[1] + 1);
                    $code = 'SET' . $inc;
                } else {
                    $code = 'SET001';
                }
                $post['code'] = $code;
            }

            $products->code = $code;
            $products->save();
            return redirect('admin/subsettype')->with('Success', 'Sucessfully add');
        }
    }
    public function edit($id){
        $ac = Subtype::findOrFail($id);
        return view('admin.subsettype.edit',compact('ac'));
    }
    public function update(Request $request,$id)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {

            $user = Subtype::findOrFail($id);

            $user->update($validator);
            return redirect('admin/subsettype');
        }
    }
    public function destroy($id)
    {
        //echo"hello";die;
        $acs = Subtype::findOrFail($id);
        //print_r($acs);die;
        $acs->delete();

        return redirect('admin/subsettype');
    }
    public function active($id)
    {
        $subCat =  Subtype::where('id', $id)->first();
        $subCat->status = 1;
        $subCat->save();
        return redirect()->back();
    }

    public function inactive($id)
    {
        //echo 'd';exit;
        $subCat =  Subtype::where('id', $id)->first();
        $subCat->status = 0;
        $subCat->save();
        return redirect()->back();
    }
    public function edits()
    {
        $ac = DB::table('subsettype')->where('id', '=', $_POST['id'])->get();
        return view('admin.subsettype.edit',compact('ac'));
    }

}
