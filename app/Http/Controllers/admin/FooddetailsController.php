<?php

namespace App\Http\Controllers\admin;

use App\Fooddetails;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class FooddetailsController extends Controller
{
    //
    public function index()
    {
        $bst = Fooddetails::all();
        return view('admin.fooddetails.index',compact('bst'));
    }
    public function create()
    {
        return view('admin.fooddetails.create'); 
    }
    public function store(Request $request)
    {
        $validator = $request->validate([
            'foodname' => 'required',
        ], [
            'foodname.required' => 'Food Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {
            $input = $request->all();
            $products = New Fooddetails();
            $products->foodname = $request->foodname;
            //$products->date = $request->date;
            $products->protien = $request->protien;
            $products->carbs = $request->carbs;
            $products->grams = $request->gram;
            $products->calories = $request->calories;
            $products->fiber = $request->fiber;
            $products->fat = $request->fat;

            $catlast = Fooddetails::orderBy('id', 'desc')->first();
            // echo " $catlast";die;
            $code='';
            if ($catlast) { 

                if ($catlast->code) {
                    $exp = explode('FOOD', $catlast->code);
                    $inc = sprintf('%03d', $exp[1] + 1);
                    $code = 'FOOD' . $inc;
                } else {
                    $code = 'FOOD001';
                }
                $post['code'] = $code;
            }

            $products->code = $code;
            $products->save();
            return redirect('admin/fooddetails')->with('Success', 'Sucessfully add');
        }
    }
    public function edit($id){
        $ac = Fooddetails::findOrFail($id);
        return view('admin.fooddetails.edit',compact('ac'));
    }
    public function update(Request $request,$id)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {

            $user = Fooddetails::findOrFail($id);

            $user->update($validator);
            return redirect('admin/fooddetails');
        }
    }
    public function destroy($id)
    {
        //echo"hello";die;
        $acs = Fooddetails::findOrFail($id);
        //print_r($acs);die;
        $acs->delete();

        return redirect('admin/fooddetails');
    }
    public function active($id)
    {
        $subCat =  Fooddetails::where('id', $id)->first();
        $subCat->status = 1;
        $subCat->save();
        return redirect()->back();
    }
 
    public function inactive($id)
    {
        //echo 'd';exit;
        $subCat =  Fooddetails::where('id', $id)->first();
        $subCat->status = 0;
        $subCat->save();
        return redirect()->back();
    }
    public function edits()
    {
        $ac = DB::table('fooddetails')->where('id', '=', $_POST['id'])->get();
        return view('admin.fooddetails.edit',compact('ac'));
    }

    public function editFood($id)
    {
        $food = Fooddetails::where('id', $id)->first();
        // return view('admin.fooddetails.edit',compact('ac'));
        //print_r($ac);die();
        return view('admin.fooddetails.foodEdit',compact('food')); 
    }

    public function foodupdate(Request $request)
    {
        $validator = $request->validate([
            'foodname' => 'required',
        ], [
            'foodname.required' => 'Food Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {
            $input = $request->all();
            $products =Fooddetails::where('id',$request->food_id)->first();
            //$products = New Fooddetails();
            $products->foodname = $request->foodname;
            //$products->date = $request->date;
            $products->protien = $request->protien;
            $products->carbs = $request->carbs;
            $products->grams = $request->gram;
            $products->calories = $request->calories;
            $products->fiber = $request->fiber;
            $products->fat = $request->fat;

            // $catlast = Fooddetails::orderBy('id', 'desc')->first();
            // // echo " $catlast";die;
            // $code='';
            // if ($catlast) { 

            //     if ($catlast->code) {
            //         $exp = explode('FOOD', $catlast->code);
            //         $inc = sprintf('%03d', $exp[1] + 1);
            //         $code = 'FOOD' . $inc;
            //     } else {
            //         $code = 'FOOD001';
            //     }
            //     $post['code'] = $code;
            // }

            // $products->code = $code;
            $products->save();
            return redirect('admin/fooddetails')->with('Success', 'Sucessfully add');
        }
    }

}
