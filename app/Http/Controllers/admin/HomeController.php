<?php

namespace App\Http\Controllers\admin;
use App\Addnewlead;
use App\Http\Controllers\Controller;
use  App\Cities;
use  App\Counteries;
use App\States;
use App\Invoice;
use App\Vendor;
use App\Tax;
use App\Locality;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
//use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
        $lead=Addnewlead::all();
        $member=Vendor::all();
        $inactive=Vendor::where('status','=',0)->get();
        $active=Vendor::where('status','=',1)->get();
        $invoice=Invoice::all();
        return view('admin.home',compact('member','lead','inactive','active','invoice'));  
    }

    public function logout(Request $request){

        $request->session()->flush();
        $request->session()->regenerate();
        return redirect('login');
    }

    public function Locality(){

        $countries = Counteries::all();
        $states = States::where('country_id', '101')->get();

        return view('admin.locality.locality', compact('countries','states'));
    }

    public function localityStore(Request $request){

          $locality = new Locality();

          $locality->country_id = $request->country_id;
          $locality->state_id   = $request->state_id;
          $locality->city_id    = $request->cities_id;
          $locality->locality   = $request->locality;

          $locality->save();
          
          Session::flash('message', 'Locality Successfully inserted');

        $countries = Counteries::all();
        $states = States::where('country_id', '101')->get();

        return view('admin.locality.locality', compact('countries','states'));
    }
    
     public function Tax(){
       
       $bst = Tax::all();
        return view('admin.tax.tax',compact('bst'));
    }
    
    public function taxStore(Request $request){

          $input = new Tax();
          
          $input->name         = $request->name;
          $input->percentage   = $request->percentage;
         

          $input->save();
          
         $bst = Tax::all();
        return view('admin.tax.tax', compact('bst'));
    }
    
    public function taxedit($id){
        
        //echo "hello"; die;

          $data = Tax::where('id',$id)->first();
         

        return view('admin.tax.taxedit', compact('data'));
    }
    
    public function taxUpdate(Request $request){

         $id = $request->id;
          $input = Tax::findOrFail($id);
         
          $input->name         = $request->name;
          $input->percentage   = $request->percent;
         

          $input->save();
          
         $bst = Tax::all();
        return view('admin.tax.tax', compact('bst'));
    }
    
    public function taxDestroy($id)
    {
        $acs = Tax::findOrFail($id);
        $acs->delete();

        return redirect('admin/tax');
    }
}
