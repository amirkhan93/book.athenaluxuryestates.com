<?php

namespace App\Http\Controllers\admin;

use App\Excercise;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExerciseController extends Controller
{
    //
    public function index()
    {
        $bst = Excercise::all();
        return view('admin.excercisetype.index',compact('bst'));
    }
    public function create()
    {
        return view('admin.excercisetype.create');
    }
    public function store(Request $request)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {
            $input = $request->all();
            $products = New Excercise();
            $products->name = $request->name;

            $catlast = Excercise::orderBy('id', 'desc')->first();
            // echo " $catlast";die;
            $code='';
            if ($catlast) {

                if ($catlast->code) {
                    $exp = explode('EXC', $catlast->code);
                    $inc = sprintf('%03d', $exp[1] + 1);
                    $code = 'EXC' . $inc;
                } else {
                    $code = 'EXC001';
                }
                $post['code'] = $code;
            }

            $products->code = $code;
            $products->save();
            return redirect('admin/excercisetype')->with('Success', 'Sucessfully add');
        }
    }
    public function edit($id){
        $ac = Excercise::findOrFail($id);
        return view('admin.excercisetype.edit',compact('ac'));
    }
    public function update(Request $request,$id)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {

            $user = Excercise::findOrFail($id);

            $user->update($validator);
            return redirect('admin/excercisetype');
        }
    }
    public function destroy($id)
    {
        //echo"hello";die;
        $acs = Excercise::findOrFail($id);
        //print_r($acs);die;
        $acs->delete();

        return redirect('admin/excercisetype');
    }
    public function active($id)
    {
        $subCat =  Excercise::where('id', $id)->first();
        $subCat->status = 1;
        $subCat->save();
        return redirect()->back();
    }

    public function inactive($id)
    {
        //echo 'd';exit;
        $subCat =  Excercise::where('id', $id)->first();
        $subCat->status = 0;
        $subCat->save();
        return redirect()->back();
    }

}
