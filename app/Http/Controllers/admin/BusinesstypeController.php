<?php

namespace App\Http\Controllers\admin;

use App\Businesstype;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BusinesstypeController extends Controller
{
    //
    public function index()
    {
        $bst = Businesstype::all();
        return view('admin.businesstype.index',compact('bst'));
    }
    public function create()
    {
        return view('admin.businesstype.create');
    }
    public function store(Request $request)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {
            $input = $request->all();
            $products = New Businesstype();
            $products->name = $request->name;
            $products->choices = implode(',',$request->choices);
            $catlast = Businesstype::orderBy('id', 'desc')->first();
            // echo " $catlast";die;
            $code='';
            if ($catlast) {

                if ($catlast->code) {
                    $exp = explode('BST', $catlast->code);
                    $inc = sprintf('%03d', $exp[1] + 1);
                    $code = 'BST' . $inc;
                } else {
                    $code = 'BST001';
                }
                $post['code'] = $code;
            }

            $products->code = $code;
            $products->save();
            return redirect('admin/businesstype')->with('Success', 'Sucessfully add');
        }
    }
    public function edit($id){
        $ac = Businesstype::findOrFail($id);
        return view('admin.businesstype.edit',compact('ac'));
    }
    public function update(Request $request,$id)
    {
        $validator = $request->validate([
            'name' => 'required',
        ], [
            'name.required' => 'Name is required',
        ]);
        if ($validator == false) {
            return back();
        } else {

            $user = Businesstype::findOrFail($id);
            if(!empty($request->choices)){
                $user->choices = implode(',',$request->choices);
            }
            
           // $user->choices = implode(',',$request->choices);
            $user->update($validator);
            return redirect('admin/businesstype');
        }
    }
    public function destroy($id)
    {
        $acs = Businesstype::findOrFail($id);
        $acs->delete();

        return redirect('admin/businesstype');
    }
    public function active($id)
    {
        $subCat =  Businesstype::where('id', $id)->first();
        $subCat->status = 1;
        $subCat->save();
        return redirect()->back();
    }

    public function inactive($id)
    {
        //echo 'd';exit;
        $subCat =  Businesstype::where('id', $id)->first();
        $subCat->status = 0;
        $subCat->save();
        return redirect()->back();
    }
    public function edits()
    {
        $ac = DB::table('businesstype')->where('id', '=', $_POST['id'])->get();
        return view('admin.businesstype.edit',compact('ac'));
    }
    
   

}
