<?php

namespace App\Http\Controllers\admin;

use App\Packages;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Auth;
use DB;
use App\Addnewoutlet;
use App\Vendor;
use App\Assignpackages;

class PackageController extends Controller
{
    public function index()
    {
        if (Auth::guard()->id()) {
            $bst = Packages::all();
            return view('ifitmash.packages.index', compact('bst'));
        }
    }

    public function create()
    {
        return view('ifitmash.packages.create');
    }

    public function store(Request $request)
    {
        
            $validator = $request->validate([
                'name' => 'required',
            ], [
                'name.required' => 'Name is required',
            ]);
            if ($validator == false) {
                return back();
            } else {
                //$vendor_id = Auth::guard('vendor')->id();
                $input = $request->all();
                $products = New     Packages();
               // $products->vendor_id = $vendor_id;
                $products->name = $request->name;
                $products->price = $request->price;
                $catlast = Packages::orderBy('id', 'desc')->first();
                $code_get_id = Packages::select('code')->orderBy('id','desc')->first();
                // echo " $catlast";die;
                $code_id = '';
                if($code_get_id['code'] == ''):
                    $code_id = 'PAC001';
                else:
                    $code_id = "".$code_get_id['code']."";
                    $code_id++;
                endif;

                $products->code = $code_id;
                $products->save();
                return redirect('admin/packages')->with('Success', 'Sucessfully add');
            }
        
    }
    public function edit($id){
        $ac = Packages::findOrFail($id);
        return view('ifitmash.packages.edit',compact('ac'));
    }
    public function update(Request $request,$id)
    {
        $validator = $request->validate([
            'name' => 'required',
            'price'=> 'required|numeric'
        ], [
            'name.required' => 'Name is required',
            'price.required' => 'Price is required',
        ]);
        if ($validator == false) {
            return back();
        } else {
       $data=array(
               'name'=>$validator['name'],
               'price'=>$validator['price'],
       );

     
         DB::table('packages')
            ->where('id', $id)
            ->update($data);
            return redirect('admin/packages');
        }
    }
    public function destroy($id)
    {
        $acs = Packages::findOrFail($id);
        $acs->delete();

        return redirect('admin/packages');
    }
    public function active($id)
    {
        $subCat =  Packages::where('id', $id)->first();
        $subCat->status = 1;
        $subCat->save();
        return redirect()->back();
    }

    public function inactive($id)
    {
        //echo 'd';exit;
        $subCat =  Packages::where('id', $id)->first();
        $subCat->status = 0;
        $subCat->save();
        return redirect()->back();
    }


    public function assignPackage($id)
    {
         $ac = Vendor::all();
         $pack_outlet = Assignpackages::where('package_id',$id)->get();
         return view('ifitmash.packages.assignpackage',compact('ac','id','pack_outlet'));
    }

     public function packageAssign(Request $request)
    {
        
         $validator = $request->validate([
                'vendor_id' => 'required',
            ], [
                'vendor_id.required' => 'vendor_id is required',
            ]);
            if ($validator == false) {
                return back();
            } else {
              
                 $input = $request->all();
                
                
             
                 for($i=0;$i<count($input['vendor_id']);$i++){
                  

                $ifExits=DB::table('assignpackages')->where('vendor_id',$input['vendor_id'][$i])->get()->toArray();

               
                if(empty($ifExits)){
                  
                        $products = New     Assignpackages();
                        $products->package_id = $input['package_id'];
                        $products->vendor_id = $input['vendor_id'][$i];
                        $products->save();

                    }else{
                      
                       DB::table('assignpackages')
                        ->where(['vendor_id' => $input['vendor_id'][$i]])
                        ->update(['package_id'=>$input['package_id'],'vendor_id'=>$input['vendor_id'][$i]]);
                    }
                    //$vendor_id=Addnewoutlet::where('id',$input['outlet_id'][$i])->first()->vendor_id;
                       
                 }
            
                return redirect('admin/packages/assignPackage/'.$input['package_id'])->with('Success', 'Sucessfully add');
            }
        
    }


}

