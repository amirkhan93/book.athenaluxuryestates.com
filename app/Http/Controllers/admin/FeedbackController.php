<?php

namespace App\Http\Controllers\admin;


use App\BusinessFeatures;
use App\DiscountType;
use App\Target;
use App\Addnewoutlet;
use App\Feedback;
use App\Assignstaff;
use DemeterChain\B;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use DB;
use Session;
use Auth;
use Illuminate\Support\Facades\Mail;

class FeedbackController extends Controller
{
    //
    public function index()
    { 
       //echo "aaaa";die;

              $bst = Feedback::orderby('id','DESC')->get();
           
             return view('admin.feedback.index',compact('bst'));
         
    }
    
  /*  public function store(Request $request)
    {
         if ($request->has('_token')) {
            $input=$request->all();

            $products = new Feedback();

            $products->outlet_id = Auth::guard('outlet')->id();
            $products->token_num = $request->token_num;
            $products->subject = $request->subject;
            $products->message = $request->message;
            $products->save();
            
            if(!empty($input)){

           
              Mail::send('ifitmash.feedback.feedback_mail',['data' => $input],function($message) use ($input){
                  $message->from('admin@ifitmash.com');
                  $message->to('vikas.nagar.1904@gmail.com');
                  $message->cc('sritu2567@gmail.com');
                  $message->subject("Feedback");
                });
             
             }
            //echo "success";die;
           return redirect('outlet/feedback');
            
           
        }
    }*/
    public function edit($id=''){
        //echo "aaaa";die;
        $ac = Feedback::findOrFail($id);
       
        return view('admin.feedback.edit',compact('ac'));
    }
    public function update(Request $request, $id)
    {
      
       DB::table('feedback')->where('id', $id)->update(['reply'=>$request->reply,'status'=>1]);
        return redirect('admin/feedback');
        
    }

    public function destroy($id)
    {
        $acs =  Staff::findOrFail($id);
        $acs->delete();

        return redirect()->back();
    }
    public function active($id)
    {
        $subCat = Staff::where('id', $id)->first();
        $subCat->status = 1;
        $subCat->save();
        return redirect()->back();
    }

    public function inactive($id)
    {
        //echo 'd';exit;
        $subCat = Staff::where('id', $id)->first();
        $subCat->status = 0;
        $subCat->save();
        return redirect()->back();
    }
    
    public function assignStaff(Request $request)
        {
            echo $request->outlet_id;
        }
    
    public function assignStaffstore(Request $request)
        {
            
            foreach($request->staff_id as $staff){
                //echo $staff; die;
                $products = new Assignstaff();
                if($products->outlet_id == $request->assignoutlet_id && $products->staff_id == $staff)
                {
                    return back();
                }
                else{
                     $products->vendor_id = Auth::guard('vendor')->id();
                        $products->outlet_id = $request->assignoutlet_id;
                        $products->staff_id  = $staff;
                        
                        $products->save();
                }
               
                
            }
            
            return redirect('member/addnewoutlet');
        }
        
    public function getStaffs(Request $request)
    {
        $staffs = Assignstaff::where('vendor_id',Auth::guard('vendor')->id())->where('outlet_id',$request->outlet_id)->get();
        
        $staffdata = array();
        foreach($staffs as $staff){
            
            $staffdata[] = Staff::where('id',$staff->staff_id)->first();
            
        }
        
        $staffdata = json_encode($staffdata);
        echo $staffdata;
    }
    
    
 public function staffstatus(Request $request)
 
    {
        $id = $request->staff_id;
        $status = $request->status;
        $subCat = Staff::where('id', $id)->first();
        if($status == 1)
        {
        $subCat->status = 0;
        }
        else{
            $subCat->status = 1;
        }
        $subCat->save();
       echo "success";
    }
    

}
