<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Exception;
use App\AppUsers;
use App\FeedsList;
use App\sharefeeds; 

class TaskController extends Controller
{
    protected $userid = 0;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function registeruser(Request $request)
    {
        $data = $request->all();
        // print_r($data);die;

        if(!empty($data['email'] && $data['mobile'] && $data['password'] && $data['name'] )){

            //DB::enableQueryLog();
            // $check = AppUsers::whereRaw("email = '?' OR mobile = '?' ",[$data['email'],$data['mobile']])->get();

           // $laQuery = DB::getQueryLog();
            // print_r($laQuery); die;               
            $check =  AppUsers::where('email',$data['email'])
                                //->orWhere('mobile',$data['mobile'])
                                ->first();
             $check1 =  AppUsers::where('mobile',$data['mobile'])
                                //->orWhere('mobile',$data['mobile'])
                                ->first();
            // print_r($data['deviceType']);print_r($data['deviceToken']);die;   

            if(empty($check && $check1)){
                $user = AppUsers::create([
                        'name' => $data['name'],
                        'email' => $data['email'],
                        'password' => md5($data['password']),
                        'mobile' => $data['mobile'],
                        'devicetype' => $data['deviceType'],
                        'devicetoken' => $data['deviceToken'],
                        'token' => sha1(time())
                    ]);

                //$id = DB::AppUsers()->lastInsertId();

                $userid = $user->id;

                $lastuser['data'] = AppUsers::select('id','token','email','mobile','name','dob','status','devicetype','devicetoken')
                                ->where('id','=',$userid)
                                 ->first();

                return response()->json([
                        'message' => 'User created!',
                        'status' => 1,
                        'result' => $lastuser
                    ]);

            }
            else{
                 return response()->json([
                    'message' => 'Email or mobile already exists'
                ]);
            }

        }
        else{
             return response()->json([
                    'message' => 'Please fill the required fields!'
                ]);
        }
    }

    public function otp(Request $request)
    {       
            $data = $request->all();  

            if(!empty($data['otp'])){ 

                 $userdata = AppUsers::selectRaw('max(id) as userid')->first();
                 // print_r($userdata);die;
                 $qry = AppUsers::where('id',$userdata->userid)
                                ->update([
                                            'otp' => $data['otp'],
                                            'lat' => $data['lat'],
                                            'lng' => $data['lng'],
                                            'devicetype' => $data['devicetype'],
                                            'devicetoken' => $data['devicetoken']
                                        ]);

                // print_r($qry);die;
                if($qry){
                    $result['data'] = AppUsers::selectRaw('id,name,mobile,email,lat,lng')
                                            ->where('id',$userdata->userid)->first();
                    return response()->json([
                    'message' => 'User created!',
                    'status' => 1,
                    'result' => $result
                    ]);
                }
                else{
                    return response()->json([
                    'message' => 'Error in otp'
                    ]);
                }
            
        } 
        else{
            return response()->json([
                'message' => 'please enter otp'
            ]);
        }

    }

    public function login(Request $request)
    {
        //echo "hello";die;
         $data = $request->all(); 
         // print_r($data);die;

         if(!empty($data['password'] && $data['email'])){

                // DB::enableQueryLog();
                $user['data'] = AppUsers::select('id','token','email','mobile','name')
                                ->where('email','=',$data['email'])
                                 //->orWhere('mobile','=',$data['mobile'])
                                 ->where('password',md5($data['password']))
                                 ->first();
        
                //print_r($user); die;

                if($user && !empty($user['data'])){

                     return response()->json([
                        'message' => 'Successfully login!',
                        'status' => 1,
                        'result' => $user
                    ]);
                }
                else{
                     return response()->json([
                        'message' => 'Not a valid user',
                        'status' => 0
                    ]);
                }
         }
         else{
             return response()->json([
                        'message' => 'Please fill required fields'
                    ]);
         }
         

    }

    public function getfeeds(Request $request)
    {
        // $data = $request->all(); 
        // if(!empty($data['member_id'])){

            $feedlist = FeedsList::selectRaw('feedslist.id as feed_id, IFNULL(feedslist.title,"") as title, IFNULL(feedslist.status,"")as status, IFNULL(feedslist.feed_type,"")as feed_type, IFNULL(feedslist.like_count,"")as like_count, feedslist.created_at, IFNULL(appusers.id,"")as user_id, IFNULL(appusers.name,"")as display_name, IFNULL(appusers.profile_pic,"")as profile_pic')
                ->join('appusers','appusers.id','=','feedslist.member_id')
                // ->where('feedslist.member_id',$data['member_id'])
                ->orderBy('feedslist.id','desc')
                ->get();

                $send = []; $i=0;
                foreach ($feedlist as  $value) {
                       
                 $output =   DB::table('feeds_content')
                                    ->select('id','path','feed_id','member_id','is_img')
                                    ->where('feed_id',$value->feed_id)->get();


                  $send['feed'][$i] = $value;
                  $send['feed'][$i]->date_format = date_format($value->created_at,'d M Y'); 
                  $send['feed'][$i]->content = $output;
                  $i++;
                }                        
                
                $result['data'] = $send;

                if($result && !empty($result)){

                         return response()->json([
                        'message' => 'Success',
                        'status'=>1,
                        'result' => $result
                        ]);
                }
                else{
                        return response()->json([
                        'message' => 'No feeds available'
                        ]);
                }
        // }
        // else{
        //         return response()->json([
        //         'message' => 'Member Id can not be empty'
        //         ]);
        // }
    }

    public function postfeeds1(Request $request)
    {
        $data = $request->all();

        if(!empty($data['member_id'])){

                $user = FeedsList::create([
                            'member_id' => $data['member_id'],
                            'title' => $data['title'],
                            //'description' => $data['description'],
                            //'images' => implode(",",$images),
                            //'videos' => implode(",",$videos),
                            'feed_type' => $data['feed_type']
                        ]);  

                //$images = array();

                if($request->hasFile('images')){

                    $files = $request->file('images');
                    //print_r($files); die;

                    foreach($files as $file){
                        
                        $targets = base_path() . "/resources/assets/images/";
                        $profileimages = $file->getClientOriginalName(); 
                        $file->move($targets, $profileimages);
                        $imagespath = "/resources/assets/images/".$profileimages;


                            DB::table('feeds_content')->insert(
                                            ['path'=> $imagespath,
                                             'feed_id'=>$user->id,
                                             'is_img'=>'1',
                                             'member_id'=>$data['member_id']]);
                        
                    }
                }

                // $videos = array();
                if($request->hasFile('videos')){

                    $files11=$data['videos'];

                   foreach($files11 as $file1){
                        
                        $targets1 = base_path() . "/resources/assets/images/";
                        $profileimages1 = $file1->getClientOriginalName();
                        $file1->move($targets1, $profileimages1);
                        $videosname = "/resources/assets/images/".$profileimages1;

                         DB::table('feeds_content')->insert(
                                            ['path'=> $videosname,
                                             'feed_id'=>$user->id,
                                             'is_img'=>'0',
                                             'member_id'=>$data['member_id']]);
                        
                    }
                }

            if($user){
                 return response()->json([
                    'message' => 'Feeds Saved!',
                    'status'=> 1
                    ]);
            }
            else{
                 return response()->json([
                    'message' => 'Error in saving feeds!'
                    ]);
            } 
        }
    }

    public function postfeeds(Request $request)
    {
        $data = $request->all();

        if(!empty($data['member_id'])){

                $user = FeedsList::create([
                            'member_id' => $data['member_id'],
                            'title' => $data['title'],
                            //'description' => $data['description'],
                            //'images' => implode(",",$images),
                            //'videos' => implode(",",$videos),
                            'feed_type' => $data['feed_type']
                        ]);  

                //$images = array();
                //print_r($request->hasFile('images0'));die;
                if($request->hasFile('images0')){

                    //$files = $request->file('images');
                    //print_r($files); die;

                    /*foreach($files as $file){
                        
                        $targets = base_path() . "/resources/assets/images/";
                        $profileimages = $file->getClientOriginalName(); 
                        $file->move($targets, $profileimages);
                        $imagespath = "/resources/assets/images/".$profileimages;


                            DB::table('feeds_content')->insert(
                                            ['path'=> $imagespath,
                                             'feed_id'=>$user->id,
                                             'is_img'=>'1',
                                             'member_id'=>$data['member_id']]);
                        
                    }*/
                    
                    for($i=0;$i<$_POST["size"];$i++){
                        $files = $request->file('images'.$i);
                        $targets = base_path() . "/resources/assets/images/";
                        $profileimages = $files->getClientOriginalName(); 
                        $files->move($targets, $profileimages);
                        $imagespath = "/resources/assets/images/".$profileimages;


                            DB::table('feeds_content')->insert(
                                            ['path'=> $imagespath,
                                             'feed_id'=>$user->id,
                                             'is_img'=>'1',
                                             'member_id'=>$data['member_id']]);
                    }
                    
                }

                // $videos = array();
                if($request->hasFile('videos0')){

                    //$files11=$data['videos'];

                  /* foreach($files11 as $file1){
                        
                        $targets1 = base_path() . "/resources/assets/images/";
                        $profileimages1 = $file1->getClientOriginalName();
                        $file1->move($targets1, $profileimages1);
                        $videosname = "/resources/assets/images/".$profileimages1;

                         DB::table('feeds_content')->insert(
                                            ['path'=> $videosname,
                                             'feed_id'=>$user->id,
                                             'is_img'=>'0',
                                             'member_id'=>$data['member_id']]);
                        
                    }*/
                    
                     for($i=0;$i<$_POST["size"];$i++){
                        $files = $request->file('videos'.$i);
                        $targets = base_path() . "/resources/assets/images/";
                        $profileimages = $files->getClientOriginalName(); 
                        $files->move($targets, $profileimages);
                        $imagespath = "/resources/assets/images/".$profileimages;

                            DB::table('feeds_content')->insert(
                                            ['path'=> $imagespath,
                                             'feed_id'=>$user->id,
                                             'is_img'=>'0',
                                             'member_id'=>$data['member_id']]);
                    }
                }

            if($user){
                 return response()->json([
                    'message' => 'Feeds Saved!',
                    'status'=> 1
                    ]);
            }
            else{
                 return response()->json([
                    'message' => 'Error in saving feeds!'
                    ]);
            } 
        }
    }

    public function postlike(Request $request){

        //print_r($request->all());die;

        $data = $request->all(); 
        if(!empty($data['member_id'] && $data['feed_id'] && $data['feed_user_id'])){

            if($data['like_type'] == '1'){

                // like process--------------------------
                $get_like = FeedsList::select('like_count')
                                    ->where('member_id',$data['feed_user_id'])
                                    ->where('id',$data['feed_id'])
                                    ->first();

                if (empty($get_like)) {

                    return response()->json([
                    'message' => 'Could not find the feed!',
                    ]);
                    // die;
                }

                $update_like = FeedsList::where('member_id',$data['feed_user_id'])
                                    ->where('id',$data['feed_id'])
                                    ->update(['like_count'=>$get_like->like_count+1]);


                $keep_record = DB::table('feed_like_records')
                                    ->insert(['feed_user_id'=>$data['feed_user_id'],
                                                'feed_id'=>$data['feed_id'],
                                                'member_id'=>$data['member_id'],
                                                'like_type'=>$data['like_type']
                                            ]);
                

                if ($update_like && $keep_record) {
                    $get_like1['data'] = FeedsList::selectRaw('IFNULL(dislike_count,"")as dislike_count,IFNULL(like_count,"")as like_count')
                                        ->where('member_id',$data['feed_user_id'])
                                        ->where('id',$data['feed_id'])
                                        ->first();
                    return response()->json([
                    'message' => 'Post liked successfully!',
                    'status' => $update_like,
                    'result' => $get_like1
                    ]);
                }
                else{
                     return response()->json([
                    'message' => 'Could not liked the post!',
                    'response' => $update_like
                    ]);
                }

            }
            elseif ($data['like_type'] == '0') {



                //dislike process------------------
                 $get_dislike = FeedsList::select('dislike_count')
                                    ->where('member_id',$data['feed_user_id'])
                                    ->where('id',$data['feed_id'])
                                    ->first();

                if (empty($get_dislike)) {

                    return response()->json([
                    'message' => 'Could not find the feed!',
                    ]);
                    // die;
                }

                $update_dislike = FeedsList::where('member_id',$data['feed_user_id'])
                                    ->where('id',$data['feed_id'])
                                    ->update(['dislike_count'=>$get_dislike->dislike_count+1]);

                $keep_record = DB::table('feed_like_records')
                                    ->insert(['feed_user_id'=>$data['feed_user_id'],
                                                'feed_id'=>$data['feed_id'],
                                                'member_id'=>$data['member_id'],
                                                'like_type'=>$data['like_type']
                                            ]);

                if ($update_dislike && $keep_record) {
                    $get_like1['data'] = FeedsList::selectRaw('IFNULL(dislike_count,"")as dislike_count,IFNULL(like_count,"")as like_count')
                                        ->where('member_id',$data['feed_user_id'])
                                        ->where('id',$data['feed_id'])
                                        ->first();
                    return response()->json([
                    'message' => 'Post disliked successfully!',
                    'status' => $update_dislike,
                    'result' => $get_like1
                    ]);
                }
                else{
                     return response()->json([
                    'message' => 'Could not liked the post!',
                    'response' => $update_dislike
                    ]);
                }
            
            }
            
        }
        else{
                 return response()->json([
                'message' => 'Please enter required fields',
                'status' => 0
                ]);
            }
    }

    public function getfeeds_userwise(Request $request)
    {
        $data = $request->all(); 
        if(!empty($data['member_id'])){

            $feedlist = FeedsList::selectRaw('feedslist.id as feed_id, IFNULL(feedslist.title,"") as title, IFNULL(feedslist.status,"")as status, IFNULL(feedslist.feed_type,"")as feed_type, IFNULL(feedslist.like_count,"")as like_count, feedslist.created_at, IFNULL(appusers.id,"")as user_id, IFNULL(appusers.name,"")as display_name, IFNULL(appusers.profile_pic,"")as profile_pic')
                ->join('appusers','appusers.id','=','feedslist.member_id')
                ->where('feedslist.member_id',$data['member_id'])
                ->orderBy('feedslist.id','desc')
                ->get();

                $send = []; $i=0;
                foreach ($feedlist as  $value) {
                       
                 $output =   DB::table('feeds_content')
                                    ->select('id','path','feed_id','member_id','is_img')
                                    ->where('feed_id',$value->feed_id)->get();


                  $send['feed'][$i] = $value;
                  $send['feed'][$i]->date_format = date_format($value->created_at,'d M Y'); 
                  $send['feed'][$i]->content = $output;
                  $i++;
                }                        
                
                $result['data'] = $send;

                if($result && !empty($result)){

                         return response()->json([
                        'message' => 'Success',
                        'status'=>1,
                        'result' => $result
                        ]);
                }
                else{
                        return response()->json([
                        'message' => 'No feeds available'
                        ]);
                }
        }
        else{
                return response()->json([
                'message' => 'Member Id can not be empty'
                ]);
        }
    }

    public function feeds_liked_by_me(Request $request){

        $data = $request->all();
        $member_id = $data['member_id'];

        //DB::enableQueryLog(); 
        $feedlist = DB::table('feed_like_records')
            ->selectRaw('feedslist.id as feed_id, IFNULL(feedslist.title,"") as title, IFNULL(feedslist.status,"")as status, IFNULL(feedslist.feed_type,"")as feed_type, IFNULL(feedslist.like_count,"")as like_count,feedslist.created_at, IFNULL(appusers.id,"")as user_id, IFNULL(appusers.name,"")as display_name, IFNULL(appusers.profile_pic,"")as profile_pic')
           ->join('feedslist','feedslist.member_id','=','feed_like_records.member_id')
           ->join('appusers','appusers.id','=','feed_like_records.member_id')
            ->where('feed_like_records.member_id','=',$member_id)
            ->orderBy('feed_like_records.created_at','desc')
            ->get();
            // print_r($feedlist);
            // die;
            //$laQuery = DB::getQueryLog();
            //print_r($laQuery); die;

            if (empty($feedlist)) {
                return response()->json(['message'=>'No feeds are available!','status'=>0]);
            }

            $send = []; $i=0; 
            foreach ($feedlist as  $value) {
                   
              $output =   DB::table('feeds_content')
                                ->select('id','path','feed_id','member_id','is_img')
                                ->where('feed_id','=',$value->feed_id)->get();

              $send['feed'][$i] = $value;
              $send['feed'][$i]->date_format = date("d M Y", strtotime($value->created_at));
           // $send['feed'][$i]->feed_like_time = date("d M Y", strtotime($value->created_at)); 
              $send['feed'][$i]->content = $output;
              $i++;
            }                        
            
            $result['data'] = $send;


            if ($feedlist) {
                 return response()->json([
                        'message' => 'Success',
                        'status'=>1,
                        'result' => $result
                        ]);
            }

    }
   

   public function changePassword(Request $request)
    {
        //echo "hello";die;
         $data = $request->all(); 
         // print_r($data);die;

         if(!empty($data['userId'] && $data['oldPassword'] && $data['newPassword'])){

                // DB::enableQueryLog();
                $user = AppUsers::select('password')
                                ->where('id','=',$data['userId'])
                                 //->orWhere('mobile','=',$data['mobile'])
                                 ->where('password',md5($data['oldPassword']))
                                 ->first();
        
                //print_r($user); die;

                if(!empty($user)){

                    if($data['newPassword'] == $data['oldPassword'])
                    {

                         return response()->json([
                        'message' => 'Old Password and New Password should not be same',
                        'success' => 0,
                         //'result' => []
                      
                    ]);
                     
                    }
                    else{
                          $qry = AppUsers::where('id',$data['userId'])
                                ->update([
                                            'password' => md5($data['newPassword'])
                                            
                                        ]);  

                         return response()->json([
                        'message' => 'Password Successfully Changed!',
                        'success' => 1,
                        // 'result' => $user
                        
                    ]);

                    }

                    
                }
                else{
                     return response()->json([
                        'message' => 'Old Password not matched',
                        'success' => 0
                    ]);
                }
         }
         else{
             return response()->json([
                        'message' => 'Please fill required fields',
                        'success' => 0,
                    ]);
         }
         

    }


    public function updateProfile(Request $request)
    {
        $data = $request->all();
        // print_r($data);die;

        if(!empty($data['userId'] && $data['name'] && $data['dob'] && $data['status'])){

            $user = AppUsers::where('id',$data['userId'])->first();

            $oldimage = $user['profile_pic'];

            if(!empty($data['profilepick']))
            {
                $file = $request->file('profilepick');
                $filename = $file->getClientOriginalName();
                $movefile = $file->move(base_path('\resources\assets\images\userimage'), $file->getClientOriginalName());

                if($movefile)
                {
                    $qry = AppUsers::where('id',$data['userId'])
                                ->update([
                                            'name'        => $data['name'],
                                            'status'      => $data['status'],
                                            'dob'         => $data['dob'],
                                            'profile_pic' => $filename
                                            
                                        ]);
                if($qry){
                    return response()->json([
                        'message' => 'Profile Updated Successfully!',
                        'success' => 1
                        
                    ]);
                }
                else
                {
                    return response()->json([
                        'message' => 'Something went wrong!',
                        'success' => 0
                        
                    ]);
                }

                

                }
            }
            else
            {
                $qry = AppUsers::where('id',$data['userId'])
                                ->update([
                                            'name'        => $data['name'],
                                            'status'      => $data['status'],
                                            'dob'         => $data['dob'],
                                              
                                        ]);

                return response()->json([
                        'message' => 'Profile Updated Successfully!',
                        'success' => 1
                        
                    ]);
            }

        }
        else{
             return response()->json([
                    'message' => 'Please fill the required fields!',
                    'success' => 0
                ]);
        }
    }

    /*public function updateProfile(Request $request)
    {
        $data = $request->all();
        // print_r($data);die;

        if(!empty($data['userId'] && $data['name'] && $data['dob'] && $data['status'])){

           
            $qry = AppUsers::where('id',$data['userId'])
                                ->update([
                                            'name'     => $data['name'],
                                            'status'   => $data['status'],
                                            'dob'      => $data['dob']
                                            
                                        ]);

                return response()->json([
                        'message' => 'Profile Updated Successfully!',
                        'success' => 1
                        
                    ]);

           
        }
        else{
             return response()->json([
                    'message' => 'Please fill the required fields!'
                    'success' => 0
                ]);
        }
    }*/

    public function shareFeed(Request $request)
    {
       $data = $request->all();
       if($data['loggedin_userid'] == '')
       {
          print('loggedin userid missing');
          return;
       }

       else if($data['feed_id'] == '')
       {
          print('feed id  missing');
          return ;
       }
 
       else{
               try {

                $feeds = FeedsList::where('id',$data['feed_id'])->first();
                $is_user = AppUsers::where('id',$data['loggedin_userid'])->first();
                //print_r($feeds);die();
                if (!empty($feeds) && !empty($is_user)) {
                   $insert = DB::table('sharefeeds')->insert(
                                            ['feed_id'=> $data['feed_id'],
                                             'shared_by_id'=>$data['loggedin_userid'],
                                             'shared_member_id'=>$feeds['member_id'],
                                             ]);
                }
                else{
                    // throw new Exception(response()->json([
                    //     'message' => 'Something went wrong',
                    //     'success' => 0
                        
                    // ]));
                    

                    return response()->json([
                        'message' => 'Something went wrong',
                        'success' => 0
                        
                    ]);
                } 
                
                 
                 if($insert){

                    return response()->json([
                        'message' => 'Feed Shared Successfully!',
                        'success' => 1
                        
                    ]);
                 }
                 else{
                    return response()->json([
                        'message' => 'Something went wrong',
                        'success' => 0
                        
                    ]);
                 }
                   
               } catch (Exception $e) {

                   return $e->getMessage();
               }
       }
    }
   

   public function deleteFeed(Request $request)
    {
       $data = $request->all();
       if($data['loggedin_userid'] == '')
       {
          print('loggedin userid missing');
          return;
       }

       else if($data['feed_id'] == '')
       {
          print('feed id  missing');
          return ;
       }
 
       else{
               try {

                $feeds = FeedsList::where('id',$data['feed_id'])->where('member_id',$data['loggedin_userid'])->delete();
                
                if($feeds){

                    return response()->json([
                        'message' => 'Feed Deleted Successfully!',
                        'success' => 1
                        
                    ]);

                }
                else{
                    // throw new Exception(response()->json([
                    //     'message' => 'Something went wrong',
                    //     'success' => 0
                        
                    // ]));
                    return response()->json([
                        'message' => 'Something went wrong',
                        'success' => 0
                        
                    ]);
                    
                }
                
                 
                   
               } catch (Exception $e) {

                   return $e->getMessage();
               }
       }
    }


    public function followMenber(Request $request)
    {
       $data = $request->all();
       if($data['loggedin_userid'] == '')
       {
          print('loggedin userid is missing');
          return;
       }

       else if($data['member_id'] == '')
       {
          print('member id is missing');
          return ;
       }

       else if($data['follow_type'] == '')
       {
          print('follow type missing is missing');
          return ;
       }
 
       else{
               try {

                
                $is_logedin_user = AppUsers::where('id',$data['loggedin_userid'])->first();
                $followedmember_id = AppUsers::where('id',$data['member_id'])->first();
                
                if (!empty($is_logedin_user) && !empty($followedmember_id)) {
                   $insert = DB::table('followed_member')->insert(
                            ['follower_member_id'=> $data['loggedin_userid'],
                             'following_member_id'=>$data['member_id'],
                             'follow_type'=>$data['follow_type'],
                             ]);
                }

    
                else{
                    
                    return response()->json([
                        'message' => 'Something went wrong',
                        'success' => 0
                        
                    ]);
                    
                }

                if($insert)
                {
                    return response()->json([
                        'message' => 'Followed successfully',
                        'success' => 1
                        
                    ]);
                    
                }
                else
                {
                    return response()->json([
                        'message' => 'Something went wrong',
                        'success' => 0
                        
                    ]);
                }
                
                 
                   
               } catch (Exception $e) {

                   return $e->getMessage();
               }
       }
    }


    // public function followingMenberList(Request $request)
    // {
    //    $data = $request->all();
    //    if($data['user_id'] == '')
    //    {
    //       print('user id is missing');
    //       return;
    //    }

       
    //    else{
    //            try {

                
    //             $is_user = AppUsers::where('id',$data['user_id'])->first();
                
    //             if (!empty($is_user)) {
    //                $memberlist = DB::table('followed_member')wher()->get();
    //             }

    
    //             else{
                    
    //                 return response()->json([
    //                     'message' => 'Something went wrong',
    //                     'success' => 0
                        
    //                 ]);
                    
    //             }

    //             if($insert)
    //             {
    //                 return response()->json([
    //                     'message' => 'Followed successfully',
    //                     'success' => 1
                        
    //                 ]);
                    
    //             }
    //             else
    //             {
    //                 return response()->json([
    //                     'message' => 'Something went wrong',
    //                     'success' => 0
                        
    //                 ]);
    //             }
                
                 
                   
    //            } catch (Exception $e) {

    //                return $e->getMessage();
    //            }
    //    }
    // }
  
}
