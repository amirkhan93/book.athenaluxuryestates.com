<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paymentstatus extends Model
{
    //
    protected $table='payment_status';
    protected  $fillable=[ 'name','code'];
}
