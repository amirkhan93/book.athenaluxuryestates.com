<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sms extends Model
{
    //
    protected $table='sms';
    protected $fillable=['choice','lead_id','member_id','outlet_id','message'];

}
