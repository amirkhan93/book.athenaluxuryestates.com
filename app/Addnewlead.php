<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Addnewlead extends Model
{
    //
    protected $table ='addnewlead';
    protected $fillable =['name','code','email','contact','status_id','source_id','gender','dob','followup','traildate','trailtime','address','note','outlet_id','description','workname','source_id','profile_pic'];
}
