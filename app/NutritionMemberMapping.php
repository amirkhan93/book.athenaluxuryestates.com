<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NutritionMemberMapping extends Model
{
    protected $table='member_nutrition_mapping';
    protected $fillable=['member_id','nutrition_id'];
}
