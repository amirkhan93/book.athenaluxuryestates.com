<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parq extends Model
{
    //
    protected $table='parq';
    protected $fillable=['name','vendor_id','code'];
}
