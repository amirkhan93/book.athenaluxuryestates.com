<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    //
    protected $table='invoice';
    protected $fillable=['member_id','sales_id','tags_id','package_id','code','vendor_id','outlet_id','addmember_id'];
}
