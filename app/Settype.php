<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settype extends Model
{
    //
    protected $table='settype';
    protected $fillable=['name','code'];
}
