<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class Locality extends Model
{
    protected $table='locality';
    protected $fillable=['country_id','state_id','city_id','locality'];
}
