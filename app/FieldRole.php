<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FieldRole extends Model
{
    //
    protected $table='fieldrole';
    protected $fillable=['name','code','vendor_id'];
}
