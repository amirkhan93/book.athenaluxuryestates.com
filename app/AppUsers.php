<?php

namespace App; 

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class AppUsers extends Model
{
    use Notifiable;
    protected $guard = 'user';
    protected $table='appusers';
    protected $fillable=['name','mobile','password','email','token','devicetype','devicetoken'];
}
