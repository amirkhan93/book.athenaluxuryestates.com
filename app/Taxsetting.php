<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Taxsetting extends Model
{
    //
    protected $table='taxsettings';
    protected $fillable=['baseprice','tax','totalamount','outlet_id','package_id'];
}
