<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
class Addnewoutlet extends Authenticatable
{
    
    use Notifiable;
    protected $guard = 'outlet';
    protected $table='addnewoutlet';
    protected $fillable=['outlet_name','code','street_address1','street_address2','country_id','state_id','city_id','name_spoc','smsspoc','contact','email','facility_id','billing_name','billing_address','pricecommited','classcommited','po_number','billingcycle_id','pincode','choices','summary','payeename','accountno','ifsccode','bankname_id','accounttype_id','paymentmode_id','status','classcommited','landmark','maps','grace_period','outlet_manager_photo','outlet_photos','outlet_manager_name','secondary_contact'];
}
