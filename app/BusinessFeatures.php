<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessFeatures extends Model
{
    //
    protected $table='business_features';
    protected $fillable=['id', 'business_id','name'];
}
