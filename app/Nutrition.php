<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nutrition extends Model
{
    protected $table = 'nutrition';
    protected $fillable = ['outlet_id','meal_type','meal_time','week','days_id'];
}
