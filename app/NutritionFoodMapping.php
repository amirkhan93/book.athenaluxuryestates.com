<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NutritionFoodMapping extends Model
{
    protected $table = 'nutrition_food_mapping';
    protected $fillable = ['nutrition_id','food_name'];
}
