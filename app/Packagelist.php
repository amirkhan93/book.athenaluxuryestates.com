<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Packagelist extends Model
{
    //
    protected $table='packagelist';
    protected $fillable=['package_id','vendor_id','outlet_id'];
}
