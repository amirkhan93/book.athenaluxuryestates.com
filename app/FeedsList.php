<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FeedsList extends Model
{
    protected $table='feedslist';
    protected $fillable=['title','description','images','videos','feed_type','member_id','like_count'];
}
