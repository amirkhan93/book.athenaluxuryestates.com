<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Addnewmember extends Model
{
    //
    protected $table='addnewmember';
    protected $fillable=['name','code','member_id','email','contact','outlet_id',
        'dob','gender','note','description','workout','address','tags_id'];
}
