<?php

/**
 * Helpers
 *
 * @package     Makent
 * @subpackage  Start
 * @category    Helper
 * @author      Trioangle Product Team
 * @version     1.5.2
 * @link        http://trioangle.com
 */

namespace App\Http\Start;

use App\Models\SiteSettings;
use View;
use Session;
use App\Models\Metas;
use Image;
use Maatwebsite\Excel\Classes\LaravelExcelWorksheet;
use Maatwebsite\Excel\Writers\LaravelExcelWriter;
use DB;
class Helpers
{

	// Get current controller method name
	public function current_action($route)
	{
		$current = explode('@', $route); // Example $route value: App\Http\Controllers\HomeController@login
		View::share('current_action',$current[1]); // Share current action to all view pages
	}

	// Set Flash Message function
	public function flash_message($class, $message)
	{
		Session::flash('alert-class', 'alert-'.$class);
	    Session::flash('message', $message);
	}

	// Dynamic Function for Showing Meta Details
	public static function meta($url, $field)
	{
		$metas = Metas::where('url', $url);

		if($metas->count())
			return $metas->first()->$field;
		else if($field == 'title')
			return 'Page Not Found';
		else
			return '';
	}
	public static function sitesetting()
	{
		$metas = SiteSettings::get();
        //echo "<pre>";print_r($data['results']);die;
		if($metas->count())
			return $metas;
		else
			return '';
	}

	public function compress_image($source_url, $destination_url, $quality, $width = 225, $height = 225)
	{
        $info = getimagesize($source_url);

            if ($info['mime'] == 'image/jpeg')
                    $image = imagecreatefromjpeg($source_url);

            elseif ($info['mime'] == 'image/gif')
                    $image = imagecreatefromgif($source_url);

        elseif ($info['mime'] == 'image/png')
                    $image = imagecreatefrompng($source_url);

            imagejpeg($image, $destination_url, $quality);

        $this->crop_image($source_url, $width, $height);

        return $destination_url;
    }

    public function crop_image($source_url='', $crop_width=225, $crop_height=225, $destination_url = ''){
    	$image = Image::make($source_url);
        $image_width = $image->width();
        $image_height = $image->height();

        if($image_width < $crop_width && $crop_width < $crop_height){
            $image = $image->fit($crop_width, $image_height);
        }if($image_height < $crop_height  && $crop_width > $crop_height){
            $image = $image->fit($crop_width, $crop_height);
        }

        // if($image_width > $image_height){
        // 	$primary_crop_width = $image_height;
        // 	$primary_crop_height = $image_height;

        // 	$primary_x = round(($image_width - $image_height)/2);
        // 	$primary_y = 0;

        // }if($image_width <= $image_height){
        // 	$primary_crop_width = $image_width;
        // 	$primary_crop_height = $image_width;

        // 	$primary_x = 0;
        // 	$primary_y = $image_width < $image_height ? round(($image_height - $image_width)/2) : 0;

        // }

        // $primary_cropped_image = $image->crop($primary_crop_width, $primary_crop_height, $primary_x, $primary_y);
  		$primary_cropped_image = $image;

        $croped_image = $primary_cropped_image->fit($crop_width, $crop_height);

		if($destination_url == ''){
			$source_url_details = pathinfo($source_url);
			$destination_url = @$source_url_details['dirname'].'/'.@$source_url_details['filename'].'_'.$crop_width.'x'.$crop_height.'.'.@$source_url_details['extension'];
		}
		$croped_image->save($destination_url);
		return $destination_url;
    }

 	public function phone_email_remove($message)
    {
        $replacement = "[removed]";

  $dots=".*\..*\..*";

        $email_pattern = "/[^@\s]*@[^@\s]*\.[^@\s]*/";
        $url_pattern = "/[a-zA-Z]*[:\/\/]*[A-Za-z0-9\-_]+[\.][^\.\s]+[A-Za-z0-9\?\/%&=\?\-_]+/i";
        $phone_pattern = "/\+?[0-9][0-9()\s+]{4,20}[0-9]/";

        $find = array($email_pattern, $phone_pattern);
        $replace = array($replacement, $replacement);

        $message = preg_replace($find, $replace, $message);
        if($message==$dots)
        {

        $message = preg_replace($url_pattern, $replacement, $message);
    }
    else
{
        $message = preg_replace($find, $replace, $message);
      }


        return $message;
    }

    //export files to alignment
     public static function buildExcelFile($filename, $data, $width = array())
    {
        /** @var \Maatwebsite\Excel\Excel $excel */
        $excel = app('excel');

        $excel->getDefaultStyle()
        ->getAlignment()
        ->setHorizontal('left');
        foreach ($data as $key => $array) {
            foreach ($array as $k => $v) {
                if(!$v){
                    $data[$key][$k] = '--';
                }
            }
        }

        // dd($filename, $data, $width);
        return $excel->create($filename, function (LaravelExcelWriter $excel) use($data, $width){
            $excel->sheet('exported-data', function (LaravelExcelWorksheet $sheet) use($data, $width) {
                $sheet->fromArray($data)->setWidth($width);
                $sheet->setAllBorders('thin');
            });
        });
    }

    public static function getSiteDetail(){

		$data['get_site_detail'] = DB::table('site_settings')->get();
		if($data['get_site_detail']){
			return $data['get_site_detail'];
		}else{
			return false;
		}

	}
}
