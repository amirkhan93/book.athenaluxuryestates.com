<?php

/**
 * Home Controller
 *
 * @package     Makent
 * @subpackage  Controller
 * @category    Home
 * @author      Trioangle Product Team
 * @version     1.5.2
 * @link        http://trioangle.com
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Start\Helpers;
use App\Http\Helper\FacebookHelper;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use Validator;
use Auth;
use App;
use Session;
use Route;


class PageController extends Controller
{
    protected $helper; // Global variable for Helpers instance
    private $fb;	// Global variable for store Facebook instance

    /**
     * Constructor to Set FacebookHelper instance in Global variable
     *
     * @param array $fb   Instance of FacebookHelper
     */
    public function __construct(FacebookHelper $fb)
    {
        $this->helper = new Helpers;
        $this->fb = $fb;
    }

    /**
     * Load Home view file
     *
     * @return home page view
     */

    /**
     * View Cancellation Policies
     *
     * @return Cancellation Policies view file
     */
    public function oceanreserve()
    {
        return view('page.ocean-reserve');
    }

    public function marenasbeach()
    {
        return view('page.marenas-beach-resort');
    }

    public function trumpinternational()
    {
        return view('page.trump-international');
    }

    public function ramadajerusalem()
    {
        return view('page.ramada-jerusalem');
    }

    public function richicondos()
    {
        return view('page.ricchi-condos');
    }

    public function fontainebleau()
    {
        return view('page.fontainebleau');
    }

    public function luxuryescazu()
    {
        return view('page.luxuryescazu');
    }

    public function casablanca()
    {
        return view('page.ricchi-condos');
    }

    public function concierge()
    {
        return view('page.concierge');
    }
    
    public function about() {
        return view('page.about');
    }

    public function contact()
    {
        return view('page.contact');
    }
    
    public function nearby_attr()
    {
        return view('page.nearby_attr');
    }

    public function villaroyal()
    {
        return view('page.villaroyal');
    }

    public function request()
    {
        return view('page.request');
    }


    public function ownerjoin()
    {
        return view('page.villaroyal');
    }







}

