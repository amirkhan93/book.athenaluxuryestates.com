<?php

/**
 * Property Type Controller
 *
 * @package     Makent
 * @subpackage  Controller
 * @category    Property Type
 * @author      Trioangle Product Team
 * @version     1.5.2
 * @link        http://trioangle.com
 */

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\DataTables\PropertyDataTable;
use App\Models\Property;
use App\Models\Rooms;
use App\Http\Start\Helpers;
use Validator;

class PropertyController extends Controller
{
    protected $helper;  // Global variable for instance of Helpers

    public function __construct()
    {
        $this->helper = new Helpers;
    }

    /**
     * Load Datatable for Property Type
     *
     * @param array $dataTable  Instance of PropertyTypeDataTable
     * @return datatable
     */
    public function index(PropertyDataTable $dataTable)
    {
   
         $rooms=Property::orderBy("room_id","desc")->paginate(5);
        return view("admin.property.view",compact("rooms"));
    }

    /**
     * Add a New Property Type
     *
     * @param array $request  Input values
     * @return redirect     to Property Type view
     */
    public function add(Request $request)
    {
        if(!$_POST)
        {
            return view('admin.property.add');
        }
        else if($request->submit)
        {
            // Add Property Type Validation Rules
            $rules = array(
                'room_id'    => 'required|unique:property'
            );

            // Add Property Type Validation Custom Names
           

            $validator = Validator::make($request->all(), $rules);
           

            /*if ($validator->fails())
            {
                return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
            }
            else
            {*/
                $property = new Property;

                $property->room_id        = $request->room_id;

                $property->save();

                $this->helper->flash_message('success', 'Added Successfully'); // Call flash message function

                return redirect('admin/property');
            /*}*/
        }
        else
        {
            return redirect('admin/property');
        }
    }

    /**
     * Update Property Type Details
     *
     * @param array $request    Input values
     * @return redirect     to Property Type View
     */
    public function update(Request $request)
    {
        if(!$_POST)
        {
            $data['result'] = Property::find($request->id);

            return view('admin.property.edit', $data);
        }
        else if($request->submit)
        {
            // Edit Property Type Validation Rules
            

            
                $property_type = Property::find($request->id);

                $property_type->room_id        = $request->room_id;
              

                $property_type->save();

                $this->helper->flash_message('success', 'Updated Successfully'); // Call flash message function

                return redirect('admin/property');
            
        }
        else
        {
            return redirect('admin/property');
        }
    }

    /**
     * Delete Property Type
     *
     * @param array $request    Input values
     * @return redirect     to Property Type View
     */
    public function delete(Request $request)
    {
        $count = Property::find($request->id)->delete();
        if($count){
            
            $this->helper->flash_message('success', 'Deleted Successfully'); // Call flash message function
        }
        return redirect('admin/property');
    }
}

