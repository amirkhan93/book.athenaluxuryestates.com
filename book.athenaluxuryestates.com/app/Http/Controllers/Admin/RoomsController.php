<?php

/**
 * Rooms Controller
 *
 * @package     Makent
 * @subpackage  Controller
 * @category    Rooms
 * @author      Trioangle Product Team
 * @version     1.5.2
 * @link        http://trioangle.com
 */

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Controllers\EmailController;
use App\DataTables\RoomsDataTable;
use App\Models\BedType;
use App\Models\PropertyType;
use App\Models\RoomType;
use App\Models\Country;
use App\Models\Currency;
use App\Models\Amenities;
use App\Models\RoomsPhotos;
use App\Models\Rooms;
use App\Models\User;
use App\Models\RoomsAddress;
use App\Models\RoomsDescription;
use App\Models\RoomsPrice;
use App\Models\RoomsStepsStatus;
use App\Models\Reservation;
use App\Models\SavedWishlists;
use App\Models\SpecialOffer;
use App\Models\Reviews;
use App\Models\Payouts;
use App\Models\HostPenalty;
use App\Models\ImportedIcal;
use App\Models\Calendar;
use App\Models\PayoutPreferences;
use App\Http\Helper\PaymentHelper;
use App\Http\Start\Helpers;
use App\Http\Controllers\CalendarController;
use Validator;

class RoomsController extends Controller
{


    protected $payment_helper; // Global variable for Helpers instance

    protected $helper;  // Global variable for instance of Helpers

    public function __construct(PaymentHelper $payment)
    {
        $this->payment_helper = $payment;
        $this->helper = new Helpers;
    }

    /**
     * Load Datatable for Rooms
     *
     * @param array $dataTable  Instance of RoomsDataTable
     * @return datatable
     */
    public function index(RoomsDataTable $dataTable)
    {
        return $dataTable->render('admin.rooms.view');
    }

    /**
     * Add a New Room
     *
     * @param array $request  Input values
     * @return redirect     to Rooms view
     */
    public function add(Request $request)
    {
        if(!$_POST)
        {
            $bedrooms = [];
            $bedrooms[0] = 'Studio';
            for($i=1; $i<=10; $i++)
                $bedrooms[$i] = $i;

            $beds = [];
            for($i=1; $i<=16; $i++)
                $beds[$i] = ($i == 16) ? $i.'+' : $i;

            $bathrooms = [];
            $bathrooms[0] = 0;
            for($i=0.5; $i<=8; $i+=0.5)
                $bathrooms["$i"] = ($i == 8) ? $i.'+' : $i;

            $min_stay = [];
            $min_stay[0] = 0;
            for($i=1; $i<=30; $i+=1)
                $min_stay["$i"] = ($i == 30) ? $i.'+' : $i;

            $accommodates = [];
            for($i=1; $i<=40; $i++)
                $accommodates[$i] = ($i == 40) ? $i.'+' : $i;

            $data['bedrooms']      = $bedrooms;
            $data['beds']          = $beds;
            $data['bed_type']      = BedType::where('status','Active')->lists('name','id');
            $data['bathrooms']     = $bathrooms;
            $data['min_stay']      = $min_stay == 0 ? 1 : $min_stay;
            $data['property_type'] = PropertyType::where('status','Active')->lists('name','id');
            $data['room_type']     = RoomType::where('status','Active')->lists('name','id');
            $data['accommodates']  = $accommodates;
            $data['country']       = Country::lists('long_name','short_name');
            $data['amenities']     = Amenities::active_all();
            $data['users_list']    = User::whereStatus('Active')->lists('first_name','id');

            return view('admin.rooms.add', $data);
        }
        else if($_POST)
        {
            $rooms = new Rooms;

            $rooms->user_id       = $request->user_id;
            $rooms->calendar_type = $request->calendar;
            $rooms->bedrooms      = $request->bedrooms;
            $rooms->beds          = $request->beds;
            $rooms->bed_type      = $request->bed_type;
            $rooms->bathrooms     = $request->bathrooms;
            $rooms->min_stay      = $request->min_stay;
            $rooms->check_in_time  = $request->checkin_time;
            $rooms->check_out_time = $request->checkout_time;
            $rooms->property_type = $request->property_type;
            $rooms->room_type     = $request->room_type;
            $rooms->accommodatwes  = $request->accommodates;
            $rooms->name          = $request->name;

            $search     = '#(.*?)(?:href="https?://)?(?:www\.)?(?:youtu\.be/|youtube\.com(?:/embed/|/v/|/watch?.*?v=))([\w\-]{10,12}).*#x';
                $count      = preg_match($search, $request->video);
                if($count == 1) {
                    $replace    = 'http://www.youtube.com/embed/$2';
                    $video      = preg_replace($search,$replace,$request->video);
                    $rooms->video = $video;
                }
                else {
                    $rooms->video = $request->video;
                }

            $rooms->sub_name      = RoomType::find($request->room_type)->name.' in '.$request->city;
            $rooms->summary       = $request->summary;
            $rooms->amenities     = @implode(',', @$request->amenities);
            $rooms->booking_type  = $request->booking_type;
            $rooms->started       = 'Yes';
            $rooms->status        = 'Listed';
            $rooms->cancel_policy = $request->cancel_policy;

            $rooms->save();

            $rooms_address = new RoomsAddress;

            $rooms_address->room_id        = $rooms->id;
            $rooms_address->address_line_1 = $request->address_line_1;
            $rooms_address->address_line_2 = $request->address_line_2;
            $rooms_address->city           = $request->city;
            $rooms_address->state          = $request->state;
            $rooms_address->country        = $request->country;
            $rooms_address->postal_code    = $request->postal_code;
            $rooms_address->latitude       = $request->latitude;
            $rooms_address->longitude      = $request->longitude;

            $rooms_address->save();

            $rooms_description = new RoomsDescription;

            $rooms_description->room_id               = $rooms->id;
            $rooms_description->space                 = $request->space;
            $rooms_description->access                = $request->access;
            $rooms_description->interaction           = $request->interaction;
            $rooms_description->notes                 = $request->notes;
            $rooms_description->house_rules           = $request->house_rules;
            $rooms_description->neighborhood_overview = $request->neighborhood_overview;
            $rooms_description->transit               = $request->transit;

            $rooms_description->save();

            $rooms_price = new RoomsPrice;

            $rooms_price->room_id          = $rooms->id;
            $rooms_price->night            = $request->night;
            $rooms_price->week             = $request->week;
            $rooms_price->month            = $request->month;
            $rooms_price->cleaning         = $request->cleaning;
            $rooms_price->additional_guest = $request->additional_guest;
            $rooms_price->guests           = ($request->additional_guest) ? $request->guests : '0';
            $rooms_price->security         = $request->security;
            $rooms_price->weekend          = $request->weekend;
            $rooms_price->currency_code    = $request->currency_code;
            $rooms_price->pet_fee          = $request->pet_fee;
            $rooms_price->pool_fee          = $request->pool_fee;
            $rooms_price->admin_fee          = $request->admin_fee;
//            $rooms_price->security_fee          = $request->security_fee;
            $rooms_price->tax          = $request->tax;

            $rooms_price->save();

            // Image upload
            if(isset($_FILES["photos"]["name"]))
            {
            foreach($_FILES["photos"]["error"] as $key=>$error)
            {
                $tmp_name = $_FILES["photos"]["tmp_name"][$key];

                $name = str_replace(' ', '_', $_FILES["photos"]["name"][$key]);

                $ext = strtolower(pathinfo($name, PATHINFO_EXTENSION));

                $name = time().'_'.$name;

                $filename = dirname($_SERVER['SCRIPT_FILENAME']).'/images/rooms/'.$rooms->id;

                if(!file_exists($filename))
                {
                    mkdir(dirname($_SERVER['SCRIPT_FILENAME']).'/images/rooms/'.$rooms->id, 0777, true);
                }

             if($ext == 'png' || $ext == 'jpg' || $ext == 'jpeg' || $ext == 'gif')
                {
                // if($this->helper->compress_image($tmp_name, "images/rooms/".$rooms->id."/".$name, 80))
                if(move_uploaded_file($tmp_name, "images/rooms/".$rooms->id."/".$name))
                {
                    $this->helper->compress_image("images/rooms/".$rooms->id."/".$name, "images/rooms/".$rooms->id."/".$name, 80, 1440, 960);
                    $this->helper->compress_image("images/rooms/".$rooms->id."/".$name, "images/rooms/".$rooms->id."/".$name, 80, 1349, 402);
                    $this->helper->compress_image("images/rooms/".$rooms->id."/".$name, "images/rooms/".$rooms->id."/".$name, 80, 450, 250);

                    $photos          = new RoomsPhotos;
                    $photos->room_id = $rooms->id;
                    $photos->name    = $name;
                    $photos->save();
                }
                }
            }
            $photosfeatured = RoomsPhotos::where('room_id',$rooms->id);
            if($photosfeatured->count() != 0){
            $photos_featured = RoomsPhotos::where('room_id',$rooms->id)->where('featured','Yes');
            if($photos_featured->count() == 0)
            {
                $photos = RoomsPhotos::where('room_id',$rooms->id)->first();
                $photos->featured = 'Yes';
                $photos->save();
            }
            }
            }

            $rooms_steps = new RoomsStepsStatus;

            $rooms_steps->room_id     = $rooms->id;
            $rooms_steps->basics      = 1;
            $rooms_steps->description = 1;
            $rooms_steps->location    = 1;
            $rooms_steps->photos      = 1;
            $rooms_steps->pricing     = 1;
            $rooms_steps->calendar    = 1;

            $rooms_steps->save();

            $this->helper->flash_message('success', 'Room Added Successfully'); // Call flash message function
            return redirect('admin/rooms');
        }
        else
        {
            return redirect('admin/rooms');
        }
    }

       //update validation function
      public function update_price(Request $request)
    {

        $minimum_amount = $this->payment_helper->currency_convert('USD',$request->currency_code, 10);
        $currency_symbol = Currency::whereCode($request->currency_code)->first()->original_symbol;
   if(isset($request->night) || isset($request->week) || isset($request->month))
   {
        $night_price = $request->night;        $week_price = $request->week;       $month_price = $request->month;

         // all error validation check
         if(isset($request->night) && isset($request->week) && isset($request->month))
        {
            if($night_price < $minimum_amount && $week_price < $minimum_amount && $month_price < $minimum_amount)
                {
                     return json_encode(['success'=>'all_error','msg' => trans('validation.min.numeric', ['attribute' => trans('messages.inbox.price'), 'min' => $currency_symbol.$minimum_amount]), 'attribute' => 'price', 'currency_symbol' => $currency_symbol,'min_amt' => $minimum_amount]);

                }
        }
         // night validation check
        if(isset($request->night))
        {
            $night_price = $request->night;
            if($night_price < $minimum_amount)
            {
                return json_encode(['success'=>'night_false','msg' => trans('validation.min.numeric', ['attribute' => trans('messages.inbox.price'), 'min' => $currency_symbol.$minimum_amount]), 'attribute' => 'price', 'currency_symbol' => $currency_symbol,'min_amt' => $minimum_amount,'val' => $night_price]);
            }else  {   return json_encode(['success'=>'true','msg' => 'true']);     }
        }
         // week validation check
        elseif(isset($request->week) && @$request->week !='0')
            {
                $week_price = $request->week;
                if($week_price < $minimum_amount)
                {
                    return json_encode(['success'=>'week_false','msg' => trans('validation.min.numeric', ['attribute' => 'price', 'min' => $currency_symbol.$minimum_amount]), 'attribute' => 'week', 'currency_symbol' => $currency_symbol,'val' => $week_price]);
                }else
                {
                    return json_encode(['success'=>'true','msg' => 'true']);
                }
            }
        // month validation check
       elseif(isset($request->month) && @$request->month !='0')
           {
                $month_price = $request->month;
                if($month_price < $minimum_amount)
                {
                    return json_encode(['success'=>'month_false','msg' => trans('validation.min.numeric', ['attribute' => 'price', 'min' => $currency_symbol.$minimum_amount]), 'attribute' => 'month', 'currency_symbol' => $currency_symbol,'val' => $month_price]);
                }else
                {
                    return json_encode(['success'=>'true','msg' => 'true']);
                }

           }

        else {  return json_encode(['success'=>'true','msg' => 'true']); }
    }


    }

    /**
     * Update Room Details
     *
     * @param array $request    Input values
     * @return redirect     to Rooms View
     */
    public function update(Request $request, CalendarController $calendar)
    {
        $rooms_id = Rooms::find($request->id); if(empty($rooms_id))  abort('404');
        if(!$_POST)
        {
            $bedrooms = [];
            $bedrooms[0] = 'Studio';
            for($i=1; $i<=10; $i++)
                $bedrooms[$i] = $i;

            $beds = [];
            for($i=1; $i<=16; $i++)
                $beds[$i] = ($i == 16) ? $i.'+' : $i;

            $bathrooms = [];
            $bathrooms[0] = 0;
            for($i=0.5; $i<=8; $i+=0.5)
                $bathrooms["$i"] = ($i == 8) ? $i.'+' : $i;

            $min_stay = [];
            $min_stay[0] = 0;
            for($i=1; $i<=30; $i+=1)
                $min_stay["$i"] = ($i == 30) ? $i.'+' : $i;

            $accommodates = [];
            for($i=1; $i<=40; $i++)
                $accommodates[$i] = ($i == 40) ? $i.'+' : $i;

            $data['bedrooms']      = $bedrooms;
            $data['beds']          = $beds;

            $data['bed_type']      = BedType::where('status','Active')->lists('name','id');
            $data['bathrooms']     = $bathrooms;
            $data['min_stay']      = $min_stay == 0 ? 1 : $min_stay;
            $data['property_type'] = PropertyType::where('status','Active')->lists('name','id');
            $data['room_type']     = RoomType::where('status','Active')->lists('name','id');
            $data['accommodates']  = $accommodates;
            $data['country']       = Country::lists('long_name','short_name');
            $data['amenities']     = Amenities::active_all();
            $data['users_list']    = User::lists('first_name','id');
            $data['room_id']       = $request->id;
            $data['result']        = Rooms::find($request->id);
            $data['rooms_photos']  = RoomsPhotos::where('room_id',$request->id)->get();
          //  $data['calendar']      = str_replace(url('manage-listing/'.$request->id.'/calendar'), url('admin/edit_room/'.$request->id),$calendar->generate($request->id));
          //  $data['last_update_file']     = ImportedIcal::where('room_id',$request->id)->limit(1)->get();
          $data['calendar']      = str_replace(['<form name="calendar-edit-form">','</form>', url('manage-listing/'.$request->id.'/calendar')], ['', '', 'javascript:void(0);'],$calendar->generate($request->id));
            $data['prev_amenities'] = explode(',', $data['result']->amenities);

            return view('admin.rooms.edit', $data);
        }
        else if($request->submit == 'basics')
        {
            $rooms = Rooms::find($request->room_id);

            $rooms->bedrooms      = $request->bedrooms;
            $rooms->beds          = $request->beds;
            $rooms->bed_type      = $request->bed_type;
            $rooms->bathrooms     = $request->bathrooms;
            $rooms->min_stay      = $request->min_stay;
            $rooms->check_in_time  = $request->checkin_time;
            $rooms->check_out_time = $request->checkout_time;
            $rooms->property_type = $request->property_type;
            $rooms->room_type     = $request->room_type;
            $rooms->accommodates  = $request->accommodates;

            $rooms->save();

            $this->helper->flash_message('success', 'Room Updated Successfully'); // Call flash message function
            return redirect('admin/rooms');
        }
        else if($request->submit == 'booking_type')
        {
            $rooms = Rooms::find($request->room_id);

            $rooms->booking_type  = $request->booking_type;

            $rooms->save();

            $this->helper->flash_message('success', 'Room Updated Successfully'); // Call flash message function
            return redirect('admin/rooms');
        }
        else if($request->submit == 'description')
        {

            $rooms = Rooms::find($request->room_id);

            $rooms->name          = $request->name;
            $rooms->sub_name      = RoomType::find($request->room_type)->name.' in '.$request->city;
            $rooms->summary       = $request->summary;

            $rooms->save();

            $rooms_description = RoomsDescription::find($request->room_id);

            $rooms_description->space                 = $request->space;
            $rooms_description->access                = $request->access;
            $rooms_description->interaction           = $request->interaction;
            $rooms_description->notes                 = $request->notes;
            $rooms_description->house_rules           = $request->house_rules;
            $rooms_description->neighborhood_overview = $request->neighborhood_overview;
            $rooms_description->transit               = $request->transit;

            $rooms_description->save();

            $this->helper->flash_message('success', 'Room Updated Successfully'); // Call flash message function
            return redirect('admin/rooms');
        }
        else if($request->submit == 'location')
        {
            $rooms_address = RoomsAddress::find($request->room_id);

            $rooms_address->address_line_1 = $request->address_line_1;
            $rooms_address->address_line_2 = $request->address_line_2;
            $rooms_address->city           = $request->city;
            $rooms_address->state          = $request->state;
            $rooms_address->country        = $request->country;
            $rooms_address->postal_code    = $request->postal_code;
            $rooms_address->latitude       = $request->latitude;
            $rooms_address->longitude      = $request->longitude;

            $rooms_address->save();

            $this->helper->flash_message('success', 'Room Updated Successfully'); // Call flash message function
            return redirect('admin/rooms');
        }
        else if($request->submit == 'amenities')
        {
            $rooms = Rooms::find($request->room_id);

            $rooms->amenities     = @implode(',', @$request->amenities);

            $rooms->save();

            $this->helper->flash_message('success', 'Room Updated Successfully'); // Call flash message function
            return redirect('admin/rooms');
        }
        else if($request->submit == 'photos')
        {
            // Image upload
        if(isset($_FILES["photos"]["name"]))
           {
                foreach($_FILES["photos"]["error"] as $key=>$error)
                {
                $tmp_name = $_FILES["photos"]["tmp_name"][$key];

                $name = str_replace(' ', '_', $_FILES["photos"]["name"][$key]);

                $ext = strtolower(pathinfo($name, PATHINFO_EXTENSION));

                $name = time().'_'.$name;

                $filename = dirname($_SERVER['SCRIPT_FILENAME']).'/images/rooms/'.$request->room_id;

                if(!file_exists($filename))
                {
                    mkdir(dirname($_SERVER['SCRIPT_FILENAME']).'/images/rooms/'.$request->room_id, 0777, true);
                }

                if($ext == 'png' || $ext == 'jpg' || $ext == 'jpeg' || $ext == 'gif')
                {
                // if($this->helper->compress_image($tmp_name, "images/rooms/".$request->room_id."/".$name, 80))
                if(move_uploaded_file($tmp_name, "images/rooms/".$request->room_id."/".$name))
                {
                    $this->helper->compress_image("images/rooms/".$request->id."/".$name, "images/rooms/".$request->id."/".$name, 80, 1440, 960);
                    $this->helper->compress_image("images/rooms/".$request->id."/".$name, "images/rooms/".$request->id."/".$name, 80, 1349, 402);
                    $this->helper->compress_image("images/rooms/".$request->id."/".$name, "images/rooms/".$request->id."/".$name, 80, 450, 250);

                    $photos          = new RoomsPhotos;
                    $photos->room_id = $request->room_id;
                    $photos->name    = $name;
                    $photos->save();
                }
                }
           }

            $photos_featured = RoomsPhotos::where('room_id',$request->room_id)->where('featured','Yes');

            if($photos_featured->count() == 0)
            {
                $photos = RoomsPhotos::where('room_id',$request->room_id)->first();
                $photos->featured = 'Yes';
                $photos->save();
            }
            }
            $this->helper->flash_message('success', 'Room Updated Successfully'); // Call flash message function
            return redirect('admin/rooms');
        }
        else if($request->submit == 'video')
        {
            $rooms = Rooms::find($request->room_id);

            $search     = '#(.*?)(?:href="https?://)?(?:www\.)?(?:youtu\.be/|youtube\.com(?:/embed/|/v/|/watch?.*?v=))([\w\-]{10,12}).*#x';
                $count      = preg_match($search, $request->video);
                $rooms      = Rooms::find($request->id);
                if($count == 1) {
                    $replace    = 'http://www.youtube.com/embed/$2';
                    $video      = preg_replace($search,$replace,$request->video);
                    $rooms->video = $video;
                }
                else {
                    $rooms->video = $request->video;
                }

            $rooms->save();

            $this->helper->flash_message('success', 'Room Updated Successfully'); // Call flash message function
            return redirect('admin/rooms');
        }
        else if($request->submit == 'pricing')
        {
            $rooms_price = RoomsPrice::find($request->room_id);

            $rooms_price->night            = $request->night;
            $rooms_price->week             = $request->week;
            $rooms_price->month            = $request->month;
            $rooms_price->cleaning         = $request->cleaning;
            $rooms_price->additional_guest = $request->additional_guest;
            $rooms_price->guests           = ($request->additional_guest) ? $request->guests : '0';
            $rooms_price->security         = $request->security;
            $rooms_price->weekend          = $request->weekend;
            $rooms_price->currency_code    = $request->currency_code;
            $rooms_price->pet_fee          = $request->pet_fee;
            $rooms_price->pool_fee          = $request->pool_fee;
            $rooms_price->admin_fee          = $request->admin_fee;
//            $rooms_price->security_fee          = $request->security_fee;
            $rooms_price->tax          = $request->tax;

            $rooms_price->save();

            $this->helper->flash_message('success', 'Room Updated Successfully'); // Call flash message function
            return redirect('admin/rooms');
        }
        else if($request->submit == 'terms')
        {
            //echo "<pre>";print_r($request);die;
            $rooms = Rooms::find($request->room_id);
            //echo $request->firstpayment.','.$request->secondpyment.','.$request->thirdpayment.','.$request->fourthpayment.','.$request->fifthpayment;die;
                $rooms->cancel_policy = $request->cancel_policy;
                $rooms->firstpayment = $request->firstpayment;
                $rooms->secondpayment = $request->secondpayment;
                $rooms->thirdpayment = $request->thirdpayment;
                $rooms->fourthpayment = $request->fourthpayment;
                $rooms->fifthpayment = $request->fifthpayment;

            $rooms->save();

            $this->helper->flash_message('success', 'Room Updated Successfully'); // Call flash message function
            return redirect('admin/rooms');
        }else if($request->submit == 'mainimage'){
			if(isset($_FILES["mainphotos"]["name"]))
           {

                $tmp_name = $_FILES["mainphotos"]["tmp_name"];

                $name = str_replace(' ', '_', $_FILES["mainphotos"]["name"]);

                $ext = strtolower(pathinfo($name, PATHINFO_EXTENSION));

                $name = time().'_'.$name;

                $filename = dirname($_SERVER['SCRIPT_FILENAME']).'/images/rooms/'.$request->room_id;

                if(!file_exists($filename))
                {
                    mkdir(dirname($_SERVER['SCRIPT_FILENAME']).'/images/rooms/'.$request->room_id, 0777, true);
                }

                if($ext == 'png' || $ext == 'jpg' || $ext == 'jpeg' || $ext == 'gif')
                {
                // if($this->helper->compress_image($tmp_name, "images/rooms/".$request->room_id."/".$name, 80))
                if(move_uploaded_file($tmp_name, "images/rooms/".$request->room_id."/".$name))
                {
                    $this->helper->compress_image("images/rooms/".$request->id."/".$name, "images/rooms/".$request->id."/".$name, 80, 1440, 960);
                    $this->helper->compress_image("images/rooms/".$request->id."/".$name, "images/rooms/".$request->id."/".$name, 80, 1349, 402);
                    $this->helper->compress_image("images/rooms/".$request->id."/".$name, "images/rooms/".$request->id."/".$name, 80, 450, 250);

                   // $photos          = new Rooms;
					$photos = Rooms::find($request->room_id);
                   // $photos->room_id = $request->room_id;
                    $photos->main_image    = $name;
                    $photos->save();
                }

           }

            $photos_featured = RoomsPhotos::where('room_id',$request->room_id)->where('featured','Yes');

            if($photos_featured->count() == 0)
            {
                $photos = RoomsPhotos::where('room_id',$request->room_id)->first();
                $photos->featured = 'Yes';
                $photos->save();
            }
            }
            $this->helper->flash_message('success', 'Room Updated Successfully'); // Call flash message function
            return redirect('admin/rooms');
		}
        else if($request->submit == 'cancel')
        {
            return redirect('admin/rooms');
        }
        else
        {
            return redirect('admin/rooms');
        }
    }

    public function calendar_edit(Request $request,EmailController $email_controller)
    {
        $start_date = date('Y-m-d', strtotime($request->start_date));
        $start_date = strtotime($start_date);

        $end_date   = date('Y-m-d', strtotime($request->end_date));
        $end_date   = strtotime($end_date);
        if($request->price && $request->price-0 > 0){
            for ($i=$start_date; $i<=$end_date; $i+=86400)
            {
                $date = date("Y-m-d", $i);

                $is_reservation = Reservation::whereRoomId($request->id)->whereRaw('status!="Declined"')->whereRaw('status!="Expired"')->whereRaw('status!="Cancelled"')->whereRaw('(checkin = "'.$date.'" or (checkin < "'.$date.'" and checkout > "'.$date.'")) ')->get()->count();
                if($is_reservation == 0)
                {
                $data = [ 'room_id' => $request->id,
                          'price'   => ($request->price) ? $request->price : '0',
                          'status'  => $request->status,
                          'min_stay' => $request->min_stay,
                          'notes'   => $request->notes,
                        ];
                Calendar::updateOrCreate(['room_id' => $request->id, 'date' => $date], $data);
                }
            }
            return redirect('admin/edit_room/'.$request->id);
        }
         $email_controller->room_details_updated($request->id, 'Calendar');
    }

 public function update_video(Request $request)
     {

            $data_calendar     = @json_decode($request['data']);
            $rooms = Rooms::find($data_calendar->id);

            $search     = '#(.*?)(?:href="https?://)?(?:www\.)?(?:youtu\.be/|youtube\.com(?:/embed/|/v/|/watch?.*?v=))([\w\-]{10,12}).*#x';
                $count      = preg_match($search, $data_calendar->video);
                $rooms      = Rooms::find($data_calendar->id);
                if($count == 1) {
                    $replace    = 'http://www.youtube.com/embed/$2';
                    $video      = preg_replace($search,$replace,$data_calendar->video);
                    $rooms->video = $video;
                }
                else {
                    $rooms->video = $data_calendar->video;
                }

            $rooms->save();

            return json_encode(['success'=>'true', 'steps_count' => $rooms->steps_count,'video' => $rooms->video]);
        }
    /**
     * Delete Rooms
     *
     * @param array $request    Input values
     * @return redirect     to Rooms View
     */
    public function delete(Request $request)
    {
        $check = Reservation::whereRoomId($request->id)->count();

        if($check) {
            $this->helper->flash_message('error', 'This room has some reservations. So, you cannot delete this room.'); // Call flash message function
        }
        else
        {
            Rooms::find($request->id)->Delete_All_Room_Relationship();
           $this->helper->flash_message('success', 'Deleted Successfully'); // Call flash message function
        }

        return redirect('admin/rooms');
    }

    /**
     * Users List for assign Rooms Owner
     *
     * @param array $request    Input values
     * @return json Users table
     */
    public function users_list(Request $request)
    {
        return User::where('first_name', 'like', $request->term.'%')->select('first_name as value','id')->get();
    }

    /**
     * Ajax function of Calendar Dropdown and Arrow
     *
     * @param array $request    Input values
     * @param array $calendar   Instance of CalendarController
     * @return html Calendar
     */
    public function ajax_calendar(Request $request, CalendarController $calendar)
    {

        // dd($request->id);
        // $data['result']      = Rooms::check_user($request->id); // Check Room Id and User Id is correct or not
        $data['result'] = Rooms::find($request->id);
        $data_calendar     = @json_decode($request['data']);
        $year              = @$data_calendar->year;
        $month             = @$data_calendar->month;
        $data['room_id']   = $request->id;
        $data['calendar']      = str_replace(url('manage-listing/'.$request->id.'/calendar'), url('admin/edit_room/'.$request->id),$calendar->generate($request->id, $year, $month));
        $data['last_update_file']     = ImportedIcal::where('room_id',$request->id)->limit(1)->get();

        return view('admin.rooms.edit_calendar', $data);
    }

    public function currency_check(Request $request)
    {
        $id             = $request->id;
        $new_price      = $request->n_price;
        $price          = RoomsPrice::find($id);
        $minimum_amount = $this->payment_helper->currency_convert('USD', $price->currency_code, 10);
        $currency_symbol = Currency::whereCode($price->currency_code)->first()->original_symbol;
        if($minimum_amount > $new_price)
        {
            echo "fail";
        }
        else
        {
            echo "success";
        }
    }

    /**
     * Delete Rooms Photo
     *
     * @param array $request    Input values
     * @return json success
     */
    public function delete_photo(Request $request)
    {

        $photos          = RoomsPhotos::find($request->photo_id);
        if($photos != NULL){
            $photos->delete();
        }
        $photos_featured = RoomsPhotos::where('room_id',$request->room_id)->where('featured','Yes');

        if($photos_featured->count() == 0)
        {
            $photos_featured = RoomsPhotos::where('room_id',$request->room_id);

            if($photos_featured->count() !=0)
            {
                $photos = RoomsPhotos::where('room_id',$request->room_id)->first();
                $photos->featured = 'Yes';
                $photos->save();
            }
        }

        return json_encode(['success'=>'true']);
    }

    /**
     * Ajax List Your Space Photos Highlights
     *
     * @param array $request    Input values
     * @return json success
     */
    public function photo_highlights(Request $request)
    {
        $photos = RoomsPhotos::find($request->photo_id);

        $photos->highlights = $request->data;

        $photos->save();

        return json_encode(['success'=>'true']);
    }

    public function popular(Request $request)
    {
        $prev = Rooms::find($request->id)->popular;

        if($prev == 'Yes')
            Rooms::where('id',$request->id)->update(['popular'=>'No']);
        else
            Rooms::where('id',$request->id)->update(['popular'=>'Yes']);

        $this->helper->flash_message('success', 'Updated Successfully'); // Call flash message function
        return redirect('admin/rooms');
    }

    public function recommended(Request $request)
    {
        $room = Rooms::find($request->id);
        $user_check = User::find($room->user_id);
        if($room->status != 'Listed')
        {
            $this->helper->flash_message('error', 'Not able to recommend for unlisted listing');
            return back();
        }
        if($user_check->status != 'Active')
        {
            $this->helper->flash_message('error', 'Not able to recommend for Not Active users');
            return back();
        }

        $prev = $room->recommended;

        if($prev == 'Yes')
            Rooms::where('id',$request->id)->update(['recommended'=>'No']);
        else
            Rooms::where('id',$request->id)->update(['recommended'=>'Yes']);

        $this->helper->flash_message('success', 'Updated Successfully'); // Call flash message function
        return redirect('admin/rooms');
    }

    public function featured_image(Request $request)
    {

        RoomsPhotos::whereRoomId($request->id)->update(['featured' => 'No']);

        RoomsPhotos::whereId($request->photo_id)->update(['featured' => 'Yes']);

        return 'success';
    }
}
