<?php

/**
 * Room Type Controller
 *
 * @package     Makent
 * @subpackage  Controller
 * @category    Room Type
 * @author      Trioangle Product Team
 * @version     1.5.2
 * @link        http://trioangle.com
 */

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\DataTables\RoomTypeDataTable;
use App\Models\RoomType;
use App\Models\Rooms;
use App\Http\Start\Helpers;
use Validator;

class RoomTypeController extends Controller
{
    protected $helper;  // Global variable for instance of Helpers

    public function __construct()
    {
        $this->helper = new Helpers;
    }

    /**
     * Load Datatable for Room Type
     *
     * @param array $dataTable  Instance of RoomTypeDataTable
     * @return datatable
     */
    public function index(RoomTypeDataTable $dataTable)
    {
        return $dataTable->render('admin.room_type.view');
    }

    /**
     * Add a New Room Type
     *
     * @param array $request  Input values
     * @return redirect     to Room Type view
     */
    public function add(Request $request)
    {
        if(!$_POST)
        {
            return view('admin.room_type.add');
        }
        else if($request->submit)
        {
            // Add Room Type Validation Rules
            $rules = array(
                    'name'    => 'required|unique:room_type',
                    'status'  => 'required'
                    );

            // Add Room Type Validation Custom Names
            $niceNames = array(
                        'name'    => 'Name',
                        'status'  => 'Status'
                        );

            $validator = Validator::make($request->all(), $rules);
            $validator->setAttributeNames($niceNames); 

            if ($validator->fails()) 
            {
                return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
            }
            else
            {
                $room_type = new RoomType;

			    $room_type->name        = $request->name;
			    $room_type->description = $request->description;
			    $room_type->status      = $request->status;

                $room_type->save();

                $this->helper->flash_message('success', 'Added Successfully'); // Call flash message function

                return redirect('admin/room_type');
            }
        }
        else
        {
            return redirect('admin/room_type');
        }
    }

    /**
     * Update Room Type Details
     *
     * @param array $request    Input values
     * @return redirect     to Room Type View
     */
    public function update(Request $request)
    {
        if(!$_POST)
        {
            $data['result'] = RoomType::find($request->id);

            return view('admin.room_type.edit', $data);
        }
        else if($request->submit)
        {
            // Edit Room Type Validation Rules
            $rules = array(
                    'name'    => 'required|unique:room_type,name,'.$request->id,
                    'status'  => 'required'
                    );

            // Edit Room Type Validation Custom Fields Name
            $niceNames = array(
                        'name'    => 'Name',
                        'status'  => 'Status'
                        );

            $validator = Validator::make($request->all(), $rules);
            $validator->setAttributeNames($niceNames); 

            if ($validator->fails()) 
            {
                return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
            }
            else
            {
                $room_type = RoomType::find($request->id);
                $room_status=RoomType::where('id','!=',$request->id)->where('status','Active')->get();
                // echo "<pre>"; print_r($room_status); exit;
                $room_type->name        = $request->name;
                $room_type->description = $request->description;
                $c_status=count($room_status);
                if ($c_status >= "1") {
                    $status_room=$request->status; 

                }
                else
                {
                    $status_room="Active";  
                }
                $room_type->status=$status_room;

                $room_type->save();                
                if($c_status == 0)
                  {
                       $this->helper->flash_message('error', 'Atleast One Roomtype shoud be Active'); 
                   }else{
                $this->helper->flash_message('success', 'Updated Successfully'); // Call flash message function
                }
                return redirect('admin/room_type');
            }
        }
        else
        {
            return redirect('admin/room_type');
        }
    }



    //for Atleast One RoomType in "Active"...

    public function chck_status($id)
    {
        $room_status=RoomType::where('status','Active')->get();
        if(count($room_status) > "1")
        {
            echo "Active";
            exit;
        }
        else
        {
            echo "InActive";
            exit;
        }
    }

    /**
     * Delete Room Type
     *
     * @param array $request    Input values
     * @return redirect     to Room Type View
     */
    public function delete(Request $request)
    {
        $count = Rooms::where('room_type', $request->id)->count();
        $room_type_counts=RoomType::where('status','Active')->count();
        $delete_room_type_counts=RoomType::whereId($request->id)->where('status','Active')->count();
        if($count > 0)
             $this->helper->flash_message('error', 'Atleast one  Room type shoud be Active.'); // Call flash message function
        else {

             if($room_type_counts < 2)
             {
                if($delete_room_type_counts==1)
                { 
                 $this->helper->flash_message('danger', "Atleast one  Room type shoud be Active"); // Call flash message function
                 return redirect('admin/room_type');
                }
             }

            RoomType::find($request->id)->delete();
            $this->helper->flash_message('success', 'Deleted Successfully'); // Call flash message function
        }
        return redirect('admin/room_type');
    }
}

