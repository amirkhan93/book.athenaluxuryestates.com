<?php

/**
* Payment Controller
*
* @package     Makent
* @subpackage  Controller
* @category    Payment
* @author      Trioangle Product Team
* @version     1.5.2
* @link        http://trioangle.com
*/

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Omnipay\Omnipay;
use App\Models\Rooms;
use App\Models\Currency;
use App\Models\Country;
use App\Models\PaymentGateway;
use App\Models\Reservation;
use App\Models\Calendar;
use App\Models\Messages;
use App\Models\Payouts;
use App\Models\CouponCode;
use App\Models\Referrals;
use App\Models\AppliedTravelCredit;
use App\Models\HostPenalty;
use App\Models\SpecialOffer;
use App\Models\YapstonePaymentGateway;
use Validator;
use App\Http\Helper\PaymentHelper;
use App\Models\AuthrizePaymentGateway;
use App\Http\Controllers\EmailController;
use App\Http\Start\Helpers;
use DateTime;
use Session;
use Auth;
use DB;
use JWTAuth;
use Stripe;

class PaymentController extends Controller
{
  protected $omnipay; // Global variable for Omnipay instance

  protected $payment_helper; // Global variable for Helpers instance

  /**
  * Constructor to Set PaymentHelper instance in Global variable
  *
  * @param array $payment   Instance of PaymentHelper
  */
  public function __construct(PaymentHelper $payment)
  {


    $this->payment_helper = $payment;
    $this->helper = new Helpers;
  }

  /**
  * Setup the Omnipay PayPal API credentials
  *
  * @param string $gateway  PayPal Payment Gateway Method as PayPal_Express/PayPal_Pro
  * PayPal_Express for PayPal account payments, PayPal_Pro for CreditCard payments
  */
  public function setup($gateway = 'PayPal_Express')
  {
    // Get PayPal credentials from payment_gateway table
    $paypal_credentials = PaymentGateway::where('site', 'PayPal')->get();

    // Create the instance of Omnipay
    $this->omnipay  = Omnipay::create($gateway);

    $this->omnipay->setUsername($paypal_credentials[0]->value);
    $this->omnipay->setPassword($paypal_credentials[1]->value);
    $this->omnipay->setSignature($paypal_credentials[2]->value);
    $this->omnipay->setTestMode(($paypal_credentials[3]->value == 'sandbox') ? true : false);
    if($gateway == 'PayPal_Express')
    $this->omnipay->setLandingPage('Login');
  }

  /**
  * Load Payment view file
  *
  * @param $request  Input values
  * @return payment page view
  */
  public function index(Request $request)
  {
    // Already paid
    if(@$request->special_offer_id){
      $already = Reservation::where('special_offer_id',$request->special_offer_id)->where('status','Accepted')->first();
      if($already){
        $this->helper->flash_message('error', 'Already Booked');
        return redirect('trips/current');
      }
    }

    if(Session::get('get_token')!='')
    {
      $user = @JWTAuth::toUser(Session::get('get_token'));

      $mobile_web_auth_user_id=$user->id;

      Session::set('currency_symbol', Session::get('symbol')); //mobile  currency_symbol
    }
    else
    {
      $mobile_web_auth_user_id=@Auth::user()->user()->id;
    }

    $special_offer_id = '';

    $s_key = '';

    $login_user_id = $mobile_web_auth_user_id;
    if($_POST)
    {
      // to prevent host book their own list
      if($request->id)
      {
        $user_id = Rooms::find($request->id)->user_id;
        if($user_id == @$mobile_web_auth_user_id)
        {
          return redirect('rooms/'.$request->id);
        }
      }
      // to prevent host book their own list

      $lur_id =$login_user_id.$request->id;

      $i = 0;
      $s_key = $lur_id.$i;

      if(Session::has('payment')){
        $session_key = array_keys(Session::get('payment'));
        $count= count($session_key);
        $end = end($session_key);

        if(Session::has('payment.'.$s_key)){

          $last_char =  mb_substr($end, -1) + 1;
          $s_key = $lur_id.$last_char;
        }
        else
        {
          if(is_array($session_key))
          {
            $last_char =  mb_substr($end, -1) + 1;
            $s_key = $lur_id.$last_char;
          }
          else
          {
            $s_key = $s_key;
          }

        }

      }

      $payment = array(
        'payment_room_id' => $request->id,
        'payment_checkin' => $request->checkin,
        'payment_checkout' => $request->checkout,
        'payment_number_of_guests' => $request->number_of_guests,
        'payment_booking_type' => $request->booking_type,
        'payment_special_offer_id' => @$request->special_offer_id,
        'payment_reservation_id' => $request->reservation_id,
        'payment_cancellation' => $request->cancellation,
      );

      Session::put('payment.'.$s_key,$payment);

      $id               = Session::get('payment')[$s_key]['payment_room_id'];
      $checkin          = Session::get('payment')[$s_key]['payment_checkin'];
      $checkout         = Session::get('payment')[$s_key]['payment_checkout'];
      $number_of_guests = Session::get('payment')[$s_key]['payment_number_of_guests'];
      $booking_type     = Session::get('payment')[$s_key]['payment_booking_type'];
      $reservation_id   = Session::get('payment')[$s_key]['payment_reservation_id'];
      $special_offer_id = @Session::get('payment')[$s_key]['payment_special_offer_id'];
      $cancellation = Session::get('payment')[$s_key]['payment_cancellation'];

    }
    elseif(Session::has('s_key'))
    {
      $s_key = Session::get('s_key');

      if(Session::get('payment')[$s_key]['payment_room_id'])
      {
        $user_id = Rooms::find(Session::get('payment')[$s_key]['payment_room_id'])->user_id;

        if($user_id == @$mobile_web_auth_user_id)
        {
          return redirect('rooms/'.Session::get('payment')[$s_key]['payment_room_id']);
        }
      }
      // to prevent host book their own list

      $id               = @Session::get('payment')[$s_key]['payment_room_id'];
      $checkin          = @Session::get('payment')[$s_key]['payment_checkin'];
      $checkout         = @Session::get('payment')[$s_key]['payment_checkout'];
      $number_of_guests = @Session::get('payment')[$s_key]['payment_number_of_guests'];
      $booking_type     = @Session::get('payment')[$s_key]['payment_booking_type'];
      $reservation_id   = @Session::get('payment')[$s_key]['payment_reservation_id'];
      $special_offer_id = @Session::get('payment')[$s_key]['payment_special_offer_id'];
      $payment_card_type   = @Session::get('payment')[$s_key]['payment_card_type'];
      $cancellation = @Session::get('payment')[$s_key]['payment_cancellation'];

    }

    else
    {
      // to prevent host book their own list
      if($request->room_id)
      {
        $user_id = Rooms::find($request->room_id)->user_id;

        if($user_id == @$mobile_web_auth_user_id)
        {
          return redirect('rooms/'.$request->id);
        }
      }


      // to prevent host book their own list
      if(@$request->room_id !='') {

        $lur_id =$login_user_id.$request->room_id;

        $i = 0;
        $s_key = $lur_id.$i;
        if(Session::has('payment')){

          $session_key = array_keys(Session::get('payment'));
          $count= count($session_key);
          $end = end($session_key);

          if(Session::has('payment.'.$s_key)){

            $last_char =  mb_substr($end, -1) + 1;
            $s_key = $lur_id.$last_char;
          }
          else
          {
            if(is_array($session_key))
            {
              $last_char =  mb_substr($end, -1) + 1;
              $s_key = $lur_id.$last_char;
            }
            else
            {
              $s_key = $s_key;
            }

          }

        }


        $payment = array(
          'payment_room_id' => $request->room_id,
          'payment_checkin' => date('d-m-Y', strtotime($request->checkin)),
          'payment_checkout' => date('d-m-Y', strtotime($request->checkout)),
          'payment_number_of_guests' => $request->number_of_guests,
          'payment_special_offer_id' => $request->special_offer_id,
          'payment_booking_type' => 'instant_book',
          'payment_reservation_id' => $request->reservation_id,
          'payment_cancellation' => $request->cancellation,
        );
        Session::put('payment.'.$s_key,$payment);
      } else if($request->reservation_id != ''){
        $reservation_get = Reservation::find($request->reservation_id);

        $lur_id =$login_user_id.$request->reservation_id;

        $i = 0;
        $s_key = $lur_id.$i;
        if(Session::has('payment')){

          $session_key = array_keys(Session::get('payment'));
          $count= count($session_key);
          $end = end($session_key);

          if(Session::has('payment.'.$s_key)){

            $last_char =  mb_substr($end, -1) + 1;
            $s_key = $lur_id.$last_char;
          }
          else
          {
            if(is_array($session_key))
            {
              $last_char =  mb_substr($end, -1) + 1;
              $s_key = $lur_id.$last_char;
            }
            else
            {
              $s_key = $s_key;
            }

          }

        }
        $payment = array(
          'payment_room_id' => $reservation_get->room_id,
          'payment_checkin' => date('d-m-Y', strtotime($reservation_get->checkin)),
          'payment_checkout' => date('d-m-Y', strtotime($reservation_get->checkout)),
          'payment_number_of_guests' => $reservation_get->number_of_guests,
          'payment_special_offer_id' => $reservation_get->special_offer_id,
          'payment_booking_type' => 'request_to_book',
          'payment_reservation_id' => $request->reservation_id,
          'payment_cancellation' => $reservation_get->cancel_policy,
        );
        Session::put('payment.'.$s_key,$payment);
      }
      else if($request->reservation_id == '')
      {
        if(Session::has('payment')) {
          $session_key = array_keys(Session::get('payment'));
          $s_key = end($session_key);
        }
      }

      $id               = @Session::get('payment')[$s_key]['payment_room_id'];
      $checkin          = @Session::get('payment')[$s_key]['payment_checkin'];
      $checkout         = @Session::get('payment')[$s_key]['payment_checkout'];
      $number_of_guests = @Session::get('payment')[$s_key]['payment_number_of_guests'];
      $special_offer_id = @Session::get('payment')[$s_key]['payment_special_offer_id'];
      $booking_type     = @Session::get('payment')[$s_key]['payment_booking_type'];
      $reservation_id   = @Session::get('payment')[$s_key]['payment_reservation_id'];
      $cancellation    = @Session::get('payment')[$s_key]['payment_cancellation'];
    }

    $data['is_split'] = false;

    if($reservation_id != '' && !Session::has('s_key'))
    {

      if($reservation_id == '')
      $reservation_id = @$request->reservation_id;

      $reservation = Reservation::where('id', $reservation_id)->where('user_id',$mobile_web_auth_user_id)->first();
      if(!$reservation)
      {
        if(Session::get('get_token')=='')
        {

          $this->helper->flash_message('error', trans('messages.rooms.dates_not_available')); // Call flash message function
          return redirect('trips/current');
        }
        else
        {

          return response()->json([

            'success_message'=>'Rooms Dates Not Available',

            'status_code'=>'0'
          ]);

        }
      }

      $lur_id =$login_user_id.$reservation->id;

      $i = 0;
      $s_key = $lur_id.$i;

      if(Session::has('payment')){

        $session_key = array_keys(Session::get('payment'));
        $count= count($session_key);
        $end = end($session_key);

        if(Session::has('payment.'.$s_key)){

          $last_char =  mb_substr($end, -1) + 1;
          $s_key = $lur_id.$last_char;
        }
        else
        {
          if(is_array($session_key))
          {
            $last_char =  mb_substr($end, -1) + 1;
            $s_key = $lur_id.$last_char;
          }
          else
          {
            $s_key = $s_key;
          }

        }

      }

      $payment = array(
        'payment_room_id' => $reservation->room_id,
        'payment_checkin' => date('d-m-Y', strtotime($reservation->checkin)),
        'payment_checkout' => date('d-m-Y', strtotime($reservation->checkout)),
        'payment_number_of_guests' => $reservation->number_of_guests,
        'payment_special_offer_id' => $reservation->special_offer_id,
        'payment_booking_type' => 'instant_book',
        'payment_reservation_id' => $reservation->id,
        'payment_cancellation' => $reservation->cancellation,
        'payment_card_type' => $reservation->paymode,
      );
      Session::put('payment.'.$s_key,$payment);

      $id               = Session::get('payment')[$s_key]['payment_room_id'];
      $checkin          = Session::get('payment')[$s_key]['payment_checkin'];
      $checkout         = Session::get('payment')[$s_key]['payment_checkout'];
      $number_of_guests = Session::get('payment')[$s_key]['payment_number_of_guests'];
      if(Session::get('payment')[$s_key]['payment_special_offer_id']){
        $special_offer_id = Session::get('payment')[$s_key]['payment_special_offer_id'];
      }else{
        $special_offer_id = $request->special_offer_id;
      }

      $booking_type     = Session::get('payment')[$s_key]['payment_booking_type'];
      if(Session::get('payment')[$s_key]['payment_reservation_id']){
        $reservation_id   = Session::get('payment')[$s_key]['payment_reservation_id'];
      }else{
        $reservation_id   = $request->reservation_id;
      }

      $cancellation    = Session::get('payment')[$s_key]['payment_cancellation'];
     $data['installment_details']='';
      if($reservation->payment_type == "Split") {
        $data['is_split'] = true;
        $installments = [];
        $installment_array = [
          [
            'pay' => $reservation->first_installment,
            'date' => 'Today'
          ],
          [
            'pay' => $reservation->second_installment,
            'date' => date('d-m-Y', strtotime($reservation->second_payment_due))
          ],
          [
            'pay' => $reservation->third_installment,
            'date' => date('d-m-Y', strtotime($reservation->third_payment_due))
          ],
          [
            'pay' => $reservation->fourth_installment,
            'date' => date('d-m-Y', strtotime($reservation->fourth_payment_due))
          ],
          [
            'pay' => $reservation->fifth_installment,
            'date' => date('d-m-Y', strtotime($reservation->fifth_payment_due))
          ]
        ];

        foreach ($installment_array as $key => $value) {
          if($value['pay'] != 0) {
            array_push($installments, $value);
          }
        }

        $payment_count = count($installments);
        $pay_number = $payment_count - $reservation->installment_status;

        for ($i=0; $i < $payment_count; $i++) {
          if($pay_number == $i && isset($installments[$i - 1])) {
            $installments[$i - 1]['date'] = 'Paid';
          }
        }

        $data['installment_details'] = [
          'installments' => $installments,
          'pay_count' => $payment_count,
          'pay_number' => $pay_number
        ];
      }

      Session::put('payment.'.$s_key.'.is_split', $data['is_split']);
      Session::put('payment.'.$s_key.'.installment_details', $data['installment_details']);

    }

    if(!$_POST && !$checkin)
    {

      return redirect('rooms/'.$request->id);
    }

    if(!$_POST && !$id)
    {
      return redirect('404');
    }


    $data['result']           = Rooms::find($id);
    // echo "<pre>";print_r($data['result']);die;
    $data['room_id']          = $id;
    $data['checkin']          = $checkin;
    $data['checkout']         = $checkout;
    $data['number_of_guests'] = $number_of_guests;
    $data['booking_type']     = $booking_type;
    $data['special_offer_id'] = ($special_offer_id != '') ? $special_offer_id : $request->special_offer_id;
    if($data['special_offer_id']!= ''  && $data['special_offer_id'] > 0)
    $data['special_offer_type'] = SpecialOffer::find($data['special_offer_id'])->type;
    else
    $data['special_offer_type'] = '';

    $data['reservation_id']   = $reservation_id;
    $data['cancellation']     = $cancellation;
    $data['s_key']            = $s_key;
    $from                     = new DateTime($checkin);
    $to                       = new DateTime($checkout);

    $data['nights']           = $to->diff($from)->format("%a");

    $travel_credit_result = Referrals::whereUserId($mobile_web_auth_user_id)->get();
    $travel_credit_friend_result = Referrals::whereFriendId($mobile_web_auth_user_id)->get();

    $travel_credit = 0;

    foreach($travel_credit_result as $row) {
      $travel_credit += $row->credited_amount;
    }

    foreach($travel_credit_friend_result as $row) {
      $travel_credit += $row->friend_credited_amount;
    }

    if($travel_credit && Session::get('remove_coupon') != 'yes' && Session::get('manual_coupon') != 'yes' && ($data['reservation_id']!='' || $data['booking_type'] == 'instant_book')) {

      Session::put('coupon_code', 'Travel_Credit');
      Session::put('coupon_amount', $travel_credit);

    }

    $data['travel_credit'] = $travel_credit;
    $data['price_list'] = json_decode($this->payment_helper->price_calculation($data['room_id'], $data['checkin'], $data['checkout'], $data['number_of_guests'], $special_offer_id, '', $reservation_id));
    //echo "<pre>";print_r($data['price_list']);die;
    Session::put('payment.'.$s_key.'.payment_price_list', $data['price_list']);

    $pending_reservation_check = Reservation::where(['room_id' => $data['room_id'],'id' => $reservation_id, 'checkin' => date('Y-m-d', strtotime($data['checkin'])), 'checkout' => date('Y-m-d', strtotime($data['checkout'])), 'user_id' => $mobile_web_auth_user_id, 'status' => 'Pending'])->get();

    if(@$data['price_list']->status == 'Not available')
    {
      $this->helper->flash_message('error', trans('messages.rooms.dates_not_available')); // Call flash message function
      return redirect('rooms/'.$id);
    }

    $data['paypal_price']        = $this->payment_helper->currency_convert($data['result']->rooms_price->code, PAYPAL_CURRENCY_CODE, $data['price_list']->total);

    $data['paypal_price_rate']       = $this->payment_helper->currency_rate($data['result']->rooms_price->currency_code, PAYPAL_CURRENCY_CODE);

    // Get First Default Currency from currency table
    $data['currency']         = Currency::where('default_currency', 1)->take(1)->get();
    $data['country']          = Country::all()->lists('long_name', 'short_name');

    Session::save();

    return view('payment.payment', $data);
  }

  /**
  * Pre Accept send to Host
  *
  * @param array $request Input values
  * @return redirect to Rooms Detail page
  */
  public function pre_accept(Request $request, EmailController $email_controller)
  {
    if($request->session_key && $request->session_key !='')
    {
      if(Session::get('get_token')!='')
      {
        $user = JWTAuth::toUser(Session::get('get_token'));
        $mobile_web_auth_user_id=$user->id;
      }
      else
      {

        $mobile_web_auth_user_id=@Auth::user()->user()->id;
      }

      $s_key = $request->session_key;
      // to prevent host book their own list

      if(Session::get('payment')[$s_key]['payment_room_id'])
      {
        $user_id = Rooms::find(Session::get('payment')[$s_key]['payment_room_id'])->user_id;
        if($user_id == @$mobile_web_auth_user_id)
        {
          return redirect('rooms/'.Session::get('payment')[$s_key]['payment_room_id']);
        }
      }
      // to prevent host book their own list
      $data['price_list']       = json_decode($this->payment_helper->price_calculation($request->room_id, $request->checkin, $request->checkout, $request->number_of_guests,$request->special_offer_id));
      if(@$data['price_list']->status == 'Not available')
      {
        $this->helper->flash_message('error', trans('messages.rooms.dates_not_available')); // Call flash message function
        return redirect('rooms/'.$request->id);
      }

      //session and request value are equal or not

      $room_id            =@Session::get('payment')[$s_key]['payment_room_id'];
      $payment_checkin    =@Session::get('payment')[$s_key]['payment_checkin'];
      $payment_checkout   =@Session::get('payment')[$s_key]['payment_checkout'];
      $number_of_guests   =@Session::get('payment')[$s_key]['payment_number_of_guests'];
      $cancellation      =@Session::get('payment')[$s_key]['payment_cancellation'];
      $rooms = Rooms::find($room_id);

      $reservation = new Reservation;
      $reservation->room_id          = $room_id;
      $reservation->host_id          = Rooms::find($room_id)->user_id;
      $reservation->user_id          = $mobile_web_auth_user_id;
      $reservation->checkin          = $this->payment_helper->date_convert($payment_checkin);
      $reservation->checkout         = $this->payment_helper->date_convert($payment_checkout);
      $reservation->number_of_guests = $number_of_guests;
      $reservation->nights           = $data['price_list']->total_nights;
      $reservation->per_night        = $data['price_list']->rooms_price;
      $reservation->subtotal         = $data['price_list']->subtotal;
      $reservation->cleaning         = $data['price_list']->cleaning_fee;
      $reservation->additional_guest = $data['price_list']->additional_guest;
      $reservation->security         = $data['price_list']->security_fee;
      $reservation->service          = $data['price_list']->service_fee;
      $reservation->host_fee         = $data['price_list']->host_fee;
      $reservation->tax_fee          = $data['price_list']->tax_fee;
      if($data['price_list']->installmentType==1){
        $reservation->first_installment    = $data['price_list']->installmentpay;
        $reservation->total    = $data['price_list']->total;
      }else if($data['price_list']->installmentType==2){
        $reservation->second_installment    = $data['price_list']->installmentpay;
        $reservation->total    = $data['price_list']->total;
      }else if($data['price_list']->installmentType==3){
        $reservation->third_installment    = $data['price_list']->installmentpay;
        $reservation->total    = $data['price_list']->total;
      }else if($data['price_list']->installmentType==4){
        $reservation->fourth_installment    = $data['price_list']->installmentpay;
        $reservation->total    = $data['price_list']->total;
      }else if($data['price_list']->installmentType==5){
        $reservation->fifth_installment    = $data['price_list']->installmentpay;
        $reservation->total    = $data['price_list']->total;
      }else{
        $reservation->total    = $data['price_list']->installmentpay;
      }
      //$reservation->installment_status = $data['price_list']->installmentType;
      $reservation->currency_code    = $data['price_list']->currency;
      $reservation->type             = 'reservation';
      $reservation->status           = 'Pending';
      $reservation->cancellation     = $cancellation;
      $reservation->country          = @Session::get('payment.'.$s_key.'.mobile_payment_counry_code')==''
      ? 'US': Session::get('payment.'.$s_key.'.mobile_payment_counry_code');//'US'; mobile change
      $reservation->paymode          = @Session::get('payment.'.$s_key.'.payment_card_type');//mobile change

      $reservation->save();
      $reservationId = $reservation->id;

      $replacement = "[removed]";

      $dots=".*\..*\..*";

      $email_pattern = "/[^@\s]*@[^@\s]*\.[^@\s]*/";
      $url_pattern = "/[a-zA-Z]*[:\/\/]*[A-Za-z0-9\-_]+\.+[A-Za-z0-9\.\/%&=\?\-_]+/i";
      $phone_pattern = "/\+?[0-9][0-9()\s+]{4,20}[0-9]/";

      $find = array($email_pattern, $phone_pattern);
      $replace = array($replacement, $replacement);

      $question = preg_replace($find, $replace,$request->message_to_host);

      if($question==$dots)
      {
        $question = preg_replace($url_pattern, $replacement, $question);
      }
      else{
        $question = preg_replace($find, $replace,$request->message_to_host);
      }

      $message = new Messages;

      $message->room_id        = $room_id;
      $message->reservation_id = $reservationId;
      $message->user_to        = $rooms->user_id;
      $message->user_from      = $mobile_web_auth_user_id;
      $message->message        = $question;
      $message->message_type   = 1;
      $message->read           = 0;

      $message->save();
      $email_controller->inquiry($reservationId, $question);


      if(Session::get('get_token')!='')
      {
        $result=array('success_message'=>'Request Booking Send to Host','status_code'=>'1');
        return view('json_response.json_response',array('result' =>json_encode($result)));
      }

      //end mobile changes
      $this->helper->flash_message('success', trans('messages.rooms.pre-accept_request',['first_name'=>$rooms->users->first_name])); // Call flash message function

      Session::forget('s_key');
      // Session::forget('payment.'.$s_key);
      Session::forget('payment');

      return redirect('trips/current');

    }
    else
    {
      if(empty(Session::get('payment'))) return redirect('404');
      $session_key = array_keys(Session::get('payment'));
      $s_key = end($session_key);

      Session::put('s_key',$s_key);

      return redirect('payments/book/'.Session::get('payment')[$s_key]['payment_room_id']);
    }
  }

  /**
  * Appy Coupen Code Function
  *
  * @param array $request    Input values
  * @return redirect to Payemnt Page
  */
  public function apply_coupon(Request $request)
  {
    $coupon_code      = $request->coupon_code;
    $s_key            = $request->s_key;
    $result           = CouponCode::where('coupon_code', $coupon_code)->where('status','Active')->get();
    $interval         = "Check_Expired_coupon";

    if($result->count())
    {
      $datetime1 = new DateTime();
      $datetime2 = new DateTime($result[0]->expired_at);

      if($datetime1 < $datetime2)
      {
        $interval_diff = $datetime1->diff($datetime2);
        if($interval_diff->days)
        $interval      = $interval_diff->days;
        else
        $interval      = $interval_diff->h;            }
        else
        {
          $interval = "Expired_coupon";
        }
      }
      else
      $interval = "Check_Expired_coupon";

      if($interval != "Expired_coupon" && $interval != "Check_Expired_coupon")
      {
        $id               = Session::get('payment')[$s_key]['payment_room_id'];
        $price_list       = Session::get('payment')[$s_key]['payment_price_list'];
        $code             = Session::get('currency');

        $data['coupon_amount']  = $this->payment_helper->currency_convert($result[0]->currency_code,$code,$result[0]->amount);
        //echo "<pre>";print_r($price_list);die;
        if($price_list->total > $data['coupon_amount'])
        {
          $data['coupen_applied_total']  = $price_list->total - $data['coupon_amount'];

          Session::forget('coupon_code');
          Session::forget('coupon_amount');
          Session::forget('remove_coupon');
          Session::forget('manual_coupon');
          Session::put('coupon_code', $coupon_code);
          Session::put('coupon_amount', $data['coupon_amount']);
          Session::put('manual_coupon', 'yes');
        }
        else
        {
          $data['message']  = trans('messages.payments.big_coupon'); ;
        }
      }
      else
      {
        if($interval == "Expired_coupon")
        {
          $data['message']  = trans('messages.payments.expired_coupon');
        }
        else
        {
          $data['message']  = trans('messages.payments.invalid_coupon');
        }
      }

      return json_encode($data);
    }

    public function remove_coupon(Request $request)
    {
      Session::forget('coupon_code');
      Session::forget('coupon_amount');
      Session::forget('manual_coupon');
      Session::put('remove_coupon', 'yes');
    }

    /**
    * Payment Submit Function
    *
    * @param array $request    Input values
    * @return redirect to Dashboard Page
    */

    public function create_booking(Request $request)
    {

      if(isset($request->session_key) && $request->session_key !='')
      {
        $s_key = $request->session_key;

        if(Session::get('get_token')!='')
        {
          $user = JWTAuth::toUser(Session::get('get_token'));

          $mobile_web_auth_user_id=$user->id;
        }
        else
        {
          $mobile_web_auth_user_id=@Auth::user()->user()->id;
        }

        $reservation_id = $i_id=@Session::get('payment')[$s_key]['payment_reservation_id'];
        $room_id= $request->room_id;
        $checkin= $request->checkin;
        $checkout=$request->checkout;
        $date_from = strtotime($checkin);
        $date_to = strtotime($checkout);
        $date_ar=array();
        for ($i=$date_from; $i<=$date_to - 1; $i+=86400) {
          $date_ar[]= date("Y-m-d", $i).'<br />';
        }
        $check=array();
        for ($i=0; $i < count($date_ar) ; $i++)
        {
          $check[]=DB::table('calendar')->where([ 'room_id' => $room_id, 'date' => $date_ar[$i], 'status' => 'Not available' ])->get();
        }
        if(count(array_filter($check)) != 0)
        {
          $chck_reservation=Reservation::where(['room_id'=>$room_id,'checkin'=>$checkin, 'checkout'=>$checkout])->where('status','!=','Accepted')->get();
          $count_reservation=count($chck_reservation);
          if($count_reservation > 0)
          {
            foreach ($chck_reservation as $result)
            {
              Reservation::where('id',$result->id)->update(['date_check' => 'No']);
            }
          }
          return redirect('trips/current');
        }
        else
        {
          // to prevent host book their own list

          if(Session::get('payment')[$s_key]['payment_room_id'])
          {
            $user_id = Rooms::find(Session::get('payment')[$s_key]['payment_room_id'])->user_id;

            //if($user_id == @Auth::user()->user()->id)
            if($user_id == @$mobile_web_auth_user_id)
            {
              return redirect('rooms/'.Session::get('payment')[$s_key]['payment_room_id']);
            }
          }
          // to prevent host book their own list

          // Get PayPal credentials from payment_gateway table
          $paypal_credentials = PaymentGateway::where('site', 'PayPal')->get();

          $price_list     = json_decode($this->payment_helper->price_calculation($request->room_id, $request->checkin, $request->checkout, $request->number_of_guests,$request->special_offer_id, '', $reservation_id));
          $amount         = $this->payment_helper->currency_convert($request->currency, PAYPAL_CURRENCY_CODE, $price_list->total);

          $country = $request->payment_country;

          $message_to_host = $request->message_to_host;

          $room_id            =   Session::get('payment')[$s_key]['payment_room_id'];
          $checkin            =   Session::get('payment')[$s_key]['payment_checkin'];
          $checkout           =   Session::get('payment')[$s_key]['payment_checkout'];
          $number_of_guests   =   Session::get('payment')[$s_key]['payment_number_of_guests'];
          $reservation_id     =   @Session::get('payment')[$s_key]['payment_reservation_id'];
          $room               =   Rooms::find($room_id);

          $payment_description=   $room->name.' '.$checkin.' - '.$checkout;
          //mobile /web redirect
          if(Session::get('get_token')!='')
          {
            $purchaseData   =   [
              'testMode'  => ($paypal_credentials[3]->value == 'sandbox') ? true : false,
              'amount'    => $amount,
              'description' => $payment_description,
              'currency'  => PAYPAL_CURRENCY_CODE,
              'returnUrl' => url('api_payments/success?s_key='.$s_key),
              'cancelUrl' => url('api_payments/cancel?s_key='.$s_key),
            ];
          }
          else
          {

            $purchaseData   =   [
              'testMode'  => ($paypal_credentials[3]->value == 'sandbox') ? true : false,
              'amount'    => $amount,
              'description' => $payment_description,
              'currency'  => PAYPAL_CURRENCY_CODE,
              'returnUrl' => url('payments/success?s_key='.$s_key),
              'cancelUrl' => url('payments/cancel?s_key='.$s_key),
            ];
          }


          Session::put('payment.'.$s_key.'.amount', $amount);
          Session::put('payment.'.$s_key.'.payment_country', $country);
          Session::put('payment.'.$s_key.'.message_to_host_'.$mobile_web_auth_user_id, $message_to_host);

          Session::save();

          if(Session::get('payment.'.$s_key.'.payment_card_type')!='')
          {
            if($request->payment_type =='cc')
            {
              Session::put('payment.'.$s_key.'.payment_card_type','Credit Card');
            }
            else
            {
              Session::put('payment.'.$s_key.'.payment_card_type','PayPal');
            }
          }

          //if($request->payment_method == 'cc')
          if($request->payment_type =='cc')
          {

            $rules =    [
              'cc_number'        => 'required|numeric|digits_between:12,20|validateluhn',
              'cc_expire_month'  => 'required|expires:cc_expire_month,cc_expire_year',
              'cc_expire_year'   => 'required|expires:cc_expire_month,cc_expire_year',
              'cc_security_code' => 'required|integer|digits_between:1,5',
              'first_name'       => 'required',
              'last_name'        => 'required',
              'zip'              => 'required',
            ];

            $niceNames =    [
              'cc_number'        => 'Card number',
              'cc_expire_month'  => 'Expires',
              'cc_expire_year'   => 'Expires',
              'cc_security_code' => 'Security code',
              'first_name'       => 'First name',
              'last_name'        => 'Last name',
              'zip'              => 'Postal code',
            ];

            $messages =     [
              'expires'      => 'Card has expired',
              'validateluhn' => 'Card number is invalid'
            ];

            $validator = Validator::make($request->all(), $rules, $messages);
            $validator->setAttributeNames($niceNames);

            if ($validator->fails())
            {
              return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
            }

            $this->setup('PayPal_Pro');

            $card   =   [
              'firstName'       => $request->first_name,
              'lastName'        => $request->last_name,
              'number'          => $request->cc_number,
              'expiryMonth'     => $request->cc_expire_month,
              'expiryYear'      => $request->cc_expire_year,
              'cvv'             => $request->cc_security_code,
              'billingAddress1' => $request->payment_country,
              'billingCountry'  => $request->payment_country,
              'billingCity'     => $request->payment_country,
              'billingPostcode' => $request->zip,
              'billingState'    => $request->payment_country
            ];

            $purchaseData['card'] = $card;
          }
          else
          $this->setup();

          if($amount) {
            $response = $this->omnipay->purchase($purchaseData)->send();

            // Process response
            if ($response->isSuccessful())
            {
              // Payment was successful

              $result = $response->getData();

              $data = [
                'room_id'          => $request->room_id,
                'checkin'          => $request->checkin,
                'checkout'         => $request->checkout,
                'number_of_guests' => $request->number_of_guests,
                'transaction_id'   => $result['TRANSACTIONID'],
                'price_list'       => $price_list,
                'paymode'          => 'Credit Card',
                'first_name'       => $request->first_name,
                'last_name'        => $request->last_name,
                'postal_code'      => $request->zip,
                'country'          => $request->payment_country,
                'message_to_host'  => Session::get('payment')[$s_key]['message_to_host_'.$mobile_web_auth_user_id],
                's_key'            => $s_key,
              ];

              $code = $this->store($data);
              $this->status_update($request->room_id,$request->checkin,$request->checkout);

              //mobile changes
              if(Session::get('get_token')!='')
              {
                $result=array('success_message'=>'Payment Successfully Paid','status_code'=>'1');
                return view('json_response.json_response',array('result' =>json_encode($result)));

              } //end mobile changes
              //payment success
              $this->helper->flash_message('success', trans('messages.payments.payment_success')); // Call flash message function
              return redirect('reservation/requested?code='.$code);
            }
            elseif ($response->isRedirect())
            {
              // Redirect to offsite payment gateway
              $response->redirect();
            }
            else
            {
              Session::put('s_key',$s_key);
              //payment failed for web
              if(Session::get('get_token')!='')
              {
                $result=array('success_message'=>'Payment Failed','status_code'=>'0','error', $response->getMessage());
                return view('json_response.json_response',array('result' =>json_encode($result)));

              } //end mobile changes
              // Payment failed
              $this->helper->flash_message('error', $response->getMessage()); // Call flash message function
              return redirect('payments/book/'.$request->room_id);
            }
          }
          else
          {

            $data = [
              'room_id'          => $request->room_id,
              'checkin'          => $request->checkin,
              'checkout'         => $request->checkout,
              'number_of_guests' => $request->number_of_guests,
              'transaction_id'   => '',
              'price_list'       => $price_list,
              // 'paymode'          => ($request->payment_method == 'cc') ? 'Credit Card' : 'PayPal',
              'paymode'          => ($request->payment_type == 'cc') ? 'Credit Card' : 'PayPal',
              'first_name'       => $request->first_name,
              'last_name'        => $request->last_name,
              'postal_code'      => $request->zip,
              'country'          => $request->payment_country,
              'message_to_host'  => Session::get('payment')[$s_key]['message_to_host_'.$mobile_web_auth_user_id],
              's_key'            => $s_key,
            ];

            $code = $this->store($data);
            $this->status_update($request->room_id,$request->checkin,$request->checkout);
            //payment success for mobile
            if(Session::get('get_token')!='')
            {
              $result=array('success_message'=>'Payment Successfully Paid','status_code'=>'1');
              return view('json_response.json_response',array('result' =>json_encode($result)));

            } //end mobile changes
            $this->helper->flash_message('success', trans('messages.payments.payment_success')); // Call flash message function
            return redirect('reservation/requested?code='.$code);
          }
        }
      }
      else
      {
        if(empty(Session::get('payment'))) return redirect('404');
        $session_key = array_keys(Session::get('payment'));
        $s_key = end($session_key);

        Session::put('s_key',$s_key);

        return redirect('payments/book/'.Session::get('payment')[$s_key]['payment_room_id']);
      }
    }

    /**
    * Callback function for Payment Success
    *
    * @param array $request    Input values
    * @return redirect to Payment Success Page
    */
    public function success(Request $request)
    {

      $s_key = $request->s_key;
      if(Session::get('get_token')!='')
      {
        $user = JWTAuth::toUser(Session::get('get_token'));

        $mobile_web_auth_user_id=$user->id;
      }
      else
      {
        $mobile_web_auth_user_id=@Auth::user()->user()->id;
      }

      $this->setup();

      $transaction = $this->omnipay->completePurchase(array(
        'payer_id'              => $request->PayerID,
        'transactionReference'  => $request->token,
        'amount'                => Session::get('payment')[$s_key]['amount'],
        'currency'              => PAYPAL_CURRENCY_CODE
      ));

      $response = $transaction->send();

      $result = $response->getData();


      if(@$result['ACK'] == 'Success')
      {

        $data = [
          'room_id'          => Session::get('payment')[$s_key]['payment_room_id'],
          'checkin'          => Session::get('payment')[$s_key]['payment_checkin'],
          'checkout'         => Session::get('payment')[$s_key]['payment_checkout'],
          'number_of_guests' => Session::get('payment')[$s_key]['payment_number_of_guests'],
          'transaction_id'   => @$result['PAYMENTINFO_0_TRANSACTIONID'],
          'price_list'       => Session::get('payment')[$s_key]['payment_price_list'],
          'country'          => Session::get('payment')[$s_key]['payment_country'],
          'message_to_host'  => Session::get('payment')[$s_key]['message_to_host_'.$mobile_web_auth_user_id],
          'paymode'          => 'PayPal',
          's_key'            => $s_key,
        ];

        $room_id = $data['room_id'];

        $checkin = $this->payment_helper->date_convert($data['checkin']);

        $checkout = $this->payment_helper->date_convert($data['checkout']);

        $chck_reservation=Reservation::where(['room_id'=>$room_id,'checkin'=>$checkin, 'checkout'=>$checkout, 'status'=>'Accepted'])->get();


        $count_reservation=count($chck_reservation);


        if($count_reservation > '0' )
        {
          //refund process
          $refund = $this->omnipay->refund(array(
            'payer_id'              => $request->PayerID,
            'transactionReference'  => $data['transaction_id'],
            'amount'                => Session::get('payment')[$s_key]['amount'],
            'currency'              => PAYPAL_CURRENCY_CODE
          ));
          $response = $refund->send();

          $refundresult = $response->getData();

          if(@$refundresult['ACK'] == 'Success')
          {

            $this->helper->flash_message('error', trans('messages.payments.refundpayment'));

            return redirect('trips/current');
          }
        }
        else{
          //mobile changes

          $code = $this->store($data);

          if(Session::get('get_token')!='')
          {
            $result=array('success_message'=>'Payment Successfully Paid','status_code'=>'1');

            return view('json_response.json_response',array('result' =>json_encode($result)));

          } //end mobile changes

          $this->helper->flash_message('success', trans('messages.payments.payment_success'));

          return redirect('reservation/requested?code='.$code);
        }
      }
      else
      {
        Session::put('s_key',$s_key);

        //mobile changes
        if(Session::get('get_token')!='')
        {
          if($result['L_SHORTMESSAGE0']=='Duplicate Request')
          {

            $result=array('success_message'=>'Payment Successfully Paid','status_code'=>'1');

            return view('json_response.json_response',array('result' =>json_encode($result)));

          }
          else
          {
            $result=array('success_message'=>'Payment Failed',

            'status_code'=>'0',

            'error'=>$result['L_LONGMESSAGE0']);

            return view('json_response.json_response',array('result' =>json_encode($result)));

          }
        }
        //end mobile changes

        // Payment failed
        $this->helper->flash_message('error', $result['L_SHORTMESSAGE0']); // Call flash message function

        return redirect('payments/book/'.Session::get('payment')[$s_key]['payment_room_id']);
      }
    }


    // Checking for Already Booked Or Not..

    public function status_update($room_id, $checkin, $checkout)
    {
      $chck_reservation= Reservation::where(['room_id'=>$room_id,'checkin'=>$checkin, 'checkout'=>$checkout])->where('status','!=','Accepted')->get();
      $count_reservation=count($chck_reservation);
      if($count_reservation > '0')
      {
        foreach ($chck_reservation as $result)
        {
          Reservation::where('id',$result->id)->update(['date_check' => 'No']);
        }
      }
    }

    /**
    * Callback function for Payment Failed
    *
    * @param array $request    Input values
    * @return redirect to Payments Booking Page
    */
    public function cancel(Request $request)
    {
      $s_key = $request->s_key;

      Session::put('s_key',$s_key);

      // Payment failed
      if(Session::get('get_token')!='')
      {
        $result=array('success_message' => 'The payment process was cancelled.',

        'status_code'    => '0');

        return view('json_response.json_response',array('result' =>json_encode($result)));
      }
      $this->helper->flash_message('error', trans('messages.payments.payment_cancelled')); // Call flash message function
      return redirect('payments/book/'.Session::get('payment')[$s_key]['payment_room_id']);
    }

    /**
    * Create Reservation After Payment Successfully Done
    *
    * @param array $data    Payment Data
    * @return string $code  Reservation Code
    */
    public function store($data)
    {

      $s_key = $data['s_key'];

      $special_offer_ids = @Session::get('payment')[$s_key]['payment_special_offer_id'];

      if(Session::get('get_token')!='')
      {
        $user = JWTAuth::toUser(Session::get('get_token'));

        $mobile_web_auth_user_id=$user->id;
      }
      else
      {
        $mobile_web_auth_user_id=@Auth::user()->user()->id;
      }

      if(@Session::get('payment')[$s_key]['payment_reservation_id'])
      $reservation= Reservation::find(Session::get('payment')[$s_key]['payment_reservation_id']);
      else
      $reservation = new Reservation;

      $reservation->room_id           = $data['room_id'];
      $reservation->host_id           = Rooms::find($data['room_id'])->user_id;
      $reservation->user_id           = $mobile_web_auth_user_id;
      $reservation->checkin           = $this->payment_helper->date_convert($data['checkin']);
      $reservation->checkout          = $this->payment_helper->date_convert($data['checkout']);
      $reservation->number_of_guests  = $data['number_of_guests'];
      $reservation->nights            = $data['price_list']->total_nights;
      $reservation->per_night         = $data['price_list']->rooms_price;
      $reservation->subtotal          = $data['price_list']->subtotal;
      if($data['price_list']->special_offer == '' ){
        $reservation->cleaning          = $data['price_list']->cleaning_fee;
        $reservation->additional_guest  = $data['price_list']->additional_guest;
        $reservation->security          = $data['price_list']->security_fee;
      }else{
        $reservation->cleaning          = 0;
        $reservation->additional_guest  = 0;
        $reservation->security          = 0;
      }
      $reservation->service           = $data['price_list']->service_fee;
      $reservation->host_fee          = $data['price_list']->host_fee;
      if($data['price_list']->installmentType==1){
        $reservation->first_installment    = $data['price_list']->installmentpay;
        $reservation->total    = $data['price_list']->total;
      }else if($data['price_list']->installmentType==2){
        $reservation->second_installment    = $data['price_list']->installmentpay;
        $reservation->total    = $data['price_list']->total;
      }else if($data['price_list']->installmentType==3){
        $reservation->third_installment    = $data['price_list']->installmentpay;
        $reservation->total    = $data['price_list']->total;
      }else if($data['price_list']->installmentType==4){
        $reservation->fourth_installment    = $data['price_list']->installmentpay;
        $reservation->total    = $data['price_list']->total;
      }else if($data['price_list']->installmentType==5){
        $reservation->fifth_installment    = $data['price_list']->installmentpay;
        $reservation->total    = $data['price_list']->total;
      }else{
        $reservation->total    = $data['price_list']->installmentpay;
      }
      $reservation->installment_status = $data['price_list']->installmentType;
      $reservation->currency_code     = $data['price_list']->currency;

      if($data['price_list']->coupon_amount)
      {
        $reservation->coupon_code       = $data['price_list']->coupon_code;
        $reservation->coupon_amount     = $coupon_amount = $data['price_list']->coupon_amount;
      }
      if(@Session::get('payment')[$s_key]['payment_special_offer_id'])
      {
        $reservation->special_offer_id  = $special_offer_ids;
      }

      $reservation->transaction_id    = $data['transaction_id'];
      $reservation->paymode           = $data['paymode'];
      $reservation->type              = 'reservation';

      if($data['paymode'] == 'Credit Card')
      {
        $reservation->first_name   = $data['first_name'];
        $reservation->last_name    = $data['last_name'];
        $reservation->postal_code  = $data['postal_code'];
      }

      $reservation->country          = $data['country'];
      $status = 'Accepted';
      if($reservation->payment_type == "Split" && $reservation->installment_status != 0) {
        $status = 'Split-Accepted';
      }
      $reservation->status           = (@Session::get('payment')[$s_key]['payment_booking_type'] == 'instant_book') ? $status : 'Pending';

      /*if(Session::get('payment')[$s_key]['payment_reservation_id']==''){
      $reservation->cancellation      = Rooms::find($data['room_id'])->cancel_policy;
    }*/


    $reservation->save();
    $this->status_update($reservation->room_id,$reservation->checkin,$reservation->checkout);

    if(@$data['price_list']->coupon_code == 'Travel_Credit') {
      $referral_friend = Referrals::whereFriendId($mobile_web_auth_user_id)->get();
      foreach($referral_friend as $row) {
        $friend_credit = $row->friend_credited_amount;
        if($coupon_amount != 0) {
          if($friend_credit <= $coupon_amount) {
            $referral = Referrals::find($row->id);
            $referral->friend_credited_amount = 0;
            $referral->save();
            $coupon_amount = $coupon_amount - $friend_credit;

            $applied_referral = new AppliedTravelCredit;
            $applied_referral->reservation_id = $reservation->id;
            $applied_referral->referral_id = $row->id;
            $applied_referral->amount = $friend_credit;
            $applied_referral->type = 'friend';
            $applied_referral->currency_code = $data['price_list']->currency;
            $applied_referral->save();
          }
          else {
            $referral = Referrals::find($row->id);
            $referral->friend_credited_amount = $friend_credit - $coupon_amount;
            $referral->save();

            $applied_referral = new AppliedTravelCredit;
            $applied_referral->reservation_id = $reservation->id;
            $applied_referral->referral_id = $row->id;
            $applied_referral->amount = $coupon_amount;
            $applied_referral->type = 'friend';
            $applied_referral->currency_code = $data['price_list']->currency;
            $applied_referral->save();
            $coupon_amount = 0;
          }
        }
      }
      $referral_user = Referrals::whereUserId($mobile_web_auth_user_id)->get();
      foreach($referral_user as $row) {
        $user_credit = $row->credited_amount;
        if($coupon_amount != 0) {
          if($user_credit <= $coupon_amount) {
            $referral = Referrals::find($row->id);
            $referral->credited_amount = 0;
            $referral->save();
            $coupon_amount = $coupon_amount - $user_credit;

            $applied_referral = new AppliedTravelCredit;
            $applied_referral->reservation_id = $reservation->id;
            $applied_referral->referral_id = $row->id;
            $applied_referral->amount = $user_credit;
            $applied_referral->type = 'main';
            $applied_referral->currency_code = $data['price_list']->currency;
            $applied_referral->save();
          }
          else {
            $referral = Referrals::find($row->id);
            $referral->credited_amount = $user_credit - $coupon_amount;
            $referral->save();

            $applied_referral = new AppliedTravelCredit;
            $applied_referral->reservation_id = $reservation->id;
            $applied_referral->referral_id = $row->id;
            $applied_referral->amount = $coupon_amount;
            $applied_referral->type = 'main';
            $applied_referral->currency_code = $data['price_list']->currency;
            $applied_referral->save();
            $coupon_amount = 0;
          }
        }
      }
    }

    do
    {
      $code = $this->getCode(6, $reservation->id);
      $check_code = Reservation::where('code', $code)->get();
    }
    while(empty($check_code));

    $reservation_code = Reservation::find($reservation->id);

    $reservation_code->code = $code;
    if($reservation_code->payment_type != "Split") {
      $reservation_code->installment_status =$data['price_list']->installmentType;
    }
    $reservation_code->cancellation ='Flexible';

    $reservation_code->save();

    $days = $this->get_days($data['checkin'], $data['checkout']);

    // Update Calendar
    for($j=0; $j<count($days)-1; $j++)
    {
      $calendar_data = [
        'room_id' => $data['room_id'],
        'date'    => $days[$j],
        'status'  => 'Not available'
      ];

      Calendar::updateOrCreate(['room_id' => $data['room_id'], 'date' => $days[$j]], $calendar_data);
    }

    if($reservation_code->status == 'Accepted' || $reservation_code->status == 'Split-Accepted')
    {

      $reservation_details = Reservation::find($reservation_code->id);

      $penalty = HostPenalty::where('user_id',$reservation_code->host_id)->where('remain_amount','!=',0)->get();

      $penalty_result = $this->payment_helper->check_host_penalty($penalty,$reservation_details->host_payout,$reservation_details->currency_code);

      $host_amount    = $penalty_result['host_amount'];

      $penalty_id     = $penalty_result['penalty_id'];

      $penalty_amount = $penalty_result['penalty_amount'];

      $payouts = new Payouts;

      $payouts->reservation_id = $reservation_code->id;
      $payouts->room_id        = $reservation_code->room_id;
      $payouts->user_id        = $reservation_code->host_id;
      $payouts->user_type      = 'host';
      $payouts->amount         = $host_amount;
      $payouts->penalty_amount = $penalty_amount;
      $payouts->penalty_id     = $penalty_id;
      $payouts->currency_code  = $reservation_code->currency_code;
      $payouts->status         = 'Future';

      $payouts->save();
    }

    $message = new Messages;
    $messages='';
    if(@$data['message_to_host'])
    $messages = $this->helper->phone_email_remove($data['message_to_host']);

    $message->room_id        = $data['room_id'];
    $message->reservation_id = $reservation->id;
    $message->user_to        = $reservation->host_id;
    $message->user_from      = $reservation->user_id;
    $message->message        = $messages;
    $message->message_type   = 2;
    $message->read           = 0;

    $message->save();

    $email_controller = new EmailController;
    $email_controller->accepted($reservation->id);
    $email_controller->booking_confirm_host($reservation->id);
    $email_controller->booking_confirm_admin($reservation->id);

    Session::forget('coupon_code');
    Session::forget('coupon_amount');
    Session::forget('remove_coupon');
    Session::forget('manual_coupon');
    Session::forget('s_key');
    Session::forget('payment.'.$s_key);

    return $code;
  }

  /**
  * Get days between two dates
  *
  * @param date $sStartDate  Start Date
  * @param date $sEndDate    End Date
  * @return array $days      Between two dates
  */
  public function get_days($sStartDate, $sEndDate)
  {
    $sStartDate   = $this->payment_helper->date_convert($sStartDate);
    $sEndDate     = $this->payment_helper->date_convert($sEndDate);
    $aDays[]      = $sStartDate;
    $sCurrentDate = $sStartDate;

    while($sCurrentDate < $sEndDate)
    {
      $sCurrentDate = gmdate("Y-m-d", strtotime("+1 day", strtotime($sCurrentDate)));
      $aDays[]      = $sCurrentDate;
    }

    return $aDays;
  }

  /**
  * Generate Reservation Code
  *
  * @param date $length  Code Length
  * @param date $seed    Reservation Id
  * @return string Reservation Code
  */
  public function getCode($length, $seed)
  {
    $code = "";
    $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    $codeAlphabet.= "0123456789";

    mt_srand($seed);

    for($i=0;$i<$length;$i++) {
      $code .= $codeAlphabet[mt_rand(0,strlen($codeAlphabet)-1)];
    }

    return $code;
  }

  public function yapstone_booking(Request $request){
    //echo 121; die;
    $getPaymentDetails = YapstonePaymentGateway::get();
    // echo $getPaymentDetails[0]->value.'<br>';
    // echo $getPaymentDetails[1]->value.'<br>';
    // echo $getPaymentDetails[3]->value.'<br>';
    $s_key = $request->session_key;
    $rules =    [
      'cc_number'        => 'required|numeric|digits_between:12,20|validateluhn',
      'cc_expire_month'  => 'required|expires:cc_expire_month,cc_expire_year',
      'cc_expire_year'   => 'required|expires:cc_expire_month,cc_expire_year',
      'cc_security_code' => 'required|integer|digits_between:1,5',
      'first_name'       => 'required',
      'last_name'        => 'required',
      'zip'              => 'required',
      'email'       	=> 'required',
      'phone'        	=> 'required',
      'address2'          => 'required',
      'city'              => 'required',
      'state'             => 'required',
    ];

    $niceNames =    [
      'cc_number'        => 'Card number',
      'cc_expire_month'  => 'Expires',
      'cc_expire_year'   => 'Expires',
      'first_name'       => 'First name',
      'last_name'        => 'Last name',
      'zip'              => 'Postal code',
      'email'       	=> 'Email',
      'phone'        	=> 'Phone',
      'address2'           => 'Street',
      'city'              => 'City',
      'state'              => 'State',
    ];

    $messages =     [
      'expires'      => 'Card has expired',
      'validateluhn' => 'Card number is invalid'
    ];

    $validator = Validator::make($request->all(), $rules, $messages);
    $validator->setAttributeNames($niceNames);

    if ($validator->fails()) {
      return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
    }else {
      $mobile_web_auth_user_id=@Auth::user()->user()->id;

      $reservation_id = $i_id=@Session::get('payment')[$s_key]['payment_reservation_id'];
      $room_id= $request->room_id;
      $checkin= $request->checkin;
      $checkout=$request->checkout;
      $date_from = strtotime($checkin);
      $date_to = strtotime($checkout);
      $date_ar=array();

      $creditcard =$request->input('cc_number');
      $expiration =$request->input('cc_expire_month')."-".$request->input('cc_expire_year');
      $total  =3;
      $cvv        = $request->input('cc_security_code');
      $invoice    = substr(time(), 0, 6);
      $tax        = 0.00;
      $name = $request->input('first_name').' '.$request->input('last_name');
      $type = $request->input('cc_type');
      $city = $request->input('city');
      $street = $request->input('address2');
      $state = $request->input('state');
      $email = $request->input('email');
      $phone = $request->input('phone');
      $s_key="";

      $price_list  = json_decode($this->payment_helper->price_calculation($request->room_id, $request->checkin, $request->checkout, $request->number_of_guests,$request->special_offer_id, '', $reservation_id));
      $requestXml = '<?xml version="1.0" encoding="UTF-8"?>
      <CreditCardPayment>

      <username>7rs37rp</username>
      <password>4at9fxz</password>
      <propertyCode>YSQ4SPP507</propertyCode>
      <number>{{ $creditcard }}</number>
      <expiration>{{ $expiration }}</expiration>
      <type>{{ $type }}</type>
      <cardholder>{{ $name }}</cardholder>
      <street>{{ $street }}</street>
      <city>{{ $city }}</city>
      <state>{{ $state }}</state>
      <zip>{{ $zip }}</zip>
      <country>{{ $request->payment_country }}</country>
      <phone>{{ $phone }}</phone>
      <email>{{ $email }}</email>
      <amount>{{ $price_list->firstinstallpayment}}</amount>    //edited by arun at 1580
      <personFirstname>{{ $request->first_name }}</personFirstname>
      <personLastname>{{ $request->last_name }}</personLastname>
      <cvv>{{ $cvv }}</cvv>
      <GUID>0897sfgWU266909s689h</GUID>
      <dcSessionId>cf4cff9e0e1244c8a18c0f3b366d4e9b</dcSessionId>
      <ReturnToken>true</ReturnToken>
      </CreditCardPayment>';

      $url = "https://demo.rentpayment.com/api/2";
      $ch = curl_init($url);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
      curl_setopt($ch, CURLOPT_POSTFIELDS, $requestXml);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      $response = curl_exec($ch);
      curl_close($ch);

      $response_xml = simplexml_load_string($response);

      //print_r($response_xml);exit;
      $response_array = json_decode( json_encode($response_xml) , true);
      //print_r($response_array);exit;
      $reference_number = $response_array['referenceNumber'];
      //$reference_number = 123456;
      $amount = $response_array['amount'];
      $token = $response_array['token'];

      if (!empty($reference_number)) {
        $transaction_id = 0;
        // Get info from Authnet to store in the database

        // Do stuff with this. Most likely store it in a database.
        // Direct the user to a receipt or something similiar.
        $data = [ 'room_id'          => $request->room_id,
        'checkin'          => $request->checkin,
        'checkout'         => $request->checkout,
        'number_of_guests' => $request->number_of_guests,
        'transaction_id'   => $transaction_id,
        'price_list'       => $price_list,
        'paymode'          => 'Yapstone',
        'first_name'       => $request->first_name,
        'last_name'        => $request->last_name,
        'postal_code'      => $request->zip,
        'country'          => $request->payment_country,
        's_key'            => $s_key,
      ];
      /*  echo '<pre>';  print_r($data);die;*/

      $code = $this->store($data);

      $this->status_update($request->room_id,$request->checkin,$request->checkout);

      //payment success
      /* $this->helper->flash_message('success', trans('messages.payments.payment_success')); // Call flash message function*/

      //return redirect('/')->with('message','your payment has been successfully processed');
      $this->helper->flash_message('success', trans('messages.payments.payment_success')); // Call flash message function
      return redirect('reservation/requested?code='.$code);
    } else {
      $this->helper->flash_message('error', trans('messages.payments.payment_cancelled')); // Call flash message function
      return redirect('payments/book/'.Session::get('payment')[$s_key]['payment_room_id']);
    }
  }
}


public function stripe_booking(Request $request)
{

  $s_key = $request->session_key;
  $rules =    [
    'cc_number'        => 'required|numeric|digits_between:12,20|validateluhn',
    'cc_expire'  => 'required|expires:cc_expire',
    'cc_security_code' => 'required|integer|digits_between:1,5',
    'first_name'       => 'required',
    'last_name'        => 'required',
    'zip'              => 'required',
    'email'       	=> 'required',
    'phone'        	=> 'required',
    'address2'          => 'required',
    'city'              => 'required',
    'state'             => 'required',
  ];

  $niceNames =    [
    'cc_number'        => 'Card number',
    'cc_expire'  => 'Expires',
    'first_name'       => 'First name',
    'last_name'        => 'Last name',
    'zip'              => 'Postal code',
    'email'       	=> 'Email',
    'phone'        	=> 'Phone',
    'address2'           => 'Street',
    'city'              => 'City',
    'state'              => 'State',
  ];

  $messages =     [
    'expires'      => 'Card has expired',
    'validateluhn' => 'Card number is invalid'
  ];


  $validator = Validator::make($request->all(), $rules, $messages);
  $validator->setAttributeNames($niceNames);

  if ($validator->fails()) {
    return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
  }else {
    $mobile_web_auth_user_id=@Auth::user()->user()->id;


    $reservation_id = $i_id=@Session::get('payment')[$s_key]['payment_reservation_id'];
    $price_list  = json_decode($this->payment_helper->price_calculation($request->room_id, $request->checkin, $request->checkout, $request->number_of_guests,$request->special_offer_id, '', $reservation_id));

    $reservation = Reservation::find($reservation_id);

    $amount = $price_list->total;

    if($reservation->payment_type == "Split") {
      $installments = [];
      $installment_array = [
        $reservation->first_installment,
        $reservation->second_installment,
        $reservation->third_installment,
        $reservation->fourth_installment,
        $reservation->fifth_installment
      ];

      foreach ($installment_array as $key => $value) {
        if($value != 0) {
          array_push($installments, $value);
        }
      }
      $payment_count = count($installments);
      $pay_number = $payment_count - $reservation->installment_status;

      $amount = $installments[$pay_number];

    }
    try {

      Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
      Stripe\Charge::create ([
        "amount" => $price_list->total * 100,
        "currency" => "usd",
        "source" => $request->stripeToken,
        "description" => "Test payment from itsolutionstuff.com."
      ]);

      $transaction_id = 0;
      // Get info from Authnet to store in the database

      // Do stuff with this. Most likely store it in a database.
      // Direct the user to a receipt or something similiar.
      $data = [
        'room_id'          => $request->room_id,
        'checkin'          => $request->checkin,
        'checkout'         => $request->checkout,
        'number_of_guests' => $request->number_of_guests,
        'transaction_id'   => $transaction_id,
        'price_list'       => $price_list,
        'paymode'          => 'Stripe',
        'first_name'       => $request->first_name,
        'last_name'        => $request->last_name,
        'postal_code'      => $request->zip,
        'country'          => $request->payment_country,
        's_key'            => $s_key,
      ];
    /*  echo '<pre>';  print_r($data);die;*/

    $code = $this->store($data);
    $this->status_update($request->room_id,$request->checkin,$request->checkout);
    $reservation->installment_status -= 1;
    $reservation->save();

    //payment success
    /* $this->helper->flash_message('success', trans('messages.payments.payment_success')); // Call flash message function*/

    //return redirect('/')->with('message','your payment has been successfully processed');
    $this->helper->flash_message('success', trans('messages.payments.payment_success')); // Call flash message function
    return redirect('reservation/requested?code='.$code);
  } catch (\Throwable $th) {
    $this->helper->flash_message('error', trans('messages.payments.payment_cancelled')); // Call flash message function
    return redirect('payments/book/'.Session::get('payment')[$s_key]['payment_room_id']);
  }
}
}

public function authorize_booking(Request $request){

  $s_key = $request->session_key;

  $getPaymentDetails = AuthrizePaymentGateway::get();

  $card_expiration = $request->input('cc_expire_month').$request->input('cc_expire_year');
  $reservation_id = $i_id=@Session::get('payment')[$s_key]['payment_reservation_id'];
  $price_list     = json_decode($this->payment_helper->price_calculation($request->room_id, $request->checkin, $request->checkout, $request->number_of_guests,$request->special_offer_id, '', $reservation_id));

  $amount = $price_list->installmentpay;

  $x_login  	 = $getPaymentDetails[0]->value;
  $x_tran_key  = $getPaymentDetails[1]->value;
  $card_number = $request->input('cc_number');

  $invoice_num = substr(time(), 0, 6);

  $x_card_code = $request->input('cc_security_code');

  $post_values['x_invoice_num'] = $invoice_num;
  $post_values['x_login'] 		 = $x_login;
  $post_values['x_tran_key'] 	 = $x_tran_key;

  $post_values['x_card_code'] 	 = $x_card_code;

  $post_values['x_version'] 	= "3.1";
  $post_values['x_delim_data'] = "TRUE";
  $post_values['x_delim_char'] = "|";
  $post_values['x_relay_response'] = "TRUE";

  $post_values['x_type'] 	= "AUTH_CAPTURE"; //Optional
  $post_values['x_method'] = "CC";
  $post_values['x_card_num'] = $card_number;
  $post_values['x_exp_date'] = $card_expiration;
  $post_values['x_amount']   =  $amount;

  $post_values['x_cust_id']   =  @Auth::user()->user()->id;
  $post_values['x_email']   =  @Auth::user()->user()->email;
  $post_values['x_email_customer']   =  "True";


  $post_values['x_first_name'] = @Auth::user()->user()->first_name; //Optional (From Client)
  $post_values['x_last_name'] = @Auth::user()->user()->last_name; //Optional (From Client)
  $post_values['x_zip'] = $request->input('zip');
  $post_values['x_description'] = "Set Property Name";
  if($getPaymentDetails[2]->value == 'sandbox'){
    $post_url = "https://test.authorize.net/gateway/transact.dll"; // For Sandbox
  }else{
    $post_url = "https://secure.authorize.net/gateway/transact.dll"; // For Live
  }



  // This section takes the input fields and converts them to the proper format

  $post_string = "";
  foreach( $post_values as $key => $value )
  {
    $post_string .= "$key=" . urlencode( $value ) . "&";
  }
  $post_string = rtrim( $post_string, "& " );

  $requests = curl_init($post_url);

  curl_setopt($requests, CURLOPT_HEADER, 0);

  curl_setopt($requests, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($requests, CURLOPT_POSTFIELDS, $post_string);
  curl_setopt($requests, CURLOPT_SSL_VERIFYPEER, FALSE);
  $post_response = curl_exec($requests);

  curl_close ($requests);

  $response_array = explode($post_values["x_delim_char"],$post_response);

  // You can get transaction ID at index 6

  if($response_array[0]==1 && $response_array[1]==1 && $response_array[2]==1)
  {
    $data = ['room_id'     => $request->room_id,
    'checkin'          => $request['checkin'],
    'checkout'         => $request['checkout'],
    'number_of_guests' => $request['number_of_guests'],
    'transaction_id'   => $response_array[6],
    'price_list'       => $price_list,
    'paymode'          => 'Authorize Card',
    'first_name'       => $request['first_name'],
    'last_name'        => $request['last_name'],
    'postal_code'      => $request['zip'],
    'country'          => $request['payment_country'],
    's_key'            => $s_key,
  ];

  $code = $this->store($data);

  //$code = $this->storeAuthRize($request->room_id,$request->checkin,$request->checkout,$request->number_of_guests,$response_array[6],$price_list,$request->first_name,$request->last_name,$request->zip,$request->payment_country,$s_key);
  $this->status_update($request['room_id'],$request['checkin'],$request['checkout']);
  $this->helper->flash_message('success', trans('messages.payments.payment_success')); // Call flash message function
  return redirect('reservation/requested?code='.$code);
}else{

  $this->helper->flash_message('error', trans('messages.payments.payment_cancelled')); // Call flash message function
  return redirect('payments/book/'.Session::get('payment')[$s_key]['payment_room_id']);

}



}

}
