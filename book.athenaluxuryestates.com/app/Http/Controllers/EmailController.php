<?php

/**
 * Email Controller
 *
 * @package     Makent
 * @subpackage  Controller
 * @category    Email
 * @author      Trioangle Product Team
 * @version     1.5.2
 * @link        http://trioangle.com
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Mail;
use Config;
use Auth;
use DateTime;
use DateTimeZone;
use App\Models\PasswordResets;
use App\Models\User;
use App\Models\Rooms;
use App\Models\Reservation;
use App\Models\SiteSettings;
use App\Models\PayoutPreferences;
use App\Models\ReferralSettings;
use App\Models\Currency;
use App\Models\Reviews;
use App\Models\Admin;
use App;
use JWTAuth;

class EmailController extends Controller
{
    /**
     * Send Welcome Mail to Users with Confirmation Link
     *
     * @param array $user  User Details
     * @return true
     */
    public function welcome_email_confirmation($user)
    {
        $data['first_name'] = $user->first_name;
        $data['email'] = $user->email;
        $data['token'] = str_random(100); // Generate random string values - limit 100
        $data['type'] = 'welcome';
        $data['url'] = url().'/';
        $data['locale']       = App::getLocale();

        $password_resets = new PasswordResets;

        $password_resets->email      = $user->email;
        $password_resets->token      = $data['token'];
        $password_resets->created_at = date('Y-m-d H:i:s');

        $password_resets->save(); // Insert a generated token and email in password_resets table

        $data['subject'] = trans('messages.email.confirm_email_address');

        // Send Forgot password email to give user email
        /*Mail::queue('emails.email_confirm', $data, function($message) use($data) {
            $message->to($data['email'], $data['first_name'])->subject($data['subject']);
        });*/


    	Mail::send('emails.email_confirm', $data, function($message) use($data){
         $message->to($data['email'], $data['first_name'])->subject
            ($data['subject']);
      	});

        return true;
    }


    /**
     * Send Welcome Mail to Users with Confirmation Link
     *
     * @param array $user  User Details
     * @return true
     */
    public function contact_email_confirmation($user_contact)
    {
        $admin = Admin::where('id','1')->first();
        // echo "<pre>";
        // print_r($admin);die;
        $data['admin_email'] = $admin->email;
        $data['admin_name'] =  'Admin';

        $data['contact_name'] = $user_contact->name;
        $data['contact_email'] = $user_contact->email;
        $data['contact_feedback'] = $user_contact->feedback;
        $data['url'] = url().'/';
        $data['locale']       = App::getLocale();
        // Send Contact email to admin mail
        $data['subject'] = trans('messages.email.contact_us_email');
        
        Mail::send('emails.email_contact', $data, function($message) use($data) {
            $message->to($data['admin_email'], $data['admin_name'])->subject($data['subject']);
        });

        return true;
    }

    /**
     * Send Forgot Password Mail with Confirmation Link
     *
     * @param array $user  User Details
     * @return true
     */
    public function forgot_password($user)
    {
      //dd($user->first_name);
        $data['first_name'] = $user->first_name;

        $data['token'] = str_random(100); // Generate random string values - limit 100
        $data['url'] = url().'/';
        $data['locale']       = App::getLocale();

        $password_resets = new PasswordResets;

        $password_resets->email      = $user->email;
        $password_resets->token      = $data['token'];
        $password_resets->created_at = date('Y-m-d H:i:s');

        $password_resets->save(); // Insert a generated token and email in password_resets table

        // Send Forgot password email to give user email
        $data['subject'] = trans('messages.email.reset_your_pass',[], null,  $data['locale']);
        Mail::send('emails.forgot_password', $data, function($message) use($user, $data) {
            $message->to($user->email, $user->first_name)->subject($data['subject']);
        });

        return true;
    }

    /**
     * Send Email Change Mail with Confirmation Link
     *
     * @param array $user  User Details
     * @return true
     */
    public function change_email_confirmation($user)
    {
        $data['first_name'] = $user->first_name;
        $data['token'] = str_random(100); // Generate random string values - limit 100
        $data['type'] = 'change';
        $data['url'] = url().'/';
        $data['locale']       = App::getLocale();

        $password_resets = new PasswordResets;

        $password_resets->email      = $user->email;
        $password_resets->token      = $data['token'];
        $password_resets->created_at = date('Y-m-d H:i:s');

        $password_resets->save(); // Insert a generated token and email in password_resets table

        // Send Forgot password email to give user email
        $data['subject']=trans('messages.email.confirm_email_address');
        Mail::send('emails.email_confirm', $data, function($message) use($user,$data) {
            $message->to($user->email, $user->first_name)->subject($data['subject']);
        });

        return true;
    }

    /**
     * Send New Email Change Mail with Confirmation Link
     *
     * @param array $user  User Details
     * @return true
     */
    public function new_email_confirmation($user)
    {
        $data['first_name'] = $user->first_name;
        $data['token'] = str_random(100); // Generate random string values - limit 100
        $data['type'] = 'confirm';
        $data['url'] = url().'/';
        $data['locale']       = App::getLocale();

        $password_resets = new PasswordResets;

        $password_resets->email      = $user->email;
        $password_resets->token      = $data['token'];
        $password_resets->created_at = date('Y-m-d H:i:s');

        $password_resets->save(); // Insert a generated token and email in password_resets table

        // Send Forgot password email to give user email
        $data['subject']=trans('messages.email.confirm_email_address');
        Mail::send('emails.email_confirm', $data, function($message) use($user,$data) {
            $message->to($user->email, $user->first_name)->subject($data['subject']);

        });
        return true;
    }

    /**
     * Send Inquiry Mail to Host
     *
     * @param array $reservation_id Contact Request Details
     * @return true
     */
    public function inquiry($reservation_id, $question)
    {
        $data['result'] = Reservation::find($reservation_id);
        $data['question'] = $question;
        $user = $data['result']->host_users;
        $data['url'] = url().'/';
        $data['locale']       = App::getLocale();

        $data['result'] = Reservation::where('reservation.id', $reservation_id)->with(['users' => function($query) {
                $query->with('profile_picture')->with('users_verification')->with('reviews');
            }, 'rooms', 'host_users' => function($query) {
                $query->with('profile_picture')->with('users_verification')->with('reviews');
            }, 'currency']);

        $data['result'] = $data['result']->first()->toArray();

        $data['subject'] = trans('messages.email.inquiry_at').' '.$data['result']['rooms']['name'].' '.trans('messages.email.for').' '.$data['result']['dates_subject'];
        Mail::send('emails.inquiry', $data, function($message) use($user, $data) {
            $message->to($user->email, $user->first_name)->subject($data['subject']);
        });
        return true;
    }

    /**
     * Send Booking Mail to Host
     *
     * @param array $reservation_id Request Details
     * @return true
     */
    public function booking($reservation_id)
    {
        $data['result'] = Reservation::find($reservation_id);
        $user = $data['result']->host_users;
        $data['hide_header'] = true;
        $data['url'] = url().'/';
        $data['locale']       = App::getLocale();

        $data['result'] = Reservation::where('reservation.id', $reservation_id)->with(['users' => function($query) {
                $query->with('profile_picture')->with('users_verification')->with('reviews');
            }, 'rooms', 'host_users' => function($query) {
                $query->with('profile_picture')->with('users_verification')->with('reviews');
            }, 'currency', 'messages']);

        $data['result'] = $data['result']->first()->toArray();

        $data['subject'] = trans('messages.email.booking_inquiry_for').' '.$data['result']['rooms']['name'].' '.trans('messages.email.for').' '.$data['result']['dates_subject'];
        Mail::send('emails.booking', $data, function($message) use($user, $data) {
            $message->to($user->email, $user->first_name)->subject($data['subject']);
        });
        return true;
    }

    /**
     * Send itinerary Mail to Host
     *
     * @param string $reservation_id Reservation Code
     * @param string $email Friend Email
     * @return true
     */
    public function itinerary($code, $email)
    {
        $data['result'] = Reservation::where('code', $code)->first();
        $user = $data['result']->host_users;
        $data['hide_header'] = true;
        $data['email'] = $email;
        $data['url'] = url().'/';
        $data['map_key'] = MAP_KEY;
        $data['locale']       = App::getLocale();

        $data['result'] = Reservation::where('reservation.id', $data['result']->id)->with(['users' => function($query) {
                $query->with('profile_picture')->with('users_verification')->with('reviews');
            }, 'rooms' => function($query) {
                $query->with('rooms_address');
            }, 'host_users' => function($query) {
                $query->with('profile_picture')->with('users_verification')->with('reviews');
            }, 'currency']);

        $data['result'] = $data['result']->first()->toArray();
        $data['subject'] = trans('messages.email.reservation_itinerary_from').' '.$data['result']['users']['full_name'];
        Mail::send('emails.itinerary', $data, function($message) use($user, $data) {
            $message->to($data['email'], '')->subject($data['subject']);
        });
        return true;
    }

    /**
     * Send preapproval Mail to Host
     *
     * @param array $reservation_id Reservation Id
     * @param string $preapproval_message Message from Host when pre-approving
     * @param type for Checking Pre-approval or Special-Offer
     * @return true
     */
    public function preapproval($reservation_id, $preapproval_message, $type = 'pre-approval')
    {
        $data['result']              = Reservation::find($reservation_id);
        $user                        = $data['result']->users;
        $data['first_name']          = $user->first_name;
        $data['preapproval_message'] = $preapproval_message;
        $data['type']                = $type;
        $data['url'] = url().'/';
        $data['locale']       = App::getLocale();

        $data['result'] = Reservation::where('reservation.id', $reservation_id)->with(['users' => function($query) {
                $query->with('profile_picture')->with('users_verification')->with('reviews');
            }, 'rooms' => function($query) {
                $query->with('rooms_address');
            }, 'host_users' => function($query) {
                $query->with('profile_picture')->with('users_verification')->with('reviews');
            }, 'currency', 'special_offer' => function($query) {
                $query->orderby('special_offer.id','desc')->limit(1)->with('rooms');
            }]);

        $data['result'] = $data['result']->first()->toArray();

        if($type == 'pre-approval') {
            $subject = $data['result']['host_users']['first_name'].' '.trans('messages.email.reservation_itinerary_from').' '.$data['result']['special_offer']['rooms']['name']." for ".$data['result']['special_offer']['dates_subject'];
        }
        else if($type == 'special_offer') {
            $subject = $data['result']['host_users']['first_name'].' '.trans('messages.email.sent_Special_Offer_for').' '.$data['result']['special_offer']['rooms']['name']." for ".$data['result']['special_offer']['dates_subject'];
        }
        else if($type == 'split_payment') {
            $subject = $data['result']['host_users']['first_name'].' offers you to pay partialy for '.$data['result']['special_offer']['rooms']['name']." for ".$data['result']['special_offer']['dates_subject'];
        }

        Mail::send('emails.preapproval', $data, function($message) use($user, $data, $subject) {
            $message->to($user->email, $user->first_name)->subject($subject);



        });
        return true;
    }

    /**
     * Send Listed Mail to Host
     *
     * @param array $room_id Room Details
     * @return true
     */
    public function listed($room_id)
    {
        $result               = Rooms::find($room_id);
        $user                 = $result->users;
        $data['first_name']   = $user->first_name;
        $data['room_name']    = $result->name;
        $data['created_time'] = $result->created_time;
        $data['room_id']      = $result->id;
        $data['url']          = url().'/';
        $data['locale']       = App::getLocale();
        $data['subject'] = trans('messages.email.your_space_listed').' '.SITE_NAME;
        Mail::send('emails.listed', $data, function($message) use($user, $data) {
            $message->to($user->email, $user->first_name)->subject($data['subject']);
        });
        return true;
    }

    /**
     * Send Unlisted Mail to Host
     *
     * @param array $room_id Room Details
     * @return true
     */
    public function unlisted($room_id)
    {
        $result = Rooms::find($room_id);
        $user = $result->users;
        $data['first_name'] = $user->first_name;
        $data['created_time'] = $result->created_time;
        $data['room_id'] = $result->id;
        $data['url'] = url().'/';
        $data['locale']       = App::getLocale();

        $data['subject'] = trans('messages.email.listing_deactivated').' '.SITE_NAME.' '.trans('messages.email.account');
        Mail::send('emails.unlisted', $data, function($message) use($user, $data) {
            $message->to($user->email, $user->first_name)->subject($data['subject']);
        });
        return true;
    }

    /**
     * Send Updated Payout Information Mail to Host
     *
     * @param array $payout_preference_id Payout Preference Details
     * @return true
     */
    public function payout_preferences($payout_preference_id, $type = 'update')
    {
        if($type != 'delete') {
            $result = PayoutPreferences::find($payout_preference_id);
            $user = $result->users;
            $data['first_name'] = $user->first_name;
            $data['updated_time'] = $result->updated_time;
            $data['updated_date'] = $result->updated_date;
        }
        else
        {
             //get current url
           $route=@Route::getCurrentRoute();

           if($route)
           {
            $api_url = @$route->getPath();
           }
           else
           {
            $api_url = '';
            }

            $url_array=explode('/',$api_url);
            //check request from mobile or web
          if(@$url_array['0']=='api')
          {  //set api user authentication
            $data['first_name'] = JWTAuth::parseToken()->authenticate()->first_name;
            $new_str = new DateTime(date('Y-m-d H:i:s'), new DateTimeZone(Config::get('app.timezone')));
            $new_str->setTimeZone(new DateTimeZone(JWTAuth::parseToken()->authenticate()->timezone));
          }
         else
         {  //set web user authentication
            $user = Auth::user()->user();
            $data['first_name'] = $user->first_name;
            $new_str = new DateTime(date('Y-m-d H:i:s'), new DateTimeZone(Config::get('app.timezone')));
            $new_str->setTimeZone(new DateTimeZone(Auth::user()->user()->timezone));

         }
            $data['deleted_time'] = $new_str->format('d M').' at '.$new_str->format('H:i');
        }
        $data['type'] = $type;
        $data['url'] = url().'/';
        $data['locale']       = App::getLocale();

        if($type == 'update')
            $subject = trans('messages.email.your').' '.SITE_NAME." ".trans('messages.email.payout_information_updated');
        else if($type == 'delete')
            $subject = trans('messages.email.your').' '.SITE_NAME." ".trans('messages.email.payout_information_deleted');
        else if($type == 'default_update')
            $subject = trans('messages.email.payout_information_changed');
       //check usr login from mobile or web
       if(@$url_array['0']=='api')
        {   //set mobile login user details
            $user=JWTAuth::parseToken()->authenticate();

             Mail::send('emails.payout_preferences', $data, function($message) use($user, $data, $subject) {
            $message->to($user->email, $user->first_name)->subject($subject);
        });
        }
        else
        {
             Mail::send('emails.payout_preferences', $data, function($message) use($user, $data, $subject) {
            $message->to($user->email, $user->first_name)->subject($subject);
        });

        }

        return true;
    }

    /**
     * Send Need Payout Information Mail to Host/Guest
     *
     * @param array $reservation_id Reservation Details
     * @return true
     */
    public function need_payout_info($reservation_id, $type)
    {
        $result       = Reservation::find($reservation_id);
        $data['type'] = $type;

        if($type == 'guest') {
            $user = $result->users;
            $data['payout_amount'] = $result->admin_guest_payout;
        }
        else {
            $user = $result->host_users;
            $data['payout_amount'] = $result->admin_host_payout;
        }

        $data['currency_symbol'] = $result->currency->symbol;
        $data['first_name']      = $user->first_name;
        $data['user_id']         = $user->id;
        $data['url'] = url().'/';
        $data['locale']       = App::getLocale();
        $data['subject'] = trans('messages.email.information_needed');
        Mail::send('emails.need_payout_info', $data, function($message) use($user, $data) {
            $message->to($user->email, $user->first_name)->subject($data['subject']);
        });
        return true;
    }

    /**
     * Room Details Updated to Admin
     *
     * @param array $room_id, $content
     * @return true
     */
    public function room_details_updated($room_id, $field){

        $data['room_id'] = $room_id;
        $data['result'] = Rooms::find($room_id)->toArray();
        $data['field'] = $field;
        $data['user'] = User::find($data['result']['user_id']);

        $data['url'] = url().'/';
        $data['locale']       = App::getLocale();

        $data['admin'] = Admin::whereStatus('Active')->first();
        $data['first_name'] = $data['admin']->username;
        $data['subject'] = trans('messages.email.rooms_details_updated',[], null,  $data['locale']).' '.$data['result']['name'];
        if($data['result']['status'] == 'Listed'){
            Mail::send('emails.room_details_updated', $data, function($message) use($data) {
                $message->to($data['admin']->from_address, $data['admin']->username)->subject($data['subject']);
            });
        }
        return true;
    }

    /**
     * Send Need Payout Sent Mail to Host/Guest
     *
     * @param array $reservation_id Reservation Details
     * @return true
     */
    public function payout_sent($reservation_id, $type)
    {
        $data['result'] = Reservation::find($reservation_id);
        $data['type'] = $type;

        if($type == 'guest') {
            $user = $data['result']->users;
            $data['full_name'] = $data['result']->host_users->full_name;
            $data['payout_amount'] = $data['result']->admin_guest_payout;
        }
        else{
            $user = $data['result']->host_users;
            $data['full_name'] = $data['result']->users->full_name;
            $data['payout_amount'] = $data['result']->admin_host_payout;
        }

        $data['result'] = Reservation::where('reservation.id',$reservation_id)->with(['rooms', 'currency'])->first()->toArray();
        $data['first_name'] = $user->first_name;
        $data['url'] = url().'/';
        $data['locale']       = App::getLocale();

        $data['subject'] = trans('messages.email.payout_of').' '.html_entity_decode($data['result']['currency']['symbol'], ENT_NOQUOTES, 'UTF-8').$data['payout_amount']." ".trans('messages.email.sent');
        Mail::send('emails.payout_sent', $data, function($message) use($user, $data) {
            $message->to($user->email, $user->first_name)->subject($data['subject']);
        });
        return true;
    }

    /**
     * Referral Email Share
     *
     * @param array $emails Friend Emails
     * @return true
     */
    public function referral_email_share($emails)
    {
        $user_id = Auth::user()->user()->id;

        $data['result'] = $user = User::with(['profile_picture'])->whereId($user_id)->first()->toArray();

        $data['travel_credit'] = ReferralSettings::value(4);
        $data['symbol'] = Currency::first()->symbol;

        $data['url'] = url().'/';
        $data['locale']       = App::getLocale();

        $emails = explode(',', $emails);

        $data['subject'] = $user['full_name']." ".trans('messages.email.invited_you_to').' '.SITE_NAME;
        foreach($emails as $email) {
            $email = trim($email);
            Mail::send('emails.referral_email_share', $data, function($message) use($user, $data, $email) {
                $message->to($email)->subject($data['subject']);
            });
        }
        return true;
    }

    /**
     * Review Remainder
     *
     * @param array $reservation
     * @param string $type
     * @return true
     */
    public function review_remainder($reservation, $type='guest')
    {
        $data['url'] = SiteSettings::where('name', 'site_url')->first()->value.'/';
        $data['locale']       = App::getLocale();

        if($type == 'guest') {
            $email = $reservation->host_users->email;
            $user = $reservation->users;
        }
        else {
            $email = $reservation->users->email;
            $user = $reservation->host_users;
        }

        $data['users'] = $user;
        $data['result'] = $reservation->toArray();

        $data['profile_picture'] = $user->profile_picture->email_src;
        $data['review_name'] = $user->first_name;

        $data['subject'] = trans('messages.email.write_review_about')." ".$user->first_name;
        Mail::send('emails.review_remainder', $data, function($message) use($user, $data, $email) {
            $message->to($email)->subject($data['subject']);
        });

        return true;
    }

    /**
     * Review Wrote
     *
     * @param int $review_id
     * @param string $type
     * @return true
     */
    public function wrote_review($review_id, $type ='guest')
    {
        $data['url'] = url().'/';
        $data['locale']       = App::getLocale();

        $reviews = Reviews::find($review_id);

        $email = $reviews->users->email;

        $user = $reviews->users_from;

        $data['users'] = $user;
        $data['result'] = $reviews->toArray();

        $data['review_end_date'] = Reservation::find($reviews->reservation_id)->review_end_date;

        $data['profile_picture'] = $user->profile_picture->src;
        $data['review_name'] = $user->first_name;

        $data['subject'] = $user->first_name.' '.trans('messages.email.wrote_you_review');
        Mail::send('emails.wrote_review', $data, function($message) use($user, $data, $email) {
            $message->to($email)->subject($data['subject']);
        });
    }

    /**
     * Review Read
     *
     * @param int $review_id
     * @param string $type
     * @return true
     */
    public function read_review($review_id, $type ='guest')
    {
        $data['url'] = url().'/';
        $data['locale']       = App::getLocale();

        $reviews = Reviews::find($review_id);

        $email = $reviews->users->email;

        $user = $reviews->users_from;

        $data['users'] = $user;
        $data['result'] = $reviews->toArray();

        $data['review_end_date'] = Reservation::find($reviews->reservation_id)->review_end_date;

        $data['profile_picture'] = $user->profile_picture->src;
        $data['review_name'] = $user->first_name;

        $data['subject'] = trans('messages.email.read').' '.$user->first_name."'s ".trans('messages.email.review');
        Mail::send('emails.read_review', $data, function($message) use($user, $data, $email) {
            $message->to($email)->subject($data['subject']);
        });
    }

    /**
     * Send accepted Mail to Host
     *
     * @param string $reservation_code Reservation Code
     * @param string $email Friend Email
     * @return true
     */
    public function accepted($code)
    {
        $data['result']         = Reservation::where('id', $code)->first();
        $user                   = $data['result']->host_users;
        $data['hide_header']    = true;

        $data['url']            = url().'/';
        $data['map_key']        = MAP_KEY;
        $data['locale']         = App::getLocale();

        $data['result'] = Reservation::where('reservation.id', $data['result']->id)->with(['users' => function($query) {
                $query->with('profile_picture')->with('users_verification')->with('reviews');
            }, 'rooms' => function($query) {
                $query->with('rooms_address');
            }, 'host_users' => function($query) {
                $query->with('profile_picture')->with('users_verification')->with('reviews');
            }, 'currency']);

        $data['result'] = $data['result']->first()->toArray();
        $data['subject'] = trans('messages.email.reservation_confirmed').' '.$data['result']['host_users']['full_name'];

        Mail::send('emails.accepted', $data, function($message) use($user, $data) {
            $message->to($data['result']['users']['email'], '')->subject($data['subject']);
        });
        return true;
    }

     /**
     * Send accepted Mail to Host
     *
     * @param string $reservation_code Reservation Code
     * @param string $email Friend Email
     * @return true
     */
    public function pre_accepted($code)
    {
        $data['result']         = Reservation::where('id', $code)->first();
        $user                   = $data['result']->host_users;
        $data['hide_header']    = true;

        $data['url']            = url().'/';
        $data['map_key']        = MAP_KEY;
        $data['locale']         = App::getLocale();

        $data['result'] = Reservation::where('reservation.id', $data['result']->id)->with(['users' => function($query) {
                $query->with('profile_picture')->with('users_verification')->with('reviews');
            }, 'rooms' => function($query) {
                $query->with('rooms_address');
            }, 'host_users' => function($query) {
                $query->with('profile_picture')->with('users_verification')->with('reviews');
            }, 'currency']);

        $data['result'] = $data['result']->first()->toArray();
               // return view('emails.pre_accepted', $data);
        $data['subject'] = trans('messages.inbox.reservations').' '.trans('messages.inbox.pre_accepted').' '.$data['result']['host_users']['full_name'];
        Mail::send('emails.pre_accepted', $data, function($message) use($user, $data) {
            $message->to($data['result']['users']['email'], '')->subject($data['subject']);
        });
        return true;
    }







    /**
     * Booking Confirmed Email to Host
     *
     * @param array $reservation_id
     * @return true
     */
    public function booking_confirm_host($reservation_id){
        $data['result'] = Reservation::find($reservation_id);
        $user = $data['result']->host_users;
        $data['hide_header'] = true;
        $data['url'] = url().'/';
        $data['locale']       = App::getLocale();

        $data['result'] = Reservation::where('reservation.id', $reservation_id)->with(['users' => function($query) {
                $query->with('profile_picture')->with('users_verification')->with('reviews');
            }, 'rooms', 'host_users' => function($query) {
                $query->with('profile_picture')->with('users_verification')->with('reviews');
            }, 'currency', 'messages']);

        $data['result'] = $data['result']->first()->toArray();
        $data['subject'] = trans('messages.email.booking_confirmed')." ".$data['result']['rooms']['name']." ".trans('messages.email.for')." ".$data['result']['dates_subject'];
        // return view('emails.booking_confirm_host', $data);
        Mail::send('emails.booking_confirm_host', $data, function($message) use($user, $data) {
            $message->to($user->email, $user->first_name)->subject($data['subject']);
        });
        return true;
    }


    public function booking_confirm_admin($reservation_id){
        $data['result'] = Reservation::find($reservation_id);
        $user = $data['result']->host_users;
        $data['hide_header'] = true;
        $data['url'] = url().'/';
        $data['locale']       = App::getLocale();

        $data['result'] = Reservation::where('reservation.id', $reservation_id)->with(['users' => function($query) {
                $query->with('profile_picture')->with('users_verification')->with('reviews');
            }, 'rooms', 'host_users' => function($query) {
                $query->with('profile_picture')->with('users_verification')->with('reviews');
            }, 'currency', 'messages']);

        $data['result'] = $data['result']->first()->toArray();

        $data['admin'] = Admin::whereStatus('Active')->first();
   $check_in_time =  $data['result']['rooms']['check_in_time'];
        $check_out_time =  $data['result']['rooms']['check_out_time'];
        if($check_in_time == 'Flexible')
        {
            $data['check_in_time'] = 'Flexible';
        }
        else
        {

            $in_time =  $data['result']['rooms']['check_in_time'];
            $data['check_in_time'] = date("h:i A", strtotime("00-00-00 $in_time:00:00"));
        }
         if($check_out_time == 'Flexible')
        {
            $data['check_out_time'] = 'Flexible';
        }
        else
        {
        $out_time =  $data['result']['rooms']['check_out_time'];
        $data['check_out_time'] = date("h:i A", strtotime("00-00-00 $out_time:00:00"));
        }

        $data['subject'] = trans('messages.email.booking_confirmed',[], null,  $data['locale']).' '.$data['result']['rooms']['name'].' '.trans('messages.email.for',[], null,  $data['locale']).' '.$data['result']['dates_subject'];

     Mail::send('emails.booking_confirm_admin', $data, function($message) use($user, $data) {
            //$message->to($data['admin']->from_address, $data['admin']->username)->subject($data['subject']);
            $message->to($data['admin']['email'], $data['admin']['username'] )->subject($data['subject']);
        });
        return true;
    }


 public function cancel_guest($code)
    {


        $data['result']         = Reservation::where('id', $code)->first();

        $user                   = $data['result']->host_users;
        $data['hide_header']    = true;

        $data['url']            = url().'/';
        $data['map_key']        = MAP_KEY;
        $data['locale']         = App::getLocale();

        $data['result'] = Reservation::where('reservation.id', $data['result']->id)->with(['users' => function($query) {
                $query->with('profile_picture')->with('users_verification')->with('reviews');
            }, 'rooms' => function($query) {
                $query->with('rooms_address');
            }, 'host_users' => function($query) {
                $query->with('profile_picture')->with('users_verification')->with('reviews');
            }, 'currency']);

        $data['result'] = $data['result']->first()->toArray();
        $data['admin'] = Admin::whereStatus('Active')->first();


$check_in_time =  $data['result']['rooms']['check_in_time'];
        $check_out_time =  $data['result']['rooms']['check_out_time'];
        if($check_in_time == 'Flexible')
        {
            $data['check_in_time'] = 'Flexible';
        }
        else
        {

            $in_time =  $data['result']['rooms']['check_in_time'];
            $data['check_in_time'] = date("h:i A", strtotime("00-00-00 $in_time:00:00"));
        }
         if($check_out_time == 'Flexible')
        {
            $data['check_out_time'] = 'Flexible';
        }
        else
        {
        $out_time =  $data['result']['rooms']['check_out_time'];
        $data['check_out_time'] = date("h:i A", strtotime("00-00-00 $out_time:00:00"));
        }

        $data['subject']= trans('messages.email.reservation_cancelled_by').' '.$data['result']['users']['full_name'];

        Mail::send('emails.guest_cancel_confirm_admin', $data, function($message) use($user, $data) {
            $message->to($data['admin']->from_address, $data['admin']->username)->subject($data['subject']);
        });
        return true;
    }
public function cancel_host($code)
    {


        $data['result']         = Reservation::where('id', $code)->first();

        $user                   = $data['result']->host_users;
        $data['hide_header']    = true;

        $data['url']            = url().'/';
        $data['map_key']        = MAP_KEY;
        $data['locale']         = App::getLocale();

        $data['result'] = Reservation::where('reservation.id', $data['result']->id)->with(['users' => function($query) {
                $query->with('profile_picture')->with('users_verification')->with('reviews');
            }, 'rooms' => function($query) {
                $query->with('rooms_address');
            }, 'host_users' => function($query) {
                $query->with('profile_picture')->with('users_verification')->with('reviews');
            }, 'currency']);

        $data['result'] = $data['result']->first()->toArray();
        $data['admin'] = Admin::whereStatus('Active')->first();


$check_in_time =  $data['result']['rooms']['check_in_time'];
        $check_out_time =  $data['result']['rooms']['check_out_time'];
        if($check_in_time == 'Flexible')
        {
            $data['check_in_time'] = 'Flexible';
        }
        else
        {

            $in_time =  $data['result']['rooms']['check_in_time'];
            $data['check_in_time'] = date("h:i A", strtotime("00-00-00 $in_time:00:00"));
        }
         if($check_out_time == 'Flexible')
        {
            $data['check_out_time'] = 'Flexible';
        }
        else
        {
        $out_time =  $data['result']['rooms']['check_out_time'];
        $data['check_out_time'] = date("h:i A", strtotime("00-00-00 $out_time:00:00"));
        }



        if($data['result']['status']=='Declined')
           $subjects=trans('messages.email.request_cancelled_by');
        else
           $subjects=trans('messages.email.reservation_cancelled_by');

        Mail::send('emails.host_cancel_confirm_admin', $data, function($message) use($user, $data ,$subjects) {
            $message->to($data['admin']->from_address, $data['admin']->username)->subject($subjects.' '.$data['result']['host_users']['full_name']);
        });


        return true;

    }

public function reservation_expired_admin($reservation_id)
    {

        $data['results'] = Reservation::find($reservation_id);
        $user = $data['results']->host_users;
        $data['hide_header'] = true;
        $data['url'] = SiteSettings::where('name', 'site_url')->first()->value;
        $data['locale']       = App::getLocale();

           $data['result'] = Reservation::where('reservation.id', $reservation_id)->with(['users' => function($query) {
                $query->with('profile_picture')->with('users_verification');
            }, 'rooms', 'host_users' => function($query) {
                $query->with('profile_picture')->with('users_verification');
            }, 'currency'])->first()->toArray();
         // dd($data); exit;
        $data['admin'] = Admin::whereStatus('Active')->first();


        $check_in_time =  $data['result']['rooms']['check_in_time'];
        $check_out_time =  $data['result']['rooms']['check_out_time'];

        if($check_in_time == 'Flexible')
        {
            $data['check_in_time'] = 'Flexible';
        }
        else
        {
            $in_time =  $data['result']['rooms']['check_in_time'];
            $data['check_in_time'] = date("h:i A", strtotime("00-00-00 $in_time:00:00"));
        }
         if($check_out_time == 'Flexible')
        {
            $data['check_out_time'] = 'Flexible';
        }
        else
        {
        $out_time =  $data['result']['rooms']['check_out_time'];
        $data['check_out_time'] = date("h:i A", strtotime("00-00-00 $out_time:00:00"));
        }
        $data['subject'] = trans('messages.email.reservation_expired',[], null,  $data['locale']).' '.$data['results']['rooms']['name']." for ".$data['result']['dates_subject'];
      /*  return view('emails.cancel_confirm_admin', $data); */
          Mail::send('emails.cancel_confirm_admin', $data, function($message) use($user, $data) {
            $message->to($data['admin']->from_address, $data['admin']->username)->subject($data['subject']);
        });
        return true;
    }

    public function reservation_expired_guest($reservation_id)
    {

        $data['results'] = Reservation::find($reservation_id);
        $user = $data['results']->host_users;
        $data['hide_header'] = true;
        $data['url'] = SiteSettings::where('name', 'site_url')->first()->value;
        $data['locale']       = App::getLocale();

           $data['result'] = Reservation::where('reservation.id', $reservation_id)->with(['users' => function($query) {
                $query->with('profile_picture')->with('users_verification');
            }, 'rooms', 'host_users' => function($query) {
                $query->with('profile_picture')->with('users_verification');
            }, 'currency'])->first()->toArray();
         // dd($data); exit;
        $data['admin'] = Admin::whereStatus('Active')->first();


        $check_in_time =  $data['result']['rooms']['check_in_time'];
        $check_out_time =  $data['result']['rooms']['check_out_time'];

        if($check_in_time == 'Flexible')
        {
            $data['check_in_time'] = 'Flexible';
        }
        else
        {
            $in_time =  $data['result']['rooms']['check_in_time'];
            $data['check_in_time'] = date("h:i A", strtotime("00-00-00 $in_time:00:00"));
        }
         if($check_out_time == 'Flexible')
        {
            $data['check_out_time'] = 'Flexible';
        }
        else
        {
        $out_time =  $data['result']['rooms']['check_out_time'];
        $data['check_out_time'] = date("h:i A", strtotime("00-00-00 $out_time:00:00"));
        }
        $data['subject'] = trans('messages.email.reservation_expired',[], null,  $data['locale']).' '.$data['results']['rooms']['name']." for ".$data['result']['dates_subject'];
         //return view('emails.cancel_confirm_admin', $data);
          Mail::send('emails.reservation_expire_guest', $data, function($message) use($user, $data) {
            $message->to($data['results']['users']['email'], $data['results']['users']['first_name'])->subject($data['subject']);
        });
        return true;
    }


public function booking_response_remainder($reservation_id, $hours){

        $data['result'] = Reservation::find($reservation_id);

        // $user = $data['result']->users;
        $user = $data['result']->host_users;
        $data['hide_header'] = true;
        $data['hours'] = $hours;
        $data['url'] = SiteSettings::where('name', 'site_url')->first()->value.'/';
        $data['locale']       = App::getLocale();

        $data['result'] = Reservation::where('reservation.id', $reservation_id)->with(['users' => function($query) {
                $query->with('profile_picture')->with('users_verification')->with('reviews');
            }, 'rooms', 'host_users' => function($query) {
                $query->with('profile_picture')->with('users_verification')->with('reviews');
            }, 'currency']);

        $data['result'] = $data['result']->first()->toArray();

        $check_in_time =  $data['result']['rooms']['check_in_time'];
        $check_out_time =  $data['result']['rooms']['check_out_time'];

        if($check_in_time == 'Flexible')
        {
            $data['check_in_time'] = 'Flexible';
        }
        else
        {
            $in_time =  $data['result']['rooms']['check_in_time'];
            $data['check_in_time'] = date("h:i A", strtotime("00-00-00 $in_time:00:00"));
        }
         if($check_out_time == 'Flexible')
        {
            $data['check_out_time'] = 'Flexible';
        }
        else
        {

        $out_time =  $data['result']['rooms']['check_out_time'];
        $data['check_out_time'] = date("h:i A", strtotime("00-00-00 $out_time:00:00"));
        }

        $data['subject'] = trans('messages.email.booking_inquiry_expire',[], null,  $data['locale']).' '.$data['result']['rooms']['name'].' '.trans('messages.email.for',[], null,  $data['locale']).' '.$data['result']['dates_subject'];

    Mail::send('emails.booking_response_remainder', $data, function($message) use($user, $data) {
            $message->to($user->email, $user->first_name)->subject($data['subject']);
        });
        return true;

    }


}
