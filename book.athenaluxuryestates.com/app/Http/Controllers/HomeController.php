<?php

/**
 * Home Controller
 *
 * @package     Makent
 * @subpackage  Controller
 * @category    Home
 * @author      Trioangle Product Team
 * @version     1.5.2
 * @link        http://trioangle.com
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Start\Helpers;
use App\Http\Helper\FacebookHelper;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use Validator;
use Auth;
use App;
use DB;
use Session;
use Route;
use App\Models\Currency;
use App\Models\Pages;
use App\Models\SiteSettings;
use App\Models\Rooms;
use App\Models\RoomsPhotos;
use App\Models\PropertyRooms;
use App\Models\Reservation;
use App\Models\HomeCities;
use App\Models\HostBanners;
use App\Models\OurCommunityBanners;
use App\Models\Help;
use App\Models\HelpSubCategory;
use App\Models\Slider;
use App\Models\Contactus;
use App\Models\BottomSlider;
use App\Models\Admin;
use App\Models\ThemeSettings;
use App\Models\ Language;

class HomeController extends Controller
{
    protected $helper; // Global variable for Helpers instance
    private $fb;	// Global variable for store Facebook instance

	/**
     * Constructor to Set FacebookHelper instance in Global variable
     *
     * @param array $fb   Instance of FacebookHelper
     */
	public function __construct(FacebookHelper $fb)
	{
		 $this->helper = new Helpers;
		$this->fb = $fb;
	}

	/**
     * Load Home view file
     *
     * @return home page view
     */
    public function index()
    {

        // home page redirect query
        $data['redirect'] = SiteSettings::where('name','default_home')->get();
        $data['popular_rooms'] = Rooms::where('status','Listed')->get();
        $data['property_rooms'] = PropertyRooms::select("property_room.*","rooms_photos.name", "rooms_price.night")
            ->join("rooms_photos","rooms_photos.room_id","=","property_room.room_id")
            ->join("rooms_price","rooms_price.room_id","=","property_room.room_id")
            ->where('rooms_photos.featured','Yes')
            ->get();
	 	$data['state'] = DB::table('rooms_address')->select('state')->groupBy('state')->get();         //DB::table('users')->select('id','name','email')->get();
	 	$data['city_count']	   = HomeCities::all()->count();
	 	$data['result']        = ThemeSettings::get();
	 	$data['browser'] = '';
	 	if(isset($_SERVER['HTTP_USER_AGENT']))
		{
    		$agent = $_SERVER['HTTP_USER_AGENT'];
    		if(strlen(strstr($agent,"Chrome")) > 0 )
			{
    			$data['browser'] = 'chrome';
			}
		}

		$data['home_page_media'] = SiteSettings::where('name', 'home_page_header_media')->first()->value;
		$data['home_page_sliders'] = Slider::whereStatus('Active')->orderBy('order', 'asc')->get();
		$data['home_page_bottom_sliders'] = BottomSlider::whereStatus('Active')->orderBy('order', 'asc')->get();
		$data['host_banners'] = HostBanners::all();
		$data['home_city']	   = HomeCities::all();
		$data['languagess'] =  Language::where('default_language', '1')->first()->value;

		$data['bottom_sliders'] = BottomSlider::whereStatus('Active')->orderBy('order', 'asc')->get();
		$data['our_community_banners'] = OurCommunityBanners::limit(3)->get();

		//home page two data start
       $data['reservation'] = Reservation::orderBy('id', 'desc')->where('status','Accepted')->groupBy('room_id')->limit(10)->get();
       $data['view_count'] = Rooms::orderBy('views_count', 'desc')->where('status','Listed')->groupBy('id')->get();
       $data['recommented'] = Rooms::orderBy('id', 'desc')->where('recommended','Yes')->where('status','Listed')->groupBy('id')->get();
       $data['res_count'] = count($data['reservation']);
       $data['room_view_count'] = count($data['view_count']);
       $data['room_recommented_view'] = count($data['recommented']) ;

	 	 //redirect home page
		if($data['redirect'][0]->value == 'home_two')
		{
			$data['default_home'] = 'two' ;return view('home.home_two',$data);
		}
		else
		{
			return view('home.home', $data);
		}
	}

	public function phpinfo()
	{
		echo phpinfo();
	}

	/**
     * Load Login view file with Generated Facebook login URL
     *
     * @return Login page view
     */
	public function login()
	{
		$data['fb_url'] = $this->fb->getUrlLogin();

		return view('home.login', $data);
	}

	/**
     * Load Social OR Email Signup view file with Generated Facebook login URL
     *
     * @return Signup page view
     */
	public function signup_login(Request $request)
	{
		$data['class'] = '';

		$data['fb_url'] = $this->fb->getUrlLogin();

		// Social Signup Page
		if($request->input('sm') == 1 || $request->input('sm') == '') {
			Session::put('referral', $request->referral);
			return view('home.signup_login', $data);
		}

		// Email Signup Page
		else if($request->input('sm') == 2)
			return view('home.signup_login_2', $data);

		else
			abort(500); // Call 500 error page
	}

	/**
     * Set session for Currency & Language while choosing footer dropdowns
     *
     */
	public function set_session(Request $request)
	{
		if($request->currency) {
			Session::put('currency', $request->currency);
			Session::put('previous_currency',$request->previous_currency);
			$symbol = Currency::original_symbol($request->currency);
			Session::put('symbol', $symbol);
		}
		else if($request->language) {
			Session::put('language', $request->language);
			App::setLocale($request->language);
		}
	}

	/**
	* View Cancellation Policies
	*
	* @return Cancellation Policies view file
	*/
	public function cancellation_policies()
	{
		return view('home.cancellation_policies');
	}

	/**
     * View Static Pages
     *
     * @param array $request  Input values
     * @return Static page view file
     */
	public function static_pages(Request $request)
	{
		if($request->token!='')
	    {
	     Session::put('get_token',$request->token);

	    }
		$pages = Pages::where(['url'=>$request->name, 'status'=>'Active']);

		if(!$pages->count())
			abort('404');

		$pages = $pages->first();

		$data['content'] = str_replace(['SITE_NAME', 'SITE_URL'], [SITE_NAME, url()], $pages->content);
		$data['title'] = $pages->name;

		return view('home.static_pages', $data);
	}

	public function help(Request $request)
	{

	    if($request->token!='')
	    {
	     Session::put('get_token',$request->token);

	    }

		if(Route::current()->uri() == 'help')
		{
			$data['result'] = Help::whereSuggested('yes')->whereStatus('Active')->get();
			//$data['token']  =$request->token;
		}
		elseif(Route::current()->uri() == 'help/topic/{id}/{category}') {
			$count_result = HelpSubCategory::find($request->id);
			$data['subcategory_count'] = $count = (str_slug($count_result->name,'-') != $request->category) ? 0 : 1;
			$data['is_subcategory'] = (str_slug($count_result->name,'-') == $request->category) ? 'yes' : 'no';
			if($count)
				$data['result'] = Help::whereSubcategoryId($request->id)->whereStatus('Active')->get();
			else
				$data['result'] = Help::whereCategoryId($request->id)->whereStatus('Active')->get();
		}
		else {
			$data['result'] = Help::whereId($request->id)->whereStatus('Active')->get();
			$data['is_subcategory'] = ($data['result'][0]->subcategory_id) ? 'yes' : 'no';
		}

		$data['category'] = Help::with(['category','subcategory'])->whereStatus('Active')->groupBy('category_id')->get(['category_id','subcategory_id']);

		return view('home.help', $data);
	}

	public function ajax_help_search(Request $request)
	{
		$term = $request->term;

		$queries = Help::where('question', 'like', '%'.$term.'%')->get();
		if($queries->isEmpty())
		{
			$results[] = [ 'id' => '0', 'value' => 'No results found', 'question' => 'No results found'];
		}
		else
		{
			foreach ($queries as $query)
			{
	   		$results[] = [ 'id' => $query->id, 'value' => str_replace('SITE_NAME', SITE_NAME, $query->question), 'question' => str_slug($query->question, '-') ];
			}
		}

		return json_encode($results);
	}

	  public function contact(){
    	return view('home.contact');
    }


    public function contact_create(Request $request, EmailController $email_controller)
    {

         $rules = array(
        'name'            => 'required',
        'email'   => 'required|max:255|email',
        'feedback'        => 'required|min:6',
        );

        $messages = array(
        'required'                => ':attribute is required.',
        );


        $niceNames = array(
        'name'      	  => 'Name',
        'email'           => 'Email',
        'feedback'        => 'Feedback',
        );

        $validator = Validator::make($request->all(), $rules, $messages);
        $validator->setAttributeNames($niceNames);

        if ($validator->fails())
        {
            return back()->withErrors($validator)->withInput(); // Form calling with
        }
        else
        {

            $user_contact = new Contactus;

            $user_contact->name   =   $request->name;
            $user_contact->email  =   $request->email;
            $user_contact->feedback     =   $request->feedback;

            $user_contact->save();  // Create a new user

            $email_controller->contact_email_confirmation($user_contact);

               //$this->helper->flash_message('success', trans('messages.contactus.sent_successfully')); // Call flash message function
               //return redirect('contact'); // Redirect to contact page


               return back()->with('success',trans('messages.contactus.sent_successfully'));


        }
    }

    public function host_member()
    {
        return view('hostmember.host_member');
    }

}
