<?php

/**
 * Rooms Model
 *
 * @package     Makent
 * @subpackage  Model
 * @category    Rooms
 * @author      Trioangle Product Team
 * @version     1.5.2.1.1
 * @link        http://trioangle.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use DateTime;
use DateTimeZone;
use Config;
use App\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Route;
use JWTAuth;

class PropertyRooms extends Model
{
   /* use SoftDeletes;*/
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'property_room';
    
   }