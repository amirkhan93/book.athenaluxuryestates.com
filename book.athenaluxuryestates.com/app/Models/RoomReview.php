<?php

/**
 * Reviews Model
 *
 * @package     Makent
 * @subpackage  Model
 * @category    Reviews
 * @author      Trioangle Product Team
 * @version     1.5.2
 * @link        http://trioangle.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Reviews;

class RoomReview extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'room_review';

    
}
