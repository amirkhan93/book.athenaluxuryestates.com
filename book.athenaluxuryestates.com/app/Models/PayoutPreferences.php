<?php

/**
 * PayoutPreferences Model
 *
 * @package     Makent
 * @subpackage  Model
 * @category    PayoutPreferences
 * @author      Trioangle Product Team
 * @version     1.5.2
 * @link        http://trioangle.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Route;
use DateTime;
use DateTimeZone;
use Config;
use Auth;
use JWTAuth;

class PayoutPreferences extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'payout_preferences';

    public $appends = ['updated_time', 'updated_date'];

    // Get Updated time for Payout Information
    public function getUpdatedTimeAttribute()
    {    //Get currenct url 
        $route=@Route::getCurrentRoute();

        if($route)
        {

         $api_url = @$route->getPath();

        }
        else
        {

        $api_url = '';

        }
        $url_array=explode('/',$api_url);

           //check the url is web or mobile
        if(@$url_array['0']=='api')
        { 

          $new_str = new DateTime($this->attributes['updated_at'], new DateTimeZone(Config::get('app.timezone')));

          $new_str->setTimeZone(new DateTimeZone(JWTAuth::parseToken()->authenticate()->timezone));

          return $new_str->format('d M').' at '.$new_str->format('H:i');
        }
        else
        { 

          $new_str = new DateTime($this->attributes['updated_at'], new DateTimeZone(Config::get('app.timezone')));

          $new_str->setTimeZone(new DateTimeZone(Auth::user()->user()->timezone));

          return $new_str->format('d M').' at '.$new_str->format('H:i');

        }


       
    }

    // Get Updated date for Payout Information
    public function getUpdatedDateAttribute()
    {
         //Get currenct url 
        $route=@Route::getCurrentRoute();

        if($route)
        {

         $api_url = @$route->getPath();

        }
        else
        {

        $api_url = '';

        }
        $url_array=explode('/',$api_url);

           //check the url is web or mobile
        if(@$url_array['0']=='api')
        { 
           $new_str = new DateTime($this->attributes['updated_at'], new DateTimeZone(Config::get('app.timezone')));

           $new_str->setTimeZone(new DateTimeZone(JWTAuth::parseToken()->authenticate()->timezone));

           return $new_str->format('d F, Y');
        }
        else
        {  
            $new_str = new DateTime($this->attributes['updated_at'], new DateTimeZone(Config::get('app.timezone')));

           $new_str->setTimeZone(new DateTimeZone(Auth::user()->user()->timezone));

           return $new_str->format('d F, Y');

        }
        
    }

    // Join with users table
    public function users()
    {
      return $this->belongsTo('App\Models\User','user_id','id');
    }
}
