$("#home_slider").responsiveSlides({
    auto: true,
    pager: false,
    nav: false,
    speed: 2000,
    namespace: "home_slider",
    timeout: 8000
});
// if ($(window).width() < 480) {
//     $("input, select, textarea").bind("focus", function() {
//         $("#js-manage-listing-footer").hide()
//     });
//     $("input, select, textarea").bind("blur", function() {
//         $("#js-manage-listing-footer").show()
//     });
// }

$(document).mouseup(function(e) {
    var container = $(".header-dropdown");
    // if the target of the click isn't the container nor a descendant of the container
    if (!container.is(e.target) && container.has(e.target).length === 0) {
        container.hide();
    }
});

$('ul.menu-group li a').click(function() {
    $('.nav--sm').css('visibility', 'hidden');
});
$(function() {
    $('#my-element').textfill({
        maxFontPixels: 36

    });
});
$(window).scroll(function() {
    if ($(window).width() > 760) {
        if ($('#ui-datepicker-div').css('display') == 'block') {



            $('#ui-datepicker-div').hide();
        }
    }
});
$('.modal-content').scroll(function() {


    $('#ui-datepicker-div').hide();

});
$('.contact-modal').scroll(function() {

    $('#ui-datepicker-div').hide();


});
$('.checkin').click(function() {
    // $('#ui-datepicker-div').show();
});
$('.checkout').click(function() {
    // $('#ui-datepicker-div').show();
});
$('#host-profile-contact-btn').click(function() {
    $("body").addClass("pos-fix");
});
$('.modal-close').click(function() {
    $("body").removeClass("pos-fix");
    $('#ui-datepicker-div').hide();
});

//document ready function
$('#accept_submit').attr('disabled', 'disabled');

$(document).ready(function() {

    // user request message check box validation uses for host side
    $(document).on('click', '#tos_confirm', function() {
        if ($('#tos_confirm').val() == 0) {
            $('#accept_submit').removeAttr('disabled');
            $('#tos_confirm').val('1');
        } else {
            $('#accept_submit').attr('disabled', 'disabled');
            $('#tos_confirm').val('0');
        }
    });

    //used for pre approve sucess message remove
    $(document).on('click', '#pre_approve_button', function() {
        $("div").remove(".alert-success");
    });
});

$(function() {
    var targets = $('[rel~=tooltip]'),
        target = false,
        tooltip = false,
        title = false;

    targets.bind('mouseenter', function() {
        target = $(this);
        tip = target.attr('title');
        tooltip = $('<div id="tooltip1"></div>');

        if (!tip || tip == '')
            return false;

        target.removeAttr('title');
        tooltip.css('opacity', 0)
            .html(tip)
            .appendTo('body');

        var init_tooltip = function() {
            if ($(window).width() < tooltip.outerWidth() * 1.5)
                tooltip.css('max-width', $(window).width() / 2);
            else
                tooltip.css('max-width', 340);

            var pos_left = target.offset().left + (target.outerWidth() / 2) - (tooltip.outerWidth() / 2),
                pos_top = target.offset().top - tooltip.outerHeight() - 20;

            if (pos_left < 0) {
                pos_left = target.offset().left + target.outerWidth() / 2 - 20;
                tooltip.addClass('left');
            } else
                tooltip.removeClass('left');

            if (pos_left + tooltip.outerWidth() > $(window).width()) {
                pos_left = target.offset().left - tooltip.outerWidth() + target.outerWidth() / 2 + 20;
                tooltip.addClass('right');
            } else
                tooltip.removeClass('right');

            if (pos_top < 0) {
                var pos_top = target.offset().top + target.outerHeight();
                tooltip.addClass('top');
            } else
                tooltip.removeClass('top');

            tooltip.css({
                    left: pos_left,
                    top: pos_top
                })
                .animate({
                    top: '+=10',
                    opacity: 1
                }, 50);
        };

        init_tooltip();
        $(window).resize(init_tooltip);

        var remove_tooltip = function() {
            tooltip.animate({
                top: '-=10',
                opacity: 0
            }, 50, function() {
                $(this).remove();
            });

            target.attr('title', tip);
        };

        target.bind('mouseleave', remove_tooltip);
        tooltip.bind('click', remove_tooltip);
    });
});

$('.host_banner_content_slider_item').hide();
$('#host_banner_content_slider_item_0').show();

$("#host_banner_slider").responsiveSlides({
    auto: true,
    pager: false,
    nav: false,
    speed: 2000,
    timeout: 5000,
    namespace: "host_banner_slider_item",
    before: function(index) {
        items_count = $("#host_banners_count").val();
        current_index = $('.' + this.namespace + '2_on').index();
        next_index = current_index + 1;
        if (next_index > items_count) {
            next_index = 0;
        }
        $("#host_banner_content_slider_item_" + current_index).hide();
        $("#host_banner_content_slider_item_" + next_index).fadeIn(1000);
    },
});

$(function() {
    $("#bottom_slider").responsiveSlides({
        auto: true,
        pager: false,
        nav: true,
    });
});
$("#bottom_slider").responsiveSlides({
    auto: true, // Boolean: Animate automatically, true or false
    speed: 500, // Integer: Speed of the transition, in milliseconds
    timeout: 4000, // Integer: Time between slide transitions, in milliseconds
    pager: false, // Boolean: Show pager, true or false
    nav: true, // Boolean: Show navigation, true or false
    random: false, // Boolean: Randomize the order of the slides, true or false
    pause: false, // Boolean: Pause on hover, true or false
    pauseControls: true, // Boolean: Pause when hovering controls, true or false
    prevText: "Previous", // String: Text for the "previous" button
    nextText: "Next", // String: Text for the "next" button
    maxwidth: "", // Integer: Max-width of the slideshow, in pixels
    navContainer: "", // Selector: Where controls should be appended to, default is after the 'ul'
    manualControls: "", // Selector: Declare custom pager navigation
    namespace: "bottom_slider", // String: Change the default namespace used
    before: function() {}, // Function: Before callback
    after: function() {} // Function: After callback
});
if ($('.manage-listing-row-container').hasClass('has-collapsed-nav') === true) {
    $('#js-manage-listing-nav').addClass('manage-listing-nav');
}


$('.js-show-how-it-works').click(function() {
    $(".js-how-it-works").slideToggle("fast", function() {
        $('.js-how-it-works').show();
    });
});

$('.js-close-how-it-works').click(function() {
    $(".js-how-it-works").slideToggle("fast", function() {
        $('.js-how-it-works').hide();
    });
});
$('#room-type-tooltip').mouseover(function() {

    $('.tooltip-room').show();
});
$('#room-type-tooltip').mouseout(function() {
    $('.tooltip-room').hide();
});
$('[id^="amenity-tooltip"]').on("mouseover", function() {
    var id = $(this).data('id');
    $('#ame-tooltip-' + id).show();
});
$('[id^="amenity-tooltip"]').on("mouseout", function() {
    $('[id^="ame-tooltip"]').hide();
});
$('.tool-amenity1').mouseover(function() {
    $('.tooltip-amenity1').show();
});
$('.tool-amenity1').mouseout(function() {
    $('.tooltip-amenity1').hide();
});
$('.tool-amenity2').mouseover(function() {
    $('.tooltip-amenity2').show();
});
$('.tool-amenity2').mouseout(function() {
    $('.tooltip-amenity2').hide();
});
$('a.become').mouseover(function() {
    $('.drop-down-menu-host').show();
});
$('a.become').mouseout(function() {
    $('.drop-down-menu-host').hide();
});
$('.trip-drop').mouseout(function() {
    $('.drop-down-menu-trip').hide();
});
$('.trip-drop').mouseover(function() {
    $('.drop-down-menu-trip').show();
});
$('.inbox-icon').mouseout(function() {
    $('.drop-down-menu-msg').hide();
});
$('.inbox-icon').mouseover(function() {
    $('.drop-down-menu-msg').show();
});
$('.drop-down-menu-host').mouseover(function() {
    $(this).show();
});
$('.drop-down-menu-host').mouseout(function() {
    $(this).hide();
});
$('.drop-down-menu-trip').mouseover(function() {
    $(this).show();
});
$('.drop-down-menu-trip').mouseout(function() {
    $(this).hide();
});
$('.drop-down-menu-msg').mouseover(function() {
    $(this).show();
});
$('.drop-down-menu-msg').mouseout(function() {
    $(this).hide();
});
$('.burger--sm').click(function() {
    $('.header--sm .nav--sm').css('visibility', 'visible');
    $("body").addClass("remove-pos-fix pos-fix");
    $('.makent-header .header--sm .nav-content--sm').addClass('right-content');
});

$('.nav-mask--sm').click(function() {
    $('.header--sm .nav--sm').css('visibility', 'hidden');
    $("body").removeClass("remove-pos-fix pos-fix");
    $('.makent-header .header--sm .nav-content--sm').removeClass('right-content');
});

$('.search-modal-trigger, #sm-search-field').click(function() {
    $('#search-modal--sm').removeClass('hide');
    $('#search-modal--sm').attr('aria-hidden', 'false');
    // $('body').css('overflow-y', 'hidden');
});

$('.search-modal-container .modal-close').click(function() {
    $('#search-modal--sm').addClass('hide');
    $('#search-modal--sm').attr('aria-hidden', 'true');
    // $('body').css('overflow-y', 'auto');
});
$('.list-nav-link a').click(function() {
    $('.listing-nav-sm').removeClass('collapsed');
});
$('#href_pricing').click(function() {
    $('#js-manage-listing-nav').removeClass('manage-listing-nav');
    $('#js-manage-listing-nav').addClass('pos-abs');
    $('#ajax_container').addClass('mar-left-cont');
});
$('#href_terms').click(function() {
    $('#js-manage-listing-nav').removeClass('manage-listing-nav');
    $('#js-manage-listing-nav').addClass('pos-abs');
    $('#ajax_container').addClass('mar-left-cont');
});
$('#remove-manage').click(function() {
    $('#js-manage-listing-nav').removeClass('manage-listing-nav');
    $('#js-manage-listing-nav').addClass('pos-abs');
    $('#ajax_container').addClass('mar-left-cont');
});
$('#href_booking').click(function() {
    $('#js-manage-listing-nav').removeClass('manage-listing-nav');
    $('#js-manage-listing-nav').addClass('pos-abs');
    $('#ajax_container').addClass('mar-left-cont');
});
$('#href_basics').click(function() {
    $('#js-manage-listing-nav').removeClass('manage-listing-nav');
    $('#js-manage-listing-nav').addClass('pos-abs');
    $('#ajax_container').addClass('mar-left-cont');
});
$('#href_description').click(function() {
    $('#js-manage-listing-nav').removeClass('manage-listing-nav');
    $('#js-manage-listing-nav').addClass('pos-abs');
    $('#ajax_container').addClass('mar-left-cont');
});
$('#href_location').click(function() {
    $('#js-manage-listing-nav').removeClass('manage-listing-nav');
    $('#js-manage-listing-nav').addClass('pos-abs');
    $('#ajax_container').addClass('mar-left-cont');
});
$('#href_amenities').click(function() {
    $('#js-manage-listing-nav').removeClass('manage-listing-nav');
    $('#js-manage-listing-nav').addClass('pos-abs');
    $('#ajax_container').addClass('mar-left-cont');
});
$('#href_photos').click(function() {
    $('#js-manage-listing-nav').removeClass('manage-listing-nav');
    $('#js-manage-listing-nav').addClass('pos-abs');
    $('#ajax_container').addClass('mar-left-cont');
});
$('#href_details').click(function() {
    $('#js-manage-listing-nav').removeClass('manage-listing-nav');
    $('#js-manage-listing-nav').addClass('pos-abs');
    $('#ajax_container').addClass('mar-left-cont');
});
$('#href_guidebook').click(function() {
    $('#js-manage-listing-nav').removeClass('manage-listing-nav');
    $('#js-manage-listing-nav').addClass('pos-abs');
    $('#ajax_container').addClass('mar-left-cont');
});
$('#href_calendar').click(function() {
    $('#js-manage-listing-nav').addClass('manage-listing-nav');
});
$('#href_calendar').click(function() {
    $('#ajax_container').removeClass('mar-left-cont');
});
$('#href_calendar').click(function() {
    $('#js-manage-listing-nav').removeClass('pos-abs');
});
$('#header-avatar-trigger').click(function(e) {
    e.preventDefault();
    $('.tooltip.tooltip-top-right.dropdown-menu.drop-down-menu-login').toggle();
});

if (typeof(google) == 'undefined') {
    window.location.href = APP_URL + '/in_secure';
}
homeAutocomplete();
var home_autocomplete;
var home_mob_autocomplete;

function homeAutocomplete() {
    if (document.getElementById('location')) {
        home_autocomplete = new google.maps.places.Autocomplete(document.getElementById('location'));
        home_autocomplete.addListener('place_changed', trigger_checkin);
    }
    if (document.getElementById('mob-search-location')) {
        home_mob_autocomplete = new google.maps.places.Autocomplete(document.getElementById('mob-search-location'));
        google.maps.event.addListener(home_mob_autocomplete, 'place_changed', function() {
            var location = $('#mob-search-location').val();
            var locations = location.replace(" ", "+");
            window.location.href = APP_URL + '/s?location=' + locations;
        });
    }
}

var current_url = window.location.href.split('?')[0];
var last_part = current_url.substr(current_url.lastIndexOf('/'));
var last_part1 = current_url.substr(current_url.lastIndexOf('/') + 1);
if (last_part != '/s') {
    headerAutocomplete();
} else {
    $("#header-search-form-mob").keypress(function(e) {
        if (e.keyCode === 13) {
            e.preventDefault();
        }
    });

}

var header_autocomplete;
var sm_autocomplete;

function headerAutocomplete() {
    if (document.getElementById('header-search-form')) {
        header_autocomplete = new google.maps.places.Autocomplete(document.getElementById('header-search-form'));
        google.maps.event.addListener(header_autocomplete, 'place_changed', function() {
            $('#header-search-settings').addClass('shown');
            // $("#header-search-checkin").datepicker("show");
            $("#header-search-checkin").trigger('click');
            $(".webcot-lg-datepicker button").trigger("click");
        });
    }
    if (document.getElementById('header-search-form-mob')) {
        sm_autocomplete = new google.maps.places.Autocomplete(document.getElementById('header-search-form-mob'));
        google.maps.event.addListener(sm_autocomplete, 'place_changed', function() {
            // $("#modal_checkin").datepicker("show");
            $("#header-search-form").val($("#header-search-form-mob").val());
            $("#modal_checkin").trigger('click');
        });
    }
}

start = moment();
$('#header-search-checkin').daterangepicker({
    minDate: start,
    dateLimitMin:{
       "days": 1
    },
    autoApply: true,
    autoUpdateInput: false,
    locale: {
        format: "MMM DD"
    },
});

$('#header-search-checkin').on('apply.daterangepicker', function(ev, picker) {
    startDateInput = $('#header-search-checkin');
    endDateInput = $('#header-search-checkout');

    startDate = picker.startDate;
    endDate = picker.endDate;

    startDateInput.val(startDate.format('DD-MM-YYYY'));
    endDateInput.val(endDate.format('DD-MM-YYYY'));
});
$('#header-search-checkout').daterangepicker({
    minDate: start,
    dateLimitMin:{
       "days": 1
    },
    autoApply: true,
    autoUpdateInput: false,
    locale: {
        format: "MMM DD"
    },
});

$('#header-search-checkout').on('apply.daterangepicker', function(ev, picker) {
    startDateInput = $('#header-search-checkin');
    endDateInput = $('#header-search-checkout');

    startDate = picker.startDate;
    endDate = picker.endDate;

    startDateInput.val(startDate.format('DD-MM-YYYY'));
    endDateInput.val(endDate.format('DD-MM-YYYY'));
});

start = moment();
$('#modal_checkin').daterangepicker({
    minDate: start,
    dateLimitMin:{
       "days": 1
    },
    autoApply: true,
    autoUpdateInput: false,
    locale: {
        format: "MMM DD"
    },
});

$('#modal_checkin').on('apply.daterangepicker', function(ev, picker) {
    startDateInput = $('#modal_checkin');
    endDateInput = $('#modal_checkout');

    startDate = picker.startDate;
    endDate = picker.endDate;

    startDateInput.val(startDate.format('DD-MM-YYYY'));
    endDateInput.val(endDate.format('DD-MM-YYYY'));
});

$('#modal_checkout').daterangepicker({
    minDate: start,
    dateLimitMin:{
       "days": 1
    },
    autoApply: true,
    autoUpdateInput: false,
    locale: {
        format: "MMM DD"
    },
});

$('#modal_checkout').on('apply.daterangepicker', function(ev, picker) {
    startDateInput = $('#modal_checkin');
    endDateInput = $('#modal_checkout');

    startDate = picker.startDate;
    endDate = picker.endDate;

    startDateInput.val(startDate.format('DD-MM-YYYY'));
    endDateInput.val(endDate.format('DD-MM-YYYY'));
});


$('#searchbar-form').submit(function(event) {
    if ($('#location').val() == '') {
        $('.searchbar__location-error').removeClass('hide');
        event.preventDefault();
    } else
        $('.searchbar__location-error').addClass('hide');
});

$('#location, #header-location--sm').keyup(function() {
    $('.searchbar__location-error').addClass('hide');
});

$('.search-form').submit(function(event) {
    var header_checkin = $('#header-search-checkin').val();
    var header_checkout = $('#header-search-checkout').val();
    var header_guests = $('#header-search-guests').val();
    var header_room_type = '';

    $('[id^="header_room_type"]').each(function() {
        if ($(this).is(':checked'))
            header_room_type += $(this).val() + ',';
    });
    header_room_type = header_room_type.slice(0, -1);
    var location = $('#header-search-form').val();
    var locations = location.replace(" ", "+");
    window.location.href = APP_URL + '/s?location=' + locations + '&checkin=' + header_checkin + '&checkout=' + header_checkout + '&guests=' + header_guests + '&room_type=' + header_room_type;
    event.preventDefault();
});

$('#search-form--sm-btn').click(function(event) {
    var location = $('#header-search-form-mob').val();
    if (location == '') {
        $('.searchbar__location-error').removeClass('hide');
        return false;
    } else
        $('.searchbar__location-error').addClass('hide');

    var sm_checkin = $('#modal_checkin').val();
    var sm_checkout = $('#modal_checkout').val();
    var sm_guests = $('#modal_guests').val();
    var sm_room_type = '';

    $('[id^="room-type-"]').each(function() {
        if ($(this).is(':checked'))
            sm_room_type += $(this).val() + ',';
    });
    sm_room_type = sm_room_type.slice(0, -1);
    var locations = location.replace(" ", "+");
    window.location.href = APP_URL + '/s?location=' + locations + '&checkin=' + sm_checkin + '&checkout=' + sm_checkout + '&guests=' + sm_guests + '&room_type=' + sm_room_type;
    event.preventDefault();
});

// Hide header search form when click outside of that container
$('html').click(function() {
    $("#header-search-settings").removeClass('shown');
});

//click download app scroll down the page
$(document).on('click', '.menu-item', function() {
    var link = $(this).attr('href');
    if (link == '#') {
        $('body').removeClass('pos-fix');
    }
});

$('#header-search-settings').click(function(event) {
    event.stopPropagation();
});
$('#ui-datepicker-div').click(function(event) {
    event.stopPropagation();
});
$('.daterangepicker').click(function(event) {
    event.stopPropagation();
});

function trigger_checkin() {
    $("#checkin").datepicker("show");
}
if ($(".searchbar").length) {

    start = moment();
    $('#checkin').daterangepicker({
        minDate: start,
        dateLimitMin : {
            "days" :1
        },
        autoApply: true,
        autoUpdateInput: false,
        locale: {
            format: "MMM DD"
        },
    });

    $('#checkin').on('apply.daterangepicker', function(ev, picker) {
        startDateInput = $('#checkin');
        endDateInput = $('#checkout');

        startDate = picker.startDate;
        endDate = picker.endDate;

        startDateInput.val(startDate.format('DD-MM-YYYY'));
        endDateInput.val(endDate.format('DD-MM-YYYY'));
    });

    $('#checkout').daterangepicker({
        minDate: start,
        dateLimitMin : {
            "days" :1
        },
        autoApply: true,
        autoUpdateInput: false,
        locale: {
            format: "MMM DD"
        },
    });
}

// Coupon Code
app.controller('payment', ['$scope', '$http', function($scope, $http) {
    $('.open-coupon-section-link').click(function() {
        $("#billing-table").addClass("coupon-section-open");
        $('#restric_apply').hide();
    });

    $('.cancel-coupon').click(function() {
        $("#billing-table").removeClass("coupon-section-open");
        $('#restric_apply').show();
        $('#coupon_disabled_message').hide();
    });

    $('#apply-coupon').click(function() {
        var coupon_code = $('.coupon-code-field').val();
        var sessionkey = $("input[name=session_key]").val();

        $http.post(APP_URL + '/payments/apply_coupon', {
            coupon_code: coupon_code,
            s_key: sessionkey
        }).then(function(response) {
            if (response.data.message) {
                $("#coupon_disabled_message").show();
                $('#coupon_disabled_message').text(response.data.message);
                $("#after_apply_remove").hide();
            } else {
                $("#coupon_disabled_message").hide();
                $("#restric_apply").hide();
                $("#after_apply").hide();
                $("#after_apply_remove").show();
                $("#after_apply_coupon").show();
                $("#after_apply_amount").show();
                $('#applied_coupen_amount').text(response.data.coupon_amount);
                $('#payment_total').text(response.data.coupen_applied_total);
                window.location.reload();
            }
        });
    });

    $('#remove_coupon').click(function() {
        $http.post(APP_URL + '/payments/remove_coupon', {}).then(function(response) {
            window.location.reload();
        });
    });

}]);
// Coupon Codeopen-coupon-section-link 


$(document).on('change', '#payment-method-select', function() {
    if ($(this).val() == 'paypal') {
        $('#payment-method-cc').hide();
        $('.cc').hide();
        $('.' + $(this).val()).addClass('active');
        $('.' + $(this).val()).addClass('active');
        $('.' + $(this).val() + ' > .payment-logo').removeClass('inactive');
    } else {
        $('#payment-method-cc').show();
        $('.cc').show();
        $('.paypal').removeClass('active');
        $('.paypal > .payment-logo').addClass('inactive');
    }
    $('[name="payment_method"]').val($(this).val());
});
//change for mobile
$(document).ready(function() {
    setTimeout(function() {
        if ($('#payment-method-select').val() == 'paypal') {
            $('#payment-method-cc').hide();
            $('.cc').hide();
            $('.' + $('#payment-method-select').val()).addClass('active');
            $('.' + $('#payment-method-select').val()).addClass('active');
            $('.' + $('#payment-method-select').val() + ' > .payment-logo').removeClass('inactive');
        } else {
            $('#payment-method-cc').show();
            $('.cc').show();
            $('.paypal').removeClass('active');
            $('.paypal > .payment-logo').addClass('inactive');
        }
        $('[name="payment_method"]').val($('#payment-method-select').val());
    }, 1000);
});
//end change for mobile.

$('#country-select').change(function() {
    $('#billing-country').text($("#country-select option:selected").text());
    $('[name="country"]').val($(this).val());
});

$('#billing-country').text($("#country-select option:selected").text());
$('[name="country"]').val($("#country-select option:selected").val());
var previous_currency;
app.controller('footer', ['$scope', '$http', function($scope, $http) {
    $("#currency_footer").click(function() {
        // Store the current value on focus, before it changes
        previous_currency = this.value;
    }).change(function() {
        $http.post(APP_URL + "/set_session", {
            currency: $(this).val(),
            previous_currency: previous_currency
        }).then(function(data) {
            location.reload();
        });
    });

    $('#language_footer').change(function() {
        $http.post(APP_URL + "/set_session", {
            language: $(this).val()
        }).then(function(data) {
            location.reload();
        });
    });

    $('.room_status_dropdown').change(function() {
        var data_params = {};

        data_params['status'] = $(this).val();

        var data = JSON.stringify(data_params);

        var id = $(this).attr('data-room-id');

        $http.post('manage-listing/' + id + '/update_rooms', {
            data: data
        }).then(function(response) {
            if (data_params['status'] == 'Unlisted') {
                $('[data-room-id="div_' + id + '"] > i').addClass('dot-danger');
                $('[data-room-id="div_' + id + '"] > i').removeClass('dot-success');
            } else if (data_params['status'] == 'Listed') {
                $('[data-room-id="div_' + id + '"] > i').removeClass('dot-danger');
                $('[data-room-id="div_' + id + '"] > i').addClass('dot-success');
            }
        });
    });

    $(document).on('click', '.wl-modal-footer__text', function() {
        $('.wl-modal-footer__form').removeClass('hide');
    });

    $('#send-email').unbind("click").click(function() {
        var emails = $('#email-list').val();
        if (emails != '') {
            $http.post('invite/share_email', {
                emails: emails
            }).then(function(response) {
                if (response.data == "true") {
                    $('#success_message').fadeIn(800);
                    $('#success_message').fadeOut();
                    $('#email-list').val('');
                } else {
                    $('#error_message').fadeIn(800);
                    $('#error_message').fadeOut();
                }
            });
        }
    });

}]);

app.controller('payout_preferences', ['$scope', '$http', function($scope, $http) {

    $('#add-payout-method-button').click(function() {
        $('#payout_popup1').removeClass('hide').attr("aria-hidden", "false");
    });

    $('#address').submit(function() {
        var validation_container = '<div class="alert alert-error alert-error alert-header"><a class="close alert-close" href="javascript:void(0);"></a><i class="icon alert-icon icon-alert-alt"></i>';
        if ($('#payout_info_payout_address1').val().trim() == '') {
            $('#popup1_flash-container').html(validation_container+$('#blank_address').val()+'</div>');
            return false;
        }
        if ($('#payout_info_payout_city').val().trim() == '') {
            $('#popup1_flash-container').html(validation_container+$('#blank_city').val()+'</div>');
            return false;
        }
        if ($('#payout_info_payout_zip').val().trim() == '') {
            $('#popup1_flash-container').html(validation_container+$('#blank_post').val()+'</div>');
            return false;
        }
        if ($('#payout_info_payout_country').val().trim() == null) {
            $('#popup1_flash-container').html(validation_container+$('#blank_country').val()+'</div>');
            return false;
        }
        $('#payout_info_payout2_address1').val($('#payout_info_payout_address1').val());
        $('#payout_info_payout2_address2').val($('#payout_info_payout_address2').val());
        $('#payout_info_payout2_city').val($('#payout_info_payout_city').val());
        $('#payout_info_payout2_state').val($('#payout_info_payout_state').val());
        $('#payout_info_payout2_zip').val($('#payout_info_payout_zip').val());
        $('#payout_info_payout2_country').val($('#payout_info_payout_country').val());

        $('#payout_popup1').addClass('hide');
        $('#payout_popup2').removeClass('hide').attr("aria-hidden", "false");
    });

    $('#select-payout-method-submit').click(function() {
        var validation_container = '<div class="alert alert-error alert-error alert-header"><a class="close alert-close" href="javascript:void(0);"></a><i class="icon alert-icon icon-alert-alt"></i>';
        if ($('[id="payout2_method"]:checked').val() == undefined) {
            $('#popup2_flash-container').html(validation_container+$('#choose_method').val()+'</div>');
            return false;
        }

        $('#payout_info_payout3_address1').val($('#payout_info_payout2_address1').val());
        $('#payout_info_payout3_address2').val($('#payout_info_payout2_address2').val());
        $('#payout_info_payout3_city').val($('#payout_info_payout2_city').val());
        $('#payout_info_payout3_state').val($('#payout_info_payout2_state').val());
        $('#payout_info_payout3_zip').val($('#payout_info_payout2_zip').val());
        $('#payout_info_payout3_country').val($('#payout_info_payout2_country').val());
        $('#payout3_method').val($('#payout2_method').val());

        $('#payout_popup2').addClass('hide');
        $('#payout_popup3').removeClass('hide').attr("aria-hidden", "false");
    });
    $('#payout_paypal').submit(function() {
        var validation_container = '<div class="alert alert-error alert-error alert-header"><a class="close alert-close" href="javascript:void(0);"></a><i class="icon alert-icon icon-alert-alt"></i>';
        var emailChar = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (emailChar.test($('#paypal_email').val())) {
            return true;
        } else {
            $('#popup3_flash-container').removeClass('hide');
            return false;
        }
    });
    $('.panel-close').click(function() {
        $(this).parent().parent().parent().parent().parent().addClass('hide');
    });

    $('[id$="_flash-container"]').on('click', '.alert-close', function() {
        $(this).parent().parent().html('');
    });

}]);

app.directive('postsPaginationTransaction', function() {
    return {
        restrict: 'E',
        template: '<ul class="pagination"><h3 class="status-text text-center" ng-show="loading">Loading...</h3><h3 class="status-text text-center" ng-hide="result.length">No Transactions</h3>' +
            '<li ng-show="currentPage != 1"><a href="javascript:void(0)" ng-click="pagination_result(type, 1)">&laquo;</a></li>' +
            '<li ng-show="currentPage != 1"><a href="javascript:void(0)" ng-click="pagination_result(type, currentPage-1)">&lsaquo; ' + $('#pagin_prev').val() + ' </a></li>' +
            '<li ng-repeat="i in range" ng-class="{active : currentPage == i}">' +
            '<a href="javascript:void(0)" ng-click="pagination_result(type, i)">{{i}}</a>' +
            '</li>' +
            '<li ng-show="currentPage != totalPages"><a href="javascript:void(0)" ng-click="pagination_result(type, currentPage+1)">' + $('#pagin_next').val() + ' &rsaquo;</a></li>' +
            '<li ng-show="currentPage != totalPages"><a href="javascript:void(0)" ng-click="pagination_result(type, totalPages)">&raquo;</a></li>' +
            '</ul>'
    };
}).controller('transaction_history', ['$scope', '$http', function($scope, $http) {

    $scope.paid_out = 0;

    $('li > .tab-item').click(function() {
        var tab_name = $(this).attr('aria-controls');
        var tab_selected = $(this).attr('aria-selected');
        if (tab_selected == 'false') {
            $('.tab-item').attr('aria-selected', 'false');
            $(this).attr('aria-selected', 'true');
            $('.tab-panel').hide();
            $('#' + tab_name).show();
        }
        $scope.type = tab_name;
        $scope.pagination_result(tab_name, 1);
    });

    $('[class^="payout-"]').change(function() {
        var type = $(this).parent().parent().parent().parent().attr('id');
        $scope.type = type;
        $scope.pagination_result(type, 1);
    });

    $scope.pagination_result = function(type, page) {
        var data_params = {};

        data_params['type'] = type;

        data_params['payout_method'] = $('#' + data_params['type'] + ' .payout-method').val();
        data_params['listing'] = $('#' + data_params['type'] + ' .payout-listing').val();
        data_params['year'] = $('#' + data_params['type'] + ' .payout-year').val();
        data_params['start_month'] = $('#' + data_params['type'] + ' .payout-start-month').val();
        data_params['end_month'] = $('#' + data_params['type'] + ' .payout-end-month').val();

        var data = JSON.stringify(data_params);

        if (page == undefined)
            page = 1;

        if (type == 'completed-transactions')
            $scope.completed_csv_param = 'type=' + data_params['type'] + '&payout_method=' + data_params['payout_method'] + '&listing=' + data_params['listing'] + '&year=' + data_params['year'] + '&start_month=' + data_params['start_month'] + '&end_month=' + data_params['end_month'] + '&page=' + page;

        if (type == 'future-transactions')
            $scope.future_csv_param = 'type=' + data_params['type'] + '&payout_method=' + data_params['payout_method'] + '&listing=' + data_params['listing'] + '&page=' + page;

        $scope.result_show = false;
        $scope.loading = true;
        $http.post(APP_URL + '/users/result_transaction_history?page=' + page, {
            data: data
        }).then(function(response) {
            $scope.loading = false;
            $scope.result = response.data.data;
            if ($scope.result.length != 0) {
                $scope.result_show = true;
                $scope.totalPages = response.data.last_page;
                $scope.currentPage = response.data.current_page;
                $scope.type = type;

                var pages = [];
                for (var i = 1; i <= response.data.last_page; i++) {
                    pages.push(i);
                }
                $scope.range = pages;

                var total = 0;
                for (var i = 0; i < $scope.result.length; i++) {
                    total += $scope.result[i].amount;
                }
                $scope.paid_out = $scope.result[0].currency_symbol + total;
            }
        });
    }

    $scope.pagination_result('completed-transactions', 1);

}]);

app.controller('reviews', ['$scope', '$http', function($scope, $http) {

    $('li > .tab-item').click(function() {
        var tab_name = $(this).attr('aria-controls');
        var tab_selected = $(this).attr('aria-selected');
        if (tab_selected == 'false') {
            $('.tab-item').attr('aria-selected', 'false');
            $(this).attr('aria-selected', 'true');
            $('.tab-panel').hide();
            $('#' + tab_name).show();
        }
    });

}]);

app.controller('help', ['$scope', '$http', function($scope, $http) {

    $('.navtree-list .navtree-next').click(function() {
        var id = $(this).data('id');
        var name = $(this).data('name');
        $('#navtree').css({
            'left': '-300px'
        });
        $('.subnav-list li:first-child a').attr('aria-selected', 'false');
        $('.subnav-list li').append('<li> <a class="subnav-item" href="#" data-node-id="0" aria-selected="true"> ' + name + ' </a> </li>');
        $('#navtree-' + id).css({
            'display': 'block'
        });
    });

    $('.navtree-list .navtree-back').click(function() {
        var id = $(this).data('id');
        var name = $(this).data('name');
        $('#navtree').css({
            'left': '0px'
        });
        $('.subnav-list li:first-child a').attr('aria-selected', 'true');
        $('.subnav-list li').last().remove();
        $('#navtree-' + id).css({
            'display': 'none'
        });
    });

    $('#help_search').autocomplete({
            source: function(request, response) {
                $.ajax({
                    url: APP_URL + "/ajax_help_search",
                    type: "GET",
                    dataType: "json",
                    data: {
                        term: request.term
                    },
                    success: function(data) {
                        response(data);
                        $(this).removeClass('ui-autocomplete-loading');
                    }
                });
            },
            search: function() {
                $(this).addClass('loading');
            },
            open: function() {
                $(this).removeClass('loading');
            }
        })
        .autocomplete("instance")._renderItem = function(ul, item) {
            if (item.id != 0) {
                $('#help_search').removeClass('ui-autocomplete-loading');
                return $("<li>")
                    .append("<a href='" + APP_URL + "/help/article/" + item.id + "/" + item.question + "' class='article-link article-link-panel link-reset'><div class='hover-item__content'><div class='col-middle-alt article-link-left'><i class='icon icon-light-gray icon-size-2 article-link-icon icon-description'></i></div><div class='col-middle-alt article-link-right'>" + item.value + "</div></div></a>")
                    .appendTo(ul);

            } else {
                $('#help_search').removeClass('ui-autocomplete-loading');
                return $("<li style='pointer-events: none;'>")
                    .append("<span class='article-link article-link-panel link-reset'><div class='hover-item__content'><div class='col-middle-alt article-link-left'><i class='icon icon-light-gray icon-size-2 article-link-icon icon-description'></i></div><div class='col-middle-alt article-link-right'>" + item.value + "</div></div></span>")
                    .appendTo(ul);

            }
        };

}]);

app.controller('reviews_edit_host', ['$scope', '$http', function($scope, $http) {

    $('.next-facet').click(function() {
        $('#double-blind-copy').addClass('hide');
        $('#host-summary').removeClass('hide');
        $('#guest').removeClass('hide');
    });

    $('.review_submit').click(function() {
        var section = $(this).parent().parent().attr('id');

        var data_params = {};

        $('#' + section + '-form input, #' + section + ' textarea').each(function() {
            if ($(this).attr('type') != 'radio') {
                data_params[$(this).attr('name')] = $(this).val();
            } else {
                if ($(this).is(':checked'))
                    data_params[$(this).attr('name')] = $(this).val();
            }
        });

        var id = $('#reservation_id').val();
        if (section == 'host-summary' || section == 'guest') {
            if ($('#review_comments').val() == '') {
                $('[for="review_comments"]').show();
                $('#review_comments').addClass('invalid');
                return false;
            } else {
                $('[for="review_comments"]').hide();
                $('#review_comments').removeClass('invalid');
            }
            if (section == 'host-summary') {
                if (!$('[name="rating"]').is(':checked')) {
                    $('[for="review_rating"]').show();
                    return false;
                } else
                    $('[for="review_rating"]').hide();
            }
        }

        data_params['review_id'] = $('#review_id').val();

        var data = JSON.stringify(data_params);

        $('.review-container').addClass('loading');
        $http.post(APP_URL + '/reviews/edit/' + id, {
            data: data
        }).then(function(response) {
            $('.review-container').removeClass('loading');
            if (response.data.success) {
                if (section == 'host-details' || section == 'guest')
                    window.location.href = APP_URL + '/users/reviews';
                $('#review_id').val(response.data.review_id);
                $('#' + section).addClass('hide');
                $('#' + section).next().removeClass('hide');
            }
        });
    });

}]);

$(document).on('change', '#user_profile_pic', function() {
    $('#ajax_upload_form').submit();
});

// cancel reservation
app.controller('cancel_reservation', ['$scope', '$http', function($scope, $http) {

    $(document).ready(function() {

        $("[id$='-trigger']").click(function() {
            var reservation_id = $(this).attr('id').replace('-trigger', '');
            if (reservation_id != 'header-avatar') {
                $("#reserve_code").val(reservation_id);
                $("#reserve_id").val(reservation_id);
                var id = '#cancel-modal';
                var data_params = {};

                data_params['id'] = reservation_id;

                var data = JSON.stringify(data_params);

                $http.post(APP_URL + '/reservation/cencel_request_send', {
                    data: data
                }).then(function(response) {
                    if (response.data.success == 'false') {
                        var id = '#cancel-modal';
                        $(id).removeClass('hide');
                        $(id).addClass('show');
                        $(id).attr('aria-hidden', 'false');
                    } else {
                        location.reload();
                    }
                });
            }
        });

        $("[id$='-trigger-pending']").click(function() {

            var reservation_id = $(this).attr('id').replace('-trigger-pending', '');
            $("#reserve_code_pending").val(reservation_id);
            $("#reserve_id").val(reservation_id);
            //$("#cancel_reservation_form").attr('action' , APP_URL+'/trips/guest_cancel_pending_reservation')
            var id = '#pending-cancel-modal';
            $(id).removeClass('hide');
            $(id).addClass('show');
            $(id).attr('aria-hidden', 'false');
        });

        $('[data-behavior="modal-close"]').click(function(event) {
            event.preventDefault();
            $('.modal').removeClass('show');
            $('.modal').attr('aria-hidden', 'true');
        });

    });
}]);


app.controller('edit_profile', ['$scope', '$http', function($scope, $http) {
    $scope.users_phone_numbers = [];
    $scope.phone_number_val = [];
    $scope.phone_code_val = [];
    $scope.otp_val = [];

    var phone_numbers_wrapper = $("#phone_numbers_wrapper");

    $http.post(APP_URL + '/users/get_users_phone_numbers', {}).then(function(response) {
        $scope.users_phone_numbers = response.data;
    });

    $scope.add_phone_number = function() {
        phone_numbers_wrapper.addClass('loading');
        new_phone_number = {
            'id': '',
            'phone_number': '',
            'phone_code': $scope.default_phone_code,
            'status': 'Null'
        };
        $scope.users_phone_numbers.push(new_phone_number);
        phone_numbers_wrapper.removeClass('loading');
    }

    $scope.remove_phone_number = function($index) {
        phone_numbers_wrapper.addClass('loading');
        phone_numbers_wrapper.removeClass('loading');
    }

    $scope.update_phone_number = function($index) {
        phone_numbers_wrapper.addClass('loading');
        phone_number_val = $scope.phone_number_val[$index] ? $scope.phone_number_val[$index] : '';
        phone_code_val = $scope.phone_code_val[$index];

        $http.post(APP_URL + '/users/update_users_phone_number', {
            'phone_number': phone_number_val,
            'phone_code': phone_code_val
        }).then(function(response) {
            if (response.data.status == 'Success') {
                $scope.users_phone_numbers[$index].phone_number_error = '';
                $scope.users_phone_numbers = response.data.users_phone_numbers;
                $scope.phone_number_val[$index] = '';
            } else {
                $scope.users_phone_numbers[$index].phone_number_error = response.data.message;
            }
            phone_numbers_wrapper.removeClass('loading');
        });
    }

    $scope.verify_phone_number = function($index) {
        phone_numbers_wrapper.addClass('loading');

        phone_number = $scope.users_phone_numbers[$index];
        otp_val = $scope.otp_val[$index] ? $scope.otp_val[$index] : '';

        $http.post(APP_URL + '/users/verify_users_phone_number', {
            'otp': otp_val,
            'id': phone_number.id
        }).then(function(response) {
            if (response.data.status == 'Success') {
                $scope.users_phone_numbers[$index].otp_error = '';
                $scope.users_phone_numbers = response.data.users_phone_numbers;
                $scope.otp_val[$index] = '';
            } else {
                $scope.users_phone_numbers[$index].otp_error = response.data.message;
            }
            phone_numbers_wrapper.removeClass('loading');
        });
    }

    $scope.remove_phone_number = function($index) {
        phone_numbers_wrapper.addClass('loading');

        phone_number = $scope.users_phone_numbers[$index];

        $http.post(APP_URL + '/users/remove_users_phone_number', {
            'id': phone_number.id
        }).then(function(response) {
            if (response.data.status == 'Success') {
                $scope.users_phone_numbers[$index].phone_number_error = '';
                $scope.users_phone_numbers = response.data.users_phone_numbers;
            } else {
                $scope.users_phone_numbers[$index].phone_number_error = response.data.message;
            }
            phone_numbers_wrapper.removeClass('loading');
        });
    }

    $('.language-link').click(function(e) {
        e.preventDefault();
        $("body").addClass("pos-fix");
        $(".mini-language").show();
    });

    $('.login-close-language').click(function(event) {
        $("body").removeClass("pos-fix");
        $(".mini-language").hide();
    });

    $('.top-home').click(function(event) {
        event.stopPropagation();
    });

    $("#language_save_button").click(function() {
        $('#selected_language').html('');
        $('.language_select').each(function() {
            if ($(this).is(':checked')) {
                $("#selected_language").append('<span class="btn btn-lang space-1">' + $(this).data('name') + '  <a href="javascript:void(0)" class="text-normal" id="remove_language"> <input type="hidden" value=" ' + $(this).val() + '" name="language[]"> <i class="x icon icon-remove" title="Remove from selection"></i></a> </span>');
            }

            $(".mini-language").hide();
            $("body").removeClass("pos-fix");
        });
    });

    $(document).on('click', '[id^="remove_language"]', function() {
        $(this).parent().remove();
    });

}]);
app.controller('Tabsh',['$scope', function ($scope){

    $scope.show = 1;
    $scope.tab1 = true;
   

}]);
