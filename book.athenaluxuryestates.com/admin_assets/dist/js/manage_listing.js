app.controller('manage_listing', ['$scope', '$http', '$compile', function($scope, $http, $compile) {

    $(document).on('click', '.month-nav', function() {
        var month = $(this).attr('data-month');
        var year = $(this).attr('data-year');

        var data_params = {};

        data_params['month'] = month;
        data_params['year'] = year;

        var data = JSON.stringify(data_params);

        $('.ui-datepicker-backdrop').removeClass('hide');
        $('.spinner-next-to-month-nav').addClass('loading');

        $http.post($(this).attr('href').replace('edit_room', 'ajax-edit'), {
            data: data
        }).then(function(response) {
            $("#ajax_container").html($compile(response.data)($scope));
            $('.spinner-next-to-month-nav').removeClass('loading');
            $('.ui-datepicker-backdrop').addClass('hide');
        });
        return false;
    });

    $(document).on('change', '#calendar_dropdown', function() {
        var year_month = $(this).val();
        var year = year_month.split('-')[0];
        var month = year_month.split('-')[1];

        var data_params = {};

        data_params['month'] = month;
        data_params['year'] = year;

        var data = JSON.stringify(data_params);

        $('.ui-datepicker-backdrop').removeClass('hide');
        $('.spinner-next-to-month-nav').addClass('loading');

        $http.post($(this).attr('data-href').replace('edit_room', 'ajax-edit'), {
            data: data
        }).then(function(response) {
            $('.ui-datepicker-backdrop').addClass('hide');
            $('.spinner-next-to-month-nav').removeClass('loading');
            $("#ajax_container").html($compile(response.data)($scope));
        });
        return false;
    });

    /*Start - Calendar Date Selection*/
    $(document).on('click', '.tile', function() {
        if (!$(this).hasClass('other-day-selected') && !$(this).hasClass('selected') && !$(this).hasClass('tile-previous')) {
            var current_tile = $(this).attr('id');
            $('#' + current_tile).addClass('first-day-selected last-day-selected selected');
            $('<div class="tile-selection-container"><div class="tile-selection"></div></div>').insertBefore('#' + current_tile + '> .date');
            var clicked_li = $(this).index();
            var start_top = $(this).position().top + 36,
                start_left = $(this).position().left - 5,
                end_top = start_top + 5,
                end_left = start_left + 85;
            //$('.tile-selection-handle').removeClass('hide');
            $('.days-container').append('<div><div style="left:' + start_left + 'px;top:' + start_top + 'px;" class="tile-selection-handle tile-handle-left"><div class="tile-selection-handle__inner"><span class="tile-selection-handle__ridge"></span><span class="tile-selection-handle__ridge"></span><span class="tile-selection-handle__ridge"></span></div></div></div><div><div style="left: ' + end_left + 'px; top: ' + end_top + 'px;" class="tile-selection-handle tile-handle-right"><div class="tile-selection-handle__inner"><span class="tile-selection-handle__ridge"></span><span class="tile-selection-handle__ridge"></span><span class="tile-selection-handle__ridge"></span></div></div></div>');
            $('.tile').each(function() {
                if (current_tile != $(this).attr('id'))
                    $(this).addClass('other-day-selected');
            });
            calendar_edit_form();
        } else {
            if (!$(this).hasClass('selected')) {

                $('.first-day-selected').removeClass('first-day-selected')
                $('.last-day-selected').removeClass('last-day-selected');
                $('.selected').removeClass('selected');
                $('.tile-selection-container').remove();
                $('.tile-selection-handle').parent().remove();
                $('.other-day-selected').removeClass('other-day-selected');
                $('.calendar-edit-form').addClass('hide');

            }
        }
    });
    $(document).on('mouseup', '.tile-selection-handle, .tile', function() {
        selected_li_status = 0;

        var last_id = $('.selected').last().attr('id');
        var first_id = $('.selected').first().attr('id');

        $('*').removeClass('first-day-selected last-day-selected');
        $('.selected').first().addClass('first-day-selected');
        $('.selected').last().addClass('last-day-selected');

        var position = $('#' + last_id).position();
        var first_position = $('#' + first_id).position();

        if (position != undefined && first_position != undefined) {
            var start_top = first_position.top + 35,
                start_left = first_position.left - 5,
                end_top = position.top + 40,
                end_left = position.left + 80;



        $('.days-container > div > .tile-selection-handle:last').remove();
        $('.days-container > div > .tile-selection-handle:first').remove();

        $('.days-container').append('<div><div style="left:' + start_left + 'px;top:' + start_top + 'px;" class="tile-selection-handle tile-handle-left"><div class="tile-selection-handle__inner"><span class="tile-selection-handle__ridge"></span><span class="tile-selection-handle__ridge"></span><span class="tile-selection-handle__ridge"></span></div></div></div><div><div style="left: ' + end_left + 'px; top: ' + end_top + 'px;" class="tile-selection-handle tile-handle-right"><div class="tile-selection-handle__inner"><span class="tile-selection-handle__ridge"></span><span class="tile-selection-handle__ridge"></span><span class="tile-selection-handle__ridge"></span></div></div></div>');

        calendar_edit_form();
        }
    });


    var selected_li_status = 0;
    var direction = '';

    $(document).on('mousedown', '.tile-selection-handle', function() {
        selected_li_status = 1;
        if ($(this).hasClass('tile-handle-left'))
            direction = 'left';
        else
            direction = 'right';
    });

    var oldx = 0;
    var oldy = 0;

    $(document).on('mouseover', '.tile', function(e) {
        if (e.pageX > oldx && direction == 'right') {
            if (selected_li_status == 1 && !$(this).hasClass('tile-previous')) {
                var id = $(this).attr('id');
                $('#' + id).removeClass('other-day-selected');
                $('#' + id).addClass('selected');

                if (!$('#' + $(this).attr('id') + ' > div').hasClass('tile-selection-container')) {
                    $('<div class="tile-selection-container"><div class="tile-selection"></div></div>').insertBefore('#' + $(this).attr('id') + '> .date');
                }

                var last_index = $('.selected').last().index();
                var first_index = $('.selected').first().index();

                $('*').removeClass('first-day-selected last-day-selected');
                $('.selected').first().addClass('first-day-selected');
                $('.selected').last().addClass('last-day-selected');

                for (var i = (first_index + 1); i <= last_index; i++) {
                    var classd = $(".days-container > .list-unstyled > li:nth-child(" + i + ")").attr('class');
                    if (classd.includes("tile-previous") == false) {
                        var id = $(".days-container > .list-unstyled > li:nth-child(" + i + ")").attr('id');
                        $('#' + id).addClass('selected');
                        $('#' + id).removeClass('other-day-selected');

                        if (!$('#' + id + ' > div').hasClass('tile-selection-container')) {
                            $('<div class="tile-selection-container"><div class="tile-selection"></div></div>').insertBefore('#' + id + '> .date');
                        }
                    } else
                        return false;
                }
            } else if ($(this).hasClass('tile-previous')) {
                $(this).trigger('mouseup');
            }
        } else if (e.pageX < oldx && direction == 'right') {
            if (selected_li_status == 1 && !$(this).hasClass('tile-previous')) {
                var id = $(".days-container > .list-unstyled > li:nth-child(" + $(this).index() + ")").next().next().attr('id');

                var id2 = $(".days-container > .list-unstyled > li:nth-child(" + $(this).index() + ")").next().attr('id');

                $('*').removeClass('first-day-selected last-day-selected');
                $('.selected').first().addClass('first-day-selected');
                $('.selected').last().addClass('last-day-selected');

                $('#' + id).removeClass('selected');
                $('#' + id).addClass('other-day-selected');
                $(this).removeClass('selected');
                $(this).addClass('other-day-selected');
                $('#' + id2 + ' > div.tile-selection-container').remove();
            } else if ($(this).hasClass('tile-previous')) {
                $(this).trigger('mouseup');
            }

            if ($('.selected').length == 0) {
                selected_li_status = 0;
                $('.tile').each(function() {
                    $(this).removeClass('other-day-selected last-day-selected first-day-selected');
                    $('.tile-selection-container').remove();
                    $('.tile-selection-handle').remove();
                });
            }
        }

        if (e.pageX > oldx && direction == 'left') {
            if (selected_li_status == 1 && !$(this).hasClass('tile-previous')) {
                var id = $(".days-container > .list-unstyled > li:nth-child(" + $(this).index() + ")").attr('id');

                var id2 = $(".days-container > .list-unstyled > li:nth-child(" + $(this).index() + ")").attr('id');

                $('*').removeClass('first-day-selected last-day-selected');
                $('.selected').first().addClass('first-day-selected');
                $('.selected').last().addClass('last-day-selected');

                $('#' + id).removeClass('selected');
                $('#' + id).addClass('other-day-selected');
                $(this).removeClass('selected');
                $(this).addClass('other-day-selected');
                $('#' + id2 + ' > div.tile-selection-container').remove();
            } else if ($(this).hasClass('tile-previous')) {
                $(this).trigger('mouseup');
            }
        } else if (e.pageX < oldx && direction == 'left') {
            if (selected_li_status == 1 && !$(this).hasClass('tile-previous')) {
                var id = $(".days-container > .list-unstyled > li:nth-child(" + $(this).index() + ")").next().next().attr('id');

                var id2 = $(".days-container > .list-unstyled > li:nth-child(" + $(this).index() + ")").next().attr('id');

                $('#' + id).addClass('selected');
                $('#' + id).removeClass('other-day-selected');
                $(this).addClass('selected');
                $(this).removeClass('other-day-selected');

                var last_index = $('.selected').last().index();
                var first_index = $('.selected').first().index();

                for (var i = (first_index + 1); i <= last_index; i++) {
                    var classd = $(".days-container > .list-unstyled > li:nth-child(" + i + ")").attr('class');
                    if (classd.includes("tile-previous") == false) {
                        var id = $(".days-container > .list-unstyled > li:nth-child(" + i + ")").attr('id');
                        $('#' + id).addClass('selected');
                        $('#' + id).removeClass('other-day-selected');

                        if (!$('#' + id + ' > div').hasClass('tile-selection-container')) {
                            $('<div class="tile-selection-container"><div class="tile-selection"></div></div>').insertBefore('#' + id + '> .date');
                        }
                        $('#' + id).removeClass('first-day-selected last-day-selected');
                    } else
                        return false;

                }

                $('*').removeClass('first-day-selected last-day-selected');
                $('.selected').first().addClass('first-day-selected');
                $('.selected').last().addClass('last-day-selected');

                if (!$('#' + id + ' > div').hasClass('tile-selection-container')) {
                    $('<div class="tile-selection-container"><div class="tile-selection"></div></div>').insertBefore('#' + id + '> .date');
                }
            } else if ($(this).hasClass('tile-previous')) {
                $(this).trigger('mouseup');
            }

            if ($('.selected').length == 0) {
                selected_li_status = 0;
                $('.tile').each(function() {
                    $(this).removeClass('other-day-selected last-day-selected first-day-selected');
                    $('.tile-selection-container').remove();
                    $('.tile-selection-handle').remove();
                });
            }
        }

        if (e.pageY > oldy && direction == 'right') {
            if (selected_li_status == 1 && !$(this).hasClass('tile-previous')) {
                var id = $(this).attr('id');
                $('#' + id).removeClass('other-day-selected');
                $('#' + id).addClass('selected');

                if (!$('#' + $(this).attr('id') + ' > div').hasClass('tile-selection-container')) {
                    $('<div class="tile-selection-container"><div class="tile-selection"></div></div>').insertBefore('#' + $(this).attr('id') + '> .date');
                }

                var last_index = $('.selected').last().index();
                var first_index = $('.selected').first().index();

                $('*').removeClass('first-day-selected last-day-selected');
                $('.selected').first().addClass('first-day-selected');
                $('.selected').last().addClass('last-day-selected');

                for (var i = (first_index + 1); i <= last_index; i++) {
                    var classd = $(".days-container > .list-unstyled > li:nth-child(" + i + ")").attr('class');
                    if (classd.includes("tile-previous") == false) {
                        var id = $(".days-container > .list-unstyled > li:nth-child(" + i + ")").attr('id');
                        $('#' + id).addClass('selected');
                        $('#' + id).removeClass('other-day-selected');

                        if (!$('#' + id + ' > div').hasClass('tile-selection-container')) {
                            $('<div class="tile-selection-container"><div class="tile-selection"></div></div>').insertBefore('#' + id + '> .date');
                        }
                    } else
                        return false;
                }
            } else if ($(this).hasClass('tile-previous')) {
                $(this).trigger('mouseup');
            }
        }

        if (e.pageY < oldy && direction == 'right') {
            if (selected_li_status == 1 && !$(this).hasClass('tile-previous')) {
                if (!$(this).hasClass('selected')) {
                    var id = $(this).attr('id');
                    var last_index = $(this).index();
                    var first_index = $('.selected').first().index();

                    $('.selected').addClass('other-day-selected');
                    $('.selected').removeClass('selected');

                    for (var i = (first_index + 1); i <= (last_index + 1); i++) {
                        var idsd = $(".days-container > .list-unstyled > li:nth-child(" + i + ")").attr('class');
                        console.log(idsd);
                        for (var i = (first_index + 1); i <= (last_index + 1); i++) {
                            var classd = $(".days-container > .list-unstyled > li:nth-child(" + i + ")").attr('class');
                            if (classd.includes("tile-previous") == false) {
                                var id = $(".days-container > .list-unstyled > li:nth-child(" + i + ")").attr('id');
                                $('#' + id).addClass('selected');
                                $('#' + id).removeClass('other-day-selected');

                                if (!$('#' + id + ' > div').hasClass('tile-selection-container')) {
                                    $('<div class="tile-selection-container"><div class="tile-selection"></div></div>').insertBefore('#' + id + '> .date');
                                }
                            } else
                                return false;
                        }
                    }
                    $('*').removeClass('first-day-selected last-day-selected');
                    $('.selected').first().addClass('first-day-selected');
                    $('.selected').last().addClass('last-day-selected');
                }
            } else if ($(this).hasClass('tile-previous')) {
                $(this).trigger('mouseup');
            }
        }

        if (e.pageY < oldy && direction == 'left') {
            if (selected_li_status == 1 && !$(this).hasClass('tile-previous')) {
                var id = $(this).attr('id');
                $('#' + id).removeClass('other-day-selected');
                $('#' + id).addClass('selected');

                if (!$('#' + $(this).attr('id') + ' > div').hasClass('tile-selection-container')) {
                    $('<div class="tile-selection-container"><div class="tile-selection"></div></div>').insertBefore('#' + $(this).attr('id') + '> .date');
                }

                var last_index = $('.selected').last().index();
                var first_index = $('.selected').first().index();

                $('*').removeClass('first-day-selected last-day-selected');
                $('.selected').first().addClass('first-day-selected');
                $('.selected').last().addClass('last-day-selected');

                for (var i = (first_index + 1); i <= last_index; i++) {
                    var classd = $(".days-container > .list-unstyled > li:nth-child(" + i + ")").attr('class');
                    if (classd.includes("tile-previous") == false) {
                        var id = $(".days-container > .list-unstyled > li:nth-child(" + i + ")").attr('id');
                        $('#' + id).addClass('selected');
                        $('#' + id).removeClass('other-day-selected');

                        if (!$('#' + id + ' > div').hasClass('tile-selection-container')) {
                            $('<div class="tile-selection-container"><div class="tile-selection"></div></div>').insertBefore('#' + id + '> .date');
                        }
                    } else
                        return false;
                }
            } else if ($(this).hasClass('tile-previous')) {
                $(this).trigger('mouseup');
            }
        }

        if (e.pageY > oldy && direction == 'left') {
            if (selected_li_status == 1 && !$(this).hasClass('tile-previous')) {
                if (!$(this).hasClass('selected')) {
                    var id = $(this).attr('id');
                    var first_index = $(this).index();
                    var last_index = $('.selected').last().index();

                    $('.selected').addClass('other-day-selected');
                    $('.selected').removeClass('selected');

                    for (var i = (first_index + 1); i <= (last_index + 1); i++) {
                        var classd = $(".days-container > .list-unstyled > li:nth-child(" + i + ")").attr('class');
                        if (classd.includes("tile-previous") == false) {
                            var id = $(".days-container > .list-unstyled > li:nth-child(" + i + ")").attr('id');
                            $('#' + id).addClass('selected');
                            $('#' + id).removeClass('other-day-selected');

                            if (!$('#' + id + ' > div').hasClass('tile-selection-container')) {
                                $('<div class="tile-selection-container"><div class="tile-selection"></div></div>').insertBefore('#' + id + '> .date');
                            }
                        } else
                            return false;
                    }
                    $('*').removeClass('first-day-selected last-day-selected');
                    $('.selected').first().addClass('first-day-selected');
                    $('.selected').last().addClass('last-day-selected');
                }
            } else if ($(this).hasClass('tile-previous')) {
                $(this).trigger('mouseup');
            }
        }
        oldx = e.pageX;
        oldy = e.pageY;
    });


    function calendar_edit_form() {
        $('.calendar-edit-form').removeClass('hide');

        if ($('.selected').length > 1) {
            $('.calendar-edit-form > form > .panel-body').first().show();
        } else {
            $('.calendar-edit-form > form > .panel-body').first().hide();
        }

        if ($('.selected').hasClass('status-b')) {
            $scope.$apply(function() {
                $scope.segment_status = 'not available';
            });
            // $scope.segment_status = 'not available';
            $('#unavi').addClass("segmented-control__option--selected");
            $('#avi').removeClass("segmented-control__option--selected");
            $('#avi').removeClass("segmented-control__option--selected");
        } else {
            $scope.$apply(function() {
                $scope.segment_status = 'available';
            });
            // $scope.segment_status = 'available';
            $('#avi').addClass("segmented-control__option--selected");
            $('#unavi').removeClass("segmented-control__option--selected");
            $('#unavi').removeClass("segmented-control__option--selected");
        }

        $('#calendar-edit-end').val('');
        $('#calendar-edit-start').val('');
        var start_date = $('.first-day-selected').first().attr('id');
        var end_date = $('.last-day-selected').last().attr('id');

        $scope.calendar_edit_price = $('#' + start_date).find('.price > span:last').text() - 0;
        $('.sidebar-price').val($scope.calendar_edit_price);
        $scope.notes = $('#' + start_date).find('.tile-notes-text').text();
        $scope.isAddNote = ($scope.notes != '') ? true : false;
        if (start_date != end_date) {
            // $("#calendar-edit-start").datepicker({
            //    	dateFormat: "dd-mm-yy",
            //    	minDate: 0,
            //    	onSelect: function (date)
            //    	{
            //    	    var checkout = $("#calendar-edit-start").datepicker('getDate');
            //    	    $('#calendar-edit-end').datepicker('option', 'minDate', checkout);
            //    	    $('#calendar-edit-start').datepicker('option', 'maxDate', checkout);
            //    	    setTimeout(function(){
            //    	        $('#calendar-edit-end').datepicker("show");
            //    	    },20);
            //    	}
            // });

            // $('#calendar-edit-end').datepicker({
            //    	dateFormat: "dd-mm-yy",
            //    	minDate: 1,
            //    	onClose: function ()
            //    	{
            //    	    var checkin = $("#calendar-edit-start").datepicker('getDate');
            //    	    var checkout = $('#calendar-edit-end').datepicker('getDate');
            //    	    $('#calendar-edit-end').datepicker('option', 'minDate', checkout);
            //    	    $('#calendar-edit-start').datepicker('option', 'maxDate', checkout);
            //    	    if (checkout <= checkin)
            //    	    {
            //    	        var minDate = $('#calendar-edit-end').datepicker('option', 'minDate');
            //    	        $('#calendar-edit-end').datepicker('setDate', minDate);
            //    	    }
            //    	}
            // });

            $('#calendar-edit-start').val(change_format(start_date));
            $('#calendar-edit-end').val(change_format(end_date));

            // $('#calendar-edit-start').datepicker('option', 'maxDate', change_format(start_date));
            // $('#calendar-edit-end').datepicker('option', 'minDate', change_format(end_date));
        } else {
            $('#calendar-edit-start').val(change_format(start_date));
            $('#calendar-edit-end').val(change_format(end_date));
        }
    }

    function change_format(date) {
        if (date != undefined) {
            var split_date = date.split('-');
            return split_date[2] + '-' + split_date[1] + '-' + split_date[0];
        }
    }

    $scope.calendar_edit_submit = function(href) {
        console.log('Clicked');
        $scope.calendar_edit_price = $(".get_price ").val() - 0;
        $http.post(href + '/calendar_edit', {
            status: $scope.segment_status,
            start_date: $('#calendar-edit-start').val(),
            end_date: $('#calendar-edit-end').val(),
            price: $scope.calendar_edit_price,
            notes: $scope.notes
        }).then(function(response) {
            var year_month = $('#calendar_dropdown').val();
            var year = year_month.split('-')[0];
            var month = year_month.split('-')[1];

            var data_params = {};

            data_params['month'] = month;
            data_params['year'] = year;

            var data = JSON.stringify(data_params);

            $('.ui-datepicker-backdrop').removeClass('hide');
            $('.spinner-next-to-month-nav').addClass('loading');

            $http.post(href.replace('edit_room', 'ajax-edit'), {
                data: data
            }).then(function(response) {
                $('.ui-datepicker-backdrop').addClass('hide');
                $('.spinner-next-to-month-nav').removeClass('loading');
                $("#ajax_container").html($compile(response.data)($scope));
            });
            return false;
        }, function(response) {
            if (response.status == '300')
                window.location = APP_URL + '/login';
        });
    };

    $(document).on('click', '.modal-close, [data-behavior="modal-close"], .panel-close', function() {
        $('.modal').fadeOut();
        $('.tooltip').css('opacity', '0');
        $('.tooltip').attr('aria-hidden', 'true');
        $('.modal').attr('aria-hidden', 'true');
    });

    /*End - Calendar Date Selection*/
    $(document).on('click', '#export_button', function() {
        $('#export_popup').attr('aria-hidden', 'false');
    });

    $(document).on('click', '#import_button', function() {
        $('#import_popup').attr('aria-hidden', 'false');
    });
}]);
