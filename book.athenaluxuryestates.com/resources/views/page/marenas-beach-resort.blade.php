@extends('template')


@section('main')
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="index.html">Home</a>
                </li>
                <li><a>United States</a></li>
                <li><a>Florida (FL)</a></li>
                <li><a>Miami</a></li>
                <li><a>Sunny Isles Beach</a></li>
                <li class="active">Marenas Beach Resort</li>
            </ul>
            <div class="booking-item-details">
                <header class="booking-item-header">
                    <div class="row">
                        <div class="col-md-9">
                            <h2 class="lh1em">Marenas Beach Resort</h2>
                            <p class="lh1em text-small"><i class="fa fa-map-marker"></i> 18683 Collins Ave, Sunny Isles Beach, FL 33160, USA</p>
                        </div>
                        <div class="col-md-3">
                            <p class="booking-item-header-price"><small>price from</small>  <span class="text-lg">$159</span>avg/night</p>
                        </div>
                    </div>
                </header>
                <div class="row">
                    <div class="col-md-12">
                        <div class="tabbable booking-details-tabbable">
                            <ul class="nav nav-tabs" id="myTab">
                                <li class="active"><a href="#tab-1" data-toggle="tab"><i class="fa fa-camera"></i>Photos</a>
                                </li>
                                <li><a href="#tab-2" data-toggle="tab"><i class="fa fa-info-circle"></i>About</a>
                                </li>
                                <li><a href="#google-map-tab" data-toggle="tab"><i class="fa fa-map-marker"></i>On the Map</a>
                                </li>
                                <li><a href="#tab-5" data-toggle="tab"><i class="fa fa-asterisk"></i>Facilities</a>
                                </li>
                                <li><a href="#tab-4" data-toggle="tab"><i class="fa fa-info-circle"></i>Policies</a>
                                </li>
                            </ul>
                            
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="tab-1">
                                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                <li data-target="#myCarousel" data-slide-to="1"></li>
                                <li data-target="#myCarousel" data-slide-to="2"></li>
                                <li data-target="#myCarousel" data-slide-to="3"></li>
                                 <li data-target="#myCarousel" data-slide-to="4"></li>
                                <li data-target="#myCarousel" data-slide-to="5"></li>
                                <li data-target="#myCarousel" data-slide-to="6"></li>
                                <li data-target="#myCarousel" data-slide-to="7"></li>
                                <li data-target="#myCarousel" data-slide-to="8"></li>
                                <li data-target="#myCarousel" data-slide-to="9"></li>
                                <li data-target="#myCarousel" data-slide-to="10"></li>
                                <li data-target="#myCarousel" data-slide-to="11"></li>
                                 <li data-target="#myCarousel" data-slide-to="12"></li>
                                <li data-target="#myCarousel" data-slide-to="13"></li>
                                <li data-target="#myCarousel" data-slide-to="14"></li>
                                <li data-target="#myCarousel" data-slide-to="15"></li>
                                <li data-target="#myCarousel" data-slide-to="16"></li>
                                <li data-target="#myCarousel" data-slide-to="17"></li>
                                <li data-target="#myCarousel" data-slide-to="18"></li>
                                <li data-target="#myCarousel" data-slide-to="19"></li>
                                <li data-target="#myCarousel" data-slide-to="20"></li>
                                <li data-target="#myCarousel" data-slide-to="21"></li>
                                <li data-target="#myCarousel" data-slide-to="22"></li>
                                <li data-target="#myCarousel" data-slide-to="23"></li>
                                <li data-target="#myCarousel" data-slide-to="24"></li>
                                <li data-target="#myCarousel" data-slide-to="0"></li>
                                </ol>
                                <div class="carousel-inner">
                                <div class="item active">
                                <img src="img/marenas/1.jpg" style="width:100%; height:550px;" data-src="" alt="">
                                </div>
                                    <div class="item">
                                        <img src="img/marenas/2.jpg" style="width:100%; height:550px" data-src="" alt="">
                                    </div>
                                      <div class="item">
                                        <img src="img/marenas/3.jpg" style="width:100%; height:550px" data-src="" alt="">
                                    </div>
                                      <div class="item">
                                         <img src="img/marenas/4.jpg" style="width:100%; height:550px" data-src="" alt="">
                                    </div>
                                    <div class="item">
                                         <img src="img/marenas/5.jpg" style="width:100%; height:550px" data-src="" alt="">
                                    </div>
                                    <div class="item">
                                        <img src="img/marenas/6.jpg" style="width:100%; height:550px" data-src="" alt="">
                                    </div>
                                    <div class="item">
                                         <img src="img/marenas/7.jpg" style="width:100%; height:550px" data-src="" alt="">
                                    </div>
                                    <div class="item">
                                         <img src="img/marenas/8.jpg" style="width:100%; height:550px" data-src="" alt="">
                                    </div>
                                    <div class="item">
                                         <img src="img/marenas/9.jpg" style="width:100%; height:550px" data-src="" alt="">
                                    </div>
                                    <div class="item">
                                         <img src="img/marenas/10.jpg" style="width:100%; height:550px" data-src="" alt="">
                                    </div>
                                    <div class="item">
                                         <img src="img/marenas/11.jpg" style="width:100%; height:550px" data-src="" alt="">
                                    </div>
                                    <div class="item">
                                         <img src="img/marenas/12.jpg" style="width:100%; height:550px" data-src="" alt="">
                                    </div>
                                    <div class="item">
                                         <img src="img/marenas/13.jpg" style="width:100%; height:550px" data-src="" alt="">
                                    </div>
                                    <div class="item">
                                         <img src="img/marenas/14.jpg" style="width:100%; height:550px" data-src="" alt="">
                                    </div>
                                    <div class="item">
                                         <img src="img/marenas/15.jpg" style="width:100%; height:550px" data-src="" alt="">
                                    </div>
                                    <div class="item">
                                         <img src="img/marenas/16.jpg" style="width:100%; height:550px" data-src="" alt="">
                                    </div>
                                    <div class="item">
                                         <img src="img/marenas/17.jpg" style="width:100%; height:550px" data-src="" alt="">
                                    </div>
                                    <div class="item">
                                         <img src="img/marenas/18.jpg" style="width:100%; height:550px" data-src="" alt="">
                                    </div>
                                    <div class="item">
                                         <img src="img/marenas/19.jpg" style="width:100%; height:550px" data-src="" alt="">
                                    </div>
                                    <div class="item">
                                         <img src="img/marenas/20.jpg" style="width:100%; height:550px" data-src="" alt="">
                                    </div>
                                    <div class="item">
                                         <img src="img/marenas/21.jpg" style="width:100%; height:550px" data-src="" alt="">
                                    </div>
                                    <div class="item">
                                         <img src="img/marenas/22.jpg" style="width:100%; height:550px" data-src="" alt="">
                                    </div>
                                    <div class="item">
                                         <img src="img/marenas/23.jpg" style="width:100%; height:550px" data-src="" alt="">
                                    </div>
                                    <div class="item">
                                         <img src="img/marenas/24.jpg" style="width:100%; height:550px" data-src="" alt="">
                                    </div>
                                </div>
                               </div>
                                        </div>
                                
                                <div class="tab-pane fade" id="tab-2">
                                    <div class="mt20">
                                        <p>Located in the heart of Sunny Isles Beach, this hotel is within 6 mi (10 km) of Aventura Mall, Promenade Shops, and Gulfstream Park Racing and Casino. Bal Harbour Shops and Surfside Beach are also within 6 mi (10 km).

                                        <p>A restaurant, an outdoor pool, and a 24-hour health club are available at this hotel. WiFi in public areas is free. Additionally, a bar/lounge, a poolside bar, and a coffee shop/café are onsite.</p>

                                        <p>All 355 rooms offer free WiFi, 24-hour room service, and plasma TVs with cable channels. Coffee makers and free newspapers are among the other amenities available to guests.</p>

                                        <p><b>Where to Eat</b><br>
                                        Breakfast is available for a surcharge and served each morning between 6 AM and 9:30 AM.</p>

                                        <p><b>CARACOL</b><br>
                                        This restaurant specializes in American cuisine and serves breakfast, lunch, and dinner. Guests can enjoy drinks at the bar. 24-hour room service is available.</p>
                                    </div>
     
                                </div>
                                <div class="tab-pane fade" id="google-map-tab">
                                    <div id="map-canvas" style="width:100%; height:550px;"></div>
                                </div>
                                
                              
                                
                                <div class="tab-pane fade" id="tab-5">
                                    <div class="row mt20">
                                        <div class="col-md-4">
                                            <ul class="booking-item-features booking-item-features-expand mb30 clearfix">
                                                <li><i class="fa fa-check"></i><span class="booking-item-feature-title">Free Wi-Fi</span>
                                                </li>
                                                <li><i class="fa fa-check"></i><span class="booking-item-feature-title">Parking</span>
                                                </li>
                                                <li><i class="fa fa-check"></i><span class="booking-item-feature-title">HDTV</span>
                                                </li>
                                                <li><i class="fa fa-check"></i><span class="booking-item-feature-title">Fitness Center</span>
                                                </li>
                                                <li><i class="fa fa-check"></i><span class="booking-item-feature-title">Swimming Pool</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-4">
                                            <ul class="booking-item-features booking-item-features-expand mb30 clearfix">
                                                <li><i class="fa fa-check"></i><span class="booking-item-feature-title">Restaurant</span>
                                                </li>
                                                <li><i class="fa fa-check"></i><span class="booking-item-feature-title">Fitness Center</span>
                                                </li>
                                                <li><i class="fa fa-check"></i><span class="booking-item-feature-title">Refrigerator</span>
                                                </li>
                                                <li><i class="fa fa-check"></i><span class="booking-item-feature-title">Barbecue grill(s)</span>
                                                </li>
                                                <li><i class="fa fa-check"></i><span class="booking-item-feature-title">Tennis</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-4">
                                            <ul class="booking-item-features booking-item-features-expand mb30 clearfix">
                                                <li><i class="fa fa-check"></i><span class="booking-item-feature-title">Children Activites</span>
                                                </li>
                                                <li><i class="fa fa-check"></i><span class="booking-item-feature-title">Playground</span>
                                                </li>
                                                <li><i class="fa fa-check"></i><span class="booking-item-feature-title">Living Room</span>
                                                </li>
                                                <li><i class="fa fa-check"></i><span class="booking-item-feature-title">Laundry</span>
                                                </li>
                                                <li><i class="fa fa-check"></i><span class="booking-item-feature-title">Front desk (limited hours)</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                
                                 <div class="tab-pane fade" id="tab-4">
                                    <div class="mt20">
                                        <p><b>CHECK-IN AND CHECK-OUT</b><br>
                                        • Check In : 16:00 PM<br>
                                        • Check-in time ends at midnight<br>
                                        • Check Out : 11:00 AM<br>
                                        • Minimum check-in age is 21<br></p>
                                        
                                        <p><b>Special check-in instructions:</b><br>
                                        To make arrangements for check-in please contact the property at least 24 hours before arrival using the information on the booking confirmation.</p>

                                        <p><b>CANCELLATION POLICY</b><br>
                                        • There are no refunds to bookings.<br>
                                        • There are no cancellations allowed.</p>
                                        
                                        <p><b>Smoking / Pet Policy</b><br>
                                        • Smoking not allowed.<br>
                                        • Pets not allowed.</p>
                                        
                                        <p><b>DEPOSITS AND FEE</b><br>
                                        • Payment Mode : VISA, MASTERCARD, AMEX, PAYPAL, BANK TRANSFER<br>
                                        • CLEANING FEE : Varies depending on room types<br>
                                        • SECURITY DEPOSIT : $500<br>
                                        • 100% downpayment is requested to book this unit.<br>
                                        • For reservations longer than a week, Guest may be able to pay 50% up front and 50% 3 weeks before arrival.</p>
                                        
                                        <p><b>Children and extra beds</b><br>
                                        • Children are welcome.<br>
                                        • No rollaway/extra beds available<br>
                                        • No cribs (infant beds) available</p>
                                        
                                        <p><b>You need to know</b><br>
                                        • Extra-person charges may apply and vary depending on property policy.<br>
                                        • Government-issued photo identification and a credit card or cash deposit are required at check-in for incidental charges.<br>
                                        • Special requests are subject to availability upon check-in and may incur additional charges.<br>
                                        • Special requests cannot be guaranteed.<br>
                                        • Only service animals are allowed</p>
                                        
                                        <p><b>Resort Fees</b><br>
                                        You'll be asked to pay USD 27.00 per accommodation, per stay. The resort fee includes:<br>
                                        • Pool access, Beach access, Beach loungers, Beach towels, Health club access, Fitness center access, Business center/computer access, Internet access, Phone calls, In-room coffee
                                        <p>We have included all charges provided to us by the property. However, charges can vary, for example, based on length of stay or the room you book.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="gap gap-small"></div>
                <div class="row">
                    <div class="col-md-12">
                        <ul class="booking-list">
                            <li>
                                <a class="booking-item" href="rooms/mbvcs.html">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <img src="img/octhumbs/oc12.jpg" alt="" title="Marenas Beach Resort" />
                                        </div>
                                        <div class="col-md-6">
                                            <h5 class="booking-item-title">Marenas Bay View Classic Studio</h5>
                                            <p class="text-small"><b>Internet</b> - Free WiFi<br>
                                                <b>Entertainment</b> - Plasma TV with cable channels<br>
                                                <b>Food & Drink</b> - Coffee/tea maker and 24-hour room service<br>
                                                <b>Bathroom</b> - Designer toiletries and shower/tub combination<br>
                                                <b>Comfort</b> - Climate-controlled heating and air conditioning<br>
                                                <b>Need to Know</b> - Non-Smoking, Cleaning Fee : $141.25 per stay</p>
                                            <ul class="booking-item-features booking-item-features-sign clearfix">
                                                <li rel="tooltip" data-placement="top" title="Adults Occupancy"><i class="fa fa-male"></i><span class="booking-item-feature-sign">x 2</span>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Beds"><i class="im im-bed"></i><span class="booking-item-feature-sign">x 1</span>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Room footage (square feet)"><i class="im im-width"></i><span class="booking-item-feature-sign">199</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-3"><span class="booking-item-price">$159</span><span>avg/night</span><br>
                                            <span class="btn btn-primary">Book This Condo Now</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            
                            
                            <li>
                                <a class="booking-item" href="rooms/mbvd1bs.html">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <img src="img/octhumbs/oc13.jpg" alt="Marenas" title="Marenas Beach Resort" />
                                        </div>
                                        <div class="col-md-6">
                                            <h5 class="booking-item-title">Marenas Bay View Deluxe 1 Bedroom Suite</h5>
                                            <p class="text-small"><b>Layout</b> - Bedroom and sitting area<br>
                                                <b>Internet</b> - Free WiFi<br>
                                                <b>Entertainment</b> - Plasma TV with cable channels<br>
                                                <b>Food & Drink</b> - Kitchen with refrigerator, microwave, and coffee/tea maker<br>
                                                <b>Bathroom</b> - Designer toiletries and shower/tub combination<br>
                                                <b>Comfort</b> - Climate-controlled heating and air conditioning<br>
                                                <b>Need to Know</b> - Non-Smoking, Cleaning Fee : $141.25 per stay</p>
                                            <ul class="booking-item-features booking-item-features-sign clearfix">
                                                <li rel="tooltip" data-placement="top" title="Adults Occupancy"><i class="fa fa-male"></i><span class="booking-item-feature-sign">x 5</span>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Beds"><i class="im im-bed"></i><span class="booking-item-feature-sign">x 2</span>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Room footage (square feet)"><i class="im im-width"></i><span class="booking-item-feature-sign">900</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-3"><span class="booking-item-price">$199</span><span>/night</span><br>
                                            <span class="btn btn-primary">Book This Condo Now</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            
                            <li>
                                <a class="booking-item" href="rooms/bsvd1bs.html">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <img src="img/octhumbs/oc14.jpg" alt="Marenas" title="Marenas Beach Resort" />
                                        </div>
                                        <div class="col-md-6">
                                            <h5 class="booking-item-title">Bay & Sea View DLX 1 Bedroom Suite</h5>
                                            <p class="text-small"><b>Internet</b> - Free WiFi<br>
                                                <b>Entertainment</b> - Plasma TV with cable channels<br>
                                                <b>Food & Drink</b> - Coffee/tea maker and 24-hour room service<br>
                                                <b>Bathroom</b> - Designer toiletries and shower/tub combination<br>
                                                <b>Comfort</b> - Climate-controlled heating and air conditioning<br>
                                                <b>Need to Know</b> - Non-Smoking, Cleaning Fee : $141.25 per stay</p>
                                            <ul class="booking-item-features booking-item-features-sign clearfix">
                                                <li rel="tooltip" data-placement="top" title="Adults Occupancy"><i class="fa fa-male"></i><span class="booking-item-feature-sign">x 5</span>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Beds"><i class="im im-bed"></i><span class="booking-item-feature-sign">x 2</span>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Room footage (square feet)"><i class="im im-width"></i><span class="booking-item-feature-sign">900</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-3"><span class="booking-item-price">$225</span><span>avg/night</span><br>
                                            <span class="btn btn-primary">Book This Condo Now</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a class="booking-item" href="rooms/mov1bs.html">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <img src="img/octhumbs/oc15.jpg" alt="Marenas" title="Marenas Beach Resort" />
                                        </div>
                                        <div class="col-md-6">
                                            <h5 class="booking-item-title">Marenas Ocean View 1 Bedroom Suite</h5>
                                            <p class="text-small"><b>Layout</b> - Bedroom and sitting area<br>
                                                <b>Internet</b> - Free WiFi<br>
                                                <b>Entertainment</b> - Plasma TV with cable channels<br>
                                                <b>Food & Drink</b> - Coffee/tea maker and 24-hour room service<br>
                                                <b>Bathroom</b> - Designer toiletries and shower/tub combination<br>
                                                <b>Comfort</b> - Climate-controlled heating and air conditioning<br>
                                                <b>Need to Know</b> - Non-Smoking, Cleaning Fee : $141.25 per stay</p>
                                            <ul class="booking-item-features booking-item-features-sign clearfix">
                                                <li rel="tooltip" data-placement="top" title="Adults Occupancy"><i class="fa fa-male"></i><span class="booking-item-feature-sign">x 5</span>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Beds"><i class="im im-bed"></i><span class="booking-item-feature-sign">x 2</span>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Room footage (square feet)"><i class="im im-width"></i><span class="booking-item-feature-sign">900</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-3"><span class="booking-item-price">$199</span><span>avg/night</span><br>
                                            <span class="btn btn-primary">Book This Condo Now</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            
                                            
                            <li>
                                <a class="booking-item" href="rooms/bsv2bs.html">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <img src="img/octhumbs/oc16.jpg" alt="Marenas" title="Marenas Beach Resort" />
                                        </div>
                                        <div class="col-md-6">
                                            <h5 class="booking-item-title">Bay & Sea View 2 Bedroom Suite</h5>
                                            <p class="text-small"><b>Internet</b> - Free WiFi<br>
                                                <b>Entertainment</b> - Plasma TV with cable channels<br>
                                                <b>Food & Drink</b> - Coffee/tea maker and 24-hour room service<br>
                                                <b>Bathroom</b> - Designer toiletries and shower/tub combination<br>
                                                <b>Practical</b> - Cribs/infant beds available on request<br>
                                                <b>Comfort</b> - Climate-controlled heating and air conditioning<br>
                                                <b>Need to Know</b> - Non-Smoking, Cleaning Fee : $224.87 per stay</p>
                                            <ul class="booking-item-features booking-item-features-sign clearfix">
                                                <li rel="tooltip" data-placement="top" title="Adults Occupancy"><i class="fa fa-male"></i><span class="booking-item-feature-sign">x 10</span>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Beds"><i class="im im-bed"></i><span class="booking-item-feature-sign">x 4</span>
                                                </li>
                                                <li rel="tooltip" data-placement="top" title="Room footage (square feet)"><i class="im im-width"></i><span class="booking-item-feature-sign">1100</span>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="col-md-3"><span class="booking-item-price">$315</span><span>avg/night</span><br>
                                            <span class="btn btn-primary">Book This Condo Now</span>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="gap gap-small"></div>
        </div>
      @stop

