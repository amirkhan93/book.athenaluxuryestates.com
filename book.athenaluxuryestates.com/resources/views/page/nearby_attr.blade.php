@extends('template')
<!-- Web Fonts 
	================================================== -->
	<link
		href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
		rel="stylesheet" />
	<link
		href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
		rel="stylesheet">
		<link rel="stylesheet" href="https://cdn.materialdesignicons.com/3.8.95/css/materialdesignicons.min.css">

	<!-- CSS
	================================================== -->
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/bootstrap.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/font-awesome.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/ionicons.min.css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/jquery.fancybox.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/owl.carousel.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/owl.transitions.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/style.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/colors/color.css" />
	<style>
	    .hero-center-section{
	            z-index: 999;
	                top: 65%;
	    }
	    .slide__figure{
	            grid-area: inherit;
	    }
	</style>
@section('main')

    <div class="section background-dark list_section ">
        <div class="hero-center-section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 mt-3 parallax-fade-top">
                        <div class="booking-hero-wrap">
                            	<h1 class="hero-text">Nearby Attractions<br>
								<small>Curated and hosted by experts for a Lux Level Experience.</small></h1>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
       

        <div class="slideshow list-slideshow">
            <div class="slide slide--current parallax-top">
                <figure class="slide__figure">
                    <div class="slide__figure-inner">
                        <div class="slide__figure-img"></div>
                        <div class="slide__figure-reveal"></div>
                    </div>
                </figure>
            </div>
        </div>
    </div>
    
    <div class="section padding-top-bottom over-hide background-grey nearby_attractions_section">
        <div class="container">
            <div class="nearby_attractions_main">
                <ul class="list-unstyled">
                  <li class="media">
                   
                    <div class="media-body">
                         <img src="{{url()}}/resources/assets/img/near1.jpg" class="mr-3" alt="...">
                      <h5 class="mt-0 mb-1">Piestewa Peak (Sqaw Peak): <small>(32nd st trailhead is 1.2 miles from house)</small></h5>
                      <p>
                          Quartz Ridge in the Phoenix Mountain Preserve (also known as Trail 8A) is a great escape getaway trail that can be done as a single track or combined with many intersecting trails. It is a very popular trail for hikers, trail runners, pet walkers, sky gawkers and more!
                      </p>
                      <p>
                          The trailhead sits in a small cul-de-sac where 32nd St dead ends at the intersection of Lincoln Drive. It is a very small parking lot, so plan ahead and arrive early - especially for sunrise and sunset jaunts. Be careful parking across the street in the shopping center, as there are signs everywhere warning against hiker parking (pick up a water or snack before the hike and now you're an official customer!).
                      </p>
                      <p>
                          The classic Quartz Ridge hike is a straight forward out-and-back 2.2 mile hike, that meanders it's way through the beautiful Sonoran desert, littered with quartz all around you and giant granite boulders. Around each corner, you will constantly be rewarded with ever-changing views of the city and surrounding mountains (Camelback, South Mountain, Papago Park). You will be gaining 415 ft, so it's enough to get the heart pumping, definitely doable as a run for the more fit, and easy enough to bring along the out-of-towners!
                      </p>
                      <p>
                          The trail itself glides gently into the hike along the west side of a wash. At .2 miles you reach a junction with Trail 200 (Mojave Trail) which veers to the left. Around .7 miles, the trail narrows and you begin your climb. There are a few switchbacks and a moderate climb to a saddle at 1.1 miles. This is the official saddle of Quartz Ridge and the turnaround point for the out-and-back. From here, you may also connect to a few trails and make the adventure your own.
                      </p>
                      <p>
                          It's a beautiful scene at the saddle, tucked away from the bustling city that surrounds you and a bench to soak it all in.

                      </p>
                    </div>
                  </li>
                  <li class="media my-4">
                   
                    <div class="media-body">
                          <img src="{{url()}}/resources/assets/img/near2.jpg" class="mr-3" alt="...">
                      <h5 class="mt-0 mb-1">Echo canyon Recreation Area and Camelback Mountain trailhead: <small>(2 miles from house)</small></h5>
                     <p>
                         This is visible from almost anywhere in Phoenix with 360 degree views. It's an intense workout, with rock climbing almost the whole way up and great photo opportunities with plenty of wildlife.
                     </p>
                     <p>
                         If you live near or around Phoenix, you know that Camelback Mountain is pretty hard to miss. Due to the mountain's height, this hike isn't exactly what you would call easy. There are two sides of the mountain you can hike, Echo Canyon and Cholla Trail. Echo Canyon is shorter, but still an extremely difficult hike. Cholla Trail is longer and also strenuous.
                     </p>
                     <p>
                         To hike Echo Canyon, find a dirt path from the parking lot that leads up the side of the mountain. Once you get to the end of the dirt path, it's nothing but large boulders the rest of the way, making it a great workout but also very tiring. There are rails along part of the hike to hold onto, but most of it is straight rock climbing. There is also an abundance of wildlife, but be very cautious for rattle snakes – it is common to spot one from time to time while hiking.
                     </p>
                     <p>
                         Once you reach the top, the 360 degree views of the entire city are amazing. Makes the climb well worth it!
                     </p>
                    </div>
                  </li>
                  <li class="media">
                   
                    <div class="media-body">
                         <img src="{{url()}}/resources/assets/img/near3.jpg" class="mr-3" alt="...">
                      <h5 class="mt-0 mb-1">L'Amore Italian Restaurant: <small>(1.2 miles from house)</small></h5>
                      <p>
                          L'Amore Restaurant is a family owned and operated neighborhood Italian restaurant located in the heart of Phoenix. We offer fine dining with a family atmosphere. We personally operate the restaurant being there to meet and greet each guest as if they were entering our own home.
Here's a secret - we are told we have the best Cioppino in town!
                      </p>
                      <p>
                          Enjoy the formal room, the friendly bar with great live soft music from our musician Jimmy who plays on Thursday - Saturday night or sit on in our beautiful tree lined patio over looking the Phoenix Mountain Preserves. If you are on your way home call us for a takeout dinner, we are happy to serve your every need - or if you're thinking of a holiday party set up an appointment to speak to us about our catering.  
                      </p>
                      <p>
                          Ask for Jason and tell him Pearl sent you!
                      </p>
                    </div>
                  </li>
                  <li class="media">
                   
                    <div class="media-body">
                         <img src="{{url()}}/resources/assets/img/near4.png" class="mr-3" alt="...">
                      <h5 class="mt-0 mb-1">AMC Esplanade: <small>(3.7 miles from house)</small></h5>
                      <p>
                         This luxury theater has everything from luxury reclining seats, to an amazing menu, and a full bar!  They will bring your selections to your seat while you are enjoying your favorite new movie!

                      </p>
                    </div>
                  </li>
                   <li class="media">
                   
                    <div class="media-body">
                         <img src="{{url()}}/resources/assets/img/near5.jpg" class="mr-3" alt="...">
                      <h5 class="mt-0 mb-1">Old Town Scottsdale: <small>(6.3 miles from house)</small></h5>
                      <p>
                        You’ll find plenty of things to do in Old Town Scottsdale – by day or night! Spend the day browsing the Fifth Avenue shops, trying on Native American jewelry in Historic Old Town or splurging at Scottsdale Fashion Square, the Southwest’s largest retail destination. After dark, dine at one of Old Town’s renowned restaurants, check out the weekly Scottsdale ArtWalk, or hit the clubs in the Entertainment District. Best of all, you can park once and explore all of the city’s downtown hub on the free Scottsdale Trolley!

                      </p>
                    </div>
                  </li>
                </ul>
            </div>
            </div>
            
            </div>

@stop