@extends('template')
<!-- Web Fonts 
	================================================== -->
	<link
		href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
		rel="stylesheet" />
	<link
		href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
		rel="stylesheet">
		<link rel="stylesheet" href="https://cdn.materialdesignicons.com/3.8.95/css/materialdesignicons.min.css">

	<!-- CSS
	================================================== -->
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/bootstrap.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/font-awesome.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/ionicons.min.css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/jquery.fancybox.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/owl.carousel.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/owl.transitions.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/style.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/colors/color.css" />
@section('main')

<main role="main" id="site-content" class="about_main">

<div class="page-container-responsive">
   <div class="row-space-top-6 row-space-16 text-wrap">
      <h2 style="text-align: center; "><b>About Bean D'Vine Luxury Vacation Homes</b></h2>
      <div class="text-copy">
         <p>
             We at Bean D'Vine Luxury Vacation Homes believe that life is made of moments, and we need to celebrate these moments.  Whether it's sitting down with a cup of coffee and enjoying the sunrise or laughing with friends in the evening with a glass of wine, or any of the moments in between, we want to help you cherish the moments of life.  Our luxury vacation homes excel on making each moment count, from your first sip of coffee to the last drop wine.  Welcome to Bean D’Vine!
         </p>
      </div>
   </div>
</div>

</main>

@stop