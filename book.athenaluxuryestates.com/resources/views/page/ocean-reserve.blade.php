@extends('template')


@section('main')
    <div class="container">
        <ul class="breadcrumb">
            <li><a href="index.html">Home</a>
            </li>
            <li><a>United States</a></li>
            <li><a>Florida (FL)</a></li>
            <li><a>Miami</a></li>
            <li><a>Sunny Isles Beach</a></li>
            <li class="active">Ocean Reserve Luxury Condos</li>
        </ul>
        <div class="booking-item-details">
            <header class="booking-item-header">
                <div class="row">
                    <div class="col-md-9">
                        <h2 class="lh1em">Ocean Reserve Luxury Condos</h2>
                        <p class="lh1em text-small"><i class="fa fa-map-marker"></i> 19370 Collins Ave, Sunny Isles Beach, FL 33160, USA</p>
                    </div>
                    <div class="col-md-3">
                        <p class="booking-item-header-price"><small>price from</small>  <span class="text-lg">$119</span>avg/night</p>
                    </div>
                </div>
            </header>
            <div class="row">
                <div class="col-md-12">
                    <div class="tabbable booking-details-tabbable">
                        <ul class="nav nav-tabs" id="myTab">
                            <li class="active"><a href="#tab-1" data-toggle="tab"><i class="fa fa-camera"></i>Photos</a>
                            </li>
                            <li><a href="#tab-2" data-toggle="tab"><i class="fa fa-info-circle"></i>About</a>
                            </li>
                            <li><a href="#google-map-tab" data-toggle="tab"><i class="fa fa-map-marker"></i>On the Map</a>
                            </li>
                            <li><a href="#tab-5" data-toggle="tab"><i class="fa fa-asterisk"></i>Facilities</a>
                            </li>
                            <li><a href="#tab-4" data-toggle="tab"><i class="fa fa-info-circle"></i>Policies</a>
                            </li>
                        </ul>

                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tab-1">
                                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                                    <ol class="carousel-indicators">
                                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                        <li data-target="#myCarousel" data-slide-to="1"></li>
                                        <li data-target="#myCarousel" data-slide-to="2"></li>
                                        <li data-target="#myCarousel" data-slide-to="3"></li>
                                        <li data-target="#myCarousel" data-slide-to="4"></li>
                                        <li data-target="#myCarousel" data-slide-to="5"></li>
                                        <li data-target="#myCarousel" data-slide-to="6"></li>
                                        <li data-target="#myCarousel" data-slide-to="7"></li>
                                        <li data-target="#myCarousel" data-slide-to="8"></li>
                                        <li data-target="#myCarousel" data-slide-to="9"></li>
                                        <li data-target="#myCarousel" data-slide-to="10"></li>
                                        <li data-target="#myCarousel" data-slide-to="11"></li>
                                        <li data-target="#myCarousel" data-slide-to="12"></li>
                                        <li data-target="#myCarousel" data-slide-to="13"></li>
                                        <li data-target="#myCarousel" data-slide-to="14"></li>
                                        <li data-target="#myCarousel" data-slide-to="15"></li>
                                        <li data-target="#myCarousel" data-slide-to="16"></li>
                                        <li data-target="#myCarousel" data-slide-to="17"></li>
                                        <li data-target="#myCarousel" data-slide-to="18"></li>
                                        <li data-target="#myCarousel" data-slide-to="19"></li>
                                        <li data-target="#myCarousel" data-slide-to="20"></li>
                                        <li data-target="#myCarousel" data-slide-to="21"></li>
                                        <li data-target="#myCarousel" data-slide-to="22"></li>
                                        <li data-target="#myCarousel" data-slide-to="23"></li>
                                        <li data-target="#myCarousel" data-slide-to="24"></li>
                                        <li data-target="#myCarousel" data-slide-to="25"></li>
                                        <li data-target="#myCarousel" data-slide-to="26"></li>
                                        <li data-target="#myCarousel" data-slide-to="0"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                        <div class="item active">
                                            <img src="img/oceanreserve/1.jpg" style="width:100%; height:550px;" data-src="" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="img/oceanreserve/2.jpg" style="width:100%; height:550px" data-src="" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="img/oceanreserve/3.jpg" style="width:100%; height:550px" data-src="" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="img/oceanreserve/4.jpg" style="width:100%; height:550px" data-src="" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="img/oceanreserve/5.jpg" style="width:100%; height:550px" data-src="" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="img/oceanreserve/6.jpg" style="width:100%; height:550px" data-src="" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="img/oceanreserve/7.jpg" style="width:100%; height:550px" data-src="" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="img/oceanreserve/8.jpg" style="width:100%; height:550px" data-src="" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="img/oceanreserve/9.jpg" style="width:100%; height:550px" data-src="" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="img/oceanreserve/10.jpg" style="width:100%; height:550px" data-src="" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="img/oceanreserve/11.jpg" style="width:100%; height:550px" data-src="" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="img/oceanreserve/12.jpg" style="width:100%; height:550px" data-src="" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="img/oceanreserve/13.jpg" style="width:100%; height:550px" data-src="" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="img/oceanreserve/14.jpg" style="width:100%; height:550px" data-src="" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="img/oceanreserve/15.jpg" style="width:100%; height:550px" data-src="" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="img/oceanreserve/16.jpg" style="width:100%; height:550px" data-src="" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="img/oceanreserve/17.jpg" style="width:100%; height:550px" data-src="" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="img/oceanreserve/18.jpg" style="width:100%; height:550px" data-src="" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="img/oceanreserve/19.jpg" style="width:100%; height:550px" data-src="" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="img/oceanreserve/20.jpg" style="width:100%; height:550px" data-src="" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="img/oceanreserve/21.jpg" style="width:100%; height:550px" data-src="" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="img/oceanreserve/22.jpg" style="width:100%; height:550px" data-src="" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="img/oceanreserve/23.jpg" style="width:100%; height:550px" data-src="" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="img/oceanreserve/24.jpg" style="width:100%; height:550px" data-src="" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="img/oceanreserve/25.jpg" style="width:100%; height:550px" data-src="" alt="">
                                        </div>
                                        <div class="item">
                                            <img src="img/oceanreserve/26.jpg" style="width:100%; height:550px" data-src="" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="tab-2">
                                <div class="mt20">
                                    <p>Situated near the beach, Oceanview Sunny Isles Luxury Condos is within 6 mi (10 km) of Aventura Mall, Gulfstream Park Racing and Casino, and Oleta River State Park. Bayside Marketplace is 19.4 mi (31.2 km) away.</p>

                                    <p>Oceanview Sunny Isles Luxury Condos features an outdoor pool, a fitness center, and an outdoor tennis court. Free WiFi in public areas and free self parking are also provided. Other amenities include a children's pool, concierge services, and dry cleaning.</p>

                                    <p>All condos feature kitchens with refrigerators and microwaves, plus free WiFi, free wired Internet, and flat-screen TVs with cable channels. Other amenities available to guests include balconies or patios, living rooms, and dining areas. Housekeeping is available once per stay.</p>

                                    <p>In addition to an outdoor pool and a children's pool, OceanView Sunny Isles Luxury Condos provides an outdoor tennis court and a fitness center. Public areas are equipped with complimentary wired and wireless Internet access. This Sunny Isles Beach condo also offers a library, barbecue grills, and a garden.</p>

                                    <p>OceanView Sunny Isles Luxury Condos has designated areas for smoking.</p>
                                </div>

                            </div>
                            <div class="tab-pane fade" id="google-map-tab">
                                <div id="map-canvas" style="width:100%; height:550px;"></div>
                            </div>



                            <div class="tab-pane fade" id="tab-5">
                                <div class="row mt20">
                                    <div class="col-md-4">
                                        <ul class="booking-item-features booking-item-features-expand mb30 clearfix">
                                            <li><i class="fa fa-check"></i><span class="booking-item-feature-title">Free Wi-Fi</span>
                                            </li>
                                            <li><i class="fa fa-check"></i><span class="booking-item-feature-title">Parking</span>
                                            </li>
                                            <li><i class="fa fa-check"></i><span class="booking-item-feature-title">HDTV</span>
                                            </li>
                                            <li><i class="fa fa-check"></i><span class="booking-item-feature-title">Fitness Center</span>
                                            </li>
                                            <li><i class="fa fa-check"></i><span class="booking-item-feature-title">Swimming Pool</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="booking-item-features booking-item-features-expand mb30 clearfix">
                                            <li><i class="fa fa-check"></i><span class="booking-item-feature-title">Restaurant</span>
                                            </li>
                                            <li><i class="fa fa-check"></i><span class="booking-item-feature-title">Fitness Center</span>
                                            </li>
                                            <li><i class="fa fa-check"></i><span class="booking-item-feature-title">Refrigerator</span>
                                            </li>
                                            <li><i class="fa fa-check"></i><span class="booking-item-feature-title">Barbecue grill(s)</span>
                                            </li>
                                            <li><i class="fa fa-check"></i><span class="booking-item-feature-title">Tennis</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-4">
                                        <ul class="booking-item-features booking-item-features-expand mb30 clearfix">
                                            <li><i class="fa fa-check"></i><span class="booking-item-feature-title">Children Activites</span>
                                            </li>
                                            <li><i class="fa fa-check"></i><span class="booking-item-feature-title">Playground</span>
                                            </li>
                                            <li><i class="fa fa-check"></i><span class="booking-item-feature-title">Living Room</span>
                                            </li>
                                            <li><i class="fa fa-check"></i><span class="booking-item-feature-title">Laundry</span>
                                            </li>
                                            <li><i class="fa fa-check"></i><span class="booking-item-feature-title">Front desk (limited hours)</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="tab-4">
                                <div class="mt20">
                                    <p><b>CHECK-IN AND CHECK-OUT</b><br>
                                        • Check In : 16:00 PM<br>
                                        • Check-in time ends at midnight<br>
                                        • Check Out : 11:00 AM<br>
                                        • Minimum check-in age is 18<br>
                                        • There will be a late check-in fee $50 after 10:00 PM</p>
                                    <p><b>Special check-in instructions:</b><br>
                                        To make arrangements for check-in please contact the property at least 24 hours before arrival using the information on the booking confirmation.</p>
                                    <p><b>CANCELLATION POLICY</b><br>
                                        • There are no refunds to bookings.<br>
                                        • There are no cancellations allowed.</p>
                                    <p><b>Smoking / Pet Policy</b><br>
                                        • Smoking not allowed.<br>
                                        • Pets not allowed.</p>
                                    <p><b>DEPOSITS AND FEE</b><br>
                                        • Payment Mode : VISA, MASTERCARD, AMEX, PAYPAL, BANK TRANSFER<br>
                                        • CLEANING FEE : $141.25 per stay<br>
                                        • SECURITY DEPOSIT : $250<br>
                                        • 100% downpayment is requested to book this unit.<br>
                                        • For reservations longer than a week, Guest may be able to pay 50% up front and 50% 3 weeks before arrival.</p>
                                    <p><b>Children and extra beds</b><br>
                                        • Children are welcome.<br>
                                        • Rollaway/extra beds are available for USD 12.49 per night.<br>
                                        • Cribs (infant beds) are available for USD 9.99 per day.</p>
                                    <p><b>You need to know</b><br>
                                        • Extra-person charges may apply and vary depending on property policy.<br>
                                        • Government-issued photo identification and a credit card or cash deposit are required at check-in for incidental charges.<br>
                                        • Special requests are subject to availability upon check-in and may incur additional charges.<br>
                                        • Special requests cannot be guaranteed.<br>
                                        • Only service animals are allowed</p>
                                    <p><b>Resort Fees</b><br>
                                        You'll be asked to pay USD 40.00 per accommodation, per stay. The resort fee includes:<br>
                                        • Pool access, Fitness center access, Parking<br>
                                        We have included all charges provided to us by the property. However, charges can vary, for example, based on length of stay or the room you book.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="gap gap-small"></div>
            <div class="row">
                <div class="col-md-12">
                    <ul class="booking-list">
                        <li>
                            <a class="booking-item" href="rooms/or3br.html">
                                <div class="row">
                                    <div class="col-md-3">
                                        <img src="img/octhumbs/oc1.jpg" alt="" title="Ocean Reserve Luxury Condos" />
                                    </div>
                                    <div class="col-md-6">
                                        <h5 class="booking-item-title">Ocean Reserve Bay View Standard 3 Bedroom Condo</h5>
                                        <p class="text-small"><b>Layout</b> - Living room, dining area, and sitting area<br>
                                            <b>Internet</b> - Free WiFi and wired Internet access<br>
                                            <b>Entertainment</b> - Flat-screen TV with premium channels<br>
                                            <b>Food & Drink</b> - Kitchen with refrigerator, microwave and coffee/tea maker<br>
                                            <b>Bathroom</b> - Designer toiletries and a hair dryer (on request)<br>
                                            <b>Practical</b> - Safe, iron/ironing board, and desk; cribs/infant beds available on request<br>
                                            <b>Comfort</b> - Air conditioning<br>
                                            <b>Need to Know</b> - Limited housekeeping, no rollaway/extra beds available, Non-Smoking</p>
                                        <ul class="booking-item-features booking-item-features-sign clearfix">
                                            <li rel="tooltip" data-placement="top" title="Adults Occupancy"><i class="fa fa-male"></i><span class="booking-item-feature-sign">x 7</span>
                                            </li>
                                            <li rel="tooltip" data-placement="top" title="Beds"><i class="im im-bed"></i><span class="booking-item-feature-sign">x 6</span>
                                            </li>
                                            <li rel="tooltip" data-placement="top" title="Room footage (square feet)"><i class="im im-width"></i><span class="booking-item-feature-sign">1600</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3"><span class="booking-item-price">$289</span><span>avg/night</span><br>
                                        <span class="btn btn-primary">Book This Condo Now</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="booking-item" href="rooms/or3brov.html">
                                <div class="row">
                                    <div class="col-md-3">
                                        <img src="img/octhumbs/oc5.jpg" alt="" title="Ocean Reserve Luxury Condos" />
                                    </div>
                                    <div class="col-md-6">
                                        <h5 class="booking-item-title">Ocean Reserve Ocean View 3 Bedroom Condo</h5>
                                        <p class="text-small"><b>Layout</b> - 3 bedrooms, living room, and dining area<br>
                                            <b>Internet</b> - Free WiFi and wired Internet access<br>
                                            <b>Entertainment</b> - Flat-screen TV with premium channels<br>
                                            <b>Food & Drink</b> - Kitchen with refrigerator, microwave, and coffee/tea maker<br>
                                            <b>Bathroom</b> - Private bathroom, shower/tub combination, designer toiletries, and a hair dryer (on request)<br>
                                            <b>Practical</b> - Safe, iron/ironing board, and desk; rollaway/extra beds and cribs/infant beds available on request<br>
                                            <b>Comfort</b> - Air conditioning<br>
                                            <b>Need to Know</b> - Limited housekeeping, Non-Smoking</p>
                                        <ul class="booking-item-features booking-item-features-sign clearfix">
                                            <li rel="tooltip" data-placement="top" title="Adults Occupancy"><i class="fa fa-male"></i><span class="booking-item-feature-sign">x 7</span>
                                            </li>
                                            <li rel="tooltip" data-placement="top" title="Beds"><i class="im im-bed"></i><span class="booking-item-feature-sign">x 5</span>
                                            </li>
                                            <li rel="tooltip" data-placement="top" title="Room footage (square feet)"><i class="im im-width"></i><span class="booking-item-feature-sign">1645</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3"><span class="booking-item-price">$299</span><span>/night</span><br>
                                        <span class="btn btn-primary">Book This Condo Now</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="booking-item" href="rooms/orpv2bc.html">
                                <div class="row">
                                    <div class="col-md-3">
                                        <img src="img/octhumbs/oc2.jpg" alt="" title="Ocean Reserve Luxury Condos" />
                                    </div>
                                    <div class="col-md-6">
                                        <h5 class="booking-item-title">Ocean Reserve Pool View 2 Bedroom Condo</h5>
                                        <p class="text-small"><b>Layout</b> - Living room, dining area, and sitting area<br>
                                            <b>Internet</b> - Free WiFi and wired Internet access<br>
                                            <b>Entertainment</b> - Flat-screen TV with premium channels<br>
                                            <b>Food & Drink</b> - Kitchen with refrigerator, microwave, and coffee/tea maker<br>
                                            <b>Bathroom</b> - Designer toiletries and a hair dryer (on request)<br>
                                            <b>Practical</b> - Safe, iron/ironing board, and desk; cribs/infant beds available on request<br>
                                            <b>Comfort</b> - Air conditioning<br>
                                            <b>Need to Know</b> - Limited housekeeping, no rollaway/extra beds available, Non-Smoking</p>
                                        <ul class="booking-item-features booking-item-features-sign clearfix">
                                            <li rel="tooltip" data-placement="top" title="Adults Occupancy"><i class="fa fa-male"></i><span class="booking-item-feature-sign">x 7</span>
                                            </li>
                                            <li rel="tooltip" data-placement="top" title="Beds"><i class="im im-bed"></i><span class="booking-item-feature-sign">x 4</span>
                                            </li>
                                            <li rel="tooltip" data-placement="top" title="Room footage (square feet)"><i class="im im-width"></i><span class="booking-item-feature-sign">1290</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3"><span class="booking-item-price">$269</span><span>avg/night</span><br>
                                        <span class="btn btn-primary">Book This Condo Now</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="booking-item" href="rooms/orbvs2bc.html">
                                <div class="row">
                                    <div class="col-md-3">
                                        <img src="img/octhumbs/oc3.jpg" alt="" title="Ocean Reserve Luxury Condos" />
                                    </div>
                                    <div class="col-md-6">
                                        <h5 class="booking-item-title">Ocean Reserve Bay View Standard 2 Bedroom Condo</h5>
                                        <p class="text-small"><b>Layout</b> - Living room, dining area, and sitting area<br>
                                            <b>Internet</b> - Free WiFi and wired Internet access<br>
                                            <b>Entertainment</b> - Flat-screen TV with premium channels<br>
                                            <b>Food & Drink</b> - Kitchen with refrigerator, microwave, and coffee/tea maker<br>
                                            <b>Bathroom</b> - Designer toiletries and a hair dryer (on request)<br>
                                            <b>Practical</b> - Safe, iron/ironing board, and desk; cribs/infant beds available on request<br>
                                            <b>Comfort</b> - Air conditioning<br>
                                            <b>Need to Know</b> - Limited housekeeping, no rollaway/extra beds available, Non-Smoking</p>
                                        <ul class="booking-item-features booking-item-features-sign clearfix">
                                            <li rel="tooltip" data-placement="top" title="Adults Occupancy"><i class="fa fa-male"></i><span class="booking-item-feature-sign">x 7</span>
                                            </li>
                                            <li rel="tooltip" data-placement="top" title="Beds"><i class="im im-bed"></i><span class="booking-item-feature-sign">x 4</span>
                                            </li>
                                            <li rel="tooltip" data-placement="top" title="Room footage (square feet)"><i class="im im-width"></i><span class="booking-item-feature-sign">1150</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3"><span class="booking-item-price">$199</span><span>avg/night</span><br>
                                        <span class="btn btn-primary">Book This Condo Now</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="booking-item" href="rooms/orov1bc.html">
                                <div class="row">
                                    <div class="col-md-3">
                                        <img src="img/octhumbs/oc6.jpg" alt="" title="Ocean Reserve Luxury Condos" />
                                    </div>
                                    <div class="col-md-6">
                                        <h5 class="booking-item-title">Ocean Reserve Ocean View 1 Bedroom Condo</h5>
                                        <p class="text-small"><b>Layout</b> - Bedroom, living room, and dining area<br>
                                            <b>Internet</b> - Free WiFi and wired Internet access<br>
                                            <b>Entertainment</b> - Flat-screen TV with premium channels<br>
                                            <b>Food & Drink</b> - Kitchen with refrigerator, microwave, and coffee/tea maker<br>
                                            <b>Bathroom</b> - Private bathroom, shower/tub combination, designer toiletries, and a hair dryer (on request)<br>
                                            <b>Practical</b> - Safe, queen sofa bed, and iron/ironing board; cribs/infant beds available on request<br>
                                            <b>Comfort</b> - Air conditioning<br>
                                            <b>Need to Know</b> - Limited housekeeping, no rollaway/extra beds available, Non-Smoking</p>
                                        <ul class="booking-item-features booking-item-features-sign clearfix">
                                            <li rel="tooltip" data-placement="top" title="Adults Occupancy"><i class="fa fa-male"></i><span class="booking-item-feature-sign">x 5</span>
                                            </li>
                                            <li rel="tooltip" data-placement="top" title="Beds"><i class="im im-bed"></i><span class="booking-item-feature-sign">x 2</span>
                                            </li>
                                            <li rel="tooltip" data-placement="top" title="Room footage (square feet)"><i class="im im-width"></i><span class="booking-item-feature-sign">1000</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3"><span class="booking-item-price">$169</span><span>avg/night</span><br>
                                        <span class="btn btn-primary">Book This Condo Now</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="booking-item" href="rooms/orov1bd.html">
                                <div class="row">
                                    <div class="col-md-3">
                                        <img src="img/octhumbs/oc7.jpg" alt="" title="Ocean Reserve Luxury Condos" />
                                    </div>
                                    <div class="col-md-6">
                                        <h5 class="booking-item-title">Ocean Reserve Ocean View 1 Bedroom with Den</h5>
                                        <p class="text-small"><b>Layout</b> - Living room, dining area, and sitting area<br>
                                            <b>Internet</b> - Free WiFi and wired Internet access<br>
                                            <b>Entertainment</b> - Flat-screen TV with premium channels<br>
                                            <b>Food & Drink</b> - Kitchen with refrigerator, microwave, and coffee/tea maker<br>
                                            <b>Bathroom</b> - Partially open bathroom, shower, designer toiletries, and a hair dryer (on request)<br>
                                            <b>Practical</b> - Safe, twin sofa bed, and iron/ironing board; rollaway/extra beds and cribs/infant beds available on request<br>
                                            <b>Comfort</b> - Air conditioning<br>
                                            <b>Need to Know</b> - Limited housekeeping, Non-Smoking</p>
                                        <ul class="booking-item-features booking-item-features-sign clearfix">
                                            <li rel="tooltip" data-placement="top" title="Adults Occupancy"><i class="fa fa-male"></i><span class="booking-item-feature-sign">x 6</span>
                                            </li>
                                            <li rel="tooltip" data-placement="top" title="Beds"><i class="im im-bed"></i><span class="booking-item-feature-sign">x 3</span>
                                            </li>
                                            <li rel="tooltip" data-placement="top" title="Room footage (square feet)"><i class="im im-width"></i><span class="booking-item-feature-sign">1000</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3"><span class="booking-item-price">$189</span><span>avg/night</span><br>
                                        <span class="btn btn-primary">Book This Condo Now</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="booking-item" href="rooms/orbv1bc.html">
                                <div class="row">
                                    <div class="col-md-3">
                                        <img src="img/octhumbs/oc8.jpg" alt="" title="Ocean Reserve Luxury Condos" />
                                    </div>
                                    <div class="col-md-6">
                                        <h5 class="booking-item-title">Ocean Reserve Bay View 1 Bedroom Condo</h5>
                                        <p class="text-small"><b>Layout</b> - Bedroom, living room, and dining area<br>
                                            <b>Internet</b> - Free WiFi and wired Internet access<br>
                                            <b>Entertainment</b> - Flat-screen TV with premium channels<br>
                                            <b>Food & Drink</b> - Kitchen with refrigerator, microwave, and coffee/tea maker<br>
                                            <b>Bathroom</b> - Partially open bathroom, shower, designer toiletries, and a hair dryer (on request)<br>
                                            <b>Practical</b> - Safe, twin sofa bed, and iron/ironing board; rollaway/extra beds and cribs/infant beds available on request<br>
                                            <b>Comfort</b> - Air conditioning<br>
                                            <b>Need to Know</b> - Limited housekeeping, Non-Smoking</p>
                                        <ul class="booking-item-features booking-item-features-sign clearfix">
                                            <li rel="tooltip" data-placement="top" title="Adults Occupancy"><i class="fa fa-male"></i><span class="booking-item-feature-sign">x 5</span>
                                            </li>
                                            <li rel="tooltip" data-placement="top" title="Beds"><i class="im im-bed"></i><span class="booking-item-feature-sign">x 4</span>
                                            </li>
                                            <li rel="tooltip" data-placement="top" title="Room footage (square feet)"><i class="im im-width"></i><span class="booking-item-feature-sign">1000</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3"><span class="booking-item-price">$159</span><span>avg/night</span><br>
                                        <span class="btn btn-primary">Book This Condo Now</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="booking-item" href="rooms/orpv1bc.html">
                                <div class="row">
                                    <div class="col-md-3">
                                        <img src="img/octhumbs/oc9.jpg" alt="" title="Ocean Reserve Luxury Condos" />
                                    </div>
                                    <div class="col-md-6">
                                        <h5 class="booking-item-title">Ocean Reserve Pool View 1 Bedroom Condo</h5>
                                        <p class="text-small"><b>Layout</b> - Living room, dining area, and sitting area<br>
                                            <b>Internet</b> - Free WiFi and wired Internet access<br>
                                            <b>Entertainment</b> - Flat-screen TV with premium channels<br>
                                            <b>Food & Drink</b> - Kitchen with refrigerator, microwave, and coffee/tea maker<br>
                                            <b>Bathroom</b> - Designer toiletries and a hair dryer (on request)<br>
                                            <b>Practical</b> - Safe, queen sofa bed, and iron/ironing board; cribs/infant beds available on request<br>
                                            <b>Comfort</b> - Air conditioning<br>
                                            <b>Need to Know</b> - Limited housekeeping, no rollaway/extra beds available, Non-Smoking</p>
                                        <ul class="booking-item-features booking-item-features-sign clearfix">
                                            <li rel="tooltip" data-placement="top" title="Adults Occupancy"><i class="fa fa-male"></i><span class="booking-item-feature-sign">x 5</span>
                                            </li>
                                            <li rel="tooltip" data-placement="top" title="Beds"><i class="im im-bed"></i><span class="booking-item-feature-sign">x 2</span>
                                            </li>
                                            <li rel="tooltip" data-placement="top" title="Room footage (square feet)"><i class="im im-width"></i><span class="booking-item-feature-sign">1000</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3"><span class="booking-item-price">$149</span><span>avg/night</span><br>
                                        <span class="btn btn-primary">Book This Condo Now</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="booking-item" href="rooms/orgv1bc.html">
                                <div class="row">
                                    <div class="col-md-3">
                                        <img src="img/octhumbs/oc10.jpg" alt="" title="Ocean Reserve Luxury Condos" />
                                    </div>
                                    <div class="col-md-6">
                                        <h5 class="booking-item-title">Ocean Reserve Garden View 1 Bedroom Condo</h5>
                                        <p class="text-small"><b>Layout</b> - Bedroom, living room, and dining area<br>
                                            <b>Internet</b> - Free WiFi and wired Internet access<br>
                                            <b>Entertainment</b> - Flat-screen TV with premium channels<br>
                                            <b>Food & Drink</b> - Kitchen with refrigerator, microwave, and coffee/tea maker<br>
                                            <b>Bathroom</b> - Partially open bathroom, shower, designer toiletries, and a hair dryer (on request)<br>
                                            <b>Practical</b> - Safe, queen sofa bed, and iron/ironing board; rollaway/extra beds and cribs/infant beds available on request<br>
                                            <b>Comfort</b> - Air conditioning<br>
                                            <b>Need to Know</b> - Limited housekeeping, Non-Smoking</p>
                                        <ul class="booking-item-features booking-item-features-sign clearfix">
                                            <li rel="tooltip" data-placement="top" title="Adults Occupancy"><i class="fa fa-male"></i><span class="booking-item-feature-sign">x 5</span>
                                            </li>
                                            <li rel="tooltip" data-placement="top" title="Beds"><i class="im im-bed"></i><span class="booking-item-feature-sign">x 2</span>
                                            </li>
                                            <li rel="tooltip" data-placement="top" title="Room footage (square feet)"><i class="im im-width"></i><span class="booking-item-feature-sign">1044</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3"><span class="booking-item-price">$139</span><span>avg/night</span><br>
                                        <span class="btn btn-primary">Book This Condo Now</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="booking-item" href="rooms/orpvs.html">
                                <div class="row">
                                    <div class="col-md-3">
                                        <img src="img/octhumbs/oc11.jpg" alt="" title="Ocean Reserve Luxury Condos" />
                                    </div>
                                    <div class="col-md-6">
                                        <h5 class="booking-item-title">Ocean Reserve Pool View Studio</h5>
                                        <p class="text-small"><b>Layout</b> - Living room, dining area, and sitting area<br>
                                            <b>Internet</b> - Free WiFi and wired Internet access<br>
                                            <b>Entertainment</b> - Flat-screen TV with premium channels<br>
                                            <b>Food & Drink</b> - Kitchen with refrigerator, microwave, and coffee/tea maker<br>
                                            <b>Bathroom</b> - Designer toiletries and a hair dryer (on request)<br>
                                            <b>Practical</b> - Safe, iron/ironing board, and desk; cribs/infant beds available on request<br>
                                            <b>Comfort</b> - Air conditioning<br>
                                            <b>Need to Know</b> - Limited housekeeping, no rollaway/extra beds available, Non-Smoking</p>
                                        <ul class="booking-item-features booking-item-features-sign clearfix">
                                            <li rel="tooltip" data-placement="top" title="Adults Occupancy"><i class="fa fa-male"></i><span class="booking-item-feature-sign">x 2</span>
                                            </li>
                                            <li rel="tooltip" data-placement="top" title="Beds"><i class="im im-bed"></i><span class="booking-item-feature-sign">x 1</span>
                                            </li>
                                            <li rel="tooltip" data-placement="top" title="Room footage (square feet)"><i class="im im-width"></i><span class="booking-item-feature-sign">538</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3"><span class="booking-item-price">$129</span><span>avg/night</span><br>
                                        <span class="btn btn-primary">Book This Condo Now</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a class="booking-item" href="rooms/orbvss.html">
                                <div class="row">
                                    <div class="col-md-3">
                                        <img src="img/octhumbs/oc4.jpg" alt="" title="Ocean Reserve Luxury Condos" />
                                    </div>
                                    <div class="col-md-6">
                                        <h5 class="booking-item-title">Ocean Reserve Bay View Standard Studio</h5>
                                        <p class="text-small"><b>Layout</b> - Living room, dining area, and sitting area<br>
                                            <b>Internet</b> - Free WiFi and wired Internet access<br>
                                            <b>Entertainment</b> - Flat-screen TV with premium channels<br>
                                            <b>Food & Drink</b> - Kitchen with refrigerator, microwave, and coffee/tea maker<br>
                                            <b>Bathroom</b> - Designer toiletries and a hair dryer (on request)<br>
                                            <b>Practical</b> - Safe, iron/ironing board, and desk; cribs/infant beds available on request<br>
                                            <b>Comfort</b> - Air conditioning<br>
                                            <b>Need to Know</b> - Limited housekeeping, no rollaway/extra beds available, Non-Smoking</p>
                                        <ul class="booking-item-features booking-item-features-sign clearfix">
                                            <li rel="tooltip" data-placement="top" title="Adults Occupancy"><i class="fa fa-male"></i><span class="booking-item-feature-sign">x 4</span>
                                            </li>
                                            <li rel="tooltip" data-placement="top" title="Beds"><i class="im im-bed"></i><span class="booking-item-feature-sign">x 2</span>
                                            </li>
                                            <li rel="tooltip" data-placement="top" title="Room footage (square feet)"><i class="im im-width"></i><span class="booking-item-feature-sign">500</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-3"><span class="booking-item-price">$119</span><span>avg/night</span><br>
                                        <span class="btn btn-primary">Book This Condo Now</span>
                                    </div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="gap gap-small"></div>
    </div>
@stop