@extends('template')

<!-- Web Fonts
	================================================== -->
	<link
		href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
		rel="stylesheet" />
	<link
		href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
		rel="stylesheet">
		<link rel="stylesheet" href="https://cdn.materialdesignicons.com/3.8.95/css/materialdesignicons.min.css">
<link href="https://fonts.googleapis.com/css?family=Mansalva&display=swap" rel="stylesheet">
	<!-- CSS
	================================================== -->
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/bootstrap.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/font-awesome.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/ionicons.min.css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/jquery.fancybox.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/owl.carousel.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/owl.transitions.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/style.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/colors/color.css" />
@section('main')


<style>
    .form-control{
        height:44px;
            border: 1px solid #ced4da;
                font-size: 14px;
    }
</style>


        <div class="contact_main">
            <div class="container">

            <div class="row">
              <div class="col-md-7">
									@if ($message = Session::get('success'))
									<div class="alert alert-success alert-block">
										<button type="button" class="close" data-dismiss="alert">×</button>
									        <strong>{{ $message }}</strong>
									</div>
									@endif
                  <form name="contactform" class="form-horizontal" method="post" action="{{url('contact_create')}}">

                    <fieldset>





                        <div class="form-group">

                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>

                            <div class="col-md-8">

                                <input id="first_name" name="name" type="text" placeholder="First Name" class="form-control">

                            </div>

                        </div>

                        <div class="form-group">

                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-user bigicon"></i></span>

                            <div class="col-md-8">

                                <input id="last_name" name="last_name" type="text" placeholder="Last Name" class="form-control">

                            </div>

                        </div>



                        <div class="form-group">

                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-envelope-o bigicon"></i></span>

                            <div class="col-md-8">

                                <input id="email" name="email" type="text" placeholder="Email Address" class="form-control">

                            </div>

                        </div>



                        <div class="form-group">

                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-phone-square bigicon"></i></span>

                            <div class="col-md-8">

                                <input id="telephone" name="telephone" type="text" placeholder="Phone" class="form-control">

                            </div>

                        </div>



                        <div class="form-group">

                            <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-pencil-square-o bigicon"></i></span>

                            <div class="col-md-8">

                                <textarea class="form-control" id="comments" name="feedback" placeholder="Enter your massage for us here. We will get back to you as soon as possible." rows="7"></textarea>

                            </div>

                        </div>



                        <div class="form-group">

                            <div class="col-md-12 text-center">

                                <button type="submit" class="btn btn-primary">Submit</button>

                            </div>

                        </div>

                    </fieldset>

                </form>

                </div>

                <div class="col-md-4">

                    <div class="gap"></div>

                    <aside class="sidebar-right">

                        <ul class="address-list list">

                            <!--<li>-->

                            <!--    <h5>Support</h5><a href="mailto:info@luxvacationrentalhomes.com">info@luxvacationrentalhomes.com</a><br>-->

                            <!--</li>-->

                            <li>

                                <h5>Support</h5><a href="mailto:bean.dvine@gmail.com">bean.dvine@gmail.com</a>

                            </li>

                            <!--<li>

                                <h5>Toll-Free</h5><a href="tel:+1-844-376-7368">+1-843-788-9288</a>

                            </li>-->

                                <li>

                                <h5>Local</h5><a href="tel:+1 (480) 459-8306">+1 (480) 459-8306</a>

                            </li>

                            <li>

                                <h5>Address</h5><address>Arizona Vacation Homes<br />Paradise Valley, AZ , USA<br /></address>

                            </li>

                        </ul>

                    </aside>

                </div>

            </div>

            <div class="gap"></div>

            <style>

    .header {

        color: #36A0FF;

        font-size: 27px;

        padding: 10px;

    }



    .bigicon {

        font-size: 35px;

        color: #d8b74e;

    }


</style>

        </div>
        </div>




@stop
