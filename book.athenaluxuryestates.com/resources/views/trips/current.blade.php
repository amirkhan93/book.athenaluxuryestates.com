<?php use App\Models\Rooms;

?>
@extends('template')

	<link
		href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
		rel="stylesheet" />
	<link
		href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
		rel="stylesheet">
		<link rel="stylesheet" href="https://cdn.materialdesignicons.com/3.8.95/css/materialdesignicons.min.css">
<link href="https://fonts.googleapis.com/css?family=Mansalva&display=swap" rel="stylesheet">
	<!-- CSS
	================================================== -->
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/bootstrap.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/font-awesome.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/ionicons.min.css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/jquery.fancybox.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/owl.carousel.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/owl.transitions.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/style.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/colors/color.css" />
@section('main')
    <main id="site-content" role="main"  ng-controller="cancel_reservation" class="inner_page_margin_d">
    @include('common.subheader')
  <div class="page-container-responsive space-top-4 space-4">
    <div class="row">
      <div class="col-md-3 trip-left-sec">
        @include('common.sidenav')
      </div>
      <div class="col-md-9 trip-right-sec">
      @if($pending_trips->count() == 0 && $current_trips->count() == 0 && $upcoming_trips->count() == 0)
        <div class="panel">
    <div class="panel-header">
      {{ trans('messages.header.your_trips') }}
    </div>
    <div class="panel-body">
      <p>
        {{ trans('messages.your_trips.no_current_trips') }}
      </p>
      <div class="row">
        <div class="col-8  trip-search">
          <form method="get" class="row" action="{{ url() }}/s" accept-charset="UTF-8">
  <div class="col-8 trip-search-bar">
    <input type="text" placeholder="{{ trans('messages.header.where_are_you_going') }}" name="location" id="location" autocomplete="off" class="location">
  </div>
  <div class="col-4 trip-search-btn">
    <button id="submit_location" class="btn btn-primary" type="submit">
      {{ trans('messages.home.search') }}
    </button>
  </div>
</form>
        </div>
      </div>
    </div>
  </div>
  @endif
  @if($pending_trips->count() > 0)
  <div class="panel row-space-4">
    <div class="panel-header">
      {{ trans('messages.your_trips.pending_trips') }}
    </div>
    <div class="table-responsive">
      <table class="table panel-body panel-light">
        <tbody><tr>
          <th>{{ trans('messages.your_reservations.status') }}</th>
          <th>{{ trans('messages.your_trips.location') }}</th>
          <th>{{ trans('messages.your_trips.host') }}</th>
          <th>{{ trans('messages.your_trips.dates') }}</th>
          <th>{{ trans('messages.your_trips.options') }}</th>
        </tr>
        @foreach($pending_trips as $pending_trip)
            <tr>
      <td class="status">
        <span class="label label-orange label-{{ $pending_trip->status_color }}">
            @if($pending_trip->status == 'Pre-Accepted' || $pending_trip->status == 'Inquiry' || @$pending_trip->status != 'Pre-Approved')
            @if($pending_trip->checkin >= date("Y-m-d"))
              <span class="label label-{{ $pending_trip->status_color }}">
                {{ trans('messages.dashboard.'.$pending_trip->status) }}
              </span>
            @else
            <span class="label label-info">{{trans('messages.dashboard.Expired')}}</span>
            @endif
          @else
            <span class="label label-{{ $pending_trip->status_color }}">
                {{ trans('messages.dashboard.'.$pending_trip->status) }}
              </span>
          @endif
          @if($pending_trip->status=='Pre-Accepted')
           <div class="space-top-3" >
          @if($pending_trip->checkin >= date("Y-m-d"))
          @if($pending_trip->date_check!='No' || $pending_trip->avablity!=1)
        <a href="javascript:void(0)" class="btn btn-primary pre_prefer" id="{{ $pending_trip->id }}" data-room="{{ $pending_trip->room_id }}" data-checkin="{{ $pending_trip->checkin }}" data-checkout="{{ $pending_trip->checkout }}" >
        <p hidden="hidden" class="pending_id" ><?php echo $pending_trip->id;?></p>
        <span>{{ trans('messages.inbox.book_now') }}</span>
        </a>
          @else
        </div>
        <span style="color: #ff5a5f;font-weight: normal !important;" id="al_res{{ $pending_trip->id }}">{{ trans('messages.inbox.already_booked') }}</span>
        @endif
          @endif
          @endif
        </span>
        <br>
      </td>
      <td class="location">
        <a href="{{ url() }}/rooms/{{ $pending_trip->room_id }}">{{ $pending_trip->rooms->name }}</a>
        <br>      
        @if(@$pending_trip->rooms->rooms_address->city != '') {{ $pending_trip->rooms->rooms_address->city }}
        @else
        {{ $pending_trip->rooms->rooms_address->state }}
        @endif
      </td>
      <td class="host"><a href="{{ url() }}/users/show/{{ $pending_trip->host_id }}">{{ $pending_trip->rooms->users->full_name }}</a></td>
      <td class="dates">{{ $pending_trip->dates }}</td>

      <td>
      <ul class="unstyled button-list list-unstyled">
        <li class="row-space-1">
        <a class="button-steel" href="{{ url() }}/z/q/{{ $pending_trip->id }}">{{ trans('messages.your_reservations.message_history') }}</a>
        </li>
        <li class="row-space-1">        
        @if($pending_trip->date_check!='No')
        @if($pending_trip->checkin >= date("Y-m-d"))
        <a rel="nofollow" data-method="post" data-confirm="Are you sure that you want to cancel the request? Any money transacted will be refunded." id="{{ $pending_trip->id }}-trigger-pending" class="button-steel" href="javascript:void(0)" >{{ trans('messages.your_trips.cancel_request') }}</a>
        @endif
        @endif
        </li>
      </ul>
    </td>
    </tr>
    @endforeach
    </tbody>
    </table>
    </div>
  </div>
  @endif
  @if($current_trips->count() > 0)
  <div class="panel row-space-4">
    <div class="panel-header">
      Booked Trips
    </div>
    <div class="table-responsive">
      <table class="table panel-body panel-light">
        <tbody><tr>
          <th>{{ trans('messages.your_reservations.status') }}</th>
          <th>{{ trans('messages.your_trips.location') }}</th>
          <th>{{ trans('messages.your_trips.host') }}</th>
          <th>{{ trans('messages.your_trips.dates') }}</th>
          <th>{{ trans('messages.your_trips.options') }}</th>
        </tr>
        @foreach($current_trips as $current_trip)
		<?php $roomDetailC = Rooms::where('id',$current_trip->room_id)->first();
		
		?>
            <tr>
      <td class="status">
        <span class="label label-orange label-{{ $current_trip->status_color }}">
         
          @if($current_trip->status == 'Pre-Accepted' || $current_trip->status == 'Inquiry' || @$current_trip->status != 'Pre-Approved')
            @if($current_trip->checkin >= date("Y-m-d"))
              <span class="label label-{{ $current_trip->status_color }}">
               Booked
              </span>
            @else
            <span class="label label-info">{{trans('messages.dashboard.Expired')}}</span>
            @endif
          @else
            <span class="label label-{{ $current_trip->status_color }}">
                {{ trans('messages.dashboard.'.$current_trip->status) }}
              </span>
          @endif

          @if($current_trip->status=='Pre-Accepted')
           <div class="space-top-3" >
           @if($current_trip->checkin >= date("Y-m-d"))
            @if($current_trip->date_check!='No' || $current_trip->avablity!=1)
              <a href="javascript:void(0)" class="btn btn-primary pre_prefer" id="{{ $current_trip->id }}" data-room="{{ $current_trip->room_id }}" data-checkin="{{ $current_trip->checkin }}" data-checkout="{{ $current_trip->checkout }}" >
              <p hidden="hidden" class="pending_id" ><?php echo $current_trip->id;?></p>
              <span>{{ trans('messages.inbox.book_now') }}</span>
              </a>
            @else 
              </div>
        <span style="color: #ff5a5f;font-weight: normal !important;" id="al_res{{ $current_trip->id }}">{{ trans('messages.inbox.already_booked') }}</span>
            @endif
          @endif
          @endif
        </span>
        <br>
      </td>
      <td class="location">
        <a href="{{ url() }}/rooms/{{ $current_trip->room_id }}">{{ $current_trip->rooms->name }}</a>
        <br>                
        @if(@$current_trip->rooms->rooms_address->city != '') {{ $current_trip->rooms->rooms_address->city }}
        @else
        {{ $current_trip->rooms->rooms_address->state }}
        @endif
      </td>
      <td class="host"><a href="{{ url() }}/users/show/{{ $current_trip->host_id }}">{{ @$current_trip->rooms->users->full_name }}</a></td>
      <td class="dates">{{ $current_trip->dates }}</td>

      <td>
      <ul class="unstyled button-list list-unstyled">
        <li class="row-space-1">
        <a class="button-steel" href="{{ url() }}/z/q/{{ $current_trip->id }}">{{ trans('messages.your_reservations.message_history') }}</a>
        </li>
        @if($current_trip->status != "Cancelled" && $current_trip->status != "Declined" && $current_trip->status != "Expired" && $current_trip->status != " ")
        <li class="row-space-1">
        <a class="button-steel" href="{{ url() }}/reservation/itinerary?code={{ $current_trip->code }}">{{ trans('messages.your_trips.view_itinerary') }}</a>
        </li>
        <li class="row-space-1">
        <a class="button-steel" href="{{ url() }}/reservation/receipt?code={{ $current_trip->code }}">{{ trans('messages.your_trips.view_receipt') }}</a>
        </li>
        <li class="row-space-1">
        <a class="button-steel" href="javascript:void(0)" id="{{$current_trip->id}}-trigger">{{ trans('messages.your_reservations.cancel') }}</a>
        </li>
        @endif
      </ul>
    </td>
	</tr>
			 <?php if($current_trip->installment_status==1 && $roomDetailC->secondpayment > 0){?>
                <td colspan="5"><a class="label label-orange label-success" href="<?php echo url()?>/payments/book?reservation_id=<?php echo $current_trip->id?>">Second Payment</a></td>
            <?php }
            if($current_trip->installment_status==2 && $roomDetailC->thirdpayment > 0){?>
                <td colspan="5"><a class="label label-orange label-success" href="<?php echo url()?>/payments/book?reservation_id=<?php echo $current_trip->id?>">Third Payment</a></td>
            <?php }
            if($current_trip->installment_status==3 && $roomDetailC->fourthpayment > 0){?>
                <td colspan="5"><a class="label label-orange label-success" href="<?php echo url()?>/payments/book?reservation_id=<?php echo $current_trip->id?>">Fourth Payment</a></td>
            <?php }
            if($current_trip->installment_status==4 && $roomDetailC->fifthpayment > 0){?>
                <td colspan="5"><a class="label label-orange label-success" href="<?php echo url()?>/payments/book?reservation_id=<?php echo $current_trip->id?>">Fifth Payment</a></td>
            <?php }
            ?>
    
    @endforeach
    </tbody>
    </table>
    </div>
  </div>
  @endif
  @if($upcoming_trips->count() > 0)
  <div class="panel row-space-4">
    <div class="panel-header">
      {{ trans('messages.your_trips.upcoming_trips') }}
    </div>
    <div class="table-responsive">
      <table class="table panel-body panel-light">
        <tbody><tr>
          <th>{{ trans('messages.your_reservations.status') }}</th>
          <th>{{ trans('messages.your_trips.location') }}</th>
          <th>{{ trans('messages.your_trips.host') }}</th>
          <th>{{ trans('messages.your_trips.dates') }}</th>
          <th>{{ trans('messages.your_trips.options') }}</th>
        </tr>
        @foreach($upcoming_trips as $upcoming_trip)
            <?php $roomDetail = Rooms::where('id',$upcoming_trip->room_id)->first();?>
         @if(@$upcoming_trip->status !='Pre-Approved')
            <tr>
      <td class="status">

        <span class="label label-orange label-{{ $upcoming_trip->status_color }}">
          {{ $upcoming_trip->status }}
        </span>
       
        <br>
      </td>
      <td class="location">
        <a href="{{ url() }}/rooms/{{ $upcoming_trip->room_id }}">{{ $upcoming_trip->rooms->name }}</a>
        <br>
        @if(@$upcoming_trip->rooms->rooms_address->city != '') {{ $upcoming_trip->rooms->rooms_address->city }}
        @else
        {{ $upcoming_trip->rooms->rooms_address->state }}
        @endif
        
      </td>
      <td class="host"><a href="{{ url() }}/users/show/{{ $upcoming_trip->host_id }}">{{ @$upcoming_trip->rooms->users->full_name }}</a></td>
      <td class="dates">{{ $upcoming_trip->dates }}</td>

      <td>
      <ul class="unstyled button-list list-unstyled">
        <li class="row-space-1">
        <a class="button-steel" href="{{ url() }}/z/q/{{ $upcoming_trip->id }}">{{ trans('messages.your_reservations.message_history') }}</a>
        </li>
        @if($upcoming_trip->status != "Cancelled"  && $upcoming_trip->status != "Declined" )
        <li class="row-space-1">
        <a class="button-steel" href="{{ url() }}/reservation/itinerary?code={{ $upcoming_trip->code }}">{{ trans('messages.your_trips.view_itinerary') }}</a>
        </li>
        <li class="row-space-1">
        <a class="button-steel" href="{{ url() }}/reservation/receipt?code={{ $upcoming_trip->code }}">{{ trans('messages.your_trips.view_receipt') }}</a>
        </li>
        @if($upcoming_trip->status != "Expired")
        <li class="row-space-1">
        <a class="button-steel" href="javascript:void(0)" id="{{$upcoming_trip->id}}-trigger">{{ trans('messages.your_reservations.cancel') }}</a>
        </li>
        @endif
         @endif
      </ul>
    </td>

    </tr>
            <?php if($upcoming_trip->installment_status==1 && $roomDetail->secondpayment > 0){?>
                <td colspan="5"><a class="label label-orange label-success" href="<?php echo url()?>/payments/book?reservation_id=<?php echo $upcoming_trip->id?>">Second Payment</a></td>
            <?php }
            if($upcoming_trip->installment_status==2 && $roomDetail->thirdpayment > 0){?>
                <td colspan="5"><a class="label label-orange label-success" href="<?php echo url()?>/payments/book?reservation_id=<?php echo $upcoming_trip->id?>">Third Payment</a></td>
            <?php }
            if($upcoming_trip->installment_status==3 && $roomDetail->fourthpayment > 0){?>
                <td colspan="5"><a class="label label-orange label-success" href="<?php echo url()?>/payments/book?reservation_id=<?php echo $upcoming_trip->id?>">Fourth Payment</a></td>
            <?php }
            if($upcoming_trip->installment_status==4 && $roomDetail->fifthpayment > 0){?>
                <td colspan="5"><a class="label label-orange label-success" href="<?php echo url()?>/payments/book?reservation_id=<?php echo $upcoming_trip->id?>">Fifth Payment</a></td>
            <?php }
            ?>
         @endif
    @endforeach
    </tbody>
    </table>
    </div>
  </div>
  @endif

    </div>
      
    </div>
  </div>
   @if($upcoming_trips->count() > 0 || $current_trips->count() > 0)
  <div class="modal" role="dialog" id="cancel-modal" aria-hidden="true">
  <div class="modal-table">
    <div class="modal-cell">
      <div class="modal-content">
        <form accept-charset="UTF-8" action="{{ url('trips/guest_cancel_reservation') }}" id="cancel_reservation_form" method="post" name="cancel_reservation_form">
        {!! Form::token() !!}
          <div class="panel-header">
            <a href="javascript:;" class="modal-close" data-behavior="modal-close"></a>
            {{ trans('messages.your_reservations.cancel_this_reservation') }}
          </div>
          <div class="panel-body">
            <div id="decline_reason_container">
              <p>
                {{ trans('messages.your_reservations.reason_cancel_reservation') }}
              </p>
              <p>
                <strong>
                  {{ trans('messages.your_trips.response_not_shared_host') }}
                </strong>
              </p>
              
              <div class="select">
                <select id="cancel_reason" name="cancel_reason">
    <option value="">{{ trans('messages.your_reservations.why_declining') }}</option>
    <option value="no_longer_need_accommodations">{{ trans('messages.your_reservations.I_no_longer_need_accommodations') }}</option>
    <option value="travel_dates_changed">{{ trans('messages.your_reservations.My_travel_dates_changed_successfully') }}</option>
    <option value="made_the_reservation_by_accident">{{ trans('messages.your_reservations.i_made_the_reservation_by_accident') }}</option>
    <option value="I_have_an_extenuating_circumstance">{{ trans('messages.your_reservations.i_have_an_extenuating_circumstance') }}</option>
    <option value="my_host_needs_to_cancel">{{ trans('messages.your_reservations.my_host_need_to_cancel') }}</option>
    <option value="uncomfortable_with_the_host">{{ trans('messages.your_reservations.i_m_uncomfortable_with_the_host') }}</option>
    <option value="place_not_okay">{{ trans('messages.your_reservations.the_place_is_not_what_was_expecting') }}</option>
    <option value="other">{{ trans('messages.your_reservations.other') }}</option></select>
              </div>

              <div id="cancel_reason_other_div" class="hide row-space-top-2">
                <label for="cancel_reason_other">
                  {{ trans('messages.your_reservations.why_cancel') }}
                </label>
                <textarea id="decline_reason_other" name="decline_reason_other" rows="4"></textarea>
              </div>
             
            </div>

            <label for="cancel_message" class="row-space-top-2">
              {{ trans('messages.your_trips.type_msg_host') }}...
            </label>
            <textarea cols="40" id="cancel_message" name="cancel_message" rows="10"></textarea>
            <input type="hidden" name="id" id="reserve_code" value="">
          </div>
          <div class="panel-footer">
            <input type="hidden" name="decision" value="decline">
            <input class="btn btn-primary" id="cancel_submit" name="commit" type="submit" value="{{ trans('messages.your_reservations.cancel_this_reservation') }}">
            <button class="btn" data-behavior="modal-close">
              {{ trans('messages.home.close') }}
            </button>
          </div>
</form>      </div>
    </div>
  </div>
</div>
@endif
@if($pending_trips->count() > 0) 
  <div class="modal" role="dialog" id="pending-cancel-modal" aria-hidden="true">
  <div class="modal-table">
    <div class="modal-cell">
      <div class="modal-content">
        <form accept-charset="UTF-8" action="{{ url('trips/guest_cancel_pending_reservation') }}" id="cancel_reservation_form" method="post" name="cancel_reservation_form">
        {!! Form::token() !!}
        <input name="_token" type="hidden" id="_token" value="{{ csrf_token() }}" />
          <div class="panel-header">
            <a href="#" class="modal-close" data-behavior="modal-close"></a>
            {{ trans('messages.your_reservations.cancel_this_reservation') }}
          </div>
          <div class="panel-body">
            <div id="decline_reason_container">
              <p>
                {{ trans('messages.your_reservations.reason_cancel_reservation') }}
              </p>
              <p>
                <strong>
                  {{ trans('messages.your_trips.response_not_shared_host') }}
                </strong>
              </p>
               <div class="select">
                <select id="cancel_reason" name="cancel_reason">
    <option value="">{{ trans('messages.your_reservations.why_declining') }}</option>
    <option value="no_longer_need_accommodations">{{ trans('messages.your_reservations.I_no_longer_need_accommodations') }}</option>
    <option value="travel_dates_changed">{{ trans('messages.your_reservations.My_travel_dates_changed_successfully') }}</option>
    <option value="made_the_reservation_by_accident">{{ trans('messages.your_reservations.i_made_the_reservation_by_accident') }}</option>
    <option value="I_have_an_extenuating_circumstance">{{ trans('messages.your_reservations.i_have_an_extenuating_circumstance') }}</option>
    <option value="my_host_needs_to_cancel">{{ trans('messages.your_reservations.my_host_need_to_cancel') }}</option>
    <option value="uncomfortable_with_the_host">{{ trans('messages.your_reservations.i_m_uncomfortable_with_the_host') }}</option>
    <option value="place_not_okay">{{ trans('messages.your_reservations.the_place_is_not_what_was_expecting') }}</option>
    <option value="other">{{ trans('messages.your_reservations.other') }}</option></select>
              </div>

              <div id="cancel_reason_other_div" class="hide row-space-top-2">
                <label for="cancel_reason_other">
                  {{ trans('messages.your_reservations.why_cancel') }}
                </label>
                <textarea id="decline_reason_other" name="decline_reason_other" rows="4"></textarea>
              </div>
             
            </div>

            <label for="cancel_message" class="row-space-top-2">
              {{ trans('messages.your_trips.type_msg_host') }}...
            </label>
            <textarea cols="40" id="cancel_message" name="cancel_message" rows="10"></textarea>
            <input type="hidden" name="id" id="reserve_code_pending" value="">
          </div>
          <div class="panel-footer">
            <input type="hidden" name="decision" value="decline">
            <input class="btn btn-primary" id="cancel_submit" name="commit" type="submit" value="{{ trans('messages.your_reservations.cancel_this_reservation') }}">
            <button class="btn" data-behavior="modal-close">
              {{ trans('messages.home.close') }}
            </button>
          </div>
</form>      </div>
    </div>
  </div>
</div>
@endif
</main>

@stop

{!! Html::script('js/jquery-1.11.3.js') !!}

<script type="text/javascript">
  $(document).ready(function() {
    $('.pre_prefer').click(function(){
      var pending_id= $(this).attr('id');
      var r_id = $(this).data('room');
      var c_in = $(this).data('checkin');
      var c_out = $(this).data('checkout');
      var t_url="{{ url() }}/payments/book?reservation_id="+pending_id;
      $.ajax({
          type: "post",
          url: '{{ url() }}/checking/'+pending_id,
          data:{
            'room_id':r_id,
            'checkin':c_in,
            'checkout':c_out
          },
          success:function(data){
           if(data == 'Already Booked') 
            {
               location.reload();
            }
            else
            {
              window.location.replace(t_url);
            }
        },

      });

    });
});
</script>

