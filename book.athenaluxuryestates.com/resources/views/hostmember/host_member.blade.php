87<!DOCTYPE html>

<head>

	<!-- Basic Page Needs
	================================================== -->
	<meta charset="utf-8">
	<title>Luxvacationrentalhomes</title>



	<!-- Mobile Specific Metas
	================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	


	<!-- Web Fonts 
	================================================== -->
	<link
		href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
		rel="stylesheet" />
	<link
		href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
		rel="stylesheet">
		<link rel="stylesheet" href="https://cdn.materialdesignicons.com/3.8.95/css/materialdesignicons.min.css">

	<!-- CSS
	================================================== -->
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/bootstrap.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/font-awesome.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/ionicons.min.css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/jquery.fancybox.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/owl.carousel.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/owl.transitions.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/style.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/colors/color.css" />
	<style>
		.red_border {
			border:2px solid red;
		}
	</style>



</head>

<body>

	<!--<div class="loader">
		<div class="loader__figure"></div>
	</div>-->

	<svg class="hidden">
		<svg id="icon-nav" viewBox="0 0 152 63">
			<title>navarrow</title>
			<path
				d="M115.737 29L92.77 6.283c-.932-.92-1.21-2.84-.617-4.281.594-1.443 1.837-1.862 2.765-.953l28.429 28.116c.574.57.925 1.557.925 2.619 0 1.06-.351 2.046-.925 2.616l-28.43 28.114c-.336.327-.707.486-1.074.486-.659 0-1.307-.509-1.69-1.437-.593-1.442-.315-3.362.617-4.284L115.299 35H3.442C2.032 35 .89 33.656.89 32c0-1.658 1.143-3 2.552-3H115.737z" />
		</svg>
	</svg>


	<!-- Nav and Logo
	================================================== -->

	@include('common/headertwo')

	<!-- Primary Page Layout
	================================================== -->

	<div class="section background-dark over-hide host-members">

    </div>
		<div class="hero-center-section">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12 mt-3 parallax-fade-top">
						<div class="booking-hero-wrap">
							<h1 class="hero-text">Host Members<br>
								<!--<small>Curated and hosted by experts for a Lux Level Experience.</small>--></h1>
							
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
		<div class="section  over-hide">
		    <div class="container">
		    
		        <div class="about-members">
		            <p>Not just any property owner or manager can be a LuxHomePro Host Member.  Each member below has gone through a rigorous program to attain the highest quality standards of excellence and create a unique, cherished, memorable and LUXURIOUS experience for guests who stay at our member properties.  Our exclusive membership program and training criteria ensures our hosts are knowledgeable and committed to our guests having an outstanding stay.  Whether you seek a best-in-class getaway for a corporate retreat, a wedding party or for your own family vacation, you are in the best care with our LuxHomePro Vacation Rental Host Members listed below.  To learn more about how you can apply to become a Host Member click HERE.</p>
		        <!-- Start-->
		        <div class="row mt-4">
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/amber.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Amber Maurer</h5>
                                    <h6 >
                                        <strong>
                                             Arizona Area
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Diamond Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>    
		            
		        <!--end-->

					<!--Start-->	
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/annie.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Annie Henderson</h5>
                                    <h6 >
                                        <strong>
                                             Texas Area
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                        Gold Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>    
		            <!--end-->  
		                
		            
		            <!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/brian.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Brian Baldonado</h5>
                                    <h6 >
                                        <strong>
                                             Florida Area
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                          Diamond Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		           </div>
					<!--end-->
				   
		            
		             <!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/carol.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Carol Larsen</h5>
                                    <h6 >
                                        <strong>
                                             Florida Area
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                          Platinum Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->    
		                
		            
		  			
		             <!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/cindy.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Cindy Falsey</h5>
                                    <h6 >
                                        <strong>
                                             California Area
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                          Platinum Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
					</div>
		            <!--end-->
		            
		  
		      
				 <!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/carla.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Carla Lindsay</h5>
                                    <h6 >
                                        <strong>
                                             California Area
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                          Gold Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
	
	
				 <!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/donald.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Donald Morrow</h5>
                                    <h6 >
                                        <strong>
                                             Arizona Area
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                          Diamond Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
		            					

				<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/dillon.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Dillon Bracken</h5>
                                    <h6 >
                                        <strong>
                                             Las Vegas,
											 Nevada Area
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                          Gold Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
		            
					
					
				<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/darlene.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Darlene Gelibert</h5>
                                    <h6 >
                                        <strong>
                                             Arizona Area
											 
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                          Gold Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/denese.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Denese Hayes</h5>
                                    <h6 >
                                        <strong>
                                             North Carolina Area
											 
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                          Platinum Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/dan.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Dan & Alana Pine</h5>
                                    <h6 >
                                        <strong>
                                             Arizona Area
											 
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                          Platinum Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/douglas.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Douglas Knupp</h5>
                                    <h6 >
                                        <strong>
                                             Arizona Area
											 
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                          Platinum Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/dave lee.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Dave & Theresa Lee</h5>
                                    <h6 >
                                        <strong>
                                             Scottsdale,
											 Arizona Area
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                          Platinum Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/daniel.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Daniel & Marketta White</h5>
                                    <h6 >
                                        <strong>
                                             Florida & Nevada Areas
											 
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                          Gold Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/erik.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Erik Dyrr</h5>
                                    <h6 >
                                        <strong>
                                             Wyoming Area
											 
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                          Diamond Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
						
						
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/felipe.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Felipe Del Valle Yarza</h5>
                                    <h6 >
                                        <strong>
                                             Arizona Area
											 
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Diamond Member Host
										  
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->	
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/geoff.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Geoff Vicente</h5>
                                    <h6 >
                                        <strong>
                                             Massachusetts Area
											 
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Gold Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->

					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/herbert.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Herbert Schmidt</h5>
                                    <h6 >
                                        <strong>
                                             California Area
											 
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Platinum Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/immaculate.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Immaculate and Mary Mukasa</h5>
                                    <h6 >
                                        <strong>
                                             
											 
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Gold Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
						
						
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/john1.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">John & Michelle Humphrey</h5>
                                    <h6 >
                                        <strong>
                                         Phoenix,    
										 Arizona Area 
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Diamond Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->	
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/jennette.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Jennette & Todd Hofmann</h5>
                                    <h6 >
                                        <strong>
                                         Arizona Area    
										 
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Gold Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/jason.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Jason Merritt</h5>
                                    <h6 >
                                        <strong>
                                         Florida Area    
										 
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Diamond Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/john.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">John Canada</h5>
                                    <h6 >
                                        <strong>
                                         Florida Area    
										 
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Platinum Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/kiko.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Kiko Khounvichith</h5>
                                    <h6 >
                                        <strong>
                                         Scottsdale, Arizona Area    
										 
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Diamond Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/kevin.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Kevin Kim</h5>
                                    <h6 >
                                        <strong>
                                         Washington Area    
										 
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Diamond Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/kellie.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Kellie Rhymes</h5>
                                    <h6 >
                                        <strong>
   
										 
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Gold Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/kevin o.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Kevin O'Neill</h5>
                                    <h6 >
                                        <strong>
   
										 
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Gold Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/kelli.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Kelli & Thein Nguyen-Ha</h5>
                                    <h6 >
                                        <strong>
										Texas Area
										 
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Diamond Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/luis.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Luis Rivera</h5>
                                    <h6 >
                                        <strong>
										California Area
										 
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Platinum Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->


					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/lorenzo.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Lorenzo Madrid</h5>
                                    <h6 >
                                        <strong>
										Texas Area
										 
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Platinum Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/monica.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Monica McKenna</h5>
                                    <h6 >
                                        <strong>
										Texas Area
										 
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Platinum Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/mark.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Mark Minson</h5>
                                    <h6 >
                                        <strong>
										Utah Area
										 
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Gold Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/mike.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Mike Warren</h5>
                                    <h6 >
                                        <strong>
										Arizona Area
										 
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Diamond Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/meijing.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Meijing Shih</h5>
                                    <h6 >
                                        <strong>
										
										 
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Platinum Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/necia.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Necia Betts</h5>
                                    <h6 >
                                        <strong>
										Scottsdale,
										Arizona Area 
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Diamond Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/osvaldo.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Osvaldo Hernandez</h5>
                                    <h6 >
                                        <strong>
										Scottsdale, Arizona Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Diamond Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/preston.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Preston Mitchell</h5>
                                    <h6 >
                                        <strong>
										Florida Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Diamond Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/robert.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Robert Chou</h5>
                                    <h6 >
                                        <strong>
										California Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Diamond Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img style="transform: rotate(90deg);" src="{{url()}}/resources/assets/Rick.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Rick Greer</h5>
                                    <h6 >
                                        <strong>
										Washington Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Gold Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->


					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/stephanie.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Stephanie Turner</h5>
                                    <h6 >
                                        <strong>
										
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Gold Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/scott.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Scott & Linda Tyler</h5>
                                    <h6 >
                                        <strong>
										Arizona Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Diamond Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/shawn.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Shawn Clee</h5>
                                    <h6 >
                                        <strong>
										Pennsylvania Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Platinum Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/stacy.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Stacy Godwin</h5>
                                    <h6>
                                        <strong>
										Florida Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Diamond Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/ticiana.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Ticiana Ferreira Larocca</h5>
                                    <h6>
                                        <strong>
										Florida Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Diamond Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/tia.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Tia Castle</h5>
                                    <h6>
                                        <strong>
										Florida Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Gold Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/tiamo.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Tiamo & Diane De Vetorri</h5>
                                    <h6>
                                        <strong>
										California Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Gold Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/tom.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Tom Jones</h5>
                                    <h6>
                                        <strong>
										
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Diamond Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Nick Apollo</h5>
                                    <h6>
                                        <strong>
										Phoenix,
										Arizona Area
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Gold Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Carlos Balagot</h5>
                                    <h6>
                                        <strong>
										Scottsdale,
										Arizona Area
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Diamond Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->

					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Asher Balagot</h5>
                                    <h6>
                                        <strong>
										Scottsdale,
										Arizona Area
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Diamond Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Donna Barnett</h5>
                                    <h6>
                                        <strong>
										Washington Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Platinum Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->

					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Belinda Bellaflores</h5>
                                    <h6>
                                        <strong>
										
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Diamond Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
	
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Shawnte Broadus</h5>
                                    <h6>
                                        <strong>
										Florida Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Gold Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Fredrick L Jackson</h5>
                                    <h6>
                                        <strong>
										Phoenix,
										Arizona Area
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         (386) 307-8543 <br>
										 fredjacksonproperties@gmail.com
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Dino Brown</h5>
                                    <h6>
                                        <strong>
										Georgia Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         (678) 887-7521 <br>
										 dino.realestatesolutions@gmail.com
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Samuel Bryson</h5>
                                    <h6>
                                        <strong>
										Florida Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         (509) 540-1323 <br>
										 sammbryson@yahoo.com
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Augusta Carballo</h5>
                                    <h6>
                                        <strong>
										California Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         (415) 341-3486 <br>
										 gusc123@yahoo.com
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Grant Davis</h5>
                                    <h6>
                                        <strong>
										Arizona Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         (804) 467-3518 <br>
										 sellit81@gmail.com
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Trisha Frauenhofer</h5>
                                    <h6>
                                        <strong>
										Arizona Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         (831) 840-9698 <br>
										 payperclickqueen@gmail.com
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">James Michael Farren</h5>
                                    <h6>
                                        <strong>
										California Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Gold Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
		
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Kerry Holt</h5>
                                    <h6>
                                        <strong>
										Texas Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Gold Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Darrell Hughston</h5>
                                    <h6>
                                        <strong>
										Virginia Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Gold Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Ray Khov</h5>
                                    <h6>
                                        <strong>
										Arizona Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Platinum Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Kelli Klumpp</h5>
                                    <h6>
                                        <strong>
										California Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Diamond Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Douglas McGrath</h5>
                                    <h6>
                                        <strong>
										California Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Diamond Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Richard Kubis</h5>
                                    <h6>
                                        <strong>
										Wisconsin Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Gold Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Robert Lawrence</h5>
                                    <h6>
                                        <strong>
										Scottsdale,
										Arizona Area
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Diamond Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Mason Turner</h5>
                                    <h6>
                                        <strong>
										Scottsdale,
										Arizona Area
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Diamond Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Scott Lee</h5>
                                    <h6>
                                        <strong>
										California Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Gold Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Deborah Marcinkiewicz</h5>
                                    <h6>
                                        <strong>
										Texas Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Platinum Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Tonimarie Marrese</h5>
                                    <h6>
                                        <strong>
										North Carolina Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Gold Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Tonia McCandless</h5>
                                    <h6>
                                        <strong>
										Las Vegas,
										Nevada Area
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Gold Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">David Penrod</h5>
                                    <h6>
                                        <strong>
										Texas Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Platinum Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Kama Moiha</h5>
                                    <h6>
                                        <strong>
										California Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Gold Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Jeff & Kim Mossman</h5>
                                    <h6>
                                        <strong>
										Iawa Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Platinum Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		   <!--         <div class="col-md-4 text-center">-->
		   <!--             <div class="card">-->
		   <!--                 <div class="card-body">-->
		   <!--                     <div class="media">-->
     <!--                             <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">-->
     <!--                             <div class="media-body">-->
     <!--                               <h5 class="mt-0">Aiden O’Neill</h5>-->
     <!--                               <h6>-->
     <!--                                   <strong>-->
										
										
     <!--                                   </strong>-->
     <!--                               </h6>-->
     <!--                               <p class="mt-3">-->
     <!--                                    (301) 748-4046 <br>-->
					<!--					 Kevin@SellFastGetPaid.com-->
     <!--                               </p>-->
     <!--                             </div>-->
     <!--                           </div>-->
		   <!--                 </div>-->
		   <!--             </div>-->
		   <!--         </div>-->
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Earl Odom</h5>
                                    <h6>
                                        <strong>
										Mississippi Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Platinum Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Melissa Tullis</h5>
                                    <h6>
                                        <strong>
										Mississippi Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Platinum Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Joe Reinhardt</h5>
                                    <h6>
                                        <strong>
										Florida Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Platinum Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Nilesh Sawale</h5>
                                    <h6>
                                        <strong>
										Texas Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                        Gold Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Susan and Greg Sheen</h5>
                                    <h6>
                                        <strong>
										Wisconsin Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Gold Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Nelson Vicente</h5>
                                    <h6>
                                        <strong>
										Massachusetts Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Gold Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Jay Walton</h5>
                                    <h6>
                                        <strong>
										Alabama Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Diamond Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
					
					<!--start-->
		            <div class="col-md-4 text-center">
		                <div class="card">
		                    <div class="card-body">
		                        <div class="media">
                                  <img src="{{url()}}/resources/assets/user.jpg" class="mr-3" alt="...">
                                  <div class="media-body">
                                    <h5 class="mt-0">Mitchell Wu</h5>
                                    <h6>
                                        <strong>
										Texas Area
										
                                        </strong>
                                    </h6>
                                    <p class="mt-3">
                                         Gold Member Host
                                    </p>
                                  </div>
                                </div>
		                    </div>
		                </div>
		            </div>
		            <!--end-->
					
	
	
				
		       
		    </div>
		</div>
		</div>
		

	@include('common/footertwo')


	<!-- JAVASCRIPT
    ================================================== -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="{{url()}}/resources/assets/home/js/popper.min.js"></script>
	<script src="{{url()}}/resources/assets/home/js/bootstrap.min.js"></script>
	<script src="{{url()}}/resources/assets/home/js/plugins.js"></script>
	<script src="{{url()}}/resources/assets/home/js/flip-slider.js"></script>
	<script src="{{url()}}/resources/assets/home/js/reveal-home.js"></script>
	<script src="{{url()}}/resources/assets/home/js/custom.js"></script>
	<script>
		$(document).ready(function(){
			$("#start-date-1").datepicker();
			$("#end-date-1").datepicker();
			$("#book_now").click(function(e){
				e.preventDefault();
				locations = $("#location").val();
				start_date = $("#start-date-1").val();
				end_date = $("#end-date-1").val();
				guests = $("#guests").val();
				if(locations=='' && start_date!='' && end_date!='' && guests!='')
				{
					$("#location").addClass("red_border");
				}
				else if(locations!='' && start_date!='' && end_date!='' && guests!='')
				{
					window.location.href="{{URL::to('search')}}"+"/"+locations+"/"+guests;
				}
			});
		});
	</script>
</body>
</html>