@extends('template')
<!-- Web Fonts 
	================================================== -->
	<link
		href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
		rel="stylesheet" />
	<link
		href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
		rel="stylesheet">
		<link rel="stylesheet" href="https://cdn.materialdesignicons.com/3.8.95/css/materialdesignicons.min.css">
<link href="https://fonts.googleapis.com/css?family=Mansalva&display=swap" rel="stylesheet">
	<!-- CSS
	================================================== -->
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/bootstrap.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/font-awesome.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/ionicons.min.css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/jquery.fancybox.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/owl.carousel.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/owl.transitions.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/style.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/colors/color.css" />
@section('main')   
<main id="site-content" role="main" class="inner_page_margin_d">
  @include('common.subheader')
  <div id="host-dashboard-container">
    <div class="host-dashboard">
      <div class="page-container-full">
        <!--<div class="header-color">
          <div class="page-container-responsive">
            <div class="row header-background">
              <div class="col-md-8 text-contrast hide-sm">
                <div class="row">
                  <div class="col-md-2">
                    <div class="va-container va-container-h collapsed-header">
                      <div class="va-middle">
                        <a href="{{ url('users/show/'.$user->id) }}" class="media-photo media-round pull-right" data-tracking="{&quot;section&quot;:&quot;header_profile_photo&quot;}">
                          <img src="{{$user->profile_picture->src}}" class="img-responsive">
                        </a>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-10">
                    <div class="va-container va-container-h collapsed-header">
                      <div class="va-middle text-lead">
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
                       <div class="carousel-inner">
      <div class="item active">
        <strong>
                            {{trans('messages.host_dashboard.hi_first_name', ['first_name' => $user->first_name])}}
                          </strong>                           
                       {{ trans('messages.host_dashboard.title') }}
      </div>
<div class="item">
        <strong>
                            {{trans('messages.host_dashboard.hi_first_name', ['first_name' => $user->first_name])}}
                          </strong>
                          {{trans('messages.host_dashboard.welcome_message')}}
      </div>
      <div class="item">
        <strong>
                            {{trans('messages.host_dashboard.hi_first_name', ['first_name' => $user->first_name])}}
                          </strong>                         
                          {{ trans('messages.host_dashboard.title') }}
      </div>
                    
                          
                      
                      </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              </div>
              <div class="text-contrast col-md-4 panel-right collapsed-header">
                <div class="text-center">
                  <div class="va-container collapsed-header va-container-h">
                    <div class="va-middle text-contrast text-lead">
                    <h2><sup>{{$currency_symbol}}</sup><strong>{{ $completed_payout  + $future_payouts }} </strong></h2>
                    <p>for {{ $completed_nights  +  $future_nights }} nights in {{ date('F') }}</p>
                     <div class="table-responsive">          
                     <table class="table borderless">
                      <thead>
                        <tr><th class="text-center" colspan="2">{{date('F')}} {{ trans('messages.host_dashboard.breakdown') }}</th></tr>
                      </thead>
    <tbody>
      <tr>
        <td class="text-left">{{ trans('messages.host_dashboard.already_paid_out') }}</td>
        <td class="text-right"><strong><sup>{{ $currency_symbol }}</sup>{{ $completed_payout }}</strong></td>
     </tr>
     <tr>
      <td class="text-left">{{ trans('messages.host_dashboard.expected_earnings') }}</td>
      <td class="text-right"><strong><sup>{{$currency_symbol}}</sup>{{ $future_payouts }}</strong></td>
      </tr>
      <tr class="total">
      <td class="text-left">
        {{ trans('messages.rooms.total') }} <i class="fa fa-question-circle" aria-hidden="true" title="total details"></i>
</td>
        <td class="text-right"><strong><sup>{{$currency_symbol}}</sup>{{ $completed_payout  + $future_payouts}} </strong></td>
     </tr>
     <tr class="total_paid">
        <td class="text-left"> {{ trans('messages.host_dashboard.total_paid_out_in') }} {{date('Y')}}</td>
        <td class="text-right"><strong><sup>{{$currency_symbol}}</sup>{{ $total_payout}}</strong></td>
     </tr>
    </tbody>
  </table>
  </div>

<div class="transaction_history">
<h6><a href="{{ url('users/transaction_history') }}">{{ trans('messages.host_dashboard.transaction_history') }}</a></h6>
</div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>-->
      </div>
      <div class="page-container-full alt-bg-module-panel-container relative">
        <div>
          <div class="page-container-responsive relative">
            <div ng-controller="Tabsh">
            <ul role="tablist" class="tabs">
           
              <li>
                <a href="javascript:;"  ng-click="show= 1;tab1=true;tab2=false" class="tab-item text-lead h4" role="tab" aria-controls="hdb-tab-standalone-first"  aria-selected="@{{tab1}}" data-tracking="{&quot;section&quot;:&quot;inbox_pending_requests_tab&quot;}">
                  ({{@$pending_count}} {{ trans('messages.dashboard.new') }}) Messages
                </a>
              </li>
              <li class="relative">
                <a href="javascript:;" ng-click="show = 2 ;tab2=true;tab1=false" class="tab-item text-lead h4" role="tab" aria-controls="hdb-tab-standalone-second" aria-selected="@{{tab2}}" data-tracking="{&quot;section&quot;:&quot;inbox_alerts_tab&quot;}">
                   {{ trans('messages.host_dashboard.Notifications') }}
                </a>
                
              </li>
            </ul>
         
          <ul class="list-unstyled page-container-responsive" ng-show="show == 1">
            <li id="hdb-tab-standalone-first" class="tab-panel hdb-light-bg" role="tabpanel" aria-hidden="false">
              <div class="text-lead text-muted no-req-res-row text-center">
               <div class="panel space-4">
      
        <ul class="list-layout">
            {{--@foreach($pending as $all)--}}
             {{--@if(@$all->host_check==1 && (@$all->reservation->status == 'Pending' ||  @$all->reservation->status == 'Inquiry'))--}}
            {{--<li id="thread_{{ $all->id }}" class="panel-body thread-read thread">--}}
  {{--<div class="row">--}}
    {{--<div class="col-sm-2 col-md-3 lang-chang-label thread-author">--}}
      {{--<div class="row row-table">--}}
        {{--<div class="col-sm-12 col-lg-5 lang-chang-label">--}}
          {{--<a data-popup="true" href="{{ url('users/show/'.$all->user_details->id)}}"><img height="50" width="50" title="{{ @$all->user_details->first_name }}" src="{{ $all->user_details->profile_picture->src }}" class="media-round media-photo" alt="{{ $all->user_details->first_name }}"></a>--}}
        {{--</div>--}}
        {{--<div class="col-sm-12 col-lg-7">--}}
          {{--{{ $all->user_details->first_name }}--}}
          {{--<br>--}}
          {{--{{ $all->created_time }}--}}
        {{--</div>--}}
      {{--</div>--}}
    {{--</div>--}}
    {{--@if(@$all->host_check ==1 && @$all->reservation->status == 'Pending')--}}
    {{--<a class="link-reset text-muted" href="{{ url('reservation')}}/{{ @$all->reservation_id }}">--}}
    {{--@elseif(@$all->host_check ==1 && @$all->reservation->status != 'Pending')--}}
    {{--<a class="link-reset text-muted" href="{{ url('messaging/qt_with')}}/{{ @$all->reservation_id }}">--}}
    {{--@endif--}}
    {{--@if(@$all->guest_check !=0)--}}
    {{--<a class="link-reset text-muted" href="{{ url('z/q')}}/{{ @$all->reservation_id }}">--}}
    {{--@endif--}}
      {{--<div class="col-sm-7 col-md-5 col-lg-6 thread-body lang-chang-label">--}}
         {{--<span class="thread-subject ng-binding unread_message">{{ @$all->message }}</span>--}}
        {{--<br>--}}
        {{--<span class="text-muted">--}}
            {{--<span class="street-address" ng-show="{{ @$all->reservation->status == 'Accepted' }}">{{ @$all->rooms_address->address_line_1 }} {{ @$all->rooms_address->address_line_2 }},</span><span class="locality">{{ @$all->rooms_address->city }},</span> <span class="region">{{ @$all->rooms_address->state }}</span>--}}
         {{--<span> ({{  (date('M d', strtotime( @$all->reservation->checkin))) }}, {{  (date('M d, Y', strtotime( @$all->reservation->checkout))) }})</span>--}}
        {{--</span>--}}
      {{--</div>--}}
{{--</a>--}}
   {{--<div class="col-md-3 col-sm-7 col-lg-3 lang-chang-label">--}}
        {{--<span class="label label-{{ @$all->reservation->status_color }}">{{ @$all->reservation->status }}</span>--}}
        {{--<br>--}}

       {{--<span class="lang-chang-label" ng-show="{{ ($all->host_check) ? 'true' : 'false' }}"> {{ @$all->reservation->currency->original_symbol }} {{ @$all->reservation->subtotal - @$all->reservation->host_fee }}--}}
            {{--</span>--}}
       {{--<span class="lang-chang-label" ng-show="{{ ($all->guest_check) ? 'true' : 'false' }}"> {{ @$all->reservation->currency->original_symbol }} {{ @$all->reservation->total }}--}}
            {{--</span>--}}
    {{--</div>--}}
  {{--</div>--}}
{{--</li>@endif--}}
{{--@endforeach--}}
 

          </ul>
          
          <div class="panel-body">
            <a href="{{ url('inbox') }}">{{ trans('messages.dashboard.all_messages') }}</a>
          </div>
      </div>
              </div>
            </li>
            
          </ul>

<!-- notification -->

          <ul class="list-unstyled page-container-responsive" ng-show="show==2">
            <li id="hdb-tab-standalone-first" class="tab-panel hdb-light-bg" role="tabpanel" aria-hidden="false">
              <div class="text-lead text-muted no-req-res-row text-center">
               <div class="panel space-4">
      
        <ul class="list-layout">
            {{--@foreach($all_message as $all)--}}
             {{--@if(@$all->reservation->status!='Pending')--}}
              {{--@if(@$all->reservation->status!='Inquiry')--}}
            {{--<li id="thread_{{ $all->id }}" class="panel-body thread-read thread">--}}
  {{--<div class="row">--}}
    {{--<div class="col-sm-2 col-md-3 lang-chang-label thread-author">--}}
      {{--<div class="row row-table">--}}
        {{--<div class="col-sm-12 col-lg-5 lang-chang-label">--}}
          {{--<a data-popup="true" href="{{ url('users/show/'.$all->user_details->id)}}"><img height="50" width="50" title="{{ @$all->user_details->first_name }}" src="{{ $all->user_details->profile_picture->src }}" class="media-round media-photo" alt="{{ $all->user_details->first_name }}"></a>--}}
        {{--</div>--}}
        {{--<div class="col-sm-12 col-lg-7">--}}
          {{--{{ $all->user_details->first_name }}--}}
          {{--<br>--}}
          {{--{{ $all->created_time }}--}}
        {{--</div>--}}
      {{--</div>--}}
    {{--</div>--}}
    {{--@if(@$all->host_check ==1 && @$all->reservation->status == 'Pending')--}}
    {{--<a class="link-reset text-muted" href="{{ url('reservation')}}/{{ @$all->reservation_id }}">--}}
    {{--@elseif(@$all->host_check ==1 && @$all->reservation->status != 'Pending')--}}
    {{--<a class="link-reset text-muted" href="{{ url('messaging/qt_with')}}/{{ @$all->reservation_id }}">--}}
    {{--@endif--}}
    {{--@if(@$all->guest_check !=0)--}}
    {{--<a class="link-reset text-muted" href="{{ url('z/q')}}/{{ @$all->reservation_id }}">--}}
    {{--@endif--}}
      {{--<div class="col-sm-7 col-md-5 col-lg-6 thread-body lang-chang-label">--}}
         {{--<span class="thread-subject ng-binding unread_message">{{ @$all->message }}</span>--}}
        {{--<br>--}}
        {{--<span class="text-muted">--}}
            {{--<span class="street-address" ng-show="{{ @$all->reservation->status == 'Accepted' }}">{{ @$all->rooms_address->address_line_1 }} {{ @$all->rooms_address->address_line_2 }},</span><span class="locality">{{ @$all->rooms_address->city }},</span> <span class="region">{{ @$all->rooms_address->state }}</span>--}}
         {{--<span> ({{  (date('M d', strtotime( @$all->reservation->checkin))) }}, {{  (date('M d, Y', strtotime( @$all->reservation->checkout))) }})</span>--}}
        {{--</span>--}}
      {{--</div>--}}
{{--</a>--}}
   {{--<div class="col-md-3 col-sm-7 col-lg-3 lang-chang-label">--}}
        {{--<span class="label label-{{ @$all->reservation->status_color }}">{{ @$all->reservation->status }}</span>--}}
        {{--<br>--}}

       {{--<span class="lang-chang-label" ng-show="{{ ($all->host_check) ? 'true' : 'false' }}"> {{ @$all->reservation->currency->original_symbol }} {{ @$all->reservation->subtotal - @$all->reservation->host_fee }}--}}
            {{--</span>--}}
       {{--<span class="lang-chang-label" ng-show="{{ ($all->guest_check) ? 'true' : 'false' }}"> {{ @$all->reservation->currency->original_symbol }} {{ @$all->reservation->total }}--}}
            {{--</span>--}}
    {{--</div>--}}
  {{--</div>--}}
{{--</li>@endif--}}
{{--@endif--}}
{{--@endforeach--}}

          </ul>
          <div class="panel-body">
            <a href="{{ url('inbox') }}">{{ trans('messages.dashboard.all_messages') }}</a>
          </div>
      </div>
              </div>
            </li>
           
          </ul>
          </div>
        </div>
      </div>
      <!--<div class="page-container-full alt-bg-module-panel-container relative">
        <div class="page-container-responsive referral-panel">
          <div class="panel">
            <div class="panel-body text-center">
              <div class="space-top-4 space-4">
                <h3>
                  <strong>
                    {{trans('messages.host_dashboard.earn_Travel')}}
                  </strong>
                </h3>
                <p>
                 {{ trans('messages.referrals.earn_up_to') }} {{ $result->value(5) }}{{ $result->value(2) + $result->value(3) }} {{ trans('messages.referrals.everyone_invite') }}.
                </p>
                <a data-tracking="{&quot;section&quot;:&quot;promo_invite_friends&quot;}" href="{{ url('invite') }}" class="btn btn-large btn-primary">
                   {{trans('messages.host_dashboard.invite_friends')}}
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>-->
    </div>
  </div>
</main>
@stop      

