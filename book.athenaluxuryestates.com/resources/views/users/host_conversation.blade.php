@extends('template')
<!-- Web Fonts
================================================== -->
<link
href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
rel="stylesheet" />
<link
href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
rel="stylesheet">
<link rel="stylesheet" href="https://cdn.materialdesignicons.com/3.8.95/css/materialdesignicons.min.css">
<link href="https://fonts.googleapis.com/css?family=Mansalva&display=swap" rel="stylesheet">
<!-- CSS
================================================== -->
<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/bootstrap.min.css" />
<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/font-awesome.min.css" />
<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/ionicons.min.css" />
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/jquery.fancybox.min.css" />
<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/owl.carousel.css" />
<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/owl.transitions.css" />
<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/style.css" />
<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/colors/color.css" />
<style>
.responsive-calendar .previousMonth, .responsive-calendar .nextMonth {

	display: flex;
	align-items: center;
	justify-content: center;
}
</style>
@section('main')
<main id="site-content" role="main" ng-controller="conversation" class="inner_page_margin_d">

	@include('common.subheader')

	<div class="page-container page-container-responsive row-space-top-4">

		<h1 class="h2 row-space-4">
			{{ trans('messages.inbox.conversation_with') }} {{ $messages[0]->reservation->first_name }}
		</h1>

		@if($messages[0]->reservation->status == 'Accepted')
		<div class="row">
			<div class="col-lg-12">

				<div class="alert alert-with-icon alert-success alert-block row-space-4">
					<i class="icon alert-icon icon-star-circled"></i>

					<div class="h5 row-space-1">
						{{ trans('messages.inbox.accepted') }}
					</div>

					<p>
						{{ trans('messages.inbox.you_have_accepted_reservation',['site_name'=>$site_name, 'first_name'=>$messages[0]->reservation->first_name]) }} <a class="alert-link" href="mailto:{{ $messages[0]->reservation->email }}">{{ trans('messages.inbox.email') }}</a>
						@if($messages[0]->reservation->primary_phone_number != ''){{trans('messages.login.or')}} {{strtolower(trans('messages.profile.phone_number'))}} ({{$messages[0]->reservation->primary_phone_number}}) @endif <i class="icon icon-question"></i>
					</p>

					<div>
						<a class="alert-link" href="{{ url() }}/reservation/itinerary?code={{ $messages[0]->reservation->code }}">{{ trans('messages.your_trips.view_itinerary') }}</a>
						<span id="conversation_alert_link_divider">|</span>
						<!-- <a class="alert-link" href="{{ url() }}/reservation/change?code={{ $messages[0]->reservation->code }}">Change or Cancel</a> -->
					</div>
				</div>
			</div>
		</div>
		@endif

		<div class="row">
			<div class="col-lg-8 lang-chang-label">
				<ul class="list-unstyled">
					<li class="thread-list-item" id="message_friction_react"></li>
					<li id="post_message_box" class="thread-list-item">
						<div class="row row-condensed row-space-4">
							<div class="col-sm-12 col-md-12">
								<div class="panel-quote-flush panel">
									<div class="panel-body normal-form-fields">

										<form id="non_special_offer_form" data-key="non_special_offer_form" class="message_form clearfix">

											<input type="hidden" value="{{ $messages[0]->reservation_id }}" name="inquiry_post_id" id="reservation_id">
											<input type="hidden" value="{{ $messages[0]->reservation->room_id }}" name="room_id" id="room_id">

											<input type="hidden" value="" name="template">
											<textarea style="resize: none; height: 62px; overflow: hidden; word-wrap: break-word;" placeholder="{{ trans('messages.inbox.add_personal_msg') }}" name="message" id="message_text" class="row-space-2 input-resize-vert"></textarea>

											<div class="pull-right">
												@if($messages[0]->reservation->type != 'contact')
												<a class="btn offer_attach btn-primary" href="javascript:void(0);">
													{{ trans('messages.inbox.attach_special_offer') }}
												</a>
												@endif
												@if($messages[0]->reservation->type == 'contact')
												<a id="pre_approve_button" class="btn pre_approve row-space-1" href="javascript:void(0);">
													{{ trans('messages.inbox.pre_approve') }} / {{ trans('messages.your_reservations.decline') }}
												</a>
												@endif

												<input type="button" value="{{ trans('messages.your_reservations.send_message') }}" class="btn btn-primary" ng-click="reply_message('non_special_offer_form')">
											</div>
										</form>
									</div>

									<div class="inquiry-form-fields inqiue hide">
										<div class="panel-body panel-dark">
											<div class="row">
												<div class="col-md-8 lang-chang-label">
													<div class="h4">
														{{ $messages[0]->reservation->name }}
														<br>
														<small>
															{{ $messages[0]->reservation->dates }} ({{ $messages[0]->reservation->nights }} {{ trans_choice('messages.rooms.night',1) }}{{ ($messages[0]->reservation->nights > 1) ? 's' : '' }})
															·
															{{ $messages[0]->reservation->number_of_guests }} {{ trans_choice('messages.home.guest',$messages[0]->reservation->number_of_guests) }}
														</small>
													</div>
												</div>
												<div class="col-md-4">

													<div class="h2 pull-right text-right">
														<sup class="h4">
															{{ $messages[0]->reservation->currency->symbol }}
														</sup>{{ $messages[0]->reservation->subtotal - $messages[0]->reservation->host_fee }}
													</div>
												</div>
											</div>
										</div>

										<div class="panel-body">
											<ul class="list-unstyled option-list">
												<li data-tracking-section="accept" class="option-list-item positive">

													<a class="option-link h3" href="javascript:void(0);">
														{{ trans('messages.inbox.allow_guest_book') }}
													</a>

													<form class="message_form positive" id="allow_guest">
														<input type="hidden" value="{{ $messages[0]->reservation_id }}" name="inquiry_post_id">

														<ul class="list-unstyled action-list hide">
															<!-- 1 -->
															<li data-key="pre-approve" class="action-list-item template_1 row-space-top-1">
																<hr>
																<label class="action-label">
																	<div class="row row-condensed">
																		<div class="col-sm-1">
																			<input type="radio" value="1" name="template">
																		</div>
																		<div class="col-sm-11">
																			<strong>{{ trans('messages.inbox.pre_approve_book',['first_name'=>$messages[0]->reservation->first_name]) }}</strong>
																		</div>
																	</div>
																</label>

																<div class="row row-condensed">
																	<div class="col-sm-offset-1 col-sm-11">
																		<div class="drawer hide">
																			<p class="description">{{ trans('messages.inbox.pre_approve_desc',['first_name'=>$messages[0]->reservation->first_name]) }}</p>

																			<textarea style="resize: none; height: 162px; overflow: hidden; word-wrap: break-word;" class="row-space-2 input-resize-vert" placeholder="{{ trans('messages.inbox.include_msg',['first_name'=>$messages[0]->reservation->first_name]) }}" name="message"></textarea>

																			<div class="pull-right">
																				<input type="submit" value="{{ trans('messages.inbox.pre_approve') }}" class="btn btn-primary" ng-click="reply_message('pre-approve')">
																			</div>
																		</div>
																	</div>
																</div>
															</li>
															<!-- 2 -->
															<li data-key="special_offer" class="action-list-item template_2 row-space-top-1">
																<hr>
																<label class="action-label">
																	<div class="row row-condensed">
																		<div class="col-sm-1">
																			<input type="radio" value="2" name="template">
																		</div>
																		<div class="col-sm-11">
																			<strong>{{ trans('messages.inbox.send_a_special_offer',['first_name'=>$messages[0]->reservation->first_name]) }}</strong>
																		</div>
																	</div>
																</label>

																<div class="row row-condensed">
																	<div class="col-sm-offset-1 col-sm-11">
																		<div class="drawer hide">
																			<p class="description">{{ trans('messages.inbox.special_offer_desc',['first_name'=>$messages[0]->reservation->first_name]) }}</p>

																			<fieldset class="available-special-offer">
																				<label for="pricing_room_id">{{ trans('messages.lys.listing') }}</label>
																				<div class="select select-block row-space-1">
																					{!! Form::select('pricing[hosting_id]', $rooms_unlist, $messages[0]->reservation->room_id, ['id'=>'pricing_room_id']); !!}
																				</div>

																				<div class="special-offer-date-fields">
																					<div class="row row-condensed row-space-1">
																						<div class="col-4">
																							<label for="pricing_start_date">{{ trans('messages.your_reservations.checkin') }}</label>
																							<input type="text" value="" readonly="readonly" name="pricing[start_date]" id="pricing_start_date" class="checkin ui-datepicker-target">
																						</div>
																						<div class="col-4">
																							<label for="pricing_end_date">{{ trans('messages.your_reservations.checkout') }}</label>
																							<input type="text" value="" readonly="readonly" name="pricing[end_date]" id="pricing_end_date" class="checkout ui-datepicker-target">
																						</div>
																						<div class="col-4">
																							<label for="pricing_guests">{{ trans_choice('messages.home.guest',2) }}</label>
																							<div class="select select-block">
																								<select name="pricing[guests]" id="pricing_guests">
																									@for($i=1;$i<= 10;$i++)
																									<option value="{{ $i }}">{{ $i }}</option>
																									@endfor
																								</select>
																							</div>
																							<input type="hidden" value="nightly" name="pricing[unit]" id="pricing_unit">
																						</div>
																					</div>
																				</div>
																				<input type="hidden" name="pricing[status]" id="availability_status" value="Available" />
																				<div id="availability_warning" class="alert alert-with-icon alert-info  row-space-top-2 hide">
																					<i class="icon alert-icon icon-comment"></i>
																					{{ trans('messages.inbox.already_marked_dates') }}
																				</div>
																				<input type="hidden" name="currency" value="{!! Session::get('currency') !!}">

																				<div class="row row-condensed row-space-2">
																					<div class="col-4">
																						<label for="pricing_price">{{ trans('messages.inbox.price') }}</label>
																						<div class="input-addon pricing-field">
																							<span class="input-prefix">{{ $messages[0]->reservation->currency->symbol }}</span>
																							<!--  <input type="number" min="0" value="{{ $messages[0]->reservation->subtotal - $messages[0]->reservation->host_fee }}" name="pricing[price]" id="pricing_price" class="input-stem"> -->
																							<input type="number" min="0"  name="pricing[price]" id="pricing_price" class="input-stem">
																							<span class="text-danger">{{ $errors->first('pricing_price') }}</span>
																						</div>
																					</div>
																					<div class="col-4">
																						<label for="pricing_price_type">&nbsp;</label>
																						<div class="select hide">
																							<select name="pricing[price_type]" id="pricing_price_type" disabled="">
																								<option value="total">{{ trans('messages.inbox.subtotal_price') }}</option>
																								<option value="per_unit">{{ trans('messages.rooms.per_month') }}</option>
																							</select>
																						</div>
																					</div>
																				</div>
																				<input type="hidden" name="currency1" value="{!! Session::get('currency') !!}">
																				<div class="split_payment">
																					<div class="form-group">
																						<input type="checkbox" id="split__payment" ng-change="isChecked()" ng-model="split_payment" />
																						<label for="split__payment" style="display: inline-block; margin-left: 5px;">Enable Split Payment</label>
																					</div>
																					<div class="split--payment_box" >
																						<div class="payment__box_1 row" ng-if="split_payment">
																							<div class="col-4">
																								<select class="form-control per_select" name="percent__1" id="percent__1" ng-model="percents[0]" ng-change="getSplitValue(percents[0], 'percent__1', {{ $reservation_id }})">
																									<option selected disabled>Select %</option>
																									<option value="10">10%</option>
																									<option value="20">20%</option>
																									<option value="30">30%</option>
																									<option value="40">40%</option>
																									<option value="50">50%</option>
																									<option value="60">60%</option>
																									<option value="70">70%</option>
																									<option value="80">80%</option>
																									<option value="90">90%</option>
																									<option value="100">100%</option>
																								</select>
																							</div>
																							<div class="col-4">
																								<input type="text" class="form-control" name="amount__1" readonly ng-model="values[0]"/>
																							</div>
																							<div class="col-4">
																								<!-- <input type="number" class="form-control" name="days_before__1" min="0" max="365" ng-model="dues[0]" value="0" /> -->
																								<span>Due Now</span>
																							</div>
																						</div>
																						<div class="payment__box_2 row" >
																							<div class="col-4">
																								<select class="form-control per_select" name="percent__2" id="percent__2" ng-model="percents[1]" ng-change="getSplitValue(percents[1], 'percent__2', {{ $reservation_id }})">
																									<option selected disabled>Select %</option>
																									<option value="10">10%</option>
																									<option value="20">20%</option>
																									<option value="30">30%</option>
																									<option value="40">40%</option>
																									<option value="50">50%</option>
																									<option value="60">60%</option>
																									<option value="70">70%</option>
																									<option value="80">80%</option>
																									<option value="90">90%</option>
																									<option value="100">100%</option>
																								</select>
																							</div>
																							<div class="col-4">
																								<input type="text" class="form-control" name="amount__2" readonly ng-model="values[1]"/>
																							</div>
																							<div class="col-4">
																								<input type="text" class="form-control date_picker_day_before" name="days_before__2" min="0" max="365" ng-model="dues[1]" value="0" />
																							</div>
																						</div>
																						<div class="payment__box_3 row">
																							<div class="col-4">
																								<select class="form-control per_select" name="percent__3" id="percent__3" ng-model="percents[2]" ng-change="getSplitValue(percents[2], 'percent__3', {{ $reservation_id }})">
																									<option selected disabled>Select %</option>
																									<option value="10">10%</option>
																									<option value="20">20%</option>
																									<option value="30">30%</option>
																									<option value="40">40%</option>
																									<option value="50">50%</option>
																									<option value="60">60%</option>
																									<option value="70">70%</option>
																									<option value="80">80%</option>
																									<option value="90">90%</option>
																									<option value="100">100%</option>
																								</select>
																							</div>
																							<div class="col-4">
																								<input type="text" class="form-control" name="amount__3" readonly ng-model="values[2]"/>
																							</div>
																							<div class="col-4">
																								<input type="text" class="form-control date_picker_day_before" name="days_before__3" min="0" max="365" ng-model="dues[2]" value="0" />
																							</div>
																						</div>
																						<div class="payment__box_4 row">
																							<div class="col-4">
																								<select class="form-control per_select" name="percent__4" id="percent__4" ng-model="percents[3]" ng-change="getSplitValue(percents[3], 'percent__4', {{ $reservation_id }})">
																									<option selected disabled>Select %</option>
																									<option value="10">10%</option>
																									<option value="20">20%</option>
																									<option value="30">30%</option>
																									<option value="40">40%</option>
																									<option value="50">50%</option>
																									<option value="60">60%</option>
																									<option value="70">70%</option>
																									<option value="80">80%</option>
																									<option value="90">90%</option>
																									<option value="100">100%</option>
																								</select>
																							</div>
																							<div class="col-4">
																								<input type="text" class="form-control" name="amount__4" readonly ng-model="values[3]"/>
																							</div>
																							<div class="col-4">
																								<input type="text" class="form-control date_picker_day_before" name="days_before__4" min="0" max="365" ng-model="dues[3]" value="0" />
																							</div>
																						</div>
																						<div class="payment__box_5 row">
																							<div class="col-4">
																								<select class="form-control per_select" name="percent__5" id="percent__5" ng-model="percents[4]" ng-change="getSplitValue(percents[4], 'percent__5', {{ $reservation_id }})">
																									<option selected disabled>Select %</option>
																									<option value="10">10%</option>
																									<option value="20">20%</option>
																									<option value="30">30%</option>
																									<option value="40">40%</option>
																									<option value="50">50%</option>
																									<option value="60">60%</option>
																									<option value="70">70%</option>
																									<option value="80">80%</option>
																									<option value="90">90%</option>
																									<option value="100">100%</option>
																								</select>
																							</div>
																							<div class="col-4">
																								<input type="text" class="form-control" name="amount__5" readonly ng-model="values[4]"/>
																							</div>
																							<div class="col-4">
																								<input type="text" class="form-control date_picker_day_before" name="days_before__5" min="0" max="365" ng-model="dues[4]" value="0" />
																							</div>
																						</div>
																						<p id="msg_error" style="color: red;"></p>
																					</div>
																				</div>
																				<div id="availability_warning1" class="alert alert-with-icon alert-info  row-space-top-2 hide">
																					<i class="icon alert-icon icon-comment"></i>
																					Please Enter Amount
																				</div>
																				<p data-error="price" class="ml-error"></p>
																				<div class="row-space-2">
																					{{ trans('messages.inbox.price_include_additional_fees') }}
																				</div>

																				<div class="row-space-2" id="price-breakdown"></div>
																			</fieldset>

																			<textarea style="resize: none; height: 162px; overflow: hidden; word-wrap: break-word;" class="row-space-2 input-resize-vert" placeholder="{{ trans('messages.inbox.include_msg',['first_name'=>$messages[0]->reservation->first_name]) }}" name="message"></textarea>

																			<div class="pull-right">
																				<input type="submit" value="{{ trans('messages.inbox.send_offer') }}" class="btn btn-primary" ng-click="reply_message('special_offer')">
																			</div>
																		</div>
																	</div>
																</div>
															</li>
														</ul>
													</form>
												</li>

												<li data-tracking-section="decline" class="option-list-item negative">
													<hr>

													<a class="option-link h3" href="javascript:void(0);">
														Tell the guest your property is not not unavailable
													</a>

													<form class="message_form negative" id="decline">
														<input type="hidden" value="" name="inquiry_post_id">

														<ul class="list-unstyled action-list hide">
															<!-- NOT_AVAILABLE -->
															<li data-key="dates_not_available" class="action-list-item template_NOT_AVAILABLE row-space-top-1">
																<hr>
																<label class="action-label">
																	<div class="row row-condensed">
																		<div class="col-sm-1">
																			<input type="radio" value="NOT_AVAILABLE" name="template">
																		</div>
																		<div class="col-sm-11">
																			<strong>{{ trans('messages.inbox.dates_not_available_block',['dates'=>$messages[0]->reservation->dates]) }}</strong>

																		</div>
																	</div>
																</label>

																<div class="row row-condensed">
																	<div class="col-sm-offset-1 col-sm-11">
																		<div class="drawer hide">
																			<p class="description">{{ trans('messages.inbox.calc_marked_unavailable',['dates'=>$messages[0]->reservation->dates]) }}</p>

																			<textarea style="resize: none; height: 82px; overflow: hidden; word-wrap: break-word;" class="row-space-2 input-resize-vert" placeholder="{{ trans('messages.inbox.optional_msg') }}" name="message"></textarea>

																			<div class="pull-right">
																				<input type="submit" value="{{ trans('messages.inbox.send') }}" class="btn btn-primary" ng-click="reply_message('dates_not_available')">
																			</div>
																		</div>
																	</div>
																</div>
															</li>
															<!-- 9 -->
															<li data-key="not_comfortable" class="action-list-item template_9 row-space-top-1">
																<hr>
																<label class="action-label">
																	<div class="row row-condensed">
																		<div class="col-sm-1">
																			<input type="radio" value="9" name="template">
																		</div>
																		<div class="col-sm-11">
																			<strong>Others</strong>
																		</div>
																	</div>
																</label>

																<div class="row row-condensed">
																	<div class="col-sm-offset-1 col-sm-11">
																		<div class="drawer hide">

																			<textarea style="resize: none; height: 82px; overflow: hidden; word-wrap: break-word;" class="row-space-2 input-resize-vert " placeholder="{{ trans('messages.inbox.optional_msg') }}" name="message"></textarea>

																			<div class="pull-right">
																				<input type="submit" value="{{ trans('messages.inbox.send') }}" class="btn btn-primary" ng-click="reply_message('not_comfortable')">
																			</div>
																		</div>
																	</div>
																</div>
															</li>
															<!-- 9 -->


														</ul>
													</form>
												</li>

												<li data-tracking-section="discussion" class="option-list-item neutral">
													<hr>

													<a class="option-link h3" href="javascript:void(0);">
														{{ trans('messages.inbox.write_back_to_learn') }}
													</a>

													<form class="message_form neutral" id="discussion">
														<input type="hidden" value="" name="inquiry_post_id">

														<ul class="list-unstyled action-list hide">
															<!-- 7 -->
															<li data-key="discussion" class="action-list-item template_7 row-space-top-1">
																<hr>
																<label class="action-label">
																	<div class="row row-condensed">
																		<div class="col-sm-1">
																			<input type="radio" value="7" name="template">
																		</div>
																		<div class="col-sm-11">
																			<strong>{{ trans('messages.inbox.need_answer_question') }}</strong>

																		</div>
																	</div>
																</label>

																<div class="row row-condensed">
																	<div class="col-sm-offset-1 col-sm-11">
																		<div class="drawer hide">

																			<textarea style="resize: none; height: 122px; overflow: hidden; word-wrap: break-word;" class="row-space-2 input-resize-vert required" placeholder="{{ trans('messages.inbox.only_guest_see_msg') }}" name="message"></textarea>

																			<div class="pull-right">
																				<input type="submit" value="{{ trans('messages.inbox.send') }}" class="btn btn-primary" ng-click="reply_message('discussion')">
																			</div>
																		</div>
																	</div>
																</div>
															</li>
														</ul>
													</form>
												</li>

											</ul>

										</div>
									</div>

								</div>
							</div>
						</div>
					</li>

					<div id="thread-list">
						@for($i=0; $i < count($messages); $i++)

						@if($messages[$i]->user_to == Auth::user()->user()->id)

						<li id="question2_post_{{ $messages[$i]->id }}" class="thread-list-item">

							@if($messages[$i]->message_type == 7)

							<div class="row row-condensed">
								<div class="col-md-10 col-md-offset-1">
									<div class="panel row-space-4">

										<div class="panel-body panel-dark">
											<span class="label label-info">
												{{ trans('messages.inbox.special_offer') }}
											</span>
											<div class="h5">

												{{ $messages[$i]->reservation->first_name }} {{ trans('messages.inbox.pre_approved_stay_at') }} <a href="{{ url('rooms/'.$messages[$i]->room_id) }}">{{ $messages[$i]->name }}</a>
											</div>
											<p class="text-muted">
												{{ $messages[$i]->dates }}
												·
												{{ $messages[$i]->number_of_guests }} {{ trans_choice('messages.home.guest',$messages[$i]->number_of_guests) }}

												<br>
												<strong>{{ trans('messages.inbox.you_could_earn') }} {{ $messages[$i]->currency->symbol.$messages[$i]->price }} {{ $messages[$i]->currency->session_code }}</strong> ({{ trans('messages.inbox.once_reservation_made') }})
											</p>

										</div>
										@if(@$messages[$i]->is_booked)
										<div class="panel-body">
											<a href="{{ url() }}/messaging/remove_special_offer/{{ $messages[$i]->special_offer_id }}" class="btn" data-confirm="Are you sure?" data-method="post" rel="nofollow">{{ trans('messages.inbox.remove_special_offer') }}</a>

										</div>
										@endif
									</div>
								</div>
							</div>
							@endif

							@if($messages[$i]->message_type == 6)
							<div class="row row-condensed">
								<div class="col-md-10 col-md-offset-1">
									<div class="panel row-space-4">
										<div class="panel-body panel-dark">
											<div class="h5">
												{{ $messages[$i]->reservation->first_name }} {{ trans('messages.inbox.pre_approved_stay_at') }} <a href="{{ url('rooms/'.$messages[$i]->reservation->room_id) }}">{{ $messages[$i]->name }}</a>
											</div>
											<p class="text-muted">
												{{ $messages[$i]->dates }}
												·
												{{ $messages[$i]->number_of_guests }} {{ trans_choice('messages.home.guest',$messages[$i]->number_of_guests) }}
												·
												{{ $messages[$i]->currency->symbol.($messages[$i]->price - $messages[$i]->reservation->host_fee) }} {{ $messages[$i]->currency->session_code }}

											</p>

										</div>
										@if(@$messages[$i]->is_booked)

										<div class="panel-body">
											<a href="{{ url() }}/messaging/remove_special_offer/{{ $messages[$i]->special_offer_id }}" class="btn" data-confirm="Are you sure?" data-method="post" rel="nofollow">{{ trans('messages.inbox.remove_pre_approval') }}</a>
										</div>@endif

									</div>
								</div>
							</div>
							@endif

							<div class="row row-condensed">
								<div class="col-sm-2 col-md-1 text-center">
									<a aria-label="{{ $messages[$i]->reservation->first_name }}" data-behavior="tooltip" class="media-photo media-round" href="{{ url() }}/users/show/{{ $messages[$i]->reservation->host_id }}"><img width="36" height="36" title="{{ $messages[$i]->reservation->first_name }}" src="{{ $messages[$i]->reservation->rooms->users->profile_picture->src }}" alt="{{ $messages[$i]->reservation->first_name }}"></a>
								</div>

								<div class="col-sm-10 ">
									<div class="row-space-4">
										<div class="panel panel-quote panel-quote-flush ">
											<div class="panel-body">

												<div class="message-text">
													<p class="trans">{{ $messages[$i]->message }}</p>
												</div>
											</div>

										</div>

										<div class="time-container text-muted ">
											<small title="{{ $messages[$i]->created_at }}" class="time">
												{{ $messages[$i]->created_time }}

											</small>
											<small class="exact-time hide">
												{{ $messages[$i]->created_at }}
											</small>
										</div>
									</div>

								</div>

							</div>
						</li>
						@endif

						@if($messages[$i]->user_from!= Auth::user()->user()->id)

						<li id="question2_post_{{ $messages[$i]->id }}" class="thread-list-item">

							@if($messages[$i]->message_type == 1 || $messages[$i]->message_type == 9)

							<div class="row row-condensed">
								<div class="col-md-10 col-md-offset-1">
									<div class="panel row-space-4">
										<div class="panel-body panel-dark">
											<div class="h5">
												{{ trans('messages.inbox.inquiry_about') }} <a locale="en" data-popup="true" href="{{ url() }}/rooms/{{ $messages[$i]->reservation->room_id }}">{{ $messages[$i]->reservation->rooms->name }}</a>
											</div>
											<p class="text-muted">
												{{ $messages[$i]->reservation->dates }}
												·
												{{ $messages[$i]->reservation->number_of_guests }} {{ trans_choice('messages.home.guest',$messages[$i]->reservation->number_of_guests) }}
												<br>
												{{ trans('messages.inbox.you_will_earn') }} {{ $messages[$i]->reservation->currency->symbol.$messages[$i]->reservation->host_payout }} {{ $messages[$i]->reservation->currency->code }}
											</p>

										</div>
									</div>
								</div>
							</div>
							@endif

							@if($messages[$i]->message_type == 10)
							<div class="inline-status text-branding space-6">
								<div class="horizontal-rule-text">
									<span class="horizontal-rule-wrapper">
										<span>
											<span>{{ trans('messages.inbox.reservation_declined') }} </span>
											<span>{{ $messages[$i]->created_time }}</span>
										</span>
									</span>
								</div>
							</div>
							@endif

							<div class="row row-condensed">

								<div class="col-sm-10 col-md-push-1">
									<div class="row-space-4">
										<div class="panel panel-quote panel-quote-flush panel-quote-right">
											<div class="panel-body">

												<div class="message-text">
													<p class="trans">{{ $messages[$i]->message }}</p>
												</div>
											</div>

										</div>

										<div class="time-container text-muted text-right">
											<small title="{{ $messages[$i]->created_at }}" class="time">
												{{ $messages[$i]->created_time }}

											</small>
											<small class="exact-time hide">
												{{ $messages[$i]->created_at }}
											</small>
										</div>
									</div>

								</div>

								<div class="col-sm-2 col-md-1 col-md-push-1 text-center">
									<a aria-label="{{ $messages[$i]->reservation->first_name }}" data-behavior="tooltip" class="media-photo media-round" href="{{ url() }}/users/show/{{ $messages[$i]->reservation->user_id }}"><img width="36" height="36" title="{{ $messages[$i]->reservation->first_name }}" src="{{ $messages[$i]->reservation->rooms->users->profile_picture->src }}" alt="{{ $messages[$i]->reservation->first_name }}"></a>
								</div>

							</div>
						</li>


						@endif
						@endfor
					</div>
				</ul>

			</div>
			<div class="col-lg-4 host-mini">

				<div class="panel row-space-4">

					<div class="mini-profile va-container media ">
						<div class="va-top pull-left">
							<a class="media-photo" href="{{ url() }}/users/show/{{ $messages[0]->reservation->user_id }}">
								<img width="100" height="100" alt="{{ $messages[0]->reservation->first_name }}" src="{{ $messages[0]->reservation->rooms->users->profile_picture->src }}">
							</a>
						</div>

						<div class="va-middle">
							<div class="h4">
								<a class="text-normal" href="{{ url() }}/users/show/{{ $messages[0]->reservation->user_id }}">{{ $messages[0]->reservation->first_name }}</a>
								&nbsp;<!-- <i data-tooltip-sticky="true" data-tooltip-position="bottom" data-tooltip-el="#verifications-tooltip" class="icon icon-verified-id icon-lima" id="verified-id-icon"></i> -->
								<br>
								<small>{{ $messages[0]->reservation->live }}</small>
								<br>
								<small>
									{{ trans('messages.profile.member_since') }} {{ date('Y',strtotime($messages[0]->reservation->created_at)) }}
								</small>
							</div>
						</div>
					</div>


					<div class="panel-header">
						{{ trans('messages.dashboard.verifications') }}
					</div>
					<div class="panel-body">
						<ul class="list-unstyled">
							@if($messages[0]->reservation->email == 'yes')
							<li class="row row-condensed">
								<div class="media">
									<i class="icon icon-ok icon-lima h3 pull-left"></i>
									<div class="media-body">
										<div>
											{{ trans('messages.dashboard.email_address') }}
										</div>
										<div class="text-muted">
											{{ trans('messages.dashboard.verified') }}
										</div>
									</div>
								</div>
							</li>
							@endif
							@if($messages[0]->reservation->phone_number == 'yes')
							<li class="row row-condensed">
								<div class="media">
									<i class="icon icon-ok icon-lima h3 pull-left"></i>
									<div class="media-body">
										<div>
											{{ trans('messages.profile.phone_number') }}
										</div>
										<div class="text-muted">
											{{ trans('messages.dashboard.verified') }}
										</div>
									</div>
								</div>
							</li>
							@endif
							@if($messages[0]->reservation->facebook == 'yes')
							<li class="row row-condensed">
								<div class="media">
									<i class="icon icon-ok icon-lima h3 pull-left"></i>
									<div class="media-body">
										<div>
											Facebook
										</div>
										<div class="text-muted">
											{{ trans('messages.dashboard.validated') }}
										</div>
									</div>
								</div>
							</li>
							@endif
							@if($messages[0]->reservation->google == 'yes')
							<li class="row row-condensed">
								<div class="media">
									<i class="icon icon-ok icon-lima h3 pull-left"></i>
									<div class="media-body">
										<div>
											Google
										</div>
										<div class="text-muted">
											{{ trans('messages.dashboard.validated') }}
										</div>
									</div>
								</div>
							</li>
							@endif
							@if($messages[0]->reservation->linkedin == 'yes')
							<li class="row row-condensed">
								<div class="media">
									<i class="icon icon-ok icon-lima h3 pull-left"></i>
									<div class="media-body">
										<div>
											LinkedIn
										</div>
										<div class="text-muted">
											{{ trans('messages.dashboard.validated') }}
										</div>
									</div>
								</div>
							</li>
							@endif
						</ul>
					</div>

				</div>
				<div class="select select-block row-space-2">
					{!! Form::select('hosting', $rooms, $messages[0]->reservation->room_id, ['id'=>'hosting']); !!}
				</div>
				<div class="custom_card">
					<div id="calendar-container" class="small-calendar">
						{!! $calendar !!}
					</div>

					<a href="{{ url() }}/manage-listing/{{ $messages[0]->reservation->room_id }}/calendar" id="edit_calendar_url">
						{{ trans('messages.inbox.full_calc_edit') }}
					</a>
				</div>

				<div class="custom_card">
					<h5>{{ trans('messages.inbox.protect_your_payments') }}</h5>
					<p>
						{{ trans('messages.inbox.never_pay_outside',['site_name'=>$site_name]) }}
					</p>
				</div>


			</div>
		</div>
	</div>
</main>
@stop

