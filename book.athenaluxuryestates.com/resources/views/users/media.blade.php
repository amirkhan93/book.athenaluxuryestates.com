@extends('template')
	<link
		href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
		rel="stylesheet" />
	<link
		href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
		rel="stylesheet">
		<link rel="stylesheet" href="https://cdn.materialdesignicons.com/3.8.95/css/materialdesignicons.min.css">
<link href="https://fonts.googleapis.com/css?family=Mansalva&display=swap" rel="stylesheet">
	<!-- CSS
	================================================== -->
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/bootstrap.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/font-awesome.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/ionicons.min.css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/jquery.fancybox.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/owl.carousel.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/owl.transitions.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/style.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/colors/color.css" />
@section('main')

<main id="site-content" role="main" class="inner_page_margin_d">
      
 @include('common.subheader')  
      
<div id="notification-area"></div>
<div class="page-container-responsive space-top-4 space-4">
  <div class="row">
    <div class="col-md-3 lang-chang-label space-sm-4">
      <div class="sidenav">
      @include('common.sidenav')
      </div>
      <a href="{{ url('users/show/'.Auth::user()->user()->id) }}" class="btn btn-primary btn-block row-space-top-4">{{ trans('messages.dashboard.view_profile') }}</a>
    </div>
    <div class="col-md-9">
      <div id="dashboard-content">
        
<div class="panel space-4">
  <div class="panel-header">
    {{ trans('messages.profile.profile_photo') }}
  </div>
  <div class="panel-body photos-section">
    <div class="row">
      <div class="col-lg-4 lang-chang-label text-center">
        <div data-picture-id="91711885" class="profile_pic_container picture-main space-sm-2 space-md-2">
          <div class="media-photo profile-pic-background" style="display:none;">
            <img width="225" height="225" title="{{ $result->first_name }}" src="{{ $result->profile_picture->src }}" alt="{{ $result->first_name }}">
          </div>
          <div class="media-photo media-round">
            <img width="225" height="225" title="{{ $result->first_name }}" src="{{ $result->profile_picture->src }}" alt="{{ $result->first_name }}">
          </div>
        </div>
      </div>
      <div class="col-lg-8">
        <ul class="list-layout picture-tiles clearfix ui-sortable"></ul>
        <p>
          {{ trans('messages.profile.profile_photo_desc') }}
        </p>
        <div class="ro w row-condensed">
          <div class="col-md-12">
            <span class="btn btn-primary btn-block btn-large file-input-container">
                {{ trans('messages.profile.upload_file_from_computer') }}
              <form name="ajax_upload_form" method="post" id="ajax_upload_form" enctype="multipart/form-data" action="{{ url() }}/users/image_upload" accept-charset="UTF-8">
                {!! Form::token() !!}
                  <input type="hidden" value="{{ $result->id }}" name="user_id" id="user_id">
                  <input type="file" name="profile_pic" id="user_profile_pic">
              </form>              
              <iframe style="display:none;" name="upload_frame" id="upload_frame"></iframe>
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
      </div>
    </div>
  </div>
</div>

    </main>

@stop