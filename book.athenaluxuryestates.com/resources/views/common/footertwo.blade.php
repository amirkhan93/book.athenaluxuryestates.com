<div class="section padding-top-bottom-small background-black  footer">
	<div class="container">
		<div class="row">
			<div class="col-md-4 text-center text-md-left">
			    <!--<h4  style="font-family: 'Sevillana', cursive;color: #fff;    text-transform: capitalize;">Bean D'Vine Luxury Vacation Homes</h4>-->
				<img src="{{url()}}/resources/assets/home/img/logo.png" alt="">
				<!--<p class="color-grey mb-4">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis-->
				<!--		praesentium voluptatum deleniti atque corrupti quos dolores.</p>-->
				<!--<p class="color-grey mt-4">Avenue Street 3284<br>Thessaloniki</p>-->
			</div>
			<div class="col-md-4 text-center text-md-left">
			<!--	<h6 class="color-white mb-3">Follow us</h6>
				<a href="https://www.facebook.com/Paradise-Valley-Luxury-Retreat-471742590315595/?business_id=724777274375101" target="_blank"><i class="mdi mdi-facebook"></i> Facebook</a>
				<a href="#" target="_blank"><i class="mdi mdi-twitter"></i> Twitter</a>
				<a href="#" target="_blank"><i class="mdi mdi-instagram"></i> Instagram</a>-->
			
			</div>
			<div class="col-md-4 mt-4 mt-md-0 text-center text-md-left logos-footer">					
				<!--	<h6 class="color-white mb-3">Bean D'Vine Luxury Vacation Homes</h6>
					<a href="https://beandvineluxhomes.com/rooms/8">Property Details</a>
				
					<a href="https://beandvineluxhomes.com/nearby-attractions">Nearby Attractions</a>
					
					<a href="https://beandvineluxhomes.com/about">About Us</a>
					<a href="https://beandvineluxhomes.com/contact">Contact Us</a>-->
					
				
			</div>
			<!--<div class="col mt-4 mt-md-0 text-center text-md-left logos-footer">					-->
			<!--		<h6 class="color-white mb-3">information</h6>-->
			<!--		<a href="#">Terms &amp; Conditions</a>-->
			<!--		<a href="#">House Rule</a>-->
			<!--		<a href="#">Privacy Policy</a>-->
			<!--		<a href="#">Testimonials</a>-->
					
				
			<!--</div>-->
		</div>
	</div>
</div>

<div class="section py-4 background-dark footer-bottom">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center  mb-2 mb-md-0">
				<p>2019 © Athena Luxury Estates. All rights reserved.</p>
			</div>
			
		</div>
	</div>
</div>


<div class="scroll-to-top"></div>