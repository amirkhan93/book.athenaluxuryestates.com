@include('common.head')

@if(!isset($exception))
	@if (Route::current()->uri() != 'api_payments/book/{id?}' && Route::current()->uri() != 'home/cancellation_policies') 
	 	@if(Session::get('get_token')=='')
	   		@include('common.headertwo')
	 	@endif
	@endif
@else   
        @if(Session::get('get_token')=='')
   			@include('common.headertwo')
   		@endif
@endif

@yield('main')

@if (!isset($exception))
	@if (Route::current()->uri() != 'payments/book/{id?}' && Route::current()->uri() != 'reservation/receipt' && Route::current()->uri() != 'api_payments/book/{id?}' && Route::current()->uri() != 'home/cancellation_policies')
	    @if(Session::get('get_token')=='')
		   @include('common.footertwo')
		@endif
	@endif
@else
    @if(Session::get('get_token')=='')
		@include('common.footertwo')
	@endif
@endif

@include('common.foot')