<div class="manage-listing-content-container" id="js-manage-listing-content-container">
  <div class="manage-listing-content-wrapper">
    <div class="manage-listing-content" id="js-manage-listing-content" style="background-color: transparent !important;">
      <div class="new-calendar"><div id="calendar-container col-lg-7 col-md-12">
        <div class="calendar-prompt-container"></div>

        <div class="calendar-settings-btn-container pull-right post-listed">
          <span class="label-contrast label-new
          hide">{{ trans('messages.lys.new') }}</span>
          <a class="text-normal link-icon" id="js-calendar-settings-btn" href="{{ url('manage-listing/'.$room_id.'/calendar') }}">
            <i class="icon icon-cog text-lead"></i>
            <span class="link-icon__text">{{ trans('messages.header.settings') }}</span>
          </a>
        </div>

        <div id="calendar">
          {!! $calendar !!}
          <footer class="space-top-6 calendar-footer-buttoned col-lg-12" style="margin-bottom:50px;">
            <li>
              <a href="" class="text-muted" id="import_button">{{ trans('messages.lys.import_calc') }}</a>
            </li>
            <li>
              <a class="js-calendar-sync text-muted" data-prevent-default="true" href="{{ url('calendar/sync/'.$result->id) }}">{{ trans('messages.lys.sync_other_calc') }}</a>
            </li>
            <li>
              <a href="" class="text-muted" id="export_button">{{ trans('messages.lys.export_calc') }}</a>
            </li>
          </footer>
        </div>
        <div id="calendar">
          @if($last_update_file)
          <h3>Last Import File Name</h3>
          @foreach($last_update_file as $last_update)
          <div>
            {{ $last_update->name }}
          </div>
          @endforeach
          @endif
        </div>
      </div>

      <div class="pricing-tips-sidebar-container"></div>
    </div></div>

    <div class="calendar-rules-overlay hide">
      <div class="panel text-center">
        <div class="panel-body">
          <div class="row row-condensed">
            <a class="modal-close hide" href="{{ url('admin/edit_room/'.$room_id) }}"></a>
            <div class="col-10 col-offset-1">
              <p class="row-space-2"><strong class="-heading"></strong></p>
              <div class="-example-image-container row-space-top-4">
                <!-- <img width="836" height="156" class="-example-image" src="./images/availability-previews@2x.png"> -->
              </div>
              <div class="-rule-caption"></div>
              <div class="-jump-to-month row-space-top-3 hide"></div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div id="calendar-rules" class="sidebar-overlay">
      <div class="sidebar-overlay-inner js-section">
        <h3 class="pull-left row-space-4 sidebar-overlay-heading">
          {{ trans('messages.lys.reservation_settings') }}
        </h3>
        <a href="{{ url('admin/edit_room/'.$room_id) }}" class="modal-close" data-prevent-default=""></a>
        <div class="js-saving-progress saving-progress" style="display: none;">
          <h5>{{ trans('messages.lys.saving') }}...</h5>
        </div>
        <div class="clearfix"></div>
        <div class="row-space-4">
          <label for="select-min_days_notice" class="text-muted">
            {{ trans('messages.lys.sameday_requests') }}
          </label>
          <div id="min-days-select" class="calendar-select"><div class="select                          select-block select-chosen">
            <select name="min_days_notice" id="select-min_days_notice" style="display: none;">
              <option value="-1" selected="selected">{{ trans('messages.lys.are_okay') }}</option>
              <option value="0">{{ trans('messages.lys.donot_want_sameday_requests') }}</option>
              <option value="1">{{ trans('messages.lys.donot_sameday_nextday_requests') }}</option>
            </select>
            <div class="chosen-container chosen-container-single chosen-container-single-nosearch" style="width: 279px;" title="" id="select_min_days_notice_chosen"><a class="chosen-single" tabindex="-1"><span>{{ trans('messages.lys.are_okay') }}</span><div><b></b></div></a><div class="chosen-drop"><div class="chosen-search"><input type="text" autocomplete="off" readonly="" /></div><ul class="chosen-results"></ul></div></div>
          </div>
        </div>
      </div>



      <div class="row-space-4">
        <label for="select-turnover_days" class="text-muted">
          {{ trans('messages.lys.preparation_time') }}

        </label>
        <div id="turnover-days-select" class="calendar-select"><div class="select                          select-block select-chosen">
          <select name="turnover_days" id="select-turnover_days" style="display: none;">

            <option value="0" selected="selected">{{ trans('messages.account.none') }}</option>

            <option value="1">{{ trans('messages.lys.saving',['count'=>1]) }}</option>

            <option value="2">{{ trans('messages.lys.saving',['count'=>2]) }}</option>

          </select><div class="chosen-container chosen-container-single chosen-container-single-nosearch" style="width: 279px;" title="" id="select_turnover_days_chosen"><a class="chosen-single" tabindex="-1"><span>{{ trans('messages.account.none') }}</span><div><b></b></div></a><div class="chosen-drop"><div class="chosen-search"><input type="text" autocomplete="off" readonly=""></div><ul class="chosen-results"></ul></div></div>
        </div>
      </div>
    </div>

    <div class="row-space-4">
      <label for="select-max_days_notice" class="text-muted">
        {{ trans('messages.lys.distant_requests') }}
      </label>
      <div id="max-days-select" class="calendar-select"><div class="select select-block select-chosen">
        <select name="max_days_notice" id="select-max_days_notice" style="display: none;">

          <option value="-1" selected="selected">{{ trans('messages.lys.guests_arriving_anytime') }}</option>

          <option value="90">{{ trans('messages.lys.guests_arrive_month',['count'=>3]) }}</option>

          <option value="180">{{ trans('messages.lys.guests_arrive_month',['count'=>6]) }}</option>

          <option value="365">{{ trans('messages.lys.guests_arrive_year') }}</option>

        </select><div class="chosen-container chosen-container-single chosen-container-single-nosearch" style="width: 279px;" title="" id="select_max_days_notice_chosen"><a class="chosen-single" tabindex="-1"><span>{{ trans('messages.lys.guests_arriving_anytime') }}</span><div><b></b></div></a><div class="chosen-drop"><div class="chosen-search"><input type="text" autocomplete="off" readonly=""></div><ul class="chosen-results"></ul></div></div>
      </div>
    </div>
  </div>

  <div data-hook="min_max_nights" class="row row-space-2"><div class="col-6">
    <label class="label-large">{{ trans('messages.lys.minimum_stay') }}</label>
    <div class="input-addon">
      <input name="min_nights_input_value" id="min-nights" value="" type="text" class="input-stem input-large">
      <span class="input-suffix">{{ trans('messages.lys.nights') }}</span>
    </div>
  </div>
  <div class="col-6">
    <label class="label-large">{{ trans('messages.lys.maximum_stay') }}</label>
    <div class="input-addon">
      <input name="max_nights_input_value" id="max-nights" value="" type="text" class="input-stem input-large">
      <span class="input-suffix">{{ trans('messages.lys.nights') }}</span>
    </div>
  </div>
  <p id="min-max-error" class="ml-error" style="display:none;"></p>
</div>
<div data-hook="seasonal_min_max_overview">
  <div class="row">
    <div class="col-12">
      <small>
        <a href="{{ url('manage-listing/'.$room_id.'/calendar') }}" class="text-muted link-underline" data-prevent-default="true">{{ trans('messages.lys.add_requirement_seasons') }}</a>
      </small>
    </div>
  </div></div>

  <div class="js-calendar-sync-section sidebar-overlay-highlight-section">

    <div></div>
    <h3 id="calendar_sync_heading" data-hook="calendar_sync_heading" class="row-space-4 sidebar-overlay-heading">
      {{ trans('messages.lys.sync_calc') }}
    </h3>
    <div data-hook="calendar_sync">
      <div class="space-2">
        <div class="row row-condensed">
          <div class="col-sm-12">
            <ul class="list-unstyled" style="margin-bottom:0;">
              <li class="space-1">
                <a href="{{ url('manage-listing/'.$room_id.'/calendar') }}" data-prevent-default="true" class="text-muted link-icon">
                  <i name="download" class="icon icon-download">
                  </i>
                  <span>&nbsp;</span>
                  <span>{{ trans('messages.lys.import_calc') }}</span>
                </a>
              </li>
              <li>
                <a href="{{ url('manage-listing/'.$room_id.'/calendar') }}" data-prevent-default="true" class="text-muted link-icon">
                  <i name="share" class="icon icon-share">
                  </i>
                  <span>&nbsp;</span>
                  <span>{{ trans('messages.lys.export_calc') }}</span>
                </a>
              </li>
            </ul>
            <p class="get_n_day" hidden="hidden"></p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<div class="manage-listing-help hide" id="js-manage-listing-help"></div>
</div>
<div class="manage-listing-content-background" style="background-color: transparent !important;"></div>
</div>

<div tabindex="-1" aria-hidden="true" role="dialog" class="modal show" id="export_popup">
  <div class="modal-table">
    <div class="modal-cell">
      <div class="modal-content">
        <div class="panel">
          <div class="panel-header">
            <span>{{ trans('messages.lys.export_calc') }}</span>
            <a data-behavior="modal-close" class="modal-close" href="javascript:;">
            </a>
          </div>
          <div class="panel-body">
            <p>
              <span>{{ trans('messages.lys.copy_past_ical_link') }}</span>
            </p>
            <input type="text" value="{{ url('calendar/ical/'.$result->id.'.ics') }}" readonly="">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<div tabindex="-1" aria-hidden="{{ ($errors->has('name')) ? 'false' : 'true' }}" role="dialog" class="modal show" id="import_popup">
  <div class="modal-table">
    <div class="modal-cell">
      <div style="max-width:552px;" class="modal-content">
        <div class="wizard">
          <div class="wizard-page-wrapper">
            <div class="panel">
              <div class="panel-header">
                <span>{{ trans('messages.lys.import_new_calc') }}</span>
                <a data-behavior="modal-close" class="modal-close" href="javascript:;">
                </a>
              </div>
              <div class="panel-body">
                <p style="margin-bottom:20px;">
                  <span>{{ trans('messages.lys.import_calendar_desc') }}</span>
                </p>
                {!! Form::open(['url' => url('calendar/import/'.$result->id), 'name' => 'export']) !!}
                <label style="margin-bottom:20px;padding:0;">
                  <p style="margin-bottom:10px;" class="label">
                    <span>{{ trans('messages.lys.calendar_address') }}</span>
                  </p>
                  <input type="text" value="{{ Input::old('url') }}" name="url" placeholder="{{ trans('messages.lys.ical_url_placeholder') }}" class="space-1 {{ ($errors->has('url')) ? 'invalid' : '' }}">
                  <span class="text-danger">{{ $errors->first('url') }}</span>
                </label>
                <label style="padding:0;margin-bottom:0;">
                  <p style="margin-bottom:10px;" class="label">
                    <span>{{ trans('messages.lys.name_your_calendar') }}</span>
                  </p>
                  <input type="text" value="{{ Input::old('name') }}" name="name" placeholder="{{ trans('messages.lys.ical_name_placeholder') }}" class="space-1 {{ ($errors->has('name')) ? 'invalid' : '' }}">
                  <span class="text-danger">{{ $errors->first('name') }}</span>
                </label>
                <div style="margin-top:20px;">
                  <button name="" data-prevent-default="true" class="btn btn-primary" ng-disabled="export.$invalid">
                    <span>{{ trans('messages.lys.import_calc') }}</span>
                  </button>
                </div>
                {!! Form::close() !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<style type="text/css">
#js-manage-listing-content-container, #ajax_container, .manage-listing-content-wrapper {
  height: auto !important;
}
.manage-listing-content, #site-content #ajax_container .manage-listing-content-wrapper{overflow: auto !important;}
.btn.btn-host{
  padding: 5px 10px !important;
  border: 1px solid #ccc !important;
  border-radius: 0px !important;
  top: 0px !important;
}

@media (min-width: 765px){
  html[lang="ar"] .calendar-month.col-lg-8.col-md-12{float: right;}
  html[lang="ar"] #ajax_container{
    left: 16.66667% !important;
  }
}
.subnav.ml-header-subnav{padding-right: 0px !important}
html[lang="ar"] .manage-listing-content {
  float: right !important;
  width: 100% !important;
}
html[lang="ar"] .calendar-month .tile,html[lang="ar"] .calendar-month .days-of-week li{float: right;}
html[lang="ar"] .calendar-month .month-nav-next{margin-left: 10px;margin-right:0px;margin-top: 2px;float: right;}
html[lang="ar"] .calendar-month .month-nav-prev{margin-left: 10px;}

html[lang="ar"] .has-collapsed-nav .manage-listing-nav{left: auto;right: 0px;width: 269px !important;}
html[lang="ar"] .has-collapsed-nav .manage-listing-nav.collapsed{left: auto;right: -270px;}

.btn_status_change{
  display: none;
}

</style>

{!! Html::script('js/jquery-1.11.3.js') !!}

<script type="text/javascript">
$(document).ready(function() {
  $('#js-manage-listing-nav').addClass('manage-listing-nav');
  $('#js-manage-listing-nav').removeClass('pos-abs');
  $('#js-manage-listing-nav').addClass('collapsed');
  $('#ajax_container').removeClass('mar-left-cont');
  var input = document.getElementById("myInput");

  input.onkeypress = function(e) {    e = e || window.event;
    var charCode = (typeof e.which == "number") ? e.which : e.keyCode;

    // Allow non-printable keys
    if (!charCode || charCode == 8 /* Backspace */ ) {
      return;
    }

    var typedChar = String.fromCharCode(charCode);

    // Allow numeric characters
    if (/\d/.test(typedChar)) {
      return;
    }

    // Allow the minus sign (-) if the user enters it first
    if (typedChar == "-" && this.value == "") {
      return;
    }

    // In all other cases, suppress the event
    return false;
  };

  $('.getprice').keyup(function(event){
    if (event.shiftKey == true) {
      event.preventDefault();
    }
    if($(this).val().indexOf('') !== -1 && event.keyCode == 190)
    event.preventDefault();

  });

  $('#s_chck1').addClass('btn_status_change');
  $('.segmented-control__input').change(function(){
    var options= $(this).val();

    // alert(options);
    if(options != "available")
    {
      $('#s_chck1').removeClass('btn_status_change');
      $('#s_chck').addClass('btn_status_change');
    }
    else
    {
      // alert(options);
      $('#s_chck').removeClass('btn_status_change');
      $('#s_chck1').addClass('btn_status_change');
      // $('.get_price').text(' ');
    }
  });

  $('#s_chck').click(function(){

    var new_price=$('.get_price').val();
    var ii_id= '{{ $room_id }}';
    $.ajax({
      type: "post",
      url: '{{ url() }}/admin/edit_room/'+ii_id+'/currency_check',
      data:{'n_price': new_price },
      success:function(data){
        if(data =='success')
        {
          setTimeout( function(){
            $('#s_chck1').click();
            $(".price_error").hide();
          }, 5000 );
        }
        else
        {
          console.log('false');
          $(".price_error").show();
          return false;
        }
      },
    });
  });
});
</script>
