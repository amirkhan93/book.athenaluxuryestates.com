@extends('admin.template')

@section('main')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Payment Gateway
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Payment Gateway</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- right column -->
        <div class="col-md-8 col-sm-offset-2">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Payment Gateway Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
              {!! Form::open(['url' => 'admin/yapstone_payment_gateway', 'class' => 'form-horizontal']) !!}
              <div class="box-body">
              <span class="text-danger">(*)Fields are Mandatory</span>
                <div class="form-group">
                  <label for="input_paypal_username" class="col-sm-3 control-label">Yapstone Username<em class="text-danger">*</em></label>

                  <div class="col-sm-6">
                    {!! Form::text('authrize_username', $result[0]->value, ['class' => 'form-control', 'id' => 'input_authrize_username', 'placeholder' => 'Yapstone Username']) !!}
                    <span class="text-danger">{{ $errors->first('authrize_username') }}</span>
                  </div>
                </div>
              
                <div class="form-group">
                  <label for="input_paypal_password" class="col-sm-3 control-label">Yapstone Password<em class="text-danger">*</em></label>

                  <div class="col-sm-6">
                    <!-- {!! Form::text('paypal_password', $result[1]->value, ['class' => 'form-control', 'id' => 'input_paypal_password', 'placeholder' => 'Authrize Password']) !!} -->
                    <input type="text" value="{{$result[1]->value}}" name="authrize_password" placeholder="Yapstone  Password" id="input_authrize_password" class="form-control">
                    <span class="text-danger">{{ $errors->first('authrize_password') }}</span>
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="input_paypal_password" class="col-sm-3 control-label">Yapstone Code<em class="text-danger">*</em></label>

                  <div class="col-sm-6">
                    <!-- {!! Form::text('paypal_password', $result[3]->value, ['class' => 'form-control', 'id' => 'input_paypal_code', 'placeholder' => 'Yapstone Password']) !!} -->
                    <input type="text" value="{{$result[3]->value}}" name="authrize_code" placeholder="Yapstone Code" id="input_authrize_code" class="form-control">
                    <span class="text-danger">{{ $errors->first('authrize_code') }}</span>
                  </div>
                </div>
              
              
               
                
                <div class="form-group">
                  <label for="input_paypal_mode" class="col-sm-3 control-label">Yapstone Mode</label>

                  <div class="col-sm-6">
                    {!! Form::select('authrize_mode', array('sandbox' => 'Sandbox', 'live' => 'Live'), $result[2]->value, ['class' => 'form-control', 'id' => 'input_authrize_mode']) !!}
                    
                  </div>
                </div>
              

              
             
              
              <!-- /.box-body -->
              <div class="box-footer">
                 <button type="submit" class="btn btn-info pull-right" name="submit" value="submit">Submit</button>
                 <button type="submit" class="btn btn-default pull-left" name="cancel" value="cancel">Cancel</button>
              </div>
              <!-- /.box-footer -->
            {!! Form::close() !!}
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@stop