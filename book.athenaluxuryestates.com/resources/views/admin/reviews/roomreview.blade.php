@extends('admin.template')

@section('main')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Reviews
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Reviews</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Reviews Management</h3>
              <div style="float:right;"><a class="btn btn-success" href="{{ url('admin/add_review') }}">Add Reviews</a></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
<table
                      class="table table-bordered table-striped" <?php if(count($results) > 0){?>id="example" <?php }?>>
                <thead>

                <tr>
                  <th>Sr No.</th>
                  <th>Room Id</th>
				  <th>Visitor Name</th>
				  <th>Review Title</th>
                  <th>Review</th>
				  
				  <th>Operation</th>
                 

                </tr>
                </thead>
                <tbody  id="table-div">

                @forelse($results as $key => $b)
                  <tr>

                    <td>{{  ++$key }}</td>
                    <td>{{ $b->room_id }}</td>
					<td>{{ $b->username }}</td>
					<td>{{ $b->review_title }}</td>
					<td>{{ $b->review }}</td>
                    <td><a href="{{ url('admin/delete_review/'.$b->id) }}" class="btn btn-primary btn-xs" > Delete</a></td>


                  </tr>

                @empty
                  <tr>
                    <th colspan="9" class="text-danger text-center text-uppercase">No found</th>
                  </tr>
                @endforelse
                </tbody>
              </table>
</div>
</div>
</div>
</div>
</section>
</div>
@endsection

@push('scripts')
<link rel="stylesheet" href="{{ url('css/buttons.dataTables.css') }}">
<script src="{{ url('js/dataTables.buttons.js') }}"></script>
<script src="{{ url('js/buttons.server-side.js') }}"></script>
<script>
$(document).ready(function() {
    $('#example').DataTable();
} );
</script>
@stop
