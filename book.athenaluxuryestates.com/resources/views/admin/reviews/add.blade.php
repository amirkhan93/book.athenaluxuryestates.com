@extends('admin.template')

@section('main')

<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Review
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Review</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- right column -->
        <div class="col-md-8 col-sm-offset-2">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Add Review Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['url' => 'admin/addreviews', 'class' => 'form-horizontal']) !!}
             
              <div class="box-body">
              <span class="text-danger">(*)Fields are Mandatory</span>
                <div class="form-group">
                  <label for="input_first_name" class="col-sm-3 control-label">Property<em class="text-danger">*</em></label>

                  <div class="col-sm-6">
                   <select id="room_id" name="room_id" class="form-control" required>
                       <option value="">Select</option>
                       <?php if($properties){
                       foreach($properties as $propertie){
                       ?>
                       <option value="<?php echo $propertie->id?>"><?php echo $propertie->name?></option>
                       <?php } }?>
                   </select>
                    <span class="text-danger">{{ $errors->first('first_name') }}</span>
                  </div>
                </div>
				
				<div class="form-group">
					<label for="username" class="col-sm-3 control-label">Visitor Name<em class="text-danger">*</em></label>
					<div class="col-sm-6">
						<input type="text" name="username" id="username" class="form-control" required>
					</div>
				</div>
				
				<div class="form-group">
					<label for="review_title" class="col-sm-3 control-label">Review Title<em class="text-danger">*</em></label>
					<div class="col-sm-6">
						<input type="text" name="review_title" id="review_title" class="form-control" required>
					</div>
				</div>
               
               
               <!--  <div class="form-group">
                  <label for="input_phone_no" class="col-sm-3 control-label">Phone No<em class="text-danger">*</em></label>

                  <div class="col-sm-6">
                    {!! Form::text('phone_no', '', ['class' => 'form-control', 'id' => 'input_phone_no', 'placeholder' => 'Phone No']) !!}
                    <span class="text-danger">{{ $errors->first('phone_no') }}</span>
                  </div>
                </div> -->

                <div class="form-group">
                  <label for="input_password" class="col-sm-3 control-label">Review<em class="text-danger">*</em></label>

                  <div class="col-sm-6">
                   <textarea id="review" name="review" class="form-control"></textarea>
                    <span class="text-danger">{{ $errors->first('password') }}</span>
                  </div>
                </div>
                
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
               <button type="submit" class="btn btn-info pull-right" name="submit" value="submit">Submit</button>
                 <button type="submit" class="btn btn-default pull-left" name="cancel" value="cancel">Cancel</button>
              </div>
              <!-- /.box-footer -->
            {!! Form::close() !!}
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@push('scripts')
<script>
  $('#input_dob').datepicker({ 'format': 'dd-mm-yyyy'});
</script>
@stop
@stop