@extends('layouts.myapp')

@section('content')
    <div class="content-wrapper">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Packages </h4>
                    <form class="form-sample" action="{{url('admin/packages/packageAssign')}}" method = "post">
                        {{csrf_field()}}
                        <p class="card-description">

                        </p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Packages </label>
                                    <div class="col-sm-9">
                                 <input type="hidden" name="package_id" value="{{$id}}">
                                
                                <select class="js-example-basic-multiple" multiple="multiple"  name="vendor_id[]"  id="vendor_id" style="width:100%;" required>
                                    <option value="">Select</option>
                                    @if(!empty($ac))
                                    @foreach($ac as $outlet)

                                        <option value="{{$outlet->id}}">{{$outlet->name}}</option>
                                    @endforeach
                                    @endif
                                </select> 
                                
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-sm-9">
                                        <input type="submit" class="form-control btn btn-primary" />
                                    </div>
                                </div>
                            </div>


                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="card">
            <div class="card-body">
                <h4 class="card-title">Assign Outlet</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Code</th>

                                    <th>Outlet Name</th>
                                    <th>Email</th>
                                  
                                   
                                    
                                </tr>
                                </thead>
                                <tbody>
                                 <?php $i = 1; foreach($pack_outlet as $outlet) {
                                    $getOutlet= \App\Vendor::where('id',$outlet->vendor_id)->first();
                                    //echo $getOutlet;
                                   // $getPackage= \App\packages::where('id',$outlet->package_id)->first();
                                    ?>
                                <tr>

                                    <td>{{ $i }}</td>
                                    <td>{{ $getOutlet->code }}</td>
                                    <td>{{ $getOutlet->name }}</td>
                                    <td>{{ $getOutlet->email }}</td>
                                  
                                  
                                   
                                </tr>
                            <?php } ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
