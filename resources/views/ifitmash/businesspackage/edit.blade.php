@extends('layouts.myappwf')

@section('content')
    <div class="content-wrapper">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Package Type</h4>
                    <form class="form-sample" action="{{url('member/businesspackage/update/'.$ac->id)}}" method ="post">
                        {{csrf_field()}}
                        <p class="card-description">

                        </p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label"> Package</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name = "name" value="{{$ac->name}}"/>
                                        @if($errors->has('name'))
                                            <span class="text-danger">{{$errors->first('name')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Tax Type</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="taxtype" value="{{$ac->taxtype}}"/>
                                        @if($errors->has('name'))
                                            <span class="text-danger">{{$errors->first('name')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Price</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name = "price" value="{{$ac->price}}"/>
                                                    @if($errors->has('name'))
                                                        <span class="text-danger">{{$errors->first('name')}}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Start Date</label>
                                    <div class="col-sm-9">
                                        <input type="date" placeholder="dd/mm/yyyy" class="form-control" name="startdate" value="{{$ac->start_date}}"/>
                                        @if($errors->has('name'))
                                            <span class="text-danger">{{$errors->first('name')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">End Date</label>
                                    <div class="col-sm-9">
                                        <input type="date" placeholder="dd/mm/yyyy" class="form-control" name="enddate" value="{{$ac->end_date}}"/>
                                        @if($errors->has('name'))
                                            <span class="text-danger">{{$errors->first('name')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-sm-9">
                                        <input type="submit" class="form-control btn btn-primary" />
                                    </div>
                                </div>
                            </div>



                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection
