@extends('layouts.myappwf')
@section('content')
    <!-- Main content -->
    <div class="content-wrapper">
        <div >
            <?php //echo Auth::user()->id;?>

            <a  href="{{ url('member/role/create') }}" style="
    background: #1976d2;
    border: 1px solid #1976d2;
    text-decoration: none;
    font-size: 14px;
    padding: 2px 5px;
    border-radius: 4px;
    color: #fff;
    cursor: pointer;
">Add User Role</a>

        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">User Role</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>

                                    <th>Name</th>
                                    <th>Dispaly Name</th>
                                    <th>Description</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                  @foreach($roleData as $data)

                                    <td><a href="{{ ($data->id != 1) ? url("member/role/edit/$data->id") :'#'}}">{{ $data->name }}</a></td>

                                    <td>{{ $data->display_name }}</td>
                                    <td>{{ $data->description }}</td>

                                <td>
                                    @if ($data->id != 1)

                                        <button   class="btn btn-outline-primary"><a href="{{ url("member/role/edit/$data->id") }}">Edit</a></button>
                                        <button   class="btn  btn-danger"><a href="{{ url("member/role/delete/$data->id") }}">Delete</a></button>


                                    @endif
                                    </td>
                                </tr>
                                 @endforeach


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Add Popup Start -->

    </div>

@endsection

