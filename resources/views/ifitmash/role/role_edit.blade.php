@extends('layouts.myappwf')

@section('content')
    <div class="content-wrapper">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">User Role </h4>
                    <form class="form-sample" action="{{ url('member/role/update') }}" method = "post">
                        {{csrf_field()}}
                        <input type="hidden" value="{{$role->id}}" name="id" id="id">
                        <p class="card-description">

                        </p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label"> Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="name" value="{{$role->name}}"/>
                                        @if($errors->has('name'))
                                            <span class="text-danger">{{$errors->first('name')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Display Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="display_name" value="{{$role->display_name}}"/>
                                        @if($errors->has('name'))
                                            <span class="text-danger">{{$errors->first('display_name')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Description</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="description" value="{{$role->description}}"/>
                                        @if($errors->has('name'))
                                            <span class="text-danger">{{$errors->first('description')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">Permission</label>

                                <div class="col-sm-6">

                                    <ul style="display: inline-block;list-style-type: none;padding:0; margin:0;">

                                        @foreach($permissions as $row)

                                            <li class="checkbox" style="display: inline-block; min-width: 155px;">
                                                <label>
                                                    <input type="checkbox" name="permission[]" value="{{ $row->id }}" >
                                                    {{ $row->display_name }}
                                                </label>
                                            </li>

                                        @endforeach

                                    </ul>

                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-sm-9">

                                        <input type="submit" class="form-control btn btn-primary" />
                                    </div>
                                </div>
                            </div>


                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection
