@extends('layouts.myappwf')

@section('content')
  <div class="content-wrapper">
    <div class="col-12 grid-margin">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">UsersForm </h4>
          <form class="form-sample" action="{{url('member/users/update/'.$acs->id)}}" method = "post" enctype="multipart/form-data">
            {{csrf_field()}}
            <p class="card-description">

            </p>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label">Username </label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" name ="name" value="{{$acs->username}}"/>
                    @if($errors->has('name'))
                      <span class="text-danger">{{$errors->first('name')}}</span>
                    @endif
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label">Email </label>
                  <div class="col-sm-9">
                    <input type="email" class="form-control" name="email" value="{{$acs->email}}"/>
                    @if($errors->has('email'))
                      <span class="text-danger">{{$errors->first('email')}}</span>
                    @endif
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label">Password </label>
                  <div class="col-sm-9">
                    <input type="password" class="form-control" name="password"  value=""/>
                    @if($errors->has('password'))
                      <span class="text-danger">{{$errors->first('passsword')}}</span>
                    @endif
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label">Roles</label>
                  <div class="col-sm-9">
                    <select class="form-control "  name="roles"  id="roles">
                      <option value="">Select</option>

                      @if($ac)
                        @foreach($ac as $users)

                          <option value="{{$users->id}}">{{$users->name}}</option>
                        @endforeach
                      @endif

                    </select>
                  </div>
                </div>
                <div class="form-group row">
                  <label class="col-sm-3 col-form-label">Select Outlet</label>
                  <div class="col-sm-9">
                    <select class="js-example-basic-multiple "  name="outlet"  id="outlet" multiple="multiple" style="width:100px;">
                      <option value="">Select</option>

                      @if($outlet)
                        @foreach($outlet as $users)

                          <option value="{{$users->id}}">{{$users->outlet_name}}</option>
                        @endforeach
                      @endif

                    </select>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group row">
                  <div class="col-sm-9">
                    <input type="submit" class="form-control btn btn-primary" />
                  </div>
                </div>
              </div>
            </div>


          </form>
        </div>
      </div>
    </div>

  </div>
@endsection
