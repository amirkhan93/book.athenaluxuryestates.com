@extends('layouts.myappwf')

@section('content')

  <div class="content-wrapper">
    <div >
      <a  href="{{ url('member/users/create') }}" style="
    background: #1976d2;
    border: 1px solid #1976d2;
    text-decoration: none;
    font-size: 14px;
    padding: 2px 5px;
    border-radius: 4px;
    color: #fff;
    cursor: pointer;
">Add Users</a>
    </div>
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">User Form</h4>
        <div class="row">
          <div class="col-12">
            <div class="table-responsive">
              <table id="order-listing" class="table">
                <thead>
                <tr>
                  <th>Id</th>
                   <th>Code</th>
                  <th>Username</th>
                  <th>Email</th>
                  <th>Roles</th>
                  <th>Outlet</th>
                   <th>Status</th>
                  <th>Actions</th>
                </tr>
                </thead>
                <?php $i = 1; foreach($bst as $user) {?>
                  <?php  $roles=\App\Role::where('id',$user->roles_id)->first();
                         $outlet=\App\Addnewoutlet::where('id',$user->outlet_id)->first();
                  ?>
                <tr>

                  <td>{{ $i }}</td>
                  <td>{{$user->code}}</td>
                  <td>{{ $user->username }}</td>
                  <td>{{ $user->email}}</td>
                  <td>@if($roles){{$roles->name}}@endif</td>
                  <td>@if($outlet){{$outlet->outlet_name}}@endif</td>
                    <td>@if($user->status != 1)
                            <a href="{{url('member/users/active/'.$user->id)}}"
                               onclick="if(! confirm('Click Ok To Active ')){ return false}"
                               class="btn btn  btn-danger" data-toggle="tooltip" >In-Active</a>
                        @else
                            <a href="{{url('member/users/inactive/'.$user->id)}}"
                               onclick="if(! confirm('Click Ok To InActivate')){ return false}"
                               class="btn btn-success btn-sm" data-toggle="tooltip" >Active</a>
                        @endif</td>
                  <td>
                    <button  class="btn btn-outline-primary"><a href="{{ url('member/users/edit',[$user->id]) }}">Edit</a></button>
                    <button   class="btn  btn-danger"><a href="{{ url('member/users/delete',[$user->id]) }}">Delete</a></button>
                  </td>
                </tr>
                <?php $i++;}?>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>



  </div>
@endsection
