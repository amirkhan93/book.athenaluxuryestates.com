@extends('layouts.myappwf')

@section('content')
    <div class="content-wrapper">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Add New Member information</h4>
                    <div class="tab">
                        <button class="tablinks" onclick="openCity(event, 'generalinfo')">Profile</button>
                        <button class="tablinks" onclick="openCity(event, 'bankdetail')">Tags</button>

                    </div>
                    <form class="form-sample" action="{{url('member/addnewmember/store')}}" method = "post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <p class="card-description">

                        </p>
                        <div id="generalinfo" class="tabcontent" class = "col-md-12" style="display:block">
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Member Id</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="memberid"/>
                                        @if($errors->has('memberid'))
                                            <span class="text-danger">{{$errors->first('memberid')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="name"/>
                                        @if($errors->has('name'))
                                            <span class="text-danger">{{$errors->first('name')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Email Id</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="email"/>
                                        @if($errors->has('email'))
                                            <span class="text-danger">{{$errors->first('email')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Contact Number</label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" name="contact"/>
                                        @if($errors->has('contact'))
                                            <span class="text-danger">{{$errors->first('contact')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>



                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Gender</label>
                                    <div class="col-sm-4">
                                        <div class="form-check form-check-warning">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input" name="gender" id="male" value="male" checked>
                                                Male
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="form-check form-check-warning">
                                            <label class="form-check-label ">
                                                <input type="radio" class="form-check-input" name="gender" id="female" value="female">
                                                Female
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>




                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">DOB</label>
                                    <div class="col-sm-9">
                                        <input type="date"  placeholder="dd/mm/yyyy" class="form-control" name="dob"/>
                                        @if($errors->has('dob'))
                                            <span class="text-danger">{{$errors->first('dob')}}</span>
                                        @endif
                                    </div>
                                </div>

                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Select Source<sup>*</sup>  </label>
                                    <div class="col-sm-9">
                                        <select class="form-control"  name="sources"  id="sources">
                                            <option value="">Select</option>

                                            @if($ac)
                                                @foreach($ac as $users)

                                                    <option value="{{$users->id}}">{{$users->name}}</option>
                                                @endforeach
                                            @endif

                                        </select>
                                        @if($errors->has('sources'))
                                            <span class="text-danger">{{$errors->first('sources')}}</span>
                                        @endif

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Note
                                    </label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" id="note" name="note" rows="4"></textarea>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Active/Block
                                    </label>
                                    <label class="switch">
                                        <input type="checkbox" id = "active/block" name="active" checked >
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label"> Is Personal Class
                                    </label>
                                    <label class="switch">
                                        <input type="checkbox" id = "chkbox" onclick="check()">
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                            <div id = "desworkdiv" class = "col-md-12" style = "display:none">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Description
                                        </label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control" id="description" name="description" rows="4"></textarea>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Workout name
                                        </label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control" id="workoutname" name="workoutname" rows="4"></textarea>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Address
                                        </label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control" id="address" name="address" rows="4"></textarea>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>

                            <div id="bankdetail" class="tabcontent"  class = "col-md-12" style = "display:none">
                                <div class="col-md-12">
                                    <div class="form-group row">

                                        <div class="col-sm-12">
                                            <select class="form-control "  id="seacrhtags" name="searchtags" placeholder="searchtags">


                                                @foreach($tags as $users)

                                                    <option value="{{$users->id}}">{{$users->name}}</option>
                                                @endforeach

                                            </select>

                                        </div>
                                    </div>
                                </div>
                            </div>


                        <div class="row">
                        <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-sm-9">
                                        <input type="submit" value="Add and Generate invoice" class="form-control btn btn-primary" />
                                    </div>
                                </div>
                            </div>
                         </div>


                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('js')
    <script>
        /*$('#chkbox').click(function(){
          //  if($('#chkbox')=='on')
            {
            //alert('hello');
          //   if($('#chkbox').checked == true){
                 document.getElementById('desworkdiv').style.display = "block";
            // }
           // }else{
           //    document.getElementById('desworkdiv').style.display = "none";
            }

        });*/
        function check() {
            var c =document.getElementById("chkbox").checked;
            var d=  document.getElementById('desworkdiv');
            if (c == true){

                d.style.display = "block";
            } else {
                d.style.display = "none";
            }



        }
    </script>
@endsection

<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #2196F3;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
</style>
<script>
    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }
</script>
<style>
    body {font-family: Arial;}

    /* Style the tab */
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #ccc;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        /* border: 1px solid #ccc;*/

        border-top: none;
    }
</style>