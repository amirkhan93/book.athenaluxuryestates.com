
@extends('layouts.myappoutlet')
@section('content')
    <div class="content-wrapper">

        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Expired Membership</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Member Name</th>
                                    <th>Email</th>
                                    <th>Contact</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php $i = 1;
                                // echo $grace_period; die;

                                foreach($expired_membership as $user) {?>


                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $user->name}}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->contact}}</td>
                                    <td>{{ date('Y-m-d',strtotime($user->created_at)) }}</td>
                                    <td>{{ date('Y-m-d',strtotime($user->expiry_date))  }}</td>
                                </tr>
                                <?php $i++;}?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
