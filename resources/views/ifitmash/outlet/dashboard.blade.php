@extends('layouts.myappoutlet')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                <a href="{{url('outlet/today_lead')}}">
                    <div class="card">
                        <div class="card-body">
                            <h1 class="font-weight-light mb-4">{{count($lead)}}</h1>
                            <div class="d-flex flex-wrap align-items-center">
                                <div>
                                    <h4 class="font-weight-normal">Today's Lead</h4>
                                    <p class="text-muted mb-0 font-weight-light"></p>
                                </div>
                                <i class="mdi mdi-account-multiple icon-lg text-primary ml-auto"></i>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                <a href="{{url('outlet/addnewlead')}}">
                <div class="card">
                    <div class="card-body">
                        <h1 class="font-weight-light mb-4">{{count($lead)}}</h1>
                        <div class="d-flex flex-wrap align-items-center">
                            <div>
                                <h4 class="font-weight-normal">Trial Booked </h4>
                                <p class="text-muted mb-0 font-weight-light"></p>
                            </div>
                            <i class="mdi mdi-calendar text-primary ml-auto"></i>
                        </div>
                    </div>
                </div>
            </a>
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                <a href="{{url('outlet/followup_lead')}}">
                <div class="card">
                    <div class="card-body">
                        <h1 class="font-weight-light mb-4">{{count($leadfollow)}}</h1>
                        <div class="d-flex flex-wrap align-items-center">
                            <div>
                                <h4 class="font-weight-normal">Leads Followup</h4>
                                <p class="text-muted mb-0 font-weight-light"></p>
                            </div>
                            <i class="mdi mdi-message-processing text-primary ml-auto"></i>
                        </div>
                    </div>
                </div>
            </a>
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                <a href="{{url('outlet/members_followup')}}" target="_blank">
                    <div class="card">
                        <div class="card-body">
                            <h1 class="font-weight-light mb-4">{{count($member)}}</h1>
                            <div class="d-flex flex-wrap align-items-center">
                                <div>
                                    <h4 class="font-weight-normal">Members Followup</h4>
                                    <p class="text-muted mb-0 font-weight-light"></p>
                                </div>
                                <i class="mdi mdi-verified icon-lg text-primary ml-auto"></i>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                <a href="{{url('outlet/members_bdy')}}" target="_blank">
                    <div class="card">
                        <div class="card-body">
                            <h1 class="font-weight-light mb-4">{{count($memberBdy)}}</h1>
                            <div class="d-flex flex-wrap align-items-center">
                                <div>
                                    <h4 class="font-weight-normal">Member Birthday</h4>
                                    <p class="text-muted mb-0 font-weight-light"></p>
                                </div>
                                <i class="mdi mdi-cake-variant icon-lg text-primary ml-auto"></i>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                <a href="{{url('outlet/renewal_due')}}" target="_blank">
                    <div class="card">
                        <div class="card-body">
                            <h1 class="font-weight-light mb-4">{{ $renewal_due }}</h1>
                            <div class="d-flex flex-wrap align-items-center">
                                <div>
                                    <h4 class="font-weight-normal">Upcoming Renewal </h4>
                                    <p class="text-muted mb-0 font-weight-light"></p>
                                </div>
                                <i class="mdi mdi mdi-clock text-primary ml-auto"></i>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                <a href="{{url('outlet/expired_membership')}}" target="_blank">
                <div class="card">
                    <div class="card-body">
                        <h1 class="font-weight-light mb-4">{{$expired_membership}}</h1>
                        <div class="d-flex flex-wrap align-items-center">
                            <div>
                                <h4 class="font-weight-normal">Expired Membership</h4>
                                <p class="text-muted mb-0 font-weight-light"></p>
                            </div>
                            <i class="mdi mdi mdi-clock text-primary ml-auto"></i>
                        </div>
                    </div>
                </div>
            </a>
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                <a href="{{url('outlet/inactivemember')}}" target="_blank">
                <div class="card">
                    <div class="card-body">
                        <h1 class="font-weight-light mb-4">{{count($inactivemember)}}</h1>
                        <div class="d-flex flex-wrap align-items-center">
                            <div>
                                <h4 class="font-weight-normal">InActive Members</h4>
                                <p class="text-muted mb-0 font-weight-light"></p>
                            </div>
                            <i class="mdi mdi-verified icon-lg text-primary ml-auto"></i>
                        </div>
                    </div>
                </div>
            </a>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-8 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex align-items-start justify-content-between">
                            <h4 class="card-title">Overall Deals</h4>
                            <div class="dropdown">
                                <button class="btn btn-sm btn-outline-secondary dropdown-toggle" type="button" id="dropdownMenuDate" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    11 May 2018
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuDate">
                                    <a class="dropdown-item" href="#">24 Jan 2019</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">25 Jan 2019</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">26 Jan 2019</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#"> 27 Jan 2019</a>
                                </div>
                            </div>
                        </div>
                        <div id="statistics-legend" class="chartjs-legend mt-2 mb-4"></div>
                        <canvas id="statistics-chart"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-md-4 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Deals Closed</h4>
                        <ul class="bullet-line-list">
                            <li>
                                <h6>New Lead Added</h6>
                                <p class="mb-0">Next follow up lead</p>
                                <p class="text-muted">
                                    <i class="mdi mdi-clock-outline"></i>
                                    Status-Interested
                                </p>
                            </li>
                            <li>
                                <h6>Leads Converted</h6>
                                <p class="mb-0"></p>
                                <p class="text-muted">
                                    <i class="mdi mdi-clock-outline"></i>
                                    {{count($lead)}}
                                </p>
                            </li>
                            <li>
                                <h6>Leads Lost/Archived</h6>
                                <p class="mb-0"></p>
                                <p class="text-muted">
                                    <i class="mdi mdi-clock-outline"></i>
                                    {{count($lostlead)}}
                                </p>
                            </li>
                            <li>

                            <li>
                                <h6>Total Active Mmebers</h6>
                                <p class="mb-0"></p>
                                <p class="text-muted">
                                    <i class="mdi mdi-clock-outline"></i>
                                    {{count($activemember)}}
                                </p>
                            </li>

                            <li>
                                <h6>Total InActive Mmebers</h6>
                                <p class="mb-0"></p>
                                <p class="text-muted">
                                    <i class="mdi mdi-clock-outline"></i>
                                    {{count($inactivemember)}}
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex align-items-start justify-content-between">
                            <h4 class="card-title">Member Followup</h4>
                            <div class="dropdown">
                                <button class="btn p-0" type="button" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <i class="mdi mdi-dots-horizontal text-primary"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton1">
                                    <a class="dropdown-item" href="#">Expired Membership</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Total Deals</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Leads</a>
                                </div>
                            </div>
                        </div>
                        @foreach($member as $user)
                            <?php   $outlet=\App\Addnewoutlet::where('id',$user->outlet_id)->first();                  ?>
                        <div class="preview-list">
                            <div class="preview-item pt-0">
                                <div class="preview-thumbnail">
                                    <img src="{{url('resources/assets/images/'.$user->profile_pic)}}" alt="image" class="rounded-circle">
                                </div>
                                <div class="preview-item-content flex-grow-1">
                                    <h6>{{$user->name}}</h6>
                                    <p class="text-muted font-weight-light mb-0">{{$user->created_at}}</p>
                                </div>
                                <p class="mb-0">{{$outlet->outlet_name}}</p>
                            </div>
                        </div>
                        @endforeach

            </div>
                </div>
            </div>
            <div class="col-md-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Analysis</h4>
                        <canvas id="analysis-chart"></canvas>
                        <div class="d-lg-flex justify-content-around mt-5">
                            <div class="text-center mb-3 mb-lg-0">
                                <h3 class="font-weight-light text-success">+40.02%</h3>
                                <p class="text-muted mb-0">Leads Added</p>
                            </div>
                            <div class="text-center mb-3 mb-lg-0">
                                <h3 class="font-weight-light text-danger">-2.5%</h3>
                                <p class="text-muted mb-0">Expired Membership</p>
                            </div>
                            <div class="text-center">
                                <h3 class="font-weight-light text-primary">+23.65%</h3>
                                <p class="text-muted mb-0">Active Membership</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
@endsection
@section('js')
<script>
$('#head_venue').change(function(){
	var value = $("#head_venue").val();
	//alert(value);
	$.ajax({
		url:"{{url('outlet/change_id')}}",
		type:"GET",
		data:{"value":value},
		dataType:"html",
	success:function(data){
		location.reload();
	}	
	});
});
</script>
@endsection