
@extends('layouts.myappoutlet')

@section('content')
    <div class="content-wrapper">

        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Member's Details</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Member Name</th>
                                    <th>Email</th>
                                    <th>Contact</th>
                                    <th>DOB</th>
                                    <th>Invoice Date</th>
                                   
                                </tr>
                                </thead>
                                <tbody>

                                <?php $i = 1;
                               
                                //print_r($inactivem);die;
                                foreach($invoice as $user) {?>

                                <tr>

                                    <td>{{ $i }}</td>
                                    <td>{{ $user->name}}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->contact}}</td>
                                    <td>{{ $user->dob}}</td>
                                    <td>{{ $user->invoice}}</td>
                                  
                                </tr>
                                <?php $i++;}?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
