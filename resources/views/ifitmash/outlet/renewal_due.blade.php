
@extends('layouts.myappoutlet')
@section('content')
    <div class="content-wrapper">

        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Upcoming Renewals</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Member Name</th>
                                    <th>Email</th>
                                    <th>Contact</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Grace Period [in days]</th>
                                    <th>Last Date[with grace period]</th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php $i = 1;
                                // echo $grace_period; die;

                                foreach($renewal_due as $user) {?>


                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{ $user->name}}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->contact}}</td>
                                    <td>{{ date('Y-m-d',strtotime($user->created_at)) }}</td>
                                    <td>{{ date('Y-m-d',strtotime($user->pack_ending_date))  }}</td>
                                    <td>{{ $user->grace_period}}</td>
                                    <td>{{ date('Y-m-d',strtotime($user->with_grace))  }}</td>
                                </tr>
                                <?php $i++;}?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
