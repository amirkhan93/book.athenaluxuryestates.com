@extends('layouts.myappwf')

@section('content')  
    <div class="content-wrapper">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Add New Outlet</h4>
                    <div class="tab">
                        <?php 
                        $cities=\App\Cities::where('id',$act->city_id)->first();
                        ?>
                        <button class="tablinks" onclick="openCity(event, 'generalinfo')">General Information</button>
                        <button class="tablinks" onclick="openCity(event, 'bankdetail')">Bank Details</button>

                    </div> 
                    <form class="form-sample" action="{{url('member/addnewoutlet/update/'.$act->id)}}" method = "post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <p class="card-description">

                        </p>
                        <div id="generalinfo" class="tabcontent" class = "col-md-12" style="display:block">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="name" value="{{$act->outlet_name}}"/>
                                        @if($errors->has('name'))
                                            <span class="text-danger">{{$errors->first('name')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Facilities Available</label>
                                    <div class="col-sm-9">
                                        <?php
                                        $facilities = explode(",", $act->facility_id);
                                    
                                        ?>
                                        <select class="js-example-basic-multiple" name="facility_id[]"  id="facility_id" multiple="multiple" style="width:100%">
                                            <option value="">Select</option>
                                            @foreach($ac as $users)
                                            
                                                <option value="{{$users->id}}" <?php foreach($facilities as $key => $facility){if($facility == $users->id){echo "selected";}}?>>{{$users->name}}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('facility_id'))
                                            <span class="text-danger">{{$errors->first('facility_id')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Outlet Manager photo</label>
                                    <div class="col-sm-9">
                                        <input type="file" class="form-control" name="managerpick" value="" accept = 'image/jpeg , image/jpg, image/gif, image/png'/>
                                        <input type="hidden" name="old_manager_photo" value="{{$act->outlet_manager_photo}}">
                                        @if($errors->has('name'))
                                            <span class="text-danger">{{$errors->first('name')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Outlet Manager Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="managername" value="{{$act->outlet_manager_name}}" required="" />
                                        @if($errors->has('name'))
                                            <span class="text-danger">{{$errors->first('name')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div> 
 
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label"> Outlet photos</label>
                                    <div class="col-sm-9">
                                        <input type="file" class="form-control" name="outletpick[]" id="outletpick" value="" multiple="" accept = 'image/jpeg , image/jpg, image/gif, image/png' onchange="return  imageLimit()" />
                                        <input type="hidden" name="old_outlet_photo" value="{{$act->outlet_photos}}">
                                        @if($errors->has('name'))
                                            <span class="text-danger">{{$errors->first('name')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Primary Contact Number</label>
                                    <div class="col-sm-9">
                                        <input type="number" min="0" class="form-control" name="contact" value="{{$act->contact}}" required=""/>
                                        @if($errors->has('contact'))
                                            <span class="text-danger">{{$errors->first('contact')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Secondary contact number</label>
                                    <div class="col-sm-9">
                                        <input type="number" min="0" class="form-control" name="contact2" value="{{$act->secondary_contact}}">
                                        @if($errors->has('contact2'))
                                            <span class="text-danger">{{$errors->first('contact2')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>



                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Outlet address</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="address1" value="{{$act->street_address1}}"/>
                                        @if($errors->has('name'))
                                            <span class="text-danger">{{$errors->first('name')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>


                            <!-- <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Street Address2</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="address2" value="{{$act->street_address2}}"/>
                                        @if($errors->has('name'))
                                            <span class="text-danger">{{$errors->first('name')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div> -->

                            <div class="col-md-6"> 
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Country  </label>
                                    <div class="col-sm-9">
                                        <select class="form-control "  id="country_id" name="country_id">
                                            <option value="101" selected>India</option>

                                        </select>
                                        @if($errors->has('country_id'))
                                            <span class="text-danger">{{$errors->first('country_id')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">State</label>
                                    <div class="col-sm-9">
                                        <select class="form-control"  id="state_id" name="state_id">
                                            <option value="">Select</option>

                                            @foreach($states as $state)

                                            <option value="{{$state->id}}" <?php if($act->state_id == $state->id){echo "selected";} ?>>{{$state->name}}</option>

                                            @endforeach

                                        </select>
                                        @if($errors->has('state_id'))
                                            <span class="text-danger">{{$errors->first('state_id')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Cities  </label>
                                    <div class="col-sm-9">
                                        <select class="form-control "  name="cities_id"  id="cities_id">
                                            <option value="">Select</option>
                                            <option value="{{$act->city_id}}" selected>{{$cities->name}}</option>
                                        </select>
                                        @if($errors->has('cities_id'))
                                            <span class="text-danger">{{$errors->first('cities_id')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Google location</label>
                                    <div class="col-sm-9">
                                        <input type="text"  value="{{$act->googlemaps}}" placeholder="" class="form-control"  name="map" id="map" placeholder="Search address" />
                                        @if($errors->has('dob'))
                                            <span class="text-danger">{{$errors->first('dob')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">PinCode
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text"  placeholder=""  value="{{$act->pincode}}" class="form-control" name="pincode" value=""/>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Grace Periods [in days]</label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" name="grace_period" 
                                        value="{{$act->grace_period}}"/>
                                        @if($errors->has('grace_period'))
                                            <span class="text-danger">{{$errors->first('grace_period')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Email Id</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name = "email" value="{{$act->email}}"/>
                                        @if($errors->has('email'))
                                            <span class="text-danger">{{$errors->first('email')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                        </div>
                        </div>

                        <div id="bankdetail" class="tabcontent"  class = "col-md-12" style = "display:none">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">A/c holder name
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text"  placeholder="" class="form-control" name="payeename" value="{{$act->payeename}}"/>
                                        @if($errors->has('payeename'))
                                            <span class="text-danger">{{$errors->first('payeename')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Bank Name
                                    </label>
                                    <div class="col-sm-9">
                                        <select class="form-control "  id="bankname_id" name="bankname_id">
                                            <option value="">Select</option>


                                            @foreach($bankname as $users)

                                                <option value="{{$users->id}}" <?php if($users->id == $act->bankname_id){echo "selected";} ?>>{{$users->name}}</option>
                                            @endforeach

                                        </select>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Account Number
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text"  placeholder="" class="form-control" name="accountno" value="{{$act->accountno}}"/>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">IFSC Code
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text"  placeholder="" class="form-control" name="ifsccode" value="{{$act->ifsccode}}"/>

                                    </div>
                                </div>
                            </div>

                            


                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Account Type
                                    </label>
                                    <div class="col-sm-9">
                                        <select class="form-control "  id="accounttype_id" name="accounttype_id">
                                            <option value="">Select</option>


                                            @foreach($accounttype as $users)

                                                <option value="{{$users->id}}" <?php if($users->id == $act->accounttype_id){echo "selected";} ?>>{{$users->name}}</option>
                                            @endforeach

                                        </select>

                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-sm-9">
                                        <input type="submit" class="form-control btn btn-primary" />
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $('#country_id').change(function(){
        var chkArray = [];
        $('#country_id option:selected').each(function(){
            chkArray.push($(this).val());
        });
        var selected;
        selected = chkArray.join(',') ;
        $('#country_id').val(selected);
        //alert('hello');die;
        $.ajax({
            type : 'POST',
            url : '{{url('member/addnewoutlet/states')}}',
            data : {id : selected, _token: '{{csrf_token()}}' },
            success : function(res){
                $('#state_id').html(res);

            }
        }); 
    })
    $('#state_id').change(function(){
        var chkArray = [];
        $('#state_id option:selected').each(function(){
            chkArray.push($(this).val());
        });
        
        var selected;
        selected = chkArray.join(',') ;
        $('#state_id').val(selected);
        $.ajax({
            type : 'POST',
            url : '{{url('member/addnewoutlet/cities')}}',
            data : {id : selected, _token: '{{csrf_token()}}' },
            success : function(res){
                $('#cities_id').html(res);
            }
        });
    })
</script>
<script>
    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }
</script>
<style>
    body {font-family: Arial;}

    /* Style the tab */
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #ccc;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        /* border: 1px solid #ccc;*/

        border-top: none;
    }
</style>
@endsection