@extends('layouts.myappwf')

@section('content') 
    <div class="content-wrapper"> 
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Add New Outlet Detail</h4>
                <div class="row">
        <div class="widget-body hotel-info-table table-responsive">
        <table class="table table-bordered" border="2px" bgcolor="white">
            <thead>
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th>Value</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 1; foreach($bn as $user) { ?>
            <?php

            // echo 'd';exit;
            $facility=\App\Facilities::where('id',$user->facility_id)->first();
            $accounttype=\App\Accounttype::where('id',$user->accounttype_id)->first();
            $bankname=\App\Bankname::where('id',$user->bankname_id)->first();
            $paymentmode=\App\Payment::where('id',$user->paymentmode_id)->first();
            $bills=\App\Billingcycle::where('id',$user->billingcycle_id)->first();
            $countries=\App\Counteries::where('id',$user->country_id)->first();
            $states=\App\States::where('id',$user->state_id)->first();
            $cities=\App\Cities::where('id',$user->city_id)->first();
            // echo  $user->status_id;die;



            ?>

            <tr>
                <td>1</td>
                <td>Outlet Code</td>
                <td>{{ $user->code }}</td>
            </tr>

            <tr>
                <td>2</td>
                <td>Outlet Name</td>
                <td>{{ $user->outlet_name}}</td>
            </tr>

            <tr> 
                <td>3</td>
                <td>Outlet Manager Image</td>
                <td><img src="{{url('resources/assets/images/userimages/'.$user->outlet_manager_photo)}}" style="width: 50px; height: 50px; border-radius: 0;"></td>
            </tr>

            <tr>
                <td>4</td>
                <td>Outlet Manager Name</td>
                <td>{{ $user->outlet_manager_name}}</td>
            </tr>

            <tr>
                
                <td>5</td>
                <td>Outlet Images</td>
                <td>
                <?php  
                $outletImages = explode(",", $user->outlet_photos);
                //print_r($outletImages);die();

                  foreach ($outletImages as $key => $image) {
                      ?>
                     <span><img src="{{url('resources/assets/images/'.$image)}}" style="width: 50px; height: 50px; border-radius: 0;"> </span>

                    <?php
                  }
                
                ?>
                </td>
            </tr>


            <tr>
                <td>6</td>
                <td> Email Spoc</td>
                <td>{{ $user->email}}</td>
            </tr>

            <tr>
                <td>7</td>
                <td>Primary Contact</td>
                <td>{{ $user->contact}}</td>
            </tr>

            <tr>
                <td>8</td>
                <td>Seconday Contact</td>
                <td>{{ $user['secondary_contact']}}</td>
            </tr>
            <tr>
                <td>9</td>
                <td>Street Address1</td>
                <td>{{ $user->street_address1}}</td> 
            </tr>
            
            <tr>
                <td>10</td>
                <td>Country</td>
                <td>{{ $countries['name']}}</td>
            </tr>

            <tr>
                <td>10</td>
                <td>State</td>
                <td>{{ $states['name']}}</td>
            </tr>

            <tr>
                <td>10</td>
                <td>city</td>
                <td>{{ $cities['name']}}</td>
            </tr>
                
            
            <tr>
                <td>11</td>
                <td>Facilities Avaible</td>
                <td>{{ $facility->name}}</td>
            </tr> 
             


            <tr>
                <td>12</td>
                <td>Pincode</td>
                <td>{{ $user->pincode}}</td>
            </tr>

            <tr>
                <td>13</td>
                <td>Google location</td>
                <td>{{ $user->googlemaps}}</td>
            </tr>

            <!-- <tr>
                <td>18</td>
                <td>Agreement Summary</td>
                <td>{{ $user->summary}}</td>
            </tr> -->

            <tr>
                <td>14</td>
                <td>Payee Name</td>
                <td>{{ $user->payeename}}</td>
            </tr>

            <tr>
                <td>15</td>
                <td>Account Number</td>
                <td>{{ $user->accountno}}</td>
            </tr>

            <tr>
                <td>16</td>
                <td>IFSC Code</td>
                <td>{{ $user->ifsccode}}</td>
            </tr>

            <tr>
                <td>17</td>
                <td>Bank Name</td>
                <td>{{ $bankname->name}}</td>
            </tr>

            <tr>
                <td>18</td>
                <td>Account Type</td>
                <td>{{ $accounttype->name}}</td>
            </tr>

            <?php $i++;}?>
            </tbody>
        </table>
    </div>
    </div>
    </div>
     </div>
    </div>

@endsection
