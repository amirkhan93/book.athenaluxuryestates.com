<?php
use App\Http\Helpers\Helpers; 
//$langs = Helpers::languages();
?>
 
@extends('layouts.myappwf')

@section('content') 

    <div class="content-wrapper">
        <div class="col-12 grid-margin">
            <div class="card" >
                <div class="card-body">
                    <h4 class="card-title">Add New Outlet</h4>
                    <div class="tab">
                        <button class="tablinks" onclick="openCity(event, 'generalinfo')">General Information</button>
                        <button class="tablinks" onclick="openCity(event, 'bankdetail')">Bank Details</button>

                    </div>
                    <form class="form-sample" id ="outletForm" action="{{url('member/addnewoutlet/store')}}" method = "post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <p class="card-description">

                        </p>
                        <div id="generalinfo" class="tabcontent" class = "col-md-12" style="display:block">
                           <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label"> Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="name" value="" required="" />
                                        @if($errors->has('name'))
                                            <span class="text-danger">{{$errors->first('name')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Facilities Available</label>
                                    <div class="col-sm-9">
                                        <select class="js-example-basic-multiple"   name="facility_id[]"  id="facility_id" multiple="multiple" style="width:100%;">
                                            <option value="">Select</option>
                                            @foreach($ac as $users)

                                                <option value="{{$users->id}}">{{$users->name}}</option>
                                            @endforeach
                                        </select> 
                                        @if($errors->has('facility_id'))
                                            <span class="text-danger">{{$errors->first('facility_id')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Outlet Manager photo</label>
                                    <div class="col-sm-9">
                                        <input type="file" class="form-control" name="managerpick" value="" required="" accept = 'image/jpeg , image/jpg, image/gif, image/png'/>
                                        @if($errors->has('name'))
                                            <span class="text-danger">{{$errors->first('name')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Outlet Manager Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="managername" value="" required="" />
                                        @if($errors->has('name'))
                                            <span class="text-danger">{{$errors->first('name')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
 
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label"> Outlet photos</label>
                                    <div class="col-sm-9">
                                        <input type="file" class="form-control" name="outletpick[]" id="outletpick" value="" required="" multiple="" accept = 'image/jpeg , image/jpg, image/gif, image/png' onchange="return  imageLimit()" />
                                        @if($errors->has('name'))
                                            <span class="text-danger">{{$errors->first('name')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Primary Contact Number</label>
                                    <div class="col-sm-9">
                                        <input type="number" min="0" class="form-control" name="contact" value="" required=""/>
                                        @if($errors->has('contact'))
                                            <span class="text-danger">{{$errors->first('contact')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Secondary contact number</label>
                                    <div class="col-sm-9">
                                        <input type="number" min="0" class="form-control" name="contact2" value="">
                                        @if($errors->has('contact2'))
                                            <span class="text-danger">{{$errors->first('contact2')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Outlet address</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="address1" value="" required=""/>
                                        @if($errors->has('address1'))
                                            <span class="text-danger">{{$errors->first('address1')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>


                            <!-- <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Street Address2</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="address2" value="" required=""/>
                                        @if($errors->has('address2'))
                                            <span class="text-danger">{{$errors->first('address2')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div> -->

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Country  </label>
                                    <div class="col-sm-9">
                                        <select class="form-control "  id="country_id" name="country_id" >
                                            
                                                <option value="101" selected>India</option>
                                            
                                        </select>
                                        @if($errors->has('country_id'))
                                            <span class="text-danger">{{$errors->first('country_id')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">State</label>
                                    <div class="col-sm-9">
                                        <select class="form-control"  id="state_id" name="state_id">
                                            
                                            <option value="">Select</option>

                                            @foreach($states as $state)
                                            <option value="{{$state->id}}">{{$state->name}}</option>
                                            @endforeach
                                        </select>
                                        @if($errors->has('state_id'))
                                            <span class="text-danger">{{$errors->first('state_id')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Cities  </label>
                                    <div class="col-sm-9">
                                        <select class="form-control "  name="cities_id"  id="cities_id">
                                            <option value="">Select</option>
                                        </select>
                                        @if($errors->has('cities_id'))
                                            <span class="text-danger">{{$errors->first('cities_id')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Google location</label>
                                    <div class="col-sm-9">
                                        <input type="text"  placeholder="" class="form-control"  name="map" id="map" placeholder="Search address" />
                                        @if($errors->has('dob'))
                                            <span class="text-danger">{{$errors->first('dob')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">PinCode
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text"  placeholder="" class="form-control" name="pincode" value=""/>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Grace Periods [in days]</label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" name="grace_period" 
                                        value="" required=""/>
                                        @if($errors->has('grace_period'))
                                            <span class="text-danger">{{$errors->first('grace_period')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Email Id</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name = "email" value="" required=""/>
                                        @if($errors->has('email'))
                                            <span class="text-danger">{{$errors->first('email')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                               <div class="col-md-6">
                                   <div class="form-group row">
                                       <label class="col-sm-3 col-form-label">Password</label>
                                       <div class="col-sm-9">
                                           <input type="password" class="form-control" name="password" value="" required=""/>
                                           @if($errors->has('name'))
                                               <span class="text-danger">{{$errors->first('name')}}</span>
                                           @endif
                                       </div>
                                   </div>
                               </div>
                           

                        </div>

                        </div>

                        <div id="bankdetail" class="tabcontent"  class = "col-md-12" style = "display:none">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">A/c holder name
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text"  placeholder="" class="form-control" name="payeename" value="" required=""/>
                                        @if($errors->has('payeename'))
                                            <span class="text-danger">{{$errors->first('payeename')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Bank Name
                                    </label>
                                    <div class="col-sm-9">
                                        <select class="form-control "  id="bankname_id" name="bankname_id" required="">
                                            <option value="">Select</option>


                                            @foreach($bankname as $users)

                                                <option value="{{$users->id}}">{{$users->name}}</option>
                                            @endforeach

                                        </select>
                                        @if($errors->has('bankname_id'))
                                            <span class="text-danger">{{$errors->first('bankname_id')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Account Number
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="number"  placeholder="" class="form-control" name="accountno" value=""/>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">IFSC Code
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="text"  placeholder="" class="form-control" name="ifsccode" value=""/>

                                    </div>
                                </div>
                            </div>

                            


                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Account Type
                                    </label>
                                    <div class="col-sm-9">
                                        <select class="form-control "  id="accounttype_id" name="accounttype_id" required="">
                                            <option value="">Select</option>


                                            @foreach($accounttype as $users)

                                                <option value="{{$users->id}}">{{$users->name}}</option>
                                            @endforeach

                                        </select>
                                        @if($errors->has('accounttype_id'))
                                            <span class="text-danger">{{$errors->first('accounttype_id')}}</span>
                                        @endif
                                    </div>
                                </div> 
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-9">
                                            <input type="submit" class="form-control btn btn-primary" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </form>
            </div>
        </div>

    </div>
    </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <script>
            $('#country_id').change(function(){
               // alert('helo');
                var chkArray = [];
                $('#country_id option:selected').each(function(){
                    chkArray.push($(this).val());
                });
                var selected;
                selected = chkArray.join(',') ;
                $('#country_id').val(selected);
                //alert('hello');die;
                $.ajax({
                    type : 'POST',
                    url : '{{url('member/addnewoutlet/states')}}',
                    data : {id : selected, _token: '{{csrf_token()}}' },
                    success : function(res){
                        $('#state_id').html(res);

                    }
                });
            })
            $('#state_id').change(function(){
                var chkArray = [];
                $('#state_id option:selected').each(function(){
                    chkArray.push($(this).val());
                });
                var selected;
                selected = chkArray.join(',') ;
                $('#state_id').val(selected);
                $.ajax({
                    type : 'POST',
                    url : '{{url('member/addnewoutlet/cities')}}',
                    data : {id : selected, _token: '{{csrf_token()}}' },
                    success : function(res){
                        $('#cities_id').html(res);
                    }
                });
            })
        </script>
<script>
    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }
</script>
<style>
    body {font-family: Arial;}

    /* Style the tab */
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #ccc;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
       /* border: 1px solid #ccc;*/

          border-top: none;
    }
</style>
@endsection
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyByXjSMx3jg-oU5G5BG-REAcCskcmTuyVM&v=3.exp&sensor=false&libraries=places"></script>

<script type="text/javascript">
    function initialize() { 
     var input = document.getElementById('map');
     new google.maps.places.Autocomplete(input);
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">

 function imageLimit() {
    if ($("#outletpick")[0].files.length > 6) {
        alert("You can select only 6 images");
        $("#outletpick").val('');
        return false;
    } 
}
</script>