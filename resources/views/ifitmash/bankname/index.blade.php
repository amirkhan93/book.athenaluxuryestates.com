@extends('layouts.myapp')

@section('content')
    <div class="content-wrapper">
        <div >
            <a data-toggle="modal" data-target="#exampleModal" href="#" style="
    background: #1976d2;
    border: 1px solid #1976d2;
    text-decoration: none;
    font-size: 14px;
    padding: 2px 5px;
    border-radius: 4px;
    color: #fff;
    cursor: pointer;
">Add New</a>
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Bank Name</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>code</th>
                                    <th>Bank Name</th>
                                    <th>Created Date</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; foreach($bst as $user) {?>
                                <tr>

                                    <td>{{ $i }}</td>
                                    <td>{{$user->code}}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->created_at}}</td>
                                    <td>@if($user->status != 1)
                                            <a href="{{url('member/bankname/active/'.$user->id)}}"
                                               onclick="if(! confirm('Click Ok To Active ')){ return false}"
                                               class="btn btn  btn-danger" data-toggle="tooltip" >In-Active</a>
                                        @else
                                            <a href="{{url('member/bankname/inactive/'.$user->id)}}"
                                               onclick="if(! confirm('Click Ok To InActivate')){ return false}"
                                               class="btn btn-success btn-sm" data-toggle="tooltip" >Active</a>
                                        @endif</td>
                                    <td>
                                        <button  class="btn btn-outline-primary"><a href="{{ url('admin/bankname/edit',[$user->id]) }}">Edit</a></button>
                                        <button  href="{{ url('admin/source/delete') }}" class="btn  btn-danger"><a href="{{ url('admin/bankname/delete',[$user->id]) }}">Delete</a></button>
                                    </td>
                                </tr>
                                <?php $i++;}?>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Add Popup Start -->
        <div class="modal fade custom-popup" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Bank Name</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="content-wrapper">
                        <div class="col-12 grid-margin">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Bank Name</h4>
                                    <form class="form-sample" action="{{url('admin/bankname/store')}}" method = "post">
                                        {{csrf_field()}}
                                        <p class="card-description">

                                        </p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">Bank Name</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" name = "name"/>
                                                        @if($errors->has('name'))
                                                            <span class="text-danger">{{$errors->first('name')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-sm-9">
                                                        <input type="submit" class="form-control btn btn-primary" />
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- Add Popup Ends -->
    </div>
@endsection
