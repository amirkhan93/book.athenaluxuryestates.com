@extends('layouts.myappwf')

@section('content')
    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">IFitmash Members Detail</h4>
                <div class="row">
        <div class="widget-body hotel-info-table table-responsive">
        <table class="table table-bordered" border="2px" bgcolor="white">
            <thead>
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th>Value</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 1; foreach($vendors as $user) {?>
            <?php

            // echo 'd';exit;
            $supName = \App\Businesstype::where('id',$user->businesstype_id)->first();
            $bs = \App\Businesspackage::where('id',$user->businesspackage_id)->first();
            // echo  $supName;die;



            ?>
            <tr>
                <td>1</td>
                <td> Name</td>
                <td>{{ $user->name }}</td>
            </tr>

            <tr>
                <td>2</td>
                <td>Business Name</td>
                <td>{{$user->business_name}}</td>
            </tr>

            <tr>
                <td>3</td>
                <td>Business Type</td>
                <td>{{ $supName->name}}</td>
            </tr>
            <tr>
                <td>4</td>
                <td>Business Pacakge</td>
                <td>{{ $bs->name}}</td>
            </tr>
            <tr>
                <td>5</td>
                <td>Days</td>
                <td>{{ $bs->days}}</td>
            </tr>
            <tr>
                <td>6</td>
                <td>Price</td>
                <td>{{ $bs->price}}</td>
            </tr>
            <tr>
                <td>7</td>
                <td>Email id</td>
                <td>{{ $user->email }}</td>
            </tr>
            <?php $i++;}?>
            </tbody>
        </table>
    </div>
    </div>
    </div>
     </div>
    </div>

@endsection
