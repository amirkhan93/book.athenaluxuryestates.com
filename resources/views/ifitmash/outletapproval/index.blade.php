@extends('layouts.myappwf')

@section('content')
    <div class="content-wrapper">

        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Approval Outlet List</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>

                                    <th>Outlet Id</th>
                                    <th>Outlet Name</th>
                                     <th>Signup Date</th>
                                    <th>Status</th>

                                </tr>
                                </thead>
                                <tbody>

                                <?php $i = 1; foreach($bn as $user) {?>
                              <?php

                                // echo 'd';exit;
                              //$vendor = \App\Vendor::where('id',$user->vendor_id)->first();

                             //  echo   $businessame;die;



                                ?>

                                <tr>

                                    <td>{{ $i }}</td>

                                    <td>{{$user->code }}</td>
                                    <td>{{ $user->outlet_name }}</td>
                                     <td>{{$user->created_at}}</td>

                                    <td>
                                       <!-- <button  href="{{ url('admin/size/edit') }}" class="btn btn-danger"><a href="{{ url('admin/members/view/'.$user->id) }}">Detail</a></button>-->
                                        @if($user->status != 1)
                                            <a href="{{url('member/outlet/active/'.$user->id)}}"
                                               onclick="if(! confirm('Click Ok To Active ')){ return false}"
                                               class="btn btn  btn-danger" data-toggle="tooltip" >In Active</a>
                                        @else
                                            <a href="{{url('member/outlet/inactive/'.$user->id)}}"
                                               onclick="if(! confirm('Click Ok To InActivate')){ return false}"
                                               class="btn btn-success btn-sm" data-toggle="tooltip" >Active</a>
                                        @endif</td>

                                      <!--  <button  href="{{ url('admin/unit/edit') }}" class="btn btn-outline-primary"><a href="{{ url('admin/supplier/edit',[$user->id]) }}">Edit</a></button>-->
                                     <!--   <button  href="{{ url('admin/source/delete') }}" class="btn  btn-danger"><a href="{{ url('admin/supplier/delete',[$user->id]) }}">Delete</a></button>-->

                                </tr>
                                <?php $i++;}?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
