@extends('layouts.myappwf')

@section('content')
    <div class="content-wrapper">
        <div >
            <a href="{{ url('member/addnewlead/create') }}" style="
    background: #1976d2;
    border: 1px solid #1976d2;
    text-decoration: none;
    font-size: 14px;
    padding: 2px 5px;
    border-radius: 4px;
    color: #fff;
    cursor: pointer;
">Add New</a>
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Follow Up Lead</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>code</th>

                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Contact</th>
                                    <th>Address</th>

                                    <th>Action</th>

                                </tr>
                                </thead>
                                <tbody>

                                <?php $i = 1; foreach($bn as $user) {?>


                                <tr>

                                    <td>{{ $i }}</td>
                                    <td>{{$user->code}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->contact}}</td>

                                    <td>{{ $user->address}}</td>

                                    <td><!--@if($user->status != 1)
                                            <a href="{{url('member/addnewlead/active/'.$user->id)}}"
                                               onclick="if(! confirm('Click Ok To Active ')){ return false}"
                                               class="btn btn  btn-danger" data-toggle="tooltip" >In-Active</a>
                                        @else
                                            <a href="{{url('member/addnewlead/inactive/'.$user->id)}}"
                                               onclick="if(! confirm('Click Ok To InActivate')){ return false}"
                                               class="btn btn-success " data-toggle="tooltip" >Active</a>
                                        @endif-->
                                        <button  class="btn btn-outline-primary"><a href="{{ url('member/addnewlead/edit',[$user->id]) }}">Edit</a></button>
                                       <!-- <button  href="{{ url('admin/source/delete') }}" class="btn  btn-danger"><a href="{{ url('member/addnewlead/destroy',[$user->id]) }}">Delete</a></button>-->
                                       <button   class="btn btn-danger"><a href="{{ url('member/addnewlead/view/'.$user->id) }}">Detail</a></button>
                                       <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" >
                                                    <i class="fa fa-ellipsis-v"></i>
                                           <ul class="dropdown-menu">
                                               <li>Call</li>
                                               <li>Sms</li>
                                               <li><a href="{{ url('member/addnewlead/feedlist/'.$user->id) }}">Feedback</a></li>
                                           </ul>
                                                </button>
                                    </td>
                                </tr>
                                <?php $i++;}?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
