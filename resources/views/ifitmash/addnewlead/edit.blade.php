@extends('layouts.myappwf')

@section('content')
    <div class="content-wrapper">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Add New Lead information</h4>
                    <form class="form-sample" action="{{url('member/addnewlead/update',$ac->id)}}" method = "post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <p class="card-description">

                        </p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label"> Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="name" value="{{$ac->name}}"/>
                                        @if($errors->has('name'))
                                            <span class="text-danger">{{$errors->first('name')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Email Id</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name = "email" value="{{$ac->email}}"/>
                                        @if($errors->has('email'))
                                            <span class="text-danger">{{$errors->first('email')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Contact Number</label>
                                    <div class="col-sm-9">
                                        <input type="number" min="0" class="form-control" name="contact" value="{{$ac->contact}}"/>

                                    </div>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label ">Select Status<sup>*</sup> </label>
                                    <div class="col-sm-9">
                                        <select class="form-control"  name="status"  id="status">
                                            <option value="">Select</option>

                                            @if($acs)
                                                @foreach($acs as $users)

                                                    <option value="{{$users->id}}">{{$users->name}}</option>
                                                @endforeach
                                            @endif

                                        </select>
                                        @if($errors->has('status'))
                                            <span class="text-danger">{{$errors->first('status')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>




                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Select Source<sup>*</sup>  </label>
                                    <div class="col-sm-9">
                                        <select class="form-control"  name="sources"  id="sources">
                                            <option value="">Select</option>

                                            @if($act)
                                                @foreach($act as $users)

                                                    <option value="{{$users->id}}">{{$users->name}}</option>
                                                @endforeach
                                            @endif

                                        </select>
                                        @if($errors->has('sources'))
                                            <span class="text-danger">{{$errors->first('sources')}}</span>
                                        @endif

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Gender</label>
                                    <div class="col-sm-4">
                                        <div class="form-check form-check-warning">
                                            <label class="form-check-label">
                                                <input type="radio" class="form-check-input" name="gender" id="male" value="male" checked>
                                                Male
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <div class="form-check form-check-warning">
                                            <label class="form-check-label ">
                                                <input type="radio" class="form-check-input" name="gender" id="female" value="female">
                                                Female
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">DOB</label>
                                    <div class="col-sm-9">
                                        <input type="date"  placeholder="dd/mm/yyyy" class="form-control" value="{{$ac->dob}}" name="dob"/>
                                        @if($errors->has('dob'))
                                            <span class="text-danger">{{$errors->first('dob')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Next Follow up date
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="date"  placeholder="dd/mm/yyyy" class="form-control" name="nextfollow" value="{{$ac->followup}}"/>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Trail Date
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="date"  placeholder="dd/mm/yyyy" class="form-control" name="traildate" value="{{$ac->traildate}}"/>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Trail Time
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="time"  placeholder="0:0:0" class="form-control" name="trailtime" value="{{$ac->trailtime}}"/>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Address
                                    </label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" id="address" name="address"  rows="4">{{$ac->address}}"</textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Note
                                    </label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" id="note" name="note" rows="4">{{$ac->note}}"</textarea>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Personal Training
                                    </label>
                                    <label class="switch">
                                        <input type="checkbox" id = "chkbox" onclick="check()">
                                        <span class="slider round"></span>
                                    </label>
                                </div>
                            </div>
                            <div id = "desworkdiv" class = "col-md-12" style = "display:none">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Description
                                        </label>
                                        <div class="col-sm-9">
                                            <textarea class="form-control" id="description" name="description" rows="4">{{$ac->description}}</textarea>

                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Workout name
                                        </label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" id="workname" name="workname" value="{{$ac->workname}}">

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-sm-9">
                                        <input type="submit" class="form-control btn btn-primary" />
                                    </div>
                                </div>
                            </div>
                        </div>


                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection
@section('js')
    <script>
        /*$('#chkbox').click(function(){
          //  if($('#chkbox')=='on')
            {
            //alert('hello');
          //   if($('#chkbox').checked == true){
                 document.getElementById('desworkdiv').style.display = "block";
            // }
           // }else{
           //    document.getElementById('desworkdiv').style.display = "none";
            }

        });*/
        function check() {
            var c =document.getElementById("chkbox").checked;
            var d=  document.getElementById('desworkdiv');
            if (c == true){

                d.style.display = "block";
            } else {
                d.style.display = "none";
            }



        }
    </script>
@endsection
<style>
    .switch {
        position: relative;
        display: inline-block;
        width: 60px;
        height: 34px;
    }

    .switch input {
        opacity: 0;
        width: 0;
        height: 0;
    }

    .slider {
        position: absolute;
        cursor: pointer;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        background-color: #ccc;
        -webkit-transition: .4s;
        transition: .4s;
    }

    .slider:before {
        position: absolute;
        content: "";
        height: 26px;
        width: 26px;
        left: 4px;
        bottom: 4px;
        background-color: white;
        -webkit-transition: .4s;
        transition: .4s;
    }

    input:checked + .slider {
        background-color: #2196F3;
    }

    input:focus + .slider {
        box-shadow: 0 0 1px #2196F3;
    }

    input:checked + .slider:before {
        -webkit-transform: translateX(26px);
        -ms-transform: translateX(26px);
        transform: translateX(26px);
    }

    /* Rounded sliders */
    .slider.round {
        border-radius: 34px;
    }

    .slider.round:before {
        border-radius: 50%;
    }
</style>

