<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from /template/demo/vertical-fixed/pages/samples/login-2.html  [XR&CO'2014], Tue, 30 Oct 2018 06:39:30 GMT -->
<head>
  <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- plugins:css -->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}" />
    <title>Admin Panel</title>
    <!-- Custom CSS -->

    <link rel="stylesheet" href="{{asset('assets/css/icons.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendors/css/vendor.bundle.base.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendors/css/vendor.bundle.addons.css')}}">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{asset('assets/css/vertical-layout-light/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/themefix.css')}}">
    <style>
        .auth .login-half-bg{background:url("{{asset('assets/images/auth/login-bg.jpg')}}");background-size:cover}
    </style>
</head>

<body class="sidebar-fixed" style="background-color:#222222">
  <div class="container-scroller">
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-stretch auth auth-img-bg">
        <div class="row flex-grow">
          <div class="col-lg-6 d-flex align-items-center justify-content-center">
            <div class="auth-form-transparent text-left p-3">
              <div class="brand-logo">
                <img src="{{url('resources/assets/images/ifitmashlogo.png')}}" alt="profile" />
              </div>
              <h5 style="color:#dc3545; border: 1px">{{ session('status') }}</h5>
                <form role="form" method="POST" action="{{ route('login') }}" aria-label="{{ __('Login') }}" class = "pt-3">
                    @csrf
                <div class="form-group">
                  <label for="exampleInputEmail">E-Mail</label>
                  <div class="input-group">
                    <div class="input-group-prepend bg-transparent">
                      <span class="input-group-text bg-transparent border-right-0">
                        <i class="mdi mdi-account-outline text-primary"></i>
                      </span>
                    </div>
                    <input type="text" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-control-lg border-left-0" id="exampleInputEmail" name="email" type="email" autofocus value="{{ old('email') }}">
                      @if ($errors->has('email'))
                          <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                      @endif
                  </div>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword">Password</label>
                  <div class="input-group">
                    <div class="input-group-prepend bg-transparent">
                      <span class="input-group-text bg-transparent border-right-0">
                        <i class="mdi mdi-lock-outline text-primary"></i>
                      </span>
                    </div>
                    <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} form-control-lg border-left-0" id="exampleInputPassword" placeholder="Password" name="password" type="password">
                      @if ($errors->has('password'))
                          <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                      @endif
                  </div>
                </div>
                <div class="my-2 d-flex justify-content-between align-items-center">
                  <div class="form-check">
                    <label class="form-check-label text-muted">
                        <input name="remember" type="checkbox" value="Remember Me" id="remember" {{ old('remember') ? 'checked' : '' }}>Remember Me

                    </label>
                  </div>
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        {{ __('Forgot Your Password?') }}
                    </a>
                </div>
                <div class="my-3">
                  {{--<a class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" href="../../index.html">LOGIN</a>--}}
                    <button type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn">
                        {{ __('Login') }}
                    </button>
                </div>
                    <div class="row">
                      <div class="col-md-3">
                  <div class="text-center mt-4 font-weight-light">
                    <a href="{{url("/signup")}}" >Sign Up</a>
                  </div>
                      </div><br><br>
                      <div class="col-md-3">
                  <div class="text-center mt-4 font-weight-light">
                    <a href="{{url("/member")}}" >Member Login</a>
                  </div><br><br>
                      </div>
                      <div class="col-md-3">
                  <div class="text-center mt-4 font-weight-light">
                    <a href="{{url("/outlet")}}" >Outlet Login</a>
                  </div>
                      </div>
                    </div>
                {{--<div class="text-center mt-4 font-weight-light">
                  Don't have an account? <a href="register-2.html" class="text-primary">Create</a>
                </div>--}}
              </form>
            </div>
          </div>
          <div class="col-lg-6 login-half-bg d-flex flex-row">
            <p class="text-white font-weight-medium text-center flex-grow align-self-end">Copyright &copy; 2018  All rights reserved.</p>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- plugins:js -->
  <script src="{{asset('assets/vendors/js/vendor.bundle.base.js')}}"></script>
  <script src="{{asset('assets/vendors/js/vendor.bundle.addons.js')}}"></script>
  <!-- endinject -->
  <!-- Plugin js for this page-->
  <!-- End plugin js for this page-->
  <!-- inject:js -->
  <script src="{{asset('assets/js/off-canvas.js')}}"></script>
  <script src="{{asset('assets/js/hoverable-collapse.js')}}"></script>
  <script src="{{asset('assets/js/template.js')}}"></script>
  <script src="{{asset('assets/js/settings.js')}}"></script>
  <script src="{{asset('assets/js/todolist.js')}}"></script>
  <!-- endinject -->
  <!-- Custom js for this page-->
  <script src="{{asset('assets/js/dashboard.js')}}"></script>
  <!-- endinject -->
</body>


<!-- Mirrored from /template/demo/vertical-fixed/pages/samples/login-2.html  [XR&CO'2014], Tue, 30 Oct 2018 06:39:30 GMT -->
</html>
