


<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link" href="{{url('outlet/dashboard')}}">
                <i class="mdi mdi-view-quilt menu-icon"></i>
                <span class="menu-title">Dashboard</span>
            </a>
        </li>
     <!--   <li class="nav-item">
            <a class="nav-link" href="pages/widgets/widgets.html">
                <i class="mdi mdi-airplay menu-icon"></i>
                <span class="menu-title">Widgets</span>
            </a>
        </li>-->
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                <i class="mdi mdi-palette menu-icon"></i>
                <span class="menu-title">Master Data</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="ui-basic">
                <ul class="nav flex-column sub-menu">

                    <li class="nav-item"> <a class="nav-link" href="{{url('outlet/salesstaff')}}">Staff</a></li>
                   <!-- <li class="nav-item"> <a class="nav-link" href="{{url('member/source')}}">Source</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{url('member/facilities')}}">Facilities Available</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{url('member/billingcycle')}}">Billing Cycle</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{url('member/bankname')}}">Bank Name</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{url('member/accounttype')}}">Account Type</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{url('member/paymentmode')}}">Payment Mode</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{url('member/tags')}}">Tags</a></li>-->

                </ul>
            </div>
        </li>
       <li class="nav-item">
            <a class="nav-link"  href="{{ url('outlet/addnewlead')}}" aria-expanded="false" aria-controls="ui-basic">
                <i class="mdi mdi-layers menu-icon"></i>
                <span class="menu-title">Add New Lead</span>
              <!--  <i class="menu-arrow"></i>-->
           </a>
          <!--  </a>-->
           <!-- <div class="collapse" id="ui-advanced">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="pages/ui-features/dragula.html">Dragula</a></li>
                    <li class="nav-item"> <a class="nav-link" href="pages/ui-features/clipboard.html">Clipboard</a></li>
                    <li class="nav-item"> <a class="nav-link" href="pages/ui-features/context-menu.html">Context menu</a></li>
                    <li class="nav-item"> <a class="nav-link" href="pages/ui-features/slider.html">Sliders</a></li>
                    <li class="nav-item"> <a class="nav-link" href="pages/ui-features/carousel.html">Carousel</a></li>
                    <li class="nav-item"> <a class="nav-link" href="pages/ui-features/colcade.html">Colcade</a></li>
                    <li class="nav-item"> <a class="nav-link" href="pages/ui-features/loaders.html">Loaders</a></li>
                </ul>
            </div>-->
        </li>
       <li class="nav-item">
            <a class="nav-link"  href="{{ url('outlet/addnewmember')}}" aria-expanded="false" aria-controls="ui-advanced">
                <i class="mdi mdi-layers menu-icon"></i>
                <span class="menu-title">Add New Member</span>
              <!--<i class="menu-arrow"></i>-->
        </a>
          <!-- <div class="collapse" id="form-elements">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"><a class="nav-link" href="pages/forms/basic_elements.html">Basic Elements</a></li>
                    <li class="nav-item"><a class="nav-link" href="pages/forms/advanced_elements.html">Advanced Elements</a></li>
                    <li class="nav-item"><a class="nav-link" href="pages/forms/validation.html">Validation</a></li>
                    <li class="nav-item"><a class="nav-link" href="pages/forms/wizard.html">Wizard</a></li>
                </ul>
            </div>-->
       </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#editors" aria-expanded="false" aria-controls="editors">
                <i class="mdi mdi-pencil-box-outline menu-icon"></i>
                <span class="menu-title">Leads</span>
                <i class="menu-arrow"></i>
            </a>
           <div class="collapse" id="editors">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"><a class="nav-link" href="{{url('outlet/followupleads')}}">All followup Leads</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{url('outlet/activeleads')}}">All Active Leads</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{url('outlet/archivedleads')}}">All Archive Leads</a></li>
                </ul>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#trans" aria-expanded="false" aria-controls="editors">
                <i class="mdi mdi-layers menu-icon"></i>
                <span class="menu-title">Transactions</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="trans">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"><a class="nav-link" href="{{url('outlet/generateinvoice')}}">Generate Invoice</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{url('outlet/invoicelist')}}">Invoice List</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{url('outlet/paymentcollection')}}">Payment Collection</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{url('outlet/staffcash')}}">Staff Cash Register</a></li>
                </ul>
            </div>
        </li>
       <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#charts" aria-expanded="false" aria-controls="charts">
                <i class="mdi mdi-bell menu-icon"></i>
                <span class="menu-title">Setup</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="charts">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="{{url('outlet/packagedetail')}}">Pacakge</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{url('outlet/tax')}}">Tax</a></li>
                    <li class="nav-item"> <a class="nav-link" href="{{url('outlet/leadstatus')}}">Lead Status</a></li>
                </ul>
            </div>
        </li>


        <?php  
        $data = \App\Http\Helpers\Helpers::getOutletDetail(Auth::guard('outlet')->id());
            $ven=\App\Vendor::where('id',$data->vendor_id)->first();
            $businesstype=\App\Businesstype::where('id',$ven->businesstype_id)->first();
        ?>

        @if(isset($businesstype) && $businesstype)
               <?php $explodeBus = explode(',',$businesstype->choices);?>
               @foreach($explodeBus as $key => $explodeB)
                   <li class="nav-item">
                       <a class="nav-link" href="{{url('outlet/'.$explodeB)}}">
                            <i class="mdi mdi-comment-alert menu-icon"></i>
                            <span class="menu-title" >
                                {{ucfirst($explodeB)}}
                            </span>
                        </a>
                   </li>
               @endforeach
        @endif

       <li class="nav-item">
            <a class="nav-link" href="{{url('outlet/sms')}}">
                <i class="mdi mdi-bell menu-icon"></i>
                <span class="menu-title">SMS</span>
            </a>
        </li>
       <!-- <li class="nav-item">
            <a class="nav-link" data-toggle="collapse" href="#icons" aria-expanded="false" aria-controls="icons">
                <i class="mdi mdi-emoticon menu-icon"></i>
                <span class="menu-title">Icons</span>
                <i class="menu-arrow"></i>
            </a>
            <div class="collapse" id="icons">
                <ul class="nav flex-column sub-menu">
                    <li class="nav-item"> <a class="nav-link" href="pages/icons/flag-icons.html">Flag icons</a></li>
                    <li class="nav-item"> <a class="nav-link" href="pages/icons/font-awesome.html">Font Awesome</a></li>
                    <li class="nav-item"> <a class="nav-link" href="pages/icons/simple-line-icon.html">Simple line icons</a></li>
                    <li class="nav-item"> <a class="nav-link" href="pages/icons/themify.html">Themify icons</a></li>
                </ul>
            </div>
        </li>-->

    </ul>
</nav>
