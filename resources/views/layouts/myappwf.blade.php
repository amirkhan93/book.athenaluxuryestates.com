<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from /template/demo/vertical-fixed/index.html  [XR&CO'2014], Tue, 30 Oct 2018 06:36:24 GMT -->
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- plugins:css -->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}" />
    <title>ifitmash Panel</title>
    <!-- Custom CSS -->

    <link rel="stylesheet" href="{{asset('assets/css/icons.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendors/css/vendor.bundle.base.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendors/css/vendor.bundle.addons.css')}}">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{asset('assets/css/vertical-layout-light/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/themefix.css')}}">
</head>

<body class="sidebar-fixed">
<div class="container-scroller">
            <!--navbar-->
            @include('layouts.navbarwf')
            <!-- /static-sidebar -->
			@include('layouts.sidebarwf')

         <!-- /#page-wrapper -->
         <div class="main-panel">
		 @yield('content')
         @include('layouts.footer')
		 </div>


</div>
<!-- page-body-wrapper ends -->
</div>
<!-- container-scroller -->

<!-- plugins:js -->
<script src="{{asset('assets/vendors/js/vendor.bundle.base.js')}}"></script>
<script src="{{asset('assets/vendors/js/vendor.bundle.addons.js')}}"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="{{asset('assets/js/off-canvas.js')}}"></script>
<script src="{{asset('assets/js/hoverable-collapse.js')}}"></script>
<script src="{{asset('assets/js/template.js')}}"></script>
<script src="{{asset('assets/js/settings.js')}}"></script>
<script src="{{asset('assets/js/todolist.js')}}"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="{{asset('assets/js/dashboard.js')}}"></script>
<script src="{{asset('assets/js/data-table.js')}}"></script>
<script src="{{asset('assets/js/file-upload.js')}}"></script>
<script src="{{asset('assets/js/iCheck.js')}}"></script>
<script src="{{asset('assets/js/typeahead.js')}}"></script>
<script src="{{asset('assets/js/select2.js')}}"></script>
<!-- End custom js for this page-->
@yield('js');
</body>


<!-- Mirrored from /template/demo/vertical-fixed/index.html  [XR&CO'2014], Tue, 30 Oct 2018 06:37:00 GMT -->
</html>
