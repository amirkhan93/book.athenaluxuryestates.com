<!DOCTYPE html>
<html lang="en">
<head>
	<title>Home</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/style.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/responsive.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('resources/assets/css/owl-carousel.css')}}">
	<link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
	<div class="main-wrapper">
		<div class="banner-wrapeer" style="background: url('<?php echo asset('resources/assets/images/home-banner.png'); ?>')">
                                <div class="con-wrap">
                                    <header>
                                        <div class="header-wrap">
                                            <div class="clearfix">
                                                <div class="header-logo">
                                                    <img src="{{asset('resources/assets/images/logo.png')}}" alt="logo" />
							</div>
							<div class="header-menu">
								<ul>
									<li><a href="#">Sing In</a></li>
									<li><a href="#">DEX</a></li>
									<li><a href="#">Contact Us</a></li>
									<li class="dropdon-nav"><a href="#">USD</a>

									</li>
									<li class="custom-button register-btn"><a href="#">REGISTER</a></li>
								</ul>
							</div>
						</div>
					</div>
				</header>
				<div class="banner-con">
					<div class="banner-logo">
						<img src="{{asset('resources/assets/images/home-banner-logo.png')}}" alt="banner logo" />
					</div>
					<div class="banner-con-title">
						<p><strong>170K+ Hotels</strong> to choose from  <strong>185+ Cities</strong> around the world <strong>Millions</strong> of memories  <strong>One</strong> borderless currency</p>
					</div>
					<div class="search-sec" style="background: #fff; height: 250px; margin-top: 130px;">
						<div class="top-search-con">
		<div class="tab_container">
			<input class="input-tab" id="tab1" type="radio" name="tabs" checked>
			<label class="tab-lable" for="tab1"><i class="fa fa-building-o" aria-hidden="true"></i><span>hotels/accommodation</span></label>

			<input class="input-tab"  id="tab2" type="radio" name="tabs">
			<label class="tab-lable"  for="tab2"><i class="fa fa-plane" aria-hidden="true"></i><span>Flights</span></label>

			<input class="input-tab"  id="tab3" type="radio" name="tabs">
			<label class="tab-lable"  for="tab3"><i class="fa fa-car" aria-hidden="true"></i></i><span>car rentals</span></label>

			<input class="input-tab"  id="tab4" type="radio" name="tabs">
			<label class="tab-lable"  for="tab4"><i class="fa fa-briefcase" aria-hidden="true"></i><span>leisure tours</span></label>

			<input class="input-tab"  id="tab5" type="radio" name="tabs">
			<label class="tab-lable"  for="tab5"><i class="fa fa-envelope-o"></i><span>tour guides</span></label>

			<section class="tab-section" id="content1" class="tab-content">
				
				<div class="row clearfix">
								<div class="col-m-4">
									<div class="form-group">
										<label>LOOK & BOOK</label>
										<input type="text" name="address" class="custom-text custom-text2" placeholder="Enter a City, Location or Hotel" />
									</div>
								</div>
								<div class="col-m-2">
									<div class="form-group">
										<label>CHECK IN</label>
										<input type="text" name="checkin-date" class="custom-text custom-text2" placeholder="24-Nov-2018" />
									</div>
								</div>
								<div class="col-m-2">
									<div class="form-group">
										<label>CHECK OUT</label>
										<input type="text" name="checkout-date" class="custom-text custom-text2" placeholder="24-Nov-2018" />
									</div>
								</div>
								<div class="col-m-2">
									<div class="form-group">
										<label>ADULT/CHILD & ROOMS</label>
										<select class="custom-text custom-text2">
											<option>Pax 2 1 room</option>
											<option>Lorem Ipsum</option>
											<option>Lorem Ipsum</option>
											<option>Lorem Ipsum</option>
										</select>
									</div>
								</div>
								<div class="col-m-2">
									<div class="form-group">
										<label>NATIONALITY</label>
										<input type="text" name="nationality" class="custom-text custom-text2" placeholder="America" />
									</div>
								</div>
								<div class="col-m-4 top-search">
									<button>see more</button>
									
								</div>
							</div>

			</section>

			<section class="tab-section" id="content2" class="tab-content">
				<!-- <h3>Headline 2</h3> -->

			</section>

			<section class="tab-section" id="content3" class="tab-content">
				<!-- <h3>Headline 3</h3> -->
	
			</section>

			<section class="tab-section" id="content4" class="tab-content">
				<!-- <h3>Headline 4</h3> -->
			</section>

			<section class="tab-section" id="content5" class="tab-content">
				<!-- <h3>Headline 5</h3> -->

			</section>
		</div>


						</div>
					</div>
				</div>
			</div>
		</div>
			<section class="about-sec">
				<div class="row clearfix">
					<div class="about-left-sec">
						<h2 class="heading1">Introducing Hotelier Mart </h2>
						<div class="about-con">
							<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae.</p>
 							<p><strong>ab illo inventore veritatis et quasi architecto</strong> beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni</p>
						</div>
						<div class="founder-details">
							<div class="founder-image">
								<img src="images/founder-image.png" alt="Rana Mukharji" />
							</div>
							<p>Rana Mukherji</p>
							<h3>Rana Mukherji - CEO & Founder</h3>
						</div>
					</div>
					<div class="about-right-sec">
						<div class="clearfix">
							<div class="video-bg-image">
								<img src="{{asset('resources/assets/images/video-bg.png')}}" alt="" />
							</div>
							<div class="about-video-box">
								<img src="{{asset('resources/assets/images/about-video.png')}}" alt="" />
							</div>
							<div class="about-video-con">
								<img src="{{asset('resources/assets/images/logo.png')}}" alt="" />
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="book-sec">
				<div class="con-wrap">
					<div class="clearfix row">
						<div class="book-sec-col">
							<div class="book-sec-in">
								<h3>Why Book With Us?</h3>
							</div>
						</div>
						<div class="book-sec-col">
							<div class="book-sec-in">
								<div class="book-sec-icon">
									<img src="images/like-icon.png" alt="" />
									<h4>Best Price Guarantee</h4>
									<p>You pay unbeatably low prices when you Book</p>
								</div>

							</div>
						</div>
						<div class="book-sec-col">
							<div class="book-sec-in">
								<div class="book-sec-icon step-sec">
									<img src="images/step-icon.png" alt="" />
									<h4>Best Price Guarantee</h4>
									<p>Only three steps: Search, Choose & Book</p>
								</div>
							</div>
						</div>
						<div class="book-sec-col">
							<div class="book-sec-in">
								<div class="book-sec-icon ts-sec">
									<img src="images/ts-icon.png" alt="" />
									<h4>Trust & Safety</h4>
									<p>Accurate pictures of the hotel you want</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="inspiration-sec con-wrap">
				<div class="row">
					<div class="inspiration-col">
						<h2 class="heading1">Inspiration for your next trip</h2>
						<div class="inspiration-slider-wrap">
							<div class="owl-carousel">
							    <div class="item" style="background: url('<?php echo asset('resources/assets/images/family-image.png'); ?>'); background-repeat: no-repeat; padding: 30px;   background-position: 100% 100%; background-size: cover;">			<div class="inspiration-slider-con">
							    		<h3>FAMILY</h3>
							    		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
							   		</div>
							    </div>
							    <div class="item" style="background: url('<?php echo asset('resources/assets/images/romance-image.png');?>'); background-repeat: no-repeat; padding: 30px;   background-position: 100% 100%; background-size: cover;">			<div class="inspiration-slider-con">
							    		<h3>ROMANCE</h3>
							    		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
							    	</div>
							    </div>
							    <div class="item" style="background: url('<?php echo asset('resources/assets/images/adventure-image.png');?>'); background-repeat: no-repeat; padding: 30px;   background-position: 100% 100%; background-size: cover;">			<div class="inspiration-slider-con">
							    		<h3>ADENTURE</h3>
							    		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
							    	</div>
							    </div>
							    <div class="item" style="background: url('<?php echo asset('resources/assets/images/family-image.png');?>'); background-repeat: no-repeat; padding: 30px;   background-position: 100% 100%; background-size: cover;">			<div class="inspiration-slider-con">
							    		<h3>FAMILY</h3>
							    		<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
							    	</div>
							    </div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="feature-sec con-wrap">
				<div class="row">
					<div class="inspiration-col">
						<h2 class="heading1">Featured destinations</h2>
						<div class="featured-destination-image-wrap">
							<div class="feature-row clearfix">
								<div class="featire-col-6">
									<div class="featired-destination">
										<img src="{{asset('resources/assets/images/dubai.png')}}" alt="" />
										<div class="property-detail clearfix">
											<div class="property-country">
												<h3>DUBAI</h3>
												<p>properties <span>2,250</span></p>
											</div>
											<div class="property-price">
												<p>Average price</p>
												<p>US$366</p>
											</div>
										</div>
									</div>
								</div>
								<div class="featire-col-6">
									<div class="feature-row clearfix">
										<div class="featire-col-6">
											<div class="featired-destination">
												<img src="{{asset('resources/assets/images/egypt.png')}}" alt="" />
												<div class="property-detail clearfix">
													<div class="property-country">
														<h3>EGYPT</h3>
														<p>properties <span>2,250</span></p>
													</div>
													<div class="property-price">
														<p>Average price</p>
														<p>US$366</p>
													</div>
												</div>
											</div>
										</div>
										<div class="featire-col-6">
											<div class="featired-destination">
												<img src="{{asset('resources/assets/images/italy.png')}}" alt="" />
												<div class="property-detail clearfix">
													<div class="property-country">
														<h3>ITALY</h3>
														<p>properties <span>2,250</span></p>
													</div>
													<div class="property-price">
														<p>Average price</p>
														<p>US$366</p>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="feature-row clearfix">
								<div class="featire-col-3">
									<div class="featired-destination">
										<img src="{{asset('resources/assets/images/egland.png')}}" alt="" />
										<div class="property-detail clearfix">
											<div class="property-country">
												<h3>EGLAND</h3>
												<p>properties <span>2,250</span></p>
											</div>
											<div class="property-price">
												<p>Average price</p>
												<p>US$366</p>
											</div>
										</div>
									</div>
								</div>
								<div class="featire-col-3">
									<div class="featired-destination">
										<img src="{{asset('resources/assets/images/new-york.png')}}" alt="" />
										<div class="property-detail clearfix">
											<div class="property-country">
												<h3>NEW YOURK</h3>
												<p>properties <span>2,250</span></p>
											</div>
											<div class="property-price">
												<p>Average price</p>
												<p>US$366</p>
											</div>
										</div>
									</div>
								</div>
								<div class="featire-col-3">
									<div class="featired-destination">
										<img src="{{asset('resources/assets/images/india.png')}}" alt="" />
										<div class="property-detail clearfix">
											<div class="property-country">
												<h3>INDIA</h3>
												<p>properties <span>2,250</span></p>
											</div>
											<div class="property-price">
												<p>Average price</p>
												<p>US$366</p>
											</div>
										</div>
									</div>
								</div>
								<div class="featire-col-3">
									<div class="featired-destination">
										<img src="{{asset('resources/assets/images/france.png')}}" alt="" />
										<div class="property-detail clearfix">
											<div class="property-country">
												<h3>FRANCE</h3>
												<p>properties <span>2,250</span></p>
											</div>
											<div class="property-price">
												<p>Average price</p>
												<p>US$366</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="city-ambassadors-sec">
				<div class="con-wrap">
					<div class="row">
						<div class="inspiration-col">
							<h2 class="heading1">City ambassadors</h2>
							<div class="inspiration-slider-wrap">
								<div class="owl-carousel">
								    <div class="item" style="background: url('{{asset('resources/assets/images/1.png')}}); background-repeat: no-repeat; padding: 30px;   background-position: 100% 100%; background-size: cover;">			<div class="inspiration-slider-con">
								    		<h3>NEW DELHI</h3>
								    		<p>Sonali Tribhuvan</p>
								   		</div>
								    </div>
								    <div class="item" style="background: url('{{asset('resources/assets/images/2.png')}}'); background-repeat: no-repeat; padding: 30px;   background-position: 100% 100%; background-size: cover;">			<div class="inspiration-slider-con">
								    		<h3>HONG KONG</h3>
								    		<p>Chen Dalun 陳達侖</p>
								    	</div>
								    </div>
								    <div class="item" style="background: url('{{asset('resources/assets/images/3.png')}}'); background-repeat: no-repeat; padding: 30px;   background-position: 100% 100%; background-size: cover;">			<div class="inspiration-slider-con">
								    		<h3>AMSTERDAM</h3>
								    		<p>Emma Wilkinson</p>
								    	</div>
								    </div>
								    <div class="item" style="background: url('{{asset('resources/assets/images/2.png')}}'); background-repeat: no-repeat; padding: 30px;   background-position: 100% 100%; background-size: cover;">			<div class="inspiration-slider-con">
								    		<h3>HONG KONG</h3>
								    		<p>Chen Dalun 陳達侖</p>
								    	</div>
								    </div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</section>
			
				<div class="get-main">

			<section class="get-secret-deals"> 
				
				<div class="button-head">
				<h4>get secret deals</h4>
				<button>see more</button></div>
			
			</section>
			</div>
			<section class="top-deals-sec" style="background: url('{{asset('resources/assets/images/td-section-bg.png')}}');">
				<div class="con-wrap">
					<div class="row clearfix">
						<div class="td-left-box">
							<div class="today-top-deals">
								<h4>TODAY'S TOP DEALS</h4>
								<P>Save 40% or more on select stays</P>
								<div class="deals-time">
									<ul class="clearfix">
										<li>
											<span class="value">12</span>
											<span class="value-type">DAYS</span>
										</li>
										<li>
											<span class="value">00</span>
											<span class="value-type">HOURS</span>
										</li>
										<li>
											<span class="value">23</span>
											<span class="value-type">MINUTES</span>
										</li>
										<li>
											<span class="value">20</span>
											<span class="value-type">SECONDS</span>
										</li>
									</ul>
								</div>
								<div class="see-more-deals">
									<a href="#" class="custom-button">SEE MORE</a>
								</div>
							</div>
						</div>
						<div class="td-right-box">
							<div class="td-con">
								<h2>LIVE, TRAVEL, HAVE AN ADVENTURE AND DON’T BE SORRY.</h2>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="world-big-place-sec">
				<div class="con-wrap">
					<h2 class="heading1">The world is a big place</h2>
					<div class="big-place-box-wrap">
						<div class="row clearfix">
							<div class="big-place-box">
								<div class="big-place-box-in" style="background: url('{{asset('resources/assets/images/europe.png')}}');">
									<h4>EUROPE</h4>
									<P>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</P>
								</div>
								<div class="big-place-list-wrap">
									<ul class="clearfix">
										<li>London</li>
										<li>Barcelona</li>
										<li>Edinburgh</li>
										<li>Rome</li>
										<li>Dublin</li>
										<li>Madrid</li>
										<li>Paris</li>
										<li>Reykjavik</li>
										<li>Florence</li>
										<li>Amsterdam</li>
										<li>Venice</li>
										<li>Athens</li>
									</ul>
								</div>
							</div>
							<div class="big-place-box">
								<div class="big-place-box-in" style="background: url('{{asset('resources/assets/images/asia.png')}}');">
									<h4>ASIA</h4>
									<P>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</P>
								</div>
								<div class="big-place-list-wrap">
									<ul class="clearfix">
										<li>Bali</li>
										<li>Seoul</li>
										<li>Kyoto</li>
										<li>Tokyo</li>
										<li>Phuket</li>
										<li>Beijing</li>
										<li>Manila</li>
										<li>Shanghai</li>
										<li>New Delhi</li>
										<li>Bangkok</li>
										<li>Okinawa</li>
										<li>Cebu</li>
									</ul>
								</div>
							</div>
							<div class="big-place-box">
								<div class="big-place-box-in" style="background: url('{{asset('resources/assets/images/south-america.png')}}');">
									<h4>SOUTH AMERICA</h4>
									<P>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</P>
								</div>
								<div class="big-place-list-wrap">
									<ul class="clearfix">
										<li>Rio De Janerio</li>
										<li>Machu Picchu</li>
										<li>Santiago</li>
										<li>Medellin</li>
										<li>Lima</li>
										<li>Quito</li>
										<li>Easter Island</li>
										<li>Cartagena</li>
										<li>Ipanema Beach</li>
										<li>Bogota</li>
										<li>Buenos Aires</li>
										<li>Sao Paulo</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix view-more-btn">
						<button type="button" class="btn custom-button">VIEW MORE</button>
					</div>
				</div>
			</section>
			<section class="begin-journey-sec" style="background: url('{{asset('resources/assets/images/section8-bg.png')}}');">
				<div class="con-wrap">
					<div class="row clearfix">
						<div class="begin-journey-box">
							<div class="icon-wrap">
								<img src="{{asset('resources/assets/images/epic.png')}}" alt="" />
							</div>
							<p>EPIC JOURNEYS</p>
						</div>
						<div class="begin-journey-box">
							<div class="icon-wrap">
								<img src="{{asset('resources/assets/images/strategic.png')}}" alt="" />
							</div>
							<p>STRATEGIC PLANNING</p>
						</div>
						<div class="begin-journey-box">
							<div class="icon-wrap">
								<img src="{{asset('resources/assets/images/eco-ladet.png')}}" alt="" />
							</div>
							<p>ECO LADET & TOURS</p>
						</div>
						<div class="begin-journey-box">
							<div class="icon-wrap">
								<img src="{{asset('resources/assets/images/authentic.png')}}" alt="" />
							</div>
							<p>AUTHENTIC EXPERIENCES</p>
						</div>
					</div>
					<div class="begin-journey-con">
						<h3>BEGIN YOUR JOURNEY</h3>
						<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem</p>
					</div>
				</div>
			</section>
			<section class="subscribe-sec" style="background: url('{{asset('resources/assets/images/subscribe-bg-image.png')}}')" alt="" />
				<div class="subscribe-box-wrap">
					<h3>SUBSCRIBE TO GET SECRET DEALS</h3>
					<form class="subscribe-form clearfix">
						<input type="email" name="email" class="custom-text" placeholder="Email Address" />
						<button type="submit" name="submit" class="custom-button">subscribe</button>
					</form>
				</div>
			</section>
			<section class="still-decid-stay">
				<div class="con-wrap">
					<div class="row clearfix">
						<div class="inspiration-col still-decid-stay-in">
							<h2 class="heading1">Still deciding where to stay?</h2>
							<div class="still-decid-stay-title">
								<p>We want to make your decision easler! With over 168,520,000 verified guest revlews, we can help find perfect place to say.</p>
							</div>
							<div class="still-stay-list-wrap clearfix">
								<ul class="still-stay-list-box">
									<li>
										<p>Edinburgh</p>
										<p>283,435 hotel reviews</p>
									</li>
									<li>
										<p>Manchester</p>
										<p>194,347 hotel reviews</p>
									</li>
									<li>
										<p>Liverpool</p>
										<p>158,331 hotel reviews</p>
									</li>
									<li>
										<p>Glasgow</p>
										<p>151,122 hotel reviews</p>
									</li>
								</ul>
								<ul class="still-stay-list-box">
									<li>
										<p>Edinburgh</p>
										<p>283,435 hotel reviews</p>
									</li>
									<li>
										<p>Manchester</p>
										<p>194,347 hotel reviews</p>
									</li>
									<li>
										<p>Liverpool</p>
										<p>158,331 hotel reviews</p>
									</li>
									<li>
										<p>Glasgow</p>
										<p>151,122 hotel reviews</p>
									</li>
								</ul>
								<ul class="still-stay-list-box">
									<li>
										<p>Edinburgh</p>
										<p>283,435 hotel reviews</p>
									</li>
									<li>
										<p>Manchester</p>
										<p>194,347 hotel reviews</p>
									</li>
									<li>
										<p>Liverpool</p>
										<p>158,331 hotel reviews</p>
									</li>
									<li>
										<p>Glasgow</p>
										<p>151,122 hotel reviews</p>
									</li>
								</ul>
								<ul class="still-stay-list-box">
									<li>
										<p>Edinburgh</p>
										<p>283,435 hotel reviews</p>
									</li>
									<li>
										<p>Manchester</p>
										<p>194,347 hotel reviews</p>
									</li>
									<li>
										<p>Liverpool</p>
										<p>158,331 hotel reviews</p>
									</li>
									<li>
										<p>Glasgow</p>
										<p>151,122 hotel reviews</p>
									</li>
								</ul>
								<ul class="still-stay-list-box">
									<li>
										<p>Edinburgh</p>
										<p>283,435 hotel reviews</p>
									</li>
									<li>
										<p>Manchester</p>
										<p>194,347 hotel reviews</p>
									</li>
									<li>
										<p>Liverpool</p>
										<p>158,331 hotel reviews</p>
									</li>
									<li>
										<p>Glasgow</p>
										<p>151,122 hotel reviews</p>
									</li>
								</ul>
								<ul class="still-stay-list-box">
									<li>
										<p>Edinburgh</p>
										<p>283,435 hotel reviews</p>
									</li>
									<li>
										<p>Manchester</p>
										<p>194,347 hotel reviews</p>
									</li>
									<li>
										<p>Liverpool</p>
										<p>158,331 hotel reviews</p>
									</li>
									<li>
										<p>Glasgow</p>
										<p>151,122 hotel reviews</p>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</section>
			<footer class="footer-wrappers">
				<div class="footer-in">
					<div class="con-wrap">
						<div class="row clearfix">
							<div class="footer-col">
								<div class="footer-box">
									<div class="footer-logo">
										<img src="{{asset('resources/assets/images/logo.png')}}" alt=""/>
									</div>
									<div class="footer-address-box">
										<p>Global Hotelier Pte. Ltd.</p>
										<p>28C Stanley Street, Singapore 068737</p>
									</div>
									<div class="footer-social-sec">	
									<ul>
										<li><i class="fa fa-linkedin" aria-hidden="true"></i></li>
										<li><i class="fa fa-facebook" aria-hidden="true"></i></li>
										<li><i class="fa fa-location-arrow" aria-hidden="true"></i></li>
										<li><i class="fa fa-youtube-play" aria-hidden="true"></i></li>
										<li><i class="fa fa-twitter" aria-hidden="true"></i></li>
									</ul>								
									</div>
								</div>
							</div>
							<div class="footer-col">
								<div class="footer-logo2">
									<img src="{{asset('resources/assets/images/footer-logo.png')}}" alt="" />
								</div>
							</div>
							<div class="footer-col">
								<div class="footer-box footer-menu">
									<ul>
										<li><a href="#">About Us</a></li>
										<li><a href="#">Contact Us</a></li>
										<li><a href="#">Privacy & policy</a></li>
										<li><a href="#">Terms & Conditions</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="copyright-sec">
					<p>Copyright 2018 Hotelier Coin PTE.LTD. Singapore. All Right Reserved</p>
				</div>
			</footer>
	</div>
	<script type="text/javascript" src="{{asset('resources/assets/js/jquery.js')}}"></script>
	<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
	<script type="text/javascript" src="{{asset('resources/assets/js/owl-carousel.js')}}"></script>

	<script type="text/javascript">
		$(document).ready(function(){
			$('.owl-carousel').owlCarousel({
				  loop: true,
				  margin: 30,
				  nav: true,
				  // navText: [
				  //   "<i class='fa fa-caret-left'></i>",
				  //   "<i class='fa fa-caret-right'></i>"
				  // ],
				  autoplay: true,
				  responsive: {
				    0: {
				      items: 1
				    },
				    600: {
				      items: 2
				    },
				    1000: {
				      items: 3
				    }
				  }
			});
		});
	</script>
</body>
</html>
