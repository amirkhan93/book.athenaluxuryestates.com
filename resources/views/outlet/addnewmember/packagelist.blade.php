@extends('layouts.myappoutlet')

@section('content')
    <div class="content-wrapper">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Package List</h4>

                    <form class="form-sample" action="{{url('outlet/packagestore',$acs->id)}}" method = "post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <p class="card-description">
                         Selected Package
                        </p>

                        <?php

                        // print_r('<pre>');
                        // print_r($user_data->is_gst);
                        // die;    

                        if($baseprice=$users->price){
                            // echo $baseprice;die;
                            if($user_data->is_gst == '1'){
                                $gst=18/100*$baseprice;
                            }
                            else{
                                $gst = 0;
                            }
                          
                            $totalbalance=$baseprice+$gst;
                            // echo $totalbalance;die;
                        }                 ?>
                        {{$users->name}}
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-9">
                                            Start Date: {{$users->created_at}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-9">
                                            End Date: {{$users->updated_at}}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-9">
                                            Base Price :<input type="text"   name="baseprice" value="{{$users->price}}">
                                        </div>
                                    </div>
                                </div>
                                <?php if($gst != 0){  ?>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-9">
                                            Tax 1(18%gst):<input type="text"   name="tax" value="<?php  echo $gst;?>" >
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-9">
                                            Total Amt:<input type="text"  name="totalamount" value="<?php  echo $totalbalance;?>" >
                                        </div>
                                    </div>
                                </div>
                                <?php if($gst != 0){  ?>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-9">
                                            Tax Type:{{$users->taxtype}}
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-9">
                                            DiscountType:<input type="text"id="discount" name="discount" onchange="myFunction()" value=""> <br><br>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-9">
                                            <p>discount <input type="text" id="demo" ></p>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-sm-9">
                                        <input type="submit" value="Next" class="form-control btn btn-primary" />
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                
                                <div class="form-group row">
                                    <div class="col-sm-9">
                                        <a  class="form-control btn btn-primary"  href="{{url('outlet/generateInvoice/'.$uniq_id)}}" >Generate Invoice</a>
                                    </div>
                                </div>
                            
                            </div>


                        </div>


                    </form>
                </div>



            </div>
        </div>

    </div>
@endsection
<script>
    function myFunction() {
        var discounts = document.getElementById("discount");

        document.getElementById("demo").value = discounts.text;
    }
</script>
<style>
    input {
        background-color: transparent;
        border: 0px solid;
        height: 20px;
        width: 160px;
        color: #CCC;
    }
</style>