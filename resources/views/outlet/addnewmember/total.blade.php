@extends('layouts.myappoutlet')

@section('content')
    <div class="content-wrapper">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Payment </h4>

                    <form class="form-sample" action="{{url('outlet/paymentdetail/')}}" method = "post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <p class="card-description">

                        </p>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-8" style="background-color:darkslategrey;height:56px;width:513.3px;">
                                            <span class="iconLft"><img src="{{url('resources/assets/images/subtotal.png')}}" class="img-responsive" style="height:47px;"></span>
                                            <strong class="txtRt" style="text-align: right;padding-left:140px;">
                                                Total:
                                                <span ng-bind="InvoiceObject.TotalAmount" class="ng-binding"  style="color:white;">{{$taxdetails->totalamount}}</span>
                                            </strong>
                                        </label>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-8" style="background-color:darkslategrey;height:56px;width:513.3px;">
                                            <span class="iconLft"><img src="{{url('resources/assets/images/subtotal.png')}}" class="img-responsive" style="height:47px;"></span>
                                            <strong class="txtRt" style="text-align: right;padding-left:125px; text-color:#ffff;">
                                                Total Price:
                                                <span ng-bind="InvoiceObject.TotalAmount" class="ng-binding" style="color:white;">{{$taxdetails->baseprice}}</span>
                                            </strong>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-8" style="background-color:darkslategrey;height:56px;width:513.3px;">
                                            <span class="iconLft"><img src="{{url('resources/assets/images/subtotal.png')}}" class="img-responsive" style="height:47px;"></span>
                                            <strong class="txtRt" style="text-align: right;padding-left:140px;">
                                                Total Tax:
                                                <span ng-bind="InvoiceObject.TotalAmount" class="ng-binding"  style="color:white;">{{$taxdetails->tax}}</span>
                                            </strong>
                                        </label>

                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-md-8" style="background-color:darkslategrey;height:56px;width:513.3px;">
                                            <span class="iconLft"><img src="{{url('resources/assets/images/subtotal.png')}}" class="img-responsive" style="height:47px;"></span>
                                            <strong class="txtRt" style="text-align: right;padding-left:140px;">
                                                Balance:
                                                <span ng-bind="InvoiceObject.TotalAmount" class="ng-binding"  style="color:white;">{{$taxdetails->totalamount}}</span>
                                            </strong>
                                        </label>

                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <div class="col-sm-12">
                                            <select class="form-control"  name="payment"  id="payment">
                                                <option value="">Select</option>

                                                @if($ac)
                                                    @foreach($ac as $users)

                                                        <option value="{{$users->id}}">{{$users->name}}</option>
                                                    @endforeach
                                                @endif

                                            </select>
                                            @if($errors->has('payment'))
                                                <span class="text-danger">{{$errors->first('payment')}}</span>
                                            @endif

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Remark
                                        </label>
                                        <div class="col-md-12">
                                            <textarea class="form-control" id="note" name="note" rows="4"></textarea>

                                        </div>
                                    </div>
                                </div>
                            </div>


                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-sm-9">
                                        <input type="submit" value="Next" class="form-control btn btn-primary" />
                                    </div>
                                </div>
                            </div>
                        </div>


                    </form>
                </div>



            </div>
        </div>

    </div>
@endsection
