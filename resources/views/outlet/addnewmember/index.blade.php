@extends('layouts.myappoutlet')

@section('content')
    <div class="content-wrapper">
        <div >
            <a href="{{ url('outlet/addnewmember/create') }}" style="
    background: #1976d2;
    border: 1px solid #1976d2;
    text-decoration: none;
    font-size: 14px;
    padding: 2px 5px;
    border-radius: 4px;
    color: #fff;
    cursor: pointer;
">Add New</a>
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">ADD NEW Member</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Member Code</th>

                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Contact</th>
                                    <th>Note</th>

                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Remaining Days</th>
                                    <th>Refferal Code</th>
                                    <th>Total Earned Points</th>

                                    <th>Action</th>

                                </tr>
                                </thead>
                                <tbody>

                                <?php $i = 1; foreach($bn as $user) {  

                                  $created_at =  date("Y-m-d",(strtotime($user->created_at)));

                                  //$diff=date_diff(date_create($created_at),date_create($user->end_date));
                                ?>


                                <tr>
                                    <td>{{ $i }}</td>
                                    <td>{{$user->code}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->contact}}</td>
                                    <td>{{ $user->note}}</td>
                                    <td>{{ date("Y-m-d",(strtotime($user->created_at))) }}</td>
                                    <td>{{ date("Y-m-d",(strtotime($user->pack_ending_date)))  }}</td>
                                    <td>{{ $user->remaining_days  }}</td>
                                    <td>{{ $user->refferal_code }}</td>
                                    <td>{{ $user->total_earned_points }}</td>
                                    <td>@if($user->status != 1)
                                            <a href="{{url('outlet/addnewmember/active/'.$user->id)}}"
                                               onclick="if(! confirm('Click Ok To Active ')){ return false}"
                                               class="btn btn  btn-danger" data-toggle="tooltip" >In-Active</a>
                                        @else
                                            <a href="{{url('outlet/addnewmember/inactive/'.$user->id)}}"
                                               onclick="if(! confirm('Click Ok To InActivate')){ return false}"
                                               class="btn btn-success " data-toggle="tooltip" >Active</a>
                                        @endif
                                        <a onclick="showAssign({{$user->id}})" href="#" class="btn btn-primary">Workout</a>
                                        
                                        <a onclick="showAssignNutrition({{$user->id}})" href="#" class="btn btn-primary">Nutrition </a>
                                        
                                        <div class="row">
                                             <button  class="btn btn-outline-primary"><a href="{{ url('outlet/addnewmember/edit',[$user->id]) }}">Edit</a></button>
                                        
                                        <a  class="btn btn-danger" 
                                            onclick="return confirm('Are you sure?');" 
                                            href="{{ url('outlet/addnewmember/delete',[$user->id]) }}">
                                        Delete</a>

                                        <a class="btn btn-danger" href="{{ url('outlet/addnewmember/detail/'.$user->id) }}">Detail</a>
                                        </div>
                                    </td>
                                </tr>
                                <?php $i++;}?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade custom-popup" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Assign Workout</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="content-wrapper" id="exampleData">


                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade custom-popup" id="exampleModalNutrition" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Assign Nutrition</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="content-wrapper" id="exampleDataNutri">


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
<script>
    function showAssign(id){
        $.ajax({
            type : 'POST',
            url : '{{url('outlet/getassignWorkOut')}}',
            data : {memId : id,_token:'{{csrf_token()}}'},
            success : function(msg) {
                $('#exampleModal').modal('show');
                $('#exampleData').html(msg);
                $('#memId').val(id);
            }
        });
    }

    function showAssignNutrition(id){
        $.ajax({
            type : 'POST',
            url : '{{url('outlet/getassignNutrition')}}',
            data : {memId : id,_token:'{{csrf_token()}}'},
            success : function(msg) {
                $('#exampleModalNutrition').modal('show');
                $('#exampleDataNutri').html(msg);
                $('#memId').val(id);
            }
        });
    }

    function assignWorkOut(workId){
        var memId = $('#memId').val();
        $.ajax({
            type : 'POST',
            url : '{{url('outlet/assignWorkOut')}}',
            data : {memId : memId, workId:workId,_token:'{{csrf_token()}}'},
            success : function(msg){
                if(msg == 1){
                    alert('Successfully assign');
                    $('#exampleModal').modal('hide');
                    $('#btn_'+workId).html('<a href="#" class="btn btn-success">Assigned</a>');
                }else{
                    alert('Error in assign');
                }
            }
        });
    }

    function assignNutrition(workId){
        var memId = $('#memId').val();
        $.ajax({
            type : 'POST',
            url : '{{url('outlet/assignNutrition')}}',
            data : {memId : memId, workId:workId,_token:'{{csrf_token()}}'},
            success : function(msg){
                if(msg == 1){
                    alert('Successfully assign');
                    $('#exampleModalNutrition').modal('hide');
                    $('#btn_'+workId).html('<a href="#" class="btn btn-success">Assigned</a>');
                }else{
                    alert('Error in assign');
                }
            }
        });
    }
</script>