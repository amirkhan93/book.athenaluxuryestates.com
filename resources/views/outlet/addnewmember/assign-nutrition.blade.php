<input type="hidden" id="memId" name="memId" value="{{$memId}}">
<div class="col-12 grid-margin">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Assign Nutrition for Member</h4>
            <table id="order-listing" class="table">
                <thead>
                <tr>

                    <th>Meal Type</th>
                    <th>Meal Time</th>
                    <th>Assign</th>
                </tr>
                </thead>
                <tbody>
                <?php $i = 1; foreach($bst as $users) {
                $getAssign = \App\NutritionMemberMapping::where('member_id', $memId)->where('nutrition_id', $users->id)->first();
                ?>
                <tr>
                    <td>{{ $users->meal_type }}</td>
                    <td>{{ $users->meal_time }}</td>
                    <td id="btn_{{$users->id}}">
                        <?php if($getAssign){?>
                        <a  href="#" class="btn btn-success">Assigned</a>
                        <?php }else{?>
                        <a  href="#" onclick="assignNutrition({{$users->id}})" class="btn btn-primary">Assign</a>
                        <?php }?>
                    </td>
                </tr>
                <?php $i++;}?>
                </tbody>
            </table>
        </div>
    </div>
</div>