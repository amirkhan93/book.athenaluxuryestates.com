<input type="hidden" id="memId" name="memId" value="{{$memId}}">
<div class="col-12 grid-margin">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Assign workout for Member</h4>
            <table id="order-listing" class="table">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Settype</th>
                    <th>BodyPart</th>
                    <th>ExcerciseType</th>
                    <th>Set</th>
                    <th>View</th>
                    <th>Assign</th>

                </tr>
                </thead>
                <tbody>
                <?php $i = 1; foreach($bst as $users) {
                    $getAssign = \App\WorkoutMapping::where('member_id', $memId)->where('workout_id', $users->id)->first();
                    ?>
                <?php
                $list=$users->member_id;
                $explo = explode(',',$list);
                // print_r($explo);die;
                $member=\App\Addnewmember::whereIn('id', $explo)->get();
                $memName = array();
                if(!empty($member)){
                    foreach($member as $mem){
                        $memName[] = $mem->name;
                    }
                }
                $memNa = implode(',',$memName);

                $settype=\App\Settype::where('id',$users->settype_id)->first();
                $body=\App\Subtype::where('id',$users->bodypart_id)->first();
                $excercise=\App\Excercise::where('id',$users->excercise_id)->first();

                ?>
                <tr>

                    <td>{{ $i }}</td>
                    <td>{{$settype->name}}</td>
                    <td>{{ $body->name}}</td>
                    <td>{{$excercise->name}}</td>
                    <td>
                        {{$users->excercise_id}}
                    </td>
                    <td>
                        <a  href="{{url('outlet/view-workout-detail/'.$users->id)}}" target="_blank" class="btn btn-primary">View</a>
                    </td>
                    <td id="btn_{{$users->id}}">
                        <?php if($getAssign){?>
                            <a  href="#" class="btn btn-success">Assigned</a>
                        <?php }else{?>
                            <a  href="#" onclick="assignWorkOut({{$users->id}})" class="btn btn-primary">Assign</a>
                        <?php }?>
                    </td>
                </tr>
                <?php $i++;}?>
                </tbody>
            </table>
        </div>
    </div>
</div>