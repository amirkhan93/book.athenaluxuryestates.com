@extends('layouts.myappoutlet')

@section('content')
    <div class="content-wrapper">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Sms</h4>
                    <form class="form-sample" action="{{url('outlet/sendsms')}}" method="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <p class="card-description">

                        </p>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label ">Select Lead/Member <sup>*</sup> </label>
                                    <div class="col-sm-9">
                                        <select class="form-control"  name="choice"  id="choice" onchange="check(this.value)">
                                            <option value="">Select</option>
                                            <option value="1">Lead</option>
                                            <option value="2">Member</option>
                                            @if($errors->has('choice'))
                                                <span class="text-danger">{{$errors->first('choice')}}</span>
                                            @endif

                                        </select>

                                    </div>
                                </div>
                            </div>
                        <div id="deswork" class="col-md-6">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Select Leads<sup>*</sup>  </label>
                                    <div class="col-sm-9">
                                        <select  class="js-example-basic-multiple "  name="lead_id[]"  id="lead_id"  multiple="multiple" style="width:100%;" >
                                            <option value="">Select Leads</option>
                                            @if($lead)
                                                @foreach($lead as $users)

                                                    <option value="{{$users->contact}}">{{$users->name}}/{{$users->code}}</option>
                                                @endforeach
                                            @endif
                                            @if($errors->has('lead_id'))
                                                <span class="text-danger">{{$errors->first('lead_id')}}</span>
                                            @endif

                                            {{--<option value="1">Gaint</option>
                                            <option value="2">Trisect</option>
                                            <option value="3">Superset</option>
                                            <option value="4">Mono</option>--}}
                                        </select>

                                    </div>
                                </div>
                            </div>
                        </div>
                            <div id="deswork1" class="col-md-6" style="display:none;">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Select Member<sup>*</sup>  </label>
                                        <div class="col-sm-9">
                                            <select  class="js-example-basic-multiple " multiple="multiple"  name="member_id[]"  id="member_id" style="width:100%;" >
                                                <option value="">Select Members</option>
                                                @if($member)
                                                    @foreach($member as $users)

                                                        <option value="{{$users->contact}}">{{$users->name}}/{{$users->code}}</option>
                                                    @endforeach
                                                @endif

                                                {{--<option value="1">Gaint</option>
                                                <option value="2">Trisect</option>
                                                <option value="3">Superset</option>
                                                <option value="4">Mono</option>--}}
                                            </select>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Text
                                    </label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" id="message" name="message" rows="4"></textarea>

                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-sm-9">
                                        <input type="submit" value="send" class="form-control btn btn-primary" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

    <script>
        function check(valu) {
            if(valu==1){
                $('#deswork').show();
                $('#deswork1').hide();
             //   $('#desworkdiv3').hide();
            }else if(valu==2){
                $('#deswork').hide();
                $('#deswork1').show();


            }
            else {

            }
        }
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>




@endsection
