@extends('layouts.myappoutlet')

@section('content')
    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Tax</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>code</th>
                                    <th>Tax Name</th>
                                    <th>Tax Amount</th>
                                </tr>
                                </thead>

                                <tr>

                                    <td>1</td>
                                    <td>TAX001</td>
                                    <td>GST</td>
                                    <td>18%</td>


                                </tr>



                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
    </div>
@endsection
