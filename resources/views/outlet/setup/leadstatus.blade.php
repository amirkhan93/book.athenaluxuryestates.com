@extends('layouts.myappoutlet')

@section('content')
    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Lead Status</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>code</th>
                                    <th>Lead Status</th>
                                    <th>Lead Name</th>
                                    <th>Created On</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; foreach($bn as $user) {?>
                               <?php
                                 $bn=\App\Status::where('id',$user->status_id)->first();

                                ?>
                                <tr>

                                    <td>{{ $i }}</td>
                                    <td>{{$user->code}}</td>
                                    <td>{{ $bn->name}}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->created_at}}</td>


                                </tr>
                                <?php $i++;}?>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
