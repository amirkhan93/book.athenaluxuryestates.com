@extends('layouts.myappoutlet')

@section('content')
    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Roles</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>code</th>
                                    <th>Role Name</th>
                                    <th>Created On</th>
                                    <th>Created By</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; foreach($bn as $user) {?>
                               <?php
                                $bn=\App\Vendor::where('id',$user->vendor_id)->first();

                                ?>
                                <tr>

                                    <td>{{ $i }}</td>
                                    <td>{{$user->code}}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->created_at}}</td>
                                     <td>{{$bn->name}}</td>

                                </tr>
                                <?php $i++;}?>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
