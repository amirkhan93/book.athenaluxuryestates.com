@extends('layouts.myappoutlet')

@section('content')
    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Total Packages</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>code</th>
                                    <th>Package</th>
                                    <th>Price</th>
                                    <th>TaxType</th>
                                    <th> Validity</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; foreach($bn as $user) {?>
                                <?php
                                $start=  new DateTime($user->start_date);
                                $end =  new DateTime($user->end_date);
                                $difference = $start->diff($end);

                               // print_r($difference);die;

                                ?>
                                <tr>

                                    <td>{{ $i }}</td>
                                    <td>{{$user->code}}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->price }}</td>
                                    <td>{{ $user->taxtype}}</td>
                                    <td><?php  echo
                                            $difference->d.' days';?></td>

                                </tr>
                                <?php $i++;}?>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
