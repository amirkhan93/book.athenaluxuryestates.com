@extends('layouts.myappoutlet')

@section('content')
    <div class="content-wrapper">
        <div >
            <a  href="{{url('outlet/workout/create')}}" style="
    background: #1976d2;
    border: 1px solid #1976d2;
    text-decoration: none;
    font-size: 14px;
    padding: 2px 5px;
    border-radius: 4px;
    color: #fff;
    cursor: pointer;
">Add New</a>
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">WorkOut</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Settype</th>
                                    <th>BodyPart</th>
                                    <th>ExcerciseType</th>
                                    <th>Set</th>
                                    <th>Action</th>

                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; foreach($bst as $user) {?>
                                <?php
                                     $list=$user->member_id;
                                $explo = explode(',',$list);
                                    // print_r($explo);die;
                                $member=\App\Addnewmember::whereIn('id', $explo)->get();
                                $memName = array();
                                if(!empty($member)){
                                    foreach($member as $mem){
                                        $memName[] = $mem->name;
                                    }
                                }
                                $memNa = implode(',',$memName);

                                $settype=\App\Settype::where('id',$user->settype_id)->first();
                                $body=\App\Subtype::where('id',$user->bodypart_id)->first();
                                $excercise=\App\Excercise::where('id',$user->excercise_id)->first();

                                ?>
                                <tr>

                                    <td>{{ $i }}</td>
                                    <td>{{$settype->name}}</td>
                                    <td>@if($body){{ $body->name}} @endif</td>
                                    <td>{{$excercise->name}}</td>
                                    <td>
                                       {{$user->excercise_id}}
                                    </td>
                                    <td><a href="{{ url('outlet/workouts/delete/'.$user->id) }}" class="btn btn-danger" onclick="confirm('Are you sure want to delete')">Delete</a></td>
                                </tr>
                                <?php $i++;}?>
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Add Popup Start -->

        <!-- Add Popup Ends -->
    </div>
@endsection
