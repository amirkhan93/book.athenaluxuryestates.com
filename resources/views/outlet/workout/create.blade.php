@extends('layouts.myappoutlet')

@section('content')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/css/bootstrap-select.css">

    <div class="content-wrapper">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">WorkOut</h4>
                    <form class="form-sample" action="{{url('outlet/workout/store')}}" method ="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <p class="card-description">

                        </p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label ">Select Days<sup>*</sup> </label>
                                    <div class="col-sm-9">
                                        <select class="js-example-basic-multiple "  name="days[]"  id="member_id" multiple="multiple" style="width:100%;">
                                            <option value="">Select</option>
                                            <option value="1">Monday</option>
                                            <option value="2">Tuesday</option>
                                            <option value="3">Wednesday</option>
                                            <option value="4">Thursday</option>
                                            <option value="5">Friday</option>

                                            <option value="6">Saturday</option>
                                            <option value="7">Sunday</option>

                                        </select>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label ">Select Week<sup>*</sup> </label>
                                    <div class="col-sm-9">
                                        <select class="js-example-basic-multiple "  name="week"  id="week"  style="width:100%;">
                                            <option value="">Select</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                        </select>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                           <!--  <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label ">Select Member<sup>*</sup> </label>
                                    <div class="col-sm-9">
                                        <select class="js-example-basic-multiple "  name="member_id[]"  id="member_id" multiple="multiple" style="width:100%;">
                                            <option value="">Select</option>
                                            @if($ac)
                                                @foreach($ac as $users)

                                                    <option value="{{$users->id}}">{{$users->name}}{{$users->code}}</option>
                                                @endforeach
                                            @endif
                                            @if($errors->has('member_id'))
                                                <span class="text-danger">{{$errors->first('member_id')}}</span>
                                            @endif

                                        </select>

                                    </div>
                                </div>
                            </div> -->

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Select SetType<sup>*</sup>  </label>
                                    <div class="col-sm-9">
                                        <select class="form-control"  name="settype_id"  id="settype_id" onchange="check(this.value)">
                                            <option value="">Select Settype</option>
                                            @if($settype)
                                                @foreach($settype as $users)

                                                    <option value="{{$users->id}}">{{$users->name}}</option>
                                                @endforeach
                                            @endif
                                            {{--<option value="1">Gaint</option>
                                            <option value="2">Trisect</option>
                                            <option value="3">Superset</option>
                                            <option value="4">Mono</option>--}}
                                        </select>
                                        @if($errors->has('settype_id'))
                                            <span class="text-danger">{{$errors->first('settype_id')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Select BodyPart<sup>*</sup>  </label>
                                    <div class="col-sm-9">
                                        <select class="form-control"  name="workout[0][bodypart_id]"  id="bodypart_id" >
                                            <option value="">Select</option>
                                            @if($body)
                                                @foreach($body as $users)

                                                    <option value="{{$users->id}}">{{$users->name}}</option>
                                                @endforeach
                                            @endif

                                        </select>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Select ExcerciseType<sup>*</sup>  </label>
                                    <div class="col-sm-9">
                                        <select class="form-control"  name="workout[0][excercise_id]"  id="excercise_id">
                                            <option value="">Select</option>
                                            @if($excercise)
                                                @foreach($excercise as $users)

                                                    <option value="{{$users->id}}">{{$users->name}}</option>
                                                @endforeach
                                            @endif

                                        </select>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">SET</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="workout[0][set]" value=""/>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">REP</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name = "workout[0][rep]" value=""/>
                                    @if($errors->has('email'))
                                        <span class="text-danger">{{$errors->first('email')}}</span>
                                    @endif
                                </div>
                            </div>
                        </div>

                            <div class="col-md-3">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">REST</label>
                                    <div class="col-sm-9">
                                        <input type="time"  name ="workout[0][rest]" value=""/>
                                        @if($errors->has('email'))
                                            <span class="text-danger">{{$errors->first('email')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label ">Name<sup>*</sup> </label>
                                    <div class="col-sm-9">
                                      
                                      <input type="text" name="name" class="form-control"> 

                                    </div>
                                </div>
                            </div> 
                        </div>

                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Upload Video
                                    </label>
                                    <div class="col-sm-9">
                                        <input type="file"   name="workout[0][video]"  required value=""/>
                                        @if($errors->has('workout[0][video]'))
                                            <span class="text-danger">{{$errors->first('workout[0][video]')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Upload Video Url
                                    </label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" id="note" name="workout[0][video_url]" rows="4"></textarea>

                                    </div>
                                </div>
                            </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Upload Images
                                        </label>
                                        <div class="col-sm-9">
                                            <input type="file"  id="userimage" name="workout[0][userimage]" required>
                                            @if($errors->has('userimage'))
                                                <span class="text-danger">{{$errors->first('userimage')}}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                        </div>
                         <br><br>
                        <div class="row">
                            <div id = "desworkdiv" class = "col-md-12" style = "display:none">
                                <?php for($i=1;$i<=3;$i++){?>
                                <div>
                                    <h5> Gaint Form <?php echo $i;?></h5>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Select BodyPart<sup>*</sup>  </label>
                                            <div class="col-sm-9">
                                                <select class="form-control"  name="workout[<?php echo $i;?>][bodypart_id]"  id="bodypart_id">
                                                    <option value="">Select</option>
                                                    @if($body)
                                                        @foreach($body as $users)

                                                            <option value="{{$users->id}}">{{$users->name}}</option>
                                                        @endforeach
                                                    @endif

                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Select ExcerciseType<sup>*</sup>  </label>
                                            <div class="col-sm-9">
                                                <select class="form-control"  name="workout[<?php echo $i;?>][excercise_id]"  id="excercise_id">
                                                    <option value="">Select</option>
                                                    @if($excercise)
                                                        @foreach($excercise as $users)

                                                            <option value="{{$users->id}}">{{$users->name}}</option>
                                                        @endforeach
                                                    @endif

                                                </select>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">SET</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="workout[<?php echo $i;?>][set]" value=""/>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">REP</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name = "workout[<?php echo $i;?>][rep]" value=""/>
                                                @if($errors->has('email'))
                                                    <span class="text-danger">{{$errors->first('email')}}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">REST</label>
                                            <div class="col-sm-9">
                                                <input type="time"  name ="workout[<?php echo $i;?>][rest]" value=""/>
                                                @if($errors->has('email'))
                                                    <span class="text-danger">{{$errors->first('email')}}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Upload Video
                                            </label>
                                            <div class="col-sm-9">
                                                <input type="file"   name="workout[<?php echo $i;?>][video]" value=""/>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Upload Video Url
                                            </label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" id="note" name="workout[<?php echo $i;?>][video_url]" rows="4"></textarea>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Upload Images
                                            </label>
                                            <div class="col-sm-9">
                                                <input type="file"  id="userimage" name="workout[<?php echo $i;?>][userimage]">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </div>
<?php }?>
                        </div>
                            <div id = "desworkdiv2" class = "col-md-12" style = "display:none">
                                <?php for($i=1;$i<=2;$i++){?>
                                <div>
                                   <h5> Trisect Form <?php echo $i;?></h5>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Select BodyPart<sup>*</sup>  </label>
                                                <div class="col-sm-9">
                                                    <select class="form-control"  name="workout[<?php echo $i;?>][bodypart_id]"  id="bodypart_id">
                                                        <option value="">Select</option>
                                                        @if($body)
                                                            @foreach($body as $users)

                                                                <option value="{{$users->id}}">{{$users->name}}</option>
                                                            @endforeach
                                                        @endif

                                                    </select>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Select ExcerciseType<sup>*</sup>  </label>
                                                <div class="col-sm-9">
                                                    <select class="form-control"  name="workout[<?php echo $i;?>][excercise_id]"  id="excercise_id">
                                                        <option value="">Select</option>
                                                        @if($excercise)
                                                            @foreach($excercise as $users)

                                                                <option value="{{$users->id}}">{{$users->name}}</option>
                                                            @endforeach
                                                        @endif

                                                    </select>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">SET</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name="workout[<?php echo $i;?>][set]" value=""/>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-3">
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">REP</label>
                                                <div class="col-sm-9">
                                                    <input type="text" class="form-control" name = "workout[<?php echo $i;?>][rep]" value=""/>
                                                    @if($errors->has('email'))
                                                        <span class="text-danger">{{$errors->first('email')}}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">REST</label>
                                                <div class="col-sm-9">
                                                    <input type="time"  name ="workout[<?php echo $i;?>][rest]" value=""/>
                                                    @if($errors->has('email'))
                                                        <span class="text-danger">{{$errors->first('email')}}</span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Upload Video
                                                </label>
                                                <div class="col-sm-9">
                                                    <input type="file"   name="workout[<?php echo $i;?>][video]" value=""/>

                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Upload Video Url
                                                </label>
                                                <div class="col-sm-9">
                                                    <textarea class="form-control" id="note" name="workout[<?php echo $i;?>][video_url]" rows="4"></textarea>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group row">
                                                <label class="col-sm-3 col-form-label">Upload Images
                                                </label>
                                                <div class="col-sm-9">
                                                    <input type="file"  id="userimage" name="workout[<?php echo $i;?>][userimage]">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php }?>
                            </div>
                        </div>

                        <div id = "desworkdiv3" class = "col-md-12" style = "display:none">
                            <?php for($i=1;$i<=1;$i++){?>
                            <div>
                                <h5>  Superset Form <?php echo $i;?><h6>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Select BodyPart<sup>*</sup>  </label>
                                            <div class="col-sm-9">
                                                <select class="form-control"  name="workout[<?php echo $i;?>][bodypart_id]"  id="bodypart_id">
                                                    <option value="">Select</option>
                                                    @if($body)
                                                        @foreach($body as $users)

                                                            <option value="{{$users->id}}">{{$users->name}}</option>
                                                        @endforeach
                                                    @endif

                                                </select>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Select ExcerciseType<sup>*</sup>  </label>
                                            <div class="col-sm-9">
                                                <select class="form-control"  name="workout[<?php echo $i;?>][excercise_id]"  id="excercise_id">
                                                    <option value="">Select</option>
                                                    @if($excercise)
                                                        @foreach($excercise as $users)

                                                            <option value="{{$users->id}}">{{$users->name}}</option>
                                                        @endforeach
                                                    @endif

                                                </select>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">SET</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name="workout[<?php echo $i;?>][set]" value=""/>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">REP</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" name = "workout[<?php echo $i;?>][rep]" value=""/>
                                                @if($errors->has('email'))
                                                    <span class="text-danger">{{$errors->first('email')}}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">REST</label>
                                            <div class="col-sm-9">
                                                <input type="time"  name ="workout[<?php echo $i;?>][rest]" value=""/>
                                                @if($errors->has('email'))
                                                    <span class="text-danger">{{$errors->first('email')}}</span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Upload Video
                                            </label>
                                            <div class="col-sm-9">
                                                <input type="file"   name="workout[<?php echo $i;?>][video]" value=""/>

                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Upload Video Url
                                            </label>
                                            <div class="col-sm-9">
                                                <textarea class="form-control" id="note" name="workout[<?php echo $i;?>][video_url]" rows="4"></textarea>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label class="col-sm-3 col-form-label">Upload Images
                                            </label>
                                            <div class="col-sm-9">
                                                <input type="file"  id="userimage" name="workout[<?php echo $i;?>][userimage]">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php }?>
                        </div>





                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-sm-9">
                                        <input type="submit" class="form-control btn btn-primary" />
                                    </div>
                                </div>
                            </div>
                        </div>


                    </form>
                </div>
            </div>
        </div>

    </div>


@endsection


<script>

    function check(valu) {
		if(valu==1){
			$('#desworkdiv').show();
            $('#desworkdiv2').hide();
            $('#desworkdiv3').hide();
		}else if(valu==2){
			$('#desworkdiv').hide();
            $('#desworkdiv2').show();
            $('#desworkdiv3').hide();
		}else if(valu==3){
            $('#desworkdiv3').show();
            $('#desworkdiv').hide();
            $('#desworkdiv2').hide();

        }else if(valu==4){
            $('#desworkdiv3').hide();
            $('#desworkdiv').hide();
            $('#desworkdiv2').hide();
		}else {
			
		}
    }
</script>

