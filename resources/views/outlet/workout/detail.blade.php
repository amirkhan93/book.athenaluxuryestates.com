@extends('layouts.myappoutlet')

@section('content')

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/css/bootstrap-select.css">

    <div class="content-wrapper">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">WorkOut Detail</h4>

                        {{csrf_field()}}

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Select SetType<sup>*</sup>  </label>
                                    <div class="col-sm-9">
                                        <select class="form-control"  name="settype_id"  id="settype_id" onchange="check(this.value)">
                                            <option value="">Select Settype</option>
                                            @if($settype)
                                                @foreach($settype as $users)
                                                    <option value="{{$users->id}}" @if($users->id==$bst->settype_id) selected @endif>{{$users->name}}</option>
                                                @endforeach
                                            @endif
                                            {{--<option value="1">Gaint</option>
                                            <option value="2">Trisect</option>
                                            <option value="3">Superset</option>
                                            <option value="4">Mono</option>--}}
                                        </select>
                                        @if($errors->has('settype_id'))
                                            <span class="text-danger">{{$errors->first('settype_id')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Select BodyPart<sup>*</sup>  </label>
                                    <div class="col-sm-9">
                                        <select class="form-control"  name="workout[0][bodypart_id]"  id="bodypart_id" >
                                            <option value="">Select</option>
                                            @if($body)
                                                @foreach($body as $users)

                                                    <option value="{{$users->id}}" @if($users->id==$bst->bodypart_id) selected @endif>{{$users->name}}</option>
                                                @endforeach
                                            @endif

                                        </select>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Select ExcerciseType<sup>*</sup>  </label>
                                    <div class="col-sm-9">
                                        <select class="form-control"  name="workout[0][excercise_id]"  id="excercise_id">
                                            <option value="">Select</option>
                                            @if($excercise)
                                                @foreach($excercise as $users)

                                                    <option value="{{$users->id}}" @if($users->id==$bst->excercise_id) selected @endif>{{$users->name}}</option>
                                                @endforeach
                                            @endif

                                        </select>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">SET</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="workout[0][set]" value="{{$bst->sets}}"/>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">REP</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name = "workout[0][rep]" value="{{$bst->rep}}"/>
                                        @if($errors->has('email'))
                                            <span class="text-danger">{{$errors->first('email')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">REST</label>
                                    <div class="col-sm-9">
                                        <input type="time"  name ="workout[0][rest]" value="{{$bst->rest}}"/>
                                        @if($errors->has('email'))
                                            <span class="text-danger">{{$errors->first('email')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label ">Select Days<sup>*</sup> </label>
                                    <div class="col-sm-9">
                                        <?php $explode = explode(',',$bst->days_id)?>
                                        <select class="js-example-basic-multiple "  name="days[]"  id="member_id" multiple="multiple" style="width:100%;">
                                            <option value="">Select</option>
                                            <option value="1" @if(in_array('1',$explode)) selected @endif>Monday</option>
                                            <option value="2" @if(in_array('2',$explode)) selected @endif>Tuesday</option>
                                            <option value="3" @if(in_array('3',$explode)) selected @endif>Wednesday</option>
                                            <option value="4" @if(in_array('4',$explode)) selected @endif>Thursday</option>
                                            <option value="5" @if(in_array('5',$explode)) selected @endif>Friday</option>

                                            <option value="6" @if(in_array('6',$explode)) selected @endif>Saturday</option>
                                            <option value="7" @if(in_array('7',$explode)) selected @endif>Sunday</option>

                                        </select>

                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label ">Name<sup>*</sup> </label>
                                    <div class="col-sm-9">

                                        <input type="text" name="name" class="form-control" value="{{$bst->name}}">

                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <?php if($bst->videoz){?>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Upload Video
                                    </label>

                                </div>
                            </div>
                            <?php }?>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Upload Video Url
                                    </label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" id="note" name="workout[0][video_url]" rows="4">{{$bst->video_url}}</textarea>

                                    </div>
                                </div>
                            </div>
                            <?php if($bst->image){?>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Upload Images
                                    </label>
                                    <div class="col-sm-9">
                                    <img src="{{url('resources/assets/images/'.$bst->image)}}" style="width: 50px;height: 50px;">

                                    </div>
                                </div>
                            </div>
                            <?php }?>
                        </div>

                </div>
            </div>
        </div>

    </div>


@endsection
