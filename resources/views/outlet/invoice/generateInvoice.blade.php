<!DOCTYPE html>
<html lang="en">
	<head> 
		<title>Invoice</title>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>
		<meta charset="utf-8">
	  	<meta name="viewport" content="width=device-width, initial-scale=1">
	</head>
	<body>	
		
		                 @php
                            $data = \App\Http\Helpers\Helpers::getOutletDetail(Auth::guard('outlet')->id());
                            $datass = \App\Http\Helpers\Helpers::getVendorDetail($data->vendor_id);
                            $out_data = DB::table('addnewoutlet')->where('vendor_id', $data->vendor_id)->get();
                        @endphp

                       


		<div class="main-cont" id="print">
			<div style="width: 100%; max-width: 900px; margin: 30px auto; border: 2px solid #ccc; position: relative;padding: 25px;">
				<div style="display: block;">
					<div style="width: 48%; margin-right: 2%; display: inline-block;">
						<div style="margin-bottom: 10px;">
							<a href="#"><img src="http://super.ifitmash.club/resources/assets/images/<?php echo $datass->profile_image?>" alt="" style="width: 127px; height: 71px;"></a>
						</div>
						<div style="font-size: 16px; color: #333333; line-height: 21px;  font-weight: 600;">
							<p>{{$data->street_address1}} {{$data->street_address2}}<br>{{$data->contact}}</p>
							<!-- <span style="font-size: 13px;">GSTIN 06AEHPK3053J1ZK</span> -->
						</div>
					</div>
					 <?php

                        if($baseprice=$users->price){
                           
                            if($user_data->is_gst == '1'){
                                $gst=18/100*$baseprice;
                            }
                            else{
                                $gst = 0;
                            }
                          
                            $totalbalance=$baseprice+$gst;
                           
                        }                 ?>
					<div style="width: 49%; text-align: right;     display: inline-block;">
						<h1 style="margin: 10px 0px 20px; font-size: 26px; line-height: 30px;">TAX INVOICE</h1>
						<div>
							<p>Balance Due</p>
							<h2>INR {{$totalbalance}}</h2>
						</div>
					</div>
				</div>
				<div style="display: block; clear: both;">
					<div style="display: inline-block; width: 48%; margin-right: 2%;">
						<div style="margin-top: 20px;">
							<p>Bill To</p>
							<p style="font-size: 14px;"><strong>{{$user_data->name}}
								<!-- <span style="margin-left: 20px;">GSTN 06AAACZ9608E2Z8</span></strong></p> -->
							<p style="margin-top: 25px;">Address: {{$user_data->name}}</p>
						</div>
					</div>
					<div style="display: inline-block; width: 49%;">
						<div style="display: block; clear: both;">
							<table cellspacing="0" style="float: right; width: 350px; color: #333; font-weight: 500; text-align: right; margin-top: 20px;">
								<tbody >
									<tr>
										<td style="padding-bottom: 5px;">Invoice Date: </td>
										<td style="padding-bottom: 5px;"><?php echo date('d/m/Y') ?></td>
									</tr>
									<tr>
										<td style="padding-bottom: 5px;">Terms: </td>
										<td style="padding-bottom: 5px;">Due on Receipt</td>
									</tr>
									<tr>
										<td style="padding-bottom: 5px;">Due Date: </td>
										<!-- <td style="padding-bottom: 5px;">21/06/2018</td> -->
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<div style="display: block; clear: both;">
					<table cellspacing="0" style="width: 100%;">							
						<thead style="background: #333; color: #fff; font-size: 14px;">
							<tr>
								<th style="padding: 10px; border: 0px; text-align: center;">#</th>
								<!-- <th style="padding: 10px; border: 0px; text-align: center;">Item & Description</th> -->
								<th style="padding: 10px; border: 0px; text-align: center;">Package</th>
								<th style="padding: 10px; border: 0px; text-align: center;">Rat</th>
								<th style="padding: 10px; border: 0px; text-align: center;">GST</th>
								<!-- <th style="padding: 10px; border: 0px; text-align: center;">SGST</th> -->
								<th style="padding: 10px; border: 0px; text-align: center;">Amount</th>
							</tr>
						</thead>
						<tbody>
							<tr style="border-bottom: 1px solid #ccc;">
								<td style="padding: 10px; border: 0px; text-align: center; border-bottom: 1px solid #ccc;">1</td>
								<!-- <td style="padding: 10px; border: 0px; text-align: center; border-bottom: 1px solid #ccc;">Invoice April 2018</td> -->
								<td style="padding: 10px; border: 0px; text-align: center; border-bottom: 1px solid #ccc;">{{$users->name}}</td>
								<td style="padding: 10px; border: 0px; text-align: center; border-bottom: 1px solid #ccc;">INR {{$users->price}}</td>
								<td style="padding: 10px; border: 0px; text-align: center; border-bottom: 1px solid #ccc;">@if($user_data->is_gst == '1')
                         
                            <span>18%</span>@else<span>NA</span>@endif</td>
								<!-- <td style="padding: 10px; border: 0px; text-align: center; border-bottom: 1px solid #ccc;">106.65 <span>9%</span></td> -->
								<td style="padding: 10px; border: 0px; text-align: center; border-bottom: 1px solid #ccc;">INR {{$totalbalance}}</td>
							</tr>							
						</tbody>
					</table>
					<div style="display: block; clear: both;">
						<table cellspacing="0" style="width: 100%; max-width: 350px; float: right; text-align: right; position: relative; right: 30px; margin-top: 10px;">
							<tbody>
								<tr>
									<td style="padding: 5px 0px;">Sub Total</td>
									<td style="padding: 5px 0px;">INR {{$totalbalance}}</td>
								</tr>
								<!-- <tr>
									<td style="padding: 5px 0px;">CGST9 (9%)</td>
									<td style="padding: 5px 0px;">938.25</td>
								</tr> -->
								@if($user_data->is_gst == '1')
								<tr>
									<td style="padding: 5px 0px;">GST</td>
									<td style="padding: 5px 0px;">{{$gst}}
                            </td>
                            @else
                            <tr>
									<td style="padding: 5px 0px;">GST</td>
									<td style="padding: 5px 0px;">NA
                            </td>
                            @endif
								</tr>
								<tr>
									<td style="padding: 5px 0px;"><strong>Total</strong></td>
									<td style="padding: 5px 0px;"><strong>INR {{$totalbalance}}</strong></td>
								</tr>
								<tr style="background: #f9f9f9;">
									<td style="padding: 5px 0px;"><strong>Balace Due</strong></td>
									<td style="padding: 5px 0px;"><strong>INR {{$totalbalance}}</strong></td>
								</tr>
							</tbody>
						</table>
						<div style="display: flex; width: 100%;">
							<div style="display: block; width: 65%; display: inline-block; margin-top: 10px;">
								
							</div>
							<!-- <div style="display: block; width: 35%; display: inline-block; margin-top: 10px;">
								<p style="float: left; width: 120px;">Toatl In Words: </p>
								<p style="float: left; width: calc(100% - 120px);"><strong>Rupees twelve thounand eight hundred ninety-one and fifty paise</strong></p>
							</div> -->
						</div>
					</div>
				</div>
				<div style="width: 100%; display: block; margin-top: 50px;">
					<p>Authorized Signature <span style="width: 150px; height: 1px; background: #333; display: inline-block; margin-left: 5px;"></span></p>
				</div>
			</div>
		</div>

		

</script>
 

</body>
</html>
	</body>
</html>