@extends('layouts.myappoutlet')

@section('content')
    <div class="content-wrapper">
        <div >
            <a  href="{{url('outlet/nutrition/create')}}" style="
    background: #1976d2;
    border: 1px solid #1976d2;
    text-decoration: none;
    font-size: 14px;
    padding: 2px 5px;
    border-radius: 4px;
    color: #fff;
    cursor: pointer;
">Add New</a>
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Nutrition</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>
                                    <th>Meal Type</th>
                                    <th>Meal Time</th>
                                    <th>View Food</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($results as $result)
                                    <tr>
                                        <td>{{$result->meal_type}}</td>
                                        <td>{{$result->meal_time}}</td>
                                        <td><a href="#" onclick="showFoodData({{$result->id}})" class="btn btn-primary">View</a></td>
                                        <td><a href="{{ url('outlet/nutritions/delete/'.$result->id) }}" class="btn btn-danger" onclick="confirm('Are you sure want to delete')">Delete</a></td>
                                    </tr>

                                @endforeach
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade custom-popup" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Food Details</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="content-wrapper" id="exampleData">


                    </div>
                </div>
            </div>
        </div>
        <!-- Add Popup Start -->

        <!-- Add Popup Ends -->
    </div>
@endsection
<script>
    function showFoodData(id){
        $.ajax({
            type : 'POST',
            url : '{{url('outlet/getnutritionfood')}}',
            data : {id : id,_token:'{{csrf_token()}}'},
            success : function(msg) {
                $('#exampleModal').modal('show');
                $('#exampleData').html(msg);
            }
        });
    }
</script>