@extends('layouts.myappoutlet')

@section('content')

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.8.1/css/bootstrap-select.css">
    <div class="content-wrapper">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Nutrition</h4>
                    <form class="form-sample" action="{{url('outlet/nutrition/store')}}" method ="post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <p class="card-description">

                        </p>
                        <div  id="meal_type_div">
                            <div class="row"  style="border: 1px solid lightgrey;padding: 10px">
                                <div class="col-md-5">
                                    <div class="form-group row">
                                        <div class="col-sm-7">
                                            <label class="col-sm-7">Meal Type</label>
                                            <input type="text" class="form-control" name="nutrition[0][meal_type]" value=""/>
                                            @if($errors->has('nutrition[0][meal_type]'))
                                            <span class="text-danger">{{$errors->first('nutrition[0][meal_type]')}}</span>
                                        @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-5">
                                    <div class="form-group row">
                                        <div class="col-sm-7">
                                            <label class="col-sm-7">Meal Time</label>
                                            <input type="time" class="form-control" name = "nutrition[0][meal_time]" value=""/>
                                            @if($errors->has('nutrition[0][meal_time]'))
                                                <span class="text-danger">{{$errors->first('nutrition[0][meal_time]')}}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-5">
                                    <div class="form-group row">
                                        <div class="col-sm-7">
                                            <label class="col-sm-7">Days</label>
                                            <select class="form-control js-example-basic-multiple"  name="days[]"  id="member_id" multiple="multiple" style="width:100%;" data-placeholder="Select Days">
                                                <option value="">Select</option>
                                                <option value="1">Monday</option>
                                                <option value="2">Tuesday</option>
                                                <option value="3">Wednesday</option>
                                                <option value="4">Thursday</option>
                                                <option value="5">Friday</option>

                                                <option value="6">Saturday</option>
                                                <option value="7">Sunday</option>

                                            </select>
                                            @if($errors->has('days'))
                                            <span class="text-danger">{{$errors->first('days')}}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group row">
                                        <div class="col-sm-7">
                                            <label class="col-sm-7">Week</label>
                                            <select class="js-example-basic-multiple "  name="week"  id="week"  style="width:100%;">
                                                <option value="">Select</option>
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                            </select>
                                            @if($errors->has('week'))
                                            <span class="text-danger">{{$errors->first('week')}}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group row">
                                        <div class="col-sm-2">
                                            <button type="button" class="btn btn-primary" onclick="showMeal(0)"><i class="fa fa-plus"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="form-group row">
                                        <label class="col-sm-7">Food 1</label>
                                    </div>
                                </div>

                                <div class="col-md-5">
                                    <div class="form-group row">
                                        <div class="col-sm-7">
                                            <input type="text" class="form-control" name = "nutrition[0][nutrifood][]" value=""/>
                                            @if($errors->has('nutrition[0][nutrifood][]'))
                                                <span class="text-danger">{{$errors->first('nutrition[0][nutrifood][]')}}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group row">
                                        <div class="col-sm-2">
                                            <button type="button" class="btn btn-primary" onclick="showFood(0)"><i class="fa fa-plus"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div id="food_type_div0">
                                    <div id="showFood_0"></div>
                                </div>
                            </div>

                            <div id="showMeal_0"></div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-sm-4">
                                        <input type="submit" class="form-control btn btn-primary" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>


@endsection


<script>

    var e = 1;
    function showMeal(typ) {
        var id = 'meal_type_div' + e;
        var html = '<div id="' + id + '" class="row" style="border: 1px solid lightgrey;padding: 10px;margin-top:40px;"> ' +
                '<div class="col-md-5">'+
                    '<div class="form-group row">'+
                        '<div class="col-sm-7">'+
                            '<label class="col-sm-7">Meal Type</label>'+
                            '<input type="text" class="form-control" name="nutrition['+e+'][meal_type]" value=""/>'+
                        '</div> ' +
                    '</div>' +
                ' </div>'+

                '<div class="col-md-5">'+
                    '<div class="form-group row">'+
                        '<div class="col-sm-7">'+
                            '<label class="col-sm-7">Meal Time</label>'+
                            '<input type="time" class="form-control" name = "nutrition['+e+'][meal_time]" value=""/>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<div class="col-md-2">'+
                    '<div class="form-group row">'+
                        '<div class="col-sm-2">'+
                            '<button type="button" class="btn btn-danger" onclick="remove_meal('+e+')"><i class="fa fa-minus"></i></button>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<div class="col-md-5">'+
                    '<div class="form-group row">'+
                        '<label class="col-sm-7">Food 1</label>'+
                    '</div>'+
                '</div>'+
                '<div class="col-md-5">'+
                    '<div class="form-group row">'+
                        '<div class="col-sm-7">'+
                            '<input type="text" class="form-control" name = "nutrition['+e+'][nutrifood][]" value=""/>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<div class="col-md-2">'+
                    '<div class="form-group row">'+
                        '<div class="col-sm-2">'+
                            '<button type="button" class="btn btn-primary" onclick="showFood('+e+')"><i class="fa fa-plus"></i></button>'+
                        '</div>'+
                    '</div>'+
                '</div>'+
                '<div id="food_type_div'+e+'">'+
                    '<div id="showFood_'+e+'"></div>'+
                '</div>'+
            '</div>';
        $('#showMeal_'+typ).append(html);
        e++;

    }

    function remove_meal(id) {
        var div = '#meal_type_div'+id;
        $(div).remove();
    }

    var x = 1;
    function showFood(tys) {
        var id = 'food_type_div'+tys + x;
        var inc = x+1;
        var html = '<div id="' + id + '" class="row"> ' +
            '<div class="col-md-5">'+
            '<div class="form-group row">'+
            '<label class="col-sm-7">Food '+inc+'</label>'+
            '</div>'+
            '</div>'+

            '<div class="col-md-5">'+
            '<div class="form-group row">'+
            '<div class="col-sm-7">'+
            '<input type="text" class="form-control" name = "nutrition['+x+'][nutrifood][]" value=""/>'+
            '</div>'+
            '</div>'+
            '</div>'+
            '<div class="col-md-2">'+
            '<div class="form-group row">'+
            '<div class="col-sm-2">'+
            '<button type="button" class="btn btn-danger" onclick="remove_food('+tys+','+x+')"><i class="fa fa-minus"></i></button>'+
            '</div>'+
            '</div>'+
            '</div>'+
            '</div>';
        $('#showFood_'+tys).append(html);
        x++;

    }

    function remove_food(tys,id) {
        var div = '#food_type_div'+tys+id;
        $(div).remove();
    }
</script>

