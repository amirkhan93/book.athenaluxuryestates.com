<div class="col-12 grid-margin">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Food</h4>
            <table id="order-listing" class="table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
               @foreach($results as $result)
                <tr id="rem_{{$result->id}}">
                    <td>{{$result->food_name}}</td>
                    <td><a href="#" class="btn btn-danger" onclick="removeData({{$result->id}})"><i class="fa fa-trash-o"></i></a></td>
                </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>
    function removeData(id){
        if(confirm('Are you sure want to delete')){
            $.ajax({
               type : 'POST',
               url : '{{url('outlet/deleteNutrifood')}}',
               data : {id:id,_token:'{{csrf_token()}}'},
                success : function(msg){
                    if(msg==1){
                        alert('Deleted Successfully');
                        $('#rem_'+id).remove();
                    }else{
                        alert('Error in delete');
                    }
                }
            });
        }
    }
</script>