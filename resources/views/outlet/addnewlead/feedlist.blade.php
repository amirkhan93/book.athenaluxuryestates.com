@extends('layouts.myappoutlet')

@section('content')
    <div class="content-wrapper">
        <div >
            <h5 style="color:#dc3545; border: 1px">{{ session('status') }}</h5>
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Feeds List</h4>
                <div> <a data-toggle="modal" data-target="#exampleModal" href="#" style="
    background: #1976d2;
    border: 1px solid #1976d2;
    text-decoration: none;
    font-size: 14px;
    padding: 2px 5px;
    border-radius: 4px;
    color: #fff;
    cursor: pointer;
"><i class="mdi mdi-plus-circle" ></i></a></div>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>code</th>

                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Contact</th>
                                    <th>Address</th>
                                    <th>Status</th>
                                    <th>Source</th>
                                    <th>Gender</th>
                                    <th>DOB</th>



                                </tr>
                                </thead>
                                <tbody>

                                <?php $i = 1; foreach($bn as $user) {?>
                                <?php

                                // echo 'd';exit;
                                $source=\App\Source::where('id',$user->source_id)->first();
                                $st=\App\Status::where('id',$user->status_id)->first();

                                // echo  $user->status_id;die;



                                ?>

                                <tr>

                                    <td>{{ $i }}</td>
                                    <td>{{$user->code}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->contact}}</td>

                                    <td>{{ $user->address}}</td>
                                    <td>{{ $st->name}}</td>
                                    <td>{{ $source->name}}</td>
                                    <td>{{ $user->gender}}</td>
                                    <td>{{ $user->dob}}</td>


                                </tr>
                                <?php $i++;}?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade custom-popup" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Feedback</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="content-wrapper">
                        <div class="col-12 grid-margin">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Feedback</h4>
                                    <form class="form-sample" action="{{url('outlet/addnewlead/feedstore/')}}" method ="post" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <p class="card-description">

                                        </p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">Message </label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" name = "message"/>
                                                        @if($errors->has('name'))
                                                            <span class="text-danger">{{$errors->first('name')}}</span>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-sm-9">
                                                        <input type="submit" class="form-control btn btn-primary" />
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
