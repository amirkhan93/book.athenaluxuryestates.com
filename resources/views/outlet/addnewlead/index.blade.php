@extends('layouts.myappoutlet')

@section('content')
    <div class="content-wrapper">
        <div >
            <a href="{{ url('outlet/addnewlead/create') }}" style="
    background: #1976d2;
    border: 1px solid #1976d2;
    text-decoration: none;
    font-size: 14px;
    padding: 2px 5px;
    border-radius: 4px;
    color: #fff;
    cursor: pointer;
">Add New</a>
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Follow Up Lead</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>code</th>

                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Contact</th>
                                    <th>Address</th>
                                    <th>Trail DateTime</th>
                                    <th>Action</th>

                                </tr>
                                </thead>
                                <tbody>

                                <?php $i = 1; foreach($bn as $user) {?>


                                <tr>

                                    <td>{{ $i }}</td>
                                    <td>{{$user->code}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->contact}}</td>
                                    <td>{{ $user->address}}</td>
                                    <td>{{ $user->traildate }} {{ $user->trailtime }}</td>
                                    <td>
                                        <div class="row">
                                             @if($user->status != 1)
                                            <a href="{{url('outlet/addnewlead/active/'.$user->id)}}"
                                               onclick="if(! confirm('Click Ok To Open ')){ return false}"
                                               class="btn btn  btn-danger" data-toggle="tooltip" >Closed</a>
                                            @else
                                                <a href="{{url('outlet/addnewlead/inactive/'.$user->id)}}"
                                                   onclick="if(! confirm('Click Ok To Close')){ return false}"
                                                   class="btn btn-success " data-toggle="tooltip" >Open</a>
                                            @endif
                                            <button  class="btn btn-outline-primary"><a href="{{ url('outlet/addnewlead/edit',[$user->id]) }}">Edit</a>
                                            </button>
                                            
                                           
                                            <a  class="btn btn-danger" 
                                            onclick="return confirm('Are you sure ?');" 
                                            href="{{ url('outlet/addnewlead/delete',[$user->id]) }}">Delete
                                            </a> 
                                         

                                           <!-- <button type="button"  class="btn btn-danger"> -->
                                            <a  class="btn btn-danger" 
                                            href="{{ url('outlet/addnewlead/view/'.$user->id) }}">Detail</a>
                                            <!-- </button> -->

                                           <button type="button" class="btn btn-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" >
                                                        <i class="fa fa-ellipsis-v"></i>
                                               <ul class="dropdown-menu">
                                                   <li>Call</li>
                                                   <li>Sms</li>
                                                   <li><a href="{{ url('outlet/addnewlead/feedlist/'.$user->id) }}">Feedback</a></li>
                                               </ul>
                                            </button>

                                        </div>
                                       
                                    </td>
                                </tr>
                                <?php $i++;}?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
