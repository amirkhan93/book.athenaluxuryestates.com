@extends('layouts.myappoutlet')

@section('content')
    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Add New Lead Detail</h4>
                <div class="row">
        <div class="widget-body hotel-info-table table-responsive">
        <table class="table table-bordered" border="2px" bgcolor="white">
            <thead>
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th>Value</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 1; foreach($bn as $user) { ?>
            <?php

            // echo 'd';exit;
           $source=\App\Source::where('id',$user->source_id)->first();
            $st=\App\Status::where('id',$user->status_id)->first();

            // echo  $user->status_id;die;



            ?>

            <tr>
                <td>1</td>
                <td>Lead Code</td>
                <td>{{ $user->code }}</td>
            </tr>

            <tr>
                <td>2</td>
                <td> Name</td>
                <td>{{ $user->name}}</td>
            </tr>
            <tr>
                <td>3</td>
                <td> Email</td>
                <td>{{ $user->email}}</td>
            </tr>

            <tr>
                <td>4</td>
                <td>Contact</td>
                <td>{{ $user->contact}}</td>
            </tr>
            <tr>
                <td>5</td>
                <td>Status</td>
                <td>{{ $st->name}}</td>
            </tr>
            <tr>
                <td>6</td>
                <td>Sources</td>
                <td>{{ $source->name}}</td>
            </tr>
            <tr>
                <td>7</td>
                <td>Gender</td>
                <td>{{ $user->gender}}</td>
            </tr>

                <td>8</td>
                <td>DOB</td>
                <td>{{ $user->dob}}</td>
            </tr>
            <tr>
                <td>9</td>
                <td>Next Follow Update</td>
                <td>{{ $user->followup}}</td>
            </tr>
            <tr>
            <tr>
                <td>10</td>
                <td>TrailDate</td>
                <td>{{ $user->traildate}}</td>
            </tr>
            <tr>
                <td>11</td>
                <td>TrailTime</td>
                <td>{{ $user->trailtime}}</td>
            </tr>
            <tr>
                <td>12</td>
                <td>Address</td>
                <td>{{ $user->address}}</td>
            </tr>

            <tr>
                <td>13</td>
                <td>Note</td>
                <td>{{ $user->note}}</td>
            </tr>



            <?php $i++;}?>
            </tbody>
        </table>
    </div>
    </div>
    </div>
     </div>
    </div>

@endsection
