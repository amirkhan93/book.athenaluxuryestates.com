@extends('layouts.myappoutlet')

@section('content')
    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Invoice List</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Code</th>
                                    <th>Member Name</th>
                                    <th>Staff Name</th>
                                    <th>Invoice Date</th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php $i = 1; foreach($bn as $user) {?>
                              <?php
                                $outlet=\App\Vendorusers::where('vendor_id',$user->vendor_id)->get()->toArray();
                               // echo "<pre>";
                               // print_r($vendor);
                                ?>

                                <tr>

                                     <td>{{ $i }}</td>
                                   
                                    
                                    <td>{{$user->code}}</td>
                                    <td></td>
                                    <td></td>
                                    <td>{{date('d-m-Y',strtotime($user->created_at))}}</td>
                                </tr>
                                <?php $i++;}?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
