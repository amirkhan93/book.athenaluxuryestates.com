@extends('layouts.myappoutlet')

@section('content')
    <div class="content-wrapper">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Generate Invoice</h4>

                    <form class="form-sample" action="{{url('outlet/invoicestore/')}}" method = "post" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <p class="card-description">

                        </p>

                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label"> Select Invoice</label>
                                        <div class="col-sm-9">
                                            <input type="date"  placeholder="dd/mm/yyyy" class="form-control" name="invoice"/>
                                            @if($errors->has('invoice'))
                                                <span class="text-danger">{{$errors->first('invoice')}}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Member Name<sup style="color:red">*</sup>  </label>
                                        <div class="col-sm-9">
                                            <select class="form-control"  name="membername"  id="membername">
                                                <option value="">Select</option>

                                                @if($ac)
                                                    @foreach($ac as $users)

                                                        <option value="{{$users->id}}">{{$users->name}}</option>
                                                    @endforeach
                                                @endif

                                            </select>
                                            @if($errors->has('membername'))
                                                <span class="text-danger">{{$errors->first('membername')}}</span>
                                            @endif

                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Sales Credit for Staff<sup style="color:red">*</sup>  </label>
                                        <div class="col-sm-9">
                                            <select class="form-control"  name="sales"  id="sales">
                                                <option value="">Select</option>

                                                @if($sales)
                                                    @foreach($sales as $users)

                                                        <option value="{{$users->id}}">{{$users->name}}</option>
                                                    @endforeach
                                                @endif

                                            </select>
                                            @if($errors->has('sales'))
                                                <span class="text-danger">{{$errors->first('sales')}}</span>
                                            @endif

                                        </div>
                                    </div>
                                </div>
                            </div>

                               <!-- <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">  </label>
                                        <div class="col-sm-12">
                                            <select class="form-control "  id="package" name="pacakge" placeholder="select package">
                                                <option value="">Select Pacakage</option>

                                                @foreach($package as $users)

                                                    <option value="{{$users->id}}">{{$users->name}}</option>
                                                @endforeach

                                            </select>

                                        </div>
                                    </div>
                                </div>-->
                                <div class="col-md-12">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">  </label>
                                        <div class="col-sm-12">
                                            <btn class="btn btn-success btn-sm " data-toggle="modal" data-target="#exampleModal" href="#">Select Package</btn>
                                        </div>
                                    </div>
                                </div>
                         <!--   <div class="col-md-12">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Tags  </label>
                                    <div class="col-sm-12">
                                        <select class="form-control"  id="tags" name="tags" placeholder="tags">
                                           <option value=""></option>

                                            @foreach($tags as $users)

                                                <option value="{{$users->id}}">{{$users->name}}</option>
                                            @endforeach

                                        </select>

                                    </div>
                                </div>
                            </div>
                            </div>-->

                        <input type="hidden" name="package_id" id="package_id" value="">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-sm-9">
                                        <input type="submit"  class="form-control btn btn-primary" />
                                    </div>
                                </div>
                            </div>
                        </div>


                    </form>
                </div>



            </div>
            <div class="modal fade custom-popup" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Package</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="content-wrapper">
                            <div class="col-12 grid-margin">
                                <div class="card">
                                    <div class="card-body">
                                        <form class="form-sample" action="{{url('outlet/packagelist')}}" method = "post">
                                            {{csrf_field()}}
                                            <p class="card-description">
                                            </p>
                                            @foreach($package as $users)
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <div class="form-group row">
                                                            <div class="col-sm-9">
                                                                <input type="radio" class="form-check-input" name="choices" id="choices" value="{{$users->id}}">
                                                              
                                                                {{$users->name}}
                                                                <p>Rs. {{ $users->price }}/-</p>
                                                                
                                                                @if($errors->has('choices'))
                                                                    <span class="text-danger">{{$errors->first('choices')}}</span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-group row">
                                                        <div class="col-sm-9">

                                                        </div>
                                                        <div class="col-sm-9">

                                                            <input type="submit" value="next" class="form-control btn btn-primary" />
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
<script>
    function selectPlan(id){
        // alert(id);
        $('#package_id').val(id);
        $('#exampleModal').modal('hide');
    }
</script>
