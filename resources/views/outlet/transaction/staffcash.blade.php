@extends('layouts.myappoutlet')

@section('content')
    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Staff Register</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>code</th>
                                    <th>taffname</th>
                                    <th>StartDate</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; foreach($bst as $user) {?>
                                <tr>

                                    <td>{{ $i }}</td>
                                    <td>{{$user->code}}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->created_at}}</td>
                                </tr>
                                <?php $i++;}?>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Add Popup Start -->

        <!-- Add Popup Ends -->
    </div>
@endsection
