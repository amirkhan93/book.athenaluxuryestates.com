@extends('layouts.myapp')

@section('content')
    <div class="content-wrapper">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Business Type</h4>
                    <form class="form-sample" action="{{url('admin/businesstype/update/'.$ac->id)}}" method = "post">
                        {{csrf_field()}}
                        <p class="card-description">

                        </p>
                        <?php $explode = explode(',',$ac->choices)?>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Business Type</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name = "name" value="{{$ac->name}}"/>
                                        @if($errors->has('name'))
                                            <span class="text-danger">{{$errors->first('name')}}</span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-8">
                                            <div class="form-check form-check-warning">
                                                <label class="form-check-label " class="col-sm-3 col-form-label">
                                                    <input type="checkbox" class="form-check-input" name="choices[]" id="workout" value="workout" @if(in_array('workout',$explode)) checked @endif>
                                                    Workout
                                                </label>
                                            </div>
                                        </div>

                                        <div class="col-sm-8">
                                            <div class="form-check form-check-warning">
                                                <label class="form-check-label " class="col-sm-3 col-form-label">
                                                    <input type="checkbox" class="form-check-input" name="choices[]" id="nutrition" value="nutrition" @if(in_array('nutrition',$explode)) checked @endif>
                                                    Nutrition
                                                </label>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-sm-9">
                                        <input type="submit" class="form-control btn btn-primary" />
                                    </div>
                                </div>
                            </div>


                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection
