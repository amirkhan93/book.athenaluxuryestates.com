@extends('layouts.myapp')

@section('content')
    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title"> Members Detail</h4>
                <div class="tab">
                    <button class="tablinks" onclick="openCity(event, 'generalinfo')">General Information</button>
                    <button class="tablinks" onclick="openCity(event, 'bankdetail')">Outlet Information</button>

                </div>
    <div id="generalinfo" class="tabcontent" class = "col-md-12" style="display:block">
                <div class="row">
        <div class="widget-body hotel-info-table table-responsive">
        <table class="table table-bordered" border="2px" bgcolor="white">
            <thead>
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th>Value</th>
            </tr>
            </thead>
            <tbody>
            <?php $i = 1; foreach($vendors as $user) {?>
            <?php

            // echo 'd';exit;
            $supName = \App\Businesstype::where('id',$user->businesstype_id)->first();
            $bs = \App\Businesspackage::where('id',$user->businesspackage_id)->first();
            // echo  $supName;die;



            ?>
            <tr>
                <td>1</td>
                <td> Name</td>
                <td>{{ $user->name }}</td>
            </tr>

            <tr>
                <td>2</td>
                <td>Business Name</td>
                <td>{{$user->business_name}}</td>
            </tr>

            <tr>
                <td>3</td>
                <td>Business Type</td>
                <td>{{ $supName->name}}</td>
            </tr>
            <tr>
                <td>4</td>
                <td>Business Pacakge</td>
                <td>{{ $bs->name}}</td>
            </tr>
            <tr>
                <td>5</td>
                <td>Days</td>
                <td>{{ $bs->days}}</td>
            </tr>
            <tr>
                <td>6</td>
                <td>Price</td>
                <td>{{ $bs->price}}</td>
            </tr>
            <tr>
                <td>7</td>
                <td>Email id</td>
                <td>{{ $user->email }}</td>
            </tr>
            <?php $i++;}?>
            </tbody>
        </table>
    </div>
    </div>
    </div>

                <div id="bankdetail" class="tabcontent"  class = "col-md-12" style = "display:none">
                    <div class="row">
                        <div class="widget-body hotel-info-table table-responsive">
                            <table class="table table-bordered" border="2px" bgcolor="white">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Title</th>
                                    <th>Value</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; foreach($outlet as $user) {?>
                                <?php

                                // echo 'd';exit;
                                $supName = \App\Businesstype::where('id',$user->businesstype_id)->first();
                                $bs = \App\Businesspackage::where('id',$user->businesspackage_id)->first();
                                // echo  $supName;die;



                                ?>
                                <tr>
                                    <td>1</td>
                                    <td> Code</td>
                                    <td>{{ $user->code }}</td>
                                </tr>

                                <tr>
                                    <td>2</td>
                                    <td>Name</td>
                                    <td>{{$user->outlet_name}}</td>
                                </tr>

                                <tr>
                                    <td>3</td>
                                    <td>Address</td>
                                    <td>{{ $user->street_address1}}</td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>Created Date</td>
                                    <td>{{ $user->created_at}}</td>
                                </tr>
                                <?php $i++;}?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
     </div>
    </div>

@endsection
<script>
    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }
</script>
<style>
    body {font-family: Arial;}

    /* Style the tab */
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #f1f1f1;
    }

    /* Style the buttons inside the tab */
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }

    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #ccc;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        /* border: 1px solid #ccc;*/

        border-top: none;
    }
</style>