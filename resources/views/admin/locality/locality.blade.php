@extends('layouts.myapp')
 
@section('content')
    <div class="content-wrapper">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Locality</h4>
                    <form class="form-sample" action="{{url('admin/locality/store')}}" method = "post">
                        {{csrf_field()}}
                        <p class="card-description">
                            @if(Session::has('message'))
<p class="alert alert-info">{{ Session::get('message') }}</p>
@endif
                        </p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Country</label>
                                    <div class="col-sm-9">
                                        <select class="form-control "  id="country_id" name="country_id" >
                                            
                                                <option value="101" selected>India</option>
                                            
                                        </select>
                                        @if($errors->has('foodname'))
                                            <span class="text-danger">{{$errors->first('foodname')}}</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">States</label>
                                    <div class="col-sm-9">
                                        <select class="form-control"  id="state_id" name="state_id" required="">
                                            
                                            <option value="">Select</option>

                                            @foreach($states as $state)
                                            <option value="{{$state->id}}">{{$state->name}}</option>
                                            @endforeach
                                        </select>
                                       
                                    </div>
                                </div>

                                
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Cities</label>
                                    <div class="col-sm-9">
                                        <select class="form-control "  name="cities_id"  id="cities_id" required="">
                                            <option value="">Select</option>
                                        </select>
                                       
                                    </div> 
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Locality</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="locality" required="" />
                                       
                                    </div>
                                </div>

                                <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-9">
                                            <input type="submit" class="form-control btn btn-primary" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                               
                            </div>
                        
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

        <script>
            $('#country_id').change(function(){
               // alert('helo');
                var chkArray = [];
                $('#country_id option:selected').each(function(){
                    chkArray.push($(this).val());
                });
                var selected;
                selected = chkArray.join(',') ;
                $('#country_id').val(selected);
                //alert('hello');die;
                $.ajax({
                    type : 'POST',
                    url : '{{url('member/addnewoutlet/states')}}',
                    data : {id : selected, _token: '{{csrf_token()}}' },
                    success : function(res){
                        $('#state_id').html(res);

                    }
                });
            })
            $('#state_id').change(function(){
                
                var chkArray = [];
                $('#state_id option:selected').each(function(){
                    chkArray.push($(this).val());
                });
                var selected;
                selected = chkArray.join(',') ;
                $('#state_id').val(selected);
                $.ajax({
                    type : 'POST',
                    url : '{{url('member/addnewoutlet/cities')}}',
                    data : {id : selected, _token: '{{csrf_token()}}' },
                    success : function(res){
                        $('#cities_id').html(res);
                    }
                });
            })
        </script>
<script>
    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }
</script>

@endsection
