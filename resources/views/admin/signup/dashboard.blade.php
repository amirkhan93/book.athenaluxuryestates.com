@extends('layouts.myappwf')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <?php foreach($outlets as $user){?>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">

                <div class="card">

                    <div class="card-body">

                        <h6 class="font-weight-bold mb-4">{{$user->code}}</h6>
                        <div class="d-flex flex-wrap align-items-center">
                            <div>
                                <h4 class="font-weight-normal">{{$user->outlet_name}}</h4>
                                <p class="text-muted mb-0 font-weight-light"></p>
                            </div>
                            <i class="mdi mdi-account-multiple icon-lg text-primary ml-auto"></i>
                        </div>

                    </div>
                </div>

            </div>
                <?php }?>
        </div>

        <div class="row">
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                <!-- <a href="" target="_blank"> -->
                <div class="card">
                    <div class="card-body">
                        <h1 class="font-weight-light mb-4">0 {{-- count($invoice) --}}</h1>
                        <div class="d-flex flex-wrap align-items-center">
                            <div>
                                <h4 class="font-weight-normal">Total Sales</h4>
                                <p class="text-muted mb-0 font-weight-light"></p>
                            </div>
                            <i class="mdi mdi-chart-pie icon-lg text-primary ml-auto"></i>
                        </div>
                    </div>
                </div>
            <!-- </a> -->
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                <!-- <a href="" target="_blank"> -->
                <div class="card">
                    <div class="card-body">
                        <h1 class="font-weight-light mb-4">0 {{-- count($invoice) --}}</h1>
                        <div class="d-flex flex-wrap align-items-center">
                            <div>
                                <h4 class="font-weight-normal">Collected </h4>
                                <p class="text-muted mb-0 font-weight-light"></p>
                            </div>
                            <i class="mdi mdi-cash-multiple text-primary ml-auto"></i>
                        </div>
                    </div>
                </div>
            <!-- </a> -->
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                <!-- <a href="" target="_blank"> -->
                <div class="card">
                    <div class="card-body">
                        <h1 class="font-weight-light mb-4"> 0 {{-- count($invoice) --}}</h1>
                        <div class="d-flex flex-wrap align-items-center">
                            <div>
                                <h4 class="font-weight-normal">Pending</h4>
                                <p class="text-muted mb-0 font-weight-light"></p>
                            </div>
                            <i class="mdi  mdi-cash-multiple text-primary ml-auto"></i>
                        </div>
                    </div>
                </div>
            <!-- </a> -->
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                <!-- <a href="" target="_blank"> -->
                <div class="card">
                    <div class="card-body">
                        <h1 class="font-weight-light mb-4"> 0 {{-- count($invoice) --}}</h1>
                        <div class="d-flex flex-wrap align-items-center">
                            <div>
                                <h4 class="font-weight-normal">Overdue Payment</h4>
                                <p class="text-muted mb-0 font-weight-light"></p>
                            </div>
                            <i class="mdi  mdi-cash-multiple text-primary ml-auto"></i>
                        </div>
                    </div>
                </div>
            <!-- </a> -->
            </div>   
        </div>


    </div>
@endsection
