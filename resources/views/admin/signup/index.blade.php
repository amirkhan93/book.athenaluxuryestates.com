@extends('layouts.myappwf')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h1 class="font-weight-light mb-4">18834</h1>
                        <div class="d-flex flex-wrap align-items-center">
                            <div>
                                <h4 class="font-weight-normal">@lang('quickadmin.header.customers')</h4>
                                <p class="text-muted mb-0 font-weight-light">800 @lang('quickadmin.header.newcustomer')</p>
                            </div>
                            <i class="mdi mdi-account-multiple icon-lg text-primary ml-auto"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h1 class="font-weight-light mb-4">5757</h1>
                        <div class="d-flex flex-wrap align-items-center">
                            <div>
                                <h4 class="font-weight-normal">@lang('quickadmin.header.orders')</h4>
                                <p class="text-muted mb-0 font-weight-light">1600</p>
                            </div>
                            <i class="mdi mdi-chart-pie icon-lg text-primary ml-auto"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h1 class="font-weight-light mb-4">28534</h1>
                        <div class="d-flex flex-wrap align-items-center">
                            <div>
                                <h4 class="font-weight-normal">@lang('quickadmin.header.delievery')</h4>
                                <p class="text-muted mb-0 font-weight-light">760</p>
                            </div>
                            <i class="mdi mdi-car icon-lg text-primary ml-auto"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h1 class="font-weight-light mb-4">59450</h1>
                        <div class="d-flex flex-wrap align-items-center">
                            <div>
                                <h4 class="font-weight-normal">@lang('quickadmin.header.verified')</h4>
                                <p class="text-muted mb-0 font-weight-light">540 @lang('quickadmin.header.verifiedusers')</p>
                            </div>
                            <i class="mdi mdi-verified icon-lg text-primary ml-auto"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex align-items-start justify-content-between">
                            <h4 class="card-title">@lang('quickadmin.header.stastics')</h4>
                            <div class="dropdown">
                                <button class="btn btn-sm btn-outline-secondary dropdown-toggle" type="button" id="dropdownMenuDate" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    11 May 2018
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuDate">
                                    <a class="dropdown-item" href="#">12 May 2018</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">13 May 2018</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">14 May 2018</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">15 May 2018</a>
                                </div>
                            </div>
                        </div>
                        <div id="statistics-legend" class="chartjs-legend mt-2 mb-4"></div>
                        <canvas id="statistics-chart"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-md-4 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">@lang('quickadmin.header.updates')</h4>
                        <ul class="bullet-line-list">
                            <li>
                                <h6>@lang('quickadmin.header.projectrelease')</h6>
                                <p class="mb-0">@lang('quickadmin.header.duedate')</p>
                                <p class="text-muted">
                                    <i class="mdi mdi-clock-outline"></i>
                                    2 minutes ago
                                </p>
                            </li>
                            <li>
                                <h6>@lang('quickadmin.header.certification')</h6>
                                <p class="mb-0">@lang('quickadmin.header.newcourse')</p>
                                <p class="text-muted">
                                    <i class="mdi mdi-clock-outline"></i>
                                    5 minutes ago
                                </p>
                            </li>
                            <li>
                                <h6>@lang('quickadmin.header.promotion')</h6>
                                <p class="mb-0">@lang('quickadmin.header.newsite')</p>
                                <p class="text-muted">
                                    <i class="mdi mdi-clock-outline"></i>
                                    3 hours ago
                                </p>
                            </li>
                            <li>
                                <h6>@lang('quickadmin.header.leaverequest')</h6>
                                <p class="mb-0">@lang('quickadmin.header.requestapproved')</p>
                                <p class="text-muted">
                                    <i class="mdi mdi-clock-outline"></i>
                                    6 hours ago
                                </p>
                            </li>
                            <li>
                                <h6>@lang('quickadmin.header.newmember')</h6>
                                <p class="mb-0">@lang('quickadmin.header.newjionee')</p>
                                <p class="text-muted">
                                    <i class="mdi mdi-clock-outline"></i>
                                    8 @lang('quickadmin.home.dashboard') hours ago
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex align-items-start justify-content-between">
                            <h4 class="card-title">@lang('quickadmin.header.userstickets')</h4>
                            <div class="dropdown">
                                <button class="btn p-0" type="button" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <i class="mdi mdi-dots-horizontal text-primary"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton1">
                                    <a class="dropdown-item" href="#">@lang('quickadmin.header.settings')</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">@lang('quickadmin.header.detail')</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">@lang('quickadmin.header.export')</a>
                                </div>
                            </div>
                        </div>
                        <div class="preview-list">
                            <div class="preview-item pt-0">
                                <div class="preview-thumbnail">
                                    <img src="assets/images/faces/face7.jpg" alt="image" class="rounded-circle">
                                </div>
                                <div class="preview-item-content flex-grow-1">
                                    <h6>Franklin Anderson</h6>
                                    <p class="text-muted font-weight-light mb-0">6 @lang('quickadmin.header.powerfultips')</p>
                                </div>
                                <p class="mb-0">19 Jan 2018</p>
                            </div>
                        </div>
                        <div class="preview-list">
                            <div class="preview-item">
                                <div class="preview-thumbnail">
                                    <img src="assets/images/faces/face6.jpg" alt="image" class="rounded-circle">
                                </div>
                                <div class="preview-item-content flex-grow-1">
                                    <h6>Louise Horton</h6>
                                    <p class="text-muted font-weight-light mb-0">@lang('quickadmin.header.research')</p>
                                </div>
                                <p class="mb-0">27 Feb 2018</p>
                            </div>
                        </div>
                        <div class="preview-list">
                            <div class="preview-item">
                                <div class="preview-thumbnail">
                                    <img src="assets/images/faces/face9.jpg" alt="image" class="rounded-circle">
                                </div>
                                <div class="preview-item-content flex-grow-1">
                                    <h6>Eric Garcia</h6>
                                    <p class="text-muted font-weight-light mb-0">@lang('quickadmin.header.createadventrious')</p>
                                </div>
                                <p class="mb-0">03 Feb 2018</p>
                            </div>
                        </div>
                        <div class="preview-list">
                            <div class="preview-item">
                                <div class="preview-thumbnail">
                                    <img src="assets/images/faces/face10.jpg" alt="image" class="rounded-circle">
                                </div>
                                <div class="preview-item-content flex-grow-1">
                                    <h6>Amy Cole</h6>
                                    <p class="text-muted font-weight-light mb-0">@lang('quickadmin.header.whydouneeed')</p>
                                </div>
                                <p class="mb-0">25 Sep 2018</p>
                            </div>
                        </div>
                        <div class="preview-list">
                            <div class="preview-item">
                                <div class="preview-thumbnail">
                                    <img src="assets/images/faces/face8.jpg" alt="image" class="rounded-circle">
                                </div>
                                <div class="preview-item-content flex-grow-1">
                                    <h6>Russell Rodriquez</h6>
                                    <p class="text-muted font-weight-light mb-0">@lang('quickadmin.header.effective')</p>
                                </div>
                                <p class="mb-0">29 Sep 2018</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">@lang('quickadmin.header.analysis')</h4>
                        <canvas id="analysis-chart"></canvas>
                        <div class="d-lg-flex justify-content-around mt-5">
                            <div class="text-center mb-3 mb-lg-0">
                                <h3 class="font-weight-light text-success">+40.02%</h3>
                                <p class="text-muted mb-0">@lang('quickadmin.header.growth')</p>
                            </div>
                            <div class="text-center mb-3 mb-lg-0">
                                <h3 class="font-weight-light text-danger">2.5%</h3>
                                <p class="text-muted mb-0">@lang('quickadmin.header.refund')</p>
                            </div>
                            <div class="text-center">
                                <h3 class="font-weight-light text-primary">+23.65%</h3>
                                <p class="text-muted mb-0">@lang('quickadmin.header.online')</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">@lang('quickadmin.header.latest')</h4>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>@lang('quickadmin.header.name')</th>
                                    <th>@lang('quickadmin.home.startdate')</th>
                                    <th>@lang('quickadmin.header.enddate')</th>
                                    <th>@lang('quickadmin.home.status')</th>
                                    <th>@lang('quickadmin.header.assigned')</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        <h6 class="font-weight-normal">@lang('quickadmin.header.besingle')</h6>
                                        <p class="mb-0 text-muted">@lang('quickadmin.header.advertising')</p>
                                    </td>
                                    <td>
                                        13 May 2018
                                    </td>
                                    <td>
                                        19 Jun 2018
                                    </td>
                                    <td>
                                        <label class="badge badge-success">@lang('quickadmin.header.done')</label>
                                    </td>
                                    <td>
                                        Betty Howard
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h6 class="font-weight-normal">@lang('quickadmin.header.buyyoutube')</h6>
                                        <p class="mb-0 text-muted">@lang('quickadmin.header.promotewithaddress')</p>
                                    </td>
                                    <td>
                                        30 Oct 2018
                                    </td>
                                    <td>
                                        17 Sep 2018
                                    </td>
                                    <td>
                                        <label class="badge badge-primary">@lang('quickadmin.header.onhold')</label>
                                    </td>
                                    <td>
                                        Calvin Wood
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h6 class="font-weight-normal">@lang('quickadmin.header.importance')</h6>
                                        <p class="mb-0 text-muted">@lang('quickadmin.header.iilustration')</p>
                                    </td>
                                    <td>
                                        06 Dec 2018
                                    </td>
                                    <td>
                                        31 Jul 2018
                                    </td>
                                    <td>
                                        <label class="badge badge-warning">@lang('quickadmin.header.progress')</label>
                                    </td>
                                    <td>
                                        Carl Dennis
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h6 class="font-weight-normal">@lang('quickadmin.header.enlightment')</h6>
                                        <p class="mb-0 text-muted">@lang('quickadmin.header.promotional')</p>
                                    </td>
                                    <td>
                                        15 Jul 2018
                                    </td>
                                    <td>
                                        19 Nov 2018
                                    </td>
                                    <td>
                                        <label class="badge badge-danger">@lang('quickadmin.header.cancelled')</label>
                                    </td>
                                    <td>
                                        Florence Holland
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <h6 class="font-weight-normal">@lang('quickadmin.header.feedbackmgmt')</h6>
                                        <p class="mb-0 text-muted">@lang('quickadmin.header.characterstics')</p>
                                    </td>
                                    <td>
                                        04 Sep 2018
                                    </td>
                                    <td>
                                        20 Jul 2018
                                    </td>
                                    <td>
                                        <label class="badge badge-success">@lang('quickadmin.header.done')</label>
                                    </td>
                                    <td>
                                        Abbie Hughes
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div class="d-flex mt-4">
                                <nav class="ml-auto">
                                    <ul class="pagination pagination-flat pagination-primary">
                                        <li class="page-item"><a class="page-link" href="#"><i class="mdi mdi-chevron-left"></i></a></li>
                                        <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                                        <li class="page-item"><a class="page-link" href="#">4</a></li>
                                        <li class="page-item"><a class="page-link" href="#">5</a></li>
                                        <li class="page-item"><a class="page-link" href="#"><i class="mdi mdi-chevron-right"></i></a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
