<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from /template/demo/vertical-fixed/pages/samples/login-2.html  [XR&CO'2014], Tue, 30 Oct 2018 06:39:30 GMT -->
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- plugins:css -->
    <link rel="shortcut icon" href="{{asset('assets/images/favicon.png')}}" />
    <title>Admin Panel</title>
    <!-- Custom CSS -->

    <link rel="stylesheet" href="{{asset('assets/css/icons.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendors/css/vendor.bundle.base.css')}}">
    <link rel="stylesheet" href="{{asset('assets/vendors/css/vendor.bundle.addons.css')}}">
    <!-- endinject -->
    <!-- plugin css for this page -->
    <!-- End plugin css for this page -->
    <!-- inject:css -->
    <link rel="stylesheet" href="{{asset('assets/css/vertical-layout-light/style.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/themefix.css')}}">
    <link rel="stylesheet" href="{{url('local/resources/assets/plugins/css/plugins.css')}}" />

    <link
            rel="stylesheet"
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"
    />
    <link href="{{ url('local/resources/assets/css/custom.css') }}" rel="stylesheet" />
    <link href="{{ url('local/resources/assets/css/util.css')}}"rel="stylesheet" />

</head>

<body >
<div class="content-wrapper">
    <div class="col-12 grid-margin">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Signup</h4>
                <form class="form-sample" action="{{ url('/vendorregister') }}" method = "post" enctype="multipart/form-data">
                    {{csrf_field()}}
                    <p class="card-description">

                    </p>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Full Name </label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name = "name"/>
                                    @if($errors->has('name'))
                                        <span class="text-danger">{{$errors->first('name')}}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Business Name </label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name = "businessname"/>
                                    @if($errors->has('name'))
                                        <span class="text-danger">{{$errors->first('name')}}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Buisness Type  </label>
                                    <div class="col-sm-9">
                                        <select class="form-control "  name="businesstype"  id="status">
                                            <option value="">Select</option>

                                            @if($ac)
                                                @foreach($ac as $users)

                                                    <option value="{{$users->id}}">{{$users->name}}</option>
                                                  @endforeach
                                                @endif

                                        </select>
                                        @if($errors->has('org_type'))
                                            <span class="text-danger">{{$errors->first('org_type')}}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-3 col-form-label">Email  </label>
                                        <div class="col-sm-9">
                                            <input type="email" class="form-control" name = "email"/>
                                            @if($errors->has('email'))
                                                <span class="text-danger">{{$errors->first('email')}}</span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Password  </label>
                                <div class="col-sm-9">
                                    <input type="password" class="form-control" name="password"/>
                                    @if($errors->has('password'))
                                        <span class="text-danger">{{$errors->first('password')}}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row">
                                <label class="col-sm-3 col-form-label">Profile Image  </label>
                                <div class="col-sm-9">
                                    <input type="file" class="form-control" name="profile_image" required/>
                                    @if($errors->has('password'))
                                        <span class="text-danger">{{$errors->first('password')}}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                   

                    <div class="col-md-6">
                        <div class="form-group row">
                            <div class="col-sm-9">
                                <input type="submit" class="form-control btn btn-primary" />
                            </div>
                        </div>
                    </div>

                </form>

            <div class="modal fade custom-popup" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content"  style="width: 1000px !important;margin-left: -100px !important;">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Choose Your Plan</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                        @endif

                        <div class="hero-area top-head">
                            <div class="hero-area-bg-wrap">
                                <div
                                        class="hero-area-bg-pattern hero-area-bg-pattern-ultra-light"
                                ></div>
                                <div class="hero-area-grad-mask"></div>
                            </div>
                        </div>
                        <div class="hero-area plan-section">
                            <div class="hero-area-body">
                                <div class="page-section">
                                    <div class="container">


                                            <div class="">
                                                <div class="col-md-4 col-sm-6 ">
                                                    <div class="pricingTable">


                                                        <div class="pricingTable-header">
                                                            <i class="fa fa-adjust"></i>
                                                            <div class="price-value">
                                                                Price    <span class="per-month">per month</span>
                                                            </div>
                                                        </div>
                                                        <h3 class="heading">Name</h3>
                                                        <div class="pricing-content">
                                                            <ul>
                                                                <li><b>12 Month</b> Listing</li>
                                                                <li><b>Unlimited</b> Photos</li>
                                                                <li><b>Unlimited</b> Text</li>
                                                                <li><b>Owner</b> Dashboard</li>
                                                                <li><b>MONTHLY</b> REPORTING</li>
                                                                <li><b>One Calendar</b> Synchronization</li>
                                                                <li><b>-</b></li>
                                                            </ul>
                                                        </div>
                                                        <div class="pricingTable-signup"><a href="#" >select plan</a></div>

                                                    </div>
                                                </div>

                                                <div class="col-md-4 col-sm-6">
                                                    <div class="pricingTable green">
                                                        <div class="pricingTable-header">
                                                            <i class="fa fa-briefcase"></i>
                                                            <div class="price-value">
                                                               Price   <span class="per-month" >per month</span>
                                                            </div>
                                                        </div>
                                                        <h3 class="heading">Name</h3>
                                                        <div class="pricing-content">
                                                            <ul>
                                                                <li><b>12 Month</b> Listing</li>
                                                                <li><b>Unlimited</b> Photos</li>
                                                                <li><b>Unlimited</b> Text</li>
                                                                <li><b>Owner</b> Dashboard</li>
                                                                <li><b>MONTHLY</b> REPORTING</li>
                                                                <li><b>Two Calendar</b> Synchronization</li>
                                                                <li><b>ONE</b> EXTERNAL LINK</li>
                                                            </ul>
                                                        </div>
                                                        <div class="pricingTable-signup"><a href="#" >select plan</a></div>
                                                    </div>
                                                </div>

                                                <div class="col-md-4 col-sm-6">
                                                    <div class="pricingTable blue">
                                                        <div class="pricingTable-header">
                                                            <i class="fa fa-diamond"></i>
                                                            <div class="price-value">
                                                              price  <span class="per-month">per month</span>
                                                            </div>
                                                        </div>
                                                        <h3 class="heading">Name</h3>
                                                        <div class="pricing-content">
                                                            <ul>
                                                                <li><b>12 Month</b> Listing</li>
                                                                <li><b>Unlimited</b> Photos</li>
                                                                <li><b>Unlimited</b> Text</li>
                                                                <li><b>Owner</b> Dashboard</li>
                                                                <li><b>MONTHLY</b> REPORTING</li>
                                                                <li><b>Two Calendar</b> Synchronization</li>
                                                                <li><b>ONE</b> EXTERNAL LINK</li>
                                                            </ul>
                                                        </div>
                                                        <div class="pricingTable-signup"><a href="#" >select plan</a></div>
                                                    </div>
                                                </div>
                                            </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
            </div>
        </div>
    </div>



</div>
<!-- container-scroller -->
<!-- plugins:js -->
<script src="{{asset('assets/vendors/js/vendor.bundle.base.js')}}"></script>
<script src="{{asset('assets/vendors/js/vendor.bundle.addons.js')}}"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="{{asset('assets/js/off-canvas.js')}}"></script>
<script src="{{asset('assets/js/hoverable-collapse.js')}}"></script>
<script src="{{asset('assets/js/template.js')}}"></script>
<script src="{{asset('assets/js/settings.js')}}"></script>
<script src="{{asset('assets/js/todolist.js')}}"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="{{asset('assets/js/dashboard.js')}}"></script>
<!-- endinject -->
</body>


<!-- Mirrored from /template/demo/vertical-fixed/pages/samples/login-2.html  [XR&CO'2014], Tue, 30 Oct 2018 06:39:30 GMT -->
</html>
<script>
    $('#country_id').change(function(){
        var chkArray = [];
        $('#country_id option:selected').each(function(){
            chkArray.push($(this).val());
        });
        var selected;
        selected = chkArray.join(',') ;
        $('#country_id').val(selected);
        $.ajax({
            type : 'POST',
            url : '{{url('admin/signup/states')}}',
            data : {id : selected, _token: '{{csrf_token()}}' },
            success : function(res){
                $('#state_id').html(res);
            }
        });
    })
    $('#state_id').change(function(){
        var chkArray = [];
        $('#state_id option:selected').each(function(){
            chkArray.push($(this).val());
        });
        var selected;
        selected = chkArray.join(',') ;
        $('#state_id').val(selected);
        $.ajax({
            type : 'POST',
            url : '{{url('admin/signup/cities')}}',
            data : {id : selected, _token: '{{csrf_token()}}' },
            success : function(res){
                $('#cities_id').html(res);
            }
        });
    })
    function selectPlan(id){
        $('#plan_id').val(id);
        $('#exampleModal').modal('hide');
    }
    $(function () {
        $("#Aplocaltion").geocomplete({
            details: ".details",
            detailsAttribute: "data-geo"
        });
    })
</script>