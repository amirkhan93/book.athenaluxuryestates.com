@extends('layouts.myapp')
 
@section('content')
    <div class="content-wrapper">
        <div >
            <a href="{{url('admin/fooddetails/create')}}">Add New</a>
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Food Details</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>code</th>
                                    <th>Food Name</th>
                                   <!--  <th>Date</th> -->
                                    <th>Protien</th>
                                    <th>Carbs</th>
                                    <th>Grams</th>
                                    <th>Calories</th>
                                    <th>Fiber</th>
                                    <th>Fat</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php $i = 1; foreach($bst as $user) {?>
                                <tr>

                                    <td>{{ $i }}</td>
                                    <td>{{ $user->code}}</td>
                                    <td>{{ $user->foodname }}</td>
                                    <!-- <td>{{ $user->date}}</td> -->
                                    <td>{{ $user->protien}}</td>
                                    <td>{{ $user->carbs}}</td>
                                    <td>{{ $user->grams}}</td>
                                    <td>{{ $user->calories}}</td>
                                    <td>{{ $user->fiber}}</td>
                                    <td>{{ $user->fat}}</td>
                                    <td><a class="btn btn-outline-primary"
                                             href="{{ url('admin/fooddetails/foodEdit/'.$user->id) }}">Edit</a>
                                    
                                         @if($user->status != 1)
                                            <a href="{{url('admin/fooddetails/active/'.$user->id)}}"
                                               onclick="if(! confirm('Click Ok To Active ')){ return false}"
                                               class="btn btn  btn-danger" data-toggle="tooltip" >In-Active</a>
                                        @else
                                            <a href="{{url('admin/fooddetails/inactive/'.$user->id)}}"
                                               onclick="if(! confirm('Click Ok To InActivate')){ return false}"
                                               class="btn btn-success btn-sm" data-toggle="tooltip" >Active</a>
                                        @endif
                                        <a class="btn btn-outline-primary"
                                                onclick="return confirm('Are you sure?');" 
                                             href="{{ url('admin/fooddetails/destroy',[$user->id]) }}">Delete</a>

                                    </td>
                                   <!--  <td>
                                   @if($user->status != 1)
                                            <a href="{{url('admin/fooddetails/active/'.$user->id)}}"
                                               onclick="if(! confirm('Click Ok To Active ')){ return false}"
                                               class="btn btn  btn-danger" data-toggle="tooltip" >In-Active</a>
                                        @else
                                            <a href="{{url('admin/fooddetails/inactive/'.$user->id)}}"
                                               onclick="if(! confirm('Click Ok To InActivate')){ return false}"
                                               class="btn btn-success btn-sm" data-toggle="tooltip" >Active</a>
                                        @endif</td>
                                    <td> -->
                                        <!-- <button  class="btn btn-outline-primary"> -->
                                            <!-- <a class="btn btn-outline-primary"
                                                onclick="return confirm('Are you sure?');" 
                                             href="{{ url('admin/fooddetails/destroy',[$user->id]) }}">Delete</a> -->
                                        <!-- </button> -->
                                      <!--  <button  href="{{ url('admin/source/delete') }}" class="btn  btn-danger"><a href="{{ url('admin/fooddetails/delete',[$user->id]) }}">Delete</a></button>-->
                                    <!-- </td> -->
                                </tr>
                                <?php $i++;}?>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Add Popup Start -->
        <div class="modal fade custom-popup" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Set Type</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="content-wrapper">
                        <div class="col-12 grid-margin">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title">Set Type</h4>
                                    <form class="form-sample" action="{{url('admin/fooddetails/store')}}" method = "post">
                                        {{csrf_field()}}
                                        <p class="card-description">

                                        </p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label">Set Type</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" name="name"/>
                                                        @if($errors->has('name'))
                                                            <span class="text-danger">{{$errors->first('name')}}</span>
                                                        @endif
                                                    </div>
                                                </div>

                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <div class="col-sm-9">
                                                        <input type="submit" class="form-control btn btn-primary" />
                                                    </div>
                                                </div>
                                            </div>


                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!-- Add Popup Ends -->
    </div>
@endsection
