@extends('layouts.myapp')
 
@section('content')
    <div class="content-wrapper">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Food Types</h4>
                    <form class="form-sample" action="{{url('admin/fooddetails/store')}}" method = "post">
                        {{csrf_field()}}
                        <p class="card-description">

                        </p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Food Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" required="required" class="form-control" name="foodname"/>
                                        @if($errors->has('foodname'))
                                            <span class="text-danger">{{$errors->first('foodname')}}</span>
                                        @endif
                                    </div>
                                </div>

                                <!-- <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Date</label>
                                    <div class="col-sm-9">
                                        <input type="date" required="required" class="form-control" name="date"/>
                                      
                                    </div>
                                </div>
 -->
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Protien</label>
                                    <div class="col-sm-9">
                                        <input type="number" required="required" class="form-control" name="protien" step=".01" />
                                       
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Carbs</label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" name="carbs" step=".01"/>
                                       
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Fiber</label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" name="fiber" step=".01"/>
                                       
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Grams</label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" name="gram" step=".01"/>
                                       
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Calories</label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" name="calories" step=".01"/>
                                       
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Fat</label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" name="fat" step=".01"/>
                                        
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label"></label>
                                    <div class="col-sm-9">
                                        <input type="submit" class="form-control btn btn-primary" />
                                    </div>
                                </div>

                            </div>
                         <!--    <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-sm-9">
                                        <input type="submit" class="form-control btn btn-primary" />
                                    </div>
                                </div>
                            </div> -->


                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection
