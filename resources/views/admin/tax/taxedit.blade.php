@extends('layouts.myapp')
 
@section('content')
    <div class="content-wrapper">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Tax</h4>
                    <form class="form-sample" action="{{url('admin/tax/update')}}" method = "post">
                        {{csrf_field()}}
                        <p class="card-description">
                            @if(Session::has('message'))
<p class="alert alert-info">{{ Session::get('message') }}</p>
@endif
                        </p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Name</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" name="name" value = "{{$data['name']}}" required="" />
                                        <input type="hidden" class="form-control" name="id" value = "{{$data['id']}}" required="" />
                                       
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">percentage</label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" name="percent" value = "{{$data['percentage']}}" required="" />
                                       
                                    </div>
                                </div>

                                <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <div class="col-sm-9">
                                            <input type="submit" class="form-control btn btn-primary" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                               
                            </div>
                        
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </div>



@endsection
