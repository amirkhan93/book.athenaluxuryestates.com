@extends('layouts.myapp')

@section('content')

            <div class="content-wrapper">
                <div >
                   
        </div>
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Feedback</h4>
                <div class="row">
                    
                   
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Outlet</th>
                                    <th>Request Number</th>
                                    <th>Subject</th>
                                    <th>Message</th>
                                    <th>Replay</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                 
                                <?php if(!empty($bst)){
                                    $i = 1; foreach($bst as $user) {
                                        $outlet=\App\Addnewoutlet::where('id',$user['outlet_id'])->first();
                                 ?>
                                <tr>

                                    <td>{{ $i }}</td>
                                   <td>{{ $outlet['outlet_name'] }}</td>
                                    <td>  <a href="#">{{ $user['token_num'] }}</a></td>
                                    <td>{{ $user['subject'] }}</td>
                                    <td>{{ $user['message'] }}</td>
                                     <td>
                                        @if(!empty($user['reply']))
                                          {{$user['reply']}}
                                          @else
                                          ----
                                        @endif  
                                    </td>

                                     <td>
                                        @if($user['status']==0)
                                           <a class="btn btn-danger" href="{{url('admin/feedback/edit/'.$user['id'])}}"> Open</a>
                                         @else
                                            <a class="btn btn-success" href="{{url('admin/feedback/edit/'.$user['id'])}}"> Close </a>

                                         @endif  
                                 </td>
                                  
                                </tr>
                                <?php $i++;} }?>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


              
    
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script>
        $(document).ready(function(){
         
        });
    </script>
