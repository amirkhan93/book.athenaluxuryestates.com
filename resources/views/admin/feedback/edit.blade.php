@extends('layouts.myapp')

@section('content')
    <div class="content-wrapper">
        <div class="col-12 grid-margin">
            <div class="card">
                <div class="card-body">


                    <h4 class="card-title">Feedback </h4>

                    <table align="center" border="2">
                        <tr>
                     <th>Subject: </th>
                     <td>{{$ac->subject}}</td></tr>
                     <tr>
                     <th>Message: </th>
                     <td>{{$ac->message}}</td>
                     </tr>
                    </table>
                    <form class="form-sample" action="{{url('admin/feedback/update/'.$ac->id)}}" method = "post">
                        {{csrf_field()}}
                        <p class="card-description">

                        </p>
                            
                             <div class="row">
                                <div class="col-md-6">
                                <div class="form-group row">
                                    <label class="col-sm-3 col-form-label">Reply</label>
                                    <div class="col-sm-9">

                                          <textarea class="form-control" rows="5" id="comment" name="reply"></textarea>
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                         <div class="row">
                            <div class="col-md-6">
                                <div class="form-group row">
                                    <div class="col-sm-9">
                                        <input type="submit" class="form-control btn btn-primary" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>

    </div>
@endsection
