@extends('layouts.myapp')

@section('content')
    <div class="content-wrapper">
        <div class="row">
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h1 class="font-weight-light mb-4">{{count($lead)}}</h1>
                        <div class="d-flex flex-wrap align-items-center">
                            <div>
                                <h4 class="font-weight-normal">Today's Lead</h4>
                                <p class="text-muted mb-0 font-weight-light"></p>
                            </div>
                            <i class="mdi mdi-account-multiple icon-lg text-primary ml-auto"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h1 class="font-weight-light mb-4">{{count($lead)}}</h1>
                        <div class="d-flex flex-wrap align-items-center">
                            <div>
                                <h4 class="font-weight-normal">Trail Leads </h4>
                                <p class="text-muted mb-0 font-weight-light"></p>
                            </div>
                            <i class="mdi mdi-calendar text-primary ml-auto"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h1 class="font-weight-light mb-4">{{count($lead)}}</h1>
                        <div class="d-flex flex-wrap align-items-center">
                            <div>
                                <h4 class="font-weight-normal">Leads Followup</h4>
                                <p class="text-muted mb-0 font-weight-light"></p>
                            </div>
                            <i class="mdi mdi-message-processing text-primary ml-auto"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h1 class="font-weight-light mb-4">{{count($member)}}</h1>
                        <div class="d-flex flex-wrap align-items-center">
                            <div>
                                <h4 class="font-weight-normal">Members Followup</h4>
                                <p class="text-muted mb-0 font-weight-light"></p>
                            </div>
                            <i class="mdi mdi-verified icon-lg text-primary ml-auto"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h1 class="font-weight-light mb-4">{{count($member)}}</h1>
                        <div class="d-flex flex-wrap align-items-center">
                            <div>
                                <h4 class="font-weight-normal">Member Birthday</h4>
                                <p class="text-muted mb-0 font-weight-light"></p>
                            </div>
                            <i class="mdi mdi-cake-variant icon-lg text-primary ml-auto"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h1 class="font-weight-light mb-4">{{count($inactive)}}</h1>
                        <div class="d-flex flex-wrap align-items-center">
                            <div>
                                <h4 class="font-weight-normal">Upcoming Renewal </h4>
                                <p class="text-muted mb-0 font-weight-light">160 requests</p>
                            </div>
                            <i class="mdi mdi mdi-clock text-primary ml-auto"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h1 class="font-weight-light mb-4">{{count($inactive)}}</h1>
                        <div class="d-flex flex-wrap align-items-center">
                            <div>
                                <h4 class="font-weight-normal">Expired Membership</h4>
                                <p class="text-muted mb-0 font-weight-light"></p>
                            </div>
                            <i class="mdi mdi mdi-clock text-primary ml-auto"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h1 class="font-weight-light mb-4">{{count($inactive)}}</h1>
                        <div class="d-flex flex-wrap align-items-center">
                            <div>
                                <h4 class="font-weight-normal">InActive Members</h4>
                                <p class="text-muted mb-0 font-weight-light"></p>
                            </div>
                            <i class="mdi mdi-verified icon-lg text-primary ml-auto"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h1 class="font-weight-light mb-4">{{count($invoice)}}</h1>
                        <div class="d-flex flex-wrap align-items-center">
                            <div>
                                <h4 class="font-weight-normal">Total Sales</h4>
                                <p class="text-muted mb-0 font-weight-light"></p>
                            </div>
                            <i class="mdi mdi-chart-pie icon-lg text-primary ml-auto"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h1 class="font-weight-light mb-4">{{count($invoice)}}</h1>
                        <div class="d-flex flex-wrap align-items-center">
                            <div>
                                <h4 class="font-weight-normal">Collected </h4>
                                <p class="text-muted mb-0 font-weight-light"></p>
                            </div>
                            <i class="mdi mdi-cash-multiple text-primary ml-auto"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h1 class="font-weight-light mb-4">{{count($invoice)}}</h1>
                        <div class="d-flex flex-wrap align-items-center">
                            <div>
                                <h4 class="font-weight-normal">Pending</h4>
                                <p class="text-muted mb-0 font-weight-light"></p>
                            </div>
                            <i class="mdi  mdi-cash-multiple text-primary ml-auto"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-3 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h1 class="font-weight-light mb-4">{{count($invoice)}}</h1>
                        <div class="d-flex flex-wrap align-items-center">
                            <div>
                                <h4 class="font-weight-normal">Overdue Payment</h4>
                                <p class="text-muted mb-0 font-weight-light"></p>
                            </div>
                            <i class="mdi  mdi-cash-multiple text-primary ml-auto"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex align-items-start justify-content-between">
                            <h4 class="card-title">Overall Deals</h4>
                            <div class="dropdown">
                                <button class="btn btn-sm btn-outline-secondary dropdown-toggle" type="button" id="dropdownMenuDate" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    11 May 2018
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuDate">
                                    <a class="dropdown-item" href="#">24 Jan 2019</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">25 Jan 2019</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">26 Jan 2019</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#"> 27 Jan 2019</a>
                                </div>
                            </div>
                        </div>
                        <div id="statistics-legend" class="chartjs-legend mt-2 mb-4"></div>
                        <canvas id="statistics-chart"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-md-4 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Deals Closed</h4>
                        <ul class="bullet-line-list">
                            <li>
                                <h6>New Lead Added</h6>
                                <p class="mb-0">Next follow up lead</p>
                                <p class="text-muted">
                                    <i class="mdi mdi-clock-outline"></i>
                                    Status-Interested
                                </p>
                            </li>
                            <li>
                                <h6>Leads Converted</h6>
                                <p class="mb-0"></p>
                                <p class="text-muted">
                                    <i class="mdi mdi-clock-outline"></i>
                                    250
                                </p>
                            </li>
                            <li>
                                <h6>Leads Lost/Archived</h6>
                                <p class="mb-0"></p>
                                <p class="text-muted">
                                    <i class="mdi mdi-clock-outline"></i>
                                    0
                                </p>
                            </li>
                            <li>

                            <li>
                                <h6>Total Active Members</h6>
                                <p class="mb-0"></p>
                                <p class="text-muted">
                                    <i class="mdi mdi-clock-outline"></i>
                                    {{count($active)}}
                                </p>
                            </li>

                            <li>
                                <h6>Total InActive Mmebers</h6>
                                <p class="mb-0"></p>
                                <p class="text-muted">
                                    <i class="mdi mdi-clock-outline"></i>
                                    {{count($inactive)}}
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <div class="d-flex align-items-start justify-content-between">
                            <h4 class="card-title">Member Followup</h4>
                            <div class="dropdown">
                                <button class="btn p-0" type="button" id="dropdownMenuButton1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                    <i class="mdi mdi-dots-horizontal text-primary"></i>
                                </button>
                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton1">
                                    <a class="dropdown-item" href="#">Expired Membership</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Total Deals</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#">Leads</a>
                                </div>
                            </div>
                        </div>
                        @foreach($member as $user)
                        <div class="preview-list">
                            <div class="preview-item pt-0">
                                <div class="preview-thumbnail">
                                    <img src="{{url('resources/assets/images/founder-image.png')}}" alt="image" class="rounded-circle">
                                </div>
                                <div class="preview-item-content flex-grow-1">
                                    <h6>{{$user->name}}</h6>
                                    <p class="text-muted font-weight-light mb-0">{{$user->created_at}}</p>
                                </div>
                                <p class="mb-0">{{$user->business_name}}</p>
                            </div>
                        </div>
                        @endforeach
                        {{--<div class="preview-list">--}}
                            {{--<div class="preview-item">--}}
                                {{--<div class="preview-thumbnail">--}}
                                    {{--<img src="{{url('resources/assets/images/founder-image.png')}}" alt="image" class="rounded-circle">--}}
                                {{--</div>--}}
                                {{--<div class="preview-item-content flex-grow-1">--}}
                                    {{--<h6>Louis Hortzon</h6>--}}
                                    {{--<p class="text-muted font-weight-light mb-0">Review:Jan 24 2019</p>--}}
                                {{--</div>--}}
                                {{--<p class="mb-0">Assign to:-Stcakmindz</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="preview-list">--}}
                            {{--<div class="preview-item">--}}
                                {{--<div class="preview-thumbnail">--}}
                                    {{--<img src="{{url('resources/assets/images/founder-image.png')}}" alt="image" class="rounded-circle">--}}
                                {{--</div>--}}
                                {{--<div class="preview-item-content flex-grow-1">--}}
                                    {{--<h6>Eric Garcia</h6>--}}
                                    {{--<p class="text-muted font-weight-light mb-0">Review:Jan 24 2019</p>--}}
                                {{--</div>--}}
                                {{--<p class="mb-0">Assign to:-Fitness Studio</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="preview-list">--}}
                            {{--<div class="preview-item">--}}
                                {{--<div class="preview-thumbnail">--}}
                                    {{--<img src="{{url('resources/assets/images/founder-image.png')}}" alt="image" class="rounded-circle">--}}
                                {{--</div>--}}
                                {{--<div class="preview-item-content flex-grow-1">--}}
                                    {{--<h6>Amy Cole</h6>--}}
                                    {{--<p class="text-muted font-weight-light mb-0">Review:Jan 24 2019</p>--}}
                                {{--</div>--}}
                                {{--<p class="mb-0">Assign to:-Fitness Studio</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="preview-list">--}}
                            {{--<div class="preview-item">--}}
                                {{--<div class="preview-thumbnail">--}}
                                    {{--<img src="{{url('resources/assets/images/founder-image.png')}}" alt="image" class="rounded-circle">--}}
                                {{--</div>--}}
                                {{--<div class="preview-item-content flex-grow-1">--}}
                                    {{--<h6>Russell Rodriquez</h6>--}}
                                    {{--<p class="text-muted font-weight-light mb-0">Review:Jan 24 2019</p>--}}
                                {{--</div>--}}
                                {{--<p class="mb-0">Assign to:-Muscles gym</p>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
            <div class="col-md-6 grid-margin stretch-card">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Analysis</h4>
                        <canvas id="analysis-chart"></canvas>
                        <div class="d-lg-flex justify-content-around mt-5">
                            <div class="text-center mb-3 mb-lg-0">
                                <h3 class="font-weight-light text-success">+40.02%</h3>
                                <p class="text-muted mb-0">Leads Added</p>
                            </div>
                            <div class="text-center mb-3 mb-lg-0">
                                <h3 class="font-weight-light text-danger">-2.5%</h3>
                                <p class="text-muted mb-0">Expired Membership</p>
                            </div>
                            <div class="text-center">
                                <h3 class="font-weight-light text-primary">+23.65%</h3>
                                <p class="text-muted mb-0">Active Membership</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection
