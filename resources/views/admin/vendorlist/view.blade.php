@extends('layouts.myapp')

@section('content')
    <div class="content-wrapper">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Members Detail</h4>
                <div class="tab">
                    <button class="tablinks" onclick="openCity(event, 'generalinfo')">General Information</button>
                    <button class="tablinks" onclick="openCity(event, 'bankdetail')">Outlet Information</button>

                </div>
    <div id="generalinfo" class="tabcontent" class = "col-md-12" style="display:block">
        <div class="row">
            <div class="col-12">
                <div class="table-responsive">
                    <table id="order-listing" class="table">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>code</th>
                            <th>Name</th>
                            <th>Business Name</th>
                            <th>Business Type</th>
                            <th>EmailId</th>

                        </tr>
                        </thead>
                        <tbody>

                        <?php $i = 1; foreach($vendors as $user) {?>
                        <?php

                        // echo 'd';exit;
                        $supName = \App\Businesstype::where('id',$user->businesstype_id)->first();
                        $bs = \App\Businesspackage::where('id',$user->businesspackage_id)->first();
                        //  echo   $businessame;die;



                        ?>

                        <tr>

                            <td>{{ $i }}</td>
                            <td>{{$user->code}}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{$user->business_name }}</td>
                            <td>{{ $supName->name }}</td>
                            <td>{{$user->email }}</td>


                        <!--  <button  href="{{ url('admin/unit/edit') }}" class="btn btn-outline-primary"><a href="{{ url('admin/supplier/edit',[$user->id]) }}">Edit</a></button>-->
                        <!--   <button  href="{{ url('admin/source/delete') }}" class="btn  btn-danger"><a href="{{ url('admin/supplier/delete',[$user->id]) }}">Delete</a></button>-->

                        </tr>
                        <?php $i++;}?>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

                <div id="bankdetail" class="tabcontent"  class = "col-md-12" style = "display:none">
                    <div class="row">
                        <div class="col-12">
                            <div class="table-responsive">
                                <table id="order-listing" class="table">
                                    <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>code</th>
                                        <th>Address</th>
                                        <th>Outlet Name</th>
                                        <th>Created Date</th>

                                    </tr>
                                    </thead>
                                    <tbody>

                                    <?php $i = 1; foreach($outlet as $user) {?>
                                    <?php

                                    // echo 'd';exit;
                                  //  $supName = \App\Businesstype::where('id',$user->businesstype_id)->first();
                                  //  $bs = \App\Businesspackage::where('id',$user->businesspackage_id)->first();
                                    //  echo   $businessame;die;



                                    ?>

                                    <tr>

                                        <td>{{ $i }}</td>
                                        <td>{{$user->code}}</td>
                                        <td>{{ $user->street_address1}}</td>
                                        <td>{{$user->outlet_name }}</td>
                                        <td>{{ $user->created_at }}</td>


                                    <!--  <button  href="{{ url('admin/unit/edit') }}" class="btn btn-outline-primary"><a href="{{ url('admin/supplier/edit',[$user->id]) }}">Edit</a></button>-->
                                    <!--   <button  href="{{ url('admin/source/delete') }}" class="btn  btn-danger"><a href="{{ url('admin/supplier/delete',[$user->id]) }}">Delete</a></button>-->

                                    </tr>
                                    <?php $i++;}?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
    </div>

@endsection
<script>
    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }
</script>
<style>
    body {font-family: Arial;}

    /* Style the tab */
    .tab {
        overflow: hidden;
        border: 1px solid #ccc;
        background-color: #44519e;
    }

    /* Style the buttons inside the tab */
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }

    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #f5a623;
    }

    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #f5a623;
    }

    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        /* border: 1px solid #ccc;*/

        border-top: none;
    }
</style>