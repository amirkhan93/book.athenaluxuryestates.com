@extends('layouts.myapp')

@section('content')
    <div class="content-wrapper">

        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Members List for Approval</h4>
                <div class="row">
                    <div class="col-12">
                        <div class="table-responsive">
                            <table id="order-listing" class="table">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>code</th>
                                    <th>Name</th>
                                    <th>Business Name</th>
                                    <th>Business Type</th>




                                    <th>Status</th>

                                </tr>
                                </thead>
                                <tbody>

                                <?php $i = 1; foreach($vendors as $user) {?>
                              <?php

                                // echo 'd';exit;
                                $supName = \App\Businesstype::where('id',$user->businesstype_id)->first();
                                $bs = \App\Businesspackage::where('id',$user->businesspackage_id)->first();
                             //  echo   $businessame;die;



                                ?>

                                <tr>

                                    <td>{{ $i }}</td>
                                    <td>{{$user->code}}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{$user->business_name }}</td>
                                    <td>{{ $supName->name }}</td>



                                   <td>

                                        @if($user->status == 0)
                                        
                                            <a href="{{url('admin/members/active/'.$user->id)}}"
                                               onclick="if(! confirm('Click Ok To Active ')){ return false}"
                                               class="btn btn  btn-primary" data-toggle="tooltip" >Accept</a>
                                               <a href="{{url('admin/members/reject/'.$user->id)}}"
                                               onclick="if(! confirm('Click Ok To Reject ')){ return false}"
                                               class="btn btn  btn-primary" data-toggle="tooltip" >Reject</a>
                                        @elseif($user->status == 2)
                                        
                                            <a href="" class="btn btn-danger" data-toggle="tooltip" >Rejected</a>
                                        @endif
                                    </td>

                                      <!--  <button  href="{{ url('admin/unit/edit') }}" class="btn btn-outline-primary"><a href="{{ url('admin/supplier/edit',[$user->id]) }}">Edit</a></button>-->
                                     <!--   <button  href="{{ url('admin/source/delete') }}" class="btn  btn-danger"><a href="{{ url('admin/supplier/delete',[$user->id]) }}">Delete</a></button>-->

                                </tr>
                                <?php $i++;}?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
