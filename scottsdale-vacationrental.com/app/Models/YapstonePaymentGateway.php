<?php

/**
 * Payment Gateway Model
 *
 * @package     Makent
 * @subpackage  Model
 * @category    Payment Gateway
 * @author      Trioangle Product Team
 * @version     1.5.2
 * @link        http://trioangle.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class YapstonePaymentGateway extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'yapstone_payment_gateway';

    public $timestamps = false;
}
