<?php

/**
 * Property Type Model
 *
 * @package     Makent
 * @subpackage  Model
 * @category    Property Type
 * @author      Trioangle Product Team
 * @version     1.5.2
 * @link        http://trioangle.com
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'property_room';

    public $timestamps = false;

    // Get all Active status records
    public static function active_all()
    {
    	return Property::where('1=1')->get();
    }

    // Get all Active status records in lists type
    public static function dropdown()
    {
        return Property::where('1=1')->lists('room_id','id');
    }
    
}
