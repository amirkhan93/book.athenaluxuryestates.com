 $('.burger--sm').click(function() {
     $('.header--sm .nav--sm').css('visibility', 'visible');

     $('.makent-header .header--sm .nav-content--sm').addClass('right-content');
     $('.arrow-icon').toggleClass('fa-angle-down');
     $('.arrow-icon').toggleClass('fa-angle-up');
     $('.arrow-icon1').toggleClass('fa-bars');
     $('.arrow-icon1').toggleClass('fa-bars-up');
     $("body").addClass("pos-fix");
     $("body").addClass("remove-pos-fix pos-fix");
     $('.makent-header .header--sm .title--sm').toggleClass('hide');
 });
 $('.nav-mask--sm').click(function() {
     $('.header--sm .nav--sm').css('visibility', 'hidden');

     $('.makent-header .header--sm .nav-content--sm').removeClass('right-content');
     $('.arrow-icon').toggleClass('fa-angle-down');
     $('.arrow-icon').toggleClass('fa-angle-up');
     $('.arrow-icon1').toggleClass('fa-bars');
     $('.arrow-icon1').toggleClass('fa-bars-up');
     $("body").removeClass("remove-pos-fix pos-fix");
     $('.makent-header .header--sm .title--sm').toggleClass('hide');
 });
 $('.foryou').click(function() {
     $('.foryou').toggleClass('current');
     $('.homes').toggleClass('current');
 });
 $('.homes').click(function() {
     $('.foryou').toggleClass('current');
     $('.homes').toggleClass('current');
 });
 //$('#header-avatar-trigger').click(function(e){
 //e.preventDefault();
 //  e.stopPropagation();
 //$(this).unbind('mouseenter mouseleave');
 // $('.tooltip.tooltip-top-right.dropdown-menu.drop-down-menu-login').toggleClass('show');
 //   $('.tooltip.tooltip-top-right.dropdown-menu.drop-down-menu-login').toggleClass('hide');

 // });

 $('body').click(function() {
     //$('.tooltip.tooltip-top-right.dropdown-menu.drop-down-menu-login').addClass('hide');
     $('.tooltip.tooltip-top-right.dropdown-menu.drop-down-menu-login').removeClass('show');
     $('.panel-drop-down').addClass('hide-drop-down');
 });
 $('.button-sm-search').click(function(e) {
     e.stopPropagation();
     // $('.panel-drop-down').toggleClass('hide-drop-down');
     $('#search-modal--sm').removeClass('hide');
     $('#search-modal--sm').attr('aria-hidden', 'false');
 });
 $('.arrow-button').click(function(e) {
     e.stopPropagation();
     $('.panel-drop-down').toggleClass('hide-drop-down');
 });
 $(document).ready(function() {
     //$('#header-avatar-trigger').unbind('mouseenter mouseleave');

 });
 $('.home-bx-slider .bxslider').bxSlider({
     infiniteLoop: false,
     hideControlOnEnd: true,
     minSlides: 1,
     maxSlides: 3,
     slideWidth: 320,
     slideMargin: 20,
     moveSlides: 1,
     onSliderLoad: function() {
         setTimeout(function() {
             $("#lazy_load_slider").removeClass('lazy-load');
         }, 2000);
     }
 });
 start = moment();
 $('.webcot-lg-datepicker button').daterangepicker({
     startDate: start,
     minDate: start,
     dateLimitMin:{
        "days": 1
     },
     autoApply: true,
     autoUpdateInput: false,
     locale: {
         format: "MMM DD"
     },
 });
 $('.webcot-lg-datepicker button').on('show.daterangepicker', function(ev, picker) {
     $(this).parent().css('opacity', 0);
     $(this).parent().parent().find('.DateRangePickerDiv').parent().css('cssText', 'opacity: 1 !important');
 });
 $('.webcot-lg-datepicker button').on('apply.daterangepicker', function(ev, picker) {
     startDateInput = $('[name="checkin"]');
     endDateInput = $('[name="checkout"]');

     startDate = picker.startDate;
     endDate = picker.endDate;

     startDateInput.val(startDate.format('DD-MM-YYYY'));
     startDateInput.next().html(startDate.format('MMM DD'));
     endDateInput.val(endDate.format('DD-MM-YYYY'));
     endDateInput.next().html(endDate.format('MMM DD'));
 });
 $(".webcot-lg-datepicker button").on('hide.daterangepicker', function(ev, picker) {
     if (!picker.startDate || !picker.endDate) {
         $(this).parent().css('opacity', 1);
         $(this).parent().parent().find('.DateRangePickerDiv').parent().css('cssText', 'opacity: 0 !important');
     }
 });

 $(document).ready(function(){  

$('.searchButton_n8fchz').click(function(e)
{   
if($('#header-search-form').val()=='')
{
e.preventDefault();
$('.container_e4p5a8').before('<p class="set_location_error">Please set a location</p>');
}

})
$('#header-search-form').change(function(){
     $('p.set_location_error').remove();

})

$('[data-toggle="tooltip"]').tooltip(); 

 })

 