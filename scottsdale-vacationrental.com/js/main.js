(function ($) {
	"use strict";


jQuery(document).ready(function($){

	// showcase slider
	$('.showcase_slider').owlCarousel({
        loop:true,
        autoHeight: true,
        autoplay: true,
        nav: false,
        margin:0,
        responsive:{
            0:{
                items:2
            },
            600:{
                items:4
            },
            1000:{
                items:6
            }
        }
    });
$('.rio-promos').slick({
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        arrows: true,
        responsive: [{
          breakpoint: 600,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1
          }
        },
        {
           breakpoint: 400,
           settings: {
              arrows: false,
              slidesToShow: 1,
              slidesToScroll: 1
           }
        }]
    });
	//bootstrap carousel
	$('.carousel').carousel({
	    pause: true,
	    interval: 2000
	});

	// start vertical lightslider
	$('#vertical').lightSlider({
      gallery:true,
      item:1,
      vertical:true,
      verticalHeight:320,
      vThumbWidth:100,
      thumbItem:4,
      thumbMargin:15,
      enableDrag: true,
      slideMargin:0
    });

	// end vertical lightslider


	// start normal lightslider
	$('#image-gallery').lightSlider({
	      gallery:true,
	      item:1,
	      thumbItem:6,
	      slideMargin: 0,
	      thumbMargin:24,
	      verticalHeight:600,
	    	vThumbWidth:100,
	      speed:500,
	      auto:true,
	      loop:true,
	      onSliderLoad: function() {
	          $('#image-gallery').removeClass('cS-hidden');
	      }
	  });
	// end normal lightslider




})

	/** start prelaoder js **/

	$(window).load(function() { // makes sure the whole site is loaded
		$('#status').fadeOut(); // will first fade out the loading animation
		$('#loader-wrapper').delay(300).fadeOut('slow'); // will fade out the white DIV that covers the website.
		$('body').delay(350).css({'overflow-x':'hidden'});
	})

	/** end prelaoder js **/

	$('#region').on('change', function() {

      if(this.value == "Arizona"){
          $("#city").html("<option value='-1'>Select City</option><option>Phoenix</option><option>Scottsdale</option>")

      }
      else  if(this.value == "California"){
           $("#city").html("<option value='-1'>Select City</option><option>Tahoe Vista</option>")
      }
      else  if(this.value == "Texas"){
          $("#city").html("<option value='-1'>Select City</option><option>Baytown</option><option>Bolivar Peninsula</option><option>La Porte</option>")
      }
       else  if(this.value == "-1"){
          $("#city").html("<option value='-1'>Select City</option>")
           $("#property").html("<option value='-1'>Select City</option>")
      }
    });
	$('#city').on('change', function() {

      if(this.value == "Phoenix"){
          $("#property").html("<option value='-1'>Select City</option><option>The Sanctuary</option><option>Cactus Acres Home</option><option>Cactus Home</option><option>City Lights Home</option>")

      }
      else if(this.value == "Scottsdale"){
           $("#property").html("<option value='-1'>Select City</option><option>LUXURY★R </option><option>SCOTTSDALE </option><option>Casa de Encanto</option><option>Tatum Retreat Home</option><option>Hayden Manor Home</option><option>Presidio-Kierland</option><option>Delcoa Home</option>")


      }
      else if(this.value == "Tahoe Vista"){
          $("#property").html("<option value='-1'>Select City</option><option>Rustic 3 BR</option>")
      }
      else if(this.value == "Baytown"){
          $("#property").html("<option value='-1'>Select City</option><option>Old and Lost</option>")
      }
       else if(this.value == "Bolivar Peninsula"){
          $("#property").html("<option value='-1'>Select City</option><option>Sea Life's</option>")
      }
       else if(this.value == "La Porte"){
          $("#property").html("<option value='-1'>Select City</option><option>Caddo Ct Home</option>")
      }
       else if(this.value == "-1"){
          $("#property").html("<option value='-1'>Select City</option>")
      }
    });
	$('#property').on('change', function() {

      if(this.value == "The Sanctuary"){
          $(".search-form .btn").attr("href", "https://luxvacationrentalhomes.com/rooms/6");

      }
      else if(this.value == "Cactus Acres Home"){
          $(".search-form .btn").attr("href", "https://luxvacationrentalhomes.com/rooms/7");

      }
    });


}(jQuery));
