function step(e){$(".frm").hide(),$("#sf"+e).show()}function next(e){v.form()&&(11!=e?($(".frm").hide(),$("#sf"+(e+1)).show()):document.getElementById("add_room_form").submit())}function back(e){$(".frm").hide(),$("#sf"+(e-1)).show()}$("#input_dob").datepicker({dateFormat:"dd-mm-yy"});var night_value=$("#night").val(),week=$("#week").val(),month=$("#month").val(),currency_code=$("#currency_code").find("option:selected").prop("value"),v=$("#add_room_form").validate({rules:{calendar:{required:!0},bedrooms:{required:!0},beds:{required:!0},bed_type:{required:!0},bathrooms:{required:!0},property_type:{required:!0},room_type:{required:!0},accommodates:{required:!0},name:{required:!0},summary:{required:!0},country:{required:!0},address_line_1:{required:!0},city:{required:!0},state:{required:!0},night:{required:!0,digits:!0,min:1},week:{digits:!0},month:{digits:!0},video:{url:!0},"photos[]":{required:{depends:function(e){return 0==$("#js-photo-grid li").length}},extension:"png|jpg|jpeg|gif"},cancel_policy:{required:!0},user_id:{required:!0}},errorElement:"span",errorClass:"text-danger",extension:"Only png file is allowed!"});jQuery.extend(jQuery.validator.messages,{min:jQuery.validator.format("Please enter a value greater than 0")}),$.validator.addMethod("extension",function(e,t,a){return a="string"==typeof a?a.replace(/,/g,"|"):"png|jpe?g|gif",this.optional(t)||e.match(new RegExp(".("+a+")$","i"))},$.validator.format("Please upload the images like JPG,JPEG,PNG,GIF File Only.")),$(".frm").hide(),$(".frm#sf1").show(),app.controller("rooms_admin",["$scope","$http","$compile",function(e,t,a){function o(){d=new google.maps.places.Autocomplete(document.getElementById("address_line_1")),d.addListener("place_changed",n)}function n(){e.autocomplete_used=!0,r(d.getPlace())}function r(t){"street_address"==t.types&&(e.location_found=!0);var a={street_number:"short_name",route:"long_name",sublocality_level_1:"long_name",sublocality:"long_name",locality:"long_name",administrative_area_level_1:"long_name",country:"short_name",postal_code:"short_name"};$("#city").val(""),$("#state").val(""),$("#country").val(""),$("#address_line_1").val(""),$("#address_line_2").val(""),$("#postal_code").val("");var o=t;e.street_number="";for(var n=0;n<o.address_components.length;n++){var r=o.address_components[n].types[0];if(a[r]){var i=o.address_components[n][a[r]];if("street_number"==r&&(e.street_number=i),"route"==r)var d=e.street_number+" "+i;$("#address_line_1").val($.trim(d)),"postal_code"==r&&$("#postal_code").val(i),"locality"==r&&$("#city").val(i),"administrative_area_level_1"==r&&$("#state").val(i),"country"==r&&$("#country").val(i)}}var s=($("#address_line_1").val(),o.geometry.location.lat()),l=o.geometry.location.lng();""==$("#address_line_1").val()&&$("#address_line_1").val($("#city").val()),""==$("#city").val()&&$("#city").val(""),""==$("#state").val()&&$("#state").val(""),""==$("#postal_code").val()&&$("#postal_code").val(""),$("#latitude").val(s),$("#longitude").val(l)}function i(){"0"==$("#additional_guest").val()?$("#guests").prop("disabled",!0):$("#guests").prop("disabled",!1)}o(),e.location_found=!1,e.autocomplete_used=!1;var d;$("#username").autocomplete({source:APP_URL+"/admin/rooms/users_list",select:function(e,t){$("#user_id").val(t.item.id)}}),$(document).on("click",".month-nav",function(){var o=$(this).attr("data-month"),n=$(this).attr("data-year"),r={};r.month=o,r.year=n;var i=JSON.stringify(r);return t.post(APP_URL+"/admin/ajax_calendar/"+$("#room_id").val(),{data:i}).then(function(t){$("#ajax_container").html(a(t.data)(e))}),!1}),$(document).on("change","#calendar_dropdown",function(){var o=$(this).val(),n=o.split("-")[0],r=o.split("-")[1],i={};i.month=r,i.year=n;var d=JSON.stringify(i);return t.post(APP_URL+"/admin/ajax_calendar/"+$("#room_id").val(),{data:d}).then(function(t){$("#ajax_container").html(a(t.data)(e))}),!1}),$(document).on("click",".delete-photo-btn",function(){var e=$(this).attr("data-photo-id"),a=$("#room_id").val();$('[id^="photo_li_"]').size()>1?t.post(APP_URL+"/admin/delete_photo",{photo_id:e,room_id:a}).then(function(t){"true"==t.data.success&&$("#photo_li_"+e).remove()}):alert("You cannnot delete last photo. Please upload alternate photos and delete this photo.")}),$(document).on("click",".featured-photo-btn",function(){var e=$(this).attr("data-featured-id"),a=$("input[id=room_id]").val();t.post(APP_URL+"/admin/featured_image",{id:a,photo_id:e}).then(function(e){"true"==e.data.success&&alert("success")})}),$(document).on("keyup",".highlights",function(){var e=$(this).val(),a=$(this).attr("data-photo-id");$("#saved_message").fadeIn(),t.post(APP_URL+"/admin/photo_highlights",{photo_id:a,data:e}).then(function(e){$("#saved_message").fadeOut()})}),$(document).on("change","#additional_guest",function(){i()}),i()}]);function step(e) {
    $(".frm").hide(), $("#sf" + e).show()
}

function next(e) {
    v.form() && (11 != e ? ($(".frm").hide(), $("#sf" + (e + 1)).show()) : document.getElementById("add_room_form").submit())
}

function back(e) {
    $(".frm").hide(), $("#sf" + (e - 1)).show()
}
$("#input_dob").datepicker({
    dateFormat: "dd-mm-yy"
});
var night_value = $("#night").val(),
    week = $("#week").val(),
    month = $("#month").val(),
    currency_code = $("#currency_code").find("option:selected").prop("value"),
    v = $("#add_room_form").validate({
        rules: {
            calendar: {
                required: !0
            },
            bedrooms: {
                required: !0
            },
            beds: {
                required: !0
            },
            bed_type: {
                required: !0
            },
            bathrooms: {
                required: !0
            },
            property_type: {
                required: !0
            },
            room_type: {
                required: !0
            },
            accommodates: {
                required: !0
            },
            name: {
                required: !0
            },
            summary: {
                required: !0
            },
            country: {
                required: !0
            },
            address_line_1: {
                required: !0
            },
            city: {
                required: !0
            },
            state: {
                required: !0
            },
            night: {
                required: !0,
                digits: !0,
                min: 1
            },
            week: {
                digits: !0
            },
            month: {
                digits: !0
            },
            video: {
                url: !0
            },
            "photos[]": {
                required: {
                    depends: function(e) {
                        return 0 == $("#js-photo-grid li").length
                    }
                },
                extension: "png|jpg|jpeg|gif"
            },
            cancel_policy: {
                required: !0
            },
            user_id: {
                required: !0
            }
        },
        errorElement: "span",
        errorClass: "text-danger",
        extension: "Only png file is allowed!"
    });
jQuery.extend(jQuery.validator.messages, {
    min: jQuery.validator.format("Please enter a value greater than 0")
}), $.validator.addMethod("extension", function(e, t, a) {
    return a = "string" == typeof a ? a.replace(/,/g, "|") : "png|jpe?g|gif", this.optional(t) || e.match(new RegExp(".(" + a + ")$", "i"))
}, $.validator.format("Please upload the images like JPG,JPEG,PNG,GIF File Only.")), $(".frm").hide(), $(".frm#sf1").show(), app.controller("rooms_admin", ["$scope", "$http", "$compile", function(e, t, a) {
    function o() {
        d = new google.maps.places.Autocomplete(document.getElementById("address_line_1")), d.addListener("place_changed", n)
    }

    function n() {
        e.autocomplete_used = !0, r(d.getPlace())
    }

    function r(t) {
        "street_address" == t.types && (e.location_found = !0);
        var a = {
            street_number: "short_name",
            route: "long_name",
            sublocality_level_1: "long_name",
            sublocality: "long_name",
            locality: "long_name",
            administrative_area_level_1: "long_name",
            country: "short_name",
            postal_code: "short_name"
        };
        $("#city").val(""), $("#state").val(""), $("#country").val(""), $("#address_line_1").val(""), $("#address_line_2").val(""), $("#postal_code").val("");
        var o = t;
        e.street_number = "";
        for (var n = 0; n < o.address_components.length; n++) {
            var r = o.address_components[n].types[0];
            if (a[r]) {
                var i = o.address_components[n][a[r]];
                if ("street_number" == r && (e.street_number = i), "route" == r) var d = e.street_number + " " + i;
                $("#address_line_1").val($.trim(d)), "postal_code" == r && $("#postal_code").val(i), "locality" == r && $("#city").val(i), "administrative_area_level_1" == r && $("#state").val(i), "country" == r && $("#country").val(i)
            }
        }
        var s = ($("#address_line_1").val(), o.geometry.location.lat()),
            l = o.geometry.location.lng();
        "" == $("#address_line_1").val() && $("#address_line_1").val($("#city").val()), "" == $("#city").val() && $("#city").val(""), "" == $("#state").val() && $("#state").val(""), "" == $("#postal_code").val() && $("#postal_code").val(""), $("#latitude").val(s), $("#longitude").val(l)
    }

    function i() {
        "0" == $("#additional_guest").val() ? $("#guests").prop("disabled", !0) : $("#guests").prop("disabled", !1)
    }
    o(), e.location_found = !1, e.autocomplete_used = !1;
    var d;
    $("#username").autocomplete({
        source: APP_URL + "/admin/rooms/users_list",
        select: function(e, t) {
            $("#user_id").val(t.item.id)
        }
    }), $(document).on("click", ".month-nav", function() {
        var o = $(this).attr("data-month"),
            n = $(this).attr("data-year"),
            r = {};
        r.month = o, r.year = n;
        var i = JSON.stringify(r);
        return t.post(APP_URL + "/admin/ajax_calendar/" + $("#room_id").val(), {
            data: i
        }).then(function(t) {
            $("#ajax_container").html(a(t.data)(e))
        }), !1
    }), $(document).on("change", "#calendar_dropdown", function() {
        var o = $(this).val(),
            n = o.split("-")[0],
            r = o.split("-")[1],
            i = {};
        i.month = r, i.year = n;
        var d = JSON.stringify(i);
        return t.post(APP_URL + "/admin/ajax_calendar/" + $("#room_id").val(), {
            data: d
        }).then(function(t) {
            $("#ajax_container").html(a(t.data)(e))
        }), !1
    }), $(document).on("click", ".delete-photo-btn", function() {
        var e = $(this).attr("data-photo-id"),
            a = $("#room_id").val();
        $('[id^="photo_li_"]').size() > 1 ? t.post(APP_URL + "/admin/delete_photo", {
            photo_id: e,
            room_id: a
        }).then(function(t) {
            "true" == t.data.success && $("#photo_li_" + e).remove()
        }) : alert("You cannnot delete last photo. Please upload alternate photos and delete this photo.")
    }), $(document).on("click", ".featured-photo-btn", function() {
        var e = $(this).attr("data-featured-id"),
            a = $("input[id=room_id]").val();
        t.post(APP_URL + "/admin/featured_image", {
            id: a,
            photo_id: e
        }).then(function(e) {
            "true" == e.data.success && alert("success")
        })
    }), $(document).on("keyup", ".highlights", function() {
        var e = $(this).val(),
            a = $(this).attr("data-photo-id");
        $("#saved_message").fadeIn(), t.post(APP_URL + "/admin/photo_highlights", {
            photo_id: a,
            data: e
        }).then(function(e) {
            $("#saved_message").fadeOut()
        })
    }), $(document).on("keyup", ".photo__num", function() {
        var e = $(this).val(),
            a = $(this).attr("data-photo-id");
        $("#saved_message").fadeIn(), t.post(APP_URL + "/admin/photo_number", {
            photo_id: a,
            data: e
        }).then(function(e) {
            $("#saved_message").fadeOut()
        })
    }), $(document).on("change", "#additional_guest", function() {
        i()
    }), i()
}]);
