@extends('template')
<!-- Web Fonts 
	================================================== -->
	<link
		href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
		rel="stylesheet" />
	<link
		href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
		rel="stylesheet">
		<link rel="stylesheet" href="https://cdn.materialdesignicons.com/3.8.95/css/materialdesignicons.min.css">
<link href="https://fonts.googleapis.com/css?family=Mansalva&display=swap" rel="stylesheet">
	<!-- CSS
	================================================== -->
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/bootstrap.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/font-awesome.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/ionicons.min.css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/jquery.fancybox.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/owl.carousel.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/owl.transitions.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/style.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/colors/color.css" />
@section('main')

  <main id="site-content" role="main" ng-controller="inbox" class="inner_page_margin_d">
      
@include('common.subheader')

<div class="page-container-responsive page-container-inbox space-4 space-top-4">
  <div id="inbox" class="threads" ng-cloak>
    <div >
      <div class="panel-header">
        <div class="row">
          <form accept-charset="UTF-8" action="" class="col-md-4" id="inbox_filter_form" method="get"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="✓"></div>            <div class="select select-large select-block">
              <select id="inbox_filter_select" name="filter" ng-cloak>
            <option value="all" selected="selected" >{{ trans('messages.dashboard.all_messages') }} (@{{message_count.all_message_count}})</option>
<option value="starred" >{{ trans('messages.inbox.starred') }} (@{{message_count.stared_count}})</option>
<option value="unread" >{{ trans('messages.inbox.unread') }} (@{{message_count.unread_count}})</option>
<option value="reservations">{{ trans('messages.inbox.reservations') }} (@{{message_count.reservation_count}})</option>
<!-- <option value="late_or_no_response">{{ trans('messages.inbox.late_or_noresponse') }} (0)</option> -->
<option value="pending_requests">{{ trans('messages.inbox.pending_requests') }} (@{{message_count.pending_count}})</option>
<option value="hidden">{{ trans('messages.inbox.archived') }} (@{{message_count.archived_count}})</option></select>
            </div>
<input type="hidden" id="pagin_next" value= "{{ trans('messages.pagination.pagi_next') }} ">
<input type="hidden" id="pagin_prev" value= "{{ trans('messages.pagination.pagi_prev') }} ">
</form>          <div class="hide-sm col-md-8">
            <div class="pull-right">
              
            </div>
          </div>
        </div>
      </div>
<input type="hidden" ng-model="user_id" ng-init="user_id = {{ $user_id }}">
      <ul id="threads" class="list-layout messages_list">
      
          <li id="thread_153062093" class=" js-thread is-starred thread"  ng-repeat="all in message_result.data" ng-class="(all.read == '1') ? ' thread--read' : ''" ng-cloak>

  <div class="row">
    <div class="col-sm-2 col-md-2 thread-author lang-chang-label">
      <div class="row row-table">
        <div class="thread-avatar col-md-5 lang-chang-label">
          <a href="{{ url('users/show/')}}/@{{ all.user_details.id  }}"><img height="50" width="50" title="@{{ all.user_details.first_name }}" ng-src=" @{{all.user_details.profile_picture.src }}" class="media-round media-photo" alt="@{{ all.user_details.first_name }}"></a>
        </div>
        <div class=" thread-name">
          @{{ all.user_details.first_name }}
          
          <br>
          <span class="thread-date">@{{ all.created_time }}</span>
        </div>
      </div>
    </div>
      
    <a  class="col-md-7 thread-link link-reset text-muted" ng-show="all.host_check ==1 && all.reservation.status == 'Pending'"  href="{{ url('reservation')}}/@{{ all.reservation_id }}">
    
     <div class=" thread-body lang-chang-label" >&nbsp;

      <span class="thread-subject" ng-class="(all.read == '1') ? '' : 'unread_message'">
           @{{ all.message }}
        </span>

        <div class="text-muted ">

    
            <span class="street-address"  ng-if="all.rooms_address.address_line_1 || all.rooms_address.address_line_2 " ng-show="all.reservation.status == 'Accepted'">@{{ all.rooms_address.address_line_1 }} @{{ all.rooms_address.address_line_2 }},</span>             
            <span class="locality" ng-if= "all.rooms_address.city">@{{ all.rooms_address.city }}, </span> 
            <span class="region">@{{ all.rooms_address.state }}</span>
            <span>  (@{{ all.reservation.checkin | date : 'MMM dd'  }} , @{{ all.reservation.checkout | date : 'MMM dd, yyyy'  }})</span>
        </div>
      </div>
</a>  
    
    <a  class="col-md-7 thread-link link-reset text-muted" ng-show="all.host_check ==1 && all.reservation.status != 'Pending'"  href="{{ url('messaging/qt_with')}}/@{{ all.reservation_id }}">
      <div class=" thread-body lang-chang-label" >&nbsp;
        <span class="thread-subject" ng-class="(all.read == '1') ? '' : 'unread_message'">
          @{{ all.message }}
        </span>

        <div class="text-muted "> 

            <span class="street-address" ng-if="all.rooms_address.address_line_1 || all.rooms_address.address_line_2 " ng-show="all.reservation.status == 'Accepted'">@{{ all.rooms_address.address_line_1 }} @{{ all.rooms_address.address_line_2 }},</span> 
            <span class="locality" ng-if= "all.rooms_address.city">@{{ all.rooms_address.city }},</span>
             <span class="region">@{{ all.rooms_address.state }}</span>
              <span> (@{{ all.reservation.checkin | date : 'MMM dd'  }} , @{{ all.reservation.checkout | date : 'MMM dd, yyyy'  }})</span>
        </div>
      </div>
    </a>  


    <a  class="col-md-7 thread-link link-reset text-muted" ng-show="all.guest_check"  href="{{ url('z/q')}}/@{{ all.reservation_id }}">
      <div class=" thread-body lang-chang-label" >&nbsp;
      <span class="thread-subject" ng-class="(all.read == '1') ? '' : 'unread_message'">
           @{{ all.message }}
        </span>
        <div class="text-muted ">

            <span class="street-address" ng-if="all.rooms_address.address_line_1 || all.rooms_address.address_line_2 " ng-show="all.reservation.status == 'Accepted'">@{{ all.rooms_address.address_line_1 }} @{{ all.rooms_address.address_line_2 }},</span> 
            <span class="locality" ng-if= "all.rooms_address.city">@{{ all.rooms_address.city }},</span> 
            <span class="region">@{{ all.rooms_address.state }}</span>
             <span> (@{{ all.reservation.checkin | date : 'MMM dd'  }} , @{{ all.reservation.checkout | date : 'MMM dd, yyyy'  }})</span>
        </div>
      </div>
    </a>    

<div class="col-md-3 thread-label lang-chang-label text-center">
      <div class="row">
        <div  class="col-sm-12 col-md-6 acceptrej lang-chang-label" >
          <span class="label label-@{{ all.reservation.status_color }} label label-info" 
            ng-show="@{{(all.reservation.status == 'Pre-Accepted' || all.reservation.status == 'Inquiry') && all.reservation.checkin < (today | date : 'y-MM-dd') }}" >{{trans('messages.dashboard.Expired')}}</span>
            <span class="label label-@{{ all.reservation.status_color }}" 
            ng-hide="@{{(all.reservation.status == 'Pre-Accepted' || all.reservation.status == 'Inquiry') && all.reservation.checkin < (today | date : 'y-MM-dd') }}">            
          <!-- @{{ all.reservation.status }} -->  @{{ all.reservation.status }}
            </span>
            <br>

       
        </div>
        <!--<div id="options_153062093" class="col-sm-6 options thread-actions hide-sm">
          <ul class="thread-actions list-unstyled">
            <li class="row-space-1">
            <a data-thread-id="153062093" href="javascript:void(0);" class="link-icon thread-action js-star-thread ">
                <i ng-show="all.star == 1" class="icon istar_@{{ all.user_from }} icon-star icon-beach"></i>
                <i ng-show="all.star == 0" class="icon iunstar_@{{ all.user_from }} icon-star-alt icon-gray"></i>
                <span ng-show="all.star == 1" class="thread-star link-icon__text star_@{{all.user_from}}" ng-click="star($index, all.user_from,all.id,'Unstar')">{{ trans('messages.inbox.unstar') }}</span>
                <span ng-show="all.star == 0" class="thread-star link-icon__text un_star_@{{all.user_from}}" ng-click="star($index, all.user_from,all.id,'Star')">{{ trans('messages.inbox.star') }}</span>
              </a>
            </li>
            <li id="thread_153062093_hidden">
               <a data-thread-id="153062093" href="javascript:void(0);" class="link-icon thread-action js-archive-thread ">
                <i ng-show="all.archive == 1" class="icon icon-archive icon-kazan"></i>
                <i ng-show="all.archive == 0" class="icon icon-archive icon-gray"></i>
                <span  ng-show="all.archive == 0" class="link-icon__text user_from_@{{all.user_from}}"  ng-click="archive($index, all.user_from,all.id,'Archive')">{{ trans('messages.inbox.archive') }}</span>
                <span ng-show="all.archive == 1" class="link-icon__text un_user_from_@{{all.user_from}}"  ng-click="archive($index,all.user_from ,all.id,'Unarchive')">{{ trans('messages.inbox.unarchive') }}</span>
              </a>
            </li>
          </ul>
        </div>-->
      </div>
    </div>
  </div>
</li>

        </ul>

        <div class="results-footer">

  <div class="pagination-buttons-container row-space-8" ng-cloak>
    <div class="results_count" >
     <div> <p>
        <span>@{{ message_result.from }} – @{{ message_result.to }}</span><span> of</span><span> @{{ message_result.total }} </span><span>{{ trans_choice('messages.dashboard.message',2) }}</span>
      </p></div>
      <posts-pagination></posts-pagination>
    </div>
    <div>
      
    </div>
  </div>


</div>

    </div>
  </div>
</div>



    </main>
    <div class="loader">
        <div class="loader__figure"></div>
    </div>
   
  @stop
  {!! Html::script('js/jquery-1.11.3.js') !!}
  <script>
  $(window).load(function(){
    setTimeout(function(){  $('.loader').hide(); }, 1000);
  });
    </script>
  


