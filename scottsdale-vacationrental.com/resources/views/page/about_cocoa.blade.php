@extends('template')

@section('main')
<div class="bg-holder top-bg">
<div class="bg-mask"></div>
<div class="bg-content">
<div class="container">
<div class="gap"></div>
<div class="row text-center text-white">
<h1 class="page-title">About Cocoa Beach</h1>
</div>
<div class="gap"></div>
</div>
</div>
</div>
<div class="gap">
</div>
<div class="container">
<article class="post">
<div class="post-inner">
<p>Cocoa Beach is located on a barrier island in Brevard County; nestled between the Atlantic Ocean and the Banana River Lagoon on Florida&#39;s Central East Coast. Just six miles long, and mostly less than one mile wide. Cocoa Beach is truly a unique place for vacation accommodations.</p>
<p>The Orlando attractions, including Disney World, Epcot, Sea World, Universal and MGM Studios and the airport is less than an hour to the west and Daytona Beach is just over an hour&#39;s drive north of Sandcastles rentals. Melbourne and the Melbourne International Airport are only about a 30 minute drive south from Sandcastles.</p>
<p>Cocoa Beach&#39;s vacation rental accommodations are just south of Cape Canaveral and southeast of Merritt Island. The John F. Kennedy Space Center is only about 1 Mile away. Sandcastles is just minutes from Port Canaveral. Florida&#39;s fastest growing and second busiest cruise ports in the world.</p>
<p>Sandcastles is right on the strip and within walking distance of all the shopping, dining and easy beach style nightlife activities of Cocoa Beach. The Cocoa Beach Pier is located right on the beach, there is so much to do here it could take you all day and well into the night if you choose. The Cocoa Beach area is home to great theaters, theater companies, museums and historic places of interest. <strong>Stay in OUR HomeAway at Sandcastle&#39;s Condos, wake up to a spectacular sunrise on the beach and let us become YOUR HomeAway too!</strong></p>
<p>The Cocoa Beach area is a Mecca of recreational activities and features endless ways to enjoy your time. You can catch some awesome waves, water-ski, windsurf, parasail, golf, play tennis or volleyball, go deep sea fishing or sailing for the day, or visit the Brevard County Zoo.</p>
<p>While on the Space Coast, you can observe dolphins and the endangered Florida Manatee while taking a relaxing kayak tour through Cocoa Beach&#39;s beautiful Thousand Islands, view majestic cranes and wading birds while going on the Blackpoint Wildlife Drive at the Merritt Island National Wildlife Refuge, or explore the amazing hammocks of mixed tropical and temperate vegetation while hiking through a fabulous Nature Trail. The possibilities are endless when staying in charming Sandcastles condo rentals.</p>
<p>Cocoa Beach was featured in the Endless Vacation Magazine November 2006...no rides, no lines, just a beach, an island, and enough blue skies to launch your imagination along Florida&#39;s Space Coast! You&#39;ll find everything you need in Cocoa Beach and the Space Coast area to make your stay one to remember!!</p>
</div>
<h3>THE LOCAL AREA</h3>
<div class="col-md-12">
<div class="lc-area">
<div class="col-md-3">
<img src="{{url()}}/resources/assets/newHome/img/attractions-150x150.jpg" alt="Attractions">
<p>Attractions Information</p>
</div>
<div class="col-md-3">
<img src="{{url()}}/resources/assets/newHome/img/Churches-150x150.jpg" alt="Churches">
<p>Churches</p>
</div>
<div class="col-md-3">
<img src="{{url()}}/resources/assets/newHome/img/dining-150x150.jpg" alt="Dining">
<p>Dining</p>
</div>
<div class="clear"></div>
<div class="col-md-3">
<img src="{{url()}}/resources/assets/newHome/img/entertainment-150x150.jpg" alt="Entertainment">
<p>Entertainment</p>
</div>
<div class="col-md-3">
<img src="{{url()}}/resources/assets/newHome/img/golfcourses-150x150.jpg" alt="Golf Courses">
<p>Golf Courses</p>
</div>
<div class="col-md-3">
<img src="{{url()}}/resources/assets/newHome/img/nightlife-150x150.jpg" alt="Night Life">
<p>Night Life</p>
</div>
<div class="clear"></div>
<div class="col-md-3">
<img src="{{url()}}/resources/assets/newHome/img/shopping-150x150.jpg" alt="Shopping">
<p>Shopping</p>
</div>
<div class="col-md-3">
<img src="{{url()}}/resources/assets/newHome/img/evenmore-150x150.jpg" alt="Even More">
<p>Even More</p>
</div>
<div class="col-md-3">
<img src="{{url()}}/resources/assets/newHome/img/cruisesandgambling-150x150.jpg" alt="Cruises and Gambling">
<p> Cruises and Gambling</p>
</div>
<div class="clear"></div>
</div>
</div>
<h5>Cocoa Beach Pier</h5>
<p><img alt="Cocoa Beach Pier" src="{{url()}}/resources/assets/newHome/img/cocoabeachpier.jpg" style="width: auto;margin: 20px 0;" /></p>
<h5>Cocoa Beach Wind Surfing</h5>
<p><img alt="Cocoa Beach Wind Surfing" src="{{url()}}/resources/assets/newHome/img/cocoabeachwindsurfing.jpg" style="width: auto;margin: 20px 0;" /></p>
</article>
</div>
@stop