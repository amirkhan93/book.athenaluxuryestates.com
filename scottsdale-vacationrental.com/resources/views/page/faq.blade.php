@extends('template')

@section('main')
<div class="container faq-pan">
<h1 class="page-title">Frequently Asked Questions</h1>
<h3>Apartment & Complex</h3>
<h5><i class="fa fa-hand-o-right"></i> Is there Internet Access?</h5><ul>
<li>Yes. The condo has a PRIVATE WIRELESS Internet connection.</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i>  What are the activities at the Sandcastles Resort Complex?</h5><ul>
<li>Large Heated Swimming Pool (open 9am – 10pm all week)</li>
<li>Heated Hot Tub</li>
<li>Night-lit Tennis Courts (tennis racquets & balls provided in the condo)</li>
<li>Ground Floor Recreation Room with TV, Bathrooms, Full Showers, and Sauna</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> Is there ample parking for cars at Sandcastles Resort Complex?</h5><ul>
<li>Yes! There is plenty of parking on this 7 acre complex.</li>
<li>There are no assigned parking places. You can park anywhere.</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> Are Boats, RVs, or Large Trucks permitted in the parking lot?</h5><ul>
<li>No. Boats & RVs are not permitted at anytime.</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> Is there an Elevator at Sandcastles Resort Complex?</h5><ul>
<li>Yes, with a coded entry for guest security.</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> Do I need to bring any towels or linens?</h5><ul>
<li>No. They are provided for you.</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> Does the Unit have a hair dryer?</h5><ul>
<li>Yes, 2…one in each bathroom</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> Is there a washer and dryer?</h5><ul>
<li>There is a Private Full Laundry room in each Condo Apartment Unit which has a full size Washer & Dryer--laundry detergent is also provided.</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> Is there a grill on the patio?</h5><ul>
<li>Yes. An electric grill can be provided under certain conditions.</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> Are there pots and pans in the condo?</h5><ul>
<li>Yes, Wolfgang Puck Cookware & Chopping Knives, bake ware, pizza pans, glassware, stemware, silverware, utensils, hand mixer, coffee maker, microwave, etc.</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> Does the Condo Unit have a dishwasher?</h5><ul>
<li>Yes, each Condo Apartment Unit is equipped with a dishwasher – dish detergent is also provided.</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> Is there shopping and restaurants nearby?</h5><ul>
<li>See the “Things to Do” and “Area Map”</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> How many TVs are in the condo?</h5><ul>
<li>The condo has televisions in the living room (with surround sound), the master bedroom and the guest bedroom(s) with cable throughout.</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> Is there a DVD?</h5><ul>
<li>No.</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> Are there portable beds, inflatable beds, or roll-away beds?</h5><ul>
<li>Yes. A self-inflating bed and bedding is available under certain conditions.</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> Do you provide any coolers?</h5><ul>
<li>Yes, the condo is equipped with two medium-sized coolers and plastic ice packs.</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> Is there any beach equipment in the condo we can take down to the beach?</h5><ul>
<li>Yes. There are 2 beach chairs, 2 boogie-boards, 1 large beach umbrella, bamboo mats, beach towels, racquet games, etc. in the condo’s beach closet.</li>
</ul>
<h3>Reservations</h3>                         
<h5><i class="fa fa-hand-o-right"></i> Are Credit Cards accepted?</h5><ul>
<li>Yes. Visa, MasterCard, American Express, and Discover are accepted.</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> ARE DEBIT CARDS ACCEPTED?</h5><ul>
<li>Yes</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> ARE CHECKS ACCEPTED?</h5><ul>
<li>No.*</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> ARE MONEY ORDERS ACCEPTED?</h5><ul>
<li>No. *</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> ARE MONEY ORDERS ACCEPTED?</h5><ul>
<li>No. *</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> ARE CASHIER’S CHECKS OR CERTIFIED CHECK ACCEPTED?</h5><ul>
<li>No. *</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> WHY DON’T YOU TAKE CHECKS OR MONEY ORDERS?</h5><ul>
<li>* It slows down the confirmation of the reservation; as well, it increases the cost of labor.</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> IS THERE EVER AN EXCEPTION?</h5><ul>
<li>Yes, with conditions. No personal checks. Only bank checks, cashiers checks or money orders A bank check, cashiers check or money order will be accepted if it is sent overnight express at your expense. Write the tracking number in the comments section during the online booking process. Cashiers checks, bank checks, money orders must be overnight express mailed and Payable To: Diamond Properties, PO Box 895533, Leesburg, FL 34789. Your reservation is not guaranteed until the payment has arrived and the dates are still available when the payment arrives.</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> DO YOU OFFER LONG TERM RENTALS AT A REDUCED RATE?</h5><ul>
<li>Yes, during slow season…between September and February.</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> HOW DO I RESERVE A CONDO UNIT?</h5><ul>
<li>Goto the property page and click on calendar dates to book ( 611 and 715 ) .</li>
<li>Call 352-552-4273</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> DO YOU REQUIRE A SECURITY DEPOSIT?</h5><ul>
<li>Yes. $500 (3 Bedroom Unit 611) and $300 (2 Bedroom Unit 715) which is refunded within 1 week of your departure.</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> WHAT IF WE NEED TO CANCEL, WILL WE GET OUR MONEY BACK?</h5><ul>
<li>We recommend purchasing Travel Insurance: InsureMyTrip . Any cancellation caries with it a fee of $100 for the cost of credit card processing and administration. If the cancellation should occur 60 days prior to the arrival date, all but the $100 is refunded. If the cancellation should occur after 60 days, the amount of the refund is determined based on the re-rental.</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> WHAT HAPPENS IN THE EVENT OF A HURRICANE?</h5><ul>
<li>If evacuation is necessary, a credit would be provided for the dates you either have booked or the days left of your stay. This credit will be applied toward your re-booking. Re-booking will be based upon availability.</li>
</ul>
<h3>Check In & Check Out</h3>                        
<h5><i class="fa fa-hand-o-right"></i> How do I access the unit?</h5><ul>
<li>There is a lock box affixed to the front door jamb of the condo. Punch in the code that will be provided to you to unlock the box.</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> Why do you rent Saturday to Saturday</h5><ul>
<li>Saturday to Saturday is by far, the most requested one week stay. For example, if we were to rent out from Tuesday to Tuesday it would be highly unlikely we could fill in the gaps. The result would be a one week rental that took up the time of two full weeks.</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> Is there a maid service? </h5><ul>
<li>No. When you arrive your condo will be clean, laundered and ready to go but there is no daily maid service.</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> When is check in time and check out time?</h5><ul>
<li>Check in time is 4pm. Check out time is 10am.</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> Are there exceptions to check in and check out times?</h5><ul>
<li>Yes, but rarely. During slow season, we will make exceptions, but it must be in writing from Diamond Properties.</li>
</ul>
<h5><i class="fa fa-hand-o-right"></i> Who do we call if we need any type of service when we are in Florida?</h5><ul>
<li>The owner, Jill Sandcastles. My cell phone is on 24/7 and if you leave a message and if it’s of an urgent nature, I would not be inconvenienced if you called again within an hour.</li>
</ul>
</div>

@stop