@extends('template')

@section('main')
<div class="bg-holder top-bg excursion">
<div class="bg-mask"></div>
<div class="bg-content">
<div class="container">
<div class="gap"></div>
<div class="row text-center text-white">
<h1 class="page-title">A Day’s Excursion</h1>
</div>
<div class="gap"></div>
</div>
</div>
</div>
<div class="gap">
</div>
<div class="container">
<article class="post">
<div class="post-inner">
<h3 style="font-family: 'EdwardianScriptITC';color: #0054B4;font-size: 55px;">An Interesting Day&#39;s Experience.</h3>
<h4>Plan Your Own Special Progressive Dinner</h4>
<h5>Take the Monorail at Downtown Disney</h5>
<ul>
<li>Free Parking</li>
<li>Monorail Free</li>
</ul>
<h4>Board Monorail for Resort Hotels:</h4>
<p>Contemporary Hotel</p>
<p>Epcot Fireworks Display (After 8pm)</p>
<p>Have Dessert and Cocktails at The Contemporary. Time it for 8:00pm so you can see the Fireworks.</p>
<p>Plan dinner at The Polynesian Resort or The Grand Floridian. The Grand Floridian has many different restaurants, depending on your budget from</p>
<h5>Victoria & Albert</h5>
<ul>
<li>Pricey, but so worth it</li>
<li>Reservations required</li>
<li>Jacket & Dress</li>
</ul>
<h5>Monorail</h5>
<p><img alt="Monorail" src="{{url()}}/resources/assets/newHome/img/monorail.jpg" style="width: auto;margin: 20px 0;" />
<h4>Plan your day by going to the Disney Monorail Resort Train.</h4>
<h5>Make a Reservation in the:</h5>
<ul>
<li><a href="https://disneyworld.disney.go.com/resorts/grand-floridian-resort-and-spa" target="_blank">Grand Floridian</a> (407) 824-3000</li>
<li><a href="https://disneyworld.disney.go.com/resorts/polynesian-resort/" target="_blank">The Polynesian</a> (407) 824-2000</li>
<li><a href="https://disneyworld.disney.go.com/resorts/contemporary-resort/" target="_blank">The Contemporary</a> (407) 824-1000</li>
</ul>
</p>
<p>This was recommended to me by a Chef. We did this and it was so much fun; we took the Monorail from the Contemporary to the Grand Floridian, had dinner at Victoria & Albert&#39;s $$$$, early seating. There are many restaurants in the Grand Floridian to choose from at all price ranges.</p>
<p>I would start early, between 2:00 and 3:00 so you can walk around this fabulous Resort. After dinner board the Monorail, stop at the Polynesian for dessert, etc. Walk around the Resort. Then board the Monorail and go back to the Contemporary, should be around 7:00 or 8:00pm, have cocktails and watch the fabulous firework displays.</p>
<p>Disney’s iconic monorail is a fast—and fun— way to get around Walt Disney World Resort. There are 2 different monorail routes.</p>
<p>The first route makes stops at the following locations:</p>
<p>Magic Kingdom park Disney’s Contemporary Resort Transportation and Ticket Center (adjacent to Magic Kingdom parking) Disney&#39;s Polynesian Village Resort Disney&#39;s Grand Floridian Resort & Spa Please note that the above route has 2 lines. The Magic Kingdom line shuttles between Magic Kingdom Park and the Transportation and Ticket Center. The Resort Hotels line stops at all of the above locations</p>
<p><strong>This would be an amazing day!!</strong></p>
<h3 style="font-family: 'EdwardianScriptITC';color: #0054B4; font-size: 55px;">A Day at NASA and Cape Canaveral</h3>
<p><strong>An "Out of This World" tour at NASA</strong></p>
<p>The Kenendy Space Center Visitor Complex was awarded the Trip Advisor "Certificate of Excellence" in 2016.</p>
<p>You can purchase admission tickets or review the tour attractions at <strong>Kennedy Space Center Tour</strong></p>
<p>A tentative calendar of events including Rocket Launches at Cape Canaveral can be accessed at <strong>Events Calendar.</strong></p>
<p>A list of things to do and see in Cape Canaveral can be accessed at <strong>Visit Cape Canaveral</strong></p>
<h5>NasaClose</h5>
<p><img alt="Monorail" src="{{url()}}/resources/assets/newHome/img/nasaclose.jpg" style="width: auto;margin: 20px 0;" /></p>
</div>
</article>
</div>
@stop