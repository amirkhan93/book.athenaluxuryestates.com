@extends('template')

@section('main')
<div class="container text-justify">
<h1 class="page-title">About Owner</h1>
<h3><strong>Why This Website Was Established</strong></h3>

<p>This Website was established to serve those travelers that prefer to deal with Owners Directly, rather than the larger Vacation Rental Companies. We feel that a Service Fee should not be charged to Book your Vacation. We believe in being available to our guests from the start of the booking process through departure and making sure each property is cleaned and prepared for each arrival.</p>

<h3><strong>About Sandcastles Complex</strong></h3>

<p>This Vacation Rental is located Directly on Cocoa Beach, Florida. These Vacation Condo&rsquo;s are just<br />
45 minutes from Disney World. We are the closest Beach to Disney &amp; Epcot Resorts.<br />
The Beach is privately entered directly from our Complex.<br />
When you stay in this relaxing Complex, you can enjoy a dip in our beautiful Resort-Style Pool (heated in Winter), or soak in our Jacuzzi (Hot Tub), Play Tennis on our Tennis Court (which can be lit by night).<br />
Our Club House has a small Sauna, Shower, TV, Barbeque, and tables.<br />
Our Complex also has,&nbsp;<strong>Wi-Fi and Internet Connection</strong>&nbsp;in Each Condo On The Complex.<br />
We have Elevators, and a Large Parking Lot so we don&rsquo;t need assigned parking.</p></div>
<div class="gap"></div>

@stop