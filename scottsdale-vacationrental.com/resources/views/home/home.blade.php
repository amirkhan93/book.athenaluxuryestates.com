<!DOCTYPE html>
<html lang="en">


<head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>The Guilt Trip & Golfers Paradise</title>
<meta name="description" content="Welcome to Sandcastles at Cocoa Beach. We pride ourselves on Booking Your Vacation Directly with the Owner. There is NO service fee for booking on our site." />


<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="icon" href="img/fav.png" sizes="16x16">
<!-- GOOGLE FONTS -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600" rel="stylesheet" type="text/css">
<!-- /GOOGLE FONTS -->
<link rel="stylesheet" href="{{url()}}/resources/assets/newHome/css/font-awesome.min.css">
<link rel="stylesheet" href="{{url()}}/resources/assets/newHome/css/bootstrap.css">
<link rel="stylesheet" href="{{url()}}/resources/assets/newHome/css/styles.css">
<script src="{{url()}}/resources/assets/newHome/js/modernizr.js"></script>
<script src="{{url()}}/resources/assets/newHome/js/jquery.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Pacifico&display=swap" rel="stylesheet">
</head>
<body>
<div class="global-wrap">
@include('common/headertwo')

<div class="top-area show-onload main-slider">
<div class="bg-holder full" id="vidid">
<div class="bg-mask"></div>
<div class="vimeo-wrapper" style="height: 80vh !important;">
            <iframe src="https://player.vimeo.com/video/375387322?background=1&autoplay=1&loop=1&byline=0&title=0"
                    frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
	</div>

	<div class="caption_content">
		<h1>
		Luxury Home - The Guilt Trip & Golfers Paradise
		</h1>
	</div>
	<!-- <div id="carousel" class="carousel slide carousel-fade" data-ride="carousel">
		<ol class="carousel-indicators">
		<li data-target="#carousel" data-slide-to="0" class="active"></li>
		<li data-target="#carousel" data-slide-to="1"></li>
		<li data-target="#carousel" data-slide-to="2"></li>
		<li data-target="#carousel" data-slide-to="3"></li>
		<li data-target="#carousel" data-slide-to="4"></li>
		</ol>
		<div class="carousel-inner">
			<div class="item active">
			<img src="{{url()}}/resources/assets/newHome/img/slider/3.jpg" alt="Slider">
			</div>
			<div class="item">
			<img src="{{url()}}/resources/assets/newHome/img/slider/5.jpg" alt="Slider">
			</div>
			<div class="item">
			<img src="{{url()}}/resources/assets/newHome/img/slider/2.jpg" alt="Slider">
			</div>
			<div class="item">
			<img src="{{url()}}/resources/assets/newHome/img/slider/4.jpg" alt="Slider">
			</div>
			<div class="item">
			<img src="{{url()}}/resources/assets/newHome/img/slider/6.jpg" alt="Slider">
			</div>
			</div>
		<a class="carousel-control left" href="#carousel" data-slide="prev">&lsaquo;</a>
		<a class="carousel-control right" href="#carousel" data-slide="next">&rsaquo;</a>
		</div>
	</div> -->

<div class="scroll-down">
<span><img src="{{url()}}/resources/assets/newHome/img/d-arrow-down.png" alt="Scroll"></span>
<a href=".html" class="crunchify-bottom bounce" id="move-down" style="display: block; opacity: 1;"><!--<i class="fa fa-arrow-down" aria-hidden="true"></i>--></a>
</div>
</div>
<div class="container pro-list"  id="lazyme">
<div class="gap"></div>
<!--<h1 class="text-center mb20">Our Properties</h1>-->
<!--<div class="row row-wrap">-->
<!--<div class="col-md-4">-->
<!--<div class="thumb">-->
<!--<header class="thumb-header">-->
<!--<a class="hover-img fp_img" href="https://sandcastlescocoabeachrentals.com/rooms/10081">-->
<!--<img src="{{url()}}/resources/assets/newHome/uploads/1/FRIEND_1503719234.jpg" alt="" />-->
<!--<h5 class="hover-title-center">View Now</h5>-->
<!--</a>-->
<!--</header>-->
<!--<div class="thumb-caption">-->
<!--<h5 class="thumb-title"><a class="text-darken" href="#">611 - Truly Direct Oceanfront 3 Bedroom 2 Bath</a></h5>-->
<!--<p class="mb0"><small><i class="fa fa-home"></i> Type : Condominium</small>-->
<!--</p>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--<div class="col-md-4">-->
<!--<div class="thumb">-->
<!--<header class="thumb-header">-->
<!--<a class="hover-img fp_img" href="https://sandcastlescocoabeachrentals.com/rooms/10082">-->
<!--<img src="{{url()}}/resources/assets/newHome/uploads/2/711Balcony.8.30.17_1504115394.jpg" alt="" />-->
<!--<h5 class="hover-title-center">View Now</h5>-->
<!--</a>-->
<!--</header>-->
<!--<div class="thumb-caption">-->
<!--<h5 class="thumb-title"><a class="text-darken" href="#">Unit 711 Breathtaking View</a></h5>-->
<!--<p class="mb0"><small><i class="fa fa-home"></i> Type : Condominium</small>-->
<!--</p>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--<div class="col-md-4">-->
<!--<div class="thumb">-->
<!--<header class="thumb-header">-->
<!--<a class="hover-img fp_img" href="https://sandcastlescocoabeachrentals.com/rooms/10083">-->
<!--<img src="{{url()}}/resources/assets/newHome/uploads/3/10.jpg" alt="" />-->
<!--<h5 class="hover-title-center">View Now</h5>-->
<!--</a>-->
<!--</header>-->
<!--<div class="thumb-caption">-->
<!--<h5 class="thumb-title"><a class="text-darken" href="#">Ocean View Immaculate - 2 Bedroom 2 Bath - Unit 715</a></h5>-->
<!--<p class="mb0"><small><i class="fa fa-home"></i> Type : Condominium</small>-->
<!--</p>-->
<!--</div>-->
</div>
</div>
<!--<div class="col-md-4">-->
<!--<div class="thumb">-->
<!--<header class="thumb-header">-->
<!--<a class="hover-img fp_img" href="property-details931c.html?property=4">-->
<!--<img src="uploads/4/Picture188s_1508587133.jpg" alt="" />                        -->
<!--<h5 class="hover-title-center">View Now</h5>-->
<!--</a>-->
<!--</header>-->
<!--<div class="thumb-caption">-->
<!--<h5 class="thumb-title"><a class="text-darken" href="#">410 - Sandcastles Direct Ocean front Corner condo</a></h5>-->
<!--<p class="mb0"><small><i class="fa fa-home"></i> Type : Direct Oceanfront Condo</small>-->
<!--</p>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
<!--<div class="col-md-4">-->
<!--<div class="thumb">-->
<!--<header class="thumb-header">-->
<!--<a class="hover-img fp_img" href="property-details88c4.html?property=5">-->
<!--<img src="uploads/5/1_1502482340.jpg" alt="" />                        -->
<!--<h5 class="hover-title-center">View Now</h5>-->
<!--</a>-->
<!--</header>-->
<!--<div class="thumb-caption">-->
<!--<h5 class="thumb-title"><a class="text-darken" href="#">615 Sandcastles Side Ocean View, Cocoa Beach Florida</a></h5>-->
<!--<p class="mb0"><small><i class="fa fa-home"></i> Type : Condo</small>-->
<!--</p>-->
<!--</div>-->
<!--</div>-->
<!--</div>-->
</div>
<div class="gap gap-small"></div>
</div>
<div class="bg-holder text-center">
<div class="bg-mask"></div>
<div class="bg-img" style="background-image:url({{url()}}/resources/assets/newHome/img/background.jpg);"></div>
<div class="bg-content">
<div class="container">
<div class="gap"></div>
<div class="row">
<div class="col-md-12">
<div class="col-md-10 bg-white padding20" style="float:none;margin:0 auto;padding-top:40px;padding-bottom:40px">
<article>
<div class="field field-name-body homePage_about">
<h2 class="m-title">Why Book<em>With</em>Owner Directly?</h2>
<h4>There is a Difference</h4>

<h4><em>NO Service Fee for booking!</em></h4>

<h4>This IS&nbsp;<strong>TRULY</strong>&nbsp;an Owner Direct Website</h4>

<p><em>*Other sites charge as much as 12% &quot;service fee&quot; for booking!*</em></p>

<p>Welcome to Sunshine properties.</p>

<p>Sunshine Properties attracts the out of state business travelers and the leisure travelers who are seeking something different. Our company pre-screens tenants for identification, and they will provide a deposit prior to their visit. </br>

The individuals are 30+ years old, no pets, no smoking allowed, and they typically stay 1-3 nights.</br>

There are no additional fees, charges, hidden cots for allowing us to lease your property. Sunshine Properties Unlimited will sign a 6, 12, or 24 month lease and provide a deposit like a tenant would.</p></div>
</article>
</div>
</div>
</div>
<div class="gap-small gap"></div>
</div>
</div>
</div>
<div class="gap"></div>
<div class="things-todo container">
<h2 class="m-title text-center mb20">Things To Do</h2>
<div class="gap">
<div class="row row-wrap">
<div class="col-md-6">
<div class="thumb">
<header class="thumb-header">
<a class="hover-img curved" href="https://scottsdale-vacationrental.com/top_things_in_scottsdale">
<img src="{{url()}}/resources/assets/newHome/img/scottsdale.jpg" alt="Cocoa Beach" title="Cocoa Beach" /><i class="fa fa-plus box-icon-white box-icon-border hover-icon-top-right round"></i>
</a>
</header>
<div class="thumb-caption">
<h4 class="thumb-title">Scottsdale</h4>
<p class="thumb-desc"><a href="https://scottsdale-vacationrental.com/top_things_in_scottsdale">Click here</a> to know more about Scottsdale.</p>
</div>
</div>
</div>
<div class="col-md-6">
<div class="thumb">
<header class="thumb-header">
<a class="hover-img curved" href="https://scottsdale-vacationrental.com/days_excursion">
<img src="{{url()}}/resources/assets/newHome/img/day.jpg" alt="A Day’s Excursion" title="A Day’s Excursion" /><i class="fa fa-plus box-icon-white box-icon-border hover-icon-top-right round"></i>
</a>
</header>
<div class="thumb-caption">
<h4 class="thumb-title">A Day’s Excursion</h4>
<p class="thumb-desc"><a href="https://scottsdale-vacationrental.com/days_excursion">Click here</a> to know more about A Day’s Excursion.</p>
</div>
</div>
</div>
</div>
</div>
</div>
<div class="container contact-pan">
<div class="gap gap-small"></div>
<div class="gap gap-small"></div>
<div class="row row-wrap" data-gutter="60">
<div class="col-md-4">
<div class="thumb"><i class="fa fa-phone box-icon-left round box-icon-normal box-icon-black animate-icon-top-to-bottom"></i>
<div class="thumb-caption">
<h5 class="thumb-title">Call Us</h5>
<p class="thumb-desc">+1 (877) 589 6274<br />
</p>
</div>
</div>
</div>
<div class="col-md-4">
<div class="thumb"><i class="fa fa-envelope-o box-icon-left round box-icon-normal box-icon-black animate-icon-top-to-bottom"></i>
<div class="thumb-caption">
<h5 class="thumb-title">Email</h5>
<p class="thumb-desc">presidiostay@gmail.com<br /></p>
</div>
</div>
</div>
<div class="col-md-4">
<div class="thumb"><i class="fa fa-map-marker box-icon-left round box-icon-normal box-icon-black animate-icon-top-to-bottom"></i>
<div class="thumb-caption">
<h5 class="thumb-title">Location</h5>
<p class="thumb-desc">Scottsdale, AZ<br /></p>
</div>
</div>
</div>
</div>
</div>
@include('common/footertwo')
<script>
function refreshCaptcha()
{
var img = document.images['captchaimg'];
img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
}
</script>
<!-- <script src="../cdnjs.cloudflare.com/ajax/libs/mootools/1.5.1/mootools-core-full-compat.min.js" type="text/javascript"></script>-->
<script  src="{{url()}}/resources/assets/newHome/js/bootstrap.js"></script>
<script  src="{{url()}}/resources/assets/newHome/js/slimmenu.js"></script>
<script  src="{{url()}}/resources/assets/newHome/js/bootstrap-datepicker.js"></script>
<script  src="{{url()}}/resources/assets/newHome/js/bootstrap-timepicker.js"></script>
<!--  <script  src="js/LazyLoad.js" type="text/javascript"></script>-->
<script  src="{{url()}}/resources/assets/newHome/js/dropit.js"></script>
<script  src="{{url()}}/resources/assets/newHome/js/icheck.js"></script>
<script  src="{{url()}}/resources/assets/newHome/js/fotorama.js"></script>
<script  src="{{url()}}/resources/assets/newHome/js/typeahead.js"></script>
<script  src="{{url()}}/resources/assets/newHome/js/magnific.js" ></script>
<!-- <script  src="js/fitvids.js"   type="text/javascript"></script>
<script  src="js/gridrotator.js" type="text/javascript"></script>-->
<!--<script src="//www.exoticholidayrentals.com/translator/javascript/jquery.translator.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/bootstrapValidator.min.js"></script>-->
<script src="{{url()}}/resources/assets/newHome/js/index.js"></script>
<script>
$(document).ready(function(e) {
$("#scroller").simplyScroll();
});
</script>
<script src="{{url()}}/resources/assets/newHome/js/custom.js"></script>
<script>
$(document).on('click', '#move-down', function(event){
event.preventDefault();
$('html, body').animate({scrollTop: $( $.attr(this, 'href') ).offset().top}, 500);
//$('.book-section').css("margin-top","30px");
});
</script>
</div>
<link href="{{url()}}/resources/assets/newHome/datepicker/jquery-ui.css" rel="Stylesheet" type="text/css" />
<script src="{{url()}}/resources/assets/newHome/datepicker/jquery-ui.js"></script>
<script>
$(function(){
$("#arri").datepicker({
minDate: 0 ,
changeMonth: true,
changeYear: true,
numberOfMonths: 1,
onClose: function( selectedDate ) {
$("#deprt").datepicker( "option", "minDate", selectedDate );
}
});
$("#deprt").datepicker({
minDate: 0,
changeMonth: true,
changeYear: true,
numberOfMonths: 1,
onClose: function( selectedDate ) {
$("#arri").datepicker( "option", "maxDate", selectedDate );
}
});
});
</script></body>

</html>
