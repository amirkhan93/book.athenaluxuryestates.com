  <style type="text/css">
    .btn-large {
    padding: 20px 27px !important;
    }
  </style>
  @extends('template')
   <!-- Web Fonts 
	================================================== -->
	<link
		href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
		rel="stylesheet" />
	<link
		href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
		rel="stylesheet">
		<link rel="stylesheet" href="https://cdn.materialdesignicons.com/3.8.95/css/materialdesignicons.min.css">
<link href="https://fonts.googleapis.com/css?family=Mansalva&display=swap" rel="stylesheet">
	<!-- CSS
	================================================== -->
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/bootstrap.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/font-awesome.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/ionicons.min.css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/jquery.fancybox.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/owl.carousel.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/owl.transitions.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/style.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/colors/color.css" />

	@section('main')
	
    <main id="site-content" role="main" style="" class="login_bg">
      <div class="overlay"></div>

<div class="page-container-responsive page-container-auth margintop login_container" style="margin-top:20px">
  <div class="row">
    
        <div class="log_pop col-center">
          <div class="panel top-home">
            <div class="panel-body pad-25 bor-none padd1 ">
              
                {{--<a href="{{ $fb_url }}" class="fb-button fb-blue btn icon-btn btn-block btn-large row-space-1 btn-facebook font-normal pad-top">--}}
                  {{--<span ><i class="icon icon-facebook"></i></span>--}}
                  {{--<span >Log in with Facebook</span>--}}
                {{--</a>--}}
    {{----}}
                {{--<a href="{{URL::to('googleLogin')}}" class="btn icon-btn btn-block btn-large row-space-1 btn-google font-normal pad-top mr1">--}}
                  {{--<span ><i class="icon icon-google-plus"></i></span>--}}
                  {{--<span >Log in with Google</span>--}}
                {{--</a>--}}

                {{--<a href="{{URL::to('auth/linkedin')}}" class="li-button li-blue btn icon-btn btn-block btn-large row-space-1 btn-linkedin mr1">--}}
                  {{--<span ><i class="icon icon-linkedin"></i></span>--}}
                  {{--<span >Log in with LinkedIn</span>--}}
                {{--</a>          --}}

              {{--<div class="signup-or-separator">--}}
                {{--<span class="h6 signup-or-separator--text">or</span>  <hr>--}}
              {{--</div>--}}

              <div class="clearfix"></div>

              <form method="POST" action="{{ url('authenticate') }}" accept-charset="UTF-8" class="signup-form login-form ng-pristine ng-valid" data-action="Signin" novalidate="true"><input name="_token" type="hidden">

                <input id="from" name="from" type="hidden" value="email_login">
  
               <div class="control-group row-space-2 field_ico">
                 @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif 
                 <div class="pos_rel">
                 <i class="icon-envelope"></i>     
                  <input class="{{ $errors->has('email') ? 'decorative-input inspectletIgnore invalid' : 'decorative-input inspectletIgnore name-icon' }}"  placeholder="Email address" id="signin_email" name="email" type="email" value=""> 
                     </div>          
                </div>


                <div class="control-group row-space-3 field_ico"> 
                   @if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif 
                 <div class="pos_rel"> 
                  <i class="icon-lock"></i>  
                  <input class="{{ $errors->has('password') ? 'decorative-input inspectletIgnore invalid' : 'decorative-input inspectletIgnore name-icon' }}" placeholder="Password" id="signin_password" data-hook="signin_password" name="password" type="password" value="">
                 </div>
                </div>

                <div class="clearfix row-space-3">
                  <label for="remember_me2" class="checkbox remember-me">
                    <input id="remember_me2" class="remember_me" name="remember_me" type="checkbox" value="1"> Remember me
                  </label>
                  <a href="{{ url('forgot_password') }}" class="forgot-password link_color pull-right h5">Forgot password?</a>
                </div>

                <input class="btn btn-primary btn-block pad-top " id="user-login-btn" type="submit" value="Log In">
              </form>              
            </div>
          <div class="panel-body bottom-panel1 text-center">  <hr>
            Don’t have an account?
            <a href="{{url('users/signup_email')}}" class="btn btn-primary btn-block  link-to-signup-in-login login-btn link_color" id="create_using_email_button">
              Sign Up </a>
          </div>
        </div>
       </div>
    
  </div>
</div>

    </main>
@stop