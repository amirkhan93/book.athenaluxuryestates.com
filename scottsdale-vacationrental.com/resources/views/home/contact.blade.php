@extends('template')


@section('main')
<div class="container">
<h1 class="page-title">Contact Us</h1>
<div class="row">
<div class="col-md-12">
<form class="mt5" id="buyNowFormuser" name="frm" method="post">
<div id="head"></div>
<div class="row">
<div class="col-md-6">
<h4><strong>SEND US A MESSAGE</strong></h4>   
<div class="form-group">
<label>Your name</label>
<input class="form-control" type="text" name='name' value="" required maxlength="30"/>
<span class="val_name"></span>
</div>
<div class="form-group">
<label>Your e-mail</label>
<input class="form-control" type="email" name='email' value="" required/>
<span class="val_email"></span>
</div>
<div class="form-group">
<label>Your phone</label>
<input class="form-control" type="text" name="phone" value="" required maxlength="14"/>
<span class="val_phone"></span>
</div>
<div class="form-group">
<label>Subject</label>
<input class="form-control" type="text" name="subject" value="" required/>
<span class="val_subject"></span>
</div>
<div class="form-group">
<label>Your message</label>
<textarea class="form-control" rows="6" cols="10" name="message"></textarea>
</div>
<!--<div class="form-group">-->
<!--<label>Security Code</label>-->
<!--<img id="capImage" src="captcha_login.jpg" alt="CAPTCHA code" style="width:250px; border-radius:0px;padding-top:15px;">-->
<!--<p style="color:#000; padding:0px">Can't read the image? click <a  href="javascript:void(0);" style="color:red;" onclick="javascript:$('#capImage').attr('src','captcha_login.jpg');"> Here </a> to refresh</p>-->
<!--</div>-->
<!--<div class="form-group">-->
<!--<input class="form-control" type="text" placeholder="Security Code"name="captcha_code" id="captchatextlogin" maxlength="7" required>-->
<!--</div>-->
<input class="btn btn-primary" type="submit" name="send_data" value="Send Message">
</div>
<div class="col-md-6">
<h4><strong>CONTACTS</strong></h4> 
<div class="thumb mb40">
<h5 class="thumb-title mt35">FOR INQUIRIES</h5>
<i class="fa fa-envelope box-icon-left round box-icon-normal box-icon-black animate-icon-top-to-bottom"></i>
<div class="thumb-caption">
<!-- <h5 class="thumb-title">Email</h5>-->
<p class="thumb-desc">raykhov@gmail.com<br>
<!--<a data-toggle="modal" data-target="#combine" href="#">Read More</a>-->
</p>
</div>
</div>
<h4><strong>CALL US</strong></h4> 
<div class="thumb mb40 call-pan">
<h5 class="thumb-title">Main Number</h5>
<i class="fa fa-phone box-icon-left round box-icon-normal box-icon-black animate-icon-top-to-bottom"></i>
<div class="thumb-caption">
<p class="thumb-desc">+1 (909) 202 7712<br>
<p class="thumb-desc"><br>
<!--<a data-toggle="modal" data-target="#trust" href="#">Read More</a>-->
</p>
</div>
</div>
<h4><strong>ADDRESS HERE</strong></h4> 
<div class="thumb mb40">
<h5 class="thumb-title">MAIN LOCATION</h5>
<i class="fa fa-phone box-icon-left round box-icon-normal box-icon-black animate-icon-top-to-bottom"></i>
<div class="thumb-caption">
<p class="thumb-desc">Scottsdale, AZ<br>
<!--<a data-toggle="modal" data-target="#trust" href="#">Read More</a>-->
</p>
</div>
</div>
</div>
</div>    
</form>
</div>
</div>
</div></div>
<div class="gap"></div>
@stop



