<!DOCTYPE html>
<head>

	<!-- Basic Page Needs
	================================================== -->
	<meta charset="utf-8">
	<title>Luxvacationrentalhomes</title>



	<!-- Mobile Specific Metas
	================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	


	<!-- Web Fonts 
	================================================== -->
	<link
		href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
		rel="stylesheet" />
	<link
		href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
		rel="stylesheet">
		<link rel="stylesheet" href="https://cdn.materialdesignicons.com/3.8.95/css/materialdesignicons.min.css">

	<!-- CSS
	================================================== -->
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/bootstrap.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/font-awesome.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/ionicons.min.css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/jquery.fancybox.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/owl.carousel.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/owl.transitions.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/style.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/colors/color.css" />
	<style>
		.red_border {
			border:2px solid red;
		}
	</style>
</head>
<body>

	<!--<div class="loader">
		<div class="loader__figure"></div>
	</div>-->

	<svg class="hidden">
		<svg id="icon-nav" viewBox="0 0 152 63">
			<title>navarrow</title>
			<path
				d="M115.737 29L92.77 6.283c-.932-.92-1.21-2.84-.617-4.281.594-1.443 1.837-1.862 2.765-.953l28.429 28.116c.574.57.925 1.557.925 2.619 0 1.06-.351 2.046-.925 2.616l-28.43 28.114c-.336.327-.707.486-1.074.486-.659 0-1.307-.509-1.69-1.437-.593-1.442-.315-3.362.617-4.284L115.299 35H3.442C2.032 35 .89 33.656.89 32c0-1.658 1.143-3 2.552-3H115.737z" />
		</svg>
	</svg>
	<!-- Nav and Logo
	================================================== -->

	@include('common/headertwo')

	<!-- Primary Page Layout
	================================================== -->


		<div class="hero-center-section">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-12 mt-3 parallax-fade-top">
						<div class="booking-hero-wrap">
							<h1 class="hero-text">Luxury vacation homes,<br>
								<small>Curated and hosted by experts for a Lux Level Experience.</small></h1>
							<div class="row justify-content-center">
								<div class="col-md-3 pr-0 pl-0 cl_m">
									<select name="location" id="location" class="wide">
										<option data-display="Locations"></option>
										<option value="all locations">All Locations</option>
										@foreach($state as $row)
										<option value="{{ $row->state }}">{{ $row->state }}</option>
										@endforeach
									</select>
								</div>
								<div class="col-md-3 pl-0 cl_m">
									<div class="input-daterange input-group" id="flight-datepicker">
										<div class="row">
											<div class="col-6 pr-0">
												<div class="form-item">
													<span class="fontawesome-calendar"></span>
													<input class="input-sm" type="text" id="start-date-1" name="start" placeholder="check-in date" autocomplete="off"/>
													<span class="date-text date-depart"></span>
												</div>
											</div>
											<div class="col-6 pl-0 pr-0">
												<div class="form-item">
													<span class="fontawesome-calendar"></span>
													<input class="input-sm" type="text" id="end-date-1" name="end" placeholder="check-out date" autocomplete="off"/>
													<span class="date-text date-return"></span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-2  pl-0 pr-0 cl_m">
									<select name="adults" class="wide" id="guests">
										<option data-display="Guests">Guests</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6+">6+</option>
									</select>
								</div>
								<div class="col-md-2  pl-0 pr-0 cl_m">
									<a class="booking-button" href="javascript:void(0)" id="book_now">Search now</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="slideshow">
			<div class="slide slide--current parallax-top">
				<figure class="slide__figure">
					<div class="slide__figure-inner">
						<div class="slide__figure-img" style="background-image: url({{url()}}/resources/assets/home/img/1.jpg)"></div>
						<div class="slide__figure-reveal"></div>
					</div>
				</figure>
			</div>
			<div class="slide parallax-top">
				<figure class="slide__figure">
					<div class="slide__figure-inner">
						<div class="slide__figure-img" style="background-image: url({{url()}}/resources/assets/home/img/2.jpg)"></div>
						<div class="slide__figure-reveal"></div>
					</div>
				</figure>
			</div>
			<div class="slide parallax-top">
				<figure class="slide__figure">
					<div class="slide__figure-inner">
						<div class="slide__figure-img" style="background-image: url({{url()}}/resources/assets/home/img/3.jpg)"></div>
						<div class="slide__figure-reveal"></div>
					</div>
				</figure>
			</div>

		
			<div class="slideshow__indicator"></div>
			<div class="slideshow__indicator"></div>
		</div>
	</div>

	<div class="section  over-hide">
		<div class="container">
			<div class="center-content">
				<div class="left-center-bg">
					<img class="play-button" src="{{url()}}/resources/assets/home/img/playButton.c534a481.svg">
				</div>
				<div class="right-center-text">
					<h2>Your Lux Experience Lasts a Lifetime</h2>
					<p>Don’t settle for ‘standard’ – you, your family, your team can have a getaway that stays with you, long after the trip ends.</p>
				</div>
			</div>
		</div>
	</div>
	
	<div class="section background-grey over-hide">
		<div class="container-fluid px-0">
			<div class="row mx-0">
				<div class="col-xl-6 px-0">
					<div class="img-wrap" id="rev-1">
						<img src="{{url()}}/resources/assets/home/img/homes/1.jpg" alt="">
						
					</div>
				</div>
				<div class="col-xl-6 px-0 mt-4 mt-xl-0 align-self-center">
					<div class="row justify-content-center">
						<div class="col-10 col-xl-8 text-center">
							<h3 class="text-center">Specialists in Luxury Short Term Rentals</h3>
							<p class="text-center mt-4">Our homes are selected for a unique, unparalleled experience.  Each home has attractive features you cannot find in any other vacation home.</p>
							<!-- <a class="mt-5 btn btn-primary" href="search.html">check availability</a> -->
						</div>
					</div>
				</div>
			</div>
			<div class="row mx-0">
				<div class="col-xl-6 px-0 mt-4 mt-xl-0 pb-5 pb-xl-0 align-self-center">
					<div class="row justify-content-center">
						<div class="col-10 col-xl-8 text-center">
							<h3 class="text-center">Room for All</h3>
							<p class="text-center mt-4">The point is to bring everyone together.  Our properties have enough room for everyone to stay and plenty of room to spread out.</p>
							<!-- <a class="mt-5 btn btn-primary" href="search.html">check availability</a> -->
						</div>
					</div>
				</div>
				<div class="col-xl-6 px-0 order-first order-xl-last mt-5 mt-xl-0">
					<div class="img-wrap" id="rev-2">
						<img src="{{url()}}/resources/assets/home/img/homes/2.jpg" alt="">
					
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="section padding-top-bottom over-hide">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-md-8 align-self-center">
						
						<h3 class="text-center padding-bottom-small">Bonus Features:
								Something for Everyone</h3>
					</div>
					<div class="section clearfix"></div>
					<div class="col-sm-6 col-lg-4">
						<div class="services-box text-center">
							<img src="{{url()}}/resources/assets/home/img/icons/checkin.png" alt="">
							<h5 class="mt-2">Streamlined Check-In</h5>
							<p class="mt-3">We offer flexibility with remote access codes before you arrive, so your check in is smooth and easy.  No waiting for someone to greet you at a designated time.</p>
							
						</div>
					</div>
					<div class="col-sm-6 col-lg-4 mt-5 mt-sm-0">
						<div class="services-box text-center">
							<img src="{{url()}}/resources/assets/home/img/icons/wifi.png" alt="">
							<h5 class="mt-2">HI-SPEED WIFI & ENTERTAINMENT</h5>
							<p class="mt-3">
Whether you are working on your laptop or the kids are playing video games or watching a movie on HD flat screen TV, we’ve thought of all your technology needs.</p>
							
						</div>
					</div>
					<div class="col-sm-6 col-lg-4 mt-5 mt-lg-0">
						<div class="services-box text-center">
							<img src="{{url()}}/resources/assets/home/img/icons/service.png" style="width:60px" alt="">
							<h5 class="mt-2">SERVICE EXCELLENCE</h5>
							<p class="mt-3">We provide 24 hour round the clock support.  We are available to handle all your needs, answer your questions and solve any problems that come up.</p>
							
						</div>
					</div>
					<div class="col-sm-6 col-lg-4 mt-5">
						<div class="services-box text-center">
							<img src="{{url()}}/resources/assets/home/img/4.svg" alt="">
							<h5 class="mt-2">LUXURY FEATURES</h5>
							<p class="mt-3">
From gourmet kitchens to virtual golfing, to pools & spas, spectacular views, beautiful grounds and home theaters, every Lux rental offers something special.</p>
							
						</div>
					</div>
					<div class="col-sm-6 col-lg-4 mt-5">
						<div class="services-box text-center">
							<img src="{{url()}}/resources/assets/home/img/5.svg" alt="">
							<h5 class="mt-2">RELAX & PLAY TO THE MAX</h5>
							<p class="mt-3">Our homes have plenty to enjoy, we have your recreation covered – from swimming pools to billiards to games, horseshoes, bocce, tennis, golf putting – there is fun for all.</p>
							
						</div>
					</div>
					<div class="col-sm-6 col-lg-4 mt-5">
						<div class="services-box text-center">
							<img src="{{url()}}/resources/assets/home/img/icons/security.png" alt="">
							<h5 class="mt-2">SAFETY & SECURITY</h5>
							<p class="mt-3">Most of our properties are equipped with perimeter outdoor cameras, safes to secure your valuables and other security measures for your protection.</p>
							
						</div>
					</div>
				</div>
			</div>
		</div>




	<div class="section padding-top-bottom-big over-hide">
		<div class="parallax" style="background-image: url('{{url()}}/resources/assets/home/img/homes/5.jpg')"></div>
		<div class="section z-bigger">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-md-8">
						<div id="owl-sep-1" class="owl-carousel owl-theme">
							<div class="item">
								<div class="quote-sep">
									<h4>"This home exceeded my expectations! Stayed with about 14 other friends for a bachelor party and there was plenty of room for everyone to sleep. The pool was great to have and we got a lot of use out of it. Only about 15-20 min to Old Town and 15-20 min from PHX airport. Highly recommend this home!"</h4>
									<h6>Nicholas</h6>
								</div>
							</div>
							<div class="item">
								<div class="quote-sep">
									<h4>"The house was an ideal setting to enjoy the Arizona sunshine. The patio & pool area was perfect. Note that the hot tub only works if you set it up to use ahead of time with the host. The house was roomy and well setup. We really enjoyed our stay!"</h4>
									<h6>Angie</h6>
								</div>
							</div>
							<div class="item">
								<div class="quote-sep">
									<h4>"The home itself was top notch. We had s group of 8 and there wasn’t a time anyone was bored. There are so many things to do on the property itself. A ton of included amenities and ample space."</h4>
									<h6>Mathew</h6>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>



	<div class="section padding-top-bottom background-grey over-hide">
		<div class="container">
			
			<div class="row background-white p-0 m-0">
				<div class="col-xl-6 p-0">
					<div class="img-wrap" id="rev-3">
							<img src="{{url()}}/resources/assets/home/img/homes/3.jpg" alt="">
					</div>
				</div>
				<div class="col-xl-6 p-0 align-self-center">
					<div class="row justify-content-center">
						<div class="col-9 pt-4 pt-xl-0 pb-5 pb-xl-0 text-center">
							<h5 class=""> A Better Choice for Group Gatherings</h5>
							<p class="mt-3">Whether you are booking Leadership Summits, Corporate Masterminds, Friend Getaways or Family Reunions, our Lux Vacation Rental properties are the perfect place to host your next event.</p>
							
						</div>
					</div>
				</div>
			</div>
			
		</div>
	</div>



	@include('common/footertwo')


	<!-- JAVASCRIPT
    ================================================== -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script src="{{url()}}/resources/assets/home/js/popper.min.js"></script>
	<script src="{{url()}}/resources/assets/home/js/bootstrap.min.js"></script>
	<script src="{{url()}}/resources/assets/home/js/plugins.js"></script>
	<script src="{{url()}}/resources/assets/home/js/flip-slider.js"></script>
	<script src="{{url()}}/resources/assets/home/js/reveal-home.js"></script>
	<script src="{{url()}}/resources/assets/home/js/custom.js"></script>
	<script>
		$(document).ready(function(){
			$("#start-date-1").datepicker();
			$("#end-date-1").datepicker();
			$("#book_now").click(function(e){
				e.preventDefault();
				locations = $("#location").val();
				start_date = $("#start-date-1").val();
				end_date = $("#end-date-1").val();
				guests = $("#guests").val();
				if(locations=='')
				{
					$("#location").addClass("red_border");
				}
				else if(locations!='')
				{
					window.location.href="{{URL::to('search')}}"+"/"+locations+"/"+guests;
				}
				
				/*	if(locations=='' && start_date!='' && end_date!='' && guests!='')
				{
					$("#location").addClass("red_border");
				}
				else if(locations!='' && start_date!='' && end_date!='' && guests!='')
				{
					window.location.href="{{URL::to('search')}}"+"/"+locations+"/"+guests;
				}*/
			});
		});
	</script>
</body>
</html>