  <style type="text/css">
    .btn-large {
    padding: 20px 27px !important;
    }
  </style>
@extends('template')
   <!-- Web Fonts 
	================================================== -->
	<link
		href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
		rel="stylesheet" />
	<link
		href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
		rel="stylesheet">
		<link rel="stylesheet" href="https://cdn.materialdesignicons.com/3.8.95/css/materialdesignicons.min.css">

	<!-- CSS
	================================================== -->
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/bootstrap.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/font-awesome.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/ionicons.min.css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/jquery.fancybox.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/owl.carousel.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/owl.transitions.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/style.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/colors/color.css" />
@section('main')

    <main id="site-content" role="main">
      

<div class="page-container-responsive page-container-auth margintop" style="margin-top:40px;margin-bottom:40px;">
  <div class="row">
    
        <div class="log_pop col-center">          
          <div class="panel top-home">
          <!--<div class="login-close">
          <img src="images/close.png">
          </div>-->
            <div class="alert alert-with-icon alert-error alert-header panel-header hidden-element notice" id="notice">
              <i class="icon alert-icon icon-alert-alt"></i>
            </div>
            <div class="panel-padding panel-body pad-25 padd1">
              <div class="social-buttons">
                <a href="{{ $fb_url }}" class="fb-button fb-blue btn icon-btn btn-block row-space-1 btn-large btn-facebook pad-23" data-populate_uri="" data-redirect_uri="{{URL::to('/')}}/authenticate">
                  <span>
                    <i class="icon icon-facebook"></i>
                  </span>
                  <span >
                    {{ trans('messages.login.signup_with') }} Facebook
                  </span>
                </a>

      
                <a href="{{URL::to('googleLogin')}}" class="btn icon-btn btn-block row-space-1 btn-large btn-google pad-23">
                  <span>
                    <i class="icon icon-google-plus"></i>
                  </span>
                  <span >
                    {{ trans('messages.login.signup_with') }} Google
                  </span>
                </a>

                <a href="{{URL::to('auth/linkedin')}}" class="li-button li-blue btn icon-btn btn-block btn-large row-space-1 btn-linkedin">
                  <span >
                    <i class="icon icon-linkedin"></i>
                  </span>
                  <span >
                    {{ trans('messages.login.signup_with') }} LinkedIn
                  </span>
                </a>
              </div>

              <div class="text-center social-links hide">
                {{ trans('messages.login.signup_with') }} <a href="{{ $fb_url }}" class="facebook-link-in-signup">Facebook</a> {{ trans('messages.login.or') }} <a href="{{URL::to('googleLogin')}}">Google</a>
              </div>

              <div class="signup-or-separator">
                <span class="h6 signup-or-separator--text">{{ trans('messages.login.or') }}</span>
                <hr>
              </div>

              <div class="text-center">
                <a href="" class="create-using-email btn-block  row-space-2 btn btn-primary btn-block btn-large large icon-btn pad-23 signup_popup_head2 btn_new1" id="create_using_email_button">
                <span >
                  <i class="icon icon-envelope"></i>
                </span>
                <span >
                  {{ trans('messages.login.signup_with') }} {{ trans('messages.login.email') }}
                </span>
                </a>
              </div>

            <div id="tos_outside" class="row-space-top-3">
              <small class="small-font style1">
                {{ trans('messages.login.signup_agree') }} {{ $site_name }}'s <a class="link_color" href="{{URL::to('/')}}/terms_of_service" data-popup="true">{{ trans('messages.login.terms_service') }}</a>, <a class="link_color" href="{{URL::to('/')}}/privacy_policy" data-popup="true">{{ trans('messages.login.privacy_policy') }}</a>, <a class="link_color" href="{{URL::to('/')}}/guest_refund" data-popup="true">{{ trans('messages.login.guest_policy') }}</a>, {{ trans('messages.header.and') }} <a class="link_color" href="{{URL::to('/')}}/host_guarantee" data-popup="true">{{ trans('messages.login.host_guarantee') }}</a>.
              </small>
            </div>
          </div>

          <div class="panel-body bottom-panel1 text-center">
            <hr>
            {{ trans('messages.login.already_an') }} {{ $site_name }} {{ trans('messages.login.member') }}
            <a href="{{ url('login') }}" class="modal-link link-to-login-in-signup login-btn login_popup_head link_color" data-modal-href="/login_modal?" data-modal-type="login">
              {{ trans('messages.header.login') }}
            </a>
          </div>
        </div>
      </div>
    
  </div>
</div>    </main>
    
 @stop