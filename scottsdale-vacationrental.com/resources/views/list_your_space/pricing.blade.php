 <style type="text/css">
       html[lang="ar"] #js-manage-listing-nav{position: fixed !important;right: 0px !important;}
         @media (min-width: 767px){

  #ajax_container{margin-left:25%;}
  html[lang="ar"] #js-manage-listing-nav{position: fixed;}
  html[lang="ar"] #ajax_container{margin-right:25% !important;margin-left: 0px !important;}
}
  @media(min-width: 1100px){
  #ajax_container{margin-left: 16.66667%;}
  html[lang="ar"] #ajax_container{margin-right: 16.66667% !important; margin-left: 0px !important;}
}
    </style>
<div id="js-manage-listing-content-container" class="manage-listing-content-container">
      <div class="manage-listing-content-wrapper">
        <div id="js-manage-listing-content" class="manage-listing-content col-lg-12 col-md-12"><div>
<div class="row-space-4">
  <div class="row">
      <h3 class="col-12">{{ trans('messages.lys.pricing_title') }}</h3>
  </div>
  <p class="text-muted">{{ trans('messages.lys.pricing_desc') }}</p>
</div>

  <hr>

<div id="help-panel-nightly-price" class="js-section">
  <!-- <div style="display: none;" class="js-saving-progress saving-progress base_price">
  <h5>{{ trans('messages.lys.saving') }}...</h5>
</div> -->
<div class="row">
  <div class="col-md-12">
    <h4>Select Dates</h4>
  </div>
  <div class="col-md-4">
      <label for="start_date" class="label-large">Start Date</label>
      <input type="text" name="start_date" placeholder="Start Date" />
  </div>
  <div class="col-md-4">
      <label for="end_date" class="label-large">End Date</label>
      <input type="text" name="end_date" placeholder="End Date" />
  </div>
</div>
  <div class="row">
  <div class="col-md-4">
  <h4>{{ trans('messages.lys.base_price') }}</h4>

<label for="listing_price_native" class="label-large">{{ trans('messages.lys.nightly_price') }}</label>
<div class="row row-table row-space-1">
  <div class="col-12 col-middle">
    <div class="input-addon">
      <span class="input-prefix">{{ $result->rooms_price->currency->original_symbol }}</span>
      <input type="number" min="0" limit-to=9 data-suggested="" id="price-night" value="{{ ($result->rooms_price->original_night == 0) ? '' : $result->rooms_price->original_night }}" name="night" class="input-stem input-large autosubmit-text" data-saving="base_price">
      <input type="hidden" id="price-night-old" value="{{ ($result->rooms_price->original_night == 0) ? '' : $result->rooms_price->original_night }}" name="night_old" class="input-stem input-large autosubmit-text">
    </div>
  </div>

</div>
<p data-error="price" class="ml-error"></p>

<div class="row row-space-top-3">
    <div class="col-12">
      <label class="label-large">{{ trans('messages.account.currency') }}</label>
      <div id="currency-picker"><div class="select
            select-large
            select-block">
  {!! Form::select('currency_code',$currency, $result->rooms_price->currency_code, ['id' => 'price-select-currency_code', 'data-saving' => 'base_price']) !!}
</div>
</div>
    </div>
  </div>
  </div>
  <div class="col-md-4">
  <h4>{{ trans('messages.lys.long_term_prices') }}</h4>
  <div class="" id="js-long-term-prices">

<div id="js-long-term-prices" class="js-section">
  <!-- <div style="display: none;" class="js-saving-progress saving-progress long_price">
  <h5>{{ trans('messages.lys.saving') }}...</h5>
</div> -->
  <div class="row-space-3">
    <div>
      <label for="listing_weekly_price_native" class="label-large">
        {{ trans('messages.lys.weekly_price') }}
      </label>
      <div class="row row-table row-space-1">
        <div class="col-12 col-middle">
          <div class="input-addon">
            <span class="input-prefix">{{ $result->rooms_price->currency->original_symbol }}</span>
            <input type="number" min="0" limit-to=9 data-suggested="" id="price-week" class="input-stem input-large autosubmit-text" value="{{ ($result->rooms_price->original_week == 0) ? '' : $result->rooms_price->original_week }}" name="week" data-saving="long_price">
          </div>
        </div>

      </div>

      <p data-error="week" class="ml-error hide"></p>
      <div class="js-advanced-weekly-pricing"></div>
    </div>
  </div>

  <div class="row-space-3">
    <div>
      <label for="listing_monthly_price_native" class="label-large">
        {{ trans('messages.lys.monthly_price') }}
      </label>
      <div class="row row-table row-space-1">
        <div class="col-12 col-middle">
          <div class="input-addon">
            <span class="input-prefix">{{ $result->rooms_price->currency->original_symbol }}</span>
            <input type="number" min="0" limit-to=9 data-suggested="₹16905" id="price-month" class="autosubmit-text input-stem input-large" value="{{ ($result->rooms_price->original_month == 0) ? '' : $result->rooms_price->original_month }}" name="month" data-saving="long_price">
          </div>
        </div>

      </div>

      <p data-error="month" class="ml-error hide"></p>
      <span class="js-advanced-monthly-pricing"></span>
    </div>
  </div>

</div>

</div>
  </div>
  <div class="col-md-4">

  </div>
  </div>






</div>


<hr class="row-space-top-6 row-space-5 post-listed">


<div class="js-section {{ ($result->status != NULL) ? 'pre-listed' : 'post-listed' }}">
  <!-- <div style="display: none;" class="js-saving-progress saving-progress additional-saving">
  <h5>{{ trans('messages.lys.saving') }}...</h5>
</div> -->

  <h4>{{ trans('messages.lys.additional_pricing') }}</h4>

  <div class="row">
    <div class="col-md-4">
    <div id="js-cleaning-fee" class="row-space-3 js-tooltip-trigger">
  <label for="listing_cleaning_fee_native_checkbox" class="label-large label-inline">
    <input type="checkbox" data-extras="true" ng-model="cleaning_checkbox" id="listing_cleaning_fee_native_checkbox" ng-init="cleaning_checkbox = {{ ($result->rooms_price->original_cleaning == 0) ? 'false' : 'true' }}" ng-checked="{{ ($result->rooms_price->original_cleaning == 0) ? 'false' : 'true' }}">
    {{ trans('messages.lys.cleaning') }}
  </label>

  <div data-checkbox-id="listing_cleaning_fee_native_checkbox" ng-show="cleaning_checkbox" ng-cloak>
    <div class="row row-table row-space-1">
      <div class="col-12 col-middle">
        <div class="input-addon">
          <span class="input-prefix">{{ $result->rooms_price->currency->original_symbol }}</span>
          <input type="number" min="0" limit-to=9 data-extras="true" id="price-cleaning" value="{{ ($result->rooms_price->original_cleaning == 0) ? '' : $result->rooms_price->original_cleaning }}" name="cleaning" class="autosubmit-text input-stem input-large" data-saving="additional-saving">
        </div>
      </div>

    </div>
    <p class="text-muted">
      {{ trans('messages.lys.cleaning_desc') }}
    </p>
    <p data-error="extras_price" class="ml-error"></p>
  </div>
</div>
    </div>
    <div class="col-md-4">
    <div id="js-security-deposit" class="row-space-3 js-tooltip-trigger">
  <label for="listing_security_deposit_native_checkbox" class="label-large label-inline">
    <input type="checkbox" data-extras="true" ng-model="security_checkbox" id="listing_security_deposit_native_checkbox" ng-init="security_checkbox = {{ ($result->rooms_price->original_security == 0) ? 'false' : 'true' }}" ng-checked="{{ ($result->rooms_price->original_security == 0) ? 'false' : 'true' }}">
    {{ trans('messages.lys.security_deposit') }}
  </label>

  <div data-checkbox-id="listing_security_deposit_native_checkbox" ng-show="security_checkbox" ng-cloak>
    <div class="row row-space-1">
      <div class="col-12">
        <div class="input-addon">
          <span class="input-prefix">{{ $result->rooms_price->currency->original_symbol }}</span>
          <input type="number" min="0" limit-to=9 data-extras="true" value="{{ ($result->rooms_price->original_security == 0) ? '' : $result->rooms_price->original_security }}" id="price-security" name="security" class="autosubmit-text input-stem input-large" data-saving="additional-saving">
        </div>
      </div>
    </div>
    <p data-error="security_deposit" class="ml-error"></p>
    <p class="text-muted">
      {{ trans('messages.lys.security_deposit_desc') }}
    </p>
  </div>
</div>
    </div>
  </div>











<div id="js-weekend-pricing" class="row-space-3 js-tooltip-trigger">
  <label for="listing_weekend_price_native_checkbox" class="label-large label-inline">
    <input type="checkbox" data-extras="true" ng-model="weekend_checkbox" id="listing_weekend_price_native_checkbox" ng-init="weekend_checkbox = {{ ($result->rooms_price->original_weekend == 0) ? 'false' : 'true' }}" ng-checked="{{ ($result->rooms_price->original_weekend == 0) ? 'false' : 'true' }}">
    {{ trans('messages.lys.weekend_pricing') }}
  </label>

  <div data-checkbox-id="listing_weekend_price_native_checkbox" ng-show="weekend_checkbox" ng-cloak>
    <div class="row row-table row-space-1">
      <div class="col-4">
        <div class="input-addon">
          <span class="input-prefix">{{ $result->rooms_price->currency->original_symbol }}</span>
          <input type="number" min="0" limit-to=9 data-extras="true" value="{{ ($result->rooms_price->original_weekend == 0) ? '' : $result->rooms_price->original_weekend }}" id="price-weekend" name="weekend" class="autosubmit-text input-stem input-large" data-saving="additional-saving">
        </div>
      </div>
    </div>

    <p class="text-muted">
      {{ trans('messages.lys.weekend_pricing_desc') }}
    </p>
  </div>
</div>



</div>


<div id="js-donation-tool-placeholder"></div>

<div class="not-post-listed row row-space-top-6 progress-buttons">

  <div class="col-4 row-space-top-1 next_step">

    @if($result->status == NULL)
      <a data-prevent-default="" href="{{ url('manage-listing/'.$room_id.'/video') }}" class="back-section-button">{{ trans('messages.lys.back') }}</a>
    @endif

    @if($result->status != NULL)
      <a data-prevent-default="" href="{{ url('manage-listing/'.$room_id.'/calendar') }}" class="back-section-button">{{ trans('messages.lys.back') }}</a>
    @endif

  </div>
  <div class="col-4 text-right next_step">

    @if($result->status == NULL)
      <a data-prevent-default="" href="{{ url('manage-listing/'.$room_id.'/calendar') }}" class="btn btn-large btn-primary next-section-button">
        {{ trans('messages.lys.next') }}
      </a>
    @endif

    @if($result->status != NULL)
      <a data-prevent-default="" href="{{ url('manage-listing/'.$room_id.'/booking') }}" class="btn btn-large btn-primary next-section-button">
        {{ trans('messages.lys.next') }}
      </a>
    @endif

  </div>
</div>

</div></div>
        <div id="js-manage-listing-help" class="manage-listing-help col-lg-4 pos-fix col-md-4 hide-sm"><div class="manage-listing-help-panel-wrapper">
  <div class="panel manage-listing-help-panel" >
    <div class="help-header-icon-container text-center va-container-h">
      <img width="50" height="50" class="col-center" src="{{ url('images/lightbulb2x.png') }}">
    </div>
    <div class="panel-body">
      <h4 class="text-center">{{ trans('messages.lys.nightly_price') }}</h4>

  <p>{{ trans('messages.lys.nightly_price_desc') }}</p>

    </div>
  </div>
</div>


</div>
      </div>
      <div class="manage-listing-content-background"></div>
    </div>
