<!DOCTYPE html>

<head>

    <!-- Basic Page Needs
	================================================== -->
    <meta charset="utf-8">
    <title>Luxvacationrentalhomes</title>



    <!-- Mobile Specific Metas
	================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate"/>
    <meta http-equiv="Pragma" content="no-cache"/>
    <meta http-equiv="Expires" content="0"/>


    <!-- Web Fonts 
	================================================== -->
    <link
        href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet" />
    <link
        href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
        rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.materialdesignicons.com/3.8.95/css/materialdesignicons.min.css">

    <!-- CSS
	================================================== -->
    <link rel="stylesheet" href="{{url()}}/resources/assets/home/css/bootstrap.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/font-awesome.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/ionicons.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/datepicker.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/jquery.fancybox.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/owl.carousel.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/owl.transitions.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/lightslider.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/style.css" />
    <link rel="stylesheet" href="{{url()}}/resources/assets/home/css/colors/color.css" />
    
    <style>
        ol.breadcrumb{
            padding: 5px 15px;
            font-size: 13px;
            margin-top: -12px;
            background-color: #f5f5f5;
        }
        ol.breadcrumb li a{
            color: #23beed !important;
        }
    </style>

</head>
<body>

    <div class="loader">
        <div class="loader__figure"></div>
    </div>

    <svg class="hidden">
        <svg id="icon-nav" viewBox="0 0 152 63">
            <title>navarrow</title>
            <path
                d="M115.737 29L92.77 6.283c-.932-.92-1.21-2.84-.617-4.281.594-1.443 1.837-1.862 2.765-.953l28.429 28.116c.574.57.925 1.557.925 2.619 0 1.06-.351 2.046-.925 2.616l-28.43 28.114c-.336.327-.707.486-1.074.486-.659 0-1.307-.509-1.69-1.437-.593-1.442-.315-3.362.617-4.284L115.299 35H3.442C2.032 35 .89 33.656.89 32c0-1.658 1.143-3 2.552-3H115.737z" />
        </svg>
    </svg>

    <!-- Nav and Logo
	================================================== -->
   	@include('common/headertwo')
   	 
    <div class="section background-dark list_section">
        <div class="hero-center-section">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 mt-3 parallax-fade-top">
                        <div class="booking-hero-wrap">
                            	<h1 class="hero-text">Luxury vacation homes,<br>
								<small>Curated and hosted by experts for a Lux Level Experience.</small></h1>
                            <div class="row justify-content-center">
                                <div class="col-md-3 pr-0 pl-0 cl_m">
                                    <select name="location" id="location" class="wide">
										<option data-display="Locations"></option>
										<option value="all locations">All Locations</option>
										@foreach($locations as $rows)
										<option value="{{ $rows->state }}">{{ $rows->state }}</option>
										@endforeach
									</select>
                                </div>
                                <div class="col-md-3 pl-0 cl_m">
                                    <div class="input-daterange input-group" id="flight-datepicker">
                                        <div class="row">
                                            <div class="col-6 pr-0">
                                                <div class="form-item">
                                                    <span class="fontawesome-calendar"></span>
                                                    <input class="input-sm" type="text" id="start-date-1" name="start" placeholder="check-in date" />
													<!--<span class="date-text date-depart"></span>-->
                                                </div>
                                            </div>
                                            <div class="col-6 pl-0 pr-0">
                                                <div class="form-item">
                                                    <span class="fontawesome-calendar"></span>
                                                    <input class="input-sm" type="text" id="end-date-1" name="end" placeholder="check-out date" />
													<!--<span class="date-text date-return"></span>-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2  pl-0 pr-0 cl_m">
									<select name="adults" class="wide" id="guests">
										<option data-display="Guests">Guests</option>
										<option value="1">1</option>
										<option value="2">2</option>
										<option value="3">3</option>
										<option value="4">4</option>
										<option value="5">5</option>
										<option value="6+">6+</option>
									</select>
                                </div>
                                <div class="col-md-2  pl-0 pr-0 cl_m">
									<a class="booking-button" href="javascript:void(0)" id="book_now">Search</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       

        <div class="slideshow list-slideshow">
            <div class="slide slide--current parallax-top">
                <figure class="slide__figure">
                    <div class="slide__figure-inner">
                        <div class="slide__figure-img"></div>
                        <div class="slide__figure-reveal"></div>
                    </div>
                </figure>
            </div>
        </div>
    </div>
    <!-- Primary Page Layout
	================================================== -->



    <div class="section padding-top-bottom over-hide background-grey">
        <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{ url() }}">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{ $id }}</li>
            </ol>
        </nav>
            <div class="row justify-content-center">
				@if(count($result)=='')
					<p style="color: #e01111;font-weight:bold;">No Data Found</p>
				@else
                @foreach($result as $row)
				<div class="col-md-4">
                    <div class="room-box background-white">
                        <div class="room-per">
                            from ${{ $row->night }}/nt
                        </div>
                       <ul class="roomSlider">
							<?php
								$sql = DB::table('rooms_photos')->select('*')->where('room_id', $row->room_id )->limit(4)->get();
								foreach($sql as $photo)
								{
							?>
									<li>
										<img src="{{url()}}/images/rooms/<?php echo $photo->room_id; ?>/<?php echo $photo->name; ?>" alt="">
									</li>
							<?php
								}
							?>
                        </ul>
                        <div class="room-box-in d-flex justify-content-between">
                            <div>
                                <h5 class="">{{ $row->name }}</h5>
                                <span>{{ $row->state }}</span>
                            </div>
                            <div class="text-right">
                                <span>{{ $row->bedrooms }} BR {{ $row->bathrooms }} BA {{ $row->accommodates }} GUESTS</span>
                                <a class="mt-1 btn btn-primary" href="{{url()}}/rooms/{{ $row->room_id }}">View Details</a>
                            </div>

                        </div>
                    </div>
                </div>
				@endforeach
				@endif
            </div>
        </div>
    </div>

   @include('common/footertwo')
    <div class="scroll-to-top"></div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="{{url()}}/resources/assets/home/js/jquery.min.js"></script>
	<script src="{{url()}}/resources/assets/home/js/popper.min.js"></script>
	<script src="{{url()}}/resources/assets/home/js/bootstrap.min.js"></script>
	<script src="{{url()}}/resources/assets/home/js/plugins.js"></script>
	<script src="{{url()}}/resources/assets/home/js/lightslider.min.js"></script>
	<script src="{{url()}}/resources/assets/home/js/flip-slider.js"></script>
	<script src="{{url()}}/resources/assets/home/js/reveal-home.js"></script>
	<script src="{{url()}}/resources/assets/home/js/custom.js"></script>
	<script>
		$(document).ready(function(){
			$("#start-date-1").datepicker();
			$("#end-date-1").datepicker();
			$("#book_now").click(function(e){
				e.preventDefault();
				locations = $("#location").val();
				start_date = $("#start-date-1").val();
				end_date = $("#end-date-1").val();
				guests = $("#guests").val();
				if(locations=='')
				{
					$("#location").addClass("red_border");
				}
				else if(locations!='')
				{
					window.location.href="{{URL::to('search')}}"+"/"+locations+"/"+guests;
				}
			});
		});
	</script>
</body>
</html>