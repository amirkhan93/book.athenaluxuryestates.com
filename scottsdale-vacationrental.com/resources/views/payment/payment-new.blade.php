@extends('template')
<link
		href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
		rel="stylesheet" />
	<link
		href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
		rel="stylesheet">
		<link rel="stylesheet" href="https://cdn.materialdesignicons.com/3.8.95/css/materialdesignicons.min.css">
<link href="https://fonts.googleapis.com/css?family=Mansalva&display=swap" rel="stylesheet">
	<!-- CSS
	================================================== -->
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/bootstrap.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/font-awesome.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/ionicons.min.css" />
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/jquery.fancybox.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/owl.carousel.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/owl.transitions.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/style.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/colors/color.css" />
	<style>
	    .checkout-main__section{
	            padding-top: 25px;
	    }
	</style>
@section('main')

  <main id="site-content" role="main" ng-controller="payment" class="inner_page_margin_d">

<div id="main-view" class="main-view page-container-responsive row-space-top-6 row-space-6">
@if($reservation_id!='' || $booking_type == 'instant_book')
<form action="{{ url('payments/stripe_booking') }}" method="post" id="checkout-form" data-stripe-publishable-key="{{ env('STRIPE_KEY') }}">
  <!--   @if(Session::get('get_token')=='')-->
  <!--<form action="{{ url('payments/yapstone_booking') }}" method="post" id="checkout-form">-->
  <!--   @else-->
  <!--<form action="{{ url('api_payments/create_booking') }}" method="post" id="checkout-form">-->
  <!--   @endif-->
@else
  @if(Session::get('get_token')=='')
  <form action="{{ url('payments/pre_accept') }}" method="post" id="checkout-form">
  @else
  <form action="{{ url('api_payments/pre_accept') }}" method="post" id="checkout-form">
  @endif
@endif
    <input name="room_id" type="hidden" value="{{ $room_id }}">
    <input name="checkin" type="hidden" value="{{ $checkin }}">
    <input name="special_offer_id" type="hidden" value="{{ $special_offer_id }}">
    <input name="checkout" type="hidden" value="{{ $checkout }}">
    <input name="number_of_guests" type="hidden" value="{{ $number_of_guests }}">
    <input name="nights" type="hidden" value="{{ $nights }}">
    <input name="cancellation" type="hidden" value="{{ $cancellation }}">
    <input name="currency" type="hidden" value="{{ $result->rooms_price->code }}">
    <input name="session_key" type="hidden" value="{{ $s_key }}">


    {!! Form::token() !!}

    <div class="row">
    <div id="content-container" class="col-md-8 lang-ar-right">
          <div class="alert alert-with-icon alert-error alert-block hide row-space-2" id="form-errors">
            <i class="icon alert-icon icon-alert-alt"></i>
                      <div class="h5 row-space-1 error-header">
                      {{ trans('messages.payments.almost_done') }}!
                    </div>
                    <ul></ul>

          </div>
          <div class="alert alert-with-icon alert-error alert-block hide row-space-2" id="server-error">
            <i class="icon alert-icon icon-alert-alt"></i>
                      {{ trans('messages.payments.connection_timed_out',['site_name'=>$site_name]) }}
          </div>
          <div class="alert alert-with-icon alert-error alert-block hide row-space-2" id="verification-error">
            <i class="icon alert-icon icon-alert-alt"></i>

                      {{ trans('messages.payments.card_not_verified') }}
          </div>
@if($reservation_id!='' || $booking_type == 'instant_book')
        <section id="payment" class="checkout-main__section payment">
        <div class="payment_form_main custom_card">

          <div class="row">
            <div class="col-sm-12">
              <h2>{{ trans('messages.payments.billing_info') }}</h2><p></p>
            </div>
          </div>


          <div class="row">
            <div class="control-group cc-first-name col-md-6">
              <label class="control-label" for="credit-card-first-name">
                {{ trans('messages.login.first_name') }}
              </label>

              {!! Form::text('first_name', '', ['id' => 'credit-card-first-name']) !!}

              @if ($errors->has('first_name')) <div class="label label-warning inline-error">{{ $errors->first('first_name') }}</div> @endif
            </div>

            <div class="control-group cc-last-name col-md-6">
              <label class="control-label" for="credit-card-last-name">
                {{ trans('messages.login.last_name') }}
              </label>

              {!! Form::text('last_name', '', ['id' => 'credit-card-last-name']) !!}

              @if ($errors->has('last_name')) <div class="label label-warning inline-error">{{ $errors->first('last_name') }}</div> @endif
            </div>
          </div>
          <div class="row">
            <div class="control-group cc-address1 col-md-6">
              <label class="control-label" for="credit-card-address1">
                Email
              </label>

              {!! Form::text('email', '', ['id' => 'credit-card-email', 'class' => 'cc-short cc-email-text']) !!}
              @if ($errors->has('email')) <div class="label label-warning inline-error">{{ $errors->first('email') }}</div> @endif
            </div>

			<div class="control-group cc-address1 col-md-6">
              <label class="control-label" for="credit-card-address1">
                Phone
              </label>

               {!! Form::text('phone', '', ['id' => 'credit-card-phone', 'class' => 'cc-short cc-phone-text']) !!}
              @if ($errors->has('phone')) <div class="label label-warning inline-error">{{ $errors->first('phone') }}</div> @endif
            </div>

            <div class="col-md-6">
              <label for="credit-card-address2">
                Street
              </label>

              {!! Form::text('address2', '', ['id' => 'credit-address2-phone', 'class' => 'cc-short cc-address2-text']) !!}
              @if ($errors->has('address2')) <div class="label label-warning inline-error">{{ $errors->first('address2') }}</div> @endif
            </div>
            <div class="control-group cc-city

                        col-md-6
                         ">
              <label for="credit-card-city">
                {{ trans('messages.account.city') }}
              </label>

             {!! Form::text('city', '', ['id' => 'credit-card-city', 'class' => 'cc-short cc-city-text']) !!}
              @if ($errors->has('city')) <div class="label label-warning inline-error">{{ $errors->first('city') }}</div> @endif
            </div>
          </div>

          <div class="row">

            <div class="cc-state col-md-6
                       ">
              <label for="credit-card-state">
                {{ trans('messages.account.state') }}
              </label>

              {!! Form::text('state', '', ['id' => 'credit-card-state', 'class' => 'cc-short cc-state-text']) !!}
              @if ($errors->has('state')) <div class="label label-warning inline-error">{{ $errors->first('state') }}</div> @endif
            </div>

            <div class="control-group cc-zip cc-zip-new

                         col-md-6">
              <label for="credit-card-zip">
                {{ trans('messages.payments.postal_code') }}
              </label>

              {!! Form::text('zip', '', ['id' => 'credit-card-zip', 'class' => 'cc-short cc-zip-text']) !!}

              @if ($errors->has('zip')) <div class="label label-warning inline-error">{{ $errors->first('zip') }}</div> @endif
            </div>

            <div class="col-md-6">
              <label aria-hidden="true">
                <span class="screen-reader-only"></span>
                &nbsp;
              </label>
              <div class="help-inline credit-card-country-name">
                <strong id="billing-country"></strong>
              </div>
            </div>
          </div>
    </div>
    <hr>
    <div class="payment_mode_main custom_card">
            <h2>{{ trans('messages.payments.payment') }} Mode</h2>
           <div class="card_payment_main" id="card_payment_main">

    <div class="card_container preload">
        <div class="creditcard">
            <div class="front">
                <div id="ccsingle"></div>
                <svg version="1.1" id="cardfront" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                    x="0px" y="0px" viewBox="0 0 750 471" style="enable-background:new 0 0 750 471;" xml:space="preserve">
                    <g id="Front">
                        <g id="CardBackground">
                            <g id="Page-1_1_">
                                <g id="amex_1_">
                                    <path id="Rectangle-1_1_" class="lightcolor grey" d="M40,0h670c22.1,0,40,17.9,40,40v391c0,22.1-17.9,40-40,40H40c-22.1,0-40-17.9-40-40V40
                            C0,17.9,17.9,0,40,0z" />
                                </g>
                            </g>
                            <path class="darkcolor greydark" d="M750,431V193.2c-217.6-57.5-556.4-13.5-750,24.9V431c0,22.1,17.9,40,40,40h670C732.1,471,750,453.1,750,431z" />
                        </g>
                        <text transform="matrix(1 0 0 1 60.106 295.0121)" id="svgnumber" class="st2 st3 st4">0123 4567 8910 1112</text>
                        <text transform="matrix(1 0 0 1 54.1064 428.1723)" id="svgname" class="st2 st5 st6">Name on card</text>
                        <text transform="matrix(1 0 0 1 54.1074 389.8793)" class="st7 st5 st8">cardholder name</text>
                        <text transform="matrix(1 0 0 1 479.7754 388.8793)" class="st7 st5 st8">expiration</text>
                        <text transform="matrix(1 0 0 1 65.1054 241.5)" class="st7 st5 st8">card number</text>
                        <g>
                            <text transform="matrix(1 0 0 1 574.4219 433.8095)" id="svgexpire" class="st2 st5 st9">01/23</text>
                            <text transform="matrix(1 0 0 1 479.3848 417.0097)" class="st2 st10 st11">VALID</text>
                            <text transform="matrix(1 0 0 1 479.3848 435.6762)" class="st2 st10 st11">THRU</text>
                            <polygon class="st2" points="554.5,421 540.4,414.2 540.4,427.9 		" />
                        </g>
                        <g id="cchip">
                            <g>
                                <path class="st2" d="M168.1,143.6H82.9c-10.2,0-18.5-8.3-18.5-18.5V74.9c0-10.2,8.3-18.5,18.5-18.5h85.3
                        c10.2,0,18.5,8.3,18.5,18.5v50.2C186.6,135.3,178.3,143.6,168.1,143.6z" />
                            </g>
                            <g>
                                <g>
                                    <rect x="82" y="70" class="st12" width="1.5" height="60" />
                                </g>
                                <g>
                                    <rect x="167.4" y="70" class="st12" width="1.5" height="60" />
                                </g>
                                <g>
                                    <path class="st12" d="M125.5,130.8c-10.2,0-18.5-8.3-18.5-18.5c0-4.6,1.7-8.9,4.7-12.3c-3-3.4-4.7-7.7-4.7-12.3
                            c0-10.2,8.3-18.5,18.5-18.5s18.5,8.3,18.5,18.5c0,4.6-1.7,8.9-4.7,12.3c3,3.4,4.7,7.7,4.7,12.3
                            C143.9,122.5,135.7,130.8,125.5,130.8z M125.5,70.8c-9.3,0-16.9,7.6-16.9,16.9c0,4.4,1.7,8.6,4.8,11.8l0.5,0.5l-0.5,0.5
                            c-3.1,3.2-4.8,7.4-4.8,11.8c0,9.3,7.6,16.9,16.9,16.9s16.9-7.6,16.9-16.9c0-4.4-1.7-8.6-4.8-11.8l-0.5-0.5l0.5-0.5
                            c3.1-3.2,4.8-7.4,4.8-11.8C142.4,78.4,134.8,70.8,125.5,70.8z" />
                                </g>
                                <g>
                                    <rect x="82.8" y="82.1" class="st12" width="25.8" height="1.5" />
                                </g>
                                <g>
                                    <rect x="82.8" y="117.9" class="st12" width="26.1" height="1.5" />
                                </g>
                                <g>
                                    <rect x="142.4" y="82.1" class="st12" width="25.8" height="1.5" />
                                </g>
                                <g>
                                    <rect x="142" y="117.9" class="st12" width="26.2" height="1.5" />
                                </g>
                            </g>
                        </g>
                    </g>
                    <g id="Back">
                    </g>
                </svg>
            </div>
            <div class="back">
                <svg version="1.1" id="cardback" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                    x="0px" y="0px" viewBox="0 0 750 471" style="enable-background:new 0 0 750 471;" xml:space="preserve">
                    <g id="Front">
                        <line class="st0" x1="35.3" y1="10.4" x2="36.7" y2="11" />
                    </g>
                    <g id="Back">
                        <g id="Page-1_2_">
                            <g id="amex_2_">
                                <path id="Rectangle-1_2_" class="darkcolor greydark" d="M40,0h670c22.1,0,40,17.9,40,40v391c0,22.1-17.9,40-40,40H40c-22.1,0-40-17.9-40-40V40
                        C0,17.9,17.9,0,40,0z" />
                            </g>
                        </g>
                        <rect y="61.6" class="st2" width="750" height="78" />
                        <g>
                            <path class="st3" d="M701.1,249.1H48.9c-3.3,0-6-2.7-6-6v-52.5c0-3.3,2.7-6,6-6h652.1c3.3,0,6,2.7,6,6v52.5
                    C707.1,246.4,704.4,249.1,701.1,249.1z" />
                            <rect x="42.9" y="198.6" class="st4" width="664.1" height="10.5" />
                            <rect x="42.9" y="224.5" class="st4" width="664.1" height="10.5" />
                            <path class="st5" d="M701.1,184.6H618h-8h-10v64.5h10h8h83.1c3.3,0,6-2.7,6-6v-52.5C707.1,187.3,704.4,184.6,701.1,184.6z" />
                        </g>
                        <text transform="matrix(1 0 0 1 621.999 227.2734)" id="svgsecurity" class="st6 st7">985</text>
                        <g class="st8">
                            <text transform="matrix(1 0 0 1 518.083 280.0879)" class="st9 st6 st10">security code</text>
                        </g>
                        <rect x="58.1" y="378.6" class="st11" width="375.5" height="13.5" />
                        <rect x="58.1" y="405.6" class="st11" width="421.7" height="13.5" />
                        <text transform="matrix(1 0 0 1 59.5073 228.6099)" id="svgnameback" class="st12 st13"></text>
                    </g>
                </svg>
            </div>
        </div>
    </div>
    <div class="form-container">
        <div class="field-container">
            <label for="name">Name</label>
            <input id="name" maxlength="20" type="text">
        </div>
        <div class="field-container">
            <label for="cardnumber">Card Number</label>
            <input id="cardnumber" type="text" pattern="[0-9]*" inputmode="numeric">
            <svg id="ccicon" class="ccicon" width="750" height="471" viewBox="0 0 750 471" version="1.1" xmlns="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink">

            </svg>
        </div>
        <div class="field-container">
            <label for="expirationdate">Expiration (mm/yy)</label>
            <input id="expirationdate" type="text" pattern="[0-9]*" inputmode="numeric">
        </div>
        <div class="field-container">
            <label for="securitycode">Security Code</label>
            <input id="securitycode" type="text" pattern="[0-9]*" inputmode="numeric">
        </div>
    </div>
    </div>
    </div>

<div class="payment-section" style="display:none">
    <div class="row">
      <div class="col-lg-6">
        <label for="country-select">
          {{ trans('messages.account.country') }}
        </label>

        <div class="select select-block">
          @if(Session::get('payment_country'))

             {!! Form::select('payment_country', $country, Session::get('mobile_payment_counry_code'), ['id' => 'country-select']) !!}
          @else
          {!! Form::select('payment_country', $country, $default_country, ['id' => 'country-select']) !!}
          @endif
        </div>
      </div>
    </div>

    <div class="payment-controls">
        <div class="row">
          <div class="col-sm-12">
            <label for="payment-method-select">
              {{ trans('messages.payments.payment_type') }}
            </label>
          </div>
        </div>
        <div class="row" id="payment-type-select">
          <div class="col-lg-6 row-space-2">
            <div class="select select-block">
              <select name="payment_type" class="grouped-field" id="payment-method-select">
              <!--change for Api payment_type-->

                  <option value="yapstone" data-payment-type="payment-method" data-cc-type="visa" data-cc-name="" data-cc-expire=""  >
                  Credit/ Debit Card
                </option>
				 <!-- <option value="200" data-payment-type="payment-method" data-cc-type="visa" data-cc-name="" data-cc-expire="">
                   Authorize Net
                  </option> -->
              </select>
            </div>
          </div>
          <div class="col-lg-6">
            <div class="payment-method grouped-field cc" style={{ (@Session::get('payment')[$s_key]['payment_card_type']=='PayPal') ? 'display:none;':'display:block'}}  >
              <div class="payment-logo unionpay hide"></div>
              <div class="payment-logo visa">{{ trans('messages.payments.credit_card') }}</div>
              <div class="payment-logo master"></div>
              <div class="payment-logo american_express"></div>
              <div class="payment-logo discover"></div>
              <div class="payment-logo jcb hide"></div>
              <div class="payment-logo postepay hide"></div>
              <i class="icon icon-lock icon-light-gray h3"></i>
                <div class="cc-data hide">
                  <div class="cc-info">
                    {{ trans('messages.payments.name') }}: <span id="selected-cc-name"></span>
                  </div>
                  <div class="cc-info">
                    {{ trans('messages.payments.expires') }}: <span id="selected-cc-expires"></span>
                  </div>
                </div>
            </div>
              <div class="payment-method grouped-field digital_river_cc">
                <div class="payment-logo visa"></div>
                <div class="payment-logo master"></div>
                <div class="payment-logo american_express"></div>
                <div class="payment-logo hipercard"></div>
                <div class="payment-logo elo"></div>
                <div class="payment-logo aura"></div>
                <i class="icon icon-lock icon-light-gray h3"></i>
              </div>
              <div class="payment-method grouped-field paypal {{ (@Session::get('payment')[$s_key]['payment_card_type']=='PayPal') ? 'active':''}} ">
                <div class="payment-logo paypal {{ (@Session::get('payment')[$s_key]['payment_card_type']=='PayPal') ? 'active':''}} ">PayPal</div>
              </div>
          </div>
          <div class="control-group cc-zip col-md-6 cc-zip-retry hide">
            <label for="credit-card-zip">
              {{ trans('messages.payments.postal_code') }}
            </label>

            <input type="text" class="cc-zip-text cc-short cc-short-half" name="zip_retry" id="credit-card-zip-retry">
            <div class="label label-warning inline-error hide"></div>
          </div>
        </div>

    </div>

  <div id="payment-methods-content">
    <div class="payment-method cc active" id="payment-method-cc">
      <div class="payment-method-container">

        <input type="hidden" name="payment_method_nonce" id="payment_method_nonce">

        <div class="new-card">
          <div class="cc-details row">
            <div class="control-group cc-type col-md-6">
              <label class="control-label" for="credit-card-type">
                {{ trans('messages.payments.card_type') }}
              </label>
                <div class="select select-block">
                  <select id="credit-card-type" class="cc-med" name="cc_type">
                      <option value="visa" selected="selected">
                        Visa
                      </option>
                      <option value="master">
                        MasterCard
                      </option>
                      <option value="american_express">
                        American Express
                      </option>
                      <option value="discover">
                        Discover
                      </option>
                  </select>
                </div>
              </div>
            <div class="control-group cc-number col-md-6">
              <label for="credit-card-number">
                {{ trans('messages.payments.card_number') }}
              </label>
                {!! Form::text('cc_number', '', ['class' => 'cc-med card-number', 'id' => 'credit-card-number', 'autocomplete' => 'off']) !!}
              @if ($errors->has('cc_number')) <div class="label label-warning inline-error">{{ $errors->first('cc_number') }}</div> @endif
              </div>
            </div>
            <div class="row">
              <div class="control-group cc-expiration col-md-6">
                <label aria-hidden="true">
                  {{ trans('messages.payments.expires_on') }}
                </label>
                <div class="row row-condensed">
                  <div class="col-sm-6">
                    <div class="select select-block">
                      <label for="credit-card-expire-month" class="screen-reader-only">
                        {{ trans('messages.login.month') }}
                      </label>
                      {!! Form::selectRangeWithDefault('cc_expire_month', 1, 12, null, 'mm', [ 'class' => 'cc-short card-expiry-month', 'id' => 'credit-card-expire-month']) !!}
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="select select-block">
                      <label for="credit-card-expire-year" class="screen-reader-only">
                        {{ trans('messages.login.year') }}
                      </label>
                      {!! Form::selectRangeWithDefault('cc_expire_year', date('Y'), date('Y')+10, null, 'yyyy', [ 'class' => 'cc-short card-expiry-year', 'id' => 'credit-card-expire-year']) !!}
                    </div>
                  </div>
                </div>
                @if ($errors->has('cc_expire_month') || $errors->has('cc_expire_year'))
                <div class="label label-warning inline-error">
                @if ($errors->has('cc_expire_month'))
                  {{ $errors->first('cc_expire_month') }}
                @endif
                @if ($errors->has('cc_expire_month') == '')
                  {{ $errors->first('cc_expire_year') }}
                @endif
                </div>
                @endif
              </div>
              <div class="control-group cc-security-code col-md-4">
                <label class="control-label" for="credit-card-security-code">
                  {{ trans('messages.payments.security_code') }}
                </label>
                <div class="row">
                  <div class="col-sm-6 col-md-8">
                    {!! Form::text('cc_security_code', '', ['class' => 'cc-short card-cvc', 'id' => 'credit-card-security-code', 'autocomplete' => 'off']) !!}
                  </div>
                </div>
                @if ($errors->has('cc_security_code')) <div class="label label-warning inline-error">{{ $errors->first('cc_security_code') }}</div> @endif
              </div>
            </div>




        </div>
      </div>
    </div>



      <div class="payment-method paypal {{ (@Session::get('payment')[$s_key]['payment_card_type']=='PayPal') ? 'active':''}}" id="payment-method-paypal">
        <div class="paypal-instructions row-space-top-2">
          <p>
           {{ trans('messages.payments.redirected_to_paypal') }}
                <strong></strong>
          </p>
        </div>
      </div>

<input name="payment_method" type="hidden" value="{{ (@Session::get('payment')[$s_key]['payment_card_type']!='PayPal') ? 'cc':''}}">
<input name="country" type="hidden" value="">
<input name="digital_river[country]" type="hidden" value="">
</div>
    </div>
</section>
@endif

            <section class="checkout-main__section">
              <div>
                <h2>
                  {{ trans('messages.payments.tell_about_your_trip',['first_name'=>$result->users->first_name]) }}
                </h2>
                <p>
                  {{ trans('messages.payments.helful_trips') }}:
                </p>
                <ul>
                  <li>
                    {{ trans('messages.rooms.what_brings_you',['city'=>$result->rooms_address->city]) }}
                  </li>
                  <li>
                    {{ trans('messages.payments.checkin_plans') }}
                  </li>
                  <li>
                    {{ trans('messages.payments.ask_recommendations') }}
                  </li>
                </ul>

                  <div class="media space-3">
                    <div class="pull-left lang-chang-label">

<div class="media-photo-badge">
@if(Session::get('get_token')=='')
  <a href="{{ url('users/show/'.$result->user_id) }}" class="media-photo media-round"><img alt="User Profile Image" class="" data-pin-nopin="true" height="115" src="{{ $result->users->profile_picture->src }}" title="{{ $result->users->first_name }}" width="115"></a>
  @else
  <a href="javascript:void(0);" class="media-photo media-round"><img alt="User Profile Image" class="" data-pin-nopin="true" height="115" src="{{ $result->users->profile_picture->src }}" title="{{ $result->users->first_name }}" width="115"></a>
  @endif
</div>

                    </div>
                    <div class="media-body">
                      <div class="panel panel-quote panel-dark">
                        <p class="panel-body">
                          @if($result->booking_message)
                           {{ $result->booking_message }}
                           @else
                          {{ trans('messages.payments.welcome_to_city',['city'=>$result->rooms_address->city]) }}
                          @endif
                        </p>
                      </div>
                    </div>
                  </div>
              </div>

                <div class="media">
                  <div class="pull-left lang-chang-label">
@if(Session::get('get_token')!='')
<div class="media-photo-badge">
  <a href="javascript:void(0);" class="media-photo media-round"><img alt="User Profile Image" class="" data-pin-nopin="true" height="115" src="{{ @Session::get('payment')[$s_key]['mobile_user_image'] }}" title="" width="115"></a>
</div>
@else
<div class="media-photo-badge">
  <a href="{{ url('users/show/'.Auth::user()->user()->id) }}" class="media-photo media-round"><img alt="User Profile Image" class="" data-pin-nopin="true" height="115" src="{{ Auth::user()->user()->profile_picture->src }}" title="{{ Auth::user()->user()->first_name }}" width="115"></a>
  </div>
@endif
                  </div>
                  <div class="media-body">
                    <div class="panel panel-quote">
                      <div class="message-to-host control-group">
                        <label for="message-to-host-input" class="screen-reader-only">
                          {{ trans('messages.payments.message_your_host') }}...
                        </label>
                         <!--payment_message_to_host set for Api start -->
                        <textarea id="message-to-host-input" name="message_to_host" rows="3" class="message-to-host-quote-input" placeholder="{{ trans('messages.payments.message_your_host') }}..."> @if(@Session::get('payment')[$s_key]['payment_message_to_host']){{ @Session::get('payment')[$s_key]['payment_message_to_host'] }} @endif</textarea>
                        <!--payment_message_to_host set for Api stop -->
                      </div>
                    </div>
                    <div class="label label-warning inline-error"></div>
                  </div>
                </div>
            </section>



          <section id="house-rules-agreement" class="checkout-main__section">
  <h2 class="section-title">
    {{ trans('messages.lys.house_rules') }}
  </h2>
  <p>
    {{ trans('messages.payments.by_booking_this_space',['first_name'=>$result->users->first_name]) }}.
  </p>
  <div class="row-space-2">
    <div class="expandable expandable-trigger-more house-rules-panel-body expanded">
      <div class="expandable-content" data-threshold="50">
        <p class="comment more">{{ $result->rooms_description->house_rules }}</p>
        <div class="expandable-indicator"></div>
      </div>
    </div>
  </div>
</section>
        <section id="policies" class="policies row-space-3">
          <div class="docs_upload_main">

          <div class="upload_docs">
                 <div id="pdfmsg"></div>

                  <input type="file" id="docsUpload" name="docsUpload" accept="application/pdf" class="btn btn-primary" />


                  <input type="button" id="upload" value="Upload">

          </div>

<?php
  $path = url().'/uploads/rental_contract/'.$room_id.'/'.$rental_contract;
 //$file=base_path().'/uploads/rental_contract/';

?>
                  <div class="file_preview">
                    <a href="{{$path}}" target="_blank">PDF Download</a>
                   </div>

        </div>

          <div class="terms media">

            <div class="media-body">
              <label for="agrees-to-terms">
                {{ trans('messages.payments.by_clicking',['booking_type'=>($booking_type == 'instant_book') ? trans('messages.payments.book_now') : trans('messages.lys.continue')]) }} <a href="{{ url('linen_rental_policies_and_procedures') }}" class="terms_link" target="_blank">Rental Policies</a>,  <a href="{{ url('general_info_and_guidelines') }}" class="cancel-policy-link" target="_blank">General Info</a> {{ trans('messages.header.and') }} <a href="{{ url('rental_agreement_contract') }}" class="refund_policy_link" target="_blank">Rental Contract Agreement</a>.
              </label>
            </div>
          </div>
        </section>
        <p>
          </p>

          <div id="paypal-container"></div>
          <button id="payment-form-submit" type="submit" class="btn btn-large btn-primary">
            {{ ($booking_type == 'instant_book') ? trans('messages.payments.book_now') : trans('messages.lys.continue') }}
          </button>
        <p></p>

        <p class="book-now-explanation default">

        </p>
        <p class="book-now-explanation immediate_charge hide">
          {{ trans('messages.payments.clicking') }} <strong>{{ trans('messages.lys.continue') }}</strong> {{ trans('messages.payments.charge_your_payment') }}
        </p>
        <p class="book-now-explanation deferred_payment hide">
          {{ trans('messages.payments.host_will_reply') }}
        </p>
      </div>
      <div class="col-md-4 row-space-2 lang-ar-left" >
        <div class="panel payments-listing " id="payment-right">
          <div class="media-photo media-photo-block text-center payments-listing-image">
            {!! Html::image('images/'.$result->photo_name, $result->name, ['class' => 'img-responsive-height']) !!}
          </div>
          <div class="panel-body">
            <section id="your-trip" class="your-trip">
              <div class="hosting-info">
                <div class="payments-listing-name h4 row-space-1" style="word-wrap: break-word;">{{ $result->name }}  <p style="font-weight: normal;font-size: 14px;margin: 10px 0px !important;">@if($result->rooms_address->city !='') {{ $result->rooms_address->city }} , @endif
                              @if($result->rooms_address->state !=''){{ $result->rooms_address->state }} @endif
                              @if($result->rooms_address->country_name !='') , {{  $result->rooms_address->country_name }} @endif </p></div>
                <div class="">
                  <hr>
                  <div class="row-space-1">
                    <strong>
                    {{ $result->room_type_name }}
                    </strong> {{ trans('messages.payments.for') }} <strong>{{ $number_of_guests }} {{ trans_choice('messages.home.guest',$number_of_guests) }}</strong>
                  </div>
                  <div>
                    <strong>{{ date('D, M d, Y', strtotime($checkin)) }}</strong> {{ trans('messages.payments.to') }} <strong>{{ date('D, M d, Y', strtotime($checkout)) }}</strong>
                  </div>
                </div>
                <hr>
                <table class="reso-info-table">
                  <tbody>
                    <tr>
                      <td>{{ trans('messages.payments.cancellation_policy') }}</td>
                      <td class="text-right">
                        @if($reservation_id!='')
                        <a href="{{ url('home/cancellation_policies#').strtolower($cancellation) }}" class="cancel-policy-link" target="_blank">{{ $cancellation }} </a>
                        @else
                        <a href="{{ url('home/cancellation_policies#').strtolower($result->cancel_policy) }}" class="cancel-policy-link" target="_blank">{{ $result->cancel_policy }} </a>
                        @endif
                      </td>
                    </tr>
                    <tr>
                      <td>{{ trans('messages.lys.house_rules') }}</td>
                      <td class="text-right">
                        <a href="#house-rules-agreement" class="house-rules-link">{{ trans('messages.payments.read_policy') }}</a>
                      </td>
                    </tr>
                    <tr>
                      <td>
                        {{ ucfirst(trans_choice('messages.rooms.night',2)) }}
                      </td>
                      <td class="text-right">
                        {{ $nights }}
                      </td>
                    </tr>
                  </tbody>
                </table>
                <hr>
                <section id="billing-summary" class="billing-summary">
                    <div class="tooltip tooltip-top-middle taxes-breakdown" role="tooltip" data-sticky="true" data-trigger="#tax-tooltip" aria-hidden="true">
    <div class="panel-body">
      <table>
        <tbody><tr>
          <td colspan="2"></td>
        </tr>
      </tbody></table>
    </div>  </div>
  <div class="tooltip tooltip-top-middle makent-credit-breakdown" role="tooltip" data-sticky="true" data-trigger="#makent-credit-tooltip" aria-hidden="true">
    <div class="panel-body">
      <table class="table makent-credit-breakdown">
      </table>
    </div>
  </div>
  <table id="billing-table" class="reso-info-table billing-table">
    <tbody>

    <tr class="base-price">
      <td class="name pos-rel">
       <span class="lang-chang-label">
       @if(Session::get('get_token')!='')
        {{ Session::get('currency_symbol') }}
        @else
        {{ $result->rooms_price->currency->symbol }}
          @endif
          </span>{{ $price_list->rooms_price }}  x {{ $nights }} {{ trans_choice('messages.rooms.night',$nights) }}
         <i id="service-fee-tooltip" rel="tooltip" title="{{ trans('messages.rooms.avg_night_rate') }}" style="position:relative;"  >

        </i>    </td>
      <td class="val text-right">
      <span class="lang-chang-label"> @if(Session::get('get_token')!='')
        {{ Session::get('currency_symbol') }}
        @else
        {{ $result->rooms_price->currency->symbol }}
          @endif</span><span >{{ $price_list->total_night_price }}</td>
    </tr>

    @if($price_list->service_fee)
    <tr class="service-fee">
      <td class="name pos-rel">
        {{ trans('messages.rooms.service_fee') }}
<i id="service-fee-tooltip" class="icon icon-question" rel="tooltip" title="{{ trans('messages.rooms.24_7_help') }}" style="position:relative;">

        </i>
      </td>
      <td class="val text-right">
      <span class="lang-chang-label"> @if(Session::get('get_token')!='')
        {{ Session::get('currency_symbol') }}
        @else
        {{ $result->rooms_price->currency->symbol }}
          @endif</span><span >{{ $price_list->service_fee }}</span></td>
    </tr>
    @endif

    @if($price_list->additional_guest)
      @if(@$special_offer_id == '' || @$special_offer_type == 'pre-approval' )
        <tr class="additional_price">
          <td class="name">
            {{ trans('messages.rooms.addtional_guest_fee') }}
          </td>
        <td class="val text-right">
        <span class="lang-chang-label">
          @if(Session::get('get_token')!='')
            {{ Session::get('currency_symbol') }}
            @else
            {{ $result->rooms_price->currency->symbol }}
              @endif</span><span>{{ $price_list->additional_guest }}</span></td>
        </tr>
        @endif
    @endif

    @if($price_list->security_fee)
      @if(@$special_offer_id =='' || @$special_offer_type == 'pre-approval')
        <tr class="security_price">
          <td class="name">
            {{ trans('messages.payments.security_deposit') }}
          </td>
        <td class="val text-right">
        <span class="lang-chang-label">
          @if(Session::get('get_token')!='')
            {{ Session::get('currency_symbol') }}
            @else
            {{ $result->rooms_price->currency->symbol }}
              @endif</span>
        <span >{{ $price_list->security_fee }}</span></td>
        </tr>
      @endif
    @endif

    @if($price_list->cleaning_fee)
      @if(@$special_offer_id =='' || @$special_offer_type == 'pre-approval')
        <tr class="cleaning_price">
          <td class="name">
            {{ trans('messages.lys.cleaning') }}
          </td>
        <td class="val text-right">
        <span class="lang-chang-label">
          @if(Session::get('get_token')!='')
            {{ Session::get('currency_symbol') }}
            @else
            {{ $result->rooms_price->currency->symbol }}
              @endif</span><span >{{ $price_list->cleaning_fee }}</span></td>
        </tr>
        @endif
    @endif

    @if($price_list->pet_fee)
      @if(@$special_offer_id =='' || @$special_offer_type == 'pre-approval')
        <tr class="pet_price">
          <td class="name">
           Pet Fee
          </td>
          <td class="val text-right">
        <span class="lang-chang-label">
          @if(Session::get('get_token')!='')
            {{ Session::get('currency_symbol') }}
          @else
            {{ $result->rooms_price->currency->symbol }}
          @endif</span><span >{{ $price_list->pet_fee }}</span></td>
        </tr>
      @endif
    @endif

    @if($price_list->pool_fee)
      @if(@$special_offer_id =='' || @$special_offer_type == 'pre-approval')
        <tr class="pool_price">
          <td class="name">
            Pool Fee
          </td>
          <td class="val text-right">
        <span class="lang-chang-label">
          @if(Session::get('get_token')!='')
            {{ Session::get('currency_symbol') }}
          @else
            {{ $result->rooms_price->currency->symbol }}
          @endif</span><span >{{ $price_list->pool_fee }}</span></td>
        </tr>
      @endif
    @endif

    @if($price_list->admin_fee)
      @if(@$special_offer_id =='' || @$special_offer_type == 'pre-approval')
        <tr class="admin_price">
          <td class="name">
            Admin Fee
          </td>
          <td class="val text-right">
        <span class="lang-chang-label">
          @if(Session::get('get_token')!='')
            {{ Session::get('currency_symbol') }}
          @else
            {{ $result->rooms_price->currency->symbol }}
          @endif</span><span >{{ $price_list->admin_fee }}</span></td>
        </tr>
      @endif
    @endif
    @if($price_list->tax_fee)
      @if(@$special_offer_id =='' || @$special_offer_type == 'pre-approval')
        <tr class="admin_price">
          <td class="name">
            Tax
          </td>
          <td class="val text-right">
        <span class="lang-chang-label">
          @if(Session::get('get_token')!='')
            {{ Session::get('currency_symbol') }}
          @else
            {{ $result->rooms_price->currency->symbol }}
          @endif</span><span >{{ $price_list->tax_fee }}</span></td>
        </tr>
      @endif
    @endif

      <tr class="editable-fields" id="after_apply">
        <td colspan="2">
          <div class="row-condensed clearfix row-space-1">
            <div class="col-sm-7">
              <input autocomplete="off" class="coupon-code-field" name="coupon_code" type="text" value="">
            </div>
            <div class="col-sm-5">
              <a href="javascript:void(0);" id="apply-coupon" class="btn btn-block apply-coupon">{{ trans('messages.payments.apply') }}</a>
            </div>
          </div>

          <p id="coupon_disabled_message" class="icon-rausch" style="display:none"></p>
          <a href="javascript:;" class="cancel-coupon">{{ trans('messages.your_reservations.cancel') }}</a>
        </td>
      </tr>
@if($reservation_id!='' || $booking_type == 'instant_book')
    <tr class="coupon">
        <td class="name">
          <span class="without-applied-coupon">
          <span class="coupon-section-link" id="after_apply_coupon" style="{{ (Session::has('coupon_amount')) ? 'display:Block;' : 'display:none;' }}">
          @if($travel_credit !=0 )
            {{ trans('messages.referrals.travel_credit') }}
          @else
            {{ trans('messages.payments.coupon') }}
          @endif
          </span>
          </span>

        </td>
        <td class="val text-right">
          <div class="without-applied-coupon label label-success" id="after_apply_amount" style="{{ (Session::has('coupon_amount')) ? 'display:Block;' : 'display:none;' }}">
           -{{ $result->rooms_price->currency->symbol }}<span id="applied_coupen_amount">{{ $price_list->coupon_amount }}</span>
          </div>
        </td>
      </tr>

      <tr id="after_apply_remove" style="{{ (Session::has('coupon_amount')) ? '' : 'display:none;' }}">
      <td>
      <a data-prevent-default="true" href="javascript:void(0);" id="remove_coupon">
      <span>
          @if($travel_credit !=0 )
            {{ trans('messages.referrals.remove_travel_credit') }}
          @else
          {{ trans('messages.payments.remove_coupon') }}
          @endif
      </span>
      </a>
      </td>
      <td>
      </td>
      </tr>
      @endif

    </tbody>
  </table>

  <hr>

  <table id="payment-total-table" class="reso-info-table billing-table">
    <tbody>
      <tr class="total">
        <td class="name"><span class="h4">{{ trans('messages.rooms.total') }}</span></td>
        <td class="text-special icon-dark-gray text-right"><span class="h4">
          @if(Session::get('get_token')!='')
             {{ Session::get('currency_symbol') }}
          @else
           {{ $result->rooms_price->currency->symbol }}
          @endif
        </span> <span class="h4" id="payment_total">{{ $price_list->total }}</span></td>


      </tr>

    </tbody>
  </table>

  <hr>
  <?php //echo "<pre>";print_r($price_list);die;?>
  <!-- <table id="payment-total-table" class="reso-info-table billing-table">
    <tbody>
      <tr class="total">
        <td class="name"><span class="h3">{{ $price_list->installmentLabel }}</span></td>
        <td class="text-special icon-dark-gray text-right"><span class="h3">
          @if(Session::get('get_token')!='')
             {{ Session::get('currency_symbol') }}
          @else
           {{ $result->rooms_price->currency->symbol }}
          @endif
        </span> <span class="h3" id="payment_total">{{ $price_list->installmentpay }}</span></td>


      </tr>

    </tbody>
  </table> -->

<div class="panel-total-charge">


  <small><div>
    <span id="currency-total-charge" class="">
      {{ trans('messages.payments.you_are_paying_in') }}
      <strong><span id="payment-currency" >{{PAYPAL_CURRENCY_SYMBOL}}{{PAYPAL_CURRENCY_CODE}}</span></strong>.
      {{ trans('messages.payments.total_charge_is') }}
      <strong><span id="payment-total-charge">{{PAYPAL_CURRENCY_SYMBOL}}{{ $paypal_price }}</span></strong>.
    </span>
    <span id="fx-messaging">All forms of payment are subject to rate conversion and transaction fees for non-USD or foreign banks.</span>
  </div></small>
</div>

                </section>
              </div>
            </section>
          </div>
        </div>
      </div>

    </div>
  </form>

  <div id="house-rules-modal" class="modal" role="dialog" aria-hidden="true">
    <div class="modal-table">
      <div class="modal-cell">
        <div class="modal-content">
          <div class="panel-header">
            <a href="javascript:void(0);" class="panel-close" data-behavior="modal-close">
              ×
              <span class="screen-reader-only">
                {{ trans('messages.payments.house_rules') }}
              </span>
            </a>
            {{ trans('messages.payments.house_rules') }}
          </div>
          <div class="panel-body"><p>{{ $result->rooms_description->house_rules }}</p></div>
        </div>
      </div>
    </div>
  </div>

  <div id="security-deposit-modal" class="modal" role="dialog" data-trigger="#security-deposit-modal-trigger" aria-hidden="true">
    <div class="modal-table">
      <div class="modal-cell">
        <div class="modal-content">
          <div class="panel-header">
            <a href="{{ url('payments/book?hosting_id=3357255&s=q315#') }}" class="panel-close" data-behavior="modal-close">
              ×
              <span class="screen-reader-only">
                {{ trans('messages.payments.security_deposit') }}
              </span>
            </a>
            {{ trans('messages.payments.security_deposit') }}
          </div>
          <div class="panel-body">
            <p>
              {{ trans('messages.payments.security_deposit_collected',['site_name'=>$site_name]) }}
            </p>
            <p>
              {{ trans('messages.payments.host_reports_problem',['site_name'=>$site_name]) }}
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

    </main>


    <div id="gmap-preload" class="hide"></div>

<div class="ipad-interstitial-wrapper"><span data-reactid=".1"></span></div>


    <div id="fb-root" class=" fb_reset"><div style="position: absolute; top: -10000px; height: 0px; width: 0px;"><div></div></div><div style="position: absolute; top: -10000px; height: 0px; width: 0px;"><div></div></div></div>

<div class="tooltip tooltip-top-middle" role="tooltip" data-trigger="#tooltip-cvv" aria-hidden="true">
      <div class="tooltip-cvv"></div>
    </div><div class="tooltip tooltip-bottom-middle" role="tooltip" aria-hidden="true">  <p class="panel-body">{{ trans('messages.payments.fee_charged_by',['site_name'=>$site_name]) }}</p></div></body></html>

@stop
@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/imask/3.4.0/imask.min.js"></script>
<script src="{{url()}}/resources/assets/js/payment_card.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>


<script type="text/javascript">
$(document).ready(function() {




	var showChar = 100;
	var ellipsestext = "...";
	var moretext = "+ More";
	var lesstext = "- Less";
	$('.more').each(function() {
		var content = $(this).html();

		if(content.length > showChar) {

			var c = content.substr(0, showChar);
			var h = content.substr(showChar-1, content.length - showChar);

			var html = c + '<span class="moreelipses">'+ellipsestext+'</span>&nbsp;<span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<strong> <a href="" class="morelink">'+moretext+'</a></strong></span>';

			$(this).html(html);
		}

	});

	$(".morelink").click(function(){
		if($(this).hasClass("less")) {
			$(this).removeClass("less");
			$(this).html(moretext);
		} else {
			$(this).addClass("less");
			$(this).html(lesstext);
		}
		$(this).parent().prev().toggle();
		$(this).prev().toggle();
		return false;
	});
});
  if(typeof $.stickysidebarscroll !== "undefined"){
     if ($(window).width() > 760){
      $.stickysidebarscroll("#payment-right",{offset: {top: 20, bottom: 20}});
    }
       }
  $(window).resize(function () {
    $(window).scrollTop( 0 );
  });

</script>
<script type="text/javascript">
  $('#payment-method-select').change(function(){
    var authorize = $('#payment-method-select').val();
    if(authorize=='yapstone'){
        $(this).closest('form').attr('action', '{{ url('payments/yapstone_booking') }}')
    }else if (authorize==200) {
    //alert('if');
      $(this).closest('form').attr('action', '{{ url('payments/authorize_booking') }}')

    }else{
        $(this).closest('form').attr('action', '{{ url('payments/create_booking') }}')
    }

});

//PDF UPLoad Here

    $('#upload').on('click', function(event){
      var file= $('#docsUpload').val();
      if(file==''){
        alert('Please Select File.');
        return false;
      }

      var data = new FormData();
      data.append('docsUpload', $('#docsUpload').prop('files')[0]);
      $.ajax({
        url:"{{url('payments/upload')}}",
        method:"POST",
        data:data,
        dataType:'JSON',
        contentType: false,
        cache: false,
        processData: false,
        success:function(data)
        {
             $('#pdfmsg').html(data.message);

        }
        });
    });


</script>
@if($reservation_id!='' || $booking_type == 'instant_book')
<script>
    $(function() {
      var $form = $("#checkout-form");
      $('form#checkout-form').bind('submit', function(e) {
        e.preventDefault();
        Stripe.setPublishableKey($(this).data('stripe-publishable-key'));
        Stripe.createToken({
          number: $('.card-number').val(),
          cvc: $('.card-cvc').val(),
          exp_month: $('.card-expiry-month').val(),
          exp_year: $('.card-expiry-year').val()
        }, stripeResponseHandler);

      });

      function stripeResponseHandler(status, response) {
            if (response.error) {
                $('.error')
                    .removeClass('hide')
                    .find('.alert')
                    .text(response.error.message);
            } else {
                // token contains id, last4, and card type
                var token = response['id'];
                // insert the token into the form so it gets submitted to the server
                $form.find('input[type=text]').empty();
                $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
                $form.get(0).submit();
            }
        }

    });
</script>
@endif
@endpush
