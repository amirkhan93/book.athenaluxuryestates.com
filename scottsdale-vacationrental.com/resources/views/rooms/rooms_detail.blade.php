@extends('template')

@section('main')

<?php

$min_stay = $result->min_stay == 0 ? 1 : $result->min_stay;

?>

<style>
#menu-wrap{
  position:relative;
  background: #000;
    height: 100px;
}
.ui-datepicker-target {
    padding: 8px 6px;
  }
  .btn .icon:first-child {
      padding: 0;
      margin: 0;
  }
  .supporting-height {
      height: 212px;
  }

  .photo-grid {
    margin-top: 100px;
  }
  @media (min-width: 768px){
.col-md-3 {
    width: 25%;
    float: left;
}
}

  @media(max-width:767px) {
      .menu a{
          color: #000 !important;
      }
      .logo{
          font-size: 22px;
      }
  }
</style>
<div class="loader">
        <div class="loader__figure"></div>
</div>
<div id="site-content" role="main" ng-controller="rooms_detail">

<div class="subnav-container">

  <div data-sticky="true" data-transition-at="#summary" aria-hidden="true" class="subnav section-titles">
    <div class="page-container-responsive">
      <ul class="subnav-list">
        <li>
          <a href="#photos" aria-selected="true" class="subnav-item">
            {{ trans_choice('messages.header.photo',2) }}
          </a>
        </li>
        <li>
          <a href="#summary" class="subnav-item" data-extra="#summary-extend" >
            {{ trans('messages.rooms.about_this_listing') }}
          </a>
        </li>
        <li>
          <a href="#reviews" class="subnav-item">
            {{ trans_choice('messages.header.review',2) }}
          </a>
        </li>
        <li>
          <a href="#host-profile" class="subnav-item">
            {{ trans('messages.rooms.the_host') }}
          </a>
        </li>
        <li>
          <a href="#neighborhood" class="subnav-item">
            {{ trans('messages.your_trips.location') }}
          </a>
        </li>
      </ul>
    </div>
  </div>
</div>

<div id="og_pro_photo_prompt" class="container"></div>

<div id="room" itemscope="" itemtype="http://schema.org/Product">


    <div id="photo-gallery" class="top_gallery_detail">
    <div class="row">
    {{--*/ $i = 1/*--}}
    @foreach($rooms_photos as $row_photos)
      @if(count($rooms_photos) == 1)
        <div class="col-md-6 full_col">
            <div class="full_img">
              <a class=" photo-trigger gallery" href="{{ url('rooms/'.$result->id.'/slider') }}" data-index="1" data-lightbox-type="iframe">
              <img src="{{ url('images/rooms/'.$room_id.'/'.$row_photos['name']) }}" /> </a>
            </div>
        </div>
      @else
        @if($i == 1)
          <div class="col-md-6 full_col">
              <div class="full_img">
                <a class=" photo-trigger gallery" href="{{ url('rooms/'.$result->id.'/slider') }}" data-index="1" data-lightbox-type="iframe">
                <img src="{{ url('images/rooms/'.$room_id.'/'.$row_photos['name']) }}" /> </a>
              </div>
          </div>
        @endif
          @if($i == 2)
          <div class="col-md-6 hide_on_mobile" >
            <div class="col-md-6 half_col">
              <a class=" photo-trigger gallery" href="{{ url('rooms/'.$result->id.'/slider') }}" data-index="2" data-lightbox-type="iframe">
                <img src="{{ url('images/rooms/'.$room_id.'/'.$row_photos['name']) }}" />
              </a>
            </div>
          @endif
          @if ($i == 3)
            <div class="col-md-6 half_col">
              <a class=" photo-trigger gallery" href="{{ url('rooms/'.$result->id.'/slider') }}" data-index="3" data-lightbox-type="iframe">
                <img src="{{ url('images/rooms/'.$room_id.'/'.$row_photos['name']) }}" />
              </a>
            </div>
          @endif
          @if ($i == 4)
            <div class="col-md-6 half_col">
              <a class=" photo-trigger gallery" href="{{ url('rooms/'.$result->id.'/slider') }}" data-index="4" data-lightbox-type="iframe">
                <img src="{{ url('images/rooms/'.$room_id.'/'.$row_photos['name']) }}" />
              </a>
            </div>
          @endif
          @if ($i == 5)
            <div class="col-md-6 half_col">
              <a class=" photo-trigger gallery" href="{{ url('rooms/'.$result->id.'/slider') }}" data-index="5" data-lightbox-type="iframe">
                <img src="{{ url('images/rooms/'.$room_id.'/'.$row_photos['name']) }}" />
              </a>
              <div class="more_images">
                <a class=" photo-trigger gallery" href="{{ url('rooms/'.$result->id.'/slider') }}" data-index="5" data-lightbox-type="iframe">
                {{ trans('messages.rooms.see_all') }} {{ round(count($rooms_photos))}} {{ trans_choice('messages.header.photo',2) }}
                </a>
              </div>
            </div>
          @endif
          @if ($i == count($rooms_photos))
            </div>
          @endif
      @endif
      {{--*/ $i++ /*--}}
    @endforeach
    </div>

        <!-- {{--*/ $i = 1 /*--}}


               <div class="row">
                @foreach($rooms_photos as $row_photos)

                 @if(count($rooms_photos) == 1)

                 <div class="col-6 row-full-height img-box1">
                   <a class="photo-grid-photo photo-trigger gallery" style="background-image: url({{ url('images/rooms/'.$room_id.'/'.$row_photos['name']) }})" href="{{ url('rooms/'.$result->id.'/slider') }}" data-index="1" data-lightbox-type="iframe">

                  </a>
              </div>
               @else


                @if($i == 1)
                 <div class="col-6 row-full-height img-box1">
                <a class="photo-grid-photo photo-trigger gallery" style="background-image: url({{ url('images/rooms/'.$room_id.'/'.$row_photos['name']) }})" href="{{ url('rooms/'.$result->id.'/slider') }}" data-index="1" data-lightbox-type="iframe">

</a>
        </div>
                @endif
                @if ($i == 2)
                <div class="col-6 right_gal_col">
                <div class="row">
                @endif
                @if($i==2 && $i >1)
        <div class="col-6 supporting-height img-box1">
          <a class="photo-grid-photo photo-trigger gallery" style="background-image: url({{ url('images/rooms/'.$room_id.'/'.$row_photos['name']) }})" href="{{ url('rooms/'.$result->id.'/slider') }}" data-index="2" data-lightbox-type="iframe">

      </a>
        </div>
        @endif

        @if($i==3 && $i >2)
        <div class="col-6 supporting-height img-box1">
          <a class="photo-grid-photo photo-trigger gallery" style="background-image: url({{ url('images/rooms/'.$room_id.'/'.$row_photos['name']) }})" href="{{ url('rooms/'.$result->id.'/slider') }}" data-index="3" data-lightbox-type="iframe">

          </a>
        </div>
        @endif
        @if($i == 3)
                </div>
        @endif
        @if($i==4 && $i >3)

        <div class="supporting-height img-box1">
              <div class="media-photo media-photo-block row-full-height">
                <div class="media-cover media-cover-dark img-box1">
                  <a class="photo-grid-photo photo-trigger gallery"
   style="background-image: url({{ url('images/rooms/'.$room_id.'/'.$row_photos['name']) }})"
   href="{{ url('rooms/'.$result->id.'/slider') }}"
   data-index="5" data-lightbox-type="iframe">
  <img src="{{ url('images/rooms/'.$room_id.'/'.$row_photos['name']) }}"
       class="hide"
       alt="Private shower/Longterm/Decent B&amp;B">
</a>
                </div>
                <a class="photo-trigger gallery"
                   href="{{ url('rooms/'.$result->id.'/slider') }}"
                   data-index="5" data-lightbox-type="iframe">
                  <div class="row row-table supporting-height">
                    <div class="col-6 col-middle text-center text-contrast">
                      <div class="h5">
                        {{ trans('messages.rooms.see_all') }} {{ round(count($rooms_photos))}} {{ trans_choice('messages.header.photo',2) }}
                      </div>
                    </div>
                  </div>
                </a></div></div>
                @endif
                @if ($i == count($rooms_photos))
                </div>
                @endif
                @endif
                {{--*/ $i++ /*--}}
                @endforeach
    </div> -->
  </div>

  <div id="summary" class="panel room-section">
    <div class="container">

      <div class="row">
        <div class="col-lg-8 lang-chang-label col-sm-12">
        <!-- <ol class="breadcrumb room_detail_breadcrumb">
          <li>
        <a href="{{ url() }}">Home</a>
        </li>
        <li>
                <a href="{{ url() }}/search/{{ str_replace('+', '%20', urlencode($result->rooms_address->state)) }}/Guest">{{$result->rooms_address->state}}</a>
            </li>
            <li class="active">
                {{$result->name}}
            </li>
        </ol> -->
<div class="row-space-4 row-space-top-4 summary-component">
  <div class="row">



    <div class="col-md-12">


      <h1 itemprop="name" class="overflow h3 row-space-1 text-center-sm" id="listing_name">
        {{ $result->name }}
      </h1>


      <div id="display-address" class="row-space-2 text-muted text-center-sm" itemprop="aggregateRating" itemscope="">

        <a href="" class="link-reset"><span class="lang-chang-label">{{$result->rooms_address->city}} @if($result->rooms_address->city !=''),</span><span class="lang-chang-label">@endif {{$result->rooms_address->state}} @if($result->rooms_address->state !=''),</span><span class="lang-chang-label">@endif {{$result->rooms_address->country_name}}</span></a>
        &nbsp;
        @if($result->overall_star_rating)
        <a href="#reviews" class="link-reset hide-sm">
        <div class="star-rating-wrapper">
        {!! $result->overall_star_rating !!}
        <span> </span>
        <span>
        <small>
        <span>(</span>
        <span>{{ $result->reviews->count() }}</span>
        <span>)</span>
        </small>
        </span>
        </div>
        </a>
        @endif
      </div>

      <div class="row row-condensed text-muted text-center roomtype-img">
          <div class="col-sm-2">
          @if( $result->room_type_name == "Private room" )
            <i class="icon icon-private-room icon-size-2"></i>
          @elseif($result->room_type_name == "Entire home/apt")
          <i class="icon icon-entire-place icon-size-2"></i>
          @else
          <i class="icon icon-shared-room icon-size-2"></i>
          @endif
          </div>
          <div class="col-sm-2">
            <i class="icon icon-group icon-size-2"></i>
          </div>
          <div class="col-sm-2">
            <i class="icon icon-double-bed icon-size-2"></i>
          </div>
          <div class="col-sm-2">
            <i class="icon icon-bathtub icon-size-2"></i>
          </div>
          <div class="col-sm-2">
            <i class="icon icon-calendar icon-size-2"></i>
          </div>
      </div>

    </div>

  </div>

  <div class="row">



    <div class="col-md-12">
      <div class="row row-condensed text-muted text-center roomtype-img">
          <div class="col-sm-2">
            {{ $result->room_type_name }}
          </div>
          <div class="col-sm-2">
            {{ $result->accommodates }} {{ trans_choice('messages.home.guest',2) }}
          </div>
          <div class="col-sm-2">
            {{ $result->beds}} {{ trans('messages.lys.beds') }}
          </div>
          <div class="col-sm-2">
            {{ $result->bathrooms}} {{ trans('messages.lys.bathrooms') }}
          </div>
          <div class="col-sm-2">
            {{ $min_stay}} {{ trans_choice('messages.reviews.day', 2) }}
          </div>
      </div>
    </div>

  </div>
</div>

        </div>
        <div class="col-lg-4 col-sm-12" style="top:1px;">

<div id="tax-descriptions-tip" class="tooltip tooltip-top-middle" role="tooltip" data-sticky="true" data-trigger="#tax-descriptions-tooltip">
</div>
  <form accept-charset="UTF-8" action="{{ url('payments/book/'.$room_id) }}" id="book_it_form" method="post">
        {!! Form::token() !!}
  <h4 class="screen-reader-only">
    {{ trans('messages.rooms.request_to_book') }}
  </h4>
  <div id="pricing" itemprop="offers" itemscope="" class="">
    <div id="price_amount" class="book-it-price-amount pull-left h3 text-special" style="padding: 10px 0;margin-top: 0;"><span class="lang-chang-label">{{ $result->rooms_price->currency->symbol }}</span> <span  id="rooms_price_amount" class="lang-chang-label" value="">{{ $result->rooms_price->night }}</span>
    @if($result->booking_type == 'instant_book')
    <span aria-label="Book Instantly"  class="h3 icon-beach" style="position:relative;">
     <i class="icon icon-instant-book icon-flush-sides tool-amenity1"  ></i>
      <div class="tooltip-amenity tooltip-bottom-middle tooltip-amenity1"  role="tooltip" data-sticky="true" aria-hidden="true" style="left: -132px; top: -120px; display: none;">
          <dl class="panel-body" style="padding:18px;">
            <dt style="    font-weight: bold !important;
    font-size: 15px !important;">Instant Book</dt>
            <dt style="    font-size: 13px;
    line-height: 22px;
    padding-top: 10px;">Book without waiting for the host to respond</dt>
          </dl>
        </div>
    </span>
    @endif
</div>
    <i class="icon icon-bolt icon-beach pull-left h3 pricing__bolt"></i>

    <div id="payment-period-container" class="pull-right">
      <div id="per_night" class="per-night">
        {{ trans('messages.rooms.per_night') }}
      </div>
      <div id="per_month" class="per-month hide">
        {{ trans('messages.rooms.per_month') }}
        <i id="price-info-tooltip" class="icon icon-question hide" data-behavior="tooltip"></i>
      </div>
    </div>
  </div>

  <div id="book_it" class="display-subtotal" style="top: -1px;">
    <div class="panel book-it-panel">
      <div class="panel-body panel-light">
        <div class="form-fields">
          <div class="row row-condensed space-3">
            <div class="col-md-12 col-sm-12 lang-chang-label">
              <div class="row-condensed">
                <div class="col-sm-4 space-1-sm lang-chang-label">
                  <label for="checkin">
                    {{ trans('messages.home.checkin') }}
                  </label>
                  <input readonly="readonly" class="checkin ui-datepicker-target" autocomplete="off" id="list_checkin" name="checkin" placeholder="{{ trans('messages.rooms.dd-mm-yyyy') }}" type="text">
                </div>

                <input readonly="readonly" type="hidden" ng-model="room_id" ng-init="room_id = {{ $room_id }}">
                <input type="hidden" id="room_blocked_dates" value="" >
                <input type="hidden" id="calendar_available_price" value="" >
                <input type="hidden" id="room_available_price" value="" >
                <input type="hidden" id="price_tooltip" value="" >
                <input type="hidden" id="weekend_price_tooltip" value="" >
                <input type="hidden" id="url_checkin" value="{{ $checkin }}" >
                <input type="hidden" id="url_checkout" value="{{ $checkout }}" >
                <input type="hidden" id="url_guests" value="{{ $guests }}" >
                <input type="hidden" name="booking_type" id="booking_type" value="{{ $result->booking_type }}" >
                <input type="hidden" name="cancellation" id="cancellation" value="{{ $result->cancel_policy }}" >

                <div class="col-sm-4 space-1-sm">
                  <label for="checkout">
                    {{ trans('messages.home.checkout') }}
                  </label>

                  <input readonly="readonly" class="checkout ui-datepicker-target" autocomplete="off" id="list_checkout" data-minstay="{{ $min_stay}}" name="checkout" placeholder="{{ trans('messages.rooms.dd-mm-yyyy') }}" type="text">
                </div>
                <div class="col-md-4 col-sm-4">
              <label for="number_of_guests">
                {{ trans_choice('messages.home.guest',2) }}
              </label>
              <div class="select select-block">
                <select id="number_of_guests" name="number_of_guests">
                @for($i=1;$i<= $result->accommodates;$i++)
                <option value="{{ $i }}"> {{ $i }}</option>
                 @endfor
                </select>
              </div>
            </div>
              </div>
            </div>
            
          </div>

          <div class="simple-dates-message-container hide">
            <div class="media text-kazan space-top-2 space-2">
              <div class="pull-left message-icon">
                <i class="icon icon-currency-inr"></i>
              </div>
              <div class="media-body">
                <strong>
                  {{ trans('messages.search.enter_dates') }}
                </strong>
              </div>
            </div>
          </div>
        </div>
        <div class="js-book-it-status">
          <div class="js-book-it-enabled clearfix">
            <div class="js-subtotal-container book-it__subtotal panel-padding-fit" style="display:none;">
            <div class="total_price_main">
            <ul>
            <li>
            <div>
           <h5>{{ trans('messages.rooms.total') }}</h5>
           <p>Includes taxes and fees</p>
           </div>
           <div>
             <p>
           <strong>
           {{ $result->rooms_price->currency->symbol }} <span class="lang-chang-label"></span><span  id="total" value="">0</span>
           </strong>
</p>

           <a tabindex="0"
            class=""
            href="javascript:void(0)"
                             id="view_details_popup"
           >View Details</a>
           <div class="details_popup">
                                <table class='table price_table' >
                                <tbody>
                                  <tr>
                                    <td class='pos-rel room-night'>
                                      <span class='lang-chang-label'> {{ $result->rooms_price->currency->symbol }}</span>  <span  class='lang-chang-label' id='rooms_price_amount_1' >{{ $result->rooms_price->night }}</span> <span class='lang-chang-label'>  x </span><span  id='total_night_count'>0</span> {{ trans_choice('messages.rooms.night',1) }}
                                      <i id='service-fee-tooltip' rel='tooltip' class='icon icon-question' title='{{ trans('messages.rooms.avg_night_rate') }}' ></i>
                                    </td>
                                    <td><span class='lang-chang-label'>{{ $result->rooms_price->currency->symbol }}</span><span  id='total_night_price'>0</span></td>
                                  </tr>
                                  <tr class='additional_price'>
                                    <td>
                                      {{ trans('messages.rooms.addtional_guest_fee') }}
                                    </td>
                                    <td>{{ $result->rooms_price->currency->symbol }}<span  id='additional_guest'>0</span></td>
                                  </tr>

                                  <tr class='security_price'>
                                    <td>
                                      {{ trans('messages.rooms.security_fee') }}
                                    </td>
                                    <td>{{ $result->rooms_price->currency->symbol }}<span  id='security_fee'>0</span></td>
                                  </tr>
                                  <tr class='cleaning_price'>
                                    <td>
                                      {{ trans('messages.rooms.cleaning_fee') }}
                                    </td>
                                    <td>{{ $result->rooms_price->currency->symbol }}<span  id='cleaning_fee'>0</span></td>
                                  </tr>
                                  <tr class='pet_price'>
                                    <td>
                                      Pet Fee
                                    </td>
                                    <td>{{ $result->rooms_price->currency->symbol }}<span  id='pet_fee'>0</span></td>
                                  </tr>
                                  <tr class='pool_price'>
                                    <td>
                                      Pool Fee
                                    </td>
                                    <td>{{ $result->rooms_price->currency->symbol }}<span  id='pool_fee'>0</span></td>
                                  </tr>
                                  <tr class='admin_price'>
                                    <td>
                                      Admin Fee
                                    </td>
                                    <td>{{ $result->rooms_price->currency->symbol }}<span  id='admin_fee'>0</span></td>
                                  </tr>
                                  <tr class = 'subtotal_price'>
                                    <td>
                                      Subtotal
                                    </td>
                                    <td>{{ $result->rooms_price->currency->symbol }}<span  id='subtotal_fee'>0</span></td>
                                  </tr>
                                  <tr class = 'tax_price'>
                                    <td>
                                      Tax
                                    </td>
                                    <td>{{ $result->rooms_price->currency->symbol }}<span  id='tax_fee'>0</span></td>
                                  </tr>
                                </tbody>
                              </table>
                              </div>
           </div>
            </li>
            </ul>
            </div>


</div>

      <div id="book_it_disabled" class="text-center" style="display:none;">
            <p id="book_it_disabled_message" class="icon-rausch">
              {{ trans('messages.rooms.dates_not_available') }}
            </p>
            <a href="{{URL::to('/')}}/s?location={{$result->rooms_address->city }}" class="btn btn-large btn-block" id="view_other_listings_button">
              {{ trans('messages.rooms.view_other_listings') }}
            </a>
          </div>
            <div class="js-book-it-btn-container {{ ($result->user_id == @Auth::user()->user()->id) ? 'hide' : '' }}">
              <button type="submit" class="js-book-it-btn btn btn-large btn-block btn-primary">
                <span class="book-it__btn-text {{ ($result->booking_type != 'instant_book') ? '' : 'hide' }}">
                  {{ trans('messages.rooms.request_to_book') }}
                </span>
                <span class="{{ ($result->booking_type == 'instant_book')}} {{ ($result->booking_type == 'instant_book') ? '' : 'book-it__btn-text--instant' }}">
                  <i class="icon icon-bolt text-beach h4 book-it__instant-book-icon"></i>
                  {{ trans('messages.lys.instant_book') }}
                </span>
              </button>
              <!-- <?php
              if(!empty($result->arrival_instruction)){
                $path = url().'/uploads/arrival_instruction/'.$room_id.'/'.$result->arrival_instruction;
              ?>
              <a id="arrival_instructions" target="_blank" class="btn btn-primary btn-block" href="{{$path}}">Arrival Instructions</a>
              <?php  }

              if(!empty($result->rental_contract)){
                $rpath = url().'/uploads/rental_contract/'.$room_id.'/'.$result->rental_contract;
              ?>
              <a id="rental_contract" target="_blank" class="btn btn-primary btn-block" href="{{$rpath}}">Short Term Rental Agreement</a>

              <?php } ?> -->
              <a id="things_to_do" class="btn btn-primary btn-block" href="{{url('top_10_list_of_things_to_do_in_cocoa_beach')}}" >Things to do</a>
          <input type="hidden" name="instant_book" value="{{ $result->booking_type }}">
          <p id="min_stay_msg"></p>
            </div>


            @if(Auth::user()->check())
              <!--<div id="contact_wrapper" style="margin-top: 10px;">-->
              <!--    <button id="host-profile-contact-btn" type="button" class="js-book-it-btn btn btn-large btn-block btn-primary">-->
              <!--        {{ trans('messages.rooms.contact_host') }}-->
              <!--    </button>-->
              <!--</div>-->
              @endif
           <!-- <p class="text-muted book-it__btn-text--instant-alt space-1 space-top-3 text-center {{ ($result->user_id == @Auth::user()->user()->id) ? 'hide' : '' }}">
              <small>
                {{ trans('messages.rooms.review_before_paying') }}
              </small>
            </p>-->



          </div>
        </div>
      </div>

    </div>



    <div class="panel wishlist-panel">
      <div class="panel-body panel-light">
      @if(Auth::user()->check())
<!--        <div class="wishlist-wrapper ">-->
<!--          <div class="rich-toggle wish_list_button not_saved" data-hosting_id="{{ $result->id }}">-->
<!--  <input type="checkbox" name="wishlist-button" id="wishlist-button" @if(@$is_wishlist > 0 ) checked @endif >-->

<!--</div>-->
<!--        </div>-->
        @endif
        <div class="other-actions  text-center">
          <div class="social-share-widget p3-share-widget">
  <!-- <span class="share-title">
    {{ trans('messages.rooms.share') }}:
  </span> -->
  <!-- <span class="share-triggers">

      <a class="share-btn link-icon" data-email-share-link="" data-network="email" rel="nofollow" title="{{ trans('messages.login.email') }}" href="mailto:?subject=I love this room&amp;body=Check out this {{ Request::url() }}">
        <span class="screen-reader-only">{{ trans('messages.login.email') }}</span>
        <i class="icon icon-envelope social-icon-size"></i>
      </a>
      <a class="share-btn link-icon" data-network="facebook" rel="nofollow" title="Facebook" href="http://www.facebook.com/sharer.php?u={{ Request::url() }}" target="_blank">
        <span class="screen-reader-only">Facebook</span>
        <i class="icon icon-facebook social-icon-size"></i>
      </a>

      <a class="share-btn link-icon" data-network="twitter" rel="nofollow" title="Twitter" href="http://twitter.com/home?status=Love this! {{ $result->name }} - {{ $result->property_type_name }} for Rent - {{ "@".$site_name}} Travel  {{ Request::url() }}" target="_blank">
        <span class="screen-reader-only">Twitter</span>
        <i class="icon icon-twitter social-icon-size"></i>
      </a>

    <a class="share-btn link-icon" data-network="pinterest" rel="nofollow" title="Pinterest" href="http://pinterest.com/pin/create/button/?url={{ Request::url() }}&media={{ url('images/'.$result->photo_name) }}&description={{ $result->summary }}" target="_blank">
        <span class="screen-reader-only">Pinterest</span>
        <i class="icon icon-pinterest social-icon-size"></i>
      </a>


      <a class="share-btn link-icon" href="https://plus.google.com/share?url={{ Request::url() }}"  itemprop="nofollow" rel="publisher" target="_blank">
            <span class="screen-reader-only">Google+</span>
            <i class="icon social-icon-size icon-google-plus"></i>
      </a>

  </span> -->
  <!-- <div class='card card-profile text-center'>

  <div class='card-block'>
    <img alt='' class='card-img-profile' src="{{ $user_details->profile_pic != '' ? $user_details->profile_pic : url().'/images/users/default.png' }}">
    <p>
        <span class="contact_mn">Contact {{ $user_details->first_name }}</span>
    </p>
   <br>


   {{--<small>
   Member: Since {{ date('Y', strtotime($user_details->created_at)) }}
   </small>--}}
  </div>
</div> -->
<div class="listing_owner">
    <div class="image_object">
      <img src="{{ $user_details->profile_pic != '' ? $user_details->profile_pic : url().'/images/users/default.png' }}" />
    </div>
    <div class="name_body">
    <button class="btn btn-primary" type="button" id="conatct_btnModal" data-toggle="modal" data-target="#myModal">Contact {{ $user_details->first_name }}</button>
   </div>
</div>
<!-- <i>
For booking assistance, call <a href="callto:888-829-7076"> {{$user_details->mobile}} </a></i> -->

<!-- <p>
   <a href="#" class="call">
    <i class="mdi mdi-email"></i> {{ $user_details->email }}
   </a> </p> -->
<!-- <div class="media">

  <div class="media-left">
    <a href="#">
      <img class="media-object" src="{{ $user_details->profile_pic != '' ? $user_details->profile_pic : url().'/images/users/default.png' }}" alt="...">
    </a>
  </div>
  <div class="media-body">
    <h4 class="media-heading">{{ $user_details->first_name }}</h4>
    ...
  </div>
</div> -->

</div>


          </div>
        </div>
      </div>
    </div>

  </div>

  <input id="hosting_id" name="hosting_id" type="hidden" value="{{ $result->id }}">
  <input id="room_types" name="room_types" type="hidden" value="{{ $room_types }}">
</form>

      </div>
      </div>
    </div>
  </div>

  <div id="details" class="details-section webkit-render-fix">
    <div id="summary-extend" class="container">
      <div class="row">
        <div class="col-lg-8 lang-chang-label col-sm-12" id="details-column">

<div class="row-space-8 r_l_t_m">

      <h4 class="row-space-4 text-center-sm">
      {{ trans('messages.rooms.about_this_listing') }}
    </h4>


    <!-- <p>{!! nl2br($result->summary) !!}</p> -->

@if(Auth::user()->check())
@if(Auth::user()->user()->id != $result->user_id)
  <p class="text-center-sm">
    <a id="contact-host-link" href="javascript:void(0);">
      <strong>{{ trans('messages.rooms.contact_host') }}</strong>
    </a>
  </p>
@endif
@endif

    <div class="space-4 space-top-4 show-sm hide_on_mobile">
    @foreach($rooms_photos as $row_photos)
    <div class="inline-photo panel-image">
      <a href="{{ url('images/rooms/'.$room_id.'/'.$row_photos['name']) }}" class="photo-trigger" data-index="1">
        <img src="{{ url('images/rooms/'.$room_id.'/'.$row_photos['name']) }}" alt="{{ $row_photos['highlights'] }}" class="media-photo media-photo-block space-1 space-top-1 img-responsive">
        <div class="panel-overlay-top-right panel-overlay-label panel-overlay-button-icon">
          <i class="icon icon-full-screen icon-white icon-size-2"></i>
        </div>
</a>    </div>
    <div class="row">
      <div class="col-lg-9">
          <p class="text-muted pull-left">{{ $row_photos['highlights'] }}</p>
      </div>
      <div class="col-lg-3">
      </div>
    </div>
    @endforeach
  </div>


  <hr>





  @if($result->rooms_description->space !='' || $result->rooms_description->access !='' || $result->rooms_description->interaction !='' || $result->rooms_description->neighborhood_overview !='' || $result->rooms_description->transit || $result->rooms_description->notes)
  <div class="row description">

    <div class="col-md-3 text-muted lang-chang-label">
      <b>{{ trans('messages.lys.description') }}</b>
    </div>

    <div class="col-md-9  all_description short_desc_height">


      <div class="expandable-content expandable-content-long">

            @if($result->rooms_description->space)
            <p><strong>{{ trans('messages.lys.the_space') }}</strong></p>
            <p >{!! nl2br($result->rooms_description->space) !!}</p>
            @endif
            @if($result->rooms_description->access)
            <p><strong>{{ trans('messages.lys.guest_access') }}</strong></p>
            <p >{!! nl2br($result->rooms_description->access) !!} </p>
            @endif
            @if($result->rooms_description->interaction)
            <p><strong>{{ trans('messages.lys.interaction_with_guests') }}</strong></p>
            <p > {!! nl2br($result->rooms_description->interaction) !!}</p>
            @endif
            @if($result->rooms_description->neighborhood_overview)
            <p><strong>{{ trans('messages.lys.the_neighborhood') }}</strong></p>
            <p > {!! nl2br($result->rooms_description->neighborhood_overview) !!}</p>
            @endif
            @if($result->rooms_description->transit)
            <p><strong>{{ trans('messages.lys.getting_around') }}</strong></p>
            <p >{!! nl2br($result->rooms_description->transit) !!}</p>
            @endif
            @if($result->rooms_description->notes)
            <p><strong>{{ trans('messages.lys.other_things_note') }}</strong></p>
            <p >{!! nl2br($result->rooms_description->notes) !!}</p>
            @endif



      </div>




    </div>
  </div>

  <hr>
@endif


  <div class="row amenities">
  <div class="col-md-3 text-muted lang-chang-label col-sm-12">
    <b>{{ trans('messages.lys.amenities') }}</b>
  </div>



    <div class="col-md-9 expandable expandable-trigger-more short_desc_height">
      <div class="expandable-content-summary">
        <div class="row rooms_amenities_before" >


            <div class="col-sm-6 lang-chang-label">

               {{--*/ $i = 1 /*--}}

               {{--*/ $count = round(count($amenities)/2) /*--}}

                @foreach($amenities as $all_amenities)


               @if($i < 6)

                @if($all_amenities->status != null)
                <div class="row-space-1">
                @else
                <div class="row-space-1 text-muted">
                @endif

                <i class="icon h3 icon-{{ $all_amenities->icon }}"></i>
                    &nbsp;
                  <span class="js-present-safety-feature"><strong>
                        @if($all_amenities->status == null)
                        <del>
                        @endif
                        {{ $all_amenities->name }}
                        @if($all_amenities->status == null)
                        </del>
                        @endif
                      </strong></span>

                </div>


                </div>
                <div class="col-sm-6">
                @endif
                {{--*/ $i++ /*--}}
                @endforeach
                <a class="expandable-trigger-more amenities_trigger" href="">
      <strong>+ {{ trans('messages.profile.more') }}</strong>
    </a>

        </div>

            </div>

                  <div class="row rooms_amenities_after" style="display:none;">


            <div class="col-sm-6 lang-chang-label">

               {{--*/ $i = 1 /*--}}

               {{--*/ $count = round(count($amenities)/2) /*--}}

                @foreach($amenities as $all_amenities)



                @if($all_amenities->status != null)
                <div class="row-space-1 new_id<?php echo $all_amenities->type_id;?>">
                <p hidden="hidden" class="get_type" data-id="<?php echo $all_amenities->type_id;?>"><?php echo $all_amenities->type_id;?></p>
                @else
                <div class="row-space-1 text-muted new_id<?php echo $all_amenities->type_id;?>">
                <p hidden="hidden" class="get_type" data-id="<?php echo $all_amenities->type_id;?>"><?php echo $all_amenities->type_id;?></p>
                @endif
                <i class="icon h3 icon-{{ $all_amenities->icon }}"></i>
                    &nbsp;
                  <span class="js-present-safety-feature"><strong>
                         @if($all_amenities->status == null)
                        <del>
                        @endif
                        {{ $all_amenities->name }}
                        @if($all_amenities->status == null)
                        </del>
                        @endif
                      </strong></span>

                </div>


                </div>
                <div class="col-sm-6">

                {{--*/ $i++ /*--}}
                @endforeach

        </div>

            </div>

        </div>
      </div>

    </div>

<hr>
@if($result->rooms_description->house_rules !='')
    <div class="row" style="margin-top:15px">
      <div class="col-md-3 lang-chang-label col-sm-12">
            <div class="text-muted">
      <b>{{ trans('messages.lys.house_rules') }}</b>
    </div>

      </div>
      <div class="col-md-9 col-sm-12">
        <p>{!! nl2br(htmlspecialchars_decode($result->rooms_description->house_rules)) !!}</p>

      </div>
    </div>

    <hr>
@endif
<div class="row" style="margin-top:15px">
      <div class="col-md-3 lang-chang-label col-sm-12">
            <div class="text-muted">
      <b>Price</b>
    </div>

      </div>
      <div class="col-md-9 col-sm-12">
       <div class="rate_image">
       <!-- <img src="{{url()}}/images/611.jpg" /> -->
       <img src="{{url()}}/uploads/rateSheet/{{$room_id}}/{{$result->rooms_price->rate_sheet}}" />
       </div>

      </div>
    </div>

    <hr>

  <div class="js-p3-safety-features-section" style="display:none">
  @if(count($safety_amenities) !=0)
    <div class="row">
      <div class="col-md-3 lang-chang-label col-sm-12">
            <div class="text-muted">
      {{ trans('messages.rooms.safety_features') }}
    </div>

      </div>
      <div class="col-md-9 col-sm-12">
        <div class="js-no-safety-features-text hide">
          {{ trans('messages.account.none') }}
        </div>
        <div class="row">
            <div class="col-sm-6 lang-chang-label">

               {{--*/ $i = 1 /*--}}

               {{--*/ $count = round(count($safety_amenities)/2) /*--}}

                @foreach($safety_amenities as $row_safety)

                @if($row_safety->status != null)
                 <div class="row-space-1">
                @else
                <div class="row-space-1 text-muted">
                @endif
                <i class="icon h3 icon-{{ $all_amenities->icon }}"></i>
                    &nbsp;
                  <span class="js-present-safety-feature cut-span"><strong>
                         @if($row_safety->status == null)
                        <del>
                        @endif
                        {{ $row_safety->name }}
                        @if($row_safety->status == null)
                        </del>
                        @endif
                      </strong></span>

                </div>


                </div>
                <div class="col-sm-6 lang-chang-label">
               in
                {{--*/ $i++ /*--}}
                @endforeach

        </div>
      </div>
    </div>

  </div>
 <hr>
 @endif

  <div class="row">
    <div class="col-md-3 lang-chang-label col-sm-12">
          <div class="text-muted">
      {{ trans('messages.rooms.availability') }}
    </div>

    </div>
    <div class="col-md-9 col-sm-12">
      <div class="row">
          <!-- <div class="col-md-6 lang-chang-label col-sm-6">
            <strong>1 {{ trans_choice('messages.rooms.night',1) }}</strong> {{ trans('messages.rooms.minimum_stay') }}
          </div> -->
        <div class="col-md-6 lang-chang-label col-sm-6">
          <a id="view-calendar" href="javascript:void(0);"><strong>{{ trans('messages.rooms.view_calendar') }}</strong></a>
        </div>
      </div>
    </div>
  </div>
</div>



  <div id="photo-gallery" class="photo-grid row-space-4 row-space-top-4 hide-sm ">

        {{--*/ $i = 1 /*--}}

               <!-- {{--*/ $count = round(count($amenities)/2) /*--}} -->
               <!-- {{ count($rooms_photos)}} -->

                @foreach($rooms_photos as $row_photos)

                 @if(count($rooms_photos) == 1)
                              <div class="row featured-height">
                 <div class="col-12 row-full-height img-box1">
          <a class="photo-grid-photo photo-trigger gallery" style="background-image: url({{ url('images/rooms/'.$room_id.'/'.$row_photos['name']) }})" href="{{ url('rooms/'.$result->id.'/slider') }}" data-index="1" data-lightbox-type="iframe">
  <img src="{{ 'images/rooms/'.$room_id.'/'.$row_photos['name'] }}"  alt="">
</a>
        </div></div>
               @else


                @if($i == 1)
                <div class="row featured-height">
                 <div class="col-12 row-full-height img-box1">
          <a class="photo-grid-photo photo-trigger gallery" style="background-image: url({{ url('images/rooms/'.$room_id.'/'.$row_photos['name']) }})" href="{{ url('rooms/'.$result->id.'/slider') }}" data-index="1" data-lightbox-type="iframe">
  <img src="{{ 'images/rooms/'.$room_id.'/'.$row_photos['name'] }}"  alt="">
</a>
        </div></div>
                @endif
                @if($i==2 && $i >1)
        <div class="col-6 supporting-height img-box1">
          <a class="photo-grid-photo photo-trigger gallery" style="background-image: url({{ url('images/rooms/'.$room_id.'/'.$row_photos['name']) }})" href="{{ url('rooms/'.$result->id.'/slider') }}" data-index="2" data-lightbox-type="iframe">
  <img src="{{ 'images/rooms/'.$room_id.'/'.$row_photos['name'] }}"  alt="">
</a>
        </div>
        @endif

        @if($i==3 && $i >2)

        <div class="col-6 supporting-height img-box1">
              <div class="media-photo media-photo-block row-full-height">
                <div class="media-cover media-cover-dark img-box1">
                  <a class="photo-grid-photo photo-trigger gallery"
   style="background-image: url({{ url('images/rooms/'.$room_id.'/'.$row_photos['name']) }})"
   href="{{ url('rooms/'.$result->id.'/slider') }}"
   data-index="5" data-lightbox-type="iframe">
  <img src="{{ url('images/rooms/'.$room_id.'/'.$row_photos['name']) }}"
       
       alt="Private shower/Longterm/Decent B&amp;B">
</a>
                </div>
                <a class="photo-trigger gallery"
                   href="{{ url('rooms/'.$result->id.'/slider') }}"
                   data-index="5" data-lightbox-type="iframe">
                  <div class="row row-table supporting-height">
                    <div class="col-6 col-middle text-center text-contrast">
                      <div class="h5">
                        {{ trans('messages.rooms.see_all') }} {{ round(count($rooms_photos))}} {{ trans_choice('messages.header.photo',2) }}
                      </div>
                    </div>
                  </div>
                </a></div></div>
                @endif
                @endif
                {{--*/ $i++ /*--}}
                @endforeach
  </div>
  @if($result->video)
  <iframe width="100%" height="300" src="{{ $result->video }}" allowfullscreen="allowfullscreen" mozallowfullscreen="mozallowfullscreen" msallowfullscreen="msallowfullscreen" oallowfullscreen="oallowfullscreen" webkitallowfullscreen="webkitallowfullscreen"></iframe>
  @endif
</div>
        </div>
      </div>
    </div>
  </div>

  <div id="reviews" class="webkit-render-fix">
    <div class="panel" style="border:none">
      <div class="container">
        <div class="row">

          <div class="col-lg-8 lang-chang-label col-sm-12">
            @if(!count($reviews))
            <div class="review-content">
                <div class="panel-body">
                    <h4 class="row-space-4 text-center-sm ">
                  {{ trans('messages.rooms.no_reviews_yet') }}
                </h4>

                  <p>
                  {{ trans_choice('messages.rooms.review_other_properties', $reviews->count(), ['count'=>$reviews->count()]) }}
                  </p>
                  <a href="{{ url('users/show/'.$result->user_id) }}" class="btn">{{ trans('messages.rooms.view_other_reviews') }}</a>

              </div>
            </div>
            @else
            <div class="review-wrapper">
            <div>
            <div class="row space-2 space-top-8 row-table">
            <div class="review-header col-md-8 lang-chang-label">
            <div class="va-container va-container-v va-container-h">
            <div class="va-bottom review-header-text">
            <h4 class="text-center-sm col-middle">
            <span>{{ $reviews->count() }} {{ trans_choice('messages.header.review',$reviews->count()) }}</span>
            <div style="display:inline-block;">
            <!--<div class="star-rating-wrapper">-->
            <!--{!! $result->overall_star_rating !!}-->
            <!--</div>-->

            </div>
            </h4>
            </div>
            </div>
            </div>
            </div>
            <div>
            <hr>
            </div>
            </div>
            <div class="review-main">
            <div class="review-inner space-top-2 space-2">
            <div class="row">
            <div class="col-lg-3 show-lg lang-chang-label">
            <!--<div class="text-muted">-->
            <!--<span>{{ trans('messages.lys.summary') }}</span>-->
            <!--</div>-->
            </div>
            <div class="col-lg-9">
            <div class="row">
            <div class="col-lg-6 lang-chang-label">
            <div>
            <div class="pull-right">
            <!--<div class="star-rating-wrapper">-->
            <!--{!! $result->accuracy_star_rating !!}-->
            <!--<span> </span>-->
            <!--</div>-->
            </div>
            <!--<strong>{{ trans('messages.reviews.accuracy') }}</strong>-->
            </div>
            <div>
            <div class="pull-right">
            <!--<div class="star-rating-wrapper">-->
            <!--{!! $result->communication_star_rating !!}-->
            <!--<span> </span>-->
            <!--</div>-->
            </div>
            <!--<strong>{{ trans('messages.reviews.communication') }}</strong>-->
            </div>
            <div>
            <div class="pull-right">
            <!--<div class="star-rating-wrapper">-->
            <!--{!! $result->cleanliness_star_rating !!}-->
            <!--<span> </span>-->
            <!--</div>-->
            </div>
            <!--<strong>{{ trans('messages.reviews.cleanliness') }}</strong>-->
            </div>
            </div>
            <div class="col-lg-6 lang-chang-label">
            <!--<div>-->
            <!--<div class="pull-right">-->
            <!--<div class="star-rating-wrapper">-->
            <!--{!! $result->location_star_rating !!}-->
            <!--<span> </span>-->
            <!--</div>-->
            <!--</div>-->
            <!--<strong>{{ trans('messages.reviews.location') }}</strong>-->
            <!--</div>-->
            <div>
            <div class="pull-right">
            <!--<div class="star-rating-wrapper">-->
            <!--{!! $result->checkin_star_rating !!}-->
            <!--<span> </span>-->
            </div>
            </div>
            <!--<strong>{{ trans('messages.home.checkin') }}</strong>-->
            </div>
            <div>
            <div class="pull-right">
            <!--<div class="star-rating-wrapper">-->
            <!--{!! $result->value_star_rating !!}-->
            <!--<span> </span>-->
            <!--</div>-->
            </div>
            <!--<strong>{{ trans('messages.reviews.value') }}</strong>-->
            </div>
            </div>
            </div>
            </div>
            </div>
            </div>
            <div class="review-content">
            <div class="panel-body">
                @if($reviews)
                <div class="row">
            @foreach($reviews as $key => $revi)
            <div class="col-md-2">

            <i class="mdi mdi-account-circle user_rw"></i>
            </div>
            <div class="col-md-10">
            <div class="review--box">
              <h4>{{ $revi->review_title }}</h4>
              <h6>{{ $revi->username }}</h6>
            <p>{{ $revi->review }}</p>
            </div>
            </div>
            @endforeach
            </div>
            @endif
            @foreach($result->reviews as $row_review)
            <div>
            <div class="row review">
            <div class="col-md-3 col-sm-12 text-center space-2 lang-chang-label">
            <div class="media-photo-badge">
            <a class="media-photo media-round" href="{{ url('users/show/'.$row_review->user_from) }}">
            <img width="67" height="67" title="{{ $row_review->users_from->first_name }}" src="{{ $row_review->users_from->profile_picture->src }}" data-pin-nopin="true" alt="shared.user_profile_image">
            </a>
            </div>
            <div class="name">
            <a target="_blank" class="text-muted link-reset" href="{{ url('users/show/'.$row_review->user_from) }}">{{ $row_review->users_from->first_name }}</a>
            </div>
            </div>
            <div class="col-md-9 col-sm-12">
            <div class="space-2">
            <div class="review-text" data-review-id="{{ $row_review->id }}">
            <div class="react-expandable expanded text-center-sm">
            <div class="expandable-content" tabindex="-1" style="">
            <p>{{ $row_review->comments }}</p>
            </div>
            </div>
            </div>
            <div class="text-muted review-subtext">
            <div class="review-translation-language">
            </div>
            <div class="">
            <div class="text-center-sm">
            <span class="date" style="display:inline-block;">{{ $row_review->date_fy }}</span>
            </div>
            </div>
            </div>
            </div>
            <span>
            </span>
            </div>
            <div class="row space-2">
            <div class="col-md-9 col-md-push-3">
            <hr>
            </div>
            </div>
            </div>
            </div>
            @endforeach
            @if($result->users->reviews->count() - $result->reviews->count())
            <div class="row row-space-top-2">
            <div class="col-lg-9 col-offset-3">
            <p>
            <span>{{ trans_choice('messages.rooms.review_other_properties', $result->users->reviews->count() - $result->reviews->count(), ['count'=>$result->users->reviews->count() - $result->reviews->count()]) }}</span>
            </p>
            <a target="blank" class="btn" href="{{ url('users/show/'.$result->user_id) }}">
            <span>{{ trans('messages.rooms.view_other_reviews') }}</span>
            </a>
            </div>
            </div>
            @endif
            </div>
            </div>
            </div>
            </div>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>

  <div id="host-profile" class="room-section webkit-render-fix">

  </div>

<div id="neighborhood" class="">
<div class="page-container-responsive" id="map-id" data-reactid=".2" style="position:relative;">
  <div class="panel location-panel">
    <div id="map" data-lat="{{ $result->rooms_address->latitude }}" data-lng="{{ $result->rooms_address->longitude }}"> </div>
<ul id="guidebook-recommendations" class="hide">
  <li class="user-image">
    <a href=""><img alt="Jeya" data-pin-nopin="true" height="90" src="" title="Jeya" width="90"></a>
  </li>
</ul>

    <div id="hover-card" class="panel">
  <div class="panel-body">
    <div class="text-center">
      {{ trans('messages.rooms.listing_location') }}
    </div>
    <div class="text-center">
        <span>
          <a href="" class="text-muted"><span>{{$result->rooms_address->state}},</span></a>
        </span>
        <span>
          <a href="" class="text-muted"><span>{{$result->rooms_address->country_name}}</span></a>
        </span>
    </div>
  </div>
</div>

  </div>
  </div>
</div>


      {{--<div id="similar-listings" class="row-space-top-4">

@if(count($similar)> 3)
<div  id="slider-next" class="" data-reactid=".2.0.1.2">
<i class="" data-reactid=".2.0.1.2.0"></i>
</div>
<div  id="slider-prev" class="" data-reactid=".2.0.1.2">
<i class="" data-reactid=".2.0.1.2.0"></i>
</div>
@endif
@if(count($similar)!= 0)

  <div class="page-container-responsive">
        <h4 class="row-space-4 text-center-sm">
      {{ trans('messages.rooms.similar_listings') }}
    </h4>
<div class="slider1">
@foreach($similar as $row_similar)
<div class="col-md-4 col-sm-12">
<div class="listing"> <div class="panel-image listing-img">
    <a href="{{ url('rooms/'.$row_similar->id) }}" target="_self" class="media-photo media-cover" target="_blank">
      <div class="listing-img-container media-cover text-center slide">
        {!! Html::image('images/'.$row_similar->photo_name, $row_similar->name, '') !!}
      </div>
    </a>
    <a href="{{ url('rooms/'.$row_similar->id) }}" target="_self" class="link-reset panel-overlay-bottom-left panel-overlay-label panel-overlay-listing-label" target="_blank">
      <div>
        <sup class="h6 text-contrast">{{ $row_similar->rooms_price->currency->symbol }}</sup>
        <span class="h3 text-contrast price-amount">{{ $row_similar->rooms_price->night }}</span>
        <sup class="h6 text-contrast"></sup>
    @if($row_similar->booking_type == 'instant_book')
    <span aria-label="Book Instantly"  class="h3 icon-beach" style="position:relative;">
      <i class="icon icon-instant-book icon-flush-sides tool-amenity1"  ></i>
      <div class="tooltip-amenity tooltip-left-middle tooltip-amenity1"  role="tooltip" data-sticky="true" aria-hidden="true" style="left: 30px; top: -10px; display: none;">
          <dl class="panel-body" style="padding:10px;">
            <dt>Instant Book</dt>
            <dt>Book without waiting for the host to respond</dt>
          </dl>
        </div>
    </span>
    @endif
      </div>
    </a>
  </div>

  <div class="panel-body panel-card-section">
    <div class="media">
        <a href="{{ url('users/show/'.$row_similar->user_id) }}" class="media-photo-badge pull-right card-profile-picture card-profile-picture-offset">
          <div class="media-photo media-round">
            <img src="{{ $row_similar->users->profile_picture->src }}" alt="">
          </div>
        </a>
      <a href="{{ url('rooms/'.$row_similar->id) }}" target="_self" class="text-normal">
        <h3 title="Anne&#39;s Room for Two " itemprop="name" class="h5 listing-name text-truncate row-space-top-1">
          {{ $row_similar->name }}
        </h3>
      </a>
      <div itemprop="description" class="text-muted listing-location text-truncate">{{ $row_similar->room_type_name }}  — {{ number_format($row_similar->distance,2) }} {{ trans('messages.rooms.km_away') }}
</div>
    </div>
  </div>
</div>
          </div>
@endforeach
</div>
</div>
  @endif
</div>--}}
</div>
</div>
</div>
</div>



<div><div>
<span>
<div class="modal-container modal-transitions contact-modal hide">
<div class="modal-table popup-scroll">
<div class="modal-cell">
<div class="modal-content host-sec">
<a data-behavior="modal-close" class="modal-close" href="#" style="font-size:3em;"></a>
<div id="contact-host-panel" class="">
<div id="compose-message" class="contact-host-panel panel-dark">
<div class="row">
<div class="host-questions-panel panel panel-dark col-md-4 col-sm-12 lang-chang-label">
<div class="panel-body">
<div class="text-center">
<div class="media-photo media-round">
<div class="media-photo-badge">
<a href="{{ url() }}/users/show/{{ $result->user_id }}" class="media-photo media-round">
<img alt="shared.user_profile_image" data-pin-nopin="true" src="{{ $result->users->profile_picture->src }}" title="{{ $result->users->first_name }}" width="120" height="120">
</a>
</div>
</div>
</div>
<div>
<h5>
<span>{{ trans('messages.rooms.send_a_message',['first_name'=>$result->users->first_name]) }}</span>
</h5>
<p>
<span>{{ trans('messages.rooms.share_following') }}:</span>
</p>
<ul>
<li>
<span>{{ trans('messages.rooms.tell_about_yourself',['first_name'=>$result->users->first_name]) }}</span>
</li>
<li>
<span>{{ trans('messages.rooms.what_brings_you',['city'=>$result->rooms_address->city]) }}?</span>
</li>
<li>
<span>{{ trans('messages.rooms.love_about_listing') }}!</span>
</li>
</ul>
</div>
</div>
</div>
<div class="guest-message-panel panel col-md-8 col-sm-12">
<div class="alert alert-with-icon alert-info error-block row-space-4 alert-header panel-header contacted-before hide">
<i class="icon alert-icon icon-comment">
</i>
<div class="not-available">
<span>{{ trans('messages.rooms.dates_arenot_available') }}</span>
</div>
<div class="other">
<strong>
</strong>
</div>
</div>
<div class="panel-body">
<form id="message_form" class="contact-host-panel" action="{{ url() }}/users/ask_question/{{ $result->id }}" method="POST">
{!! Form::token() !!}
    <h5>
<span>{{ trans('messages.rooms.when_you_traveling') }}?</span>
</h5>
<div class="row-space-4 clearfix">
<div>
<div class="col-6 input-col lang-chang-label">
<label class="screen-reader-only">{{ trans('messages.home.checkin') }}</label>
<input value="{{ (Auth::user()->check()) ? Auth::user()->user()->first_name : '' }}"  name="message_person_first_name" id="message_person_first_name" class="checkin text-center ui-datepicker-target" placeholder="First Name" type="text" required />
</div>
<span hidden="hidden" id="room_id">{{ $result->id }}</span>
<div class="col-6 input-col lang-chang-label">
<label class="screen-reader-only">{{ trans('messages.home.checkout') }}</label>
<input value="{{ (Auth::user()->check()) ? Auth::user()->user()->last_name : '' }}"  name="message_person_last_name" id="message_person_last_name" class="checkout text-center ui-datepicker-target" placeholder="Last Name" type="text" required />
</div>
</div>

</div>

<div class="row-space-4 clearfix">
<div>
<div class="col-6 input-col lang-chang-label">
<label class="screen-reader-only">{{ trans('messages.home.checkin') }}</label>
<input value="{{ (Auth::user()->check()) ? Auth::user()->user()->email : '' }}"  name="message_person_email" id="message_person_email" class="checkin text-center" placeholder="Email" type="text" required />
</div>
<div class="col-6 input-col lang-chang-label">
<label class="screen-reader-only">{{ trans('messages.home.checkin') }}</label>
<input value=""  name="message_person_phone" id="message_person_phone" class="checkin text-center" placeholder="Phone Number" type="text" required />
</div>
</div>

</div>



<div class="row-space-4 clearfix">
<div>
<div class="col-4 input-col lang-chang-label">
<label class="screen-reader-only">{{ trans('messages.home.checkin') }}</label>
<input value="" readonly="readonly" name="message_checkin" id="message_checkin" class="checkin text-center ui-datepicker-target" placeholder="{{ trans('messages.home.checkin') }}" type="text" required />
</div>
<span hidden="hidden" id="room_id">{{ $result->id }}</span>
    @if(Auth::user()->check())
        @if(Auth::user()->user()->id)
            <input value="{{ Auth::user()->user()->id }}"  name="userId" id="userId" class="checkin text-center ui-datepicker-target" placeholder="{{ trans('messages.home.checkin') }}" type="hidden" />
        @endif
    @else
        <input value="0"  name="userId" id="userId" class="checkin text-center ui-datepicker-target" placeholder="{{ trans('messages.home.checkin') }}" type="hidden" />
    @endif
  <div class="col-4 input-col lang-chang-label">
  <label class="screen-reader-only">{{ trans('messages.home.checkout') }}</label>
  <input value="" readonly="readonly" name="message_checkout" id="message_checkout" class="checkout text-center ui-datepicker-target" placeholder="{{ trans('messages.home.checkout') }}" type="text" required />
  </div>
  </div>
  <div class="col-4 input-col lang-chang-label">
  <div class="select select-block">
  <select class="text-center" name="message_guests" id="message_guests">
  @for($i=1;$i<= $result->accommodates;$i++)
          <option value="{{ $i }}">{{ $i }} {{ trans_choice('messages.home.guest',$i) }}</option>
      @endfor
  </select>
  </div>
  </div>
  <p style="color: red" class="hide" id="errors">Please Fill the details</p>
  </div>
  <div class="row">
  <div class="col-12">
  <div class="message-panel  tooltip-fixed tooltip-bottom-left row-space-4" style="background-color: #fff; border-radius: 2px; box-shadow: 0 0 0 1px rgba(0, 0, 0, 0.1);">
  <div class="panel-body">
  <textarea class="focus-on-active" name="question" placeholder="{{ trans('messages.rooms.start_your_msg') }}..."></textarea>
  </div>
  </div>
  </div>
  </div>
  <noscript>
  </noscript>
  <input name="message_save" value="1" type="hidden">
  </form>
  <div class="row">
  <div class="col-4 lang-chang-label">
  <div class="media-photo media-round">
  <div class="media-photo-badge">
  @if(Auth::user()->check())
      <a href="{{ url() }}/users/show/{{ (Auth::user()->check()) ? Auth::user()->user()->id : '' }}" class="media-photo media-round">
      <img alt="shared.user_profile_image" data-pin-nopin="true" src="{{ (Auth::user()->check()) ? Auth::user()->user()->profile_picture->src : '' }}" title="{{ (Auth::user()->check()) ? Auth::user()->user()->first_name : '' }}" width="68" height="68">
      </a>
  @endif
  </div>
  </div>
  </div>
  <div class="col-7 col-offset-1">
  <button id="contace_request_message_send" type="submit" class="btn btn-block btn-large btn-primary row-space-top-2">
  <span>{{ trans('messages.your_reservations.send_message') }}</span>
  </button>
  </div>
  </div>

  </div>
  </div>
  </div>
  </div>
  <div class="contact-host-panel hide">
  <div class="panel">
  <div class="panel-header panel-header-message-sent text-center">
  <strong>{{ trans('messages.rooms.message_sent') }}!</strong>
  </div>
  <div class="panel-body text-center">
  <div class="row">
  <p class="col-10 col-center row-space-top-4 text-lead">
  <span>{{ trans('messages.rooms.keep_contacting_other') }}</span>
  </p>
  </div>
  <div class="row">
  <div class="col-6 col-center row-space-top-4 row-space-2">
  <a href="#" class="btn btn-block btn-primary confirmation btn-large text-wrap">{{ trans('messages.rooms.ok') }}</a>
  </div>
  </div>
  </div>
  </div>
  </div>
   </div>
   </div>
   </div>
   </div>
   </div>
   </span>
   </div>
   </div>

  <div class="modal-container modal-transitions wl-modal__modal hide">
  <div class="modal-table">
  <div class="modal-cell">
  <div class="modal-content">
  <div class="wl-modal">
  <div class="row row-margin-zero">
  <div class="hide-sm col-lg-7 wl-modal__col">
  <div class="media-cover media-cover-dark background-cover background-listing-img" style="background-image:url({{ url('images/'.$result->photo_name) }});">
  </div>
  <div class="panel-overlay-top-left text-contrast wl-modal-listing-tabbed">
  <div class="va-container media">
  <img class="pull-left host-profile-img media-photo media-round space-2" height="67" width="67" src="{{ $result->users->profile_picture->src }}">
  <div class="media-body va-middle">
  <div class="h4 space-1 wl-modal-listing__name">{{ $result->name }}</div>
  <div class="wl-modal-listing__rating-container">
  <span class="hide">
  <div class="star-rating-wrapper">
  <div class="star-rating" content="0">
  <div class="foreground">
  <span> </span>
  </div>
  <div class="background">
  <span>
  <span>
  <i class="icon-star icon icon-light-gray icon-star-big">
  </i>
  <span> </span>
  </span>
  <span>
  <i class="icon-star icon icon-light-gray icon-star-big">
  </i>
  <span> </span>
  </span>
  <span>
  <i class="icon-star icon icon-light-gray icon-star-big">
  </i>
  <span> </span>
  </span>
  <span>
  <i class="icon-star icon icon-light-gray icon-star-big">
  </i>
  <span> </span>
  </span>
  <span>
  <i class="icon-star icon icon-light-gray icon-star-big">
  </i>
  <span> </span>
  </span>
  </span>
  </div>
  </div>
  <span> </span>
  <span class="h6 hide">
  <small>
  <span>(</span>
  <span>
  </span>
  <span>)</span>
  </small>
  </span>
  </div>
  <span> · </span>
  <span class="wl-modal-listing__text">
  </span>
  <span> · </span>
  </span>
  <span class="wl-modal-listing__address wl-modal-listing__text">{{ $result->rooms_address->city }}</span>
  </div>
  </div>
  </div>
  </div>
  </div>
  <div class="col-lg-5 wl-modal__col">
  <div class="panel-header panel-light wl-modal__header">
  <div class="va-container va-container-h va-container-v">
  <div class="va-middle">
  <div class="pull-left h3">{{ trans('messages.wishlist.save_to_wishlist') }}</div>
  <a class="modal-close wl-modal__modal-close">
  </a>
  </div>
  </div>
  </div>
  <div class="wl-modal-wishlists">
  <div class="panel-body panel-body-scroll wl-modal-wishlists__body wl-modal-wishlists__body--scroll">
  <div class="text-lead text-gray space-4 hide">{{ trans('messages.wishlist.save_fav_list') }}</div>
  <div class="wl-modal-wishlist-row clickable" ng-repeat="item in wishlist_list" ng-class="(item.saved_id) ? 'text-dark-gray' : 'text-gray'" ng-click="wishlist_row_select($index)" id="wishlist_row_@{{ $index }}">
  <div class="va-container va-container-v va-container-h">
  <div class="va-middle text-left text-lead wl-modal-wishlist-row__name">
  <span> </span>
  <span >@{{ item.name }}</span>
  <span> </span>
  </div>
  <div class="va-middle text-right">
  <div class="h3 wl-modal-wishlist-row__icons">
  <i class="icon icon-heart-alt icon-light-gray wl-modal-wishlist-row__icon-heart-alt" ng-hide="item.saved_id"></i>
  <i class="icon icon-heart icon-rausch wl-modal-wishlist-row__icon-heart" ng-show="item.saved_id"></i>
  </div>
  </div>
  </div>
  </div>
  </div>
  <div class="text-beach panel-body wl-modal-wishlists__body hide">
  <small>
  </small>
  </div>
  <div class="panel-footer wl-modal-footer clickable">
  <form class="wl-modal-footer__form hide">
  <strong>
  <div class="pull-left text-lead va-container va-container-v">
  <input type="text" class="wl-modal-footer__text wl-modal-footer__input" autocomplete="off" id="wish_list_text" value="{{ $result->rooms_address->city }}" placeholder="Name Your Wish List" required>
  </div>
  <div class="pull-right">
  <button id="wish_list_btn" class="btn btn-flat wl-modal-wishlists__footer__save-button btn-contrast">{{ trans('messages.wishlist.create') }}</button>
  </div>
  </strong>
  </form>
  <div class="text-rausch va-container va-container-v va-container-h">
  <div class="va-middle text-lead wl-modal-footer__text div_check">{{ trans('messages.wishlist.create_new_wishlist') }}</div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
  </div>
   <!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Contact the Owner</h4>
      </div>
      <div class="modal-body">
        <form>
        <div class="form-group">
        <input type="text" class="form-control" placeholder="First Name*"/>
        </div>
        <div class="form-group">
        <input type="text" class="form-control" placeholder="Last Name*"/>
        </div>
        <div class="form-group">
        <input type="text" class="form-control" placeholder="Email*"/>
        </div>
        <div class="form-group row">
          <div class="col-md-6">
          <select class="form-control" >
   <option value="1">US &amp; Canada (1)</option>
   <option value="93">Afghanistan (93)</option>
   <option value="355">Albania (355)</option>
   <option value="213">Algeria (213)</option>
   <option value="684">American Samoa (684)</option>
   <option value="376">Andorra (376)</option>
   <option value="244">Angola (244)</option>
   <option value="672">Norfolk Island (672)</option>
   <option value="1268">Antigua and Barbuda (268)</option>
   <option value="54">Argentina (54)</option>
   <option value="374">Armenia (374)</option>
   <option value="297">Aruba (297)</option>
   <option value="61">Australia (61)</option>
   <option value="43">Austria (43)</option>
   <option value="994">Azerbaijan (994)</option>
   <option value="1242">Bahamas (242)</option>
   <option value="973">Bahrain (973)</option>
   <option value="880">Bangladesh (880)</option>
   <option value="1246">Barbados (246)</option>
   <option value="375">Belarus (375)</option>
   <option value="32">Belgium (32)</option>
   <option value="501">Belize (501)</option>
   <option value="229">Benin (229)</option>
   <option value="1441">Bermuda (441)</option>
   <option value="975">Bhutan (975)</option>
   <option value="591">Bolivia (591)</option>
   <option value="387">Bosnia and Herzegovina (387)</option>
   <option value="267">Botswana (267)</option>
   <option value="55">Brazil (55)</option>
   <option value="673">Brunei Darussalam (673)</option>
   <option value="359">Bulgaria (359)</option>
   <option value="226">Burkina Faso (226)</option>
   <option value="257">Burundi (257)</option>
   <option value="855">Cambodia (855)</option>
   <option value="237">Cameroon (237)</option>
   <option value="238">Cape Verde (238)</option>
   <option value="345">Cayman Islands (345)</option>
   <option value="236">Central African Republic (236)</option>
   <option value="235">Chad (235)</option>
   <option value="56">Chile (56)</option>
   <option value="86">China (86)</option>
   <option value="57">Colombia (57)</option>
   <option value="269">Mayotte (269)</option>
   <option value="242">Congo (242)</option>
   <option value="243">Congo, Democratic Rep. (243)</option>
   <option value="682">Cook Islands (682)</option>
   <option value="506">Costa Rica (506)</option>
   <option value="225">Côte d Ivoire (225)</option>
   <option value="385">Croatia (385)</option>
   <option value="53">Cuba (53)</option>
   <option value="357">Cyprus (357)</option>
   <option value="420">Czech Republic (420)</option>
   <option value="45">Denmark (45)</option>
   <option value="253">Djibouti (253)</option>
   <option value="767">Dominica (767)</option>
   <option value="1809">Dominican Republic (809)</option>
   <option value="670">East Timor (670)</option>
   <option value="593">Ecuador (593)</option>
   <option value="20">Egypt (20)</option>
   <option value="503">El Salvador (503)</option>
   <option value="240">Equatorial Guinea (240)</option>
   <option value="291">Eritrea (291)</option>
   <option value="372">Estonia (372)</option>
   <option value="251">Ethiopia (251)</option>
   <option value="500">Falkland Islands (500)</option>
   <option value="298">Faroe Islands (298)</option>
   <option value="679">Fiji (679)</option>
   <option value="358">Finland (358)</option>
   <option value="33">France (33)</option>
   <option value="594">French Guiana (594)</option>
   <option value="689">French Polynesia (689)</option>
   <option value="241">Gabon (241)</option>
   <option value="220">Gambia (220)</option>
   <option value="995">Georgia (995)</option>
   <option value="49">Germany (49)</option>
   <option value="233">Ghana (233)</option>
   <option value="350">Gibraltar (350)</option>
   <option value="30">Greece (30)</option>
   <option value="299">Greenland (299)</option>
   <option value="473">Grenada (473)</option>
   <option value="590">Guadeloupe (590)</option>
   <option value="671">Guam (671)</option>
   <option value="502">Guatemala (502)</option>
   <option value="224">Guinea (224)</option>
   <option value="245">Guinea-Bissau (245)</option>
   <option value="592">Guyana (592)</option>
   <option value="509">Haiti (509)</option>
   <option value="504">Honduras (504)</option>
   <option value="852">Hong Kong (852)</option>
   <option value="36">Hungary (36)</option>
   <option value="354">Iceland (354)</option>
   <option value="91">India (91)</option>
   <option value="62">Indonesia (62)</option>
   <option value="98">Iran (Islamic Republic of) (98)</option>
   <option value="964">Iraq (964)</option>
   <option value="353">Ireland (353)</option>
   <option value="972">Israel (972)</option>
   <option value="39">Italy (39)</option>
   <option value="1876">Jamaica (876)</option>
   <option value="81">Japan (81)</option>
   <option value="962">Jordan (962)</option>
   <option value="7">Russian Federation (7)</option>
   <option value="254">Kenya (254)</option>
   <option value="686">Kiribati (686)</option>
   <option value="82">Korea, Republic of (82)</option>
   <option value="965">Kuwait (965)</option>
   <option value="996">Kyrgyzstan (996)</option>
   <option value="856">Laos (856)</option>
   <option value="371">Latvia (371)</option>
   <option value="961">Lebanon (961)</option>
   <option value="266">Lesotho (266)</option>
   <option value="231">Liberia (231)</option>
   <option value="218">Libyan Arab Jamahiriya (218)</option>
   <option value="423">Liechtenstein (423)</option>
   <option value="370">Lithuania (370)</option>
   <option value="352">Luxembourg (352)</option>
   <option value="853">Macau (853)</option>
   <option value="389">Macedonia (389)</option>
   <option value="261">Madagascar (261)</option>
   <option value="265">Malawi (265)</option>
   <option value="60">Malaysia (60)</option>
   <option value="960">Maldives (960)</option>
   <option value="223">Mali (223)</option>
   <option value="356">Malta (356)</option>
   <option value="692">Marshall Islands (692)</option>
   <option value="596">Martinique (596)</option>
   <option value="222">Mauritania (222)</option>
   <option value="230">Mauritius (230)</option>
   <option value="52">Mexico (52)</option>
   <option value="691">Micronesia (691)</option>
   <option value="373">Moldova, Republic of (373)</option>
   <option value="377">Monaco (377)</option>
   <option value="976">Mongolia (976)</option>
   <option value="664">Montserrat (664)</option>
   <option value="212">Morocco (212)</option>
   <option value="258">Mozambique (258)</option>
   <option value="95">Myanmar (95)</option>
   <option value="264">Namibia (264)</option>
   <option value="674">Nauru (674)</option>
   <option value="977">Nepal (977)</option>
   <option value="31">Netherlands (31)</option>
   <option value="599">Netherlands Antilles (599)</option>
   <option value="687">New Caledonia (687)</option>
   <option value="64">New Zealand (64)</option>
   <option value="505">Nicaragua (505)</option>
   <option value="227">Niger (227)</option>
   <option value="234">Nigeria (234)</option>
   <option value="683">Niue (683)</option>
   <option value="850">North Korea (850)</option>
   <option value="47">Norway (47)</option>
   <option value="968">Oman (968)</option>
   <option value="92">Pakistan (92)</option>
   <option value="680">Palau (680)</option>
   <option value="970">Palestine (970)</option>
   <option value="507">Panama (507)</option>
   <option value="675">Papua New Guinea (675)</option>
   <option value="595">Paraguay (595)</option>
   <option value="51">Peru (51)</option>
   <option value="63">Philippines (63)</option>
   <option value="48">Poland (48)</option>
   <option value="351">Portugal (351)</option>
   <option value="787">Puerto Rico (787)</option>
   <option value="974">Qatar (974)</option>
   <option value="262">Réunion (262)</option>
   <option value="40">Romania (40)</option>
   <option value="250">Rwanda (250)</option>
   <option value="290">Saint Helena (290)</option>
   <option value="869">Saint Kitts and Nevis (869)</option>
   <option value="758">Saint Lucia (758)</option>
   <option value="508">Saint Pierre and Miquelon (508)</option>
   <option value="784">Saint Vincent &amp; Grenadines (784)</option>
   <option value="685">Samoa (685)</option>
   <option value="378">San Marino (378)</option>
   <option value="239">São Tome and Principe (239)</option>
   <option value="966">Saudi Arabia (966)</option>
   <option value="221">Senegal (221)</option>
   <option value="248">Seychelles (248)</option>
   <option value="232">Sierra Leone (232)</option>
   <option value="65">Singapore (65)</option>
   <option value="721">Sint Maarten (721)</option>
   <option value="421">Slovakia (421)</option>
   <option value="386">Slovenia (386)</option>
   <option value="677">Solomon Islands (677)</option>
   <option value="252">Somalia (252)</option>
   <option value="27">South Africa (27)</option>
   <option value="34">Spain (34)</option>
   <option value="94">Sri Lanka (94)</option>
   <option value="249">Sudan (249)</option>
   <option value="597">Suriname (597)</option>
   <option value="268">Swaziland (268)</option>
   <option value="46">Sweden (46)</option>
   <option value="41">Switzerland (41)</option>
   <option value="963">Syrian Arab Republic (963)</option>
   <option value="886">Taiwan (886)</option>
   <option value="992">Tajikistan (992)</option>
   <option value="255">Tanzania, United Republic of (255)</option>
   <option value="66">Thailand (66)</option>
   <option value="228">Togo (228)</option>
   <option value="690">Tokelau Islands (690)</option>
   <option value="676">Tonga (676)</option>
   <option value="868">Trinidad and Tobago (868)</option>
   <option value="216">Tunisia (216)</option>
   <option value="90">Turkey (90)</option>
   <option value="993">Turkmenistan (993)</option>
   <option value="1649">Turks and Caicos Islands (649)</option>
   <option value="688">Tuvalu (688)</option>
   <option value="256">Uganda (256)</option>
   <option value="380">Ukraine (380)</option>
   <option value="971">United Arab Emirates (971)</option>
   <option value="44">United Kingdom (44)</option>
   <option value="598">Uruguay (598)</option>
   <option value="998">Uzbekistan (998)</option>
   <option value="678">Vanuatu (678)</option>
   <option value="379">Vatican City State (Holy See) (379)</option>
   <option value="58">Venezuela (58)</option>
   <option value="84">Viet Nam (84)</option>
   <option value="809">Virgin Islands (GB) (809)</option>
   <option value="1340">Virgin Islands (US) (340)</option>
   <option value="681">Wallis and Futuna Islands (681)</option>
   <option value="967">Yemen (967)</option>
   <option value="381">Yugoslavia (381)</option>
   <option value="260">Zambia (260)</option>
   <option value="263">Zimbabwe (263)</option>
</select>
          </div>
          <div class="col-md-6">
          <input type="text" maxlength="255" class="form-control" placeholder="Phone *">
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-6">
          <input type="text" id="check_in" />
          </div>
          <div class="col-md-6">
          <input type="text" id="check_out" />
          </div>
        </div>
        <div class="form-group row">
          <div class="col-md-6">
          <select  class="form-control">
   <option value="">Adults</option>
   <option value="1">1</option>
   <option value="2">2</option>
   <option value="3">3</option>
   <option value="4">4</option>
   <option value="5">5</option>
   <option value="6">6</option>
   <option value="7">7</option>
   <option value="8">8</option>
   <option value="9">9</option>
   <option value="10">10</option>
   <option value="11">11</option>
   <option value="12">12</option>
   <option value="13">13</option>
   <option value="14">14</option>
   <option value="15">15</option>
   <option value="16">16</option>
   <option value="17">17</option>
   <option value="18">18</option>
   <option value="19">19</option>
   <option value="20">20</option>
   <option value="21">21</option>
   <option value="22">22</option>
   <option value="23">23</option>
   <option value="24">24</option>
   <option value="25">25</option>
   <option value="26">26</option>
   <option value="27">27</option>
   <option value="28">28</option>
   <option value="29">29</option>
   <option value="30">30</option>
   <option value="31">31</option>
   <option value="32">32</option>
   <option value="33">33</option>
   <option value="34">34</option>
   <option value="35">35</option>
   <option value="36">36</option>
   <option value="37">37</option>
   <option value="38">38</option>
   <option value="39">39</option>
   <option value="40">40</option>
</select>
          </div>
          <div class="col-md-6">
          <select class="form-control" >
   <option value="">Children</option>
   <option value="1">1</option>
   <option value="2">2</option>
   <option value="3">3</option>
   <option value="4">4</option>
   <option value="5">5</option>
   <option value="6">6</option>
   <option value="7">7</option>
   <option value="8">8</option>
   <option value="9">9</option>
   <option value="10">10</option>
   <option value="11">11</option>
   <option value="12">12</option>
   <option value="13">13</option>
   <option value="14">14</option>
   <option value="15">15</option>
   <option value="16">16</option>
   <option value="17">17</option>
   <option value="18">18</option>
   <option value="19">19</option>
   <option value="20">20</option>
   <option value="21">21</option>
   <option value="22">22</option>
   <option value="23">23</option>
   <option value="24">24</option>
   <option value="25">25</option>
   <option value="26">26</option>
   <option value="27">27</option>
   <option value="28">28</option>
   <option value="29">29</option>
   <option value="30">30</option>
   <option value="31">31</option>
   <option value="32">32</option>
   <option value="33">33</option>
   <option value="34">34</option>
   <option value="35">35</option>
   <option value="36">36</option>
   <option value="37">37</option>
   <option value="38">38</option>
   <option value="39">39</option>
   <option value="40">40</option>
</select>
          </div>
        </div>
        <div class="form-group">
        <textarea rows="2" placeholder="Message to owner" class="form-control"></textarea>
        </div>
       
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Submit</button>
      </div>
    </div>
  </div>
</div>
  @stop
  {!! Html::script('js/jquery-1.11.3.js') !!}
  <style type="text/css">
    .show_off{
      display: none;
    }
    #pricing.fixed .tooltip-amenity.tooltip-bottom-middle{display: none !important;}
    .tooltip-amenity.tooltip-bottom-middle::before {
      content: "";
      display: inline-block;
      position: absolute;
      bottom: -10px;
      left: 50% !important;
      margin-left: -10px;
      top: auto !important;
      border: 10px solid transparent;
      border-bottom: 0;
      border-top-color: rgba(0, 0, 0, 0.1);
  }
  .tooltip-amenity{border-radius: 3px !important;}
  .tooltip-amenity.tooltip-bottom-middle::after {
      content: "";
      display: inline-block;
      position: absolute;
      bottom: -9px;
      left: 50% !important;
      margin-left: -9px;
      top: auto !important;
      border: 9px solid transparent;
      border-bottom: 0;
      border-top-color: #fff;
  }
  .tooltip-amenity1, .tooltip-amenity2 {
      min-width: 274px !important;
  }
  @media (max-width: 767px) {
  .ad-gallery .ad-image-wrapper .ad-image{
          width: 66% !important;
      left: 16% !important;
  }
  }
  @media (max-width: 1000px) {
  .tooltip-amenity1, .tooltip-amenity2 {
        left: -85px !important;
      top: -120px;
      min-width: 184px !important;
  }
  }
  </style>
  <script type="text/javascript">

  $(document).ready(function() {
    $("#arrival_instructions").click(function(e){
     e.stopPropagation();
   });
   $("#rental_contract").click(function(e){
     e.stopPropagation();
   });
   $("#things_to_do").click(function(e){
     e.stopPropagation();
   });
    $('.div_check').show();
      var type_id=4;
      $( ".get_type" ).each( function() {
        var new_val=$(this).data('id');
        if(new_val == type_id)
        {
          $( ".new_id"+type_id ).addClass("show_off");
        }
      });

      $("#wish_list_text").keyup(function(){
          $('#wish_list_btn').prop('disabled', true);
            var v_value =  $(this).val();
            var len =v_value.trim().length;
            // alert(len);
              if (len == 0)
              {
                $('#wish_list_btn').prop('disabled', true);
              }
              else{
                $('#wish_list_btn').prop('disabled', false);
              }
        });
  // $('#wish_list_btn').click(function(){
  //     $('.div_check').show();
  // });
  });


  $(window).load(function(){
    setTimeout(function(){  $('.loader').hide(); }, 3000);
  });
  $(document).on('mouseenter','#view_details_popup',function(){
    $(".details_popup").addClass('show');
  });
  $(document).on('mouseleave','#view_details_popup',function(){
    $(".details_popup").removeClass('show');
  });

  $(document).on('click','#view_details_popup',function(){
    $(".details_popup").toggleClass('show');
  });

    setTimeout(function() {
      $("#book_it").trigger('click');
      },1000);





  </script>
