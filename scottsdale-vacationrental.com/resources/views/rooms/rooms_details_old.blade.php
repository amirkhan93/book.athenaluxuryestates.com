<?php
	function time_elapsed_string($datetime, $full = false) {
		$now = new DateTime;
		$ago = new DateTime($datetime);
		$diff = $now->diff($ago);

		$diff->w = floor($diff->d / 7);
		$diff->d -= $diff->w * 7;

		$string = array(
			'y' => 'year',
			'm' => 'month',
			'w' => 'week',
			'd' => 'day',
			'h' => 'hour',
			'i' => 'minute',
			's' => 'second',
		);
		foreach ($string as $k => &$v) {
			if ($diff->$k) {
				$v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
			} else {
				unset($string[$k]);
			}
		}

		if (!$full) $string = array_slice($string, 0, 1);
		return $string ? implode(', ', $string) . ' ago' : 'just now';
	}
?>
<head>

    <!-- Basic Page Needs
	================================================== -->
    <meta charset="utf-8">
    <title>Luxvacationrentalhomes</title>



    <!-- Mobile Specific Metas
	================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


    <!-- Web Fonts 
	================================================== -->
    <link
        href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet" />
    <link
        href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
        rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.materialdesignicons.com/3.8.95/css/materialdesignicons.min.css">

    <!-- CSS
	================================================== -->
   
   
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/colors/color.css" />
    <link rel="stylesheet" href="{{url()}}/resources/assets/home/css/bootstrap.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/font-awesome.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/ionicons.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/datepicker.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/jquery.fancybox.min.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/owl.carousel.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/owl.transitions.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/lightslider.min.css" />
	 <link rel="stylesheet" href="https://cdn.rawgit.com/filamentgroup/fixed-sticky/master/fixedsticky.css">
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/style.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/colors/color.css" />
	<style>
		#list {
			margin-top:1.5em;
		}
		.price_table {
			background:#fff!important;
			font-size: 14.5px;
			color: #000000b8;
			margin-bottom:0px!important;
		}
	</style>

</head>

<body>

    <div class="loader">
        <div class="loader__figure"></div>
    </div>

    <svg class="hidden">
        <svg id="icon-nav" viewBox="0 0 152 63">
            <title>navarrow</title>
            <path
                d="M115.737 29L92.77 6.283c-.932-.92-1.21-2.84-.617-4.281.594-1.443 1.837-1.862 2.765-.953l28.429 28.116c.574.57.925 1.557.925 2.619 0 1.06-.351 2.046-.925 2.616l-28.43 28.114c-.336.327-.707.486-1.074.486-.659 0-1.307-.509-1.69-1.437-.593-1.442-.315-3.362.617-4.284L115.299 35H3.442C2.032 35 .89 33.656.89 32c0-1.658 1.143-3 2.552-3H115.737z" />
        </svg>
    </svg>


    <!-- Nav and Logo
	================================================== -->

    <nav id="menu-wrap" class="menu-back cbp-af-header">
        <div class="menu-top background-black">
            <div class="">
                <div class="menu">
                    <a href="{{url()}}">
                        <div class="logo">
                            <img src="{{url()}}/resources/assets/home/img/logo.png" alt="">
                        </div>
                    </a>
                    <ul>
                        <li>
                            <a href="#">Find Homes</a>
                        </li>
                        <li>
                            <a href="#">Parners</a>
                        </li>

                        <li>
                            <a href="#">Company Retreats</a>
                        </li>
                        <li>
                            <a href="#">contact</a>
                        </li>
                        <li>
                            <a href="#">More</a>
                            <ul>
                                <li><a href="">Careers</a></li>
                                <li><a href="">About Us</a></li>

                            </ul>
                        </li>

                    </ul>
                    <ul class="float-right ml-0">
                        <li>
                            @if(!Auth::user()->check())
							<a href="{{url()}}/login"><span>Login & Signup</span></a>
							@else
							<a href="{{url()}}/dashboard"><span>My Account</span></a>	
							@endif
                        </li>
                    </ul>
                </div>
            </div>
        </div>

    </nav>
    <div class="section big-55-height over-hide">
        <div class="parallax parallax-top" style="background-image: url('{{url()}}/images/{{ $result->photo_name }}')"></div>
        <div class="dark-over-pages"></div>

        <div class="hero-center-section pages">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-12 parallax-fade-top">
                        <div class="hero-text">{{ $result->name }}</div>
                        <h5 class="text-white">{{ $address->address_line_1 }}, {{ $address->city }} , {{ $address->state }} , {{ $address->country_name }} </h5>
				   </div>
                </div>
            </div>
        </div>
    </div>
    <div class="section background-dark padding-top-bottom-smaller over-hide">
        <div class="section z-bigger">
            <div class="container">
                <div class="row">
                        <div class="col-6 col-sm-6 col-md-4 col-lg-2 text-center mt-4 mt-md-0">
                                <div class="amenities">
                                    <i class="mdi mdi-home-city"></i>
                                    <p>{{ $result->property_type_name }}</p>
                                </div>
                            </div>
                    <div class="col-6 col-sm-6 col-md-4 col-lg-2 text-center">
                        <div class="amenities">
                            <i class="mdi mdi-account-multiple"></i>
                            <p>Sleeps: {{ $result->accommodates }}</p>
                        </div>
                    </div>
                   
                    <div class="col-6 col-sm-6 col-md-4 col-lg-2 text-center">
                            <div class="amenities">
                               <i class="mdi mdi-door-open"></i>
                                <p>Bedrooms: {{ $result->bedrooms }}</p>
                            </div>
                        </div>
                    <div class="col-6 col-sm-6 col-md-4 col-lg-2 text-center mt-4 mt-lg-0">
                        <div class="amenities">
                           <i class="mdi mdi-shower"></i>
                            <p>Bathrooms: {{ $result->bathrooms }}</p>
                        </div>
                    </div>
                    <div class="col-6 col-sm-6 col-md-4 col-lg-2 text-center mt-4 mt-lg-0">
                        <div class="amenities">
                            <i class="mdi mdi-hot-tub"></i>
                            <p>Half Bath</p>
                        </div>
                    </div>
                    <div class="col-6 col-sm-6 col-md-4 col-lg-2 text-center mt-4 mt-lg-0">
                        <div class="amenities">
                            <i class="mdi mdi-weather-night"></i>
                            <p>Min Stay: 3 nights</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="section padding-top-bottom z-bigger">
        <div class="section z-bigger">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 mt-4 mt-lg-0">
                        <div class="section">
                            <div class="customNavigation rooms-sinc-1-2">
                                <a class="next-rooms-sync-1"></a>
                                <a class="prev-rooms-sync-1"></a>

                            </div>
                            <div id="rooms-sync1" class="owl-carousel">
								@foreach($rooms_photos as $row)
                                <div class="item">
                                    <img src="{{url()}}/images/rooms/{{ $row->room_id }}/{{ $row->name }}" alt="">
                                </div>
								@endforeach
                            </div>
                        </div>
                        <div class="section">
                            <div id="rooms-sync2" class="owl-carousel">
                                @foreach($rooms_photos as $row)
                                <div class="item">
                                    <img src="{{url()}}/images/rooms/{{ $row->room_id }}/{{ $row->name }}" alt="">
                                </div>
								@endforeach
                            </div>
                        </div>
                        <div class="section pt-5">
                            <h5>discription</h5>
                            <p class="mt-3">{{ $result->summary }}</p>
                        </div>
                        <div class="section pt-4">
                            <div class="row">
                                <div class="col-12">
                                    <h5 class="mb-3">Amenities</h5>
                                </div>
								@foreach($amenities as $am)
                                <div class="col-lg-4">
                                    <p><strong class="color-black"><i class="mdi mdi-{{ $am->icon }}"></i>{{ $am->name }}</strong> </p>
                                </div>
								@endforeach
                            </div>
                        </div>
                        <div class="section pt-4">
                            <h5>Bathrooms</h5>
                            <h6 class="mt-3 mb-3"><i>{{ $result->bathrooms }} Bathrooms</i></h6>
                        </div>
                        <div class="section pt-4">
							@foreach($reviews as $user)
							<div class="Review">
								<div class="Review-details">
									<img src="{{url()}}/img/profile1.jpg" style="border: 1px solid #b3b3b3;">
									<div class="Review-meta">
										<p class="Review-author">{{ $user->username }}</p>
										<p class="Review-date"><?php echo time_elapsed_string($user->created_at);?></p>
										<div class="Review-rating">
										<span class="Review-star Review-star--active">&#9733;</span>
										<span class="Review-star Review-star--active">&#9733;</span>
										<span class="Review-star Review-star--active">&#9733;</span>
										<span class="Review-star">&#9733;</span>
										<span class="Review-star">&#9733;</span>
										</div>
									</div>
								</div>

								<div class="Review-body">
									<h3 class="Review-title">{{ $user->review_title }} </h3>
									<p>{{ $user->review }}</p>
								</div>
							</div>
							@endforeach
                        </div>
                        <div class="section pt-4">
                            <iframe width="100%" height="70%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q={{ $address->latitude }},{{ $address->longitude }}&hl=es;z=14&amp;output=embed"></iframe>
							
						</div>
                    </div>
                    <div class="col-lg-4 order-first order-lg-last sidebar-right fixedsticky">
                        <div class="section background-dark p-4">
                            <div class="row">
								<form accept-charset="UTF-8" action="{{ url('payments/book/'.$room_id) }}" id="book_it_form" method="post">
								{!! Form::token() !!}
                                <div class="col-12">
                                    <div class="input-daterange input-group" id="flight-datepicker">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-item">
                                                    <span class="fontawesome-calendar"></span>
                                                    <input readonly="readonly" class="checkin input-sm" type="text" autocomplete="off" id="list_checkin" name="checkin" placeholder="{{ trans('messages.rooms.dd-mm-yyyy') }}" />
                                                    <span class="date-text date-depart"></span>
													
													<input readonly="readonly" type="hidden" ng-model="room_id" ng-init="room_id = {{ $room_id }}">
													<input type="hidden" id="room_blocked_dates" value="" >
													<input type="hidden" id="calendar_available_price" value="" >
													<input type="hidden" id="room_available_price" value="" >
													<input type="hidden" id="price_tooltip" value="" >
													<input type="hidden" id="weekend_price_tooltip" value="" >
													<input type="hidden" id="url_checkin" value="{{ $checkin }}" >
													<input type="hidden" id="url_checkout" value="{{ $checkout }}" >
													<input type="hidden" id="url_guests" value="{{ $guests }}" >
													<input type="hidden" name="booking_type" id="booking_type" value="{{ $result->booking_type }}" >
													<input type="hidden" name="cancellation" id="cancellation" value="{{ $result->cancel_policy }}" >
													
                                                </div>
                                            </div>
                                            <div class="col-12 pt-4">
                                                <div class="form-item">
                                                    <span class="fontawesome-calendar"></span>
                                                    <input readonly="readonly" class="checkout input-sm" type="text" autocomplete="off" id="list_checkout" name="checkout" placeholder="{{ trans('messages.rooms.dd-mm-yyyy') }}" />
                                                    <span class="date-text date-return"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-12 pt-4">
                                            <select id="number_of_guests" name="number_of_guests" class="wide">
											@for($i=1;$i<= $result->accommodates;$i++)
											<option value="{{ $i }}"> {{ $i }}</option>
											 @endfor
											</select>
                                        </div>

                                    </div>
                                </div>
								<div class="col-12" id="list" style="display:none;">
									
									<table class="table table-bordered price_table" >
									<tbody>
									<tr>
									<td class="pos-rel room-night">
									<span class="lang-chang-label"> {{ $result->rooms_price->currency->symbol }}</span>  <span  class="lang-chang-label" id="rooms_price_amount_1" value="">{{ $result->rooms_price->night }}</span> <span class="lang-chang-label">  x </span><span  id="total_night_count" value="">0</span> {{ trans_choice('messages.rooms.night',1) }}
									<i id="service-fee-tooltip" rel="tooltip" class="icon icon-question" title="{{ trans('messages.rooms.avg_night_rate') }}" ></i>
									</td>
									<td><span class="lang-chang-label">{{ $result->rooms_price->currency->symbol }}</span><span  id="total_night_price" value="">0</span></td>
									</tr>



									<tr class = "additional_price"> 
									<td>
									{{ trans('messages.rooms.addtional_guest_fee') }}
									</td>
									<td>{{ $result->rooms_price->currency->symbol }}<span  id="additional_guest" value="">0</span></td>
									</tr>

									<tr class = "security_price"> 
									<td>
									{{ trans('messages.rooms.security_fee') }}
									</td>
									<td>{{ $result->rooms_price->currency->symbol }}<span  id="security_fee" value="">0</span></td>
									</tr>

									<tr class = "cleaning_price"> 
									<td>
									{{ trans('messages.rooms.cleaning_fee') }}
									</td>
									<td>{{ $result->rooms_price->currency->symbol }}<span  id="cleaning_fee" value="">0</span></td>
									</tr>
									<tr class = "pet_price">
									<td>
									Pet Fee
									</td>
									<td>{{ $result->rooms_price->currency->symbol }}<span  id="pet_fee" value="">0</span></td>
									</tr>
									<tr class = "pool_price">
									<td>
									Pool Fee
									</td>
									<td>{{ $result->rooms_price->currency->symbol }}<span  id="pool_fee" value="">0</span></td>
									</tr>
									<tr class = "admin_price">
									<td>
									Admin Fee
									</td>
									<td>{{ $result->rooms_price->currency->symbol }}<span  id="admin_fee" value="">0</span></td>
									</tr>
									<tr class = "subtotal_price">
									<td>
									Subtotal
									</td>
									<td>{{ $result->rooms_price->currency->symbol }}<span  id="subtotal_fee" value="">0</span></td>
									</tr>
									<tr class = "tax_price">
									<td>
									Tax
									</td>
									<td>{{ $result->rooms_price->currency->symbol }}<span  id="tax_fee" value="">0</span></td>
									</tr>
									<tr>
									<td>{{ trans('messages.rooms.total') }}</td>
									<td><span class="lang-chang-label">{{ $result->rooms_price->currency->symbol }}</span><span  id="total" value="">0</span></td>
									</tr>
									<tr  class = "installs" style="display: none">
									<td id="installLabel"></td>
									<td><span class="lang-chang-label">{{ $result->rooms_price->currency->symbol }}</span><span  id="installmentpay" value="">0</span></td>
									</tr>

									</tbody>
									</table>
									
									
								</div>
                                <div class="col-12 pt-4">
									<div id="book_it_disabled" class="text-center" style="display:none;">
										<p id="book_it_disabled_message" class="icon-rausch">
										{{ trans('messages.rooms.dates_not_available') }}
										</p>
										<a href="{{URL::to('/')}}/search/{{$result->rooms_address->city }}" class="btn btn-large btn-block" id="view_other_listings_button">
										{{ trans('messages.rooms.view_other_listings') }}
										</a>
									</div>          
									<div class="js-book-it-btn-container {{ ($result->user_id == @Auth::user()->user()->id) ? 'hide' : '' }}">
										<button type="submit" class="booking-button">
											@if($result->booking_type!='instant_book')
											<span class="book-it__btn-text">
											{{ trans('messages.rooms.request_to_book') }}
											</span>
											@else
											<span class="book-it__btn-text--instant">
											{{ trans('messages.lys.instant_book') }}
											</span>
											@endif
										</button> 
										<input type="hidden" name="instant_book" value="{{ $result->booking_type }}">
									</div>									
                                </div>
								<input id="hosting_id" name="hosting_id" type="hidden" value="{{ $result->id }}">
								<input id="room_types" name="room_types" type="hidden" value="{{ $room_types }}">
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="section padding-top-bottom-small background-black over-hide footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 text-center text-md-left">
                    <img src="{{url()}}/resources/assets/home/img/logo.png" alt="">
                    <p class="color-grey mb-4">At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis
                        praesentium voluptatum deleniti atque corrupti quos dolores.</p>
                    <p class="color-grey mt-4">Avenue Street 3284<br>Thessaloniki</p>
                </div>
                <div class="col text-center text-md-left">
                    <h6 class="color-white mb-3">Follow us</h6>
                    <a href="#"><i class="mdi mdi-facebook"></i> facebook</a>
                    <a href="#"><i class="mdi mdi-twitter"></i> twitter</a>
                    <a href="#"><i class="mdi mdi-instagram"></i> instagram</a>

                </div>
                <div class="col mt-4 mt-md-0 text-center text-md-left logos-footer">
                    <h6 class="color-white mb-3"> luxvacationrentalhomes</h6>
                    <a href="#">Find Homes</a>
                    <a href="#">services</a>
                    <a href="#">About Us</a>
                    <a href="#">Contact Us</a>


                </div>
                <div class="col mt-4 mt-md-0 text-center text-md-left logos-footer">
                    <h6 class="color-white mb-3">information</h6>
                    <a href="#">terms &amp; conditions</a>
                    <a href="#">House Rule</a>
                    <a href="#">Privacy Policy</a>
                    <a href="#">testimonials</a>


                </div>
            </div>
        </div>
    </div>

    <div class="section py-4 background-dark over-hide footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-center text-md-left mb-2 mb-md-0">
                    <p>2019 © Luxvacationrentalhomes. All rights reserved.</p>
                </div>

            </div>
        </div>
    </div>


    <div class="scroll-to-top"></div>


    <!-- JAVASCRIPT
    ================================================== -->
    <script src="{{url()}}/resources/assets/home/js/jquery.min.js"></script>
	<script src="{{url()}}/resources/assets/home/js/popper.min.js"></script>
	<script src="{{url()}}/resources/assets/home/js/bootstrap.min.js"></script>
	<script src="{{url()}}/resources/assets/home/js/plugins.js"></script>
	<script src="{{url()}}/resources/assets/home/js/lightslider.min.js"></script>
	<script src="{{url()}}/resources/assets/home/js/flip-slider.js"></script>
	<script src="{{url()}}/resources/assets/home/js/reveal-home.js"></script>
	<script src="https://cdn.rawgit.com/filamentgroup/fixed-sticky/master/fixedsticky.js"></script>
	 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCAs5uiBN5vxbB-h-Zs6fJGJpQvuQxOxro&libraries=places&callback=initMap"></script>
	<script src="{{url()}}/resources/assets/home/js/custom.js"></script>
    <script>
		$( '.sidebar-right' ).fixedsticky();
		$(document).ready(function(){
			/*$("#list_checkin, #list_checkout, #number_of_guests").change(function(){
				$("#list").css("display","block");
			});*/
			
			/*$("#start-date-1").change(function(e){
				e.preventDefault();
				start = $("#start-date-1").val();
				end = $("#end-date-1").val();
				alert(start);
				alert(end);
			});*/
		});
    </script>
</body>
</html>