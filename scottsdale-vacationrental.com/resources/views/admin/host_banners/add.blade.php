@extends('admin.template')

@section('main')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Host Banners
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Host Banners</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- right column -->
        <div class="col-md-8 col-sm-offset-2">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Add Host Banners Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
              {!! Form::open(['url' => 'admin/add_host_banners', 'class' => 'form-horizontal', 'files' => true]) !!}
              <div class="box-body">
              <span class="text-danger">(*)Fields are Mandatory</span>

                <div class="form-group">
                  <label for="input_description" class="col-sm-3 control-label">Image<em class="text-danger">*</em></label>
                  <div class="col-sm-6">
                    {!! Form::file('image', '', ['class' => 'form-control', 'id' => 'input_image', 'accept' => 'image/*']) !!}
                    <span class="text-danger">{{ $errors->first('image') }}</span>
                  </div>
                </div>

                <div class="form-group">
                  <label for="input_title" class="col-sm-3 control-label">Title<em class="text-danger">*</em></label>
                  <div class="col-sm-6">
                    {!! Form::text('title', '', ['class' => 'form-control', 'id' => 'input_title', 'placeholder' => 'Title']) !!}
                    <span class="text-danger">{{ $errors->first('title') }}</span>
                  </div>
				   
                </div>
				 <div class="form-group">
                  <label for="input_title" class="col-sm-3 control-label">Arabic Title<em class="text-danger">*</em></label>
                  <div class="col-sm-6">
                    {!! Form::text('arabic_title', '', ['class' => 'form-control', 'id' => 'input_arabic_title', 'placeholder' => 'Arabic Title']) !!}
                    <span class="text-danger">{{ $errors->first('arabic_title') }}</span>
                  </div>
				   
                </div>

                <div class="form-group">
                  <label for="input_description" class="col-sm-3 control-label">Description<em class="text-danger">*</em></label>
                  <div class="col-sm-6">
                    {!! Form::textarea('description', '', ['class' => 'form-control', 'id' => 'input_description', 'placeholder' => 'Description']) !!}
                    <span class="text-danger">{{ $errors->first('description') }}</span>
                  </div>
                </div>
				<div class="form-group">
                  <label for="input_description" class="col-sm-3 control-label">Arabic Description<em class="text-danger">*</em></label>
                  <div class="col-sm-6">
                    {!! Form::textarea('arabic_description', '', ['class' => 'form-control', 'id' => 'input_arabic_description', 'placeholder' => 'Arabic Description']) !!}
                    <span class="text-danger">{{ $errors->first('arabic_description') }}</span>
                  </div>
                </div>

                <div class="form-group">
                  <label for="input_link_title" class="col-sm-3 control-label">Link Title<em class="text-danger">*</em></label>
                  <div class="col-sm-6">
                    {!! Form::text('link_title', '', ['class' => 'form-control', 'id' => 'input_link_title', 'placeholder' => 'Link Title']) !!}
                    <span class="text-danger">{{ $errors->first('link_title') }}</span>
                  </div>
                </div>
				<div class="form-group">
                  <label for="input_link_title" class="col-sm-3 control-label">Arabic Link Title<em class="text-danger">*</em></label>
                  <div class="col-sm-6">
                    {!! Form::text('arabic_link_title', '', ['class' => 'form-control', 'id' => 'input_arabic_link_title', 'placeholder' => 'Arabic Link Title']) !!}
                    <span class="text-danger">{{ $errors->first('arabic_link_title') }}</span>
                  </div>
                </div>

                <div class="form-group">
                  <label for="input_link" class="col-sm-3 control-label">Link<em class="text-danger">*</em></label>
                  <div class="col-sm-6">
                    {!! Form::text('link', '', ['class' => 'form-control', 'id' => 'input_link', 'placeholder' => 'Link']) !!}
                    <span class="text-danger">{{ $errors->first('link') }}</span>
                  </div>
                </div>
				<div class="form-group">
                  <label for="input_link" class="col-sm-3 control-label">Arabic Link<em class="text-danger">*</em></label>
                  <div class="col-sm-6">
                    {!! Form::text('arabic_link', '', ['class' => 'form-control', 'id' => 'input_arabic_link', 'placeholder' => 'Arabic Link']) !!}
                    <span class="text-danger">{{ $errors->first('arabic_link') }}</span>
                  </div>
                </div>

              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right" name="submit" value="submit">Submit</button>
                 <button type="submit" class="btn btn-default pull-left" name="cancel" value="cancel">Cancel</button>
              </div>
              <!-- /.box-footer -->
            {!! Form::close() !!}
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@stop