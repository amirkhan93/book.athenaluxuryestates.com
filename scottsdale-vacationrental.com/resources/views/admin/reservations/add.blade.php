@extends('admin.template')

@section('main')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Add Reservation
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Reservation</a></li>
        <li class="active">Add</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- right column -->
        <div class="col-md-8 col-sm-offset-2">
          <!-- Horizontal Form -->
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Add Reservation Form</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            {!! Form::open(['url' => 'admin/add_reservation', 'class' => 'form-horizontal']) !!}
              <div class="box-body">
              <span class="text-danger">(*)Fields are Mandatory</span>
				<div class="form-group">
                  <label for="input_status" class="col-sm-3 control-label">Property<em class="text-danger">*</em></label>

                  <div class="col-sm-6">
                    {!! Form::select('room', $rooms, '', ['class' => 'form-control', 'id' => 'input_room', 'placeholder' => 'Select']) !!}
                    <span class="text-danger">{{ $errors->first('room') }}</span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input_first_name" class="col-sm-3 control-label">Guest Name<em class="text-danger">*</em></label>

                  <div class="col-sm-6">
                    {!! Form::text('guest_name', '', ['class' => 'form-control', 'id' => 'input_guest_name', 'placeholder' => 'Guest Name']) !!}
                    <span class="text-danger">{{ $errors->first('guest_name') }}</span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input_last_name" class="col-sm-3 control-label">Check In<em class="text-danger">*</em></label>

                  <div class="col-sm-6">
                    {!! Form::text('checkin', '', ['class' => 'form-control', 'id' => 'input_checkin', 'placeholder' => 'Check In', 'autocomplete' => 'off']) !!}
                    <span class="text-danger">{{ $errors->first('checkin') }}</span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input_email" class="col-sm-3 control-label">Check Out<em class="text-danger">*</em></label>

                  <div class="col-sm-6">
                    {!! Form::text('checkout', '', ['class' => 'form-control', 'id' => 'input_checkout', 'placeholder' => 'Check Out', 'autocomplete' => 'off']) !!}
                    <span class="text-danger">{{ $errors->first('checkout') }}</span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input_phone_no" class="col-sm-3 control-label">No Of guest<em class="text-danger">*</em></label>

                  <div class="col-sm-6">
                    {!! Form::text('guest', '', ['class' => 'form-control', 'id' => 'input_guest', 'placeholder' => 'No Of guest']) !!}
                    <span class="text-danger">{{ $errors->first('guest') }}</span>
                  </div>
                </div> 

                <div class="form-group">
                  <label for="input_password" class="col-sm-3 control-label">Total Nights<em class="text-danger">*</em></label>

                  <div class="col-sm-6">
                    {!! Form::text('nights', '', ['class' => 'form-control', 'id' => 'input_nights', 'placeholder' => 'Total Nights']) !!}
                    <span class="text-danger">{{ $errors->first('nights') }}</span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input_dob" class="col-sm-3 control-label">Email<em class="text-danger">*</em></label>

                  <div class="col-sm-6">
                    {!! Form::text('email', '', ['class' => 'form-control', 'id' => 'input_email', 'placeholder' => 'Email']) !!}
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                  </div>
                </div>
				<div class="form-group">
                  <label for="input_dob" class="col-sm-3 control-label">Phone<em class="text-danger">*</em></label>

                  <div class="col-sm-6">
                    {!! Form::text('phone', '', ['class' => 'form-control', 'id' => 'input_phone', 'placeholder' => 'Phone']) !!}
                    <span class="text-danger">{{ $errors->first('phone') }}</span>
                  </div>
                </div>
				<div class="form-group">
                  <label for="input_dob" class="col-sm-3 control-label">Total Amount<em class="text-danger">*</em></label>

                  <div class="col-sm-6">
                    {!! Form::text('amount', '', ['class' => 'form-control', 'id' => 'input_amount', 'placeholder' => 'Amount']) !!}
                    <span class="text-danger">{{ $errors->first('amount') }}</span>
                  </div>
                </div>
				<div class="form-group">
                  <label for="input_dob" class="col-sm-3 control-label">Cleaning<em class="text-danger">*</em></label>

                  <div class="col-sm-6">
                    {!! Form::text('cleaning', '', ['class' => 'form-control', 'id' => 'input_cleaning', 'placeholder' => 'Cleaning']) !!}
                    <span class="text-danger">{{ $errors->first('cleaning') }}</span>
                  </div>
                </div>
				<div class="form-group">
                  <label for="input_dob" class="col-sm-3 control-label">Security Fee<em class="text-danger">*</em></label>

                  <div class="col-sm-6">
                    {!! Form::text('security_fee', '', ['class' => 'form-control', 'id' => 'input_security_fee', 'placeholder' => 'Security fee']) !!}
                    <span class="text-danger">{{ $errors->first('security_fee') }}</span>
                  </div>
                </div>
				<div class="form-group">
                  <label for="input_dob" class="col-sm-3 control-label">Source<em class="text-danger">*</em></label>

                  <div class="col-sm-6">
                    {!! Form::text('source', '', ['class' => 'form-control', 'id' => 'input_source', 'placeholder' => 'Source']) !!}
                    <span class="text-danger">{{ $errors->first('source') }}</span>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input_status" class="col-sm-3 control-label">Status<em class="text-danger">*</em></label>

                  <div class="col-sm-6">
                    {!! Form::select('status', array('Listed' => 'Listed'), '', ['class' => 'form-control', 'id' => 'input_status', 'placeholder' => 'Select']) !!}
                    <span class="text-danger">{{ $errors->first('status') }}</span>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
               <button type="submit" class="btn btn-info pull-right" name="submit" value="submit">Submit</button>
                 <button type="submit" class="btn btn-default pull-left" name="cancel" value="cancel">Cancel</button>
              </div>
              <!-- /.box-footer -->
            {!! Form::close() !!}
          </div>
          <!-- /.box -->
        </div>
        <!--/.col (right) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@push('scripts')
<script>
  $('#input_checkin').datepicker({ 'format': 'dd-mm-yyyy'});
  $('#input_checkout').datepicker({ 'format': 'dd-mm-yyyy'});
</script>
@stop
@stop