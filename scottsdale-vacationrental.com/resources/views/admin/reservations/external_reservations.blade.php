@extends('admin.template')

@section('main')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Reservations
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Reservations</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Reservation Management</h3>
             <div style="float:right;"><a class="btn btn-success" href="{{ url('admin/add_reservation') }}">Add Reservation</a></div> 
            </div>
            <!-- /.box-header -->
            <div class="box-body">
<table class="table table-bordered table-striped" <?php if(count($reservations) > 0){?>id="example" <?php }?>>
                <thead>

                <tr>
                  <th>Id</th>
				  <th>Host Name</th>
                  <th>Guest Name</th>
				  <th>Confirmation Code</th>
                  <th>Room Name</th>
				  <th>Total Amount</th>
				  <th>Source</th>
                  <th>Status</th>
				  <th>Created At</th>
                  <th>Updated At</th>
                  <th>Action</th>

                </tr>
                </thead>
                <tbody  id="table-div">
              
                @forelse($reservations as $key => $b)
                  <tr>

                    <td>{{  $b->id }}</td>
					<td>{{ $b->host_name }}</td>
                    <td>{{ $b->guest_name }}</td>
					<td>{{ $b->confirmation_code }}</td>
                    <td>{{ $b->room_name }}</td>
					
					<td>{{ $b->total_amount }}</td>
					<td>{{ $b->source }}</td>
                    <td>{{ $b->status }}</td>
					<td>{{ $b->created_at }}</td>
                    <td>{{ $b->updated_at }}</td>
                    <td><a href="{{ url('admin/reservation/detail/'.$b->id) }}" class="btn btn-xs btn-primary" ><i class="fa fa-share"></i></a>
                      <a href="{{ url('admin/reservation/conversation/'.$b->id) }}" class="btn btn-xs btn-primary" ><i class="glyphicon glyphicon-envelope"></i></a></td>



                  </tr>

                @empty
                  <tr>
                    <th colspan="10" class="text-danger text-center text-uppercase">No found</th>
                  </tr>
                
                @endforelse
                </tbody>
              </table>
</div>
</div>
</div>
</div>
</section>
</div>
@endsection

@push('scripts')
<link rel="stylesheet" href="{{ url('css/buttons.dataTables.css') }}">
<script src="{{ url('js/dataTables.buttons.js') }}"></script>
<script src="{{ url('js/buttons.server-side.js') }}"></script>
$(document).ready(function() {
    $('#example').DataTable();
} );
@stop
