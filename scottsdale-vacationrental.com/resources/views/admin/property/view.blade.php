@extends('admin.template')

@section('main')
 <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Property
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Property</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">

          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Property Management</h3>
              <div style="float:right;"><a class="btn btn-success" href="{{ url('admin/add_property_room') }}">Add Property Room</a></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">

              <table
                      class="table table-bordered table-striped">
                <thead>

                <tr>
                  <th>Sr No.</th>
                  <th>Room Id</th>
                  <th>Action</th>

                </tr>
                </thead>
                <tbody  id="table-div">

                @forelse($rooms as $b)
                  <tr>

                    <td>{{ $b->id }}</td>
                    <td>{{ $b->room_id }}</td>

                    <td><a href="{{ url('admin/edit_property_room/'.$b->id) }}" class="btn btn-success btn-xs" ><span class="glyphicon glyphicon-pencil"></span>  Edit</a>
                      <a href="{{ url('admin/delete_property/'.$b->id) }}" class="btn btn-danger btn-xs" ><span class="glyphicon glyphicon-trash"></span>  Delete</a></td>



                  </tr>

                @empty
                  <tr>
                    <th colspan="4" class="text-danger text-center text-uppercase">No found</th>
                  </tr>
                @endforelse
                </tbody>
              </table>
</div>
</div>
</div>
</div>
</section>
</div>
@endsection

@push('scripts')
<link rel="stylesheet" href="{{ url('css/buttons.dataTables.css') }}">
<script src="{{ url('js/dataTables.buttons.js') }}"></script>
<script src="{{ url('js/buttons.server-side.js') }}"></script>

@stop
