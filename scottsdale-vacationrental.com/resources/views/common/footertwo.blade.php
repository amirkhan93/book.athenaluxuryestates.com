<footer id="main-footer">
<div class="container">
<div class="row row-wrap">
<div class="col-md-4">
<a class="logo" href="https://scottsdale-vacationrental.com/">
<!-- <img src="{{url()}}/resources/assets/newHome/img/logo1.png" alt="Logo" class="img-responsive" /> -->
Scottsdale Vacation Rental
</a>


</div>
<div class="col-md-4">
<h4>Follow Us</h4>
<ul class="list list-footer q-links">
<li><a href="#"><i class="fa fa-facebook"></i> facebook</a></li>
<li><a href="#"><i class="fa fa-twitter"></i> twitter</a></li>
<li><a href="#"><i class="fa fa-instagram"></i> instagram</a></li>


</ul>
</div>
<div class="col-md-4">
<h4>Quick Links</h4>
<ul class="list list-footer q-links">
<li><a href="https://scottsdale-vacationrental.com/top_things_in_scottsdale"><i class="fa fa-home"></i> Things To Do</a></li>
<li><a href="https://scottsdale-vacationrental.com/about"><i class="fa fa-user"></i> About Us</a></li>
<li><a href="https://scottsdale-vacationrental.com/contact-us"><i class="fa fa-link"></i> Contact Us</a></li>

</ul>
</div>
</div>
</div>
<div class="copyRight">
<div class="container">
<div class="row">
<div class="col-md-6 col-sm-6 col-xs-12">
<div class="footer_copyright">
<p>&copy; 2019 scottsdale Vacation Rental.com | All rights reserved</p>
</div>
</div>
<!--<div class="col-md-6 col-sm-6 col-xs-12">-->
<!--<div class="designBy">Powered By  :  <a target="_blank" href="https://floridahomestays.com/">FloridaHomestays </a></div> -->
<!--</div>-->
</div>
</div>
</div>
</footer>