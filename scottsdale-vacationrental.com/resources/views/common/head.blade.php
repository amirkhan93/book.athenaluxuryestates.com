<!DOCTYPE html>

<html  dir="{{ (((Session::get('language')) ? Session::get('language') : $default_language[0]->value) == 'ar') ? 'rtl' : '' }}" lang="{{ (Session::get('language')) ? Session::get('language') : $default_language[0]->value }}"  xmlns:fb="http://ogp.me/ns/fb#"><!--<![endif]--><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
<meta name="msvalidate.01" content="FC0077361CD9D4142A2FD510AAB04251" />    
<meta name="google-site-verification" content="ydywPQz_aGrYS0BUse3nFT4akCnRGvXSUwf0_nUJ_rc" />    
<meta name="msvalidate.01" content="FC0077361CD9D4142A2FD510AAB04251" />

<meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height" >
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0' >
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
<meta name = "viewport" content = "user-scalable=no, width=device-width">

      <link rel="dns-prefetch" href="https://maps.googleapis.com/">
      <link rel="dns-prefetch" href="https://maps.gstatic.com/">
      <link rel="dns-prefetch" href="https://mts0.googleapis.com/">
      <link rel="dns-prefetch" href="https://mts1.googleapis.com/">
      <link rel="shortcut icon" href="{{ $favicon }}">
      
      <link rel="icon" href="img/fav.png" sizes="16x16">
<!-- GOOGLE FONTS -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600" rel="stylesheet" type="text/css">
<!-- /GOOGLE FONTS -->
<link rel="stylesheet" href="{{url()}}/resources/assets/newHome/css/font-awesome.min.css">
<link rel="stylesheet" href="{{url()}}/resources/assets/newHome/css/bootstrap.css">
<link rel="stylesheet" href="{{url()}}/resources/assets/newHome/css/styles.css">
<script src="{{url()}}/resources/assets/newHome/js/modernizr.js"></script>
<script src="{{url()}}/resources/assets/newHome/js/jquery.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Pacifico&display=swap" rel="stylesheet">
      
    <!--[if IE]><![endif]-->
    

    <!--[if IE 8]>
      {!! Html::style('css/common_ie8.css?v='.$version) !!}
    <![endif]-->
    <!--[if !(IE 8)]><!-->
     
	
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,300,600' rel='stylesheet' type='text/css'>
	
	<!-- fonts -->
		<link href='http://fonts.googleapis.com/css?family=Playfair+Display:400,700,900' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Karla:700,400' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Lora:400,700' rel='stylesheet' type='text/css'>

        <link
		href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i"
		rel="stylesheet" />
	<link
		href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese"
		rel="stylesheet">
		<link rel="stylesheet" href="https://cdn.materialdesignicons.com/3.8.95/css/materialdesignicons.min.css">

        <!-- fontawesome -->
		<link rel="stylesheet" href="{{url()}}/resources/assets/css/font-awesome.css" />
   
        <!-- bootstrap -->
		 @if (Route::current()->uri() == '/')
		    <link rel="stylesheet" href="{{url()}}/resources/assets/css/bootstrap.min.css" />
        <link rel="stylesheet" href="{{url()}}/resources/assets/css/animate.css" />
        <link rel="stylesheet" href="{{url()}}/resources/assets/css/datepicker.css" />
        <link rel="stylesheet" href="{{url()}}/resources/assets/css/owl.carousel.css">
        <link rel="stylesheet" href="{{url()}}/resources/assets/css/rev-slider/settings.css" />
        <link rel="stylesheet" href="{{url()}}/resources/assets/css/lightslider.css">
        <link rel="stylesheet" href="{{url()}}/resources/assets/css/reset.css">
        <!-- Add the slick-theme.css if you want default styling -->
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick.css"/>
        <!-- Add the slick-theme.css if you want default styling -->
        <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.0/slick/slick-theme.css"/>
        
        <!-- Bootstrape 4-->
      
        <!-- responsive -->
		    <link rel="stylesheet" href="{{url()}}/resources/assets/css/responsive.css" />
        <!-- custom css -->
		    
		    <link rel="stylesheet" href="{{url()}}/css/header.css" />
		@else
		  {!! Html::style('css/common.css?v='.$version) !!}
      {!! Html::style('css/styles.css?v='.$version) !!}
      {!! Html::style('css/nouislider.min.css?v='.$version) !!}
      {!! Html::style('css/home.css?v='.$version) !!}
      {!! Html::style('css/themes.css?v='.$version) !!}
      {!! Html::style('pcss?css=css/dynamic.css') !!}
      {!! Html::style('css/jquery.selectBoxIt.css?v='.$version) !!}
      {!! Html::style('css/daterangepicker.css?v='.$version) !!}  
      {!! Html::style('css/header_two.css?v='.$version) !!} 
      {!! Html::style('css/host_dashboard.css?v='.$version) !!} 
    <!--<![endif]-->

    <!--[if lt IE 9]>
      {!! Html::style('css/airglyphs-ie8.css?v='.$version) !!}
    <![endif]-->

    @if (isset($exception))
      @if ($exception->getStatusCode()  == '404')
        {!! Html::style('css/error_pages_pretzel.css?v='.$version) !!}
      @endif
    @endif

    @if (!isset($exception))

    @if (Route::current()->uri() == 'signup_action')
    	{!! Html::style('css/signinup.css?v='.$version) !!}
      
    @endif

    @if(@$default_home == 'two' )  
       {!! Html::style('css/home_two.css?v='.$version) !!}          
    @endif
    
    @if (Route::current()->uri() == '/')
      {!! Html::style('css/main.css?v='.$version) !!}
     @if(@$default_home == 'two')
      {!! Html::style('css/common_two.css?v='.$version) !!}
      {!! Html::style('css/header_two.css?v='.$version) !!}
      {!! Html::style('css/daterangepicker.css?v='.$version) !!}      
      {!! Html::style('css/jquery.bxslider.css?v='.$version) !!}
       @endif
      
    @endif    


    @if (Route::current()->uri() == 'dashboard')   
         
      {!! Html::style('css/host_dashboard.css?v='.$version) !!}
      {!! Html::style('css/dashboard.css?v='.$version) !!}
    @endif

    @if (Route::current()->uri() == 'trips/current')   
         
    @endif
    @if (Route::current()->uri() == 'trips/previous')   
         
    @endif
       @if (Route::current()->uri() == 'users/transaction_history')   
    
    @endif
     @if (Route::current()->uri() == 'users/security')   
    
    @endif
    @if (Route::current()->uri() == 'rooms/new')
    
      {!! Html::style('css/new.css?v='.$version) !!}
    @endif

    @if (Route::current()->uri() == 'inbox')
    
      {!! Html::style('css/threads.css?v='.$version) !!}
    @endif
      @if (Route::current()->uri() == 'home_two')
                 {!! Html::style('css/common_two.css?v='.$version) !!}
             {!! Html::style('css/home_two.css?v='.$version) !!}
             
             {!! Html::style('css/daterangepicker.css?v='.$version) !!}

             {!! Html::style('css/jquery.bxslider.css?v='.$version) !!}
    @endif

    @if (Route::current()->uri() == 'reservation/change')
    
      {!! Html::style('css/alterations.css?v='.$version) !!}
      {!! Html::style('css/policy_timeline_v2.css?v='.$version) !!}
    @endif

     @if (Route::current()->uri() == 'alterations/{code}')
     
      {!! Html::style('css/alterations.css?v='.$version) !!}
    @endif


    @if (Route::current()->uri() == 'z/q/{id}')
    
      {!! Html::style('css/messaging.css?v='.$version) !!}
      {!! Html::style('css/tooltip.css?v='.$version) !!}
    @endif

    @if (Route::current()->uri() == 'messaging/qt_with/{id}')
      {!! Html::style('css/messaging.css?v='.$version) !!}
      
      {!! Html::style('css/tooltip.css?v='.$version) !!}
      {!! Html::style('css/responsive_calendar.css?v='.$version) !!}
    @endif

    @if (Route::current()->uri() == 'manage-listing/{id}/{page}')
    
      {!! Html::style('css/manage_listing.css?v='.$version) !!}
    @endif

    @if (Route::current()->uri() == 'wishlists/picks' || Route::current()->uri() == 'wishlists/my' || Route::current()->uri() == 'wishlists/popular' || Route::current()->uri() == 'wishlists/{id}' || Route::current()->uri() == 'users/{id}/wishlists')
      {!! Html::style('css/wishlists.css?v='.$version) !!}
      
    @endif

    @if (Route::current()->uri() == 'rooms/{id}')
      {!! Html::style('css/rooms_detail.css?v='.$version) !!}
      {!! Html::style('css/slider/nivo-lightbox.css?v='.$version) !!}
      {!! Html::style('css/slider/default.css?v='.$version) !!}
      {!! Html::style('css/jquery.bxslider.css?v='.$version) !!}
      {!! Html::style('css/p3.css?v='.$version) !!}
      <link rel="stylesheet" href="{{url()}}/resources/assets/home/css/style.css" />
	<link rel="stylesheet" href="{{url()}}/resources/assets/home/css/colors/color.css" />
    
    @endif
    
    @if (Route::current()->uri() == 'rooms')
      {!! Html::style('css/index.css?v='.$version) !!}
      {!! Html::style('css/unlist_modal.css?v='.$version) !!}
      {!! Html::style('css/dashboard.css?v='.$version) !!}
      
    @endif

    @if (Route::current()->uri() == 'reservation/itinerary')
    
    @endif

    @if (Route::current()->uri() == 'reservation/receipt')
      {!! Html::style('css/receipt.css?v='.$version) !!}
      
      {!! Html::style('css/receipt-print.css?v='.$version,['media'=>'print']) !!}
    @endif

    @if (Route::current()->uri() == 's' || Route::current()->uri() == 'wishlists/popular')
      {!! Html::style('css/map_search.css?v='.$version) !!}
      
    @endif

    @if (Route::current()->uri() == 'payments/book/{id?}' || Route::current()->uri() == 'api_payments/book/{id?}')
      {!! Html::style('css/payments.css?v='.$version) !!}
      
      {!! Html::style('css/p4.css?v='.$version) !!}
      {!! Html::style('css/StyleSheet.css?v='.$version) !!}
    @endif

    @if (Route::current()->uri() == 'reservation/requested')
      {!! Html::style('css/page5.css?v='.$version) !!}
      
    @endif
    
    @if (Route::current()->uri() == 'users/edit')
      {!! Html::style('css/address_widget.css?v='.$version) !!}
      
      {!! Html::style('css/phonenumbers.css?v='.$version) !!}
      {!! Html::style('css/edit_profile.css?v='.$version) !!}
    @endif

    @if (Route::current()->uri() == 'users/show/{id}')
      {!! Html::style('css/profile.css?v='.$version) !!}
      
    @endif

    @if (Route::current()->uri() == 'users/payout_preferences/{id}')
      {!! Html::style('css/payout_preferences.css?v='.$version) !!}
      
    @endif

    @if (Route::current()->uri() == 'home/cancellation_policies')
      {!! Html::style('css/policy_timeline_v2.css?v='.$version) !!}
      
    @endif

    @if (Route::current()->uri() == 'reviews/edit/{id}')
      {!! Html::style('css/reviews.css?v='.$version) !!}
      
    @endif

    @if (Route::current()->uri() == 'invite' || Route::current()->uri() == 'c/{username}')
      {!! Html::style('css/referrals.css?v='.$version) !!}
      
    @endif

    @if (Route::current()->uri() == 'help' || Route::current()->uri() == 'help/topic/{id}/{category}' || Route::current()->uri() == 'help/article/{id}/{question}')
      {!! Html::style('css/help.css?v='.$version) !!}
      {!! Html::style('css/jquery-ui.css?v='.$version) !!}
      
    @endif

    @endif
		<link rel="stylesheet" href="{{url()}}/resources/assets/css/bootstrap.min.css" />
		<link rel="stylesheet" href="{{url()}}/resources/assets/css/reset.css">
		
		<link rel="stylesheet" href="{{url()}}/css/header.css" />
			
	{!! Html::style('css/bootstrap.css?v='.$version) !!}
	{!! Html::style('css/font-awesome.css?v='.$version) !!}
	{!! Html::style('css/icomoon.css?v='.$version) !!}
	
	<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" type="text/css">
		@endif

    <style type="text/css">
      .ui-selecting { background: #FECA40; }
      .ui-selected { background: #F39814; color: white; }
    </style>
         
   {{--<title>{{ $title or Helpers::meta((!isset($exception)) ? Route::current()->uri() : '', 'title') }} {{ $additional_title or '' }}</title>--}}
    <title>The Guilt Trip & Golfers Paradise</title>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

     
    <meta name="description" content="{{ Helpers::meta((!isset($exception)) ? Route::current()->uri() : '', 'description') }}">
    <meta name="keywords" content="{{ Helpers::meta((!isset($exception)) ? Route::current()->uri() : '', 'keywords') }}">
    

    <meta name="twitter:widgets:csp" content="on">

 
  @if (!isset($exception))
    @if (Route::current()->uri() == 'rooms/{id}')
    <meta property="og:image" content="{{ url('images/'.$result->photo_name) }}">
    <meta itemprop="image" src="{{ url('images/'.$result->photo_name) }}">
    <link rel="image_src" href="#" src="{{ url('images/'.$result->photo_name) }}">
    @endif

    @if (Route::current()->uri() == 'wishlists/{id}')
    <meta property="og:image" content="{{ url('images/'.@$row->rooms->photo_name) }}">
    <meta itemprop="image" src="{{ url('images/'.@$row->rooms->photo_name) }}">
    <link rel="image_src" href="#" src="{{ url('images/'.@$row->rooms->photo_name) }}">
    @endif

   @endif
    <link rel="search" type="application/opensearchdescription+xml" href="#" title="">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="theme-color" content="#f5f5f5">
   
    <link rel="stylesheet" href="{{url()}}/resources/assets/css/custom.css" />
  </head>
  <body class="{{ (!isset($exception)) ? (Route::current()->uri() == '/' ? 'home' : '') : '' }}" ng-app="App">
  <!--<div class="global-wrap">-->