 <?php 
use App\Http\Start\Helpers;
$detail = Helpers::getSiteDetail();
?>
<div id="footer" class="pu ll-left container-brand-dark footer-surround footer-container">
<footer id="main-footer" class="page-container-responsive ng-scope" ng-controller="footer">
            <div class="container">

                <div class="row row-wrap">

                    <div class="col-md-3">

                        <a class="footer_logo" href="https{{url()}}">

                           <!-- <img src="https://newsmyrnabeachvacationhomes.com/images/logos/logo.png" alt="" title="Trips">-->
                           <img src="{{url()}}/images/logos/{{$detail[2]->value}}" alt="Trips"></a>

                        </a>

                        <!--<p class="mb20">Exclusive House in Scottsdale, Arizona, USA</p>-->

                        <ul class="list list-horizontal list-space">

                            <li>

                                <a class="fa fa-facebook box-icon-normal round animate-icon-bottom-to-top" href="#" target="blank"></a>

                            </li>

                            <li>

                                <a class="fa fa-twitter box-icon-normal round animate-icon-bottom-to-top" href="#" target="blank"></a>

                            </li>

                            <!--<li>-->

                            <!--    <a class="fa fa-google-plus box-icon-normal round animate-icon-bottom-to-top" href="#" target="blank"></a>-->

                            <!--</li>-->

                            <!--<li>-->

                            <!--    <a class="fa fa-youtube box-icon-normal round animate-icon-bottom-to-top" href="#" target="blank"></a>-->

                            <!--</li>-->

                            <!--<li>-->

                            <!--    <a class="fa fa-pinterest box-icon-normal round animate-icon-bottom-to-top" href="#" target="blank"></a>-->

                            <!--</li>-->

                        </ul>

                    </div>



                     <div class="col-md-3">

                    </div>

                    <div class="col-md-2">

                    </div>

                    <div class="col-md-4">

                        <h4 class="question">Have Questions?</h4>

                        <h5 class="text-color">LOCAL : <a href="tel:+1 833-589-7829"> +1 833-589-7829</a></h5>

                        <h5 class="text-color">Support : <a href="mailto:jessicaw@luxhomepro.com">jessicaw@luxhomepro.com</a></h5>

      <!--                  <h5 class="text-color">Bookings : <a href="mailto:INFO@luxvacationrentalhomes.com">INFO@luxvacationrentalhomes.com</a></h5>-->
						</div>



                </div>

            </div>
    <!--<p class="copyright">© 2018 VRSmart360.com. All rights reserved</p>-->
        </footer>
		</div>
		