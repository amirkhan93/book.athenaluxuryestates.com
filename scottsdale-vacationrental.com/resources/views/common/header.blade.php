<?php 
use App\Http\Start\Helpers;
$detail = Helpers::getSiteDetail();
?>
<style>
    .dropdown-submenu {
    position: relative;
    
}
.dropdown>.dropdown-menu{
        padding-top: 37px;
}
.dropdown>.dropdown-menu,.dropdown-submenu>.dropdown-menu{
    background: #070a2b;
}
.dropdown>.dropdown-menu>li>a,.dropdown-submenu>.dropdown-menu li a{
color:#fff;
}
.dropdown-submenu>.dropdown-menu {
    top: 0;
    left: 100%;
    margin-top: -6px;
    margin-left: -1px;
    -webkit-border-radius: 0 6px 6px 6px;
    -moz-border-radius: 0 6px 6px;
    border-radius: 0 6px 6px 6px;
}

.dropdown:hover>.dropdown-menu {
    display: block;
}

.dropdown-submenu:hover>.dropdown-menu {
    display: block;
}

.dropdown-submenu>a:after {
    display: block;
    content: " ";
    float: right;
    width: 0;
    height: 0;
    border-color: transparent;
    border-style: solid;
    border-width: 5px 0 5px 5px;
    border-left-color: #ccc;
    margin-top: 5px;
    margin-right: -10px;
}

.dropdown-submenu:hover>a:after {
    border-left-color: #fff;
}
</style>
<div id="loader-wrapper">
           <div class="logo"><img src="{{url()}}/images/loader_lux.gif" alt="Trips"></div>
            <!--<div id="loader">
                
            </div>-->
        </div>
		  <!-- start header -->
        <header class="header_area">

            <!-- start main header -->
            <div class="main_header_area">
                <div class="container">
                    <!-- start mainmenu & logo -->
                    <div class="mainmenu">
                        <div id="nav">
                            <nav class="navbar navbar-default">
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <div class="navbar-header">
                                  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                  </button>
                                  <div class="site_logo fix">
                                      <a id="brand" class="clearfix navbar-brand border-right-whitesmoke" href="{{url()}}">
									  <img src="{{url()}}/images/logos/logo.png" alt="Trips"></a>
                                      <div class="header_login floatleft">
                                          <ul>
                                              @if(!Auth::user()->check())
                                              <li id="login" class="top-user-area-avatar"> <a  href="{{ url('login') }}" >Login</a> </li>
                                              @else
                                              <li id="login" class="top-user-area-avatar"> <a  href="{{ url('dashboard') }}" >My Account</a> </li>
                                              @endif
                                              <!--<li><a href="#">Register</a></li>-->
                                          </ul>
                                      </div>
                                  </div>
                                </div>

                                <!-- Collect the nav links, forms, and other content for toggling -->
                                <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
                                  <ul class="nav navbar-nav">
                                    <li><a href="{{ url() }}">Home</a></li>
                                        <ul id="menu1" class="dropdown-menu" role="menu">
                                          <!--<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Home Page two</a></li>-->
                                        </ul>
                                    </li>        
                                    <ul class="nav navbar-nav">
                                    <li role="presentation" class="dropdown">
                                        <a id="drop-one" href="{{ url() }}" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
                                          Properties
                                        </a>
                                    
                                    <ul class="dropdown-menu multi-level">
                            <!--<li><a href="#">Level 1</a></li>                                                       -->
                            <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Arizona</a>
                                <!-- Level 2 -->
                                <ul class="dropdown-menu">
                                    <!--<li><a href="#">hjhj</a></li>                                    -->
                                    <li class="dropdown-submenu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Phoenix</a>
                                        <!-- Level 3 -->
                                        <ul class="dropdown-menu">
                                            <li><a href="{{ url() }}/rooms/6">The Sanctuary</a></li>
                                            <li><a href="{{ url() }}/rooms/7">Cactus Acres Home</a></li>
                                             <li><a href="{{ url() }}/rooms/11">Cactus Home</a></li>
                                              <li><a href="{{ url() }}/rooms/13">City Lights Home</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown-submenu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Scottsdale</a>
                                        <!-- Level 3 -->
                                        <ul class="dropdown-menu">
                                            <li><a href="{{ url() }}/rooms/4">Luxury Home </a></li>
                                            <li><a href="{{ url() }}/rooms/8">SCOTTSDALE </a></li>
                                             <li><a href="{{ url() }}/rooms/9">Casa de Encanto</a></li>
                                              <li><a href="{{ url() }}/rooms/10">Tatum Retreat Home</a></li>
                                              <li><a href="{{ url() }}/rooms/12">Hayden Manor Home</a></li>
                                              <li><a href="{{ url() }}/rooms/14">Presidio-Kierland</a></li>
                                              <li><a href="{{ url() }}/rooms/15">Delcoa Home</a></li>
                                              <li><a href="{{ url() }}/rooms/26">Artists Hacienda</a></li>
                                              <li><a href="{{ url() }}/rooms/27">Sunny 81F Golf</a></li>
                                              <li><a href="{{ url() }}/rooms/28">Luxury Getaway</a></li>
                                              <li><a href="{{ url() }}/rooms/31">Dynamite Majestic</a></li>
                                              <li><a href="{{ url() }}/rooms/32">EL PALACIO</a></li>
                                              <li><a href="{{ url() }}/rooms/33">Mini Resort with pool</a></li>
                                              <li><a href="{{ url() }}/rooms/33">Scottsdale Condo</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown-submenu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Cave Creek</a>
                                        <!-- Level 3 -->
                                        <ul class="dropdown-menu">
                                            <li><a href="{{ url() }}/rooms/29">Epic $2 Million Home</a></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown-submenu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Paradise Valley</a>
                                        <!-- Level 3 -->
                                        <ul class="dropdown-menu">
                                            <li><a href="{{ url() }}/rooms/30">Remodeled Luxury</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        
                        
                            <!--<li><a href="#">Level 1</a></li>                                                       -->
                            <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">California</a>
                                <!-- Level 2 -->
                                <ul class="dropdown-menu">
                                    <!--<li><a href="#">hjhj</a></li>                                    -->
                                    <li class="dropdown-submenu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Tahoe Vista</a>
                                        <!-- Level 3 -->
                                        <ul class="dropdown-menu">
                                            <li><a href="{{ url() }}/rooms/3">Rustic 3 BR</a></li>
                                            <li><a href="{{ url() }}/rooms/17">Kings Palace</a></li>
                                            <!-- <li><a href="{{ url() }}/rooms/11">Cactus Home</a></li>-->
                                            <!--  <li><a href="{{ url() }}/rooms/13">City Lights Home</a></li>-->
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown-submenu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Texas</a>
                                <!-- Level 2 -->
                                <ul class="dropdown-menu">
                                    <!--<li><a href="#">hjhj</a></li>                                    -->
                                    <li class="dropdown-submenu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Baytown</a>
                                        <!-- Level 3 -->
                                        <ul class="dropdown-menu">
                                            <li><a href="{{ url() }}/rooms/2">Old and Lost</a></li>
                                            <!--<li><a href="{{ url() }}/rooms/7">Cactus Acres Home</a></li>-->
                                            <!-- <li><a href="{{ url() }}/rooms/11">Cactus Home</a></li>-->
                                            <!--  <li><a href="{{ url() }}/rooms/13">City Lights Home</a></li>-->
                                        </ul>
                                    </li>
                                    <li class="dropdown-submenu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Bolivar Peninsula</a>
                                        <!-- Level 3 -->
                                        <ul class="dropdown-menu">
                                            <li><a href="{{ url() }}/rooms/5">Sea Life's</a></li>
                                            <!--<li><a href="{{ url() }}/rooms/7">Cactus Acres Home</a></li>-->
                                            <!-- <li><a href="{{ url() }}/rooms/11">Cactus Home</a></li>-->
                                            <!--  <li><a href="{{ url() }}/rooms/13">City Lights Home</a></li>-->
                                        </ul>
                                    </li>
                                    <li class="dropdown-submenu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">La Porte</a>
                                        <!-- Level 3 -->
                                        <ul class="dropdown-menu">
                                            <li><a href="{{ url() }}/rooms/1">Caddo Ct Home</a></li>
                                            <li><a href="{{ url() }}/rooms/18">Getaway Near Sylvan</a></li>
                                             <li><a href="{{ url() }}/rooms/25">Quaint 3 Bedroom</a></li>
                                            <!--  <li><a href="{{ url() }}/rooms/13">City Lights Home</a></li>-->
                                        </ul>
                                        
                                     </li>
                                    <li class="dropdown-submenu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Houston</a>
                                        <!-- Level 3 -->
                                        <ul class="dropdown-menu">
                                            <li><a href="{{ url() }}/rooms/19">Heights 4 Story</a></li>
                                            <li><a href="{{ url() }}/rooms/21">4 Story Townhome</a></li>
                                             <li><a href="{{ url() }}/rooms/23">Houston's Modern </a></li>
                                              <li><a href="{{ url() }}/rooms/24">Midtown Townhome</a></li>
                                        </ul>
                                        
                                    </li>
                                    <li class="dropdown-submenu">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Kemah</a>
                                        <!-- Level 3 -->
                                        <ul class="dropdown-menu">
                                            <li><a href="{{ url() }}/rooms/20">Kemah Kabana</a></li>
                                            <li><a href="{{ url() }}/rooms/22">Cozy 2 Room</a></li>
                                            <!-- <li><a href="{{ url() }}/rooms/11">Cactus Home</a></li>-->
                                            <!--  <li><a href="{{ url() }}/rooms/13">City Lights Home</a></li>-->
                                        </ul>
                                        
                                    </li>
                                </ul>
                            </li>
                        </ul
                                    </li>
                               
                                    
                                    
                                    <li><a href="{{ url() }}/about_us">About Us</a></li>
                                    <!--<li><a href="{{ url() }}/blog">Blog</a></li>-->
                                    <li><a href="{{ url() }}/contact">Contact Us</a></li>
                                  </ul>
                                  <div class="emergency_number">
                                      <a href="tel:1234567890"><img src="{{url()}}/img/call-icon.png" alt="">{{$detail[19]->value}}</a>
                                  </div>
                                </div><!-- /.navbar-collapse -->
                            </nav>
                        </div>
                    </div>
                    <!-- end mainmenu and logo -->
                </div>
            </div>
            <!-- end main header -->

        </header>
        <!-- end header -->