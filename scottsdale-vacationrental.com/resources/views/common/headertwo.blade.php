<header id="main-header"><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <div class="header-top">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
         
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="top-user-area clearfix">
            <ul class="top-user-area-list list list-horizontal list-border">
              <li class="top-user-area-avatar">
                <a href="#."><i class="fa fa-volume-control-phone" aria-hidden="true"></i> +1 (877) 589 6274</a></li>
                <li><a href="mailto:presidiostay@gmail.com"><i class="fa fa-envelope-o" aria-hidden="true"></i>presidiostay@gmail.com</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="nav nav_two">
      <a class="logo" href="https://scottsdale-vacationrental.com/">
      Scottsdale Vacation Rental
            <!-- <img src="{{url()}}/resources/assets/newHome/img/logo1.png" alt="Sunshine Properties Unlimited" title="Image Title" /> -->
          </a>
        <ul class="slimmenu" id="slimmenu">
          <li><a href="{{url()}}">Home</a></li>
              <li><a href="{{url()}}/rooms/10081">Golfer's Paradise</a></li>
          
          <li><a href="javascript:void(0)">Things to Do</a>
            <ul>
              <li><a href="https://scottsdale-vacationrental.com/top_things_in_scottsdale">Things to do in Scottsdale</a></li>
              <li><a href="https://scottsdale-vacationrental.com/days_excursion">A Day’s Excursion</a></li>
            </ul>
          </li>
          <li><a href="#">About Us</a></li>
          
          <li><a href="https://scottsdale-vacationrental.com/contact-us">Contact Us</a></li>
          @if((Auth::user()->user()))
            <li><a class="btn btn-primary login_btn" href="https://scottsdale-vacationrental.com/dashboard">My Account</a></li>
          @else
            <li><a class="btn btn-primary login_btn" href="https://scottsdale-vacationrental.com/login">Login</a></li>
          @endif
        </ul>
      </div>
    </div>
  </header>
