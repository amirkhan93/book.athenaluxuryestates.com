<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/','Auth\LoginController@ShowLoginForm');
Route::get('/', function()
{
    return Redirect::to('login');
});

Auth::routes();
/*superadmin*/
Route::get('logout', 'admin\HomeController@logout');
/*superadmin*/
Route::get('signup', 'admin\VendorSignupController@index');
Route::get('member/dashboard', 'admin\VendorSignupController@dashboard');
Route::get('member', 'admin\VendorSignupController@loginform');
Route::post('member/loginstore', 'admin\VendorSignupController@login');
Route::post('vendorregister', 'admin\VendorSignupController@registervendor');
Route::get('member/logout', 'admin\VendorSignupController@logout');

Route::get('outlet', 'ifitmash\OutletController@index');
Route::post('outlet/loginstore', 'ifitmash\OutletController@login');
Route::get('outlet/dashboard', 'ifitmash\OutletController@dashboard');
Route::get('outlet/change_id', 'ifitmash\OutletController@change_id');
Route::get('outlet/logout', 'ifitmash\OutletController@logout');

Route::get('home', 'admin\HomeController@index')->name('home');
Route::group(['namespace' => 'admin','middleware' => ['locale'],'prefix'=>'admin'], function () {
    Route::get('home', 'HomeController@index')->name('home');
    Route::get('businesstype', 'BusinesstypeController@index');
    Route::get('businesstype/create', 'BusinesstypeController@create');
    Route::post('businesstype/store', 'BusinesstypeController@store');
    Route::get('businesstype/edit/{id}', 'BusinesstypeController@edit');
    Route::post('businesstype/edits', 'BusinesstypeController@edits');
    Route::post('businesstype/update/{id}', 'BusinesstypeController@update');
    Route::get('businesstype/delete/{id}', 'BusinesstypeController@destroy');
    Route::get('businesstype/active/{id}', 'BusinesstypeController@active');
    Route::get('businesstype/inactive/{id}', 'BusinesstypeController@inactive');

    Route::get('subsettype', 'SubtypeController@index');
    Route::get('subsettype/create', 'SubtypeController@create');
    Route::post('subsettype/store', 'SubtypeController@store');
    Route::get('subsettype/edit/{id}', 'SubtypeController@edit');
    Route::post('subsettype/edits', 'SubtypeController@edits');
    Route::post('subsettype/update/{id}', 'SubtypeController@update');
    Route::get('subsettype/delete/{id}', 'SubtypeController@destroy');
    Route::get('subsettype/active/{id}', 'SubtypeController@active');
    Route::get('subsettype/inactive/{id}', 'SubtypeController@inactive');

    Route::get('settype', 'SettypeController@index');
    Route::get('settype/create', 'SettypeController@create');
    Route::post('settype/store', 'SettypeController@store');
    Route::get('settype/edit/{id}', 'SettypeController@edit');
    Route::post('settype/edits', 'SettypeController@edits');
    Route::post('settype/update/{id}', 'SettypeController@update');
    Route::get('settype/delete/{id}', 'SettypeController@destroy');
    Route::get('settype/active/{id}', 'SettypeController@active');
    Route::get('settype/inactive/{id}', 'SettypeController@inactive');

    Route::get('fooddetails', 'FooddetailsController@index');
    Route::get('fooddetails/create', 'FooddetailsController@create');
    Route::post('fooddetails/store', 'FooddetailsController@store');
    Route::get('fooddetails/edit/{id}', 'FooddetailsController@edit');
    Route::post('fooddetails/edits', 'FooddetailsController@edits');
    Route::post('fooddetails/update/{id}', 'FooddetailsController@update');
    Route::get('fooddetails/destroy/{id}', 'FooddetailsController@destroy');
    Route::get('fooddetails/active/{id}', 'FooddetailsController@active');
    Route::get('fooddetails/inactive/{id}', 'FooddetailsController@inactive');
    Route::get('fooddetails/foodEdit/{id}', 'FooddetailsController@editFood');
    Route::post('fooddetails/updatefood', 'FooddetailsController@foodupdate');
    
    Route::get('excercisetype', 'ExerciseController@index');
    Route::get('excercisetype/create', 'ExerciseController@create');
    Route::post('excercisetype/store', 'ExerciseController@store');
    Route::get('excercisetype/edit/{id}', 'ExerciseController@edit');
    Route::post('excercisetype/edits', 'ExerciseController@edits');
    Route::post('excercisetype/update/{id}', 'ExerciseController@update');
    Route::get('excercisetype/delete/{id}', 'ExerciseController@destroy');
    Route::get('excercisetype/active/{id}', 'ExerciseController@active');
    Route::get('excercisetype/inactive/{id}', 'ExerciseController@inactive');

    Route::get('members', 'VendorlistController@index');
    Route::get('members/view/{id}', 'VendorlistController@view');
    Route::get('members/delete/{id}', 'VendorlistController@destroy');
    Route::get('members/active/{id}', 'VendorlistController@active');
    Route::get('members/inactive/{id}', 'VendorlistController@inactive');
    Route::get('members/reject/{id}', 'VendorlistController@reject');
    Route::get('approvedmember', 'VendorlistController@approvedmemberlist');
    Route::get('locality', 'HomeController@Locality');
    Route::post('locality/store', 'HomeController@localityStore');
    Route::get('tax', 'HomeController@Tax');
    Route::post('tax/store', 'HomeController@taxStore');
    Route::get('tax/edit/{id}', 'HomeController@taxedit');
    Route::post('tax/update', 'HomeController@taxUpdate');
    Route::get('tax/delete/{id}', 'HomeController@taxDestroy');
    
    Route::get('status', 'StatusController@index');
    Route::get('status/create', 'StatusController@create');
    Route::post('status/store', 'StatusController@store');
    Route::get('status/edit/{id}', 'StatusController@edit');
    Route::post('status/update/{id}', 'StatusController@update');
    Route::get('status/delete/{id}', 'StatusController@destroy');
    Route::get('status/active/{id}', 'StatusController@active');
    Route::get('status/inactive/{id}', 'StatusController@inactive');
    
    Route::get('source', 'SourceController@index');
    Route::get('source/create', 'SourceController@create');
    Route::post('source/store', 'SourceController@store');
    Route::get('source/edit/{id}', 'SourceController@edit');
    Route::post('source/update/{id}', 'SourceController@update');
    Route::get('source/delete/{id}', 'SourceController@destroy');
    Route::get('source/active/{id}', 'SourceController@active');
    Route::get('source/inactive/{id}', 'SourceController@inactive');
    
    Route::get('bankname', 'BanknameController@index');
    Route::get('bankname/create', 'BanknameController@create');
    Route::post('bankname/store', 'BanknameController@store');
    Route::get('bankname/edit/{id}', 'BanknameController@edit');
    Route::post('bankname/edits', 'BanknameController@edits');
    Route::post('bankname/update/{id}', 'BanknameController@update');
    Route::get('bankname/delete/{id}', 'BanknameController@destroy');
    Route::get('bankname/active/{id}', 'BanknameController@active');
    Route::get('bankname/inactive/{id}', 'BanknameController@inactive');
    
    Route::get('facilities', 'FacilitiesController@index');
    Route::get('facilities/create', 'FacilitiesController@create');
    Route::post('facilities/store', 'FacilitiesController@store');
    Route::get('facilities/edit/{id}', 'FacilitiesController@edit');
    Route::post('facilities/update/{id}', 'FacilitiesController@update');
    Route::get('facilities/delete/{id}', 'FacilitiesController@destroy');
    Route::get('facilities/active/{id}', 'FacilitiesController@active');
    Route::get('facilities/inactive/{id}', 'FacilitiesController@inactive');
    
    
    Route::get('packages', 'PackageController@index');
    Route::get('packages/create', 'PackageController@create');
    Route::post('packages/store', 'PackageController@store');
    Route::get('packages/edit/{id}', 'PackageController@edit');
    Route::post('packages/update/{id}', 'PackageController@update');
    Route::get('packages/delete/{id}', 'PackageController@destroy');
    Route::get('packages/active/{id}', 'PackageController@active');
    Route::get('packages/inactive/{id}', 'PackageController@inactive');
    Route::get('packages/assignPackage/{id}', 'PackageController@assignPackage');
    Route::post('packages/packageAssign', 'PackageController@packageAssign');
    
    Route::get('paymentmode', 'PaymentmodeController@index');
    Route::get('paymentmode/create', 'PaymentmodeController@create');
    Route::post('paymentmode/store', 'PaymentmodeController@store');
    Route::get('paymentmode/edit/{id}', 'PaymentmodeController@edit');
    Route::post('paymentmode/edits', 'PaymentmodeController@edits');
    Route::post('paymentmode/update/{id}', 'PaymentmodeController@update');
    Route::get('paymentmode/delete/{id}', 'PaymentmodeController@destroy');
    Route::get('paymentmode/active/{id}', 'PaymentmodeController@active');
    Route::get('paymentmode/inactive/{id}', 'PaymentmodeController@inactive');
    
     Route::get('feedback', 'FeedbackController@index');
    Route::get('feedback/create', 'FeedbackController@create');
    Route::post('feedback/store', 'FeedbackController@store');
    Route::get('feedback/edit/{id}', 'FeedbackController@edit');
    Route::post('feedback/update/{id}', 'FeedbackController@update');
   
});
Route::group(['namespace' => 'ifitmash','middleware' => ['locale'],'prefix'=>'member'], function () {


    Route::get('mail', 'AddnewoutletController@mail');
    Route::get('addnewoutlet', 'AddnewoutletController@index');
    Route::post('addnewoutlet/states', 'AddnewoutletController@states');
    Route::post('addnewoutlet/cities', 'AddnewoutletController@cities');
    Route::get('addnewoutlet/create', 'AddnewoutletController@create');
    Route::post('addnewoutlet/store', 'AddnewoutletController@store');
    Route::get('addnewoutlet/edit/{id}', 'AddnewoutletController@edit');
    Route::post('addnewoutlet/update/{id}', 'AddnewoutletController@update');
    Route::get('addnewoutlet/destroy/{id}', 'AddnewoutletController@destroy');
    Route::get('addnewoutlet/active/{id}', 'AddnewoutletController@active');
    Route::get('addnewoutlet/inactive/{id}', 'AddnewoutletController@inactive');
    Route::get('addnewoutlet/view/{id}', 'AddnewoutletController@view');

    Route::get('outlet/login', 'OutletController@index');
    Route::post('outlet/loginstore', 'OutletController@login');
    Route::get('outlet/dashboard', 'OutletController@dashboard');
    Route::get('outlet/logout', 'OutletController@logout');


    Route::get('outlet', 'ApprovaloutletController@index');
    Route::get('outlet/active/{id}', 'ApprovaloutletController@active');
    Route::get('outlet/inactive/{id}', 'ApprovaloutletController@inactive');


    Route::get('status', 'StatusController@index');
    Route::get('status/create', 'StatusController@create');
    Route::post('status/store', 'StatusController@store');
    Route::get('status/edit/{id}', 'StatusController@edit');
    Route::post('status/update/{id}', 'StatusController@update');
    Route::get('status/delete/{id}', 'StatusController@destroy');
    Route::get('status/active/{id}', 'StatusController@active');
    Route::get('status/inactive/{id}', 'StatusController@inactive');

    Route::get('source', 'SourceController@index');
    Route::get('source/create', 'SourceController@create');
    Route::post('source/store', 'SourceController@store');
    Route::get('source/edit/{id}', 'SourceController@edit');
    Route::post('source/update/{id}', 'SourceController@update');
    Route::get('source/delete/{id}', 'SourceController@destroy');
    Route::get('source/active/{id}', 'SourceController@active');
    Route::get('source/inactive/{id}', 'SourceController@inactive');


    Route::get('facilities', 'FacilitiesController@index');
    Route::get('facilities/create', 'FacilitiesController@create');
    Route::post('facilities/store', 'FacilitiesController@store');
    Route::get('facilities/edit/{id}', 'FacilitiesController@edit');
    Route::post('facilities/update/{id}', 'FacilitiesController@update');
    Route::get('facilities/delete/{id}', 'FacilitiesController@destroy');
    Route::get('facilities/active/{id}', 'FacilitiesController@active');
    Route::get('facilities/inactive/{id}', 'FacilitiesController@inactive');

    Route::get('billingcycle', 'BillingcycleController@index');
    Route::get('billingcycle/create', 'BillingcycleController@create');
    Route::post('billingcycle/store', 'BillingcycleController@store');
    Route::get('billingcycle/edit/{id}', 'BillingcycleController@edit');
    Route::post('billingcycle/edits', 'BillingcycleController@edits');
    Route::post('billingcycle/update/{id}', 'BillingcycleController@update');
    Route::get('billingcycle/delete/{id}', 'BillingcycleController@destroy');
    Route::get('billingcycle/active/{id}', 'BillingcycleController@active');
    Route::get('billingcycle/inactive/{id}', 'BillingcycleController@inactive');


    Route::get('bankname', 'BanknameController@index');
    Route::get('bankname/create', 'BanknameController@create');
    Route::post('bankname/store', 'BanknameController@store');
    Route::get('bankname/edit/{id}', 'BanknameController@edit');
    Route::post('bankname/edits', 'BanknameController@edits');
    Route::post('bankname/update/{id}', 'BanknameController@update');
    Route::get('bankname/delete/{id}', 'BanknameController@destroy');
    Route::get('bankname/active/{id}', 'BanknameController@active');
    Route::get('bankname/inactive/{id}', 'BanknameController@inactive');

    Route::get('tags', 'TagsController@index');
    Route::get('tags/create', 'TagsController@create');
    Route::post('tags/store', 'TagsController@store');
    Route::get('tags/edit/{id}', 'TagsController@edit');
    Route::post('tags/edits', 'TagsController@edits');
    Route::post('tags/update/{id}', 'TagsController@update');
    Route::get('tags/delete/{id}', 'TagsController@destroy');
    Route::get('tags/active/{id}', 'TagsController@active');
    Route::get('tags/inactive/{id}', 'TagsController@inactive');

    Route::get('accounttype', 'AccounttypeController@index');
    Route::get('accounttype/create', 'AccounttypeController@create');
    Route::post('accounttype/store', 'AccounttypeController@store');
    Route::get('accounttype/edit/{id}', 'AccounttypeController@edit');
    Route::post('accounttype/edits', 'AccounttypeController@edits');
    Route::post('accounttype/update/{id}', 'AccounttypeController@update');
    Route::get('accounttype/delete/{id}', 'AccounttypeController@destroy');
    Route::get('accounttype/active/{id}', 'AccounttypeController@active');
    Route::get('accounttype/inactive/{id}', 'AccounttypeController@inactive');

    Route::get('paymentmode', 'PaymentmodeController@index');
    Route::get('paymentmode/create', 'PaymentmodeController@create');
    Route::post('paymentmode/store', 'PaymentmodeController@store');
    Route::get('paymentmode/edit/{id}', 'PaymentmodeController@edit');
    Route::post('paymentmode/edits', 'PaymentmodeController@edits');
    Route::post('paymentmode/update/{id}', 'PaymentmodeController@update');
    Route::get('paymentmode/delete/{id}', 'PaymentmodeController@destroy');
    Route::get('paymentmode/active/{id}', 'PaymentmodeController@active');
    Route::get('paymentmode/inactive/{id}', 'PaymentmodeController@inactive');

    Route::get('businesspackage', 'BusinesspackageController@index');
    Route::get('businesspackage/create', 'BusinesspackageController@create');
    Route::post('businesspackage/store', 'BusinesspackageController@store');
    Route::get('businesspackage/edit/{id}', 'BusinesspackageController@edit');
    Route::post('businesspackage/update/{id}', 'BusinesspackageController@update');
    Route::get('businesspackage/delete/{id}', 'BusinesspackageController@destroy');
    Route::get('businesspackage/active/{id}', 'BusinesspackageController@active');
    Route::get('businesspackage/inactive/{id}', 'BusinesspackageController@inactive');

    Route::get('role/list','RoleController@index');
    Route::get('role/create','RoleController@create');
    Route::post('role/save','RoleController@store');
    Route::get('role/edit/{id}','RoleController@edit');
    Route::post('role/update','RoleController@update');
    Route::get('role/delete/{id}','RoleController@destroy');

    Route::get('users/list','UserController@index');
    Route::get('users/create','UserController@create');
    Route::post('users/store','UserController@store');
    Route::get('users/edit/{id}','UserController@edit');
    Route::post('users/update/{id}','UserController@update');
    Route::get('users/delete/{id}','UserController@destroy');
    Route::get('users/active/{id}','UserController@active');
    Route::get('users/inactive/{id}','UserController@inactive');

    Route::get('parq','ParqController@index');
    Route::get('parq/create','ParqController@create');
    Route::post('parq/store','ParqController@store');
    Route::get('parq/edit/{id}','ParqControllerr@edit');
    Route::post('parq/update/{id}','ParqController@update');
    Route::get('parq/delete/{id}','ParqController@destroy');
    Route::get('parq/active/{id}','ParqController@active');
    Route::get('parq/inactive/{id}','ParqController@inactive');

});
Route::group(['namespace' => 'outlet','middleware' => ['locale'],'prefix'=>'outlet'], function () {

    Route::get('salesstaff', 'SalesstaffController@index');
    Route::get('salesstaff/create', 'SalesstaffController@create');
    Route::post('salesstaff/store', 'SalesstaffController@store');
    Route::get('salesstaff/edit/{id}', 'SalesstaffController@edit');
    Route::post('salesstaff/edits', 'SalesstaffController@edits');
    Route::post('salesstaff/update/{id}', 'SalesstaffController@update');
    Route::get('salesstaff/delete/{id}', 'SalesstaffController@destroy');
    Route::get('salesstaff/active/{id}', 'SalesstaffController@active');
    Route::get('salesstaff/inactive/{id}', 'SalesstaffController@inactive');

    Route::get('addnewlead', 'AddnewleadController@index');
    Route::get('addnewlead/create', 'AddnewleadController@create');
    Route::post('addnewlead/store', 'AddnewleadController@store');
    Route::get('addnewlead/view/{id}', 'AddnewleadController@view');
    Route::get('addnewlead/feedlist/{id}', 'AddnewleadController@feedlist');
    Route::get('addnewlead/edit/{id}', 'AddnewleadController@edit');
    Route::post('addnewlead/update/{id}', 'AddnewleadController@update');
    Route::get('addnewlead/destroy/{id}', 'AddnewleadController@destroy');
    Route::get('addnewlead/active/{id}', 'AddnewleadController@active');
    Route::get('addnewlead/inactive/{id}', 'AddnewleadController@inactive');
    Route::post('addnewlead/feedstore', 'AddnewleadController@feedstore');
    Route::get('followupleads', 'AddnewleadController@followupleads');
    Route::get('activeleads', 'AddnewleadController@activeleads');
    Route::get('archivedleads', 'AddnewleadController@archivedleads');

    Route::get('addnewmember', 'AddnewmemberController@index');
    Route::get('addnewmember/create', 'AddnewmemberController@create');
    Route::post('addnewmember/store', 'AddnewmemberController@store');
    Route::get('addnewmemeber/detail/{id}', 'AddnewmemberController@view');
    Route::get('addnewmember/edit/{id}', 'AddnewmemberController@edit');
    Route::post('addnewmember/update/{id}', 'AddnewmemberController@update');
    Route::get('addnewmember/destroy/{id}', 'AddnewmemberController@destroy');
    Route::get('addnewmember/active/{id}', 'AddnewmemberController@active');
    Route::get('addnewmember/inactive/{id}', 'AddnewmemberController@inactive');
    Route::get('invoice/{id}', 'AddnewmemberController@invoice');
    Route::post('packagelist', 'AddnewmemberController@packagelist');
    Route::post('packagestore/{id}', 'AddnewmemberController@packagestore');
    Route::get('packageindexlist/{id}', 'AddnewmemberController@packageindexlist');
    Route::post('invoicestore/{id}', 'AddnewmemberController@invoicestore');
    Route::get('total/{id}', 'AddnewmemberController@total');
    Route::post('paymentdetail', 'AddnewmemberController@paymentdetail');

    Route::get('generateinvoice', 'TransactionController@invoice');
    Route::post('invoicestore', 'TransactionController@invoicestore');
    Route::get('invoicelist', 'TransactionController@invoicelist');
    Route::get('paymentcollection', 'TransactionController@paymentcollection');
    Route::get('staffcash', 'TransactionController@staffcash');
    Route::get('packagedetail','SetupController@package');
    Route::get('tax','SetupController@taxdetails');
    Route::get('leadstatus','SetupController@leadstatus');
    Route::get('roles','SetupController@roles');

    Route::get('workout', 'WorkoutController@index');
    Route::get('workout/create', 'WorkoutController@create');
    Route::post('workout/store', 'WorkoutController@store');

    Route::get('webcam', 'WebcamController@index');
    Route::get('webcam/create', 'WebcamController@create');
    Route::get('sms', 'SmsController@index');
    Route::post('sms/store', 'SmsController@index');
    Route::post('sendsms','SmsController@sendsms');

    Route::post('getassignWorkOut','AddnewmemberController@getassignWorkOut');
    Route::post('assignWorkOut','AddnewmemberController@assignWorkOut');
    Route::get('view-workout-detail/{id}','WorkoutController@workoutdetail');
    Route::get('nutrition', 'NutritionController@index');
    Route::get('nutrition/create', 'NutritionController@create');
    Route::post('nutrition/store', 'NutritionController@store');
    Route::post('getnutritionfood','NutritionController@getnutritionfood');
    Route::post('deleteNutrifood','NutritionController@deleteNutrifood');
    Route::post('getassignNutrition','AddnewmemberController@getassignNutrition');
    Route::post('assignNutrition','AddnewmemberController@assignNutrition');
});

